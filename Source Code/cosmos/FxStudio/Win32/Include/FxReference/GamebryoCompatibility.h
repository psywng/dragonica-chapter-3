/*****************************************************************

    MODULE    : GamebryCompatibility.h

    PURPOSE   : Assists with backwards compatibility for older Gamebryo engines.

    CREATED   : 11/18/2008

    COPYRIGHT : (C) 2008 Aristen, Inc.

*****************************************************************/

#ifndef GAMEBRYOCOMPATIBILITY_H_
#define GAMEBRYOCOMPATIBILITY_H_

#include <NiVersion.h>

//	commented by sugulee
//#if ( GAMEBRYO_MAJOR_VERSION > 2 || (GAMEBRYO_MAJOR_VERSION == 2 && GAMEBRYO_MINOR_VERSION >= 6) )
//#define USING_LATEST_GAMEBRYO
//#elif (GAMEBRYO_MAJOR_VERSION == 2 && GAMEBRYO_MINOR_VERSION >= 5)
//#define USING_GAMEBRYO_2_5
//#else
//#error This integration works with Gamebryo 2.5 and later.
//#endif


#endif // GAMEBRYOCOMPATIBILITY_H_
