/*****************************************************************

    MODULE    : FxReference.h

    PURPOSE   : A single include file for all the headers needed
				to use the Emergent integration.
				
    CREATED   : 10/23/2008 

    COPYRIGHT : (C) 2008 Aristen, Inc.

*****************************************************************/

#ifndef FXREFERENCE_H
#define FXREFERENCE_H

#include "FxReference/FxStudioReferenceLibType.h"
#include "FxReference/FxManager.h"
#include "FxReference/FxObject.h"
#include "FxReference/FxPreview.h"
#include "FxReference/FxAssetManager.h"
#include "FxReference/FxSoundManager.h"
#include "FxReference/FxStudioFxObjectInterface.h"
#include "FxReference/GamebryoCompatibility.h"

#endif // FXREFERENCE_H