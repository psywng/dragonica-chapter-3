/*****************************************************************

    MODULE    : FxStudio.h

    PURPOSE   : This is a single include file that includes 
                everything needed to use the FxStudio SDK.

    CREATED   : 5/2/2008 

    COPYRIGHT : (C) 2008 Aristen, Inc.

*****************************************************************/

#ifndef FXSTUDIO_H_
#define FXSTUDIO_H_

#include "FxStudioRT/FxStudioDefines.h"

#include "FxStudioRT/ErrorData.h"
#include "FxStudioRT/Allocator.h"
#include "FxStudioRT/Manager.h"
#include "FxStudioRT/FxInstance.h"
#include "FxStudioRT/Component.h"
#include "FxStudioRT/Property.h"
#include "FxStudioRT/PropertyType.h"
#include "FxStudioRT/Preview.h"

#endif // FXSTUDIO_H_