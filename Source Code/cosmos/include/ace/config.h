//#define ACE_WIN32							1

//#define ACE_HAS_WCHAR                       1
#define ACE_HAS_IPV6                        1
#define ACE_HAS_SSL                         1
#define ACE_MT_SAFE                         1
#define ACE_USES_IPV4_IPV6_MIGRATION		1


#define ACE_HAS_WINNT4						1
#define ACE_HAS_STANDARD_CPP_LIBRARY		1
//#define ACE_USES_STD_NAMESPACE_FOR_STDCPP_LIB	0
//#define ACE_HAS_MFC					1
//#define ACE_USES_STATIC_MFC			1
#define ACE_AS_STATIC_LIBS					1
#define ACE_NO_INLINE						1
//#define ACE_LACKS_IOSTREAM_TOTALLY
//#define HAVE_STREAMBUF				1
#define ACE_DEFAULT_BACKLOG					5
//#define _USE_32BIT_TIME_T
//#define ACE_LEGACY_MODE		1
#define ACE_HAS_UNICODE						1
#define ACE_HAS_DEFERRED_TIMER_COMMANDS		1
#include "config-win32.h"
