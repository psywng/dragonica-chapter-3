template< typename T_INPUT >
CPacket::CPacket(DEF_PACKET_TYPE const wPacketType, T_INPUT const& input)
{
	CommonInit();

	Reserve( sizeof(wPacketType) + sizeof(T_INPUT) );
	Push(wPacketType);
	Push(input);
}

template < typename T_ELEMENT >
bool CPacket::Pop(T_ELEMENT &rElement)
{
	size_t const element_size = sizeof(T_ELEMENT);
	return PopMemory( &rElement, element_size );
}

template< typename T_VALUE >
bool CPacket::Pop( std::vector< T_VALUE > &vecout, size_t const max_count)
{
	vecout.clear();
	size_t arr_size = 0;
	if( Pop( arr_size ) 
	&&	arr_size <= max_count)
	{
		if( arr_size ) 
		{
			if( RemainSize() >= arr_size*sizeof(T_VALUE) )
			{
				vecout.resize(arr_size);
				return PopMemory(&vecout.at(0), vecout.size() * sizeof(std::vector< T_VALUE >::value_type));
			}
		}
		else
		{
			return true;
		}
	}
	return false;
}

template< typename T_VALUE >
bool CPacket::Pop( std::list< T_VALUE > &out, size_t const max_count)
{
	out.clear();
	size_t arr_size = 0;
	if( Pop( arr_size ) 
	&&	arr_size <= max_count)
	{
		if( arr_size )
		{
			if( RemainSize() >= arr_size*sizeof(T_VALUE) )
			{
				while( arr_size )
				{
					std::list< T_VALUE >::value_type element;
					if(! Pop( element ) ){return false;}
					out.push_back( element );

					--arr_size;
				}
				return true;
			}
		}
		else
		{
			return true;
		}
	}
	return false;
}

template< typename T_KEY, typename T_VALUE >
bool CPacket::Pop( std::map< T_KEY, T_VALUE > &out, size_t const max_count)
{
	out.clear();
	size_t arr_size = 0;
	if( Pop( arr_size ) 
	&&	arr_size <= max_count)
	{
		if( arr_size )
		{
			if( RemainSize() >= arr_size*(sizeof(T_KEY)+sizeof(T_VALUE)) )
			{
				while( arr_size )
				{
					std::map< T_KEY, T_VALUE >::key_type first;
					std::map< T_KEY, T_VALUE >::mapped_type second;

					if(! Pop( first ) ){return false;}
					if(! Pop( second ) ){return false;}
					
					std::map< T_KEY, T_VALUE >::_Pairib ret = out.insert( std::make_pair( first, second ) );

					if( !ret.second )
					{
						assert(false);
						return false;
					}
					--arr_size;
				}
				return true;
			}
		}
		else
		{
			return true;
		}
	}
	return false;
}

template< typename T_KEY, typename T_VALUE >
bool CPacket::Pop( std::multimap< T_KEY, T_VALUE > &input, size_t const max_count)
{
	out.clear();
	size_t arr_size = 0;
	if( Pop( arr_size ) 
	&&	arr_size <= max_count)
	{
		if( arr_size )
		{
			if( RemainSize() >= arr_size*(sizeof(T_KEY)+sizeof(T_VALUE)) )
			{
				while( arr_size )
				{
					std::map< T_KEY, T_VALUE >::key_type first;
					std::map< T_KEY, T_VALUE >::mapped_type second;

					if(! Pop( first ) ){return false;}
					if(! Pop( second ) ){return false;}
					
					std::inserter(out, out.end()) = std::make_pair( first, second );
					--arr_size;
				}
				return true;
			}
		}
		else
		{
			return true;
		}
	}
	return false;
}

template< typename T_KEY, typename T_VALUE, typename T_HASHER >
bool CPacket::Pop( stdext::hash_map< T_KEY, T_VALUE, T_HASHER > &out, size_t const max_count)
{
	out.clear();
	size_t arr_size = 0;
	if( Pop( arr_size ) 
	&&	arr_size <= max_count)
	{
		if( arr_size )
		{
			if( RemainSize() >= arr_size*(sizeof(T_KEY)+sizeof(T_VALUE)) )
			{
				while( arr_size )
				{
					stdext::hash_map< T_KEY, T_VALUE, T_HASHER >::key_type first;
					stdext::hash_map< T_KEY, T_VALUE, T_HASHER >::mapped_type second;

					if(! Pop( first ) ){return false;}
					if(! Pop( second ) ){return false;}
					
					stdext::hash_map< T_KEY, T_VALUE, T_HASHER >::_Pairib ret = out.insert( std::make_pair( first, second ) );

					if( !ret.second )
					{
						assert(false);
						return false;
					}
					--arr_size;
				}
				return true;
			}
		}
		else
		{
			return true;
		}
	}
	return false;
}

template< typename T_VALUE >
bool CPacket::Pop( std::set< T_VALUE > &out, size_t const max_count)
{
	out.clear();

	size_t arr_size = 0;
	if( Pop(arr_size) 
	&&	arr_size <= max_count)
	{
		if( arr_size )
		{
			if( RemainSize() >= arr_size*sizeof(T_VALUE) )
			{
				while(arr_size)
				{
					std::set< T_VALUE >::value_type kValue;
					if( !Pop(kValue) ) {return false;};
					
					out.insert( kValue );
					--arr_size;
				}
				return true;
			}
		}
		else
		{
			return true;
		}
	}
	return false;
}

template< typename T_VALUE >
bool CPacket::Pop( std::deque< T_VALUE > &out, size_t const max_count)
{
	std::vector< T_VALUE > vec;
	if( Pop(vec) )
	{
		out.assign(vec.begin(), vec.end());
		return true;
	}
	return false;
}

template< typename T_INPUT >
void CPacket::Push( T_INPUT const& input )
{
	size_t const in_size = sizeof( T_INPUT );

//	여기는 자주 호출 되므로. reserve 가 좋지 않다  Reserve( Size() + in_size );//메모리 예약

	Push( (void const*)&input, in_size);
}

template< >
void CPacket::Push( std::string const& input )
{
	size_t const in_size = input.length();
	
	Reserve( Size() + (sizeof(in_size) + in_size));//메모리 예약

	Push( in_size );
	Push( input.c_str(), in_size);
}

template< >
void CPacket::Push( std::wstring const& input )
{
	size_t const in_size = input.length();
	
	Reserve( Size() + (sizeof(in_size) + in_size*sizeof(std::wstring::value_type)));//메모리 예약

	Push( in_size );
	Push( input.c_str(), in_size * sizeof(std::wstring::value_type) );
}

template< >
void CPacket::Push( BM::vstring const& input )
{
	this->Push( static_cast<std::wstring>(input) );
}

template< >
void CPacket::Push(  CPacket const& input )
{
	if( this == &input)
	{
		return;
	}

	if( input.wr_pos_ > input.rd_pos_  )// 0이하가 되는 경우 막기위함.
	{
		size_t const in_size = input.wr_pos_ - input.rd_pos_;

		Reserve( Size() + in_size );//메모리 예약

		Push( &input.m_vecData.at( input.rd_pos_ ), in_size );
	}
}

template< typename T_VALUE >
void CPacket::Push( std::vector< T_VALUE > const& input )
{
	size_t const in_size = input.size();
	Reserve( Size() + (sizeof(in_size) + in_size * sizeof(T_VALUE)));//메모리 예약

	Push( in_size );
	if( in_size )
	{
		Push( &input.at(0), input.size() * sizeof(std::vector< T_VALUE >::value_type) );
	}
}

template< typename T_VALUE >
void CPacket::Push( std::list< T_VALUE > const& input )
{
	size_t const in_size = input.size();
	Reserve( Size() + (sizeof(in_size) + in_size * sizeof(T_VALUE)) );//메모리 예약

	Push( in_size );
	if( in_size )
	{
		std::list< T_VALUE >::const_iterator itor = input.begin();
		while( input.end() != itor )
		{
			Push( (*itor) );
			++itor;
		}
	}
}

template< typename T_KEY, typename T_VALUE >
void CPacket::Push( std::map< T_KEY, T_VALUE > const& input )
{
	size_t const in_size = input.size();
	Reserve( Size() + (sizeof(in_size) + in_size * (sizeof(T_KEY) + sizeof(T_VALUE))));//메모리 예약

	Push( in_size );
	if( in_size )
	{
		std::map< T_KEY, T_VALUE >::const_iterator itor = input.begin();
		while( input.end() != itor )
		{
			Push( (*itor).first );
			Push( (*itor).second );
			++itor;
		}
	}
}

template< typename T_KEY, typename T_VALUE >
void CPacket::Push( std::multimap< T_KEY, T_VALUE > const& input )
{
	size_t const in_size = input.size();
	Reserve( Size() + (sizeof(in_size) + in_size * (sizeof(T_KEY) + sizeof(T_VALUE))));//메모리 예약

	Push( in_size );
	if( in_size )
	{
		Reserve( Size() + (in_size * (sizeof(T_KEY) + sizeof(T_VALUE))));//메모리 예약

		std::map< T_KEY, T_VALUE >::const_iterator itor = input.begin();
		while( input.end() != itor )
		{
			Push( (*itor).first );
			Push( (*itor).second );
			++itor;
		}
	}
}

template< typename T_KEY, typename T_VALUE, typename T_HASHER >
void CPacket::Push( stdext::hash_map< T_KEY, T_VALUE, T_HASHER > const& input )
{
	size_t const in_size = input.size();
	Reserve( Size() + (sizeof(in_size) + in_size * (sizeof(T_KEY) + sizeof(T_VALUE))));//메모리 예약

	Push( in_size );
	if( in_size )
	{
		stdext::hash_map< T_KEY, T_VALUE, T_HASHER >::const_iterator itor = input.begin();
		while( input.end() != itor )
		{
			Push( (*itor).first );
			Push( (*itor).second );
			++itor;
		}
	}
}

template< typename T_VALUE >
void CPacket::Push( std::set< T_VALUE > const& input)
{
	size_t const in_size = input.size();
	Reserve( Size() + (sizeof(in_size) + in_size * (sizeof(T_VALUE))));//메모리 예약

	Push(in_size);
	std::set< T_VALUE >::const_iterator iter = input.begin();
	while(input.end() != iter)
	{
		Push( (*iter) );
		++iter;
	}
}

template< typename T_VALUE >
void CPacket::Push( std::deque< T_VALUE > const& input )
{
	std::vector< T_VALUE > vec(input.begin(),input.end());
	Push(vec);
}
