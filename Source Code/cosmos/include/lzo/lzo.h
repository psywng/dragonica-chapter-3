#include "lzo/minilzo.h"
#include "lzo/lzoconf.h"

#ifdef _MT_
	#pragma comment (lib, "lzo_MT.lib")
#endif

#ifdef _MTd_
	#pragma comment (lib, "lzo_MTd.lib")
#endif

#ifdef _MTo_
	#pragma comment (lib, "lzo_MTo.lib")
#endif

#ifdef _MD_
	#pragma comment (lib, "lzo_MD.lib")
#endif

#ifdef _MDd_
	#pragma comment (lib, "lzo_MDd.lib")
#endif

#ifdef _MDo_
	#pragma comment (lib, "lzo_MDo.lib")
#endif