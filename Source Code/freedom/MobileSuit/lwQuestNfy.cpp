#include "StdAfx.h"
#include "lwUI.h"
#include "variant/PgQuestInfo.h"
#include "PgQuestMan.h"
#include "lwQuestNfy.h"
#include "PgPilotMan.h"
#include "PgWorldMapUI.h"
#include "PgResourceIcon.h"
#include "PgWorldMapPopUpUI.h"
#include "lwUIBook.h"

bool SetIconInfo(XUI::CXUI_Wnd* pkWnd, SResourceIcon const& rkRscIcon, SIconImage const& rkIconImage, int const iNo);

namespace lwQuestNfy
{
	bool RegisterWrapper(lua_State *pkState)
	{
		using namespace lua_tinker;
		def(pkState, "CheckNewQuestNfy", lwQuestNfy::lwCheckNewQuestNfy);
		def(pkState, "CallQuestNfyUI", lwQuestNfy::lwCallQuestNfyUI);
		def(pkState, "OnOverQuestNfyListItem", lwQuestNfy::lwOnOverQuestNfyListItem);
		def(pkState, "OnClickQuestNfyListItem", lwQuestNfy::lwOnClickQuestNfyListItem);
		def(pkState, "OnClickDetailQuestView", lwQuestNfy::lwOnClickDetailQuestView);
		def(pkState, "OnTickChangeTextToQuestNameAndQuestGroup", lwQuestNfy::lwOnTickChangeTextToQuestNameAndQuestGroup);
		def(pkState, "OnTickCheckQuestModify", lwQuestNfy::lwOnTickCheckQuestModify);

		return true;
	}

	void lwCheckNewQuestNfy(int const CallType, int const iQuestID)
	{
		ContQuestInfo	kQuestList;
		g_kQuestMan.GetQuest(kQuestList, QT_None);

		switch( CallType )
		{
		case EQNCT_LEVELUP:
			{//레벨업한거다
				if( !CheckNewQuestToLevelUP(kQuestList) )
				{
					return;
				}
			}break;
		case EQNCT_QUEST_CLEAR:
		default:
			{//퀘스트를 클리어 한거다
				if( !CheckNewQuestToQuestClear(iQuestID, kQuestList) )
				{
					return;
				}
			}break;
		}

		bool bIsScenario = false;
		bool bIsNormal = false;
		ContQuestInfo::iterator	quest_itor = kQuestList.begin();
		while( quest_itor != kQuestList.end() )
		{
			ContQuestInfo::mapped_type const& pkQuestInfo = quest_itor->second;

			if( QT_Scenario == pkQuestInfo->Type() )
			{
				bIsScenario = true;
			}
			else
			{
				bIsNormal = true;
			}
			++quest_itor;
		}

		XUI::CXUI_Wnd* pMainUI = XUIMgr.Activate(L"FRM_NEW_QUEST_NOTICE");
		if( pMainUI )
		{
			pMainUI->SetCustomData(&CallType, sizeof(CallType));
			XUI::CXUI_Builder* pBuilder = dynamic_cast<XUI::CXUI_Builder*>(pMainUI->GetControl(L"BLD_BTN"));
			if( pBuilder )
			{
				int const iTotalIDX = pBuilder->CountX() * pBuilder->CountY();
				for(int i = 0; i < iTotalIDX; ++i)
				{
					BM::vstring	vStr(L"FRM_ITEM");
					vStr += i;
					XUI::CXUI_Wnd* pItem = pMainUI->GetControl(vStr);
					if( pItem )
					{
						pItem->SetCustomData(&iQuestID, sizeof(iQuestID));
						if( ( i == 0 && bIsScenario ) || ( i == 1 && bIsNormal ) )
						{
							pItem->Visible(true);
						}
						else
						{
							pItem->Visible(false);
						}
					}
				}
			}
		}
	}

	void lwCallQuestNfyUI(int const CallType, int const QuestID, bool const bIsScenario)
	{
		ContQuestInfo	kQuestList;

		g_kQuestMan.GetQuest(kQuestList, (bIsScenario)?(QT_Scenario):(QT_None));

		switch( CallType )
		{
		case EQNCT_LEVELUP:
			{//레벨업한거다
				if( !CheckNewQuestToLevelUP(kQuestList) )
				{
					lua_tinker::call<void, int, bool>("CommonMsgBoxByTextTable", 401061, true);
					return;
				}
			}break;
		default:
			{//퀘스트를 클리어 한거다
				if( !CheckNewQuestToQuestClear(QuestID, kQuestList) )
				{
					lua_tinker::call<void, int, bool>("CommonMsgBoxByTextTable", 401061, true);
					return;
				}
			}break;
		}

		if( !bIsScenario )
		{
			ContQuestInfo::iterator	quest_itor = kQuestList.begin();
			while( quest_itor != kQuestList.end() )
			{
				ContQuestInfo::mapped_type const& pkQuestInfo = quest_itor->second;

				if( QT_Scenario == pkQuestInfo->Type() )
				{
					quest_itor = kQuestList.erase(quest_itor);
					continue;
				}
				++quest_itor;
			}
		}

		XUI::CXUI_Wnd* pMainUI = XUIMgr.Call(L"SFRM_NEW_QUEST");
		if( pMainUI )
		{
			XUI::CXUI_List* pUIList = dynamic_cast<XUI::CXUI_List*>(pMainUI->GetControl(L"QUEST_LIST"));
			if( !pUIList )
			{
				return;
			}
			
			XUIListUtil::SetMaxItemCount(pUIList, kQuestList.size());

			XUI::SListItem*	pListItem = pUIList->FirstItem();

			ContQuestInfo::iterator	quest_itor = kQuestList.begin();
			while( quest_itor != kQuestList.end() )
			{
				ContQuestInfo::key_type const& iQuestID = quest_itor->first;
				ContQuestInfo::mapped_type const& pkQuestInfo = quest_itor->second;

				if( pListItem && pListItem->m_pWnd )
				{
					XUI::CXUI_Wnd* pkQuestName = pListItem->m_pWnd->GetControl(L"FRM_NAME");
					XUI::CXUI_Wnd* pkGroupName = pListItem->m_pWnd->GetControl(L"FRM_GROUP_NAME");
					if( pkQuestName && pkGroupName )
					{
						float fNowTime = (g_pkApp != NULL)?(g_pkApp->GetAccumTime()):(0);
						pkQuestName->Visible(true);
						pkGroupName->Visible(false);
						pkQuestName->Text(TTW(pkQuestInfo->m_iTitleTextNo));
						pkQuestName->SetCustomData(&fNowTime, sizeof(fNowTime));
						pkGroupName->Text(L"");
						pkGroupName->ClearCustomData();
						ContImportanceQuest const& QuestList = g_kQuestMan.GetImportanceQuestList();
						if( !QuestList.empty() )
						{
							ContImportanceQuest::const_iterator group_itor = QuestList.begin();
							while(QuestList.end() != group_itor)
							{
								ContImportanceQuest::mapped_type const& kGroupInfo = (*group_itor).second;

								if( kGroupInfo.Find( QuestID ) )
								{
									pkGroupName->Text(TTW(kGroupInfo.iGroupNameNo));
									pkGroupName->SetCustomData(&kGroupInfo.iGroupNameNo, sizeof(kGroupInfo.iGroupNameNo));
									break;
								}
								++group_itor;
							}
						}
					}
					pListItem->m_pWnd->SetCustomData(&iQuestID, sizeof(iQuestID));

					XUI::CXUI_Wnd* pUICheck = pListItem->m_pWnd->GetControl(L"FRM_CHECK");
					if( pUICheck )
					{
						pUICheck->UVUpdate(1);
					}

					XUI::CXUI_Icon* pIcon = dynamic_cast<XUI::CXUI_Icon*>(pListItem->m_pWnd->GetControl(L"ICON_NPC"));
					if( pIcon )
					{
						pIcon->Visible(false);
						if (!pkQuestInfo->m_kNpc_Client.empty())
						{
							SQuestNpc const& rkNPC = (*pkQuestInfo->m_kNpc_Client.begin());
							SNpcCreateInfo kInfo;
							if (g_kWorldMapUI.GetNpc(rkNPC.kNpcGuid, kInfo))
							{
								SResourceIcon kRscIcon;
								SIconImage kIconImage;
								if (PgWorldMapPopUpUI::FindNPCFromActorName(kRscIcon, kIconImage, kInfo.wstrActor))
								{
									SetIconInfo(pIcon, kRscIcon, kIconImage, kInfo.iID);
									pIcon->Visible(true);
								}
							}
						}
					}
					pListItem = pUIList->NextItem(pListItem);
				}
				++quest_itor;
			}
		}
	}

	void lwOnOverQuestNfyListItem(lwUIWnd kWnd)
	{
		XUI::CXUI_Wnd* pSelf = kWnd.GetSelf();
		if( !pSelf )
		{
			return;
		}

		XUI::CXUI_List* pList = dynamic_cast<XUI::CXUI_List*>(pSelf->Parent());
		if( pList )
		{
			XUI::SListItem*	pListItem = pList->FirstItem();
			while( pListItem && pListItem->m_pWnd )
			{
				XUI::CXUI_Wnd* pUICheck = pListItem->m_pWnd->GetControl(L"FRM_CHECK");
				if( pUICheck && pUICheck->UVInfo().Index != 3)
				{
					pUICheck->UVUpdate(1);
				}
				pListItem = pList->NextItem(pListItem);
			}
		}

		XUI::CXUI_Wnd* pUICheck = pSelf->GetControl(L"FRM_CHECK");
		if( pUICheck && pUICheck->UVInfo().Index != 3 )
		{
			pUICheck->UVUpdate(2);
		}
	}

	void lwOnClickQuestNfyListItem(lwUIWnd kWnd)
	{
		XUI::CXUI_Wnd* pSelf = kWnd.GetSelf();
		if( !pSelf )
		{
			return;
		}

		XUI::CXUI_List* pList = dynamic_cast<XUI::CXUI_List*>(pSelf->Parent());
		if( pList )
		{
			XUI::SListItem*	pListItem = pList->FirstItem();
			while( pListItem && pListItem->m_pWnd )
			{
				XUI::CXUI_Wnd* pUICheck = pListItem->m_pWnd->GetControl(L"FRM_CHECK");
				if( pUICheck )
				{
					pUICheck->UVUpdate(1);
				}
				pListItem = pList->NextItem(pListItem);
			}
		}

		XUI::CXUI_Wnd* pUICheck = pSelf->GetControl(L"FRM_CHECK");
		if( pUICheck )
		{
			pUICheck->UVUpdate(3);
		}

		int iQuestID = 0;
		pSelf->GetCustomData(&iQuestID, sizeof(iQuestID));

		XUI::CXUI_Wnd* pMainUI = pList->Parent();
		if( pMainUI )
		{
			XUI::CXUI_Wnd* pDetail = pMainUI->GetControl(L"BTN_DETAIL");
			if( pDetail )
			{
				pDetail->SetCustomData(&iQuestID, sizeof(iQuestID));
			}
		}
	}

	void lwOnClickDetailQuestView(lwUIWnd kWnd)
	{
		XUI::CXUI_Wnd* pSelf = kWnd.GetSelf();
		if( !pSelf )
		{
			return;
		}

		int iQuestID = 0;
		pSelf->GetCustomData(&iQuestID, sizeof(iQuestID));

		while( pSelf->Parent() )
		{
			pSelf = pSelf->Parent();
		}
		pSelf->Close();

		lwUIWnd kBookUI = lua_tinker::call<lwUIWnd, int>("OpenBookPage", 3);
		if(kBookUI.IsNil())
		{
			return;
		}
		XUI::CXUI_Wnd* pkBookUI = kBookUI();
		if( !pkBookUI )
		{
			return;
		}

		XUI::CXUI_Wnd* pkQuestPage = pkBookUI->GetControl(L"CBTN_TAB3");
		if( !pkQuestPage )
		{
			return;
		}

		XUI::CXUI_Wnd* pkPage = pkBookUI->GetControl(L"FRM_PAGE3");
		if( !pkPage )
		{
			return;
		}

		XUI::CXUI_List* pkQuestList = dynamic_cast<XUI::CXUI_List*>(pkPage->GetControl(L"LST_QUEST"));
		if( !pkQuestList )
		{
			return;
		}

		XUI::SListItem* pkListItem = pkQuestList->FirstItem();
		while( pkListItem && pkListItem->m_pWnd )
		{
			int iFindID = 0;

			XUI::CXUI_Wnd* pkQuest =  pkListItem->m_pWnd->GetControl(L"BTN_QUEST");
			if( pkQuest )
			{
				pkQuest->GetCustomData(&iFindID, sizeof(iFindID));
				if( iQuestID == iFindID)
				{
					lwUIBook::OnClickBookQuest( lwUIWnd(pkQuest) );
					return;
				}
			}
			pkListItem = pkQuestList->NextItem(pkListItem);
		}

	}

	void lwOnTickChangeTextToQuestNameAndQuestGroup(lwUIWnd kItemWnd, float fTickTime)
	{
		XUI::CXUI_Wnd* pSelf = kItemWnd.GetSelf();
		if( pSelf )
		{
			XUI::CXUI_Wnd* pkGroupName = pSelf->GetControl(L"FRM_GROUP_NAME");
			if( pkGroupName )
			{
				int iGroupNameNo = 0;
				pkGroupName->GetCustomData(&iGroupNameNo, sizeof(iGroupNameNo));
				if( iGroupNameNo == 0 )
				{
					return;
				}

				XUI::CXUI_Wnd* pkQuestName = pSelf->GetControl(L"FRM_NAME");
				if( !pkQuestName )
				{
					return;
				}

				float fPrevTime = 0;
				pkQuestName->GetCustomData(&fPrevTime, sizeof(fPrevTime));

				float const fNowTime = (g_pkApp != NULL)?(g_pkApp->GetAccumTime()):(0);
				if( 0 == fTickTime || ((fNowTime - fPrevTime) < fTickTime) )
				{
					return;
				}
				pkQuestName->SetCustomData(&fNowTime, sizeof(fNowTime));
				pkQuestName->Visible(!pkQuestName->Visible());
				pkGroupName->Visible(!pkQuestName->Visible());
			}
		}
	}

	bool lwOnTickCheckQuestModify(int const CallType, int const QuestID, bool const bIsScenario)
	{
		ContQuestInfo	kQuestList;
		g_kQuestMan.GetQuest(kQuestList, (bIsScenario)?(QT_Scenario):(QT_None));

		switch( CallType )
		{
		case EQNCT_LEVELUP:
			{//레벨업한거다
				return  CheckNewQuestToLevelUP(kQuestList);
			}break;
		default:
			{//퀘스트를 클리어 한거다
				 return CheckNewQuestToQuestClear(QuestID, kQuestList);
			}break;
		}
		return false;
	}

	bool CheckNewQuestToLevelUP(ContQuestInfo& kQuestList)
	{
		PgPlayer*	pkPlayer = g_kPilotMan.GetPlayerUnit();
		if( !pkPlayer )
		{
			return false;
		}

		ContQuestInfo::iterator	iter = kQuestList.begin();
		while( iter != kQuestList.end() )
		{
			ContQuestInfo::key_type const& kQuestID = iter->first;
			ContQuestInfo::mapped_type const& pkQuestInfo = iter->second;

			if( QBL_None != PgQuestInfoUtil::CheckBeginQuest(pkPlayer, kQuestID, pkQuestInfo) )
			{
				iter = kQuestList.erase(iter);
				continue;
			}
			++iter;
		}

		iter = kQuestList.begin();
		while( iter != kQuestList.end() )
		{
			ContQuestInfo::key_type const& kQuestID = iter->first;
			ContQuestInfo::mapped_type const& pkQuestInfo = iter->second;

			if( pkQuestInfo->m_kLimit.iMinLevel < pkPlayer->GetAbil(AT_LEVEL) )
			{
				iter = kQuestList.erase(iter);
				continue;
			}
			++iter;
		}

		return !kQuestList.empty();
	}

	bool CheckNewQuestToQuestClear(int const ClearQuestID, ContQuestInfo& kQuestList)
	{
		PgPlayer*	pkPlayer = g_kPilotMan.GetPlayerUnit();
		if( !pkPlayer )
		{
			return false;
		}

		ContQuestInfo::iterator	iter = kQuestList.begin();
		while( iter != kQuestList.end() )
		{
			ContQuestInfo::key_type const& kQuestID = iter->first;
			ContQuestInfo::mapped_type const& pkQuestInfo = iter->second;

			bool bIsLive = false;

			ContQuestID::iterator and_itor = pkQuestInfo->m_kLimit_PreQuestAnd.begin();
			while( and_itor != pkQuestInfo->m_kLimit_PreQuestAnd.end() )
			{
				ContQuestID::value_type const& kQuestID = (*and_itor);

				if( kQuestID == ClearQuestID )
				{
					bIsLive = true;
					break;
				}
				++and_itor;
			}

			if( !bIsLive )
			{
				ContQuestID::iterator or_itor = pkQuestInfo->m_kLimit_PreQuestAnd.begin();
				while( or_itor != pkQuestInfo->m_kLimit_PreQuestAnd.end() )
				{
					ContQuestID::value_type const& kQuestID = (*or_itor);

					if( kQuestID == ClearQuestID )
					{
						bIsLive = true;
						break;
					}
					++or_itor;
				}
			}

			if( bIsLive )
			{
				++iter;
				continue;
			}

			iter = kQuestList.erase(iter);
		}

		iter = kQuestList.begin();
		while( iter != kQuestList.end() )
		{
			ContQuestInfo::key_type const& kQuestID = iter->first;
			ContQuestInfo::mapped_type const& pkQuestInfo = iter->second;

			if( QBL_None != PgQuestInfoUtil::CheckBeginQuest(pkPlayer, kQuestID, pkQuestInfo) )
			{
				iter = kQuestList.erase(iter);
				continue;
			}
			++iter;
		}

		return !kQuestList.empty();
	}
}
