#include "stdafx.h"
#include "ServerLib.h"
#include "PgChatMgrClient.h"
#include "PgNetwork.h"
#include "PgCommandMgr.h"
#include "PgPilotMan.h"
#include "PgPvPGame.h"
#include "PgClientParty.h"

int PgPvPGame::GetRandomPvPGround( bool const bDuel, bool const bMatchLevel, BYTE const ucNowUser )
{
	if ( bDuel )
	{

	}
	else
	{
		if( bMatchLevel )
		{//! 인원에 맞는 맵중에서 랜덤.
			CONT_DEF_PVP_GROUNDGROUP const *pkDefPvPGroup = NULL;
			g_kTblDataMgr.GetContDef(pkDefPvPGroup);

			if ( pkDefPvPGroup && !(pkDefPvPGroup->empty()) )
			{
				CONT_DEF_PVP_GROUNDGROUP::const_iterator pvp_itr = pkDefPvPGroup->find(ucNowUser);
				if( pkDefPvPGroup->end() != pvp_itr )
				{
					CONT_DEF_PVP_GROUND const &kGround = pvp_itr->second;
					CONT_DEF_PVP_GROUND::iterator gnd_itr;
					if( S_OK == ::RandomElement(kGround, gnd_itr))
					{//랜덤 나와랑~
						return *(gnd_itr);
					}
				}
			}
		}
		else
		{//그냥 랜덤
			CONT_DEF_PVP_GROUNDMODE const *pkDefPvPMode = NULL;
			g_kTblDataMgr.GetContDef(pkDefPvPMode);

			if ( pkDefPvPMode && !(pkDefPvPMode->empty()) )
			{
				CONT_DEF_PVP_GROUNDMODE::const_iterator pvp_itr;
				if(S_OK == RandomElement(*pkDefPvPMode, pvp_itr))
				{//랜덤 나와랑~
					return pvp_itr->first;
				}
			}
		}
	}
	return 0;
}

bool PgPvPGame::GetPvPGroundType( int const iGndNo, EPVPTYPE &kType, bool bRandom )
{
	CONT_DEF_PVP_GROUNDMODE const *pkDefPvPMode = NULL;
	g_kTblDataMgr.GetContDef(pkDefPvPMode);

	if ( pkDefPvPMode )
	{
		CONT_DEF_PVP_GROUNDMODE::const_iterator mode_itr = pkDefPvPMode->find(iGndNo);
		if ( mode_itr != pkDefPvPMode->end() )
		{
			EPVPTYPE const _kType = (EPVPTYPE)mode_itr->second.iType;
			if ( !bRandom )
			{
				return kType & _kType;
			}
			else
			{
				std::vector<EPVPTYPE> kTemp;
				kType = PVP_TYPE_DM;	
				while ( kType )
				{
					if ( kType & _kType )
					{
						kTemp.push_back(kType);
					}
					kType <<= 1;
				}

				if ( kTemp.empty() )
				{
					return PVP_TYPE_NONE;
				}

				std::random_shuffle(kTemp.begin(),kTemp.end());
				kType = *(kTemp.begin());
			}
			return true;
		}
	}

	kType = PVP_TYPE_NONE;
	return false;
}

bool PgPvPGame::GetGroundName( int const iGndNo, std::wstring &wstrName )
{
	CONT_DEF_PVP_GROUNDMODE const *pkDefPvPMode = NULL;
	g_kTblDataMgr.GetContDef(pkDefPvPMode);

	if ( pkDefPvPMode )
	{
		CONT_DEF_PVP_GROUNDMODE::const_iterator mode_itr = pkDefPvPMode->find(iGndNo);
		if ( mode_itr != pkDefPvPMode->end() )
		{
			wchar_t const *pText = NULL;
			if ( GetDefString( mode_itr->second.iName, pText ) )
			{
				wstrName = pText;
				return true;
			}
		}
	}
	return false;
}

bool PgPvPGame::GetPreviewImgPath( int const iGndNo, std::wstring &wstrPath )
{
	CONT_DEF_PVP_GROUNDMODE const *pkDefPvPMode = NULL;
	g_kTblDataMgr.GetContDef(pkDefPvPMode);

	if ( pkDefPvPMode )
	{
		CONT_DEF_PVP_GROUNDMODE::const_iterator mode_itr = pkDefPvPMode->find(iGndNo);
		if ( mode_itr != pkDefPvPMode->end() )
		{
			wstrPath = mode_itr->second.wstrPreviewImg;
			return true;
		}
	}
	return false;
}

bool PgPvPGame::OnClick_CreateRoom( XUI::CXUI_Wnd *pkUIWnd )
{
	if ( pkUIWnd )
	{
		if ( STATUS_LOBBY == m_eStatus )
		{
			XUI::CXUI_Wnd *pkTemp = pkUIWnd->GetControl(_T("SFRM_INPUT"));
			if ( pkTemp )
			{
				// 방이름
				XUI::CXUI_Edit *pkTemp_Edit = dynamic_cast<XUI::CXUI_Edit*>(pkTemp->GetControl(_T("EDT_NAME")));
				if ( pkTemp_Edit )
				{
					m_kRoomBaseInfo.m_wstrName = pkTemp_Edit->EditText();
					if ( g_kClientFS.Filter( m_kRoomBaseInfo.m_wstrName, false, FST_BADWORD ) 
						|| !g_kUnicodeFilter.IsCorrect(UFFC_PVP_ROOM_NAME, m_kRoomBaseInfo.m_wstrName)
						)
					{
						lua_tinker::call<void, char const*, bool >("CommonMsgBox", MB(TTW(200000)), true);
						return false;
					}
				}

				// 패스워드
				pkTemp_Edit = dynamic_cast<XUI::CXUI_Edit*>(pkTemp->GetControl(_T("EDT_PW")));
				if ( pkTemp_Edit )
				{
					m_kRoomExtInfo.strPassWord = MB(pkTemp_Edit->EditText());
				}
				m_kRoomBaseInfo.m_bPwd = !m_kRoomExtInfo.strPassWord.empty();
				if ( m_kRoomBaseInfo.m_bPwd )
				{
					if ( m_kRoomExtInfo.strPassWord.size() != 4 )
					{
						lua_tinker::call<void, char const*, bool >("CommonMsgBox", MB(TTW(400406)), true);
						return false;
					}
				}
			}
			
			m_kRoomBaseInfo.m_kStatus = ROOM_STATUS_LOBBY;
			m_kRoomBaseInfo.m_iGndNo = lua_tinker::call<int,lwUIWnd>( "GetSelectPvP_Value", lwUIWnd(pkUIWnd->GetControl(_T("SFRM_TAB_MAPNAME"))) );
			m_kRoomBaseInfo.m_kType = lua_tinker::call<EPVPTYPE,lwUIWnd>( "GetSelectPvP_Value", lwUIWnd(pkUIWnd->GetControl(_T("SFRM_TAB_GAMETYPE"))) );
			m_kRoomBaseInfo.m_kMode = lua_tinker::call<EPVPMODE,lwUIWnd>( "GetSelectPvP_Value", lwUIWnd(pkUIWnd->GetControl(_T("SFRM_TAB_GAMEMODE"))) );
			m_kRoomBaseInfo.m_ucGameTime = lua_tinker::call<BYTE,lwUIWnd>( "GetSelectPvP_Value", lwUIWnd(pkUIWnd->GetControl(_T("SFRM_TAB_TIMELIMIT"))) );
			m_kRoomBaseInfo.m_sLevelLimit_Max = lua_tinker::call<short,lwUIWnd>( "GetSelectPvP_Value", lwUIWnd(pkUIWnd->GetControl(_T("SFRM_TAB_LEVELLIMIT"))) );

			if( false == IsExerciseType() )
			{
				m_kRoomBaseInfo.m_sLevelLimit_Min = GetRankingModeLevelMin();
			}
			else
			{
				m_kRoomBaseInfo.m_sLevelLimit_Min = 1;
			}

			m_kRoomBaseInfo.m_ucMaxUser = lua_tinker::call<BYTE,lwUIWnd>( "GetSelectPvP_Value", lwUIWnd(pkUIWnd->GetControl(_T("SFRM_TAB_MAXPLAYER"))) );
			m_kRoomExtInfo.ucRound = lua_tinker::call<BYTE,lwUIWnd>( "GetSelectPvP_Value", lwUIWnd(pkUIWnd->GetControl(_T("SFRM_TAB_ROUNDCOUNT"))) );

			pkTemp = pkUIWnd->GetControl(_T("SFRM_OPTION"));
			if ( pkTemp )
			{
				XUI::CXUI_CheckButton *pkTemp_CheckBtn = dynamic_cast<XUI::CXUI_CheckButton*>(pkTemp->GetControl(_T("CBTN_HANDYCAP")));
				if ( pkTemp_CheckBtn )
				{
					if ( true == pkTemp_CheckBtn->Check() )
					{
						m_kRoomExtInfo.kOption |= E_PVP_OPT_USEHANDYCAP;
					}
					else
					{
						m_kRoomExtInfo.kOption &= (~E_PVP_OPT_USEHANDYCAP);
					}
				}

				pkTemp_CheckBtn = dynamic_cast<XUI::CXUI_CheckButton*>(pkTemp->GetControl(_T("CBTN_USE_ITEM")));
				if ( pkTemp_CheckBtn )
				{
					if ( true == pkTemp_CheckBtn->Check() )
					{
						m_kRoomExtInfo.kOption |= E_PVP_OPT_USEITEM;
					}
					else
					{
						m_kRoomExtInfo.kOption &= (~E_PVP_OPT_USEITEM);
					}
				}

				pkTemp_CheckBtn = dynamic_cast<XUI::CXUI_CheckButton*>(pkTemp->GetControl(_T("CBTN_DISABLE_DASHJUMP")));
				if ( pkTemp_CheckBtn )
				{
					if ( true == pkTemp_CheckBtn->Check() )
					{
						m_kRoomExtInfo.kOption |= E_PVP_OPT_DISABLEDASHJUMP;
					}
					else
					{
						m_kRoomExtInfo.kOption &= (~E_PVP_OPT_DISABLEDASHJUMP);
					}
				}

				pkTemp_CheckBtn = dynamic_cast<XUI::CXUI_CheckButton*>(pkTemp->GetControl(_T("CBTN_USE_BATTLELEVEL")));
				if ( pkTemp_CheckBtn )
				{
					if ( true == pkTemp_CheckBtn->Check() )
					{
						m_kRoomExtInfo.kOption |= E_PVP_OPT_USEBATTLELEVEL;
					}
					else
					{
						m_kRoomExtInfo.kOption &= (~E_PVP_OPT_USEBATTLELEVEL);
					}
				}
			}
			
			BM::CPacket kPacket(PT_C_T_REQ_CREATE_ROOM);
			m_kRoomBaseInfo.WriteToPacket(kPacket);
			m_kRoomExtInfo.WriteToPacket(kPacket);
			NETWORK_SEND(kPacket)
			return true;
		}
	}
	return false;
}

bool PgPvPGame::OnClick_JoinRoom( CONT_PVPROOM_LIST::key_type const &iRoomID, std::string const &strPwd )
{
	CONT_PVPROOM_LIST::const_iterator room_itr = m_kContRoom.find(iRoomID);
	if ( room_itr != m_kContRoom.end() )
	{
		if ( room_itr->second.m_bPwd && strPwd.empty() )
		{
			// 패스워드를 입력하게 해야 한다.
			XUI::CXUI_Wnd *pkWnd = XUIMgr.Activate( ms_FRM_PVP_JOIN_PASSWORD, true );
			if ( pkWnd )
			{
				pkWnd = pkWnd->GetControl( _T("BTN_OK") );
				if ( pkWnd )
				{
					pkWnd->SetCustomData( &(iRoomID), sizeof(iRoomID) );
				}
			}
			return false;
		}

		BM::CPacket kPacket( PT_C_T_REQ_JOIN_ROOM, room_itr->second.m_iRoomIndex );
		kPacket.Push(strPwd);
		kPacket.Push(true);// 클릭해서 입장
		NETWORK_SEND(kPacket)
		return true;
	}
	return false;
}

bool PgPvPGame::OnClick_RandomJoinRoom()
{
	PgPlayer * pkPlayer = g_kPilotMan.GetPlayerUnit();
	if ( pkPlayer )
	{
		short const sMyLevel = static_cast<short>(pkPlayer->GetAbil( AT_LEVEL ));

		std::vector<CONT_PVPROOM_LIST::key_type> kTempRoomList;
		CONT_PVPROOM_LIST::const_iterator room_itr = m_kContRoom.begin();
		for ( ; room_itr!=m_kContRoom.end(); ++room_itr )
		{
			if (	(room_itr->second.m_kStatus == ROOM_STATUS_LOBBY)
				&&	!room_itr->second.m_bPwd
				&&	(room_itr->second.m_ucMaxUser > room_itr->second.m_ucNowUser )
				&&	(room_itr->second.m_sLevelLimit_Min <= sMyLevel)
				&&	(room_itr->second.m_sLevelLimit_Max >= sMyLevel)
				)
			{
				kTempRoomList.push_back(room_itr->first);
			}
		}

		if ( kTempRoomList.empty() )
		{
			// 대기 방이 없습니다.
			//lua_tinker::call<void, char const*, bool >("CommonMsgBox", MB(TTW(200107)), true);

			if( false == IsExercise() )
			{
				// 랭킹모드에서 빠른 입장을 누른 경우 방을 만들어 준다.
				OnClick_EmptyCreateRoom();
				return true;
			}
		}
		else
		{
			std::random_shuffle( kTempRoomList.begin(), kTempRoomList.end() );
			
			CONT_PVPROOM_LIST::const_iterator room_itr = m_kContRoom.find(*kTempRoomList.begin());
			if( room_itr != m_kContRoom.end() )
			{
				int const iRoomIndex = room_itr->second.m_iRoomIndex;

				std::string strPwd;
				BM::CPacket kPacket( PT_C_T_REQ_JOIN_ROOM, iRoomIndex );
				kPacket.Push(strPwd);
				kPacket.Push(false);// 빠른 입장
				NETWORK_SEND(kPacket)
				return true;
			}
		}
	}
	return false;
}

bool PgPvPGame::OnClick_AutoJoinRoom()
{
	PgPlayer* pkMyPlayer = g_kPilotMan.GetPlayerUnit();
	if( m_kContCharToTeam.empty() 
		|| m_kContTeam.empty() 
		|| !pkMyPlayer )
	{
		return false;
	}

	int iRoomIndex = 0;
	CONT_PVPLEAGUE_CHAR_TO_TEAM::const_iterator charid_itr = m_kContCharToTeam.find(pkMyPlayer->GetID());
	if( m_kContCharToTeam.end() != charid_itr )
	{
		CONT_DEF_PVPLEAGUE_TEAM::const_iterator team_itr = m_kContTeam.find(charid_itr->second);
		if( m_kContTeam.end() != team_itr )
		{
			if( m_kTournamentInfo.iLeagueLevel == team_itr->second.iLeagueLevel )
			{
				BM::CPacket kPacket( PT_C_T_REQ_JOIN_LEAGUE_ROOM);
				kPacket.Push( team_itr->first );
				NETWORK_SEND( kPacket );
				return true;
			}
		}
	}

	return false;
}

bool PgPvPGame::OnClick_RandomInviteUser()
{
	using namespace XUI;
	CXUI_List2 *pkList = GetLobbyList();
	if ( pkList )
	{
		VEC_GUID kVecGuidList;
		kVecGuidList.reserve( pkList->GetSize() );

		PgLobbyUserUI* pItem = NULL;
		CXUI_List2::CONT_LIST::const_iterator itr = pkList->Begin();
		for ( ; itr!=pkList->End() ; ++itr )
		{
			pItem = dynamic_cast<PgLobbyUserUI*>(*itr);
			if ( pItem && !(pItem->IsClosed()) )
			{
				if ( PVPUTIL::IsLobbyUser(pItem->m_kUserInfo) )
				{
					kVecGuidList.push_back( pItem->OwnerGuid() );
				}
			}
		}

		if ( kVecGuidList.size() )
		{
			std::random_shuffle( kVecGuidList.begin(), kVecGuidList.end() );

			size_t iSize = (kVecGuidList.size() > PVP_INVITE_MAX_USER_COUNT) ? PVP_INVITE_MAX_USER_COUNT : kVecGuidList.size();
			BM::CPacket kPacket( PT_C_T_REQ_INVITE_PVP, iSize );

			VEC_GUID::iterator guid_itr = kVecGuidList.begin();
			while ( iSize-- )
			{
				kPacket.Push( *guid_itr );
				++guid_itr;
			}
			NETWORK_SEND( kPacket )
			return true;
		}
	}
	return false;
}

void PgPvPGame::RefreshPageControl( XUI::CXUI_Wnd *pkUIWnd )
{
	for ( int iIndex=0; iIndex<5; ++iIndex )
	{
		BM::vstring vstrBtnName(L"BTN_NUM_");
		vstrBtnName += iIndex;
		XUI::CXUI_Wnd * pkBtn = pkUIWnd->GetControl(vstrBtnName.operator const std::wstring &());
		if( pkBtn )
		{
			vstrBtnName = m_kPage.iNowPage + iIndex +1;
			pkBtn->Text(vstrBtnName);
		}
	}
}

bool PgPvPGame::OnClick_ChangePage( size_t iPage )
{
	if ( m_kPage.iNowPage != iPage )
	{
		m_kPage.iNowPage = iPage;
		RefreshRoomList();
		return true;
	}
	return false;
}

bool PgPvPGame::OnClick_LeagueLobbyChangePage( XUI::CXUI_Wnd *pkUIWnd )
{
	XUI::CXUI_Wnd * pkWndTop = pkUIWnd->Parent();
	if( pkWndTop )
	{
		size_t iPage = 0;
		pkUIWnd->GetCustomData( &iPage, sizeof(size_t) );
		if ( m_kPage.iNowPage != m_iBeginPageNo+iPage )
		{
			BM::vstring vstrBtnName(L"BTN_NUM_");
			vstrBtnName += m_kPage.iNowPage - m_iBeginPageNo;
			XUI::CXUI_Wnd * pkOldBtn = pkWndTop->GetControl(vstrBtnName.operator const std::wstring &());
			if( pkOldBtn )
			{
				m_kPage.iNowPage = m_iBeginPageNo+iPage;
				pkOldBtn->Enable(true);
				pkUIWnd->Enable(false);
				RefreshPageControl(pkUIWnd);
				RefreshLeagueLobby();
			}
			return true;
		}
	}
	return false;
}

bool PgPvPGame::OnClick_LeagueLobbyNextPage( XUI::CXUI_Wnd *pkUIWnd )
{
	if( m_iEndPageNo >= m_iBeginPageNo+1 )
	{
		++m_iBeginPageNo;
		++m_kPage.iNowPage;
		RefreshPageControl(pkUIWnd);
		RefreshLeagueLobby();
		return true;
	}
	return false;
}

bool PgPvPGame::OnClick_LeagueLobbyPrevPage( XUI::CXUI_Wnd *pkUIWnd )
{
	if( 0 < m_iBeginPageNo )
	{
		--m_iBeginPageNo;
		--m_kPage.iNowPage;
		RefreshPageControl(pkUIWnd);
		RefreshLeagueLobby();
		return true;
	}
	return false;
}

bool PgPvPGame::OnClick_LeagueLobbyBeginPage( XUI::CXUI_Wnd *pkUIWnd )
{
	int iOrgPageNo = m_kPage.iNowPage - m_iBeginPageNo;
	BM::vstring vstrBtnName(L"BTN_NUM_");
	vstrBtnName += iOrgPageNo;
	XUI::CXUI_Wnd * pkOldBtn = pkUIWnd->GetControl(vstrBtnName.operator const std::wstring &());

	XUI::CXUI_Wnd * pkNewBtn = pkUIWnd->GetControl(L"BTN_NUM_0");
	if( pkOldBtn && pkNewBtn )
	{
		pkOldBtn->Enable(true);
		pkNewBtn->Enable(false);
		m_iBeginPageNo = 0;
		m_kPage.iNowPage = 0;
		RefreshPageControl(pkUIWnd);
		RefreshLeagueLobby();
		return true;
	}

	return false;
}

bool PgPvPGame::OnClick_LeagueLobbyEndPage( XUI::CXUI_Wnd *pkUIWnd )
{
	int iOrgPageNo = m_kPage.iNowPage - m_iBeginPageNo;
	BM::vstring vstrBtnName(L"BTN_NUM_");
	vstrBtnName += iOrgPageNo;
	XUI::CXUI_Wnd * pkOldBtn = pkUIWnd->GetControl(vstrBtnName.operator const std::wstring &());

	vstrBtnName = L"BTN_NUM_4";
	XUI::CXUI_Wnd * pkNewBtn = pkUIWnd->GetControl(vstrBtnName.operator const std::wstring &());

	if( pkOldBtn && pkNewBtn )
	{
		pkOldBtn->Enable(true);
		pkNewBtn->Enable(false);
		m_iBeginPageNo = m_iEndPageNo-4;
		m_kPage.iNowPage = m_iEndPageNo;
		RefreshPageControl(pkUIWnd);
		RefreshLeagueLobby();
		return true;
	}

	return false;
}

void PgPvPGame::OnClick_ViewWaitRoom( XUI::CXUI_Wnd *pkUI )
{
	if ( pkUI )
	{
		m_kPage.bOnlyWait = !m_kPage.bOnlyWait;
		RefreshRoomList();

		if ( m_kPage.bOnlyWait )
		{
			pkUI->Text( TTW(400299) );
		}
		else
		{
			pkUI->Text( TTW(400302) );
		}
	}
}

void PgPvPGame::OnClick_ViewRanking(void)
{
	UpdateRankingUI( XUIMgr.Activate( ms_SFRM_PVP_RANKING, true ), m_kRankIngMgr );
	
	BM::CPacket kPacket( PT_C_T_REQ_GET_PVPRANKING, m_kRankIngMgr.GetLastUpdateTime() );
	NETWORK_SEND( kPacket );
}

void PgPvPGame::OnClick_GameStart()
{
	if( IsMaster() && IsRanking() )
	{//방장이고 랭킹 모드면, 레벨에 맞는 맵을 골르자 m_kContPvPUser
		m_kRoomBaseInfo.m_iGndNo = GetRandomPvPGround(false, true, m_kContPvPUser.size() );
	}
	BM::CPacket kPacket(PT_C_T_REQ_GAME_READY);
	m_kRoomBaseInfo.WriteToPacket(kPacket);
	NETWORK_SEND(kPacket)
}

bool PgPvPGame::SetEdit_Ground( int const iGroundNo, bool bNoSend )
{
	//if ( true == IsModifyRoomAttr() )
	{
		std::wstring wstrName;
		if ( GetGroundName( iGroundNo, wstrName ) )
		{
			m_kRoomBaseInfo.m_iGndNo = iGroundNo;

			if ( !bNoSend )
			{
				BM::CPacket kPacket(PT_C_T_REQ_MODIFY_ROOM, true);
				m_kRoomBaseInfo.WriteToPacket(kPacket);
				NETWORK_SEND(kPacket)
			}
			return true;
		}
	}
	return false;
}

bool PgPvPGame::IsRoomMaster()
{	
	return IsMaster();
}

bool PgPvPGame::IsExerciseType()
{
	return IsExercise();
}

bool PgPvPGame::IsLeagueType()
{
	return IsLeague();
}

int const PgPvPGame::GetRankingModeLevel()
{
	return GetRankingModeLevelMax();
}

bool PgPvPGame::SetEdit_Mode( EPVPMODE kMode, bool bNoSend )
{
	if ( IsModifyRoomAttr() && IsPersonalGame() )
	{
		m_kRoomBaseInfo.m_kMode = kMode;
		if ( !bNoSend )
		{
			BM::CPacket kPacket(PT_C_T_REQ_MODIFY_ROOM, true);
			m_kRoomBaseInfo.WriteToPacket(kPacket);
			NETWORK_SEND(kPacket)
		}
		return true;
	}
	return false;
}

bool PgPvPGame::SetEdit_Type( EPVPTYPE kType, bool bNoSend )
{
	if ( true == IsModifyRoomAttr() )
	{
		if ( GetPvPGroundType( m_kRoomBaseInfo.m_iGndNo, kType, false ) )
		{
			m_kRoomBaseInfo.m_kType = kType;
			m_kRoomBaseInfo.m_ucGameTime = 3;

			if ( !bNoSend )
			{
				BM::CPacket kPacket(PT_C_T_REQ_MODIFY_ROOM, true);
				m_kRoomBaseInfo.WriteToPacket(kPacket);
				NETWORK_SEND(kPacket)
			}
			return true;
		}
	}
	return false;
}

bool PgPvPGame::SetEdit_Time( int const iMinTime, bool bNoSend )
{
	if ( true == IsModifyRoomAttr() )
	{
		m_kRoomBaseInfo.m_ucGameTime = (BYTE)iMinTime;
		if ( !bNoSend )
		{
			BM::CPacket kPacket(PT_C_T_REQ_MODIFY_ROOM, true);
			m_kRoomBaseInfo.WriteToPacket(kPacket);
			NETWORK_SEND(kPacket)
		}
		return true;
	}
	return false;
}

bool PgPvPGame::SetEdit_MaxLevel( short const sLevel, bool bNoSend )
{
	if ( true == IsModifyRoomAttr() )
	{
		CONT_PVP_GAME_USER::const_iterator user_itr = m_kContPvPUser.begin();
		for ( ; user_itr!=m_kContPvPUser.end(); ++user_itr )
		{
			if ( user_itr->second.sLevel > sLevel )
			{
				Notice_Show( TTW(200113), EL_Warning );
				return false;
			}
		}


		if( false == IsExerciseType() )
		{
			int const iRoomLevelMin = static_cast<int>(sLevel - 10);
			if( 0 < iRoomLevelMin )
			{
				m_kRoomBaseInfo.m_sLevelLimit_Min = iRoomLevelMin;
			}
			else
			{
				m_kRoomBaseInfo.m_sLevelLimit_Min = 1;
			}
		}
		
		m_kRoomBaseInfo.m_sLevelLimit_Max = sLevel;
		if ( !bNoSend )
		{
			BM::CPacket kPacket(PT_C_T_REQ_MODIFY_ROOM, true);
			m_kRoomBaseInfo.WriteToPacket(kPacket);
			NETWORK_SEND(kPacket)
		}
		return true;
	}
	return false;
}

bool PgPvPGame::SetEdit_RoundCount( BYTE const kRoundCount, bool bNoSend )
{
	if ( IsModifyRoomAttr() && IsRoundCount())
	{
		m_kRoomExtInfo.ucRound = kRoundCount;
		if ( !bNoSend )
		{
			BM::CPacket kPacket(PT_C_T_REQ_MODIFY_ROOM, false);
			m_kRoomExtInfo.WriteToPacket(kPacket);
			NETWORK_SEND(kPacket)
		}
		return true;
	}
	return false;
}

bool PgPvPGame::SetEdit_Option( BYTE const kOption, bool const bNoSend )
{
	if ( true == IsModifyRoomAttr() )
	{
		m_kRoomExtInfo.kOption ^= kOption;
		if ( !bNoSend )
		{
			BM::CPacket kPacket(PT_C_T_REQ_MODIFY_ROOM, false);
			m_kRoomExtInfo.WriteToPacket(kPacket);
			NETWORK_SEND(kPacket)
		}
		return true;
	}
	return false;
}

bool PgPvPGame::LoadPvPGroundList( XUI::CXUI_List* pList )
{
	if ( !pList )
	{
		return false;
	}

	CONT_DEF_PVP_GROUNDMODE const *pkDefPvPMode = NULL;
	g_kTblDataMgr.GetContDef(pkDefPvPMode);

	if ( pkDefPvPMode)
	{
		pList->ClearList();
		POINT2 kTemp(0,5);

		CONT_DEF_PVP_GROUNDMODE::const_iterator  mode_itr;
		for ( mode_itr=pkDefPvPMode->begin(); mode_itr!=pkDefPvPMode->end(); ++mode_itr )
		{
			CONT_DEF_PVP_GROUNDMODE::key_type const &kKey = mode_itr->first;
			CONT_DEF_PVP_GROUNDMODE::mapped_type const &kElement = mode_itr->second;

			std::wstring const *pwstrText = NULL;
			if ( GetDefString( kElement.iName, pwstrText ) )
			{
				XUI::SListItem* pListItem = pList->AddItem(*pwstrText);
				if( pListItem )
				{
					XUI::CXUI_Wnd* pkItemWnd = pListItem->m_pWnd;
					if( pkItemWnd )
					{
						pkItemWnd->Text(*pwstrText);
						pkItemWnd->SetCustomData( &kKey, sizeof(kKey) );
						kTemp.x = pkItemWnd->Size().x;
						kTemp.y += pkItemWnd->Size().y;
					}
				}
			}		
		}

		pList->Size(kTemp);

		XUI::CXUI_Wnd *pkParent = pList->Parent();
		if ( pkParent )
		{
			pkParent->Size(kTemp);
			XUI::CXUI_Wnd *pkOpBg = pkParent->GetControl(_T("FRM_Option_BG"));
			if ( pkOpBg )
			{
				POINT3I kPos = pkOpBg->Location();
				kPos.y = kTemp.y-4;
				pkOpBg->Location(kPos);
			}
		}
		return true;
	}
	return false;
}

bool PgPvPGame::LoadPvPTypeList( XUI::CXUI_List* pList, int const iGndNo )
{	
	if ( !pList )
	{
		return false;
	}

	CONT_DEF_PVP_GROUNDMODE const *pkDefPvPMode = NULL;
	g_kTblDataMgr.GetContDef(pkDefPvPMode);

	if ( pkDefPvPMode)
	{
		pList->ClearList();
		POINT2 kTemp(0,5);

		CONT_DEF_PVP_GROUNDMODE::const_iterator  mode_itr = pkDefPvPMode->find(iGndNo);
		if ( mode_itr != pkDefPvPMode->end() )
		{
			EPVPTYPE const _kType = (EPVPTYPE)mode_itr->second.iType;
			EPVPTYPE kType = PVP_TYPE_DM;
			do
			{
				if( kType & _kType )
				{
					if( PVP_TYPE_DM == kType
						|| PVP_TYPE_ANNIHILATION == kType)
					{//사투전/섬멸전만 리스트에 나오도록... 로비 타입별로 허용 타입을 수정하도록 디자인이 변경하고 여기 고치자.
						int const iModeName = lua_tinker::call<int, EPVPTYPE>("GetPvPTypeTextTable", kType );
						XUI::SListItem* pListItem = pList->AddItem( TTW(iModeName) );
						if( pListItem )
						{
							XUI::CXUI_Wnd* pkItemWnd = pListItem->m_pWnd;
							if( pkItemWnd )
							{
								pkItemWnd->Text( TTW(iModeName) );
								int iData = (int)kType;
								pkItemWnd->SetCustomData( &iData, sizeof(iData));

								kTemp.x = pkItemWnd->Size().x;
								kTemp.y += pkItemWnd->Size().y;
							}
						}
					}
				}
				kType <<= 1;
			} while ( kType );

			pList->Size(kTemp);

			XUI::CXUI_Wnd *pkParent = pList->Parent();
			if ( pkParent )
			{
				pkParent->Size(kTemp);
				XUI::CXUI_Wnd *pkOpBg = pkParent->GetControl(_T("FRM_Option_BG"));
				if ( pkOpBg )
				{
					POINT3I kPos = pkOpBg->Location();
					kPos.y = kTemp.y-4;
					pkOpBg->Location(kPos);
				}
			}
			return true;
		}
	}
	return false;
}
