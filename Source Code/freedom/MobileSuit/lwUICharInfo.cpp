#include "stdafx.h"
#include "lwUI.h"
#include "PgPilot.h"
#include "PgPilotMan.h"
#include "variant/Global.h"
#include "PgGuild.h"
#include "lwUICharInfo.h"
#include "PgMyActorViewMgr.h"
#include "lwUIPet.h"
#include "ServerLib.h"
#include "lwUIToolTip.h"

extern PgInventory g_kOtherViewInv;
extern bool GetDefString(int const iTextNo, wchar_t const *&pString);
extern void WstringFormat( std::wstring& rkOutMsg, size_t const iSize, const wchar_t* szMsg, ... );

namespace PgPetUIUtil
{
	void lwUI_RefreshPetSkillUI(lwUIWnd kWnd);
	void lwDrawTextToPetState(lwUIWnd kWnd = lwUIWnd(NULL));
	XUI::CXUI_Wnd* SetPetUIDefaultState(bool bEnableCover);
	XUI::CXUI_Wnd* SetPetUIDefaultState(PgBase_Item const& rkItem);
	void lwSetPetAbilInfo(BM::GUID const& rkGuid, XUI::CXUI_Wnd* pInfoMain, int iType, char const* szAddon);
	void lwSetPetUseRateInfo(BM::GUID const& rkGuid, XUI::CXUI_Wnd* pInfoMain, int iType, char const* szAddon);
	void InitPetInfoUI(XUI::CXUI_Wnd* pWnd);
};

bool lwCharInfo::RegisterWrapper(lua_State *pkState)
{
	using namespace lua_tinker;
	//-----> 케릭터 정보창 관련
	def(pkState, "SetCharInfoToUI", &lwCharInfo::lwSetCharInfoToUI);
	def(pkState, "SetCharInfo", &lwCharInfo::lwSetCharInfo);
	def(pkState, "SetCharAbilInfo", &lwCharInfo::lwSetCharAbilInfo);
	def(pkState, "ViewPlusAbilValue", &lwCharInfo::lwViewPlusAbilValue);
	def(pkState, "CallInfoDrop", &lwCharInfo::lwCallInfoDrop);
	def(pkState, "ViewEquipIconInitialize", &lwCharInfo::lwViewEquipIconInitialize);
	def(pkState, "ChangeInfoTab", &lwCharInfo::lwChangeInfoTab);
	def(pkState, "ChangeOtherInfoTab", &lwCharInfo::lwChangeOtherInfoTab);
	def(pkState, "SetPetInfoToUI", &lwCharInfo::lwSetPetInfoToUI);

	//<----- 여기까지
	return true;
}

void lwCharInfo::lwSetCharInfoToUI(lwUIWnd UIWnd, lwGUID Guid)
{
	SetCharInfoToUI(UIWnd.GetSelf(), Guid(), true);
	bool bIsMyActor = g_kPilotMan.IsMyPlayer(Guid());
	lwViewEquipIconInitialize(UIWnd, (bIsMyActor)?(KUIG_FIT):(KUIG_VIEW_OTHER_EQUIP));
}

void lwCharInfo::SetCharInfoToUI(XUI::CXUI_Wnd* pWnd, BM::GUID const& Guid, bool const bIsChanged)
{
	if( !pWnd )
	{
		return;
	}
	pWnd->OwnerGuid(Guid);
	pWnd->SetCustomData(&Guid, sizeof(Guid));

	PgPilot* pPilot = g_kPilotMan.FindPilot(Guid);
	if( !pPilot )
	{
		return;
	}

	CUnit* pUnit = pPilot->GetUnit();
	if( !pUnit )
	{
		return;
	}

	// 직업
	XUI::CXUI_Wnd* pCharIcon = pWnd->GetControl(_T("FRM_CHAR_ICON"));
	if( pCharIcon )
	{
		SUVInfo	Info = pCharIcon->UVInfo();
		Info.Index = pUnit->GetAbil(AT_CLASS);
		pCharIcon->UVInfo(Info);
		pCharIcon->Visible(true);
	}

	// 이름
	XUI::CXUI_Wnd* pCharText = pWnd->GetControl(_T("FRM_CHAR_TEXT"));
	if( pCharText )
	{
		BM::vstring	vStrName;
		vStrName = _T("[");
		vStrName += TTW(224);
		vStrName += _T(".");
		vStrName += pUnit->GetAbil(AT_LEVEL);
		vStrName += _T("]");
		vStrName += _T("\n");
		vStrName += pUnit->Name();

		pCharText->Text(vStrName);
		pCharText->Visible(true);
	}

	// 길드
	XUI::CXUI_Wnd* pGuildIcon = pWnd->GetControl(_T("FRM_GUILD_ICON"));
	XUI::CXUI_Wnd* pGuildText = pWnd->GetControl(_T("FRM_GUILD_TEXT"));
	if( pGuildIcon && pGuildText )
	{
		SGuildOtherInfo	kGuildInfo;
		if( g_kGuildMgr.GetGuildInfo(pUnit->GetGuildGuid(), pUnit->GetID(), kGuildInfo) )
		{
			pGuildIcon->Scale(GUILD_ICON_SCALE);
			SUVInfo	Info = pGuildIcon->UVInfo();
			Info.Index = kGuildInfo.cEmblem + 1;//UVIndex는 1부터 시작
			pGuildIcon->UVInfo(Info);

			pGuildText->Text(kGuildInfo.kName);
		}
		else
		{
			pGuildText->Text(_T(""));
		}

		pGuildIcon->Visible(false);
		pGuildText->Visible(false);
	}

	XUI::CXUI_CheckButton* pMyInfoBtn = dynamic_cast<XUI::CXUI_CheckButton*>(pWnd->GetControl(_T("CBTN_MY_INFO")));
	XUI::CXUI_CheckButton* pGuildInfoBtn = dynamic_cast<XUI::CXUI_CheckButton*>(pWnd->GetControl(_T("CBTN_GUILD_INFO")));
	if( !pMyInfoBtn || !pGuildInfoBtn )
	{
		return;
	}
	pMyInfoBtn->Check(true);
	pMyInfoBtn->ClickLock(true);
	pGuildInfoBtn->Check(false);
	pGuildInfoBtn->ClickLock(false);

	int iSet = lwConfig_GetValue("CHARINFO", "CHARINFO_LEFT");
	if (0 == iSet)
	{
		iSet = lwConfig_GetDefaultValue("CHARINFO", "CHARINFO_LEFT");
	}
	lwSetCharAbilInfo(lwUIWnd(pWnd), iSet-1, "L", bIsChanged);
	iSet = lwConfig_GetValue("CHARINFO", "CHARINFO_RIGHT");
	if (0 == iSet)
	{
		iSet = lwConfig_GetDefaultValue("CHARINFO", "CHARINFO_RIGHT");
	}
	lwSetCharAbilInfo(lwUIWnd(pWnd), iSet-1, "R", bIsChanged);
}

void lwCharInfo::lwSetCharAbilInfo(lwUIWnd UIWnd, int iType, char const* szAddon, bool const bIsChanged)
{
	XUI::CXUI_Wnd* pInfoMain = UIWnd.GetSelf();
	if( !pInfoMain )
	{
		return;
	}

	bool bIsPet = false;
	if( pInfoMain->ID().compare(L"FRM_CHAR_INFO") != 0 )
	{
		if(0==pInfoMain->ID().compare(L"FRM_INFO"))	//펫용
		{
			bIsPet = true;
		}
		else
		{
			pInfoMain = pInfoMain->GetControl(L"FRM_CHAR_INFO");
			if( !pInfoMain )
			{
				return;
			}
		}
	}

	BM::GUID kGuid = pInfoMain->OwnerGuid();
	if(bIsPet)
	{
		PgPlayer* pkPlayer = g_kPilotMan.GetPlayerUnit();
		if( !pkPlayer )
		{
			return;
		}

		kGuid = pkPlayer->SelectedPetID();
	}

	BM::vstring vTabTitle(_T("FRM_INFO_TYPE_"));
	vTabTitle += szAddon;

	bool bSaveConfig = false;
	XUI::CXUI_Wnd*	pTitle = pInfoMain->GetControl(vTabTitle);
	if( pTitle )
	{
		if( bIsChanged )
		{
			std::wstring	kStr(TTW(5995).c_str());
			kStr += _T("(");

			int iTTW = 790680 + iType-3;// 텍스트 번호가 다닥 다닥 붙어있어 이런 코드로 분리함 

			switch(iType)
			{
			case 0:
			case 1:
			case 2:
				{
					iTTW = 5992 + iType;
					bSaveConfig = true;
				}break;
			case 4:
				{
					iTTW = 3010;
				}break;
			case 5:
				{
					iTTW = 1804;
				}break;
			}

			pTitle->SetCustomData(&iType, sizeof(iType));
			
			kStr += TTW(iTTW);
			kStr += _T(")");
			pTitle->Text(kStr);
		}
		else
		{
			pTitle->GetCustomData(&iType, sizeof(iType));
		}
	}

	if(bIsPet)
	{
		PgPetUIUtil::lwSetPetAbilInfo(kGuid, pInfoMain, iType, szAddon);
		PgPetUIUtil::lwSetPetUseRateInfo(kGuid, pInfoMain, iType, szAddon);
		
		lwConfig_SetValue("CHARINFO", "PETINFO_LEFT", iType);
		return;//더 이상 돌 필요 없음
	}

	for(int i = 0; i < ABIL_INFO_MAX_SLOT; ++i)
	{
		BM::vstring vStr(_T("FRM_INFO_"));
		vStr += szAddon;
		vStr += i;

		XUI::CXUI_Wnd* pAbilSlot = pInfoMain->GetControl(vStr);
		if( !pAbilSlot )
		{
			continue;
		}

		int const iAbil = (iType * ABIL_INFO_MAX_SLOT) + i;
		switch( iAbil )
		{
		// 능력치 정보(기본)
		case  0:{ SetAbilValue(pAbilSlot, kGuid, AT_C_STR); }break;
		case  1:{ SetAbilValue(pAbilSlot, kGuid, AT_C_INT); }break;
		case  2:{ SetAbilValue(pAbilSlot, kGuid, AT_C_CON); }break;
		case  3:{ SetAbilValue(pAbilSlot, kGuid, AT_C_DEX); }break;
		// 능력치 정보(공격)
		case  4:{ SetAbilValue(pAbilSlot, kGuid, AT_C_PHY_ATTACK_MAX, TTW(400142).c_str());	}break;
		case  5:{ SetAbilValue(pAbilSlot, kGuid, AT_C_MAGIC_ATTACK_MAX, TTW(400143).c_str());	}break;
		case  6:{ SetAbilValue(pAbilSlot, kGuid, AT_C_HITRATE);		}break;
		case  7:{ SetAbilValue(pAbilSlot, kGuid, AT_C_DODGE_RATE);	}break;
		// 능력치 정보(방어)
		case  8:{ SetAbilValue(pAbilSlot, kGuid, AT_C_PHY_DEFENCE, TTW(400144).c_str());		}break;
		case  9:{ SetAbilValue(pAbilSlot, kGuid, AT_C_MAGIC_DEFENCE, TTW(400145).c_str());	}break;
		case 10:{ SetAbilValue(pAbilSlot, kGuid, AT_C_HP_RECOVERY);	}break;
		case 11:{ SetAbilValue(pAbilSlot, kGuid, AT_C_MP_RECOVERY);	}break;
		// 능력치 정보(보너스)
		case 12:{ SetAbilValue(pAbilSlot, kGuid, AT_STR_ADD);	}break;
		case 13:{ SetAbilValue(pAbilSlot, kGuid, AT_INT_ADD);	}break;
		case 14:{ SetAbilValue(pAbilSlot, kGuid, AT_CON_ADD);	}break;
		case 15:{ SetAbilValue(pAbilSlot, kGuid, AT_DEX_ADD);	}break;
		default:
			{
				//error
			}break;
		}
	}

	if(bSaveConfig)
	{
		if( 0 == strcmp("R", szAddon) )
		{
			lwConfig_SetValue("CHARINFO", "CHARINFO_RIGHT", iType+1);
		}
		else if( 0 == strcmp("L", szAddon) )
		{
			lwConfig_SetValue("CHARINFO", "CHARINFO_LEFT", iType+1);
		}
		lwConfig_ApplyConfig();
		lwConfig_Save(true);
	}
}

void lwCharInfo::lwSetCharInfo(lwUIWnd UIWnd, int const iType)
{
	XUI::CXUI_Wnd* pInfoMain = UIWnd.GetSelf();
	if( !pInfoMain )
	{
		return;
	}
	BM::GUID	Guid = UIWnd.GetOwnerGuid().GetGUID();

	XUI::CXUI_Wnd* pCharIcon = pInfoMain->GetControl(_T("FRM_CHAR_ICON"));
	XUI::CXUI_Wnd* pCharText = pInfoMain->GetControl(_T("FRM_CHAR_TEXT"));
	XUI::CXUI_Wnd* pGuildIcon = pInfoMain->GetControl(_T("FRM_GUILD_ICON"));
	XUI::CXUI_Wnd* pGuildText = pInfoMain->GetControl(_T("FRM_GUILD_TEXT"));
	XUI::CXUI_CheckButton* pCharBtn = dynamic_cast<XUI::CXUI_CheckButton*>(pInfoMain->GetControl(_T("CBTN_MY_INFO")));
	XUI::CXUI_CheckButton* pGuildBtn = dynamic_cast<XUI::CXUI_CheckButton*>(pInfoMain->GetControl(_T("CBTN_GUILD_INFO")));
	if( !pCharIcon || !pCharText || !pGuildIcon || !pGuildText || !pCharBtn || !pGuildBtn )
	{
		return;
	}

	pCharIcon->Visible(false);
	pCharText->Visible(false);
	pGuildIcon->Visible(false);
	pGuildText->Visible(false);
	pCharBtn->ClickLock(false);
	pCharBtn->Check(false);
	pGuildBtn->ClickLock(false);
	pGuildBtn->Check(false);
	switch(iType)
	{
	case 0:
		{
			pCharIcon->Visible(true);
			pCharText->Visible(true);
			pCharBtn->Check(true);	
			pCharBtn->ClickLock(true);
		}break;
	case 1:
		{
			PgPilot* pPilot = g_kPilotMan.FindPilot(Guid);
			if( !pPilot )
			{
				return;
			}

			CUnit* pUnit = pPilot->GetUnit();
			if( !pUnit )
			{
				return;
			}

			SGuildOtherInfo	kGuildInfo;
			if( g_kGuildMgr.GetGuildInfo(pUnit->GetGuildGuid(), pUnit->GetID(), kGuildInfo) )
			{
				pGuildIcon->Visible(true);
				pGuildText->Visible(true);
			}
			pGuildBtn->Check(true);
			pGuildBtn->ClickLock(true);
		}break;
	}
}

void lwCharInfo::SetAbilValue(XUI::CXUI_Wnd* pWnd, BM::GUID const& Guid, EAbilType Type, wchar_t const* pText)
{
	PgPilot* pPilot = g_kPilotMan.FindPilot(Guid);
	if( !pPilot )
	{
		return;
	}

	CUnit* pUnit = pPilot->GetUnit();
	if( !pUnit )
	{
		return;
	}

	int const iBaseAbil = GetBasicAbil(Type);

	if( !pText )
	{
		std::wstring AbilName;
		switch(Type)
		{
		case AT_STR_ADD:
			{
				AbilName = TTW(790658);
			}break;
		case AT_INT_ADD:
			{
				AbilName = TTW(790659);
			}break;
		case AT_CON_ADD:
			{
				AbilName = TTW(790660);
			}break;
		case AT_DEX_ADD:
			{
				AbilName = TTW(790661);
			}break;
		default:
			{
				MakeAbilNameString2(Type, AbilName);
			}break;
		}
		pWnd->Text(AbilName);
	}
	else
	{
		pWnd->Text(pText);
	}

	XUI::CXUI_Wnd* pAbliValue = pWnd->GetControl(_T("FRM_VALUE"));
	if( !pAbliValue )
	{
		return;
	}

	BM::vstring	vStr;

	switch(Type)
	{
	case AT_STR_ADD:
	case AT_INT_ADD:
	case AT_CON_ADD:
	case AT_DEX_ADD:
		{// 보너스 스테이터스(Active Status)
			vStr = lua_tinker::call<char*, int>("GetStrColorInCharInfoUI", 0);
		}break;
	default:
		{
			int const CalcPlusAbil = CalcPlusAbilValue(pUnit, Type);
			if( CalcPlusAbil != 0 )
			{
				if( CalcPlusAbil > 0 )
				{
					vStr = _T("{C=0xFF008E21/}");
				}
				else
				{
					vStr = _T("{C=0xFFFF0000/}");
				}
			}
		}break;
	}

	int iResultValue = 0;
	switch(Type)
	{// 유저 정보중
	case AT_C_HITRATE:
	case AT_C_DODGE_RATE:
		{
			iResultValue += static_cast<int>(pUnit->GetAbil(Type) * 0.01f);
		}break;
	case AT_C_PHY_ATTACK_MAX:
		{// 물리 공격력 정보를 보여줄때, %만큼 감소, 증가가 적용되어 있다면
			int const iC_Phy_Attack_Max = pUnit->GetAbil(Type);
			int iTotalPer = pUnit->GetAbil(AT_PHY_DMG_PER);
			if(0 == iTotalPer)
			{
				iTotalPer = ABILITY_RATE_VALUE;
			}
			iTotalPer +=  pUnit->GetAbil(AT_PHY_DMG_PER2);
			float fC_Phy_Dmg_Per = iTotalPer/ABILITY_RATE_VALUE_FLOAT;
			if(0 >= fC_Phy_Dmg_Per)
			{
				fC_Phy_Dmg_Per = 1.0f;
			}
			iResultValue += static_cast<int>(iC_Phy_Attack_Max*fC_Phy_Dmg_Per);
		}break;
	case AT_C_MAGIC_ATTACK_MAX:
		{// 마법 공격력 정보를 보여줄때, %만큼 감소, 증가가 적용되어 있다면
			int const iC_Mag_Attack_Max = pUnit->GetAbil(Type);
			int iTotalPer = pUnit->GetAbil(AT_MAGIC_DMG_PER);
			if(0 == iTotalPer)
			{
				iTotalPer = ABILITY_RATE_VALUE;
			}
			iTotalPer += pUnit->GetAbil(AT_MAGIC_DMG_PER2);
			float fC_Mag_Dmg_Per =  iTotalPer/ABILITY_RATE_VALUE_FLOAT;
			if(0 >= fC_Mag_Dmg_Per)
			{
				fC_Mag_Dmg_Per = 1.0f;
			}
			iResultValue += static_cast<int>(iC_Mag_Attack_Max*fC_Mag_Dmg_Per);
		}break;
	default:
		{
			iResultValue += pUnit->GetAbil(Type);
		}break;
	}

	vStr += iResultValue;
	pAbliValue->Text(vStr);
	pAbliValue->SetCustomData(&Type, sizeof(Type));
}

int lwCharInfo::CalcPlusAbilValue(CUnit* pkUnit, EAbilType Type)
{
	int const iBaseAbil = GetBasicAbil(Type);
	int const iBaseValue = pkUnit->GetAbil(iBaseAbil);
	int iCompleteValue = pkUnit->GetAbil(Type);
	switch(Type)
	{
	case AT_C_PHY_ATTACK_MAX:
		{// 물리 공격력 툴팁을 보여줄때, %만큼 감소, 증가가 적용되어 있다면
			int iTotalPer = pkUnit->GetAbil(AT_PHY_DMG_PER);
			if(0 == iTotalPer)
			{
				iTotalPer = ABILITY_RATE_VALUE;
			}
			iTotalPer += pkUnit->GetAbil(AT_PHY_DMG_PER2);
			float fC_Phy_Dmg_Per = iTotalPer/ABILITY_RATE_VALUE_FLOAT;
			if(0 >= fC_Phy_Dmg_Per)
			{
				fC_Phy_Dmg_Per = 1.0f;
			}
			iCompleteValue = static_cast<int>(iCompleteValue*fC_Phy_Dmg_Per);
		}break;
	case AT_C_MAGIC_ATTACK_MAX:
		{// 마법 공격력 툴팁을 보여줄때, %만큼 감소, 증가가 적용되어 있다면
			int iTotalPer = pkUnit->GetAbil(AT_MAGIC_DMG_PER);
			if(0 == iTotalPer)
			{
				iTotalPer = ABILITY_RATE_VALUE;
			}
			iTotalPer += pkUnit->GetAbil(AT_MAGIC_DMG_PER2);
			float fC_Mag_Dmg_Per = iTotalPer/ABILITY_RATE_VALUE_FLOAT;
			if(0 >= fC_Mag_Dmg_Per)
			{
				fC_Mag_Dmg_Per = 1.0f;
			}
			iCompleteValue = static_cast<int>(iCompleteValue*fC_Mag_Dmg_Per);
		}break;
	default:
		{
		}break;
	}

	int const iPlusValue = iCompleteValue - iBaseValue;
	return iPlusValue;
}

lwWString lwCharInfo::lwViewPlusAbilValue(lwUIWnd UISelf)
{//캐릭 스텟 
	XUI::CXUI_Wnd* pSelf = UISelf.GetSelf();
	if( !pSelf )
	{
		return lwWString("");
	}

	lwUIWnd UIParent = UISelf.GetParent().GetParent();
	BM::GUID	Guid = UIParent.GetOwnerGuid().GetGUID();

	EAbilType Type;
	pSelf->GetCustomData(&Type, sizeof(Type));

	EAbilType PlusType = (EAbilType)GetCalculateAbil(Type);
	PgPilot* pPilot = g_kPilotMan.FindPilot(Guid);
	if( !pPilot )
	{
		return lwWString("");
	}

	CUnit* pUnit = pPilot->GetUnit();
	if( !pUnit )
	{
		return lwWString("");
	}

	int iTTW = 3010;//캐릭터3010
	if(UT_PET==pUnit->UnitType())
	{
		iTTW = 1804;//펫
	}

	switch( Type )
	{
	case AT_STR:
	case AT_INT:
	case AT_CON:
	case AT_DEX://펫의 기본능력치는 툴팁 필요 없음.
	case AT_STR_ADD:
	case AT_INT_ADD:
	case AT_CON_ADD:
	case AT_DEX_ADD:
		{//보너스 스테이터스는 툴팁 필요 없음.
			return lwWString("");
		}break;
	case AT_HEALTH:
		{
			Type = AT_R_MP_RECOVERY;
		}break;
	case AT_MENTAL:
		{
			Type = AT_SKILL_EFFICIENCY;
		}break;
	}

	//타이틀
	BM::vstring vStr;
	const TCHAR* pAbilTypeName = NULL;
	if( ::GetAbilName(Type, pAbilTypeName) )
	{
		vStr += ::TTW(4208);//Font&Color
		vStr += pAbilTypeName;
		vStr += L" ";
		vStr += ::TTW(5049);//상세정보
		vStr += ::TTW(40011);//폰트 초기화
		vStr += L"\n";
		vStr += L"\n";
	}

	int const iBaseAbil = GetBasicAbil(Type);
	int iBaseValue = pUnit->GetAbil(iBaseAbil);
	switch (Type)
	{
	case AT_SKILL_EFFICIENCY:
	case AT_C_DODGE_RATE:
	case AT_C_HITRATE:
		{
			iBaseValue = static_cast<int>(pUnit->GetAbil(Type) * 0.01f);
		}break;
	case AT_R_MP_RECOVERY:
		{
			iBaseValue = pUnit->GetAbil(AT_HEALTH);
		}break;
	}

	vStr += ::TTW(40012);
	vStr += L"[";
	vStr += ::TTW(iTTW);//캐릭터3010
	vStr += L"]\n";

	//캐릭터 총합
	vStr += ::TTW(1301);
	vStr +=  ::TTW(iTTW);
	vStr += L" ";
	vStr += ::TTW(5050);
	vStr += L": ";
	vStr += iBaseValue;
	vStr += L"\n";
	vStr += ::TTW(40011);

	//캐릭터 기본
	vStr += ::TTW(5992);//기본
	if( pAbilTypeName )
	{
		vStr += L" ";
		vStr += pAbilTypeName;
	}
	vStr += L": ";

	switch( Type )
	{
	case AT_STR:
	case AT_INT:
	case AT_CON:
	case AT_DEX:
		{
			int const iBnsStatus = GetBonusStatus(pUnit, Type);
			vStr += (iBaseValue - iBnsStatus);//보너스 만큼 빼야 기본값임.
		}break;
	default:
		{
			vStr += iBaseValue;
		}break;
	}

	vStr += L"\n";

	//보너스 스테이터스(Active Status)
	switch( Type )
	{
	case AT_STR_ADD:
	case AT_INT_ADD:
	case AT_CON_ADD:
	case AT_DEX_ADD:
		{
			vStr += TTW(790680);//보너스
			if( pAbilTypeName )
			{
				vStr += L" ";
				vStr += pAbilTypeName;
			}
			vStr += L": ";
			int const iBnsStatus = GetBonusStatus(pUnit, Type);
			vStr += iBnsStatus;
			vStr += _T("{C=0xFFFF0000/}");
			vStr += L"\n";
		}break;
	}

	std::wstring wstrEnchantText = L"\n";
	wstrEnchantText += ::TTW(40012);
	wstrEnchantText += L"[";
	wstrEnchantText += ::TTW(3004);//아이템
	wstrEnchantText += L"]";
	wstrEnchantText += ::TTW(40011);//폰트 초기화	

	//인챈트된 수치
	int iItemTotalValue = 0;
	if( ::MakeToolTipText_Status(Type, wstrEnchantText, pUnit, &iItemTotalValue) )
	{
		vStr += wstrEnchantText;
	}
	
	if( AT_PHY_ATTACK_MAX == iBaseAbil
		|| AT_MAGIC_ATTACK_MAX == iBaseAbil
		)
	{// 증폭률 ( 기본은 10000이다 )
		vStr += L"\n";
		vStr += ::TTW(40012);
		vStr += L"[";
		vStr += ::TTW(790804);		//[증폭률]
		vStr += L"]\n";
		
		vStr += ::TTW(1301);	// 폰트 초기화
		
		// 총합
		vStr +=  ::TTW(790805);
		vStr += L": ";

		int const iDmg_Per_Abil = (AT_PHY_ATTACK_MAX == iBaseAbil) ? AT_PHY_DMG_PER : AT_MAGIC_DMG_PER;
		int const iDmg_Per_Abil2 = (AT_PHY_ATTACK_MAX == iBaseAbil) ? AT_PHY_DMG_PER2 : AT_MAGIC_DMG_PER2;
		int iDmg_Per_Abil_Vale = pUnit->GetAbil(iDmg_Per_Abil);
		int const iDmg_Per_Abil_Vale2 = pUnit->GetAbil(iDmg_Per_Abil2);
		if( 0 == iDmg_Per_Abil_Vale  ) 
		{
			iDmg_Per_Abil_Vale  = ABILITY_RATE_VALUE;
		}
		iDmg_Per_Abil_Vale += iDmg_Per_Abil_Vale2;
		int const iRealIncreaseRate = iDmg_Per_Abil_Vale  - ABILITY_RATE_VALUE;
		float const fPer = MAKE_ABIL_RATE(iRealIncreaseRate / ABILITY_RATE_VALUE_FLOAT);
		vStr += L"#PER#%";
		vStr.Replace( L"#PER#", BM::vstring( fPer, L"%.1f") );

		vStr += L"(";
		vStr += ::TTW(790807);
		vStr += L" ";
		int const iSum = (iBaseValue +iItemTotalValue); // 캐릭터 총합 + 아이템 총합 공격력
		int const iApproximatelyValue = ( iSum * iRealIncreaseRate ) / ABILITY_RATE_VALUE;
		vStr += iApproximatelyValue;
		vStr += L")";
		vStr += L"\n";
		vStr += ::TTW(40011);
	}
	

	//스킬에 의한 어쩌고 
	vStr += L"\n";
	vStr += ::TTW(40012);
	vStr += ::TTW(5070);

	return lwWString((std::wstring const&)vStr);
}

void lwCharInfo::lwCallInfoDrop(lwUIWnd UIWnd, char const* szAddon, char const* szUIName)
{
	XUI::CXUI_Wnd* pDrop = XUIMgr.Get(_T("SFRM_CHARINFO_POPUP"));
	if( !pDrop || pDrop->IsClosed() )
	{
		pDrop = XUIMgr.Call(_T("SFRM_CHARINFO_POPUP"));
		if( !pDrop )
		{
			return;
		}
	}
	else
	{
		pDrop->Close();
	}

	XUI::CXUI_Wnd* pParent = UIWnd.GetSelf();
	if( !pParent )
	{
		return;
	}
	std::wstring wstrAddon = UNI(szAddon);

	std::wstring wstrName(_T("FRM_INFO_TYPE_"));
	wstrName += wstrAddon;
	XUI::CXUI_Wnd* pLabel = pParent->GetControl(wstrName);
	if( !pLabel )
	{
		return;
	}

	pDrop->SetCustomData(wstrAddon.c_str(), sizeof(std::wstring::value_type)*wstrAddon.size());
	POINT2	Pt(pLabel->TotalLocation().x, pLabel->TotalLocation().y + pLabel->Size().y);
	pDrop->Location(Pt);

	XUI::CXUI_Wnd* pTemp = pDrop->GetControl(_T("FRM_Option_BG"));
	if( pTemp )
	{
		std::wstring wstrFormName(UNI(szUIName));
		if( wstrFormName.size() )
		{
			pTemp->SetCustomData(wstrFormName.c_str(), sizeof(std::wstring::value_type)*wstrFormName.size());
		}
	}
}

bool lwCharInfo::lwViewEquipIconInitialize(lwUIWnd UIWnd, int const iInvType)
{
	XUI::CXUI_Wnd* pParent = UIWnd.GetSelf();
	if( !pParent )
	{
		return false;
	}

	BM::GUID	Guid = UIWnd.GetOwnerGuid().GetGUID();
	bool const bIsMyActor = g_kPilotMan.IsMyPlayer(Guid);

	PgPilot* pPilot = g_kPilotMan.FindPilot(Guid);
	if( !pPilot )
	{
		lwAddWarnDataTT(6006);
		return false;
	}

	CUnit* pUnit = pPilot->GetUnit();
	if( !pUnit )
	{
		return false;
	}

	PgInventory* pInv = NULL;
	if( bIsMyActor )
	{
		pInv = pUnit->GetInven();
	}
	else
	{
		pInv = &g_kOtherViewInv;
	}

	if( pInv )
	{
		if ( true == lwViewEquipIconInitialize2( pParent, pInv, iInvType ) )
		{
			if( iInvType != 0 )
			{
				pParent->SetCustomData( &iInvType, sizeof(iInvType) );
			}
			return true;
		}
	}

	return false;
}

bool lwCharInfo::lwViewEquipIconInitialize2( XUI::CXUI_Wnd* pkUI, PgInventory *pkInv, int const iInvType )
{
	if ( KUIG_NONE == iInvType )
	{
		int iValue = KUIG_NONE;
		pkUI->GetCustomData( &iValue, sizeof(iValue) );

		if ( KUIG_NONE != iValue )
		{
			return lwViewEquipIconInitialize2( pkUI, pkInv, iValue );
		}
		return false;
	}

	int INV_POS[16] = { EQUIP_POS_GLASS, EQUIP_POS_BELT, EQUIP_POS_NECKLACE, EQUIP_POS_PANTS,
		EQUIP_POS_HELMET, EQUIP_POS_BOOTS, EQUIP_POS_SHOULDER, EQUIP_POS_RING_L, EQUIP_POS_CLOAK,
		EQUIP_POS_RING_R, EQUIP_POS_SHIRTS, EQUIP_POS_EARRING, EQUIP_POS_GLOVE, EQUIP_POS_MEDAL,
		EQUIP_POS_WEAPON, EQUIP_POS_SHEILD};

	XUI::CXUI_CheckButton* pButton = dynamic_cast<XUI::CXUI_CheckButton*>(pkUI->GetControl(_T("BTN_DEFAULT_VIEW")));
	XUI::CXUI_CheckButton* pButton2 = dynamic_cast<XUI::CXUI_CheckButton*>(pkUI->GetControl(_T("BTN_CASH_VIEW")));
	if( pButton && pButton2 )
	{
		switch(iInvType)
		{
		case KUIG_VIEW_OTHER_EQUIP:
		case KUIG_FIT:		
			{ 
				pButton->Check(true);
				pButton->ClickLock(true);
				INV_POS[0] = EQUIP_POS_PET;
			} break;
		case KUIG_VIEW_OTHER_EQUIP_CASH:
		case KUIG_FIT_CASH:	
			{ 
				pButton->ClickLock(false);
				pButton->Check(false);
			} break;
		default:
			{
			}break;
		}
		pButton2->ClickLock(!pButton->ClickLock());
		pButton2->Check(!pButton->Check());
	}

	for( int i = 0; i < 16; ++i )
	{
		BM::vstring	vStr(_T("FRM_EQUIP_ICON"));
		vStr += i;
		XUI::CXUI_Icon* pIcon = dynamic_cast<XUI::CXUI_Icon*>(pkUI->GetControl(vStr));
		if( !pIcon )
		{
			continue;
		}
		SIconInfo Info = pIcon->IconInfo();
		Info.iIconGroup = iInvType;
		Info.iIconKey = INV_POS[i];
		pIcon->SetIconInfo(Info);

		XUI::CXUI_Wnd* pBgMain = pkUI->GetControl(_T("FRM_BG"));
		if( pBgMain )
		{
			BM::vstring vStr(_T("FRM_ICON_OUTLINE"));
			vStr += i;
			XUI::CXUI_Wnd* pTemp = pBgMain->GetControl(vStr);
			if( !pTemp )
			{
				continue;
			}

			vStr = _T("FRM_ICON_NAME");
			vStr += i;
			XUI::CXUI_Wnd* pTemp2 = pBgMain->GetControl(vStr);
			if( !pTemp2 )
			{
				continue;
			}
			
			EInvType InvType = static_cast<EInvType>(Info.iIconGroup);
			if ( InvType > KUIG_INV_VIEW )
			{
				InvType = ((Info.iIconGroup == KUIG_VIEW_OTHER_EQUIP)?(IT_FIT):(IT_FIT_CASH));
			}

			PgBase_Item kItem;
			if( pkInv->GetItem( InvType, Info.iIconKey, kItem) != S_OK )
			{
				pTemp->Visible(false);
				pTemp2->Visible(true);
				continue;
			}

			pTemp2->Visible(false);
			E_ITEM_GRADE const eGrade = GetItemGrade(kItem);
			if( eGrade >= 0 )
			{
				pTemp->Visible(true);
				SUVInfo UVInfo = pTemp->UVInfo();
				UVInfo.Index = (eGrade + 1);
				pTemp->UVInfo(UVInfo);
			}
			else
			{
				pTemp->Visible(false);
			}
		}
	}
	return true;
}

void lwCharInfo::UpdateMyActor()
{
	BM::GUID kGuid;
	g_kPilotMan.GetPlayerPilotGuid(kGuid);
	if( g_kMyActorViewMgr.ChangeEquip("CHARINFO", kGuid) )
	{
		XUI::CXUI_Wnd* pWnd = XUIMgr.Get(_T("CharInfo"));
		if( !pWnd )
		{
			pWnd = XUIMgr.Get(_T("SFRM_OTHER_CharInfo"));
			if( !pWnd )
			{
				return;
			}
		}
		BM::GUID	kGuid;
		pWnd->GetCustomData(&kGuid, sizeof(kGuid));
		SetCharInfoToUI(pWnd, kGuid, false);
	}
}

void lwCharInfo::lwChangeInfoTab(lwUIWnd UISelf, int const iTab)
{	
	XUI::CXUI_Wnd* pSelf = UISelf.GetSelf();
	if( !pSelf )
	{
		return;
	}

	XUI::CXUI_Wnd* pParent = pSelf->Parent();
	if( !pParent )
	{
		return;
	}

	BYTE const MAX_TAB_COUNT = 3;
	XUI::CXUI_Wnd* pTab[MAX_TAB_COUNT] = {0,};
	pTab[0] = pParent->GetControl(L"FRM_CHAR_INFO");
	pTab[1] = pParent->GetControl(L"FRM_PET_INFO");
	pTab[2] = pParent->GetControl(L"FRM_CHAR_CARD");

	for( int i = 0; i < MAX_TAB_COUNT; ++i )
	{
		BM::vstring	vStr(L"CBTN_TAB_");
		vStr += i;

		XUI::CXUI_CheckButton* pBtn = dynamic_cast<XUI::CXUI_CheckButton*>(pParent->GetControl(vStr));
		bool bThisTab = i==iTab;
		if( pBtn )
		{
			pBtn->ClickLock(bThisTab);
			pBtn->Check(bThisTab);
			pTab[i]->Visible(bThisTab);
		}
	}
}

void lwCharInfo::lwChangeOtherInfoTab(lwUIWnd UISelf, int const iTab)
{
	XUI::CXUI_Wnd* pSelf = UISelf.GetSelf();
	if( !pSelf )
	{
		return;
	}

	XUI::CXUI_Wnd* pParent = (pSelf->Parent())?(pSelf->Parent()->Parent()):(NULL);
	if( !pParent )
	{
		return;
	}

	BYTE const MAX_TAB_COUNT = 2;
	XUI::CXUI_Wnd* pTab[MAX_TAB_COUNT] = {0,};
	pTab[0] = pParent->GetControl(L"FRM_CHAR_INFO");
	pTab[1] = pParent->GetControl(L"FRM_CHAR_CARD");

	for( int i = 0; i < MAX_TAB_COUNT; ++i )
	{
		BM::vstring	vStr(L"SFRM_TAB");
		vStr += i;

		XUI::CXUI_Wnd* pkTab = pParent->GetControl(vStr);
		if( pkTab )
		{
			XUI::CXUI_CheckButton* pBtn = dynamic_cast<XUI::CXUI_CheckButton*>(pkTab->GetControl(L"CBTN_TAB"));
			bool bThisTab = i==iTab;
			if( pBtn )
			{
				pBtn->ClickLock(bThisTab);
				pBtn->Check(bThisTab);
				pTab[i]->Visible(bThisTab);
			}
		}
	}
}

void lwCharInfo::lwSetPetInfoToUI(lwUIWnd UIWnd, lwGUID kGuid)
{
	SetPetInfoToUI(UIWnd.GetSelf(), kGuid(), true);
}

void lwCharInfo::ChangePetActor(BM::GUID const kGuid)
{
	g_kMyActorViewMgr.DeleteActor("PetActor");
	g_kMyActorViewMgr.UpdatePet("PetActor", kGuid);
	PgPetUIUtil::lwDrawTextToPetState();
}

void lwCharInfo::SetPetInfoToUI(XUI::CXUI_Wnd* pWnd, BM::GUID const& rkGuid, bool const bIsChanged)
{
	if( !pWnd )
	{
		return;
	}
	pWnd->SetCustomData(&rkGuid, sizeof(rkGuid));

	PgPetUIUtil::SetPetUIDefaultState(false);
	PgPilot* pPilot = g_kPilotMan.FindPilot(rkGuid);
	if( !pPilot )
	{
		if( false == SetDiePetInfoToUI(pWnd, rkGuid) )
		{
			PgPetUIUtil::SetPetUIDefaultState(true);
		}
		else
		{
			PgPlayer* pkPlayer = g_kPilotMan.GetPlayerUnit();
			if(pkPlayer)
			{
				PgInventory* pkInv = pkPlayer->GetInven();
				if( pkInv )
				{
					PgBase_Item kItem;
					if(S_OK==pkInv->GetItem(PgItem_PetInfo::ms_kPetItemEquipPos, kItem) )	//유닛은 없는데 아이템은 있으면
					{
						XUI::CXUI_Wnd* pkCover = PgPetUIUtil::SetPetUIDefaultState(kItem);	//아이템이 시간만 다 된거라면
						if(pkCover)
						{
							pkCover->Text(TTW(149));
						}
					}
				}
			}
		}
		PgPetUIUtil::InitPetInfoUI(pWnd);
		return;
	}

	PgPet* pkPet = dynamic_cast<PgPet*>(pPilot->GetUnit());
	if( !pkPet )
	{
		return;
	}

	//////////////////////////////////////////////////////////////////////
	//1차펫 2차펫에 따라 달라저야 할 UI
	bool const b1stType = (EPET_TYPE_1 == static_cast<EPetType>(pkPet->GetPetType()));	//1차펫이냐 2차 펫이냐

	XUI::CXUI_Wnd* pkBalloon = pWnd->GetControl(L"IMG_BALLOON");
	if( pkBalloon )
	{
		pkBalloon->Visible(false);
	}

	//XUI::CXUI_Wnd* pkBtn = pWnd->GetControl(L"BTN_EXPEND_USED");	//기간연장 버튼 숨김
	//if(pkBtn)
	//{
	//	pkBtn->Visible(b1stType);
	//}

	XUI::CXUI_Wnd* pkBtn = pWnd->GetControl(L"BTN_COLOR_CHANGE");	//컬러변경 버튼 숨김
	if(pkBtn)
	{
		pkBtn->Visible(!b1stType);
	}

	/*XUI::CXUI_Wnd* pkTab = pWnd->GetControl(L"FRM_USE_TITLE");	//사용시간
	if(pkTab)
	{
		pkTab->Text(TTW(b1stType ? 120 : 7508));
	}*/

	XUI::CXUI_Wnd* pkBuildTarget = NULL;	//1차펫용 아이템, 펫 동작버튼 중 선택
	/*for(int i=0; i<4; ++i)
	{
		BM::vstring kName1(L"IMG_ITEM_ICON_BG");
		BM::vstring kName2(L"BTN_PET_TRAIN");
		kName1+=i; kName2+=i;
		pkBuildTarget = pWnd->GetControl((std::wstring const&)kName1);
		if(pkBuildTarget)
		{
			pkBuildTarget->Visible(b1stType);
		}

		pkBuildTarget = pWnd->GetControl((std::wstring const&)kName2);
		if(pkBuildTarget)
		{
			pkBuildTarget->Visible(!b1stType);
		}
	}*/

	pkBuildTarget = pWnd->GetControl(L"FRM_TIRED_TEXT");	//피로도 대신 마나
	if(pkBuildTarget)
	{
		pkBuildTarget->Text(b1stType ? L"" : TTW(7509));
	}

	XUI::CXUI_AniBar* pkBar = dynamic_cast<XUI::CXUI_AniBar*>(pWnd->GetControl(L"BAR_TIRED"));	//피로도 대신 마나
	if(pkBar)
	{
		pkBar->Max(b1stType ? 100 : pkPet->GetAbil(AT_C_MAX_MP));
		pkBar->Now(b1stType ? 100 : pkPet->GetAbil(AT_MP));
	}

	/*pkBar = dynamic_cast<XUI::CXUI_AniBar*>(pWnd->GetControl(L"BAR_STATE"));	//피로도 대신 마나
	if(pkBar)
	{
		pkBar->Visible(!b1stType);
		if(!b1stType)
		{
			pkBar->Max(PgItem_PetInfo::MAX_PET_STATE_VALUE);
			pkBar->Now(PgItem_PetInfo::MAX_PET_STATE_VALUE);
//			pkBar->Now(PgPetUIUtil::CalcPetStateAvg(pkPet));
		}
	}*/

	XUI::CXUI_CheckButton *pkChkBtn = dynamic_cast<XUI::CXUI_CheckButton*>(pWnd->GetControl(L"CBTN_SKILL"));
	if(pkChkBtn)
	{
		pkChkBtn->Visible(!b1stType);
		if(false==pkChkBtn->Check())
		{
			lua_tinker::call<void, lwUIWnd, int>("UI_PetInfoTab", lwUIWnd(pWnd->GetControl(L"CBTN_SKILL")), 0);	//항상 정보창으로 초기화
		}
	}

	//
	//////////////////////////////////////////////////////////////////////

	XUI::CXUI_Wnd* pkNameUI = pWnd->GetControl(L"SFRM_NAME_SHADOW");
	if(pkNameUI)
	{
		pkNameUI->Text(pkPet->Name());
	}

	XUI::CXUI_Wnd* pkUseUI = pWnd->GetControl(L"SFRM_USE_SHADOW");
	if(pkUseUI)
	{
		std::wstring wstrText;
		wstrText = TTW(179);
//		if(b1stType)
//		{
		PgPlayer* pkPlayer = g_kPilotMan.GetPlayerUnit();
		if(pkPlayer)
		{
			PgInventory *pInv = pkPlayer->GetInven();
			if(pInv)
			{
				SItemPos const kPos = PgItem_PetInfo::ms_kPetItemEquipPos;
				PgBase_Item kBaseItem;
				if(S_OK==pInv->GetItem( kPos, kBaseItem))
				{
					__int64 i64AbilTime = kBaseItem.GetUseAbleTime();//초단위
					if(0i64==i64AbilTime)	//무기한 이거나 기간 만료
					{
						if(0i64<kBaseItem.EnchantInfo().IsTimeLimit())	//기간이 있는놈이면
						{
							wstrText = TTW(179);
						}
						else	//없는 놈
						{
							wstrText = TTW(178);
						}
					}
					else
					{
						wchar_t	szValue[40] = {0,};
						int const iRemainHour = static_cast<int>( i64AbilTime / 3600i64 );
						WstringFormat( wstrText, MAX_PATH, TTW(122).c_str(), iRemainHour / 24, iRemainHour % 24 );
					}					
				}
			}
		}
/*		}
		else
		{
			PgPlayer* pkPlayer = g_kPilotMan.GetPlayerUnit();
			if(pkPlayer && pkPlayer->GetInven())
			{
				PgBase_Item kOut;
				if(S_OK==pkPlayer->GetInven()->GetItem(PgItem_PetInfo::ms_kPetItemEquipPos, kOut) && !kOut.IsEmpty())
				{
					PgItem_PetInfo *pkPetInfo = NULL;
					if(kOut.GetExtInfo(pkPetInfo))
					{
						if(pkPetInfo->IsDead())
						{
							wstrText = TTW(329);
						}
					}
				}
			}
			if(wstrText.empty())
			{
				WstringFormat( wstrText, MAX_PATH, TTW(405001).c_str(), pkPet->GetConditionGrade()+1);
			}
		}*/

		pkUseUI->Text(wstrText);
	}

	int const iLev = pkPet->GetAbil(AT_LEVEL);

	XUI::CXUI_Wnd* pkLvUI = pWnd->GetControl(L"FRM_LV_TEXT");
	if(pkLvUI)
	{
		wchar_t	szValue[30] = {0,};
		swprintf(szValue, 29, TTW(5013).c_str(), iLev);
		pkLvUI->Text(szValue);
	}

	XUI::CXUI_Wnd* pkTypeUI = pWnd->GetControl(L"SFRM_TYPE_SHADOW");
	if(pkTypeUI)
	{
		int const iClass = pkPet->GetAbil(AT_CLASS);

		GET_DEF(PgClassPetDefMgr, kClassDefMgr);
		PgClassPetDef kPetDef;
		if ( true == kClassDefMgr.GetDef( SClassKey(iClass, iLev), &kPetDef ) )
		{
			int const iNameNo = kPetDef.GetAbil( AT_NAMENO );
			wchar_t const *pName = NULL;
			if( GetDefString(iNameNo, pName) )
			{
				pkTypeUI->Text(pName);
			}
			else
			{
				pkTypeUI->Text(L"");
			}
		}
	}

	int iSet = lwConfig_GetValue("CHARINFO", "PETINFO_LEFT");
	if (0 == iSet)
	{
		iSet = 4;
	}
	lwSetCharAbilInfo(lwUIWnd(pWnd->GetControl(L"FRM_INFO")), iSet, "L", true);

	PgPetUIUtil::lwUI_RefreshPetSkillUI(pWnd);
}

bool lwCharInfo::SetDiePetInfoToUI(XUI::CXUI_Wnd* pWnd, BM::GUID const& rkGuid)
{
	if( !pWnd ){ return false; }
	pWnd->SetCustomData(&rkGuid, sizeof(rkGuid));

	PgPlayer* pkPlayer = g_kPilotMan.GetPlayerUnit();
	if( !pkPlayer ){ return false; }

	PgInventory* pkInv = pkPlayer->GetInven();
	if( !pkInv ){ return false; }

	PgBase_Item kItem;
	if( S_OK != pkInv->GetItem(PgItem_PetInfo::ms_kPetItemEquipPos, kItem) )
	{
		return false;
	}

	PgItem_PetInfo* pkPetInfo = NULL;
	if( !kItem.GetExtInfo(pkPetInfo) )
	{
		return false;
	}

	//////////////////////////////////////////////////////////////////////
	//1차펫 2차펫에 따라 달라저야 할 UI
	GET_DEF(PgClassPetDefMgr, kClassPetDefMgr);
	PgClassPetDef kPetDef;
	if ( !kClassPetDefMgr.GetDef( pkPetInfo->ClassKey(), &kPetDef ) )
	{
		return false;
	}

	bool const b1stType = static_cast<EPetType>(EPET_TYPE_1 == kPetDef.GetPetType());	//1차펫이냐 2차 펫이냐

	XUI::CXUI_Wnd* pkBalloon = pWnd->GetControl(L"IMG_BALLOON");
	if( pkBalloon )
	{
		pkBalloon->Visible(false);
	}

	//XUI::CXUI_Wnd* pkBtn = pWnd->GetControl(L"BTN_EXPEND_USED");	//기간연장 버튼 숨김
	//if(pkBtn)
	//{
	//	pkBtn->Visible(b1stType);
	//}

	XUI::CXUI_Wnd* pkBtn = pWnd->GetControl(L"BTN_COLOR_CHANGE");	//컬러변경 버튼 숨김
	if(pkBtn)
	{
		pkBtn->Visible(!b1stType);
	}

	/*XUI::CXUI_Wnd* pkTab = pWnd->GetControl(L"FRM_USE_TITLE");	//사용시간
	if(pkTab)
	{
		pkTab->Text(TTW(b1stType ? 120 : 7508));
	}*/

	XUI::CXUI_Wnd* pkBuildTarget = NULL;	//1차펫용 아이템, 펫 동작버튼 중 선택
	/*for(int i=0; i<4; ++i)
	{
		BM::vstring kName1(L"IMG_ITEM_ICON_BG");
		BM::vstring kName2(L"BTN_PET_TRAIN");
		kName1+=i; kName2+=i;
		pkBuildTarget = pWnd->GetControl((std::wstring const&)kName1);
		if(pkBuildTarget)
		{
			pkBuildTarget->Visible(b1stType);
		}

		pkBuildTarget = pWnd->GetControl((std::wstring const&)kName2);
		if(pkBuildTarget)
		{
			pkBuildTarget->Visible(!b1stType);
		}
	}*/

	pkBuildTarget = pWnd->GetControl(L"FRM_TIRED_TEXT");	//피로도 대신 마나
	if(pkBuildTarget)
	{
		pkBuildTarget->Text(b1stType ? L"" : TTW(7509));
	}

	XUI::CXUI_AniBar* pkBar = dynamic_cast<XUI::CXUI_AniBar*>(pWnd->GetControl(L"BAR_TIRED"));	//피로도 대신 마나
	if(pkBar)
	{
		pkBar->Max(b1stType ? 100 : kPetDef.GetAbil(AT_C_MAX_MP));
		pkBar->Now(b1stType ? 100 : pkPetInfo->GetAbil(AT_MP));
	}

	/*pkBar = dynamic_cast<XUI::CXUI_AniBar*>(pWnd->GetControl(L"BAR_STATE"));	//피로도 대신 마나
	if(pkBar)
	{
		pkBar->Visible(!b1stType);
		if(!b1stType)
		{
			pkBar->Max(PgItem_PetInfo::MAX_PET_STATE_VALUE);
			pkBar->Now(0);
		}
	}*/
	//
	//////////////////////////////////////////////////////////////////////

	XUI::CXUI_Wnd* pkNameUI = pWnd->GetControl(L"SFRM_NAME_SHADOW");
	if(pkNameUI)
	{
		pkNameUI->Text(pkPetInfo->Name());
	}

	XUI::CXUI_Wnd* pkUseUI = pWnd->GetControl(L"SFRM_USE_SHADOW");
	if(pkUseUI)
	{
		std::wstring wstrText;
		wstrText = TTW(179);
		__int64 i64AbilTime = kItem.GetUseAbleTime();//초단위
		if(0i64>=i64AbilTime)	//무기한 이거나 기간 만료
		{
			if(0i64<kItem.EnchantInfo().IsTimeLimit())	//기간이 있는놈이면
			{
				wstrText = TTW(179);
			}
			else	//없는 놈
			{
				wstrText = TTW(178);
			}
		}
		else
		{
			wchar_t	szValue[40] = {0,};
			int const iRemainHour = static_cast<int>( i64AbilTime / 3600i64 );
			WstringFormat( wstrText, MAX_PATH, TTW(122).c_str(), iRemainHour / 24, iRemainHour % 24 );
		}		

		pkUseUI->Text(wstrText);
	}

	int const iLev = pkPetInfo->ClassKey().nLv;

	XUI::CXUI_Wnd* pkLvUI = pWnd->GetControl(L"FRM_LV_TEXT");
	if(pkLvUI)
	{
		wchar_t	szValue[30] = {0,};
		swprintf(szValue, 29, TTW(5013).c_str(), iLev);
		pkLvUI->Text(szValue);
	}

	XUI::CXUI_Wnd* pkTypeUI = pWnd->GetControl(L"SFRM_TYPE_SHADOW");
	if(pkTypeUI)
	{
		int const iNameNo = kPetDef.GetAbil( AT_NAMENO );
		wchar_t const *pName = NULL;
		if( GetDefString(iNameNo, pName) )
		{
			pkTypeUI->Text(pName);
		}
		else
		{
			pkTypeUI->Text(L"");
		}
	}

	PgPetUIUtil::lwUI_RefreshPetSkillUI(pWnd);
	PgPetUIUtil::lwDrawTextToPetState();
	return true;
}

int lwCharInfo::GetBonusStatus( CUnit* pkUnit, EAbilType Type )
{
	switch(Type)
	{
	case AT_C_STR:
		{
			return pkUnit->GetAbil(AT_STR_ADD);
		}break;
	case AT_C_INT:
		{
			return pkUnit->GetAbil(AT_INT_ADD);
		}break;
	case AT_C_CON:
		{
			return pkUnit->GetAbil(AT_CON_ADD);
		}break;
	case AT_C_DEX:
		{
			return pkUnit->GetAbil(AT_DEX_ADD);
		}break;
	}

	return 0;
}