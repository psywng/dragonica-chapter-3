#ifndef DRAGONICA_MOBILESUIT_lwCommonSkillUtilFunc_H
#define DRAGONICA_MOBILESUIT_lwCommonSkillUtilFunc_H

class lwActor;
class lwAction;
class lwGUID;
class lwPoint3;
class lwActionTargetList;

namespace lwCommonSkillUtilFunc
{
	extern void RegisterWrapper(lua_State *pkState);
	extern int GetUnitType(lwActor kActor);

	extern bool TryMustChangeSubPlayerAction(lwActor kOwnerActor, char const* pcActionName, BYTE byDir);		// SubPlayer에 모조건 변하는 액션을 시도한다
	extern bool TryMustChangeActorAction(lwActor kActor, char const* pcActionName);
	
};
#endif