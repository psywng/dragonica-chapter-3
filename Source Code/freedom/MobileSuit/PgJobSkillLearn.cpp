#include "stdafx.h"
#include "Variant/Global.h"
#include "PgJobSkillLearn.h"

namespace JobSkillGuide {
	// local function
	bool GetGuideItemName(int const iSaveIdx, std::wstring& rkOut) {
		CONT_DEF_JOBSKILL_SAVEIDX const* pkDefJobSkillSaveIdx = NULL;
		g_kTblDataMgr.GetContDef(pkDefJobSkillSaveIdx);
		CONT_DEF_JOBSKILL_SAVEIDX::const_iterator iter = pkDefJobSkillSaveIdx->find(iSaveIdx);
		if( pkDefJobSkillSaveIdx->end() != iter )
		{
			if( ::GetItemName((*iter).second.iBookItemNo, rkOut) )
			{
				return true;
			}
		}
		return true;
	}
}