#include "stdafx.h"
#include "lwSkillTree.h"
#include "PgSkillTree.h"
#include "lwWString.h"
#include "ServerLib.h"
#include "PgPilot.h"
#include "PgPilotMan.h"
#include "Variant/PgPlayer.h"
#include "Variant/PgControlUnit.h"
#include "lwUnit.h"
#include "lwUI.h"
#include "PgNetwork.h"

lwSkillTree	GetSkillTree()
{
	return	lwSkillTree();
}

bool IsMyPlayerLearnedAnySpecialSkill()
{// 플레이어가
	PgPlayer* pkPlayer = g_kPilotMan.GetPlayerUnit();
	if(!pkPlayer)
	{
		return false;
	}
	PgMySkill* pMySkill = pkPlayer->GetMySkill();
	if(!pMySkill)
	{
		return false;
	}
	//스페셜 스킬을 소유하고 있는가?
	ContSkillNo kContSkillNo;
	pMySkill->GetHaveSkills(SDT_Special, kContSkillNo);	
	return !kContSkillNo.empty();
}

extern bool lwGetHaveSkill(lwUnit kUnit, int iSkillNo, bool const bOverSkill = false)
{
	if(kUnit.IsNil())
	{
		return false;
	}

	PgControlUnit* pkConUnit = dynamic_cast<PgControlUnit*>(kUnit());
	if(!pkConUnit)
	{
		return false;
	}

	PgMySkill* pkMySkill = pkConUnit->GetMySkill();
	if(!pkMySkill)
	{
		return false;
	}

	return pkMySkill->IsExist(iSkillNo, bOverSkill);
}

void lwSwapStrategySkill()
{
	PgPlayer* pkPlayer = g_kPilotMan.GetPlayerUnit();
	if( !pkPlayer ){ return; }

	ESkillTabType eTabType = ESTT_BASIC;
	if( pkPlayer->GetSwapStrategySkillTabNo(eTabType) )
	{
		BM::CPacket kPacket(PT_C_M_REQ_CLIENT_CUSTOMDATA);
		kPacket.Push(ECCDT_SKILLTABNO);
		kPacket.Push(static_cast<int>(eTabType));
		NETWORK_SEND(kPacket);
	}
}

bool lwIsOpenStrategySkillSecond()
{
	PgPlayer* pkPlayer = g_kPilotMan.GetPlayerUnit();
	if( !pkPlayer ){ return false; }

	return pkPlayer->IsOpenStrategySkill(ESTT_SECOND);
}

int lwGetStrategySkillTabNo()
{
	PgPlayer* pkPlayer = g_kPilotMan.GetPlayerUnit();
	if( !pkPlayer ){ return 0; }

	return pkPlayer->GetAbil(AT_STRATEGYSKILL_TABNO);
}

void lwOnClickStrategySkill(int const iType)
{
	PgPlayer* pkPlayer = g_kPilotMan.GetPlayerUnit();
	if( !pkPlayer ){ return; }

	if( iType==pkPlayer->GetAbil(AT_STRATEGYSKILL_TABNO) ){ return; }

	ESkillTabType const eTabType = static_cast<ESkillTabType>(iType);
	if( pkPlayer->IsOpenStrategySkill(eTabType) )
	{
		BM::CPacket kPacket(PT_C_M_REQ_CLIENT_CUSTOMDATA);
		kPacket.Push(ECCDT_SKILLTABNO);
		kPacket.Push(static_cast<int>(eTabType));
		NETWORK_SEND(kPacket);
	}
}

bool lwSkillTree::RegisterWrapper(lua_State *pkState)
{
	using namespace lua_tinker;

	def(pkState, "GetSkillTree", &GetSkillTree);
	def(pkState, "IsMyPlayerLearnedAnySpecialSkill", &IsMyPlayerLearnedAnySpecialSkill);
	def(pkState, "GetHaveSkill", &lwGetHaveSkill);

	def(pkState, "IsOpenStrategySkillSecond", &lwIsOpenStrategySkillSecond);
	def(pkState, "GetStrategySkillTabNo", &lwGetStrategySkillTabNo);
	def(pkState, "SwapStrategySkill", &lwSwapStrategySkill);
	def(pkState, "OnClickStrategySkill", &lwOnClickStrategySkill);

	class_<lwSkillTree>(pkState, "SkillTree")
		.def(pkState, constructor<void>())
		.def(pkState, "GetSkillLevel", &lwSkillTree::GetSkillLevel)
		.def(pkState, "GetOverSkillLevel", &lwSkillTree::GetOverSkillLevel)		
		.def(pkState, "GetMaxSkillLevel", &lwSkillTree::GetMaxSkillLevel)
		.def(pkState, "GetSkillName", &lwSkillTree::GetSkillName)
		.def(pkState, "GetSkillIconKey", &lwSkillTree::GetSkillIconKey)
		.def(pkState, "IsActiveSkill", &lwSkillTree::IsActiveSkill)
		.def(pkState, "IsLearnedSkill", &lwSkillTree::IsLearnedSkill)
		.def(pkState, "IsCommandSkill", &lwSkillTree::IsCommandSkill)
		.def(pkState, "GetRemainSkillPoint", &lwSkillTree::GetRemainSkillPoint)
		.def(pkState, "GetUsedSkillPoint", &lwSkillTree::GetUsedSkillPoint)
		.def(pkState, "CanLevelUp", &lwSkillTree::CanLevelUp)
		.def(pkState, "CanLevelDown", &lwSkillTree::CanLevelDown)
		.def(pkState, "GetSkillNo", &lwSkillTree::GetSkillNo)
		.def(pkState, "GetKeySkillNo", &lwSkillTree::GetKeySkillNo)
		.def(pkState, "NewSkillLearned", &lwSkillTree::NewSkillLearned)
		.def(pkState, "DeleteSkill", &lwSkillTree::DeleteSkill)
		.def(pkState, "GetNextLevelSkillNo", &lwSkillTree::GetNextLevelSkillNo)
		.def(pkState, "GetTotalSkill", &lwSkillTree::GetTotalSkill)
		.def(pkState, "GetKeySkillNoByIndex", &lwSkillTree::GetKeySkillNoByIndex)
		.def(pkState, "GetKeySkillResNoByIndex", &lwSkillTree::GetKeySkillResNoByIndex)
		.def(pkState, "GetKeySkillResNoBySkillNo", &lwSkillTree::GetKeySkillResNoBySkillNo)
		.def(pkState, "GetNeedSkillPoint", &lwSkillTree::GetNeedSkillPoint)
		.def(pkState, "LevelUpTemporary", &lwSkillTree::LevelUpTemporary)
		.def(pkState, "LevelDownTemporary", &lwSkillTree::LevelDownTemporary)
		.def(pkState, "ResetTemporary", &lwSkillTree::ResetTemporary)
		.def(pkState, "ConfirmTemporary", &lwSkillTree::ConfirmTemporary)
		.def(pkState, "IsTemporaryLevelChanged", &lwSkillTree::IsTemporaryLevelChanged)
		.def(pkState, "IsTemporaryRemainSkillPoint", &lwSkillTree::IsTemporaryRemainSkillPoint)
		.def(pkState, "SetRemainSkillPoint", &lwSkillTree::SetRemainSkillPoint)
		.def(pkState, "GetCanLearn", &lwSkillTree::GetCanLearn)
		.def(pkState, "CheckAllNeedSkill", &lwSkillTree::CheckAllNeedSkill)
		.def(pkState, "GetOriginalSkill", &lwSkillTree::GetOriginalSkill)
		.def(pkState, "IsNil", &lwSkillTree::IsNil)
		;

	return	true;
}

bool lwSkillTree::IsNil()
{
	return	false;	//임시
}

int	lwSkillTree::GetSkillNo(int iKeySkillNo)
{
	PgSkillTree::stTreeNode *pFound = g_kSkillTree.GetNode(iKeySkillNo);
	if(!pFound)
	{
		return	0;
	}

	return	pFound->m_ulSkillNo;
}
int	lwSkillTree::GetNextLevelSkillNo(int iKeySkillNo)
{
	return	g_kSkillTree.GetNextLevelSkillNo(iKeySkillNo);
}
void	lwSkillTree::SetRemainSkillPoint(int iSkillPoint)
{
	g_kSkillTree.SetRemainSkillPoint(iSkillPoint);
}
int	lwSkillTree::GetRemainSkillPoint()
{
	return	g_kSkillTree.GetRemainSkillPoint();
}
int	lwSkillTree::GetUsedSkillPoint()
{
	return	g_kSkillTree.GetUsedSkillPoint();
}
void lwSkillTree::NewSkillLearned(unsigned long ulSkillNo,int iRemainSkillPoint)
{
	g_kSkillTree.NewSkillLearned(ulSkillNo,iRemainSkillPoint);
}
void lwSkillTree::DeleteSkill(unsigned long ulSkillNo)
{
	g_kSkillTree.DeleteSkill(ulSkillNo);
}
int	lwSkillTree::GetKeySkillNo(int iSkillNo)
{
	return	g_kSkillTree.GetKeySkillNo(iSkillNo);
}
int	lwSkillTree::GetNeedSkillPoint(int iKeySkillNo)
{
	return	g_kSkillTree.GetNeedSkillPoint(iKeySkillNo);
}

bool	lwSkillTree::CanLevelUp(int iKeySkillNo)
{
	return (g_kSkillTree.CanLevelUp(iKeySkillNo) == PgSkillTree::NLR_NONE);
}
bool	lwSkillTree::CanLevelDown(int iKeySkillNo)
{
	return	g_kSkillTree.CanLevelDown(iKeySkillNo);
}
bool	lwSkillTree::IsTemporaryLevelChanged(int iKeySkillNo)
{
	PgSkillTree::stTreeNode *pFound = g_kSkillTree.GetNode(iKeySkillNo);
	if(!pFound)
	{
		return	false;
	}

	return	pFound->IsTemporaryLevelChanged();
}
bool	lwSkillTree::IsTemporaryRemainSkillPoint(int iSkillNo)
{
	return	g_kSkillTree.IsTemporaryRemainSkillPoint(iSkillNo);
}
int	lwSkillTree::GetSkillLevel(int iKeySkillNo, bool bOverSkill)
{
	PgSkillTree::stTreeNode *pFound = g_kSkillTree.GetNode(iKeySkillNo);
	if(!pFound)
	{
		return	0;
	}

	if(pFound->m_bLearned == false && pFound->m_bTemporaryLearned == false)
	{
		return 0;
	}

	if(bOverSkill)
	{
		return pFound->GetOverSkillLevel();
	}

	return	pFound->m_pkSkillDef->GetAbil(AT_LEVEL);
}

int lwSkillTree::GetOverSkillLevel(int const iKeySkillNo)
{
	PgSkillTree::stTreeNode *pFound = g_kSkillTree.GetNode(iKeySkillNo);

	if(!pFound)
	{
		return	0;
	}

	if(pFound->m_bLearned == false && pFound->m_bTemporaryLearned == false)
	{
		return 0;
	}

	return pFound->GetOverSkillLevel();
	
}

int	lwSkillTree::GetMaxSkillLevel(int iKeySkillNo)
{
	PgSkillTree::stTreeNode *pFound = g_kSkillTree.GetNode(iKeySkillNo);
	if(!pFound)
	{
		return	0;
	}
	return	pFound->GetMaxSkillLevel();
}

lwWString	lwSkillTree::GetSkillName(int iKeySkillNo)
{
	PgSkillTree::stTreeNode *pFound = g_kSkillTree.GetNode(iKeySkillNo);

	if(pFound == NULL) return lwWString(_T("Undefined"));

	const wchar_t *pText = NULL;
	if(GetDefString(pFound->m_pkSkillDef->NameNo(),pText))
	{
		return	lwWString(pText);
	}
	return lwWString(_T("Undefined"));
}
int	lwSkillTree::GetSkillIconKey(int iKeySkillNo)
{
	PgSkillTree::stTreeNode *pFound = g_kSkillTree.GetNode(iKeySkillNo);
	if(!pFound)
	{
		return	0;
	}

	return	pFound->m_pkSkillDef->RscNameNo();
}
void	lwSkillTree::LevelUpTemporary(int iKeySkillNo)
{
	g_kSkillTree.LevelUpTemporary(iKeySkillNo);
}
void	lwSkillTree::LevelDownTemporary(int iKeySkillNo)
{
	g_kSkillTree.LevelDownTemporary(iKeySkillNo);
}
void	lwSkillTree::ResetTemporary()
{
	g_kSkillTree.ResetTemporary();
}
void	lwSkillTree::ConfirmTemporary()
{
	g_kSkillTree.ConfirmTemporary();
}
bool	lwSkillTree::IsLearnedSkill(int iKeySkillNo)
{
	PgSkillTree::stTreeNode *pFound = g_kSkillTree.GetNode(iKeySkillNo);
	if(!pFound)
	{
		return	false;
	}

	return pFound->m_bLearned;
}

bool	lwSkillTree::IsCommandSkill(int iKeySkillNo)
{
	PgSkillTree::stTreeNode *pFound = g_kSkillTree.GetNode(iKeySkillNo);
	if(pFound)
	{
		CONT_DEFSKILL const* pkContDefMap = NULL;
		g_kTblDataMgr.GetContDef(pkContDefMap);
		if( pkContDefMap )
		{
			CONT_DEFSKILL::const_iterator def_it = pkContDefMap->find(pFound->m_ulKeySkillNo);
			if (pkContDefMap->end() != def_it)
			{
				CONT_DEFSKILL::mapped_type const& kT = (*def_it).second;
				if( 0 != kT.iCmdStringNo )
				{
					return true;
				}
			}
		}
	}
	return false;
}

int		lwSkillTree::GetOriginalSkill(int const iKeySkillNo)
{
	PgSkillTree::stTreeNode *pFound = g_kSkillTree.GetNode(iKeySkillNo);
	if(!pFound)
	{
		return	0;
	}

	if (!pFound->m_bLearned)
	{
		return iKeySkillNo;
	}

	return pFound->GetOriginalSkillNo();
}

bool	lwSkillTree::IsActiveSkill(int iKeySkillNo)
{
	PgSkillTree::stTreeNode *pFound = g_kSkillTree.GetNode(iKeySkillNo);
	if(!pFound)
	{
		return	false;
	}

	BYTE	bySkillType = pFound->m_pkSkillDef->GetType();

	return  ((bySkillType == EST_ACTIVE) || (bySkillType == EST_TOGGLE));

}
int	lwSkillTree::GetTotalSkill()
{
	PgSkillTree::VTree&	kTree = g_kSkillTree.GetTree();
	return	kTree.size();
}
int	lwSkillTree::GetKeySkillNoByIndex(int iIndex)
{
	PgSkillTree::VTree&	kTree = g_kSkillTree.GetTree();

	int	iCount = 0;
	for(PgSkillTree::VTree::iterator itor = kTree.begin(); itor != kTree.end(); itor++)
	{
		if( iCount == iIndex )
		{
			return	itor->first;
		}
		iCount++;
	}

	return	0;
}

int	lwSkillTree::GetKeySkillResNoByIndex(int iIndex)
{
	PgSkillTree::VTree&	kTree = g_kSkillTree.GetTree();
	int	iCount = 0;
	for(PgSkillTree::VTree::iterator itor = kTree.begin(); itor != kTree.end(); itor++)
	{
		if( iCount == iIndex )
		{
			PgSkillTree::stTreeNode *pFound = itor->second;
			if(pFound && pFound->m_pkSkillDef)
			{
				return pFound->m_pkSkillDef->RscNameNo();
			}
			else
			{
				return 0;
			}
		}
		iCount++;
	}
	return 0;
}

int	lwSkillTree::GetKeySkillResNoBySkillNo(int const iNo) const
{
	PgSkillTree::stTreeNode *pFound = g_kSkillTree.GetNode(g_kSkillTree.GetKeySkillNo(iNo));
	if(pFound && pFound->m_pkSkillDef)
	{
		return pFound->m_pkSkillDef->RscNameNo();
	}

	return 0;
}

bool	lwSkillTree::GetCanLearn(int iTarget, int iFrom) const
{
	return g_kSkillTree.CanLearn(iTarget, iFrom);
}

bool lwSkillTree::CheckAllNeedSkill(int const iSkillNo) const
{
	return g_kSkillTree.CheckAllNeedSkill(iSkillNo);
}
