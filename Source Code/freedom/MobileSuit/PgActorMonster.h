#ifndef FREEDOM_DRAGONICA_RENDER_WORDOBJECT_ACTOR_PGACTORMONSTER_H
#define FREEDOM_DRAGONICA_RENDER_WORDOBJECT_ACTOR_PGACTORMONSTER_H

#include "Variant/PgMonster.h"

class PgActorMonster : public PgActor
{
	NiDeclareRTTI;

public:
	PgActorMonster();

	void SetMonsterType(EMonsterType eType) { m_eMonsterType = eType; }
	EMonsterType GetMonsterType() { return m_eMonsterType; }
	void SetLookTarget(bool bLook);

	virtual	PgIWorldObject*	CreateCopy();


	virtual bool Update(float fAccumTime, float fFrameTime);
	virtual void UpdatePhysX(float fAccumTime, float fFrameTime);
	virtual void ConcilDirection(NiPoint3 &rkLookingDir, bool bRightAway = false);
	virtual bool ProcessAction(PgAction *pkAction,bool bInvalidateDirection = false,bool bForceToTransit = false);

	void ResetActiveGrp();
	
	//!	몬스터가 인지한 공격목표를 설정한다.
	void SetAttackTarget(BM::GUID const &kTargetGUID);

	//! ActionQueue에 있는 Action을 처리함.
	virtual bool ProcessActionQueue();

	//! Sync를 시작.
	virtual bool BeginSync(PgAction *pkAction, DWORD dwOverTime);

	//! Sync를 수행.
	virtual bool UpdateSync(float fFrameTime);

	//! 공중형을 위해 Z값을 얼마나 올려야 되는지 확인.
	float AdjustToFly(NiPoint3 const &kCurPos, float fFrameTime);

	bool DoSpeech(EUnitState const eUnitState);

	//! PhysX를 초기화한다.
	virtual	void InitPhysX(NiPhysXScene *pkPhysXScene, int uiGroup);

private:
	void SetMovingDeltaZCheck();

protected:

	EMonsterType m_eMonsterType;
	bool m_bLookFocusTarget;
};
#endif // FREEDOM_DRAGONICA_RENDER_WORDOBJECT_ACTOR_PGACTORMONSTER_H