#ifndef FREEDOM_DRAGONICA_CONTENTS_JOBSKILL_LWJOBSKILLVIEW_H
#define FREEDOM_DRAGONICA_CONTENTS_JOBSKILL_LWJOBSKILLVIEW_H

namespace lwJobSkillView
{
	bool IsActivateMainWnd();
	void UpdateMainWnd();

	void RegisterWrapper(lua_State *pkState);
	void lwCallJobSkillViewUI();
	bool lwShowList(int iPage);
	bool lwNextPage();
	bool lwBeforePage();
	void lwClearList(XUI::CXUI_Wnd* pLearn);
	void lwClearAll();
	void lwCallJobSkillDelete(int const iBuildIndex);
	void lwRequestDeleteJobSkill(int const JobSkillNo);
	void ReturnResult(HRESULT const kRet);
	bool lwCloseJobSkillViewUI();
	int GetMaxPage();
	void VisibleEmptyText(bool const bVisible);
	void SetPageUI(int const iCurrent, int const iMax);
	void SetTiredGauge(PgPlayer* pPlayer);
	void SetBlessGauge(PgPlayer* pPlayer);
	void SetProductiveGauge(PgPlayer* pPlayer);
}

#endif // FREEDOM_DRAGONICA_CONTENTS_JOBSKILL_LWJOBSKILLVIEW_H