#ifndef FREEDOM_DRAGONICA_CONTENTS_ALRAMMISSIONCLIENT_H
#define FREEDOM_DRAGONICA_CONTENTS_ALRAMMISSIONCLIENT_H

template< BM::CPacket::DEF_PACKET_TYPE kType > class PgAlramMissionClient;

template< >
class PgAlramMissionClient< PT_M_C_NFY_ALRAMMISSION_BEGIN >
{
public:
	bool operator()( BM::CPacket &kPacket );
};

template< >
class PgAlramMissionClient< PT_M_C_NFY_ALRAMMISSION_END >
{
public:
	bool operator()( BM::CPacket &rkPacket );
	static bool Call( PgAlramMission &rkAlramMission );
};


template< >
class PgAlramMissionClient< PT_M_C_NFY_ALRAMMISSION_SUCCESS >
{
public:
	bool operator()( BM::CPacket &rkPacket );
	static bool Call( PgAlramMission &rkAlramMission );
};

template< >
class PgAlramMissionClient< PT_M_C_NFY_ALRAMMISSION_PARAM >
{
public:
	bool operator()( BM::CPacket &rkPacket );
};

#endif // FREEDOM_DRAGONICA_CONTENTS_ALRAMMISSIONCLIENT_H