#include "stdafx.h"
#include "Variant/TableDataManager.h"
#include "Variant/PgJobSkill.h"
#include "ServerLib.h"
#include "lwJobSkillView.h"
#include "lwUI.h"
#include "PgPilotMan.h"
#include "PgNetwork.h"

namespace lwJobSkillView
{
	//메인
	std::wstring const JOBSKILL_MAIN(L"SFRM_JOB_SKILL_VIEW");
	//배울 수 있는 직업 목록
	std::wstring const VIEW_SHADOW(L"SFRM_VIEW_SHADOW");
	std::wstring const VIEW_MAIN(L"FRM_VIEW");
	std::wstring const VIEW_ICON(L"SFRM_ICON");
	std::wstring const VIEW_TEXT(L"SFRM_TEXT");
	std::wstring const VIEW_TEXT_TITLE(L"FRM_TEXT_TITLE");
	std::wstring const VIEW_TEXT_INFO(L"FRM_TEXT_INFO");
	std::wstring const VIEW_DEBUF(L"FRM_DEBUF");
	std::wstring const BTN_DELETE(L"BTN_DELETE");
	std::wstring const VIEW_NOT_HAVE_SKILL(L"FRM_NOT_HAVE_SKILL");	
	//페이지 넘김
	std::wstring const BTN_PAGE_L(L"BTN_PAGE_L");
	std::wstring const FRM_PAGE_BG(L"FRM_PAGE_BG");
	std::wstring const BTN_PAGE_R(L"BTN_PAGE_R");
	//배운아이템
	std::wstring const BTN_LEARNED_ITME(L"BTN_LEARNED_ITEM");
	//피로도
	std::wstring const VIEW_TIRED(L"FRM_TIRED");
	//축복
	std::wstring const VIEW_BLESS(L"FRM_BLESS");
	//생산력
	std::wstring const VIEW_PRODUCTIVE(L"FRM_PRODUCTIVE");
	//피로도/축복 게이지(공통)
	std::wstring const ANI_GAUGE(L"FRM_GAUGE");

	int const MAX_SHOW_LIST = 5;
	//
	int g_kCurrentPage = 1;
	
	void RegisterWrapper(lua_State *pkState)
	{
		using namespace lua_tinker;
		def(pkState, "CallJobSkillViewUI", lwCallJobSkillViewUI);
		def(pkState, "JobSkillView_NextPage", lwNextPage);
		def(pkState, "JobSkillView_BeforePage", lwBeforePage);
		def(pkState, "CallJobSkillDelete", lwCallJobSkillDelete);
		def(pkState, "RequestDeleteJobSkill", lwRequestDeleteJobSkill);
		def(pkState, "CloseJobSkillViewUI", lwCloseJobSkillViewUI);
	}

	bool IsActivateMainWnd()
	{
		XUI::CXUI_Wnd* pkWnd = NULL;
		return XUIMgr.IsActivate(JOBSKILL_MAIN, pkWnd);
	}

	void UpdateMainWnd()
	{
		bool const bRet = lwShowList(g_kCurrentPage);
		VisibleEmptyText(!bRet);
		if(!bRet)
		{
			lwClearAll();
		}
	}
	void lwCallJobSkillViewUI()
	{
		bool const bRet = lwShowList(1);
		VisibleEmptyText(!bRet);
		if(!bRet)
		{
			lwClearAll();
		}
	}
	bool lwNextPage()
	{
		bool const bRet = lwShowList(g_kCurrentPage + 1);
		VisibleEmptyText(!bRet);
		if(!bRet)
		{
			lwClearAll();
		}
		return true;
	}
	bool lwBeforePage()
	{	
		if( 1 == g_kCurrentPage)
		{
			++g_kCurrentPage;
		}
		bool const bRet = lwShowList(g_kCurrentPage - 1);
		VisibleEmptyText(!bRet);
		if(!bRet)
		{
			lwClearAll();
		}
		return true;
	}

	bool lwShowList(int iPage)
	{
		XUI::CXUI_Wnd* pMain = XUIMgr.Activate(JOBSKILL_MAIN);
		if(!pMain)
		{
			return false;
		}
		XUI::CXUI_Wnd* pViewSD = pMain->GetControl(VIEW_SHADOW);
		if(!pViewSD)
		{
			return false;
		}

		PgPlayer* pPlayer = g_kPilotMan.GetPlayerUnit();
		if(!pPlayer)
		{
			return false;
		}
		PgJobSkillExpertness const& rkMyJobSkill = pPlayer->JobSkillExpertness();
		PgJobSkillExpertness::CONT_EXPERTNESS const& rkContJobSkill = rkMyJobSkill.GetAllSkillExpertness();

		SetTiredGauge(pPlayer);
		SetBlessGauge(pPlayer);
		SetProductiveGauge(pPlayer);

		int iMaxPage = GetMaxPage();
		if( iMaxPage < iPage )
		{
			iPage = iMaxPage;
		}

		if( !rkContJobSkill.size() ||
			1 > iPage || 0 == iMaxPage)
		{//배운게 아무것도 없으면 실패
			return false;
		}
		//현재/최대 페이지 연출
		SetPageUI(iPage, iMaxPage);

		PgJobSkillExpertness::CONT_EXPERTNESS::const_iterator itor_Job = rkContJobSkill.begin();
		int iPageCount = 0;
		while( iPageCount < ( (iPage-1)*MAX_SHOW_LIST) )
		{//지정된 페이지로 이동
			if( itor_Job == rkContJobSkill.end() )
			{ 
				break;
			}
			++itor_Job;
			++iPageCount;
		}
		
		int iListCount = 0;
		while( iListCount < MAX_SHOW_LIST )
		{
			BM::vstring kView = BM::vstring(VIEW_MAIN) + iListCount;
			XUI::CXUI_Wnd* pView = pViewSD->GetControl(kView);
			if(!pView)
			{
				return false;
			}
			if( itor_Job == rkContJobSkill.end() )
			{ 
				lwClearList(pView);
				++iListCount;
				continue;
			}
			PgJobSkillExpertness::CONT_EXPERTNESS::key_type const &rkJobSkillNo = (*itor_Job).first;
			PgJobSkillExpertness::CONT_EXPERTNESS::mapped_type const &rkExpertness = (*itor_Job).second;
			GET_DEF(CSkillDefMgr, kSkillDefMgr);
			CSkillDef const* pSkillDef;
			pSkillDef = kSkillDefMgr.GetDef(rkJobSkillNo);
			if(!pSkillDef)
			{
				lwClearList(pView);
				++iListCount;
				++itor_Job;
				continue;
			}
			
			CONT_DEF_JOBSKILL_SKILL const* pkContDefJobSkill;
			g_kTblDataMgr.GetContDef(pkContDefJobSkill);
			CONT_DEF_JOBSKILL_SKILL::const_iterator iter_Def = pkContDefJobSkill->find(rkJobSkillNo);
			if(iter_Def == pkContDefJobSkill->end())
			{
				lwClearList(pView);
				++iListCount;
				++itor_Job;
				continue;;
			}
			CONT_DEF_JOBSKILL_SKILL::mapped_type const &rkJobSkillInfo = (*iter_Def).second;

			PgPlayer* pPlayer = g_kPilotMan.GetPlayerUnit();
			int const iPenalty = JobSkill_Util::GetJobSkillPenaltyLevel(pPlayer, rkJobSkillNo);

			wchar_t const* pName = NULL;
			GetDefString(pSkillDef->NameNo(),pName);
			XUI::CXUI_Wnd* pIconMain = pView->GetControl(VIEW_ICON);
			if(pIconMain)
			{
				XUI::CXUI_Wnd* pIconWnd = pIconMain->GetControl(L"JS_Icon");
				if(pIconWnd)
				{
					XUI::CXUI_Icon* pIcon = dynamic_cast<XUI::CXUI_Icon*>(pIconWnd);
					if(pIcon)
					{
						SIconInfo kIconInfo = pIcon->IconInfo();
						kIconInfo.iIconKey = rkJobSkillNo;
						kIconInfo.iIconResNumber = pSkillDef->RscNameNo();
						pIcon->SetIconInfo(kIconInfo);
					}
				}
			}
			XUI::CXUI_Wnd* pText = pView->GetControl(VIEW_TEXT);
			if(pText)
			{
				XUI::CXUI_Wnd* pTitle = pText->GetControl(VIEW_TEXT_TITLE);
				if(pTitle)
				{//기술 이름
					pTitle->Text(pName);
				}
				XUI::CXUI_Wnd* pInfo = pText->GetControl(VIEW_TEXT_INFO);
				if(pInfo)
				{//현재 숙련도 표기
					if( 0 == rkJobSkillInfo.i01NeedParent_JobSkill_No)
					{
						wchar_t szBuf[MAX_PATH] ={0,};
						int const iCurrentExp = rkExpertness/JSE_EXPERTNESS_DEVIDE;
						int const iMaxExp = JobSkillExpertnessUtil::GetBiggestMaxExpertness(rkJobSkillNo) / JSE_EXPERTNESS_DEVIDE;
						wsprintfW(szBuf, TTW(799532).c_str(), iCurrentExp, iMaxExp);
						pInfo->Text(szBuf);
					}
					else
					{
						pInfo->Text(L"");
					}
				}
				XUI::CXUI_Wnd* pDebuf = pText->GetControl(VIEW_DEBUF);
				if(pDebuf)
				{
					int const iPenaltyLevel = JobSkill_Util::GetJobSkillPenaltyLevel(pPlayer, rkJobSkillNo);
					if( 0 < iPenaltyLevel )
					{
						pDebuf->Visible(true);
					}
					else
					{
						pDebuf->Visible(false);
					}
					pDebuf->SetCustomData(&iPenaltyLevel , sizeof(iPenaltyLevel) );
				}
				//주기술 아니면 삭제 버튼 보이지 않도록
				XUI::CXUI_Wnd* pDelete = pText->GetControl(BTN_DELETE);
				if(!pDelete)
				{
					return false;
				}
				if( 0 != rkJobSkillInfo.i01NeedParent_JobSkill_No)
				{
					pDelete->Visible(false);
				}
				else
				{
					pDelete->Visible(true);
				}
			}
			pView->SetCustomData(&rkJobSkillNo, sizeof(rkJobSkillNo) );
			++iListCount;
			++itor_Job;
		}
		g_kCurrentPage = iPage;
		return true;
	}
	void SetPageUI(int const iCurrent, int const iMax)
	{
		XUI::CXUI_Wnd* pMain = XUIMgr.Get(JOBSKILL_MAIN);
		if(!pMain)
		{
			return;
		}
		XUI::CXUI_Wnd* pPage = pMain->GetControl(FRM_PAGE_BG);
		if(pPage)
		{
			wchar_t szBuf[200] ={0,};
			wsprintfW(szBuf, TTW(799506).c_str(), iCurrent, iMax);
			pPage->Text(szBuf);
		}
	}
	void VisibleEmptyText(bool const bVisible)
	{
		XUI::CXUI_Wnd* pMain = XUIMgr.Get(JOBSKILL_MAIN);
		if(!pMain)
		{
			return;
		}

		XUI::CXUI_Wnd* pErrWnd = pMain->GetControl(VIEW_NOT_HAVE_SKILL);
		if(pErrWnd)
		{
			pErrWnd->Visible(bVisible);
		}
	}

	int GetMaxPage()	
	{
		PgPlayer* pPlayer = g_kPilotMan.GetPlayerUnit();
		if(!pPlayer)
		{
			return false;
		}
		PgJobSkillExpertness const& rkMyJobSkill = pPlayer->JobSkillExpertness();
		PgJobSkillExpertness::CONT_EXPERTNESS const& rkContJobSkill = rkMyJobSkill.GetAllSkillExpertness();

		int iMaxPage = rkContJobSkill.size() / MAX_SHOW_LIST;
		if( 0 != (rkContJobSkill.size() % MAX_SHOW_LIST) )
		{
			++iMaxPage;
		}

		return iMaxPage;
	}

	void SetTiredGauge(PgPlayer* pPlayer)
	{
		if(!pPlayer)
		{
			return;
		}
		XUI::CXUI_Wnd* pMain = XUIMgr.Get(JOBSKILL_MAIN);
		if(!pMain)
		{
			return;
		}
		XUI::CXUI_Wnd* pTiredWnd = pMain->GetControl(VIEW_TIRED);
		if(!pTiredWnd)
		{
			return;
		}
		XUI::CXUI_Wnd* pGaugeWnd = pTiredWnd->GetControl(ANI_GAUGE);
		if(!pGaugeWnd)
		{
			return;
		}
		XUI::CXUI_AniBar* pAniBar = dynamic_cast<XUI::CXUI_AniBar*>(pGaugeWnd);
		if(!pAniBar)
		{
			return;
		}

		PgJobSkillExpertness const &rkExpertness = pPlayer->JobSkillExpertness();
		int const iMax = JobSkillExpertnessUtil::GetBiggestMaxExhaustion(rkExpertness.GetAllSkillExpertness());
		int const iNow = rkExpertness.CurExhaustion();
		
		pAniBar->Max(iMax);
		pAniBar->Now(iMax-iNow);
		wchar_t szBuf[MAX_PATH] ={0,};
		wsprintfW(szBuf, L"%d / %d", iMax-iNow, iMax );
		pAniBar->Text(szBuf);

	}
	void SetBlessGauge(PgPlayer* pPlayer)
	{
		if(!pPlayer)
		{
			return;
		}
		XUI::CXUI_Wnd* pMain = XUIMgr.Get(JOBSKILL_MAIN);
		if(!pMain)
		{
			return;
		}
		XUI::CXUI_Wnd* pBlessWnd = pMain->GetControl(VIEW_BLESS);
		if(!pBlessWnd)
		{
			return;
		}
		XUI::CXUI_Wnd* pGaugeWnd = pBlessWnd->GetControl(ANI_GAUGE);
		if(!pGaugeWnd)
		{
			return;
		}
		XUI::CXUI_AniBar* pAniBar = dynamic_cast<XUI::CXUI_AniBar*>(pGaugeWnd);
		if(!pAniBar)
		{
			return;
		}

		PgJobSkillExpertness const &rkExpertness = pPlayer->JobSkillExpertness();
		int const iMax = JobSkillExpertnessUtil::GetBiggestMaxExhaustion(rkExpertness.GetAllSkillExpertness(), JST_2ND_MAIN);
		int const iNow = iMax - rkExpertness.CurBlessPoint();
		
		pAniBar->Max(iMax);
		pAniBar->Now(iNow);
		wchar_t szBuf[MAX_PATH] ={0,};
		wsprintfW(szBuf, L"%d / %d", iNow, iMax );
		pAniBar->Text(szBuf);
	}
	void SetProductiveGauge(PgPlayer* pPlayer)
	{
		if(!pPlayer)
		{
			return;
		}
		XUI::CXUI_Wnd* pMain = XUIMgr.Get(JOBSKILL_MAIN);
		if(!pMain)
		{
			return;
		}
		XUI::CXUI_Wnd* pBlessWnd = pMain->GetControl(VIEW_PRODUCTIVE);
		if(!pBlessWnd)
		{
			return;
		}
		XUI::CXUI_Wnd* pGaugeWnd = pBlessWnd->GetControl(ANI_GAUGE);
		if(!pGaugeWnd)
		{
			return;
		}
		XUI::CXUI_AniBar* pAniBar = dynamic_cast<XUI::CXUI_AniBar*>(pGaugeWnd);
		if(!pAniBar)
		{
			return;
		}

		PgJobSkillExpertness const &rkExpertness = pPlayer->JobSkillExpertness();
		int const iMax = JobSkillExpertnessUtil::GetBiggestMaxExhaustion(rkExpertness.GetAllSkillExpertness(), JST_3RD_MAIN);
		int const iNow = iMax - rkExpertness.CurProductPoint();

		pAniBar->Max(iMax);
		pAniBar->Now(iNow);
		wchar_t szBuf[MAX_PATH] ={0,};
		wsprintfW(szBuf, L"%d / %d", iNow, iMax );
		pAniBar->Text(szBuf);
	}
	void lwClearList(XUI::CXUI_Wnd* pView)
	{
		if(pView)
		{
			XUI::CXUI_Wnd* pIconMain = pView->GetControl(VIEW_ICON);
			if(pIconMain)
			{
				XUI::CXUI_Wnd* pIconWnd = pIconMain->GetControl(L"JS_Icon");
				if(pIconWnd)
				{
					XUI::CXUI_Icon* pIcon = dynamic_cast<XUI::CXUI_Icon*>(pIconWnd);
					if(pIcon)
					{
						SIconInfo kIconInfo = pIcon->IconInfo();
						kIconInfo.iIconKey = 0;
						kIconInfo.iIconResNumber = 0;
						pIcon->SetIconInfo(kIconInfo);
					}
				}
			}
			XUI::CXUI_Wnd* pText = pView->GetControl(VIEW_TEXT);
			if(pText)
			{
				XUI::CXUI_Wnd* pTitle = pText->GetControl(VIEW_TEXT_TITLE);
				if(pTitle)
				{
					pTitle->Text(L"");
				}
				XUI::CXUI_Wnd* pInfo = pText->GetControl(VIEW_TEXT_INFO);
				if(pInfo)
				{
					pInfo->Text(L"");
				}
				XUI::CXUI_Wnd* pDebuf = pText->GetControl(VIEW_DEBUF);
				if(pDebuf)
				{
					pDebuf->Visible(false);
					pDebuf->ClearCustomData();
				}
				//주기술 아니면 삭제 버튼 보이지 않도록
				XUI::CXUI_Wnd* pDelete = pText->GetControl(BTN_DELETE);
				if(pDelete)
				{
					pDelete->Visible(false);
				}
			}
			pView->ClearCustomData();
		}
	}

	void lwCallJobSkillDelete(int const iBuildIndex)
	{
		PgPlayer* pPlayer = g_kPilotMan.GetPlayerUnit();
		if(!pPlayer)
		{
			return;
		}
		XUI::CXUI_Wnd* pMain = XUIMgr.Activate(JOBSKILL_MAIN);
		if(!pMain)
		{
			return;
		}
		XUI::CXUI_Wnd* pViewSD = pMain->GetControl(VIEW_SHADOW);
		if(!pViewSD)
		{
			return;
		}
		BM::vstring kView = BM::vstring(VIEW_MAIN) + iBuildIndex;
		XUI::CXUI_Wnd* pView = pViewSD->GetControl(kView);
		if(!pView)
		{
			return;
		}
		int iJobSkillNo = 0;
		pView->GetCustomData(&iJobSkillNo, sizeof(iJobSkillNo) );
		if( 0 == iJobSkillNo)
		{
			return;
		}
		wchar_t const* pName = NULL;
		if(!GetDefString(iJobSkillNo,pName))
		{
			return;
		}
		wchar_t szBuf[200] ={0,};
		wsprintfW(szBuf, TTW(799537).c_str(), pName);
		CallYesNoMsgBox(szBuf, pPlayer->GetID() , MBT_DEL_JOBSKILL, iJobSkillNo);
	}

	void lwRequestDeleteJobSkill(int const JobSkillNo)
	{
		BM::CPacket kPacket(PT_C_M_REQ_DELETE_JOBSKILL);
		kPacket.Push(JobSkillNo);
		NETWORK_SEND(kPacket);
	}
	void ReturnResult(HRESULT const kRet)
	{
		if( S_OK == kRet)
		{
			lua_tinker::call<void, int, bool>("CommonMsgBoxByTextTable", 799539, true);
		}
		else
		{
			lua_tinker::call<void, int, bool>("CommonMsgBoxByTextTable", 799540, true);
		}
	}

	void lwClearAll()
	{
		XUI::CXUI_Wnd* pMain = XUIMgr.Activate(JOBSKILL_MAIN);
		if(!pMain)
		{
			return;
		}
		XUI::CXUI_Wnd* pViewSD = pMain->GetControl(VIEW_SHADOW);
		if(!pViewSD)
		{
			return;
		}
		PgPlayer* pPlayer = g_kPilotMan.GetPlayerUnit();
		if(!pPlayer)
		{
			return;
		}

		SetTiredGauge(pPlayer);
		SetBlessGauge(pPlayer);
		SetProductiveGauge(pPlayer);
		SetPageUI(0,0);
		g_kCurrentPage = 1;

		int iListCount = 0;
		while( iListCount < MAX_SHOW_LIST )
		{
			BM::vstring kView = BM::vstring(VIEW_MAIN) + iListCount;
			XUI::CXUI_Wnd* pView = pViewSD->GetControl(kView);
			if(!pView)
			{
				return;
			}
			lwClearList(pView);
			++iListCount;
		}
		return;
	}

	bool lwCloseJobSkillViewUI()
	{
		g_kCurrentPage = 1;
		XUIMgr.Close(JOBSKILL_MAIN);
		return true;
	}
}