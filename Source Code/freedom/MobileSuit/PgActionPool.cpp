#include "StdAfx.h"
#include "PgActionPool.h"
#include "PgAction.h"
#include "PgXmlLoader.h"
#include "PgResourceMonitor.h"

PgActionPool::PgActionPool()
{
	m_kElementPool.Init(50,100);
}

PgActionPool::~PgActionPool()
{
}

void PgActionPool::Destroy()
{
	BM::CAutoMutex kLock(m_kMutex);

	Container::iterator itr = m_kContainer.begin();
	while(itr != m_kContainer.end())
	{
		PG_ASSERT_LOG(itr->second);

		PgAction	*pkAction = itr->second;

		PgXmlLoader::ReleaseXmlDocumentInCacheByID(pkAction->GetID().c_str());
		pkAction->DeleteParamPacket();
		pkAction->Clear();
		m_kElementPool.Delete(pkAction);
		++itr;
	}

	m_kContainer.clear();
	m_kElementPool.Terminate();
}

PgAction* PgActionPool::CreateAction(char const *pcActionID, bool bWarning)
{
	BM::CAutoMutex kLock(m_kMutex);

	PgAction *pkAction = NULL;

	if (pcActionID && strlen(pcActionID) > 0)
	{
		char	strActionID[100] = { 0, };
		strncpy_s(strActionID, 100, pcActionID, 100);

		Container::const_iterator itr = m_kContainer.find(strActionID);
		
		if(itr == m_kContainer.end())
		{
			pkAction = dynamic_cast<PgAction *>(PgXmlLoader::CreateObject(strActionID));
			if(!pkAction) 
			{
				return 0;
			}
			
			itr = m_kContainer.insert(std::make_pair(strActionID, pkAction)).first;

			if(pkAction->GetID() != (std::string)pcActionID)
			{
				NILOG(PGLOG_WARNING, "Action Name isn't equal\n");
				return 0;
			}
		}
		pkAction = m_kElementPool.New();
		//pkAction->Clear();
		pkAction->Init(itr->second);
		PG_RESOURCE_MONITOR(g_kResourceMonitor.IncreaseResourceCounter(pcActionID, PgResourceMonitor::RESOURCE_TYPE_ACTION));
	}
	else
	{
		if (bWarning)
		{
			NILOG(PGLOG_WARNING, "[PgActionPool] action id is null or length is zero\n");
		}

		pkAction = m_kElementPool.New();
		pkAction->Clear();
	}

	return pkAction;
}

void PgActionPool::ReleaseAction(PgAction*& pkAction)
{
	BM::CAutoMutex kLock(m_kMutex);

	if(pkAction)
	{
		pkAction->GetTargetList()->ApplyActionEffects(true, false, 0.0f, true);
		pkAction->DeleteParamPacket();
		pkAction->Clear();
		m_kElementPool.Delete(pkAction);
	}
}
