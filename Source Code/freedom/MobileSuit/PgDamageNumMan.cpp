#include "stdafx.h"
#include "PgDamageNumMan.h"
#include "XUI/XUI_Font.h"
#include "PgRenderer.h"
#include "PgNifMan.h"
#include "PgMobileSuit.H"
#include "PgWorld.H"
#include "PgCameraMan.H"
#include "PgMath.H"
#include "PgActor.h"

#include "NewWare/Scene/ApplyTraversal.h"

NiImplementRTTI(PgDamageNumMan, PgIWorldObject);

int const MAX_NUM_DAMAGE = 7;

bool PgDamageNumMan::m_sbInitExpVertexSetting = false;

////////////////////////////////////////////////////////////////////////////////////////////////////
//	class	PgDamageNumMan
////////////////////////////////////////////////////////////////////////////////////////////////////
void	PgDamageNumMan::Init()
{
	m_fAccumTimeSaved = 0;
	m_bIsRedColor = true;

	m_spRedNum = 0;
	m_spRedNumSmall = 0;
	m_spWhiteNum = 0;
	m_spSkillTextTex = 0;
	m_spExpTex = 0;
	m_spHpTex = 0;
	m_spMpTex = 0;
	m_spCriticalTex = 0;
	m_spWhiteNum2 = 0;
	m_spKillCountTex = 0;
	m_spMissionTex = 0;
	
	if(m_spRedNum == 0)			m_spRedNum = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumDmg.dds");
	if(m_spRedNumSmall == 0)	m_spRedNumSmall = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumDmgR.dds");
	if(m_spWhiteNum == 0)		m_spWhiteNum = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumDmgG.dds");
	if(m_spExpTex == 0)			m_spExpTex = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumExp.dds");
	if(m_spHpTex == 0)			m_spHpTex = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumHp.dds");
	if(m_spMpTex == 0)			m_spMpTex = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumMp.dds");
	if(m_spCriticalTex == 0)	m_spCriticalTex = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumDmgCr.dds");
	if(m_spWhiteNum2 == 0)		m_spWhiteNum2 = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumDmgW.dds");
	if(m_spGreenTex == 0)		m_spGreenTex = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumDmgP.dds");
	if(m_spKillCountTex == 0)	m_spKillCountTex = g_kNifMan.GetTexture( "../Data/6_UI/pvp/pvKillfont.tga");
	if(m_spMissionTex == 0)		m_spMissionTex = g_kNifMan.GetTexture( "../Data/6_UI/mission/msFont.tga");

	NiNodePtr spExpNumNif = g_kNifMan.GetNif("../Data/6_ui/Combo/DamageNum_02.nif");
	//Exp 글씨쪽 Vertex를 변경 해 주는 작업
	NiAVObject	*pNifNode = spExpNumNif;
	SetExpNumber((NiNode*)pNifNode,0, true);	

	g_kNifMan.GetNif("../Data/6_ui/Combo/DamageNum_01.nif");	// 미리 한번 읽게 만든다.
	g_kNifMan.GetNif("../Data/6_ui/Combo/DamageNum_03.nif");	// 미리 한번 읽게 만든다.

	m_fScaleTime = lua_tinker::call<float>("GetDmgNumScaleTime");
	m_fStartScale = lua_tinker::call<float>("GetDmgNumInitialScale");
	m_fTargetScale = lua_tinker::call<float>("GetDmgNumScale");
	m_fHoldingTime = lua_tinker::call<float>("GetDmgNumHoldingTime");
	m_fMoveTime = lua_tinker::call<float>("GetDmgNumMoveTime");
	m_fMoveSpeed = lua_tinker::call<float>("GetDmgNumMoveSpeed");
	m_fMoveTargetScale = lua_tinker::call<float>("GetDmgNumMoveTargetScale");

}
void	PgDamageNumMan::Destroy()
{
	DeleteAllNum();

	m_spSkillTextTex = 0;
	m_spRedNum = 0;
	m_spRedNumSmall = 0;
	m_spWhiteNum = 0;
	m_spExpTex = 0;
	m_spHpTex = 0;
	m_spMpTex = 0;
	m_spCriticalTex = 0;
	m_spWhiteNum2 = 0;
	m_spGreenTex = 0;
	m_spKillCountTex = 0;
	m_spMissionTex = 0;
	
}
void	PgDamageNumMan::DeleteAllNum()
{
	for(NodeList::iterator itr = m_NodeList.begin(); itr != m_NodeList.end(); itr++)
	{
		SAFE_DELETE_NI(*itr);
	}
	m_NodeList.clear();

}

//! WorldObject를 fFrameTime(AccumTime)에 대한 시각으로 갱신
bool PgDamageNumMan::Update(float fAccumTime, float fFrameTime)
{
	m_fAccumTimeSaved = fAccumTime;

	stDamageNumNode *pNode = NULL;
	
	for(NodeList::iterator itr = m_NodeList.begin(); itr != m_NodeList.end(); )
	{
		pNode = (*itr);

		switch(pNode->m_byState)
		{
		case 0:	//	m_fStartScale -> m_fTargetScale 로 스케일링 되면서, Alpha 가 0 -> 1  로 바뀐다.
			{
				float	fElapsedTime = fAccumTime - pNode->m_fCreateTime;
				
				float	fRate = fElapsedTime / m_fScaleTime;
				if(1.0f < fRate)
				{
					fRate = 1.0f; 
				}
				if(0.0f > fRate)
				{
					fRate = 0.0f; 
				}

				float	fScale = m_fStartScale * (1.0f - fRate) + m_fTargetScale * fRate;

				NiPoint3 pt3Pos = pNode->m_vStartPos;
				switch ( pNode->m_Type )
				{
				case stDamageNumNode::T_EXP_NUM:
					{
						fScale *= 0.4f;
					}break;
				case stDamageNumNode::T_HP_OR_MP:
					{
						fScale *= 0.35f;
					}break;
				case stDamageNumNode::T_SMALL_NUM:
					{
						fScale *= 0.5f;
					}break;
				case stDamageNumNode::T_KILL_COUNT_TEXT:
					{
						fRate *= 2.8f;
						if(1.0f < fRate)
						{
							fRate = 1.0f;
						}

						NiPoint3 vec = pNode->m_vTargetPos - pNode->m_vStartPos;
						vec *= fRate;
						pt3Pos = pNode->m_vStartPos + vec;
						fScale = 0.5f;
					}break;
				case stDamageNumNode::T_ENCHANT_LEVEL:
					{
						fScale *= 1.5f;
					}break;
				}

				if(fScale<0.0f) fScale = 0.0f;

				SetAlpha(pNode->m_spNif,fRate);
				pNode->m_spNif->SetScale(fScale);
				pNode->m_spNif->SetTranslate(pt3Pos);
				pNode->m_spNif->Update(fAccumTime);

				if( fRate == 1.0f ) 
				{
					pNode->m_fHoldingStartTime = fAccumTime;
					pNode->m_byState++;
				}
			}
			break;
		case 1:	//	잠시 멈춰있는 시간.
			{
				float	fElapsedTime = fAccumTime - pNode->m_fHoldingStartTime;
				if ( stDamageNumNode::T_KILL_COUNT_TEXT == pNode->m_Type )
				{
					if ( fElapsedTime > 2.3f )
					{
						pNode->m_fMoveStartTime = fAccumTime;
						pNode->m_byState++;

						NiPoint3 vec = pNode->m_vTargetPos - pNode->m_vStartPos;
						pNode->m_vStartPos = pNode->m_vTargetPos;
						pNode->m_vTargetPos = pNode->m_vStartPos + vec;
					}
				}
				else
				{
					if(fElapsedTime>=m_fHoldingTime)
					{
						pNode->m_fMoveStartTime = fAccumTime;
						pNode->m_byState++;
					}
				}
			}
			break;
		case 2:
			{
				float	fElapsedTime = fAccumTime - pNode->m_fMoveStartTime;
				float	fRate = fElapsedTime/m_fMoveTime;
				if(fRate>1.0f) fRate = 1.0f;
				if(fRate<0.0f) fRate = 0.0f;

				float	fScale = m_fTargetScale*(1-fRate)+m_fMoveTargetScale*fRate;
				
				float	fMoveDistance = m_fMoveSpeed*fElapsedTime;
				NiPoint3 kNewPos = pNode->m_vStartPos;
				kNewPos.z += fMoveDistance;

				// 타입에 따라 최대 크기를 조절해 줄 필요가 있음.
				switch ( pNode->m_Type )
				{
				case stDamageNumNode::T_EXP_NUM:
					{
						fScale *= 0.4f;
					}break;
				case stDamageNumNode::T_HP_OR_MP:
					{
						fScale *= 0.35f;
					}break;
				case stDamageNumNode::T_SMALL_NUM:
					{
						fScale *= 0.5f;
					}break;
				case stDamageNumNode::T_KILL_COUNT_TEXT:
					{
						fRate *= 2.8f;
						if(fRate>1.0f) fRate = 1.0f;

						NiPoint3 vec = pNode->m_vTargetPos - pNode->m_vStartPos;
						vec *= fRate;
						kNewPos = pNode->m_vStartPos + vec;
						fScale = 0.5f;
					}break;
				}

				SetAlpha( pNode->m_spNif, 1.0f - fRate );
				pNode->m_spNif->SetScale(fScale);
				pNode->m_spNif->SetTranslate(kNewPos);
				pNode->m_spNif->Update(fAccumTime);

				if(fRate == 1.0f)
				{
					SAFE_DELETE_NI(pNode);
					itr = m_NodeList.erase(itr);
					continue;
				}
			}
			break;
		}

		if(pNode->m_bClampScreen)
		{
			ClampScreen(pNode);
		}

		itr++;
	}
	return	true;
}
void	PgDamageNumMan::ClampScreen(stDamageNumNode *pNode)
{
	if(!g_pkWorld) 
		return;

	PgCameraMan	*pkCameraMan = g_pkWorld->GetCameraMan();
	if(!pkCameraMan)
		return;

	if(!pNode)
		return;

	NiAVObjectPtr	spNifNode = pNode->m_spNif;
	if(!spNifNode)
		return;

	NiCameraPtr	spCamera = pkCameraMan->GetCamera();
	if(spCamera == 0)
		return;

	NiPoint3	kCurrentPos = spNifNode->GetTranslate();
	const	float	fBuffer = 20;

	kCurrentPos.z+=fBuffer;

	//	뷰 프러스텀을 구하자.
	NiFrustumPlanes const &kFP = PgRenderer::GetPgRenderer()->GetFrustumPlanes();

	const	NiPlane	&kTopPlane = kFP.GetPlane(NiFrustumPlanes::TOP_PLANE);

	if(kTopPlane.WhichSide(kCurrentPos) == NiPlane::NEGATIVE_SIDE)
	{
		//	화면 위쪽에 있는것이다. 밑으로 끌어내리자
		const	NiPoint3	kLineP1 = kCurrentPos;
		const	NiPoint3	kLineP2 = kCurrentPos+NiPoint3(0,0,-100);
		const	NiPoint3	kPlaneNormal = kTopPlane.GetNormal();
		const	NiPoint3	kPlaneP3 = kPlaneNormal*kTopPlane.GetConstant();

		const	NiPoint3	kCollPos = GetCollPos_LinePlane(
			kLineP1,kLineP2,kPlaneP3,kPlaneNormal);

		if(kCollPos != NiPoint3(-1,-1,-1))
		{
			const	float	fDistance = (kCollPos - kCurrentPos).Length();

			kCurrentPos += NiPoint3(0,0,-(fDistance+fBuffer));
			spNifNode->SetTranslate(kCurrentPos);
			spNifNode->Update(0);
		}
	}

}
void	PgDamageNumMan::SetAlpha(NiAVObject *pkObject,float fAlpha)
{
	if(NiIsKindOf(NiNode,pkObject))
	{
		NiNode	*pkNode = NiDynamicCast(NiNode,pkObject);
		int	iArrayCount = pkNode->GetArrayCount();
		NiAVObject	*pkChild = NULL;
		for(int i=0;i<iArrayCount;i++)
		{
			pkChild = pkNode->GetAt(i);
			if(pkChild)
			{
				SetAlpha(pkChild,fAlpha);
			}
		}
	}
	else if(NiIsKindOf(NiGeometry,pkObject))
	{
		NiGeometry	*pkGeom = NiDynamicCast(NiGeometry,pkObject);
		NiMaterialProperty	*pkProperty = pkGeom->GetPropertyState()->GetMaterial();
		if(pkProperty)
		{
			pkProperty->SetAlpha(fAlpha);
		}
	}
}

//! pkRenderer를 이용해서 Draw
void PgDamageNumMan::Draw(PgRenderer *pkRenderer, NiCamera *pkCamera, float fFrameTime)
{
	stDamageNumNode *pNode;
	NiAVObject	*pNifNode;
	NiDX9Renderer *pDX9Renderer = (NiDX9Renderer*)pkRenderer->GetRenderer();
	pDX9Renderer->GetD3DDevice()->SetRenderState(D3DRS_ZENABLE,false);
	float	fAlpha = 1.0f;
	
	for ( NodeList::iterator itr = m_NodeList.begin(); itr != m_NodeList.end(); ++itr )
	{
		pNode = (*itr);
		pNifNode = pNode->m_spNif;

		if(pNode->m_Type == stDamageNumNode::T_NUM || pNode->m_Type == stDamageNumNode::T_ENCHANT_LEVEL)
		{
			SetNumber((NiNode*)pNifNode,pNode->m_iNumber);
		}
		else if(pNode->m_Type == stDamageNumNode::T_EXP_NUM)
		{			
			SetExpNumber((NiNode*)pNifNode,pNode->m_iNumber);
		}
		else if(pNode->m_Type == stDamageNumNode::T_HP_OR_MP)
		{			
			SetHpMpNumber((NiNode*)pNifNode,pNode->m_iNumber);
		}
		else if(pNode->m_Type == stDamageNumNode::T_CRITICAL)
		{
			SetCriticalNumber((NiNode*)pNifNode,pNode->m_iNumber);
		}
		else if(pNode->m_Type == stDamageNumNode::T_SMALL_NUM)
		{
			SetSmallNumber((NiNode*)pNifNode,pNode->m_iNumber);
		}

		pkRenderer->PartialRenderClick_Deprecated(pNifNode);
	}
	pDX9Renderer->GetD3DDevice()->SetRenderState(D3DRS_ZENABLE,true);

}

void	PgDamageNumMan::AddNewSimpleText(int iTextType,NiPoint3 const &vLoc)
{
	if (!g_pkWorld)
		return;
	//	iTextType
	//	0 : Miss
	//	1 : Dodge
	//	2 : Block
	

	float	fTextLoc[]=
	{
		0,0,128,32,
		0,32,128,32,
		0,64,128,32,
	};

	if(m_spSimpleTextTex == 0)
		m_spSimpleTextTex = g_kNifMan.GetTexture("../Data/6_UI/main/TMiss2.dds");

	NiNodePtr	spTextNode = g_kNifMan.GetNif_DeepCopy("../Data/6_UI/billboard_quad.nif");
	if(spTextNode == 0)
		return;

	NiGeometry	*pkTextGeom = (NiGeometry*)spTextNode->GetObjectByName("Plane");
	pkTextGeom->GetModelData()->SetConsistency(NiGeometryData::VOLATILE);

	NiTexturingProperty	*pkProperty = pkTextGeom->GetPropertyState()->GetTexturing();
	pkProperty->SetBaseTexture(m_spSimpleTextTex);
	

	float	fTexWidth = 128,fTexHeight = 128;
	float	fBoxWidth = 200;
	float	fBoxHeight = static_cast<float>(fBoxWidth*(32.0/128.0));
	
	NiPoint2	*pkUV = pkTextGeom->GetModelData()->GetTextures();
	NiPoint3	*pkVertex = pkTextGeom->GetModelData()->GetVertices();

	pkVertex->x = -fBoxWidth/2; pkVertex->y = fBoxHeight; pkVertex++;
	pkVertex->x = -fBoxWidth/2; pkVertex->y = 0; pkVertex++;
	pkVertex->x = fBoxWidth/2; pkVertex->y = fBoxHeight; pkVertex++;
	pkVertex->x = fBoxWidth/2; pkVertex->y = 0; pkVertex++;

	pkUV->x = fTextLoc[iTextType*4+0]/fTexWidth;	pkUV->y = fTextLoc[iTextType*4+1]/fTexHeight;	pkUV++;
	pkUV->x = fTextLoc[iTextType*4+0]/fTexWidth;	pkUV->y = (fTextLoc[iTextType*4+1]+fTextLoc[iTextType*4+3])/fTexHeight;	pkUV++;
	pkUV->x = (fTextLoc[iTextType*4+0]+fTextLoc[iTextType*4+2])/fTexWidth;	pkUV->y = fTextLoc[iTextType*4+1]/fTexHeight;	pkUV++;
	pkUV->x = (fTextLoc[iTextType*4+0]+fTextLoc[iTextType*4+2])/fTexWidth;	pkUV->y = (fTextLoc[iTextType*4+1]+fTextLoc[iTextType*4+3])/fTexHeight;	

	pkTextGeom->GetModelData()->MarkAsChanged(NiGeometryData::TEXTURE_MASK | NiGeometryData::VERTEX_MASK);

	AddDamageNumNode( stDamageNumNode::T_SIMPLE_TEXT, spTextNode, 0, vLoc, g_pkWorld->GetAccumTime(), false, false, vLoc );
}

void PgDamageNumMan::AddNewMissionText(int iTextType, NiPoint3 const &vLoc)
{
	if (!g_pkWorld)
		return;	

	float	fTextLoc[]=
	{
		0,0,256,64,
		0,64,256,64,
		0,128,256,64,
		0,192,256,64,
	};

	if(m_spMissionTex == 0)
		m_spMissionTex = g_kNifMan.GetTexture("../Data/6_UI/mission/msFont.tga");

	NiNodePtr	spTextNode = g_kNifMan.GetNif_DeepCopy("../Data/6_UI/billboard_quad.nif");
	if(spTextNode == 0)
		return;

	NiGeometry	*pkTextGeom = (NiGeometry*)spTextNode->GetObjectByName("Plane");
	pkTextGeom->GetModelData()->SetConsistency(NiGeometryData::VOLATILE);

	NiTexturingProperty	*pkProperty = pkTextGeom->GetPropertyState()->GetTexturing();
	pkProperty->SetBaseTexture(m_spMissionTex);
	

	float	fTexWidth = 256,fTexHeight = 256;
	float	fBoxWidth = 200;
	float	fBoxHeight = static_cast<float>(fBoxWidth*(64.0/256.0));
	
	NiPoint2	*pkUV = pkTextGeom->GetModelData()->GetTextures();
	NiPoint3	*pkVertex = pkTextGeom->GetModelData()->GetVertices();

	pkVertex->x = -fBoxWidth/2; pkVertex->y = fBoxHeight; pkVertex++;
	pkVertex->x = -fBoxWidth/2; pkVertex->y = 0; pkVertex++;
	pkVertex->x = fBoxWidth/2; pkVertex->y = fBoxHeight; pkVertex++;
	pkVertex->x = fBoxWidth/2; pkVertex->y = 0; pkVertex++;

	pkUV->x = fTextLoc[iTextType*4+0]/fTexWidth;	pkUV->y = fTextLoc[iTextType*4+1]/fTexHeight;	pkUV++;
	pkUV->x = fTextLoc[iTextType*4+0]/fTexWidth;	pkUV->y = (fTextLoc[iTextType*4+1]+fTextLoc[iTextType*4+3])/fTexHeight;	pkUV++;
	pkUV->x = (fTextLoc[iTextType*4+0]+fTextLoc[iTextType*4+2])/fTexWidth;	pkUV->y = fTextLoc[iTextType*4+1]/fTexHeight;	pkUV++;
	pkUV->x = (fTextLoc[iTextType*4+0]+fTextLoc[iTextType*4+2])/fTexWidth;	pkUV->y = (fTextLoc[iTextType*4+1]+fTextLoc[iTextType*4+3])/fTexHeight;	

	pkTextGeom->GetModelData()->MarkAsChanged(NiGeometryData::TEXTURE_MASK | NiGeometryData::VERTEX_MASK);

	AddDamageNumNode( stDamageNumNode::T_MISSION_TEXT, spTextNode, 0, vLoc, g_pkWorld->GetAccumTime(), false, false, vLoc );
}

void PgDamageNumMan::AddNewKillCountText( int const iTextType, PgActor *pkActor )
{
	if ( g_pkWorld && pkActor)
	{
		if(m_spKillCountTex == 0)	m_spKillCountTex = g_kNifMan.GetTexture( "../Data/6_UI/pvp/pvKillfont.tga");

		if ( m_spKillCountTex )
		{
			NiNodePtr spTextNode = g_kNifMan.GetNif("../Data/6_UI/main/skillTextEX.nif");
			if ( spTextNode )
			{
				NiNode	*pkTextNode = (NiNode*)spTextNode->GetObjectByName("name");
//				NiNode	*pkUpDownNode = (NiNode*)spTextNode->GetObjectByName("up");

				NiGeometry	*pkTextGeom = (NiGeometry*)pkTextNode->GetAt(0);
//				NiGeometry	*pkUpDownGeom = (NiGeometry*)pkUpDownNode->GetAt(0);

				pkTextGeom->GetModelData()->SetConsistency(NiGeometryData::VOLATILE);
//				pkUpDownGeom->GetModelData()->SetConsistency(NiGeometryData::VOLATILE);

				NiTexturingProperty	*pkProperty = pkTextGeom->GetPropertyState()->GetTexturing();
				pkProperty->SetBaseTexture(m_spKillCountTex);

//				pkProperty = pkUpDownGeom->GetPropertyState()->GetTexturing();
//				pkProperty->SetBaseTexture(m_spKillCountTex);

				SetKillCountText( spTextNode, iTextType );

				NiPoint3 pt3StartPos = pkActor->GetPos();
				NiNodePtr pkDummy = (NiNode*)(pkActor->GetObjectByName(ATTACH_POINT_STAR));
				if ( pkDummy )
				{
					pt3StartPos = pkDummy->GetWorldTranslate();
				}

				NiPoint3 pt3TargetPos = pt3StartPos;

				NiCamera *pkCamera = g_pkWorld->m_kCameraMan.GetCamera();
				if ( pkCamera )
				{
					pt3StartPos += (( pkCamera->GetWorldRightVector() * 20.0f ) + ( pkCamera->GetWorldUpVector() * 20.0f ));
					pt3TargetPos = pt3StartPos + ( pkCamera->GetWorldRightVector() * 50.0f );
				}
			
				AddDamageNumNode(stDamageNumNode::T_KILL_COUNT_TEXT, spTextNode, 0, pt3TargetPos, g_pkWorld->GetAccumTime(), false, false, pt3StartPos );
			}
		}
	}
}

void	PgDamageNumMan::AddNewSkillText(int iTextType,bool bIsUP,NiPoint3 const &vLoc)
{
	if (!g_pkWorld)
		return;

	if(m_spSkillTextTex == 0)
		m_spSkillTextTex = g_kNifMan.GetTexture("../Data/6_UI/main/mnSkillText.dds");

	NiNodePtr	spTextNode = g_kNifMan.GetNif_DeepCopy("../Data/6_UI/main/skillTextEX.nif");
	if(spTextNode == 0)
		return;

	NiNode	*pkTextNode = (NiNode*)spTextNode->GetObjectByName("name");
	NiNode	*pkUpDownNode = (NiNode*)spTextNode->GetObjectByName("up");

	NiGeometry	*pkTextGeom = (NiGeometry*)pkTextNode->GetAt(0);
	NiGeometry	*pkUpDownGeom = (NiGeometry*)pkUpDownNode->GetAt(0);

	pkTextGeom->GetModelData()->SetConsistency(NiGeometryData::VOLATILE);
	pkUpDownGeom->GetModelData()->SetConsistency(NiGeometryData::VOLATILE);

	NiTexturingProperty	*pkProperty = pkTextGeom->GetPropertyState()->GetTexturing();
	pkProperty->SetBaseTexture(m_spSkillTextTex);
	
	pkProperty = pkUpDownGeom->GetPropertyState()->GetTexturing();
	pkProperty->SetBaseTexture(m_spSkillTextTex);

	SetSkillText(spTextNode,iTextType,bIsUP);

	AddDamageNumNode(stDamageNumNode::T_SKILL_TEXT, spTextNode, 0, vLoc, g_pkWorld->GetAccumTime(), false, false, vLoc);
}
void	PgDamageNumMan::AddNewNum(int const iNumber,NiPoint3 const &vLoc,bool const bRedColor,bool const bClampScreen, bool const bCritical, int const iEnchantLevel, int const iExceptAbil)
{	
	if (!g_pkWorld)
		return;

	m_bIsRedColor = bRedColor;

	NiNodePtr	spDamageNumNif = 0;

	NiSourceTexturePtr	spEnchantLevelTex = 0;
	NiSourceTexturePtr	spExceptAbilTex = 0; // 어빌에 의해서 예외 처리가 되어야하는 폰트 연출

	if(m_spWhiteNum == 0 || m_spRedNum == 0)
	{
		m_spRedNum = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumDmg.dds");
		m_spWhiteNum = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumDmgG.dds");		
	}

	if(0 < iEnchantLevel && bRedColor)
	{
		spEnchantLevelTex = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumDmgVb.tga");
	}
	else if(0 > iEnchantLevel && bRedColor)
	{
		spEnchantLevelTex = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumDmg2.tga");
	}
	else if(0 > iEnchantLevel)
	{
		spEnchantLevelTex = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumDmgG2.tga");
	}

	if(0 == m_spCriticalTex)
	{
		m_spCriticalTex = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumDmgCr.dds");
	}

	// 예외 어빌로 특정한 데미지 폰트를 출력해주어야 하는 경우.
	switch(iExceptAbil)	
	{
	case 1:
		{
			spExceptAbilTex = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumDmgB.tga");
		}break;
	default:
		{
			spExceptAbilTex = 0;
		}break;
	}

	spDamageNumNif = g_kNifMan.GetNif("../Data/6_ui/Combo/DamageNum_01.nif");
	
	if(0 == spDamageNumNif)
	{
		 return;
	}

	NiNode *pDummy = (NiNode*)spDamageNumNif->GetObjectByName("Dummy01");
	
	NiSourceTexturePtr	spTexture = m_spWhiteNum;
	
	if(bRedColor)
	{
		if(0 != iEnchantLevel)
		{
			spTexture = spEnchantLevelTex;
		}
		else
		{
			spTexture = m_spRedNum;
		}

		//예외 처리가 우선 순위
		if(0 < iExceptAbil)
		{
			spTexture = spExceptAbilTex;
		}
	}
	else
	{
		if(0 > iEnchantLevel)
		{
			spTexture = spEnchantLevelTex;
		}
	}

	//크리티컬이 가장 최우선 순위
	if(bCritical)
	{
		spTexture = m_spCriticalTex;
	}

	NiGeometry *pGeom[MAX_NUM_DAMAGE] = { NULL, };

	for(int i = 0; i < MAX_NUM_DAMAGE; ++i)
	{
		std::string strPlaneNum = "Plane0";
		char szCount[256];
		_itoa_s(i+1, szCount, 10);
		strPlaneNum += szCount;

		pGeom[i] = (NiGeometry*)spDamageNumNif->GetObjectByName(strPlaneNum.c_str());	//	숫자 첫째 자리
		pGeom[i]->GetModelData()->SetConsistency(NiGeometryData::VOLATILE);
		NiTexturingProperty	*pkProperty = pGeom[i]->GetPropertyState()->GetTexturing();
		pkProperty->SetBaseTexture(spTexture);
	}

	stDamageNumNode::TYPE eType = stDamageNumNode::T_NUM;
	if(bCritical)
	{
		eType = stDamageNumNode::T_CRITICAL;
	}

	if(bRedColor && (0 < iEnchantLevel))
	{
		eType = stDamageNumNode::T_ENCHANT_LEVEL;
	}

	AddDamageNumNode(eType, spDamageNumNif, iNumber, vLoc, g_pkWorld->GetAccumTime(), bClampScreen, true, vLoc);
}

void PgDamageNumMan::SetKillCountText(NiNode *pNifNode, int const iTextType )
{
	static float const iTextCoord[]={
		79.0f,	10.0f,	245.0f,	55.0f,
		79.0f,	80.0f,	245.0f,	55.0f,
		79.0f,	150.0f,	245.0f,	55.0f,
		79.0f,	230.0f,	245.0f,	55.0f,
		79.0f,	286.0f,	245.0f,	55.0f,
		79.0f,	367.0f,	245.0f,	55.0f,
		79.0f,	430.0f,	245.0f,	55.0f
 	};

	float	fTexWidth = 512.0f	,fTexHeight = 512.0f;

	NiNode	*pkTextNode = (NiNode*)pNifNode->GetObjectByName("name");
	if ( pkTextNode )
	{
		NiGeometry	*pkTextGeom = (NiGeometry*)pkTextNode->GetAt(0);
		if ( pkTextGeom )
		{
			NiGeometryData	*pkGeomData = pkTextGeom->GetModelData();
			if ( pkGeomData )
			{
				int	iVertexCount = pkGeomData->GetVertexCount();

				NiPoint2	*pkUV = pkGeomData->GetTextures();

				float x = iTextCoord[iTextType*4] / fTexWidth;
				float y = iTextCoord[iTextType*4+1] / fTexHeight;
				float w = iTextCoord[iTextType*4+2] / fTexWidth;
				float h = iTextCoord[iTextType*4+3] / fTexHeight;

				pkUV->x = x+w;
				pkUV->y = y+h;
				pkUV++;

				pkUV->x = x+w;
				pkUV->y = y;
				pkUV++;

				pkUV->x = x;
				pkUV->y = y+h;
				pkUV++;

				pkUV->x = x;
				pkUV->y = y;
				pkGeomData->MarkAsChanged(NiGeometryData::TEXTURE_MASK);
			}
		}
	}

	NiNode	*pkUpDownNode = (NiNode*)pNifNode->GetObjectByName("up");
	if ( pkUpDownNode )
	{
		pkUpDownNode->SetAppCulled( true );
	}
}

void	PgDamageNumMan::SetSkillText(NiNode *pNifNode,int iTextType,bool bIsUP)
{
	static int const	iSkillTextCoord[]={
		0,0,112,23,	// POWER //	x,y,width,height
		112,0,112,23,	//	DEF
		0,23,112,23,	//	MDFF
		112,23,112,23,	//	ATK
		0,46,112,23,	//	MATK
		112,46,112,23,	//	MOVE
		0,69,112,23,	//	MAX HP
		112,69,112,23,	//	MAX MP
		0,92,112,23,	//	CRITICAL
		112,92,112,23,	//	FLEE
		0,115,112,23,	//	ACCURY
	};

	static int const	iUpDownTextCoord[]={
		0,138,152,52,	//	UP
		0,190,152,52	//	DOWN
	};

	float	fTexWidth = 256.0,fTexHeight = 256.0;

	NiNode	*pkTextNode = (NiNode*)pNifNode->GetObjectByName("name");
	NiNode	*pkUpDownNode = (NiNode*)pNifNode->GetObjectByName("up");

	NiGeometry	*pkTextGeom = (NiGeometry*)pkTextNode->GetAt(0);
	NiGeometry	*pkUpDownGeom = (NiGeometry*)pkUpDownNode->GetAt(0);

	NiGeometryData	*pkGeomData = pkTextGeom->GetModelData();

	int	iVertexCount = pkGeomData->GetVertexCount();

	NiPoint2	*pkUV = pkGeomData->GetTextures();

	float	x,y,w,h;

	x = iSkillTextCoord[iTextType*4+0]/fTexWidth;
	y = iSkillTextCoord[iTextType*4+1]/fTexHeight;
	w = iSkillTextCoord[iTextType*4+2]/fTexWidth;
	h = iSkillTextCoord[iTextType*4+3]/fTexHeight;

	pkUV->x = x+w;
	pkUV->y = y+h;
	pkUV++;

	pkUV->x = x+w;
	pkUV->y = y;
	pkUV++;

	pkUV->x = x;
	pkUV->y = y+h;
	pkUV++;

	pkUV->x = x;
	pkUV->y = y;
	pkGeomData->MarkAsChanged(NiGeometryData::TEXTURE_MASK);


	pkGeomData = pkUpDownGeom->GetModelData();
	iVertexCount = pkGeomData->GetVertexCount();
	pkUV = pkGeomData->GetTextures();

	int	iUpDown = 0;
	if(!bIsUP) iUpDown = 1;

	x = iUpDownTextCoord[iUpDown*4+0]/fTexWidth;
	y = iUpDownTextCoord[iUpDown*4+1]/fTexHeight;
	w = iUpDownTextCoord[iUpDown*4+2]/fTexWidth;
	h = iUpDownTextCoord[iUpDown*4+3]/fTexHeight;

	for(int i=0;i<iVertexCount;i++)
	{
		pkUV->x = x+w*pkUV->x;
		pkUV->y = y+h*pkUV->y;
		pkUV++;
	}
	pkGeomData->MarkAsChanged(NiGeometryData::TEXTURE_MASK);

}
void	PgDamageNumMan::SetNumber(NiNode *pNifNode,int iNumber)
{
	static int const iNumTexCoord[] = {
		0,0,50,50,	//	x,y,width,height
		50,0,50,50,
		100,0,50,50,
		150,0,50,50,
		200,0,50,50,
		0,50,50,50,
		50,50,50,50,
		100,50,50,50,
		150,50,50,50,
		200,50,50,50
	};
	NiGeometryData *pGeomData=NULL;
	NiPoint2 *pTexCoord = NULL;

	char strNum[100];
	sprintf_s(strNum,100,"%d",iNumber);
	int iLen = strlen(strNum);

	float fTexWidth = 256,fTexHeight = 128;

	NiGeometry *pGeom[MAX_NUM_DAMAGE] = { NULL, };

	for(int i = 0; i < MAX_NUM_DAMAGE; ++i)
	{
		std::string strPlaneNum = "Plane0";
		char szCount[256];
		_itoa_s(i+1, szCount, 10);
		strPlaneNum += szCount;

		pGeom[i] = (NiGeometry*)pNifNode->GetObjectByName(strPlaneNum.c_str());	//	숫자 첫째 자리
		pGeom[i]->SetAppCulled(true);
	}

	int iVertexCount = 4;
	
	float	tx,ty,tw,th;
	char chNum;

	if(iLen>MAX_NUM_DAMAGE) { iLen = MAX_NUM_DAMAGE; }

	int iCount=0;

	if(5 == iLen || 4 == iLen)		{ iCount = 1;	}
	else if(3 == iLen || 2 == iLen) { iCount = 2;	}
	else if(1 == iLen)				{ iCount = 3;	}
	
	int iLen2 = iCount + iLen;
	
	for(int i = iCount ;i<iLen2;i++)
	{
		chNum = (*(strNum+i-iCount))-'0';

		pGeomData = pGeom[i]->GetModelData();
		pTexCoord = pGeomData->GetTextures();

		tx = static_cast<float>(iNumTexCoord[(chNum*4+0)]);
		ty = static_cast<float>(iNumTexCoord[(chNum*4+1)]);
		tw = static_cast<float>(iNumTexCoord[(chNum*4+2)]);
		th = static_cast<float>(iNumTexCoord[(chNum*4+3)]);

		pTexCoord->x = (tx+tw)/fTexWidth;
		pTexCoord->y = (ty+th)/fTexHeight;

		pTexCoord++;

		pTexCoord->x = (tx+tw)/fTexWidth;
		pTexCoord->y = ty/fTexHeight;

		pTexCoord++;

		pTexCoord->x = tx/fTexWidth;
		pTexCoord->y = (ty+th)/fTexHeight;

		pTexCoord++;

		pTexCoord->x = tx/fTexWidth;
		pTexCoord->y = ty/fTexHeight;

		pGeom[i]->SetAppCulled(false);
		pGeomData->MarkAsChanged(NiGeometryData::TEXTURE_MASK);
	}
}

void PgDamageNumMan::AddNewExpNum(int iNumber,NiPoint3 const &vLoc)
{
	if(!g_pkWorld)
	{
		return;
	}

	if(m_spExpTex == 0) { m_spExpTex = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumExp.dds"); }

	NiNodePtr	spExpNumNif = 0;
	spExpNumNif = g_kNifMan.GetNif("../Data/6_ui/Combo/DamageNum_02.nif");
	if(spExpNumNif == 0) { return; }

	NiNode *pDummy = (NiNode*)spExpNumNif->GetObjectByName("Dummy01");
	
	NiSourceTexturePtr	spTexture = m_spExpTex;
	
	NiGeometry *pGeom[MAX_NUM_DAMAGE] = { NULL, };

	for(int i = 0; i < MAX_NUM_DAMAGE; ++i)
	{
		std::string strPlaneNum = "Plane0";
		char szCount[256];
		_itoa_s(i+1, szCount, 10);
		strPlaneNum += szCount;

		pGeom[i] = (NiGeometry*)spExpNumNif->GetObjectByName(strPlaneNum.c_str());	//	숫자 첫째 자리
		pGeom[i]->GetModelData()->SetConsistency(NiGeometryData::VOLATILE);
		NiTexturingProperty	*pkProperty = pGeom[i]->GetPropertyState()->GetTexturing();
		pkProperty->SetBaseTexture(spTexture);
	}

	char	str[100];
	sprintf_s(str,100,"%d",iNumber);
	int	iLen = strlen(str);
	if(iLen>MAX_NUM_DAMAGE) { iLen = MAX_NUM_DAMAGE; }

	AddDamageNumNode(stDamageNumNode::T_EXP_NUM, spExpNumNif, iNumber, vLoc, g_pkWorld->GetAccumTime(), true, true, vLoc);
}

void	PgDamageNumMan::SetExpNumber(NiNode *pNifNode,int iNumber, bool bExp)
{
	static int const iNumTexCoord[] = {
		0,4,21,21,	//	x,y,width,height 0
		20,4,21,21, // 1
		39,4,21,21, // 2
		59,4,21,21, // 3
		79,4,21,21, // 4
		99,4,21,21, // 5
		0,34,21,21,  // 6
		20,34,21,21, // 7
		39,34,21,21, // 8
		59,34,21,21, // 9
		79,34,49,21, // + exp
		0,65,42,36, // x
		94,34,34,21, // exp

	};

	NiGeometryData *pGeomData;
	NiPoint2 *pTexCoord;

	char strNum[100];
	sprintf_s(strNum,100,"%d",iNumber);
	int iLen = strlen(strNum);

	float fTexWidth = 128,fTexHeight = 100;

	NiGeometry *pGeom[MAX_NUM_DAMAGE] = { NULL, };

	for(int i = 0; i < MAX_NUM_DAMAGE; ++i)
	{
		std::string strPlaneNum = "Plane0";
		char szCount[256];
		_itoa_s(i+1, szCount, 10);
		strPlaneNum += szCount;

		pGeom[i] = (NiGeometry*)pNifNode->GetObjectByName(strPlaneNum.c_str());	//	숫자 첫째 자리
		pGeom[i]->SetAppCulled(true);
	}

	int iVertexCount = 4;
	
	float	tx,ty,tw,th;
	char chNum;

	if(iLen>6) { iLen = 6; }

	int iCount=1;

	if(2 == iLen || 1 == iLen) { iCount = 2; }
	
	int iLen2 = iCount + iLen;
	
	for(int i = iCount ;i<iLen2;i++)
	{
		chNum = (*(strNum+i-iCount))-'0';
		if(0 == iNumber)
		{
			chNum = 11;
		}

		pGeomData = pGeom[i]->GetModelData();
		pTexCoord = pGeomData->GetTextures();

		tx = static_cast<float>(iNumTexCoord[(chNum*4+0)]);
		ty = static_cast<float>(iNumTexCoord[(chNum*4+1)]);
		tw = static_cast<float>(iNumTexCoord[(chNum*4+2)]);
		th = static_cast<float>(iNumTexCoord[(chNum*4+3)]);

		pTexCoord->x = (tx+tw)/fTexWidth;
		pTexCoord->y = (ty+th)/fTexHeight;

		pTexCoord++;

		pTexCoord->x = (tx+tw)/fTexWidth;
		pTexCoord->y = ty/fTexHeight;

		pTexCoord++;

		pTexCoord->x = tx/fTexWidth;
		pTexCoord->y = (ty+th)/fTexHeight;

		pTexCoord++;

		pTexCoord->x = tx/fTexWidth;
		pTexCoord->y = ty/fTexHeight;

		pGeom[i]->SetAppCulled(false);
		pGeomData->MarkAsChanged(NiGeometryData::TEXTURE_MASK);
	}

	pGeom[0]->SetAppCulled(false);

//	if(bExp)
	{
		//! 단한번만 초기화 한다.
//		if(m_sbInitExpVertexSetting) { return; }
//		m_sbInitExpVertexSetting = true;

		pGeomData = pGeom[0]->GetModelData();
		pTexCoord = pGeomData->GetTextures();
		NiPoint3* pkVex1 =  pGeomData->GetVertices();
		NiPoint3* pkVex2 =  pGeomData->GetVertices() + 1;
		NiPoint3* pkVex3 =  pGeomData->GetVertices() + 2;
		NiPoint3* pkVex4 =  pGeomData->GetVertices() + 3;

		if(0 == iNumber)
		{
			pkVex1->x = 4.5f;
			pkVex2->x = 4.5f;
			pkVex3->x = -110.0f;
			pkVex4->x = -110.0f;
		}
		else
		{
			pkVex1->x = 4.5f;
			pkVex2->x = 4.5f;
			pkVex3->x =	-122.5f;
			pkVex4->x = -122.5f;
		}
		
		int iIndex = 10;
		if(0 == iNumber)
		{
			iIndex = 12;
		}

		tx = static_cast<float>(iNumTexCoord[(iIndex*4+0)]);
		ty = static_cast<float>(iNumTexCoord[(iIndex*4+1)]);
		tw = static_cast<float>(iNumTexCoord[(iIndex*4+2)]);
		th = static_cast<float>(iNumTexCoord[(iIndex*4+3)]);

		pTexCoord->x = (tx+tw)/fTexWidth;
		pTexCoord->y = (ty+th)/fTexHeight;

		pTexCoord++;

		pTexCoord->x = (tx+tw)/fTexWidth;
		pTexCoord->y = ty/fTexHeight;

		pTexCoord++;

		pTexCoord->x = tx/fTexWidth;
		pTexCoord->y = (ty+th)/fTexHeight;

		pTexCoord++;

		pTexCoord->x = tx/fTexWidth;
		pTexCoord->y = ty/fTexHeight;

		pGeom[0]->SetAppCulled(false);
		pGeomData->MarkAsChanged(NiGeometryData::TEXTURE_MASK | NiGeometryData::VERTEX_MASK);
	}
}

void	PgDamageNumMan::AddNewHpMp(int iNumber,NiPoint3 const &vLoc, bool bHp)
{
	if(!g_pkWorld) { return; }

	if(m_spHpTex == 0) { m_spHpTex = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumHp.dds"); }
	if(m_spMpTex == 0) { m_spMpTex = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumMp.dds"); }

	NiNodePtr	spHpMpNif = 0;
	spHpMpNif = g_kNifMan.GetNif("../Data/6_ui/Combo/DamageNum_03.nif");
	if(spHpMpNif == 0) { return; }

	NiNode *pDummy = (NiNode*)spHpMpNif->GetObjectByName("Dummy01");
	
	NiSourceTexturePtr	spTexture = NULL;
	if(bHp) { spTexture = m_spHpTex; }
	else	{ spTexture = m_spMpTex; }
	
	NiGeometry *pGeom[MAX_NUM_DAMAGE] = { NULL, };

	for(int i = 0; i < MAX_NUM_DAMAGE; ++i)
	{
		std::string strPlaneNum = "Plane0";
		char szCount[256];
		_itoa_s(i+1, szCount, 10);
		strPlaneNum += szCount;

		pGeom[i] = (NiGeometry*)spHpMpNif->GetObjectByName(strPlaneNum.c_str());	//	숫자 첫째 자리
		pGeom[i]->GetModelData()->SetConsistency(NiGeometryData::VOLATILE);
		NiTexturingProperty	*pkProperty = pGeom[i]->GetPropertyState()->GetTexturing();
		pkProperty->SetBaseTexture(spTexture);
	}

	char	str[100];
	sprintf_s(str,100,"%d",iNumber);
	int	iLen = strlen(str);
	if(iLen>MAX_NUM_DAMAGE) { iLen = MAX_NUM_DAMAGE; }

	AddDamageNumNode(stDamageNumNode::T_HP_OR_MP, spHpMpNif, iNumber, vLoc, g_pkWorld->GetAccumTime(), true, true, vLoc);
}

void	PgDamageNumMan::SetHpMpNumber(NiNode *pNifNode,int iNumber)
{
	static int const iNumTexCoord[] = {
		0,4,18,22,	//	x,y,width,height 0
		21,4,18,22, // 1
		42,4,18,22, // 2
		61,4,18,22, // 3
		81,4,18,22, // 4
		101,4,18,22, // 5
		0,34,18,22,  // 6
		21,34,18,22, // 7
		42,34,18,22, // 8
		61,34,18,22, // 9
		81,34,18,22, // +
	};

	NiGeometryData *pGeomData;
	NiPoint2 *pTexCoord;

	char strNum[100];
	sprintf_s(strNum,100,"%d",iNumber);
	int iLen = strlen(strNum);

	float fTexWidth = 128,fTexHeight = 64;

	NiGeometry *pGeom[7] = { NULL, };

	for(int i = 0; i < MAX_NUM_DAMAGE; ++i)
	{
		std::string strPlaneNum = "Plane0";
		char szCount[256];
		_itoa_s(i+1, szCount, 10);
		strPlaneNum += szCount;

		pGeom[i] = (NiGeometry*)pNifNode->GetObjectByName(strPlaneNum.c_str());	//	숫자 첫째 자리
		pGeom[i]->SetAppCulled(true);
	}

	int iVertexCount = 4;
	
	float	tx,ty,tw,th;
	char chNum;

	if(iLen>6) { iLen = 6; }

	int iCount=1;
	if(5 <= iLen)		{ iCount = 1; }
	else if(4 <= iLen)	{ iCount = 2; }
	else if(1 <= iLen)	{ iCount = 3; }

	int iLen2 = iCount + iLen;
	if(iLen2>7) 
	{
		iLen2 = 6;
		iCount = 1;
		iNumber = 999999; //최대값을 세팅
	}
	
	for(int i = iCount ;i<iLen2;++i)
	{
		chNum = (*(strNum+i-iCount))-'0';

		pGeomData = pGeom[i]->GetModelData();
		pTexCoord = pGeomData->GetTextures();

		tx = static_cast<float>(iNumTexCoord[(chNum*4+0)]);
		ty = static_cast<float>(iNumTexCoord[(chNum*4+1)]);
		tw = static_cast<float>(iNumTexCoord[(chNum*4+2)]);
		th = static_cast<float>(iNumTexCoord[(chNum*4+3)]);

		pTexCoord->x = (tx+tw)/fTexWidth;
		pTexCoord->y = (ty+th)/fTexHeight;

		pTexCoord++;

		pTexCoord->x = (tx+tw)/fTexWidth;
		pTexCoord->y = ty/fTexHeight;

		pTexCoord++;

		pTexCoord->x = tx/fTexWidth;
		pTexCoord->y = (ty+th)/fTexHeight;

		pTexCoord++;

		pTexCoord->x = tx/fTexWidth;
		pTexCoord->y = ty/fTexHeight;

		pGeom[i]->SetAppCulled(false);
		pGeomData->MarkAsChanged(NiGeometryData::TEXTURE_MASK);
	}

	if(iCount < 1) { iCount = 1; }
	// + 표시
	pGeomData = pGeom[iCount - 1]->GetModelData();
	pTexCoord = pGeomData->GetTextures();

	tx = static_cast<float>(iNumTexCoord[(10*4+0)]);
	ty = static_cast<float>(iNumTexCoord[(10*4+1)]);
	tw = static_cast<float>(iNumTexCoord[(10*4+2)]);
	th = static_cast<float>(iNumTexCoord[(10*4+3)]);

	pTexCoord->x = (tx+tw)/fTexWidth;
	pTexCoord->y = (ty+th)/fTexHeight;

	pTexCoord++;

	pTexCoord->x = (tx+tw)/fTexWidth;
	pTexCoord->y = ty/fTexHeight;

	pTexCoord++;

	pTexCoord->x = tx/fTexWidth;
	pTexCoord->y = (ty+th)/fTexHeight;

	pTexCoord++;

	pTexCoord->x = tx/fTexWidth;
	pTexCoord->y = ty/fTexHeight;

	pGeom[iCount - 1]->SetAppCulled(false);
	pGeomData->MarkAsChanged(NiGeometryData::TEXTURE_MASK);
}
void	PgDamageNumMan::SetCriticalNumber(NiNode *pNifNode,int iNumber)
{
	static int const iNumTexCoord[] = {
		0,0,54,54,	//	x,y,width,height
		49,0,54,54,
		98,0,54,54,
		146,0,54,54,
		200,0,54,54,
		0,54,54,54,
		49,54,54,54,
		98,54,54,54,
		146,54,54,54,
		200,54,54,54
	};
	NiGeometryData *pGeomData;
	NiPoint2 *pTexCoord;

	char strNum[100];
	sprintf_s(strNum,100,"%d",iNumber);
	int iLen = strlen(strNum);

	float fTexWidth = 256,fTexHeight = 128;

	NiGeometry *pGeom[MAX_NUM_DAMAGE] = { NULL, };

	for(int i = 0; i < MAX_NUM_DAMAGE; ++i)
	{
		std::string strPlaneNum = "Plane0";
		char szCount[256];
		_itoa_s(i+1, szCount, 10);
		strPlaneNum += szCount;

		pGeom[i] = (NiGeometry*)pNifNode->GetObjectByName(strPlaneNum.c_str());	//	숫자 첫째 자리
		pGeom[i]->SetAppCulled(true);
	}

	int iVertexCount = 4;
	
	float	tx,ty,tw,th;
	char chNum;

	if(iLen>MAX_NUM_DAMAGE) { iLen = MAX_NUM_DAMAGE; }
	
	int iCount=0;
	if(5 == iLen || 4 == iLen)		{ iCount = 1;}
	else if(3 == iLen || 2 == iLen) { iCount = 2;}
	else if(1 == iLen)				{ iCount = 3;}
	
	int iLen2 = iCount + iLen;
	if(iLen2 > MAX_NUM_DAMAGE)
	{
		iLen2 = MAX_NUM_DAMAGE;
		iCount = 0;
	}
	
	for(int i = iCount ;i<iLen2;++i)
	{
		chNum = (*(strNum+i-iCount))-'0';

		pGeomData = pGeom[i]->GetModelData();
		pTexCoord = pGeomData->GetTextures();

		tx = static_cast<float>(iNumTexCoord[(chNum*4+0)]);
		ty = static_cast<float>(iNumTexCoord[(chNum*4+1)]);
		tw = static_cast<float>(iNumTexCoord[(chNum*4+2)]);
		th = static_cast<float>(iNumTexCoord[(chNum*4+3)]);

		pTexCoord->x = (tx+tw)/fTexWidth;
		pTexCoord->y = (ty+th)/fTexHeight;

		pTexCoord++;

		pTexCoord->x = (tx+tw)/fTexWidth;
		pTexCoord->y = ty/fTexHeight;

		pTexCoord++;

		pTexCoord->x = tx/fTexWidth;
		pTexCoord->y = (ty+th)/fTexHeight;

		pTexCoord++;

		pTexCoord->x = tx/fTexWidth;
		pTexCoord->y = ty/fTexHeight;

		pGeom[i]->SetAppCulled(false);
		pGeomData->MarkAsChanged(NiGeometryData::TEXTURE_MASK);
	}
}

void	PgDamageNumMan::AddNewSmallNum(int const iNumber,NiPoint3 const &vLoc,bool const bClampScreen, bool const bCritical, BYTE btColor, int const iEnchantLevel, int const iExceptAbil)
{
	if(!g_pkWorld)
	{
		return;
	}

	//타유저가 때릴때 크리티컬은 따로 구분되어 있지 않음/ 추후 작업이 쉽게 하기 위해 bCritical 변수만 남겨둠
	NiNodePtr	spDamageNumNif = 0;
	if(m_spWhiteNum2 == 0)	{ m_spWhiteNum2 = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumDmgW.dds"); }
	if(m_spGreenTex == 0)	{ m_spGreenTex = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumDmgP.dds"); }
	if(m_spRedNumSmall == 0)	{ m_spRedNumSmall = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumDmgR.dds"); }
	spDamageNumNif = g_kNifMan.GetNif("../Data/6_ui/Combo/DamageNum_01.nif");
	
	if(spDamageNumNif == 0) { return; }

	NiNode *pDummy = (NiNode*)spDamageNumNif->GetObjectByName("Dummy01");
	
	NiSourceTexturePtr	spTexture = NULL;
	if(C_WHITE == btColor)		{ spTexture = m_spWhiteNum2; }
	else if(C_GREEN == btColor) { spTexture = m_spGreenTex; }
	else if(C_RED == btColor )
	{
		spTexture = m_spRedNumSmall;
	}

	NiSourceTexturePtr	spEnchantLevelTex = 0;
	if(0 < iEnchantLevel)
	{
		spEnchantLevelTex = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumDmgVb.tga");
	}
	else if(0 > iEnchantLevel)
	{
		spEnchantLevelTex = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumDmg2.tga");
	}

	NiSourceTexturePtr	spExceptAbilTex = 0; // 어빌에 의해서 예외 처리가 되어야하는 폰트 연출
	// 예외 어빌로 특정한 데미지 폰트를 출력해주어야 하는 경우.
	switch(iExceptAbil)	
	{
	case 1:
		{
			spExceptAbilTex = g_kNifMan.GetTexture("../Data/6_UI/main/mnNumDmgB.tga");
		}break;
	default:
		{
			spExceptAbilTex = 0;
		}break;
	}

	stDamageNumNode::TYPE eType = stDamageNumNode::T_SMALL_NUM;

	if(0 < iExceptAbil)
	{
		eType = stDamageNumNode::T_NUM;
		spTexture = spExceptAbilTex;
	}

	NiGeometry *pGeom[MAX_NUM_DAMAGE] = { NULL, };

	for(int i = 0; i < MAX_NUM_DAMAGE; ++i)
	{
		std::string strPlaneNum = "Plane0";
		char szCount[256];
		_itoa_s(i+1, szCount, 10);
		strPlaneNum += szCount;

		pGeom[i] = (NiGeometry*)spDamageNumNif->GetObjectByName(strPlaneNum.c_str());	//	숫자 첫째 자리
		pGeom[i]->GetModelData()->SetConsistency(NiGeometryData::VOLATILE);
		NiTexturingProperty	*pkProperty = pGeom[i]->GetPropertyState()->GetTexturing();
		pkProperty->SetBaseTexture(spTexture);
	}

	char	str[100];
	sprintf_s(str,100,"%d",iNumber);
	int	iLen = strlen(str);
	if(iLen>MAX_NUM_DAMAGE) { iLen = MAX_NUM_DAMAGE; }
	
	AddDamageNumNode(eType, spDamageNumNif, iNumber, vLoc, g_pkWorld->GetAccumTime(), bClampScreen, true, vLoc);
}

void	PgDamageNumMan::SetSmallNumber(NiNode *pNifNode,int iNumber)
{
	static int const iNumTexCoord[] = {
		0,0,39,39,	//	x,y,width,height
		42,0,39,39,
		81,0,39,39,
		120,0,39,39,
		159,0,39,39,
		0,39,39,39,
		42,39,39,39,
		81,39,39,39,
		120,39,39,39,
		159,39,39,39
	};
	NiGeometryData *pGeomData = NULL;
	NiPoint2 *pTexCoord = NULL;

	char strNum[100];
	sprintf_s(strNum,100,"%d",iNumber);
	int iLen = strlen(strNum);

	float fTexWidth = 256,fTexHeight = 128;

	NiGeometry *pGeom[MAX_NUM_DAMAGE] = { NULL, };

	for(int i = 0; i < MAX_NUM_DAMAGE; ++i)
	{
		std::string strPlaneNum = "Plane0";
		char szCount[256];
		_itoa_s(i+1, szCount, 10);
		strPlaneNum += szCount;

		pGeom[i] = (NiGeometry*)pNifNode->GetObjectByName(strPlaneNum.c_str());	//	숫자 첫째 자리
		pGeom[i]->SetAppCulled(true);
	}

	int iVertexCount = 4;
	
	float	tx,ty,tw,th;
	char chNum;

	if(iLen>MAX_NUM_DAMAGE) { iLen = MAX_NUM_DAMAGE; }

	int iCount=0;

	if(5 == iLen || 4 == iLen)		{ iCount = 1; }
	else if(3 == iLen || 2 == iLen) { iCount = 2; }
	else if(1 == iLen)				{ iCount = 3; }
	
	int iLen2 = iCount + iLen;
	if(iLen2 > MAX_NUM_DAMAGE)
	{
		iLen2 = MAX_NUM_DAMAGE;
		iCount = 0;
	}
	
	for(int i = iCount ;i<iLen2;i++)
	{
		chNum = (*(strNum+i-iCount))-'0';

		pGeomData = pGeom[i]->GetModelData();
		pTexCoord = pGeomData->GetTextures();

		tx = static_cast<float>(iNumTexCoord[(chNum*4+0)]);
		ty = static_cast<float>(iNumTexCoord[(chNum*4+1)]);
		tw = static_cast<float>(iNumTexCoord[(chNum*4+2)]);
		th = static_cast<float>(iNumTexCoord[(chNum*4+3)]);

		pTexCoord->x = (tx+tw)/fTexWidth;
		pTexCoord->y = (ty+th)/fTexHeight;

		pTexCoord++;

		pTexCoord->x = (tx+tw)/fTexWidth;
		pTexCoord->y = ty/fTexHeight;

		pTexCoord++;

		pTexCoord->x = tx/fTexWidth;
		pTexCoord->y = (ty+th)/fTexHeight;

		pTexCoord++;

		pTexCoord->x = tx/fTexWidth;
		pTexCoord->y = ty/fTexHeight;

		pGeom[i]->SetAppCulled(false);
		pGeomData->MarkAsChanged(NiGeometryData::TEXTURE_MASK);
	}
}

void PgDamageNumMan::AddDamageNumNode(stDamageNumNode::TYPE eType, NiNodePtr pkNode, int iNumber, NiPoint3 vStartPos, float fCreateTime, bool bClampScreen, bool bNifUpdate, NiPoint3 const &vTargetPos )
{
	stDamageNumNode	*pNewNode = NiNew stDamageNumNode();
	pNewNode->m_Type = eType;
	pNewNode->m_spNif = pkNode;
	pNewNode->m_iNumber = iNumber;
	pNewNode->m_vStartPos = vStartPos;
	pNewNode->m_fCreateTime = fCreateTime;
	pNewNode->m_bClampScreen = bClampScreen;
	pNewNode->m_vTargetPos = vTargetPos;

	//숫자가 동시에 뜰 경우 겹치는 루틴 처리
	if(!m_NodeList.empty())
	{
		//젤 마지막 노드를 가져 온다.
		NodeList::iterator itor = --m_NodeList.end();		

		if(itor != m_NodeList.begin())
		{
			//타입이 같고 같은 위치일때만 처리 해준다. 위치가 다르면 동시에 뜨더라도 잘 보인다.
			if((*itor)->m_Type == pNewNode->m_Type && (*itor)->m_vStartPos == pNewNode->m_vStartPos)
			{
				//연속으로 3개 이상 들어오게 되면 마지막으로 들어온 Create타임이 증가한 상태이므로 클 경우가 생긴다.
				if((*itor)->m_fCreateTime >= pNewNode->m_fCreateTime)
				{
					pNewNode->m_fCreateTime = (*itor)->m_fCreateTime + 0.15f;
				}
			}
		}
	}

	m_NodeList.push_back(pNewNode);

	if(bNifUpdate)
	{
		pkNode->UpdateProperties();
        NewWare::Scene::ApplyTraversal::Geometry::SetDefaultMaterialNeedsUpdateFlag( pkNode, false );
	}
}