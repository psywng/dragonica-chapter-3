#ifndef FREEDOM_DRAGONICA_NETWORK_HANDLEPACKET01_H
#define FREEDOM_DRAGONICA_NETWORK_HANDLEPACKET01_H

extern void	Recv_PT_M_C_NFY_STATE_CHANGE2(PgPilot *pkPilot, E_SENDABIL_TARGET eType, SAbilInfo const &rkAbilInfo);
extern void Recv_PT_M_C_NFY_STATE_CHANGE64(PgPilot *pkPilot, SAbilInfo64 const &rkAbilInfo);
extern void Recv_PT_M_C_NFY_QUICKSLOTCHANGE(BM::CPacket &rkPacket);
extern void Recv_PT_M_C_NFY_PET_INVENTORY_ACTION(BM::CPacket &rkPacket);
extern void Recv_PT_M_C_NFY_ITEM_CHANGE(BM::CPacket *pkPacket);
extern void Recv_PT_M_C_NFY_WARN_MESSAGE(BM::CPacket *pkPacket);
extern void Recv_PT_M_C_NFY_WARN_MESSAGE2(BM::CPacket *pkPacket);
extern void Recv_PT_M_C_NFY_WARN_MESSAGE_STR(BM::CPacket *pkPacket);
//extern void Recv_PT_M_C_ANS_GBOXINFO(BM::CPacket *pkPacket);
extern void Recv_PT_M_C_ANS_PICKUPGBOX(BM::CPacket *pkPacket);
extern void Recv_PT_M_C_ANS_STORE_ITEM_LIST(BM::CPacket *pkPacket);
extern void Recv_PT_M_C_NFY_SHINESTONE_MSG(BM::CPacket* pkPacket);
extern void Recv_PT_C_M_ANS_ITEM_PLUS_UPGRADE(BM::CPacket* pkPacket);
extern void Recv_PT_C_M_ANS_ITEM_RARITY_UPGRADE(BM::CPacket* pkPacket);
extern void Recv_PT_M_C_NFY_CHANGE_MONEY(BM::CPacket& rkPacket);
extern void Recv_PT_M_C_NFY_CHANGE_COMBO_COUNT(BM::CPacket* pkPacket);
extern void Recv_PT_M_C_NFY_MAPMOVE_COMPLETE(BM::CPacket& rkPacket);
extern void Recv_PT_M_C_NFY_REMOVE_CHARACTER(BM::CPacket& rkPacket, int const iCallType);
extern void Recv_PT_M_C_NFY_CHANGE_CP(BM::CPacket& rkPacket);
extern void Recv_PT_M_C_NFY_CHANGE_MISSIONSCORE_COUNT(BM::CPacket* rkPacket);
extern void Recv_PT_S_C_NFY_REFRESH_DATA(BM::CPacket& rkPacket);
extern void Revc_PT_M_C_MISSION_ABILITY_DEMAGE(BM::CPacket* rkPacket);
extern void Recv_PT_M_C_ANS_GEN_SOCKET(BM::CPacket* pkPacket);
extern void Recv_PT_M_C_ANS_RESET_MONSTERCARD(BM::CPacket* pkPacket);
extern void Recv_PT_M_C_ANS_SET_MONSTERCARD(BM::CPacket* pkPacket);
extern void Recv_PT_M_C_ANS_OPEN_LOCKED_CHEST(BM::CPacket* pkPacket);
extern void Recv_PT_M_C_ANS_OPEN_GAMBLE(BM::CPacket* pkPacket);
extern void Recv_PT_M_C_ANS_EVENT_ITEM_REWARD(BM::CPacket* pkPacket);
extern void Recv_PT_M_C_ANS_CONVERTITEM(BM::CPacket* pkPacket);
extern void Recv_PT_M_C_NFY_EMPORIA_FUNCTION( PgWorld *pkWorld, BM::CPacket &rkPacket);
extern void Revc_PT_M_C_MISSION_RANK_RESULT_ITEM(BM::CPacket* rkPacket);
extern void Recv_PT_M_C_REQ_HIDDEN_MOVE_CHECK(BM::CPacket &rkPacket);
extern void Recv_PT_M_C_ANS_REMOVE_MONSTERCARD(BM::CPacket* rkPacket);

#endif // FREEDOM_DRAGONICA_NETWORK_HANDLEPACKET01_H