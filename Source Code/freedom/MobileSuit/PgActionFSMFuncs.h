#ifndef FREEDOM_DRAGONICA_RENDER_WORDOBJECT_ACTION_PGACTIONFSMFUNCS_H
#define FREEDOM_DRAGONICA_RENDER_WORDOBJECT_ACTION_PGACTIONFSMFUNCS_H

#include "PgActionFSMFuncMgr.H"

extern	void DoAutoFire(lwActor actor);

class	PgActionFSM_Act_Idle	:	public	PgActionFSM_Base
{

public:
	static	char	const*	GetID()	{	return	"Act_Idle";	}

	bool	OnEnter(lwActor actor,lwAction action)	const;
	bool	OnUpdate(lwActor actor,lwAction action,float fAccumTime,float fFrameTime)	const;
	bool	OnLeave(lwActor actor,lwAction action,bool bCancel)	const;
};

class	PgActionFSM_Act_Walk	:	public	PgActionFSM_Base
{
public:

	static	char	const*	GetID()	{	return	"Act_Walk";	}

	bool	OnEnter(lwActor actor,lwAction action)	const;
	bool	OnUpdate(lwActor actor,lwAction action,float fAccumTime,float fFrameTime)	const;
	bool	OnLeave(lwActor actor,lwAction action,bool bCancel)	const;
	bool	OnEvent(lwActor actor,std::string kTextKey,int iSeqID)	const;
};

class	PgActionFSM_Act_Run	:	public	PgActionFSM_Base
{
public:

	static	char	const*	GetID()	{	return	"Act_Run";	}

	bool	OnCheckCanEnter(lwActor actor,lwAction action)	const;
	bool	OnEnter(lwActor actor,lwAction action)	const;
	bool	OnUpdate(lwActor actor,lwAction action,float fAccumTime,float fFrameTime)	const;
	bool	OnLeave(lwActor actor,lwAction action,bool bCancel)	const;
	bool	OnEvent(lwActor actor,std::string kTextKey,int iSeqID)	const;
	bool	OnCleanUp(lwActor actor,lwAction action)	const;
};

class	PgActionFSM_Act_Dash	:	public	PgActionFSM_Base
{
public:

	static	char	const*	GetID()	{	return	"Act_Dash";	}

	bool	OnCheckCanEnter(lwActor actor,lwAction action)	const;
	bool	OnEnter(lwActor actor,lwAction action)	const;
	bool	OnUpdate(lwActor actor,lwAction action,float fAccumTime,float fFrameTime)	const;
	bool	OnCleanUp(lwActor actor,lwAction action)	const;
	bool	OnLeave(lwActor actor,lwAction action,bool bCancel)	const;
};

class	PgActionFSM_Act_Dash_Jump	:	public	PgActionFSM_Base
{
public:

	static	char	const*	GetID()	{	return	"Act_Dash_Jump";	}

	bool	OnEnter(lwActor actor,lwAction action)	const;
	bool	OnUpdate(lwActor actor,lwAction action,float fAccumTime,float fFrameTime)	const;
	bool	OnCleanUp(lwActor actor,lwAction action)	const;
	bool	OnLeave(lwActor actor,lwAction action,bool bCancel)	const;
};




#endif // FREEDOM_DRAGONICA_RENDER_WORDOBJECT_ACTION_PGACTIONFSMFUNCS_H