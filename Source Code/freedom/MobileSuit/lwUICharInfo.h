#ifndef FREEDOM_DRAGONICA_SCRIPTING_UI_LWUICHARINFO_H
#define FREEDOM_DRAGONICA_SCRIPTING_UI_LWUICHARINFO_H

namespace lwCharInfo
{
	float const GUILD_ICON_SCALE = 0.8125f;
	int const ABIL_INFO_MAX_SLOT = 4;

	bool RegisterWrapper(lua_State *pkState);

	void lwSetCharInfoToUI(lwUIWnd UIWnd, lwGUID Guid);
	void ChangePetActor(BM::GUID const kGuid);
	void lwSetPetInfoToUI(lwUIWnd UIWnd, lwGUID kGuid);
	void lwSetCharAbilInfo(lwUIWnd UIWnd, int iType, char const* szAddon, bool const bIsChanged);
	void lwSetCharInfo(lwUIWnd UIWnd, int const iType);
	lwWString lwViewPlusAbilValue(lwUIWnd UISelf);
	void lwCallInfoDrop(lwUIWnd UIWnd, char const* szAddon, char const* szUIName);
	bool lwViewEquipIconInitialize(lwUIWnd UIWnd, int const iInvType = KUIG_NONE );
	bool lwViewEquipIconInitialize2( XUI::CXUI_Wnd* pkUI, PgInventory *pkInv, int const iInvType );
	void lwChangeInfoTab(lwUIWnd UISelf, int const iTab);
	void lwChangeOtherInfoTab(lwUIWnd UISelf, int const iTab);

	void SetCharInfoToUI(XUI::CXUI_Wnd* pWnd, BM::GUID const& Guid, bool const bIsChanged);
	void SetPetInfoToUI(XUI::CXUI_Wnd* pWnd, BM::GUID const& rkGuid, bool const bIsChanged);
	bool SetDiePetInfoToUI(XUI::CXUI_Wnd* pWnd, BM::GUID const& rkGuid);
	void SetAbilValue(XUI::CXUI_Wnd* pWnd, BM::GUID const& Guid, EAbilType Type, wchar_t const* pText = NULL);
	int CalcPlusAbilValue(CUnit* pkUnit, EAbilType Type);
	void UpdateMyActor();
	int GetBonusStatus(CUnit* pkUnit, EAbilType Type);
}
#endif // FREEDOM_DRAGONICA_SCRIPTING_UI_LWUICHARINFO_H