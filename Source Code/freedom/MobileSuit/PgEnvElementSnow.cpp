#include "stdafx.h"
#include "PgEnvElementSnow.H"
#include "PgParticleMan.H"
#include "PgParticle.H"
#include "PgWorld.H"
#include "PgRenderer.H"
#include "PgRendererUtil.H"
#include "PgEnvElementFactory.H"

std::string	PgEnvElementSnow::m_kSnowParticleID;

PgEnvElementSnow::PgEnvElementSnow()
:m_fBoundRadius(500),m_bActivated(false),m_iWorldParticleSlotID(-1),
m_kPreviousCameraPosition(NiPoint3::ZERO)
{
	LoadSnowParticleID();
}

PgEnvElementSnow::~PgEnvElementSnow()
{
	Deactivate();
	ReleaseParticle();
}
void	PgEnvElementSnow::LoadSnowParticleID()
{
	if(m_kSnowParticleID.empty() == false)
	{
		return;
	}

	m_kSnowParticleID = std::string(lua_tinker::call<char const*>("GetSnowParticleID"));
}
void	PgEnvElementSnow::Update(NiCamera *pkCamera,float fAccumTime,float fFrameTime)	
{
	PgEnvElement::Update(pkCamera,fAccumTime,fFrameTime);

	UpdateParticlePosition(pkCamera);
	UpdateModifier(pkCamera);
}
void	PgEnvElementSnow::UpdateParticlePosition(NiCamera *pkCamera)
{
	if(!m_spSnowParticle)
	{
		return;
	}
	m_spSnowParticle->SetTranslate(pkCamera->GetWorldTranslate());
}
void	PgEnvElementSnow::SetValue(float fIntensity,float fTransitTime,float fAccumTime)
{
	PgEnvElement::SetValue(fIntensity,fTransitTime,fAccumTime);

}
void	PgEnvElementSnow::Activate()
{
	if(GetActivated())
	{
		return;
	}

	SetActivated(true);

	CreateParticle();

	AttachParticleToWorld();
}
void	PgEnvElementSnow::Deactivate()
{
	if(!GetActivated())
	{
		return;
	}

	SetActivated(false);

	DetachParticleFromWorld();
}
void	PgEnvElementSnow::AttachParticleToWorld()
{
	if(!g_pkWorld)
	{
		return;
	}

	m_iWorldParticleSlotID = g_pkWorld->AttachParticle(m_spSnowParticle,NiPoint3::ZERO);
}
void	PgEnvElementSnow::DetachParticleFromWorld()
{
	if(g_pkWorld)
	{
		g_pkWorld->DetachParticle(m_iWorldParticleSlotID);
	}
	m_iWorldParticleSlotID = -1;
}

void	PgEnvElementSnow::SetIntensity(float fIntensity)
{
	PgEnvElement::SetIntensity(fIntensity);

	if(GetIntensity() > 0)
	{
		Activate();
	}
	else
	{
		Deactivate();
	}
}

void	PgEnvElementSnow::CreateParticle()
{
	if(m_spSnowParticle)
	{
		return;
	}

	if(m_kSnowParticleID.empty())
	{
		return;
	}

	ReleaseParticle();

	m_spSnowParticle = NiDynamicCast(NiNode,g_kParticleMan.GetParticle(m_kSnowParticleID.c_str()));
	if(!m_spSnowParticle)
	{
		return;
	}

	PgRendererUtil::RemoveParticleSystemModifierRecursive<NiPSysAgeDeathModifier>(m_spSnowParticle);
	PgRenderer::RunUpParticleSystem(m_spSnowParticle);
	
	ModifyParticleRecursive(m_spSnowParticle);
	

}

void	PgEnvElementSnow::ReleaseParticle()
{
	m_spSnowParticle = 0;
	m_kModifierInfoCont.clear();
}

void	PgEnvElementSnow::ModifyParticleRecursive(NiAVObject	*pkAVObject)
{
	if(!pkAVObject)
	{
		return;
	}

	NiNode	*pkNode = NiDynamicCast(NiNode,pkAVObject);
	if(pkNode)
	{
		int	const	iTotalChild = pkNode->GetArrayCount();
		for(int i=0;i<iTotalChild;++i)
		{
			ModifyParticleRecursive(pkNode->GetAt(i));
		}
	}

	NiParticleSystem	*pkParticleSystem = NiDynamicCast(NiParticleSystem,pkAVObject);
	if(pkParticleSystem)
	{
		ModifyParticleSystem(pkParticleSystem);
	}
}
void	PgEnvElementSnow::ModifyParticleSystem(NiParticleSystem	*pkParticleSystem)
{

	RemoveRedundantModifiers(pkParticleSystem);

	PgRendererUtil::RemoveController<NiPSysEmitterCtlr>(pkParticleSystem);

	PgPSysBoundedPositionModifier*	pkBoundedPositionModifier = 
		AppendBoundedPositionModifier(pkParticleSystem);
	PgPSysColorModifierDecorator	*pkColorModifierDecorator = 
		AppendColorModifierDecorator(pkParticleSystem);

	float	fPositionBoundRadius = GetPositionBoundRadius(pkParticleSystem)/2;
	pkBoundedPositionModifier->SetBoundRadius(fPositionBoundRadius);

	AddModifierInfo(pkBoundedPositionModifier,pkColorModifierDecorator);

}
PgPSysColorModifierDecorator*	PgEnvElementSnow::AppendColorModifierDecorator(NiParticleSystem	*pkParticleSystem)
{
	NiPSysColorModifier	*pkColorModifier = PgRendererUtil::FindParticleSystemModifier<NiPSysColorModifier>(pkParticleSystem);
	PgPSysColorModifierDecorator *pkColorModifierDecorator = NiNew PgPSysColorModifierDecorator(
		"Color Modifier Decorator",
		pkColorModifier);

	if(pkColorModifier)
	{
		pkParticleSystem->RemoveModifier(pkColorModifier);
	}

	pkParticleSystem->AddModifier(pkColorModifierDecorator);

	return	pkColorModifierDecorator;
}

float	PgEnvElementSnow::GetPositionBoundRadius(NiParticleSystem	*pkParticleSystem)
{
	NiPSysBoxEmitter	*pkBoxEmitter = PgRendererUtil::FindParticleSystemModifier<NiPSysBoxEmitter>(pkParticleSystem);
	if(pkBoxEmitter)
	{
		return	pkBoxEmitter->GetEmitterWidth();
	}
	return	0;
}
PgPSysBoundedPositionModifier*	PgEnvElementSnow::AppendBoundedPositionModifier(NiParticleSystem	*pkParticleSystem)
{
	PgPSysBoundedPositionModifier	*pkBoundedPositionModifier = NiNew PgPSysBoundedPositionModifier("Bounded Position Modifier");
	pkParticleSystem->AddModifier(pkBoundedPositionModifier);
	return	pkBoundedPositionModifier;
}

void	PgEnvElementSnow::RemoveRedundantModifiers(NiParticleSystem	*pkParticleSystem)
{
	PgRendererUtil::RemoveParticleSystemModifier<NiPSysAgeDeathModifier>(pkParticleSystem);
	PgRendererUtil::RemoveParticleSystemModifier<NiPSysPositionModifier>(pkParticleSystem);
}

void	PgEnvElementSnow::UpdateModifier(NiCamera *pkCamera)
{

	int	const	iTotal = m_kModifierInfoCont.size();
	for(int i=0;i<iTotal;++i)
	{
		UpdateModifier(pkCamera,m_kModifierInfoCont[i]);
	}

}

void	PgEnvElementSnow::UpdateModifier(NiCamera *pkCamera,stModifierInfo &kModifierInfo)
{
	kModifierInfo.m_spPositionModifier->SetBoundCenter(pkCamera->GetWorldTranslate());
	kModifierInfo.m_spColorModifierDecorator->SetColor(NiColorA(1,1,1,GetIntensity()));
}
