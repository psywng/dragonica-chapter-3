#include "stdafx.h"
#include "PgFxSoundManager.H"

void* PgFxSoundManager::PlayGameSound(char const* szSound, const NiPoint3& pos, bool bLoop, NiPoint2* pFallOff, const NiPoint3& vel)
{
	return	NULL;
}

// Stops a sound.
void PgFxSoundManager::StopGameSound(void* hSound)
{
}

// Pauses a sound.
void PgFxSoundManager::PauseGameSound(void* hSound, bool bPaused)
{
}