#include <stdafx.h>
#include "PgComboAdvisor.h"
#include "PgNifMan.H"
#include "PgPilotMan.H"
#include "PgPilot.H"
#include "PgActor.H"
#include "PgMobileSuit.H"
#include "PgUIScene.H"
#include "lwActor.H"
#include "PgAction.h"

PgComboAdvisor	g_kComboAdvisor;


void	PgComboAdvisor::Destroy()
{
	m_spComboTexture = 0;
	m_spMaterialProperty=0;
	m_spAlphaProperty=0;
	m_spTexturingProperty=0;
	m_spVertexColorProperty=0;
	ResetComboAdvisor();
	m_vIconData.clear();
}
void PgComboAdvisor::Update(float fAccumTime,float fFrameTime)
{
	stComboTreeNode	*pkNode = NULL;
	for(NodeList::iterator itor = m_NodeList.begin(); itor != m_NodeList.end();)
	{
		pkNode = *itor;
		UpdateComboNode(pkNode,fAccumTime,fFrameTime);

		if(pkNode->m_fStartScale == 0.0f)
		{
			SAFE_DELETE(pkNode);
			itor = m_NodeList.erase(itor);
			continue;
		}
		itor++;
	}

	if(pkNode)
	{
		stComboTreeNode	*pkChild = NULL;
		for(int i=0;i<pkNode->m_iChildCount;i++)
		{
			pkChild = pkNode->m_pkChilds[i];
			UpdateComboNode(pkChild,fAccumTime,fFrameTime);		
		}
	}

}
void	PgComboAdvisor::UpdateComboNode(stComboTreeNode *pkNode,float fAccumTime,float fFrameTime)
{
	if(pkNode->m_State == CTNS_NEXT)
	{
		NiColorA	kColor;
		if(pkNode->m_spScreenElement[0])
		{
			pkNode->m_spScreenElement[0]->GetColor(0,0,kColor);
			if(kColor.a == 0.5)
				kColor.a = 1;
			else
				kColor.a = 0.5;

			pkNode->m_spScreenElement[0]->SetColors(0,kColor,kColor,kColor,kColor);
			pkNode->m_spScreenElement[1]->SetColors(0,kColor,kColor,kColor,kColor);

			pkNode->m_spScreenElement[0]->GetModelData()->MarkAsChanged(NiGeometryData::COLOR_MASK);
			pkNode->m_spScreenElement[1]->GetModelData()->MarkAsChanged(NiGeometryData::COLOR_MASK);

			pkNode->m_spScreenElement[0]->Update(0);
			pkNode->m_spScreenElement[1]->Update(0);

			if(pkNode->m_fTargetScale == pkNode->m_fStartScale)
			{
				if(pkNode->m_fStartScale == 0.8)
				{
					pkNode->m_fStartScale = 0.8f;
					pkNode->m_fTargetScale = 1.2f;
				}
				else
				{
					pkNode->m_fStartScale = 1.2f;
					pkNode->m_fTargetScale = 0.8f;
				}

				pkNode->m_fScaleStartTime = fAccumTime;
			}
		}


	}
	if(pkNode->m_State == CTNS_PASSED)
	{
		float	fElapsedTime = fAccumTime - pkNode->m_fCreationTime;
		if(pkNode->m_iChildCount>0 && fElapsedTime>1)
		{
			pkNode->ClearChilds();
		}

		NiColorA kColor;
		pkNode->m_spScreenElement[0]->GetColor(0,0,kColor);
		if(fElapsedTime>0.5 && kColor.a == 1)
		{
			kColor.a = 0.5;

			pkNode->m_spScreenElement[0]->SetColors(0,kColor,kColor,kColor,kColor);
			pkNode->m_spScreenElement[1]->SetColors(0,kColor,kColor,kColor,kColor);

			pkNode->m_spScreenElement[0]->GetModelData()->MarkAsChanged(NiGeometryData::COLOR_MASK);
			pkNode->m_spScreenElement[1]->GetModelData()->MarkAsChanged(NiGeometryData::COLOR_MASK);

			pkNode->m_spScreenElement[0]->Update(0);
			pkNode->m_spScreenElement[1]->Update(0);
		}
	}

	if(pkNode->m_State == CTNS_PASSED && pkNode->m_fTargetScale>0)
	{
		float	fElapsedTime = fAccumTime - pkNode->m_fCreationTime;
		if(fElapsedTime>2)
		{
			pkNode->m_fStartScale = pkNode->m_spScreenElement[0]->GetScale()*1.2f;
			pkNode->m_fTargetScale = 0.0f;
			pkNode->m_fScaleStartTime = fAccumTime;
		}
	}

	if(pkNode->m_fStartScale != pkNode->m_fTargetScale)
	{
		float	fTotalScaleTime = 0.1f;
		float	fElapsedTime = fAccumTime-pkNode->m_fScaleStartTime;
		float	fRate = fElapsedTime/fTotalScaleTime;
		if(fRate>1) fRate = 1;



		float	fNextScale = pkNode->m_fStartScale+(pkNode->m_fTargetScale-pkNode->m_fStartScale)*fRate;

		if(fRate == 1)
		{
			pkNode->m_fStartScale = pkNode->m_fTargetScale;
		}

		if(pkNode->m_spScreenElement[0])
		{

			if(pkNode->m_fTargetScale == 0)
			{
				NiColorA kColor(1,1,1,(1.0f-fRate)*0.5f);

				pkNode->m_spScreenElement[0]->SetColors(0,kColor,kColor,kColor,kColor);
				pkNode->m_spScreenElement[1]->SetColors(0,kColor,kColor,kColor,kColor);

				pkNode->m_spScreenElement[0]->GetModelData()->MarkAsChanged(NiGeometryData::COLOR_MASK);
				pkNode->m_spScreenElement[1]->GetModelData()->MarkAsChanged(NiGeometryData::COLOR_MASK);

			}

			pkNode->m_spScreenElement[0]->SetScale(fNextScale);
			pkNode->m_spScreenElement[1]->SetScale(fNextScale);

			pkNode->m_spScreenElement[0]->Update(0);
			pkNode->m_spScreenElement[1]->Update(0);
		}
	}

}

void PgComboAdvisor::DrawImmediate(PgRenderer *pkRenderer)
{

	PgActor	*pkPlayerActor = g_kPilotMan.GetPlayerActor();
	if(!pkPlayerActor) return;

	bool	bActorLeft = false;
	NiPoint3	kLookingDir = pkPlayerActor->GetLookingDir();
	bActorLeft = ((pkPlayerActor->GetPathNormal().Cross(kLookingDir).z>0) ? true : false);


	stComboTreeNode	*pkNode = NULL;
	for(NodeList::iterator itor = m_NodeList.begin(); itor != m_NodeList.end(); itor++)
	{

		pkNode = *itor;
		PgUIScene::Render_UIObject(pkRenderer,pkNode->m_spScreenElement[pkNode->m_iScreenElementIndex]);

	}

	if(pkNode)
	{
		stComboTreeNode	*pkChild = NULL;
		for(int i=0;i<pkNode->m_iChildCount;i++)
		{
			pkChild = pkNode->m_pkChilds[i];

			int	iScreenElementIndex = 0;
			if (bActorLeft == false) iScreenElementIndex = 1;

			PgUIScene::Render_UIObject(pkRenderer,pkChild->m_spScreenElement[iScreenElementIndex]);
		}
	}
}

void	PgComboAdvisor::AddNextAction(char const* strActionID)
{
	if(m_NodeList.size() == 0) return;
	if( strlen(strActionID) == 0 || strActionID == NULL) return;

	//	캐릭터 레벨이 10이상이면 이 기능을 사용하지 않는다.
	PgPilot	*pkPlayer = g_kPilotMan.GetPlayerPilot();
	if(pkPlayer)
	{
		if(pkPlayer->GetAbil(AT_LEVEL)>=10)	
		{
			return;
		}
	}

	PgActor	*pkPlayerActor = g_kPilotMan.GetPlayerActor();
	if(!pkPlayerActor) return;

	if(PgAction::CheckCanEnter(pkPlayerActor,strActionID,false) == false)
	{
		return;
	}

	stComboTreeNode	*pkLastNode = m_NodeList.back();

	if(pkLastNode->m_iChildCount+1>MAX_COMBO_NODE_CHILD) return;

	NiScreenElementsPtr	spElement[2];

	spElement[0] = CreateScreenElement(std::string(strActionID),"LEFT");
	spElement[1] = CreateScreenElement(std::string(strActionID),"RIGHT");

	if(spElement[0] == NULL || spElement[1] == NULL) return;

	stComboTreeNode *pkNewNode = new stComboTreeNode();

	pkNewNode->m_pkParent = pkLastNode;
	pkNewNode->m_kActionID = std::string(strActionID);
	pkNewNode->m_iChildIndex = pkLastNode->m_iChildCount;
	pkNewNode->m_spScreenElement[0] = spElement[0];
	pkNewNode->m_spScreenElement[1] = spElement[1];
	pkNewNode->m_fCreationTime = g_pkApp->GetAccumTime();

	pkLastNode->m_pkChilds[pkLastNode->m_iChildCount++] = pkNewNode;

	stComboTreeNode	*pkChild = NULL;
	for(int i=0;i<pkLastNode->m_iChildCount;i++)
	{
		pkChild = pkLastNode->m_pkChilds[i];
		SetComboNodeState(pkChild,CTNS_NEXT,NULL);		
	}
}
void	PgComboAdvisor::ClearNextAction()
{
	if(m_NodeList.size() == 0) return;

	stComboTreeNode	*pkLastNode = m_NodeList.back();

	pkLastNode->ClearChilds();
}

void	PgComboAdvisor::OnNewActionEnter(char const* strActionID)
{

	//	캐릭터 레벨이 10이상이면 이 기능을 사용하지 않는다.
	PgPilot	*pkPlayer = g_kPilotMan.GetPlayerPilot();
	if(pkPlayer)
	{
		if(pkPlayer->GetAbil(AT_LEVEL)>=10)	
		{
			return;
		}
	}

	NiScreenElementsPtr	spElement[2];

	spElement[0] = CreateScreenElement(std::string(strActionID),"LEFT");
	spElement[1] = CreateScreenElement(std::string(strActionID),"RIGHT");

	if(spElement[0] == NULL || spElement[1] == NULL) return;

	ClearNextAction();

	stComboTreeNode	*pkPrevNode = NULL;
	if(m_NodeList.size()>0)
	{
		pkPrevNode = m_NodeList.back();
	}

	stComboTreeNode	*pkNewNode = new stComboTreeNode();

	pkNewNode->m_kActionID = std::string(strActionID);
	pkNewNode->m_spScreenElement[0] = spElement[0];
	pkNewNode->m_spScreenElement[1] = spElement[1];
	pkNewNode->m_fCreationTime = g_pkApp->GetAccumTime();

	SetComboNodeState(pkNewNode,CTNS_PASSED,pkPrevNode);

	m_NodeList.push_back(pkNewNode);

	if(m_NodeList.size() > MAX_COMBO_NODE)
	{
		SAFE_DELETE(*m_NodeList.begin());
		m_NodeList.erase(m_NodeList.begin());
	}

}
void	PgComboAdvisor::ResetComboAdvisor()
{
	for(NodeList::iterator itor = m_NodeList.begin(); itor != m_NodeList.end(); itor++)
	{
		SAFE_DELETE(*itor);
	}
	m_NodeList.clear();
}

void	PgComboAdvisor::SetComboNodeState(stComboTreeNode *pkNode,ComboTreeNodeState kState,stComboTreeNode *pkPrevNode)
{
	float	fScreenWidth = static_cast<float>(NiRenderer::GetRenderer()->GetDefaultBackBuffer()->GetWidth());
	float	fScreenHeight = static_cast<float>(NiRenderer::GetRenderer()->GetDefaultBackBuffer()->GetHeight());

	pkNode->m_State = kState;

	if(kState == CTNS_DISABLED) return;

	NiPoint3	kStartPoint(0.5f,0.8f,0.0f);


	if(kState == CTNS_PASSED )
	{
		pkNode->m_fStartScale = 3.0f;
		pkNode->m_fTargetScale = 1.0f;
		pkNode->m_fScaleStartTime = g_pkApp->GetAccumTime();
	}
	if(kState == CTNS_NEXT)
	{
		pkNode->m_fStartScale = 1.2f;
		pkNode->m_fTargetScale = 0.8f;
		pkNode->m_fScaleStartTime = g_pkApp->GetAccumTime();
	}
	if(kState == CTNS_PASSED)
	{


		PgActor	*pkPlayerActor = g_kPilotMan.GetPlayerActor();
		if(!pkPlayerActor) return;

		bool	bActorLeft = false;
		NiPoint3	kLookingDir = pkPlayerActor->GetLookingDir();
		bActorLeft = ((pkPlayerActor->GetPathNormal().Cross(kLookingDir).z>0) ? true : false);

		if(bActorLeft)
			pkNode->m_iScreenElementIndex = 0;
		else
			pkNode->m_iScreenElementIndex = 1;

		float	fLeft = 0.5f,fTop = 0.8f,fLeftTemp=0.0f,fTopTemp=0.0f;

		if(m_NodeList.size()>0)
		{
			stComboTreeNode *pkCurrentNode = pkNode;

			NodeList::iterator itor = m_NodeList.end();
			while(1)
			{
				itor--;
				
				pkPrevNode = *itor;

				float	fWidth,fHeight;
				float	fWidth2,fHeight2;
				GetComboNodeRect(pkPrevNode,fLeftTemp,fTopTemp,fWidth,fHeight);
				GetComboNodeRect(pkCurrentNode,fLeftTemp,fTopTemp,fWidth2,fHeight2);

				fLeft -= fWidth/2+fWidth2/2+10/fScreenWidth;
				if(pkPrevNode->m_spScreenElement[0])
				{
					pkPrevNode->m_spScreenElement[0]->SetTranslate(NiPoint3(fLeft,fTop,0));
					pkPrevNode->m_spScreenElement[1]->SetTranslate(NiPoint3(fLeft,fTop,0));

					pkPrevNode->m_spScreenElement[0]->Update(0);
					pkPrevNode->m_spScreenElement[1]->Update(0);

				}

				if(itor == m_NodeList.begin()) break;

				pkCurrentNode = pkPrevNode;
			}
		}
	}
	else if(kState == CTNS_NEXT)
	{

		stComboTreeNode	*pkParent = pkNode->m_pkParent;
		if(pkParent && pkParent->m_spScreenElement[0])
		{	

			float	fLeft,fTop,fWidth,fHeight,fWidth2,fHeigh2;

			GetComboNodeRect(pkNode,fLeft,fTop,fWidth2,fHeigh2);

			GetComboNodeRect(pkParent,fLeft,fTop,fWidth,fHeight);

			fLeft +=  fWidth/2.0f + fWidth2/2.0f + 20.0f/fScreenWidth;

			int	iSiblingCount = pkParent->m_iChildCount;
			int	iSibilngIndex = pkNode->m_iChildIndex;

			if (kState == CTNS_NEXT)
				fTop = (fTop+fHeight/2.0f)-iSiblingCount*(32.0f/fScreenHeight)/2.0f+iSibilngIndex*(32.0f/fScreenHeight);

			kStartPoint = NiPoint3(fLeft,fTop,0);
		}

	}

	pkNode->m_spScreenElement[0]->SetTranslate(kStartPoint);
	pkNode->m_spScreenElement[1]->SetTranslate(kStartPoint);

	float	fAlpha = 1;
	if(kState == CTNS_NEXT)	fAlpha = 0.5;

	pkNode->m_spScreenElement[0]->SetColors(0,
		NiColorA(1,1,1,fAlpha),
		NiColorA(1,1,1,fAlpha),
		NiColorA(1,1,1,fAlpha),
		NiColorA(1,1,1,fAlpha));
	pkNode->m_spScreenElement[1]->SetColors(0,
		NiColorA(1,1,1,fAlpha),
		NiColorA(1,1,1,fAlpha),
		NiColorA(1,1,1,fAlpha),
		NiColorA(1,1,1,fAlpha));

	pkNode->m_spScreenElement[0]->GetModelData()->MarkAsChanged(NiGeometryData::COLOR_MASK);
	pkNode->m_spScreenElement[1]->GetModelData()->MarkAsChanged(NiGeometryData::COLOR_MASK);

	pkNode->m_spScreenElement[0]->Update(0);
	pkNode->m_spScreenElement[1]->Update(0);

}
void	PgComboAdvisor::GetComboNodeRect(stComboTreeNode *pkNode,float &fLeft,float &fTop,float &fWidth,float &fHeight)
{
	
	fLeft = pkNode->m_spScreenElement[0]->GetTranslate().x;
	fTop = pkNode->m_spScreenElement[0]->GetTranslate().y;
	
	
	float	fLeftTemp,fTopTemp;
	pkNode->m_spScreenElement[0]->GetRectangle(0,fLeftTemp,fTopTemp,fWidth,fHeight);

}
/*
void	PgComboAdvisor::OnNewActionEnter(char const* strActionID,stComboTreeNode *pkNode,bool bIsRoot)
{

	if(bIsRoot || pkNode->m_State == CTNS_PASSED)
	{
		for(int i=0;i<pkNode->m_iChildCount;i++)
		{
			OnNewActionEnter(strActionID,pkNode->m_pkChilds+i,false);
		}
		return;
	}

	if(pkNode->m_State == CTNS_NEXT || pkNode->m_State == CTNS_DISABLED)
	{
		if(pkNode->m_kActionID == std::string(strActionID))
		{

			if(pkNode->m_pkParent && pkNode->m_pkParent->m_State == CTNS_PASSED)
			{
				int iChildCount = pkNode->m_pkParent->m_iChildCount;
				for(int i=0;i<iChildCount;i++)
				{
					if(i == pkNode->m_iChildIndex) continue;

					if(pkNode->m_State == CTNS_DISABLED && (pkNode->m_pkParent->m_pkChilds+i)->m_State == CTNS_PASSED) 
					{
						SetComboNodeState(pkNode,CTNS_DISABLED);
						return;
					}
				}
			}

			SetComboNodeState(pkNode,CTNS_PASSED);

			for(int i=0;i<pkNode->m_iChildCount;i++)
			{
				OnNewActionEnter(strActionID,pkNode->m_pkChilds+i,false);
			}
			return;
		}

		if(pkNode->m_pkParent && pkNode->m_pkParent->m_State == CTNS_PASSED)
		{
			int iChildCount = pkNode->m_pkParent->m_iChildCount;
			for(int i=0;i<iChildCount;i++)
			{
				if(i == pkNode->m_iChildIndex) continue;

				if((pkNode->m_pkParent->m_pkChilds+i)->m_State == CTNS_PASSED) 
				{
					SetComboNodeState(pkNode,CTNS_DISABLED);
					return;
				}
			}

			SetComboNodeState(pkNode,CTNS_NEXT);
		}
	}

}
*/
/*
void	PgComboAdvisor::ResetComboAdvisor(stComboTreeNode *pkNode)
{
	SetComboNodeState(pkNode,CTNS_DISABLED,NULL);

	for(int i=0;i<pkNode->m_iChildCount;i++)
	{
		ResetComboAdvisor(pkNode->m_pkChilds+i);
	}
}
*/
void	PgComboAdvisor::Init()
{
	m_spComboTexture = g_kNifMan.GetTexture("../Data/6_UI/main/comboKey.tga");

	m_spMaterialProperty = NiNew NiMaterialProperty();
	m_spAlphaProperty = NiNew NiAlphaProperty();
	m_spAlphaProperty->SetAlphaBlending(true);
	m_spAlphaProperty->SetSrcBlendMode(NiAlphaProperty::ALPHA_SRCALPHA);
	m_spAlphaProperty->SetDestBlendMode(NiAlphaProperty::ALPHA_INVSRCALPHA);

	m_spTexturingProperty = NiNew NiTexturingProperty;
	m_spTexturingProperty->SetApplyMode(NiTexturingProperty::APPLY_MODULATE);
	m_spTexturingProperty->SetBaseTexture(m_spComboTexture);

	m_spVertexColorProperty = NiNew NiVertexColorProperty();
	m_spVertexColorProperty->SetSourceMode(NiVertexColorProperty::SOURCE_EMISSIVE);
	m_spVertexColorProperty->SetLightingMode(NiVertexColorProperty::LIGHTING_E);

	ParseXML();

}
void	PgComboAdvisor::ParseComboTreeIcon(const TiXmlElement *pkElement)
{
	pkElement = pkElement->FirstChildElement();
	while(pkElement)
	{
		char const *pcTagName = pkElement->Value();

		if(strcmp(pcTagName,"ACTION")==0)
		{

			stComboIconData	kIconData;

			const TiXmlAttribute *pkAttr = pkElement->FirstAttribute();

			while(pkAttr)
			{
				char const *pcAttrName = pkAttr->Name();
				char const *pcAttrValue = pkAttr->Value();

				if(strcmp(pcAttrName, "ID") == 0)
				{
					kIconData.m_kActionID = pcAttrValue;
				}
				else if(strcmp(pcAttrName, "DIR") == 0)
				{
					kIconData.m_kDir = pcAttrValue;
				}
				else if(strcmp(pcAttrName, "IconRect") == 0)
				{
					int	x,y,w,h;
					sscanf(pcAttrValue,"%d,%d,%d,%d",&x,&y,&w,&h);
					kIconData.m_kIconRect.left = x;
					kIconData.m_kIconRect.right = x+w;
					kIconData.m_kIconRect.top = y;
					kIconData.m_kIconRect.bottom = y+h;
				}
				else
				{
					PgXmlError1(pkElement, "XmlParse: Incoreect Attr '%s'", pcAttrName);
				}

				pkAttr = pkAttr->Next();
			}

			m_vIconData.push_back(kIconData);

		}

		pkElement = pkElement->NextSiblingElement();
	}
}
/*
void	PgComboAdvisor::ParseComboTreeData(const TiXmlElement *pkElement,stComboTreeNode *pkParentNode)
{
	int	iTotalChild = CountChildNode(pkElement);
	if(iTotalChild == 0) return;

	pkParentNode->m_iChildCount = iTotalChild;
	pkParentNode->m_pkChilds = new stComboTreeNode[iTotalChild];

	int	iChildIndex = 0;

	pkElement = pkElement->FirstChildElement();
	while(pkElement)
	{
		char const *pcTagName = pkElement->Value();


		if(strcmp(pcTagName,"ACTION") == 0)
		{
			const TiXmlAttribute *pkAttr = pkElement->FirstAttribute();

			std::string kActionID;

			while(pkAttr)
			{
				char const *pcAttrName = pkAttr->Name();
				char const *pcAttrValue = pkAttr->Value();

				if(strcmp(pcAttrName, "ID") == 0)
				{
					kActionID = pcAttrValue;
				}
				else
				{
					PgXmlError1(pkElement, "XmlParse: Incoreect Attr '%s'", pcAttrName);
				}

				pkAttr = pkAttr->Next();
			}

			(pkParentNode->m_pkChilds+iChildIndex)->m_pkParent = pkParentNode;
			(pkParentNode->m_pkChilds+iChildIndex)->m_iChildIndex = iChildIndex;
			(pkParentNode->m_pkChilds+iChildIndex)->m_kActionID = kActionID;
			(pkParentNode->m_pkChilds+iChildIndex)->m_spScreenElement[0] = CreateScreenElement(kActionID,"LEFT");
			(pkParentNode->m_pkChilds+iChildIndex)->m_spScreenElement[1] = CreateScreenElement(kActionID,"RIGHT");

			const TiXmlElement *pkChildElement = pkElement->FirstChildElement();
			if(pkChildElement)
			{
				ParseComboTreeData(pkElement,(pkParentNode->m_pkChilds+iChildIndex));
			}

			iChildIndex++;
		}
		

		pkElement = pkElement->NextSiblingElement();
	}
}
*/
int	PgComboAdvisor::CountChildNode(const TiXmlElement *pkElement)
{
	int	iCount = 0;
	pkElement = pkElement->FirstChildElement();
	while(pkElement)
	{
		char const *pcTagName = pkElement->Value();

		iCount++;
		pkElement = pkElement->NextSiblingElement();
	}

	return	iCount;
}
void	PgComboAdvisor::ParseXML()
{
	char	*strXmlPath = "UI/ComboTree.Xml";

	TiXmlDocument kXmlDoc(strXmlPath);
	if(!PgXmlLoader::LoadFile(kXmlDoc, UNI(strXmlPath)))
	{
		PgError1("Parse Failed [%s]", strXmlPath);
		return;
	}

	// Find Root
	const TiXmlElement *pkElement = kXmlDoc.FirstChildElement();

	pkElement = pkElement->FirstChildElement();
	while(pkElement)
	{
		char const *pcTagName = pkElement->Value();

		if(strcmp(pcTagName, "COMBO_TREE_ICON") == 0)
		{
			ParseComboTreeIcon(pkElement);
		}
	/*	else if(strcmp(pcTagName, "COMB_TREE_DATA") == 0)
		{

			const TiXmlAttribute *pkAttr = pkElement->FirstAttribute();

			int	iBaseClassID = -1;
			while(pkAttr)
			{
				char const *pcAttrName = pkAttr->Name();
				char const *pcAttrValue = pkAttr->Value();

				if(strcmp(pcAttrName, "BASE_CLASS_ID") == 0)
				{
					iBaseClassID = atoi(pcAttrValue);
				}
				else
				{
					PgXmlError1(pkElement, "XmlParse: Incoreect Attr '%s'", pcAttrName);
				}

				pkAttr = pkAttr->Next();
			}

			PG_ASSERT_LOG(iBaseClassID>=1);

			m_pkComboTree[iBaseClassID] = new stComboTreeNode();

			ParseComboTreeData(pkElement,m_pkComboTree[iBaseClassID]);
		}*/
		else
		{
			PgXmlError1(pkElement, "XmlParse: Incoreect Tag '%s'", pcTagName);
		}

		pkElement = pkElement->NextSiblingElement();
	}

}

NiScreenElementsPtr	PgComboAdvisor::CreateScreenElement(std::string kActionID,std::string kDir)
{
	float	fScreenWidth = static_cast<float>(NiRenderer::GetRenderer()->GetDefaultBackBuffer()->GetWidth());
	float	fScreenHeight = static_cast<float>(NiRenderer::GetRenderer()->GetDefaultBackBuffer()->GetHeight());

	float	fWidth,fHeight;

	float	fTexWidth = (float)m_spComboTexture->GetWidth();
	float	fTexHeight =(float)m_spComboTexture->GetHeight();

	int	iIconDataSize = m_vIconData.size();
	for(int i=0;i<iIconDataSize;i++)
	{
		if(m_vIconData[i].m_kActionID != kActionID) continue;
		if(m_vIconData[i].m_kDir != "" && m_vIconData[i].m_kDir != kDir) continue;


		NiScreenElementsPtr	spNewScreenElement = NiNew NiScreenElements(
			NiNew NiScreenElementsData(false,true,1)
			);

		fWidth = fabs(((float)(m_vIconData[i].m_kIconRect.right - m_vIconData[i].m_kIconRect.left)))/fScreenWidth;
		fHeight = fabs(((float)(m_vIconData[i].m_kIconRect.bottom - m_vIconData[i].m_kIconRect.top)))/fScreenHeight;

		spNewScreenElement->Insert(4);
		spNewScreenElement->SetRectangle(0,-fWidth/2,-fHeight/2,fWidth,fHeight);
		spNewScreenElement->UpdateBound();
		spNewScreenElement->SetTextures(0,0,
			m_vIconData[i].m_kIconRect.left/fTexWidth
			,m_vIconData[i].m_kIconRect.top/fTexHeight
			,m_vIconData[i].m_kIconRect.right/fTexWidth
			,m_vIconData[i].m_kIconRect.bottom/fTexHeight);
		spNewScreenElement->SetColors(0,NiColorA(1,1,1,1),NiColorA(1,1,1,1),NiColorA(1,1,1,1),NiColorA(1,1,1,1));

		spNewScreenElement->AttachProperty(m_spMaterialProperty);
		spNewScreenElement->AttachProperty(m_spAlphaProperty);
		spNewScreenElement->AttachProperty(m_spVertexColorProperty);
		spNewScreenElement->AttachProperty(m_spTexturingProperty);

		spNewScreenElement->SetConsistency(NiGeometryData::VOLATILE);

  		spNewScreenElement->UpdateEffects();
		spNewScreenElement->UpdateProperties();
		spNewScreenElement->Update(0.0f);

		return	spNewScreenElement;

	}

	return	NULL;

}
