#include "stdafx.h"
#include "Variant/TableDataManager.h"
#include "ServerLib.h"
#include "lwJobSkillLearn.h"
#include "lwUI.h"
#include "PgPilotMan.h"
#include "PgNetwork.h"

namespace lwJobSkillLearn
{	
	//메인
	std::wstring const JOBSKILL_MAIN(L"SFRM_JOB_SKILL_LEARN");
	//직업 선택
	std::wstring const JOB_SELECT_SHADOW(L"SFRM_JOB_SELECT_SHADOW");
	std::wstring const JOB_SELECT(L"BTN_JOB_SELECT");
	std::wstring const JOB_SELECT_ITEMTYPE(L"SFRM_SELECT_JOBTYPE_LIST");
	std::wstring const JOB_SELECT_ITEMTYPE_LIST(L"LST_SELECT_TYPE");
		
	//페이지 넘김
	std::wstring const BTN_PAGE_L(L"BTN_PAGE_L");
	std::wstring const FRM_PAGE_BG(L"FRM_PAGE_BG");
	std::wstring const BTN_PAGE_R(L"BTN_PAGE_R");
	//배울 수 있는 직업 목록
	std::wstring const LEARN_SHADOW(L"SFRM_LEARN_SHADOW");
	std::wstring const LEARN_MAIN(L"FRM_LEARN");
	std::wstring const LEARN_ICON(L"SFRM_ICON");
	std::wstring const LEARN_TEXT(L"SFRM_TEXT");
	std::wstring const LEARN_TEXT_TITLE(L"FRM_TEXT_TITLE");
	std::wstring const LEARN_TEXT_INFO(L"FRM_TEXT_INFO");
	std::wstring const LEARN_SELECT(L"FRM_SELECT");
	//가격
	std::wstring const BILL(L"SFRM_BILL");
	std::wstring const BILL_GOLD(L"SFRM_BILL_GOLD");
	std::wstring const BILL_SILVER(L"SFRM_BILL_SILVER");

	std::wstring const STR_FONT_COLOR_DEFAULT(L"{C=0xFF4D3413/}");
	std::wstring const STR_FONT_COLOR_RED(L"{C=0xFFFF0000/}");

	int const MAX_SHOW_LIST = 5;

	int g_kCurrentPage = 1;
	int g_kCurrentFilterType = 0;

	void RegisterWrapper(lua_State *pkState)
	{
		using namespace lua_tinker;
		def(pkState, "CallJobSkillLearnUI", lwCallJobSkillLearnUI);
		def(pkState, "JobSkillLearn_NextPage", lwNextPage);
		def(pkState, "JobSkillLearn_BeforePage", lwBeforePage);
		def(pkState, "JobSkillLearn_SetPrice", lwSetPrice);
		def(pkState, "BuyJobSkill", lwBuyJobSkill);
		def(pkState, "CloseJobSkillLearnUI", lwCloseJobSkillLearnUI);
		def(pkState, "SetFilterJobType", lwSetFilterJobType);
	}

	void lwCallJobSkillLearnUI()
	{
		if(lwShowList(g_kCurrentPage, g_kCurrentFilterType))
		{
			SetFilterItemType();
		}
	}
	void lwSetFilterJobType(int const iJobType)
	{
		if( !lwShowList(g_kCurrentPage, iJobType) )
		{
			lwClearAll();
			return;
		}
		g_kCurrentFilterType = iJobType;
	}
	bool lwNextPage()
	{
		if(!lwShowList(g_kCurrentPage + 1, g_kCurrentFilterType))
		{
			lwClearAll();
			return false;
		}
		return true;
	}
	bool lwBeforePage()
	{
		if( 1 == g_kCurrentPage)
		{
			++g_kCurrentPage;
		}
		if(!lwShowList(g_kCurrentPage - 1, g_kCurrentFilterType))
		{
			lwClearAll();
			return false;
		}
		return true;
	}
	void SetFilterItemType()
	{//드랍다운 메뉴 설정
		XUI::CXUI_Wnd* pMain = XUIMgr.Get(JOBSKILL_MAIN);
		if(!pMain)
		{
			return;
		}
		XUI::CXUI_Wnd* pSelectShadow = pMain->GetControl(JOB_SELECT_ITEMTYPE);
		if(!pSelectShadow)
		{
			return;
		}
		XUI::CXUI_Wnd* pSelectWnd = pSelectShadow->GetControl(JOB_SELECT_ITEMTYPE_LIST);
		if(!pSelectWnd)
		{
			return;
		}
		XUI::CXUI_List* pSelectList = dynamic_cast<XUI::CXUI_List*>(pSelectWnd);
		if(!pSelectList)
		{
			return;
		}
		pSelectList->ClearList();
		CONT_DEF_JOBSKILL_SKILL const* pkContDefJobSkill;
		g_kTblDataMgr.GetContDef(pkContDefJobSkill);
		if(!pkContDefJobSkill->size())
		{
			return;
		}
		//동일 스킬 반복되지 않도록
		std::list<int> kContSkillType;
		//전체 보기 미리 추가
		XUI::SListItem* pItem = pSelectList->AddItem(L"");
		if(pItem)
		{
			pItem->m_pWnd->Text( TTW(799501) );
			int const iJobSkillType = 0;
			pItem->m_pWnd->SetCustomData(&iJobSkillType, sizeof(iJobSkillType));
			kContSkillType.push_back(iJobSkillType);
		}
		CONT_DEF_JOBSKILL_SKILL::const_iterator iter_Job = pkContDefJobSkill->begin();
		while( iter_Job != pkContDefJobSkill->end() )
		{
			CONT_DEF_JOBSKILL_SKILL::key_type const &rkJobSkillKey = (*iter_Job).first;
			CONT_DEF_JOBSKILL_SKILL::mapped_type const &rkJobSkillInfo = (*iter_Job).second;

			if( JST_1ST_MAIN == rkJobSkillInfo.eJobSkill_Type ||
				JST_2ND_MAIN == rkJobSkillInfo.eJobSkill_Type ||
				JST_3RD_MAIN == rkJobSkillInfo.eJobSkill_Type )
			{//주스킬의 경우만 받아서 Add하기
				bool bAlreadyAdd = false;
				int const iJobSkill_Type = rkJobSkillInfo.eJobSkill_Type;
				std::list<int>::const_iterator find_iter = kContSkillType.begin();
				while( find_iter != kContSkillType.end() )
				{//중복 추가 방지
					if( iJobSkill_Type == (*find_iter) )
					{
						bAlreadyAdd = true;
						break;
					}
					++find_iter;
				}
				if( !bAlreadyAdd )
				{
					BM::vstring kTitle;
					SetFilterText(kTitle, iJobSkill_Type);
					XUI::SListItem* pItem = pSelectList->AddItem(L"");
					if(pItem)
					{
						pItem->m_pWnd->Text(kTitle);
						pItem->m_pWnd->SetCustomData(&iJobSkill_Type, sizeof(iJobSkill_Type));
						kContSkillType.push_back(iJobSkill_Type);
					}
				}
			}
			++iter_Job;
		}
	}

	bool lwShowList(int iPage, int const iFilterType)
	{
		//일단 열자
		XUI::CXUI_Wnd* pMain = XUIMgr.Activate(JOBSKILL_MAIN);
		if(!pMain)
		{
			return false;
		}
		XUI::CXUI_Wnd* pLearnSD = pMain->GetControl(LEARN_SHADOW);
		if(!pLearnSD)
		{
			return false;
		}

		CONT_DEF_JOBSKILL_SKILL kContLearnJobSkill;
		bool bFilterRet = FilterLearnedSkill(kContLearnJobSkill);
		if(!bFilterRet)
		{
			return false;
		}
		bFilterRet = FilterTypeSkill(kContLearnJobSkill, iFilterType);
		if(!bFilterRet)
		{
			return false;
		}
		int iMaxPage = kContLearnJobSkill.size() / MAX_SHOW_LIST;
		if( 0 != (kContLearnJobSkill.size() % MAX_SHOW_LIST) )
		{
			++iMaxPage;
		}
		if(  1 > iPage || 0 == iMaxPage)
		{//설정 페이지가 1보다 작거나 최대 페이지가 0이면 실패(빈거만 나오도록)
			return false;
		}
		if( iMaxPage < iPage )
		{
			iPage = iMaxPage;
		}

		//현재 정렬 타입 텍스트 설정
		XUI::CXUI_Wnd* pFilter = pMain->GetControl(JOB_SELECT_SHADOW);
		if(!pFilter)
		{
			return false;
		}
		BM::vstring kFilterText;
		SetFilterText( kFilterText, iFilterType);
		pFilter->Text(kFilterText);
		//현재 보여지는 페이지 설정
		SetPageUI(iPage, iMaxPage);
		//지정된 페이지의 스킬 목록 설정
		CONT_DEF_JOBSKILL_SKILL::const_iterator itor_Job = kContLearnJobSkill.begin();
		int iPageCount = 0;
		while( iPageCount < ( (iPage-1)*MAX_SHOW_LIST) )
		{//지정된 페이지로 이동
			if( itor_Job == kContLearnJobSkill.end() )
			{ 
				break;
			}
			++itor_Job;
			++iPageCount;
		}
		int iListCount = 0;
		while( iListCount < MAX_SHOW_LIST )
		{
			BM::vstring kSelect = BM::vstring(LEARN_SELECT) + iListCount;
			XUI::CXUI_Wnd* pSelect = pLearnSD->GetControl(kSelect);
			if(pSelect)
			{
				pSelect->Visible(false);
			}
			
			BM::vstring kLearn = BM::vstring(LEARN_MAIN) + iListCount;
			XUI::CXUI_Wnd* pLearn = pLearnSD->GetControl(kLearn);
			if(!pLearn)
			{
				break;
			}
			if( itor_Job == kContLearnJobSkill.end() )
			{ 
				lwClearList(pLearn);
				++iListCount;
				continue;
			}
			CONT_DEF_JOBSKILL_SKILL::key_type const &rkJobSkillNo = (*itor_Job).first;
			CONT_DEF_JOBSKILL_SKILL::mapped_type const &rkJobSkillInfo = (*itor_Job).second;

			XUI::CXUI_Wnd* pIconMain = pLearn->GetControl(LEARN_ICON);
			if(pIconMain)
			{
				XUI::CXUI_Wnd* pIconWnd = pIconMain->GetControl(L"JS_Icon");
				if(pIconWnd)
				{
					XUI::CXUI_Icon* pIcon = dynamic_cast<XUI::CXUI_Icon*>(pIconWnd);
					if(pIcon)
					{
						GET_DEF(CSkillDefMgr, kSkillDefMgr);
						CSkillDef const* pSkillDef;
						pSkillDef = kSkillDefMgr.GetDef(rkJobSkillNo);
						if(pSkillDef)
						{
							SIconInfo kIconInfo = pIcon->IconInfo();
							kIconInfo.iIconKey = rkJobSkillNo;
							kIconInfo.iIconResNumber = pSkillDef->RscNameNo();
							pIcon->SetIconInfo(kIconInfo);
						}
					}
				}
			}
			XUI::CXUI_Wnd* pText = pLearn->GetControl(LEARN_TEXT);
			if(pText)
			{
				XUI::CXUI_Wnd* pTitle = pText->GetControl(LEARN_TEXT_TITLE);
				if(pTitle)
				{
					BM::vstring kTitle;
					if( false == SetTitleText(rkJobSkillNo, kTitle))
					{
						lwClearList(pLearn);
						++iListCount;
						++itor_Job;
						continue;
					}
					if( 0 != kTitle.size())
					{
						pTitle->Text(kTitle);
					}
				}
				XUI::CXUI_Wnd* pInfo = pText->GetControl(LEARN_TEXT_INFO);
				if(pInfo)
				{
					BM::vstring kText;
					bool bCanLearn = true;
					PgPlayer* pPlayer = g_kPilotMan.GetPlayerUnit();
					if(pPlayer)
					{
						if( S_OK != JobSkill_LearnUtil::IsEnableLearnJobSkill(pPlayer, rkJobSkillNo) )
						{
							bCanLearn = false;
						}
					}
					if( false == bCanLearn)
					{
						kText += STR_FONT_COLOR_RED;
					}
					BM::vstring kInfoText;
					SetInfoText(rkJobSkillInfo, kInfoText);
					kText += kInfoText;
					if( false == bCanLearn)
					{
						kText += STR_FONT_COLOR_DEFAULT;
					}
					if( 0 != kText.size() )
					{
						pInfo->Text(kText);
					}
				}
			}
			pLearn->SetCustomData(&rkJobSkillNo, sizeof(rkJobSkillNo) );
			++iListCount;
			++itor_Job;
		}
		g_kCurrentPage = iPage;
		return true;
	}

	void lwClearList(XUI::CXUI_Wnd* pLearn)
	{
		if(pLearn)
		{
			XUI::CXUI_Wnd* pIconMain = pLearn->GetControl(LEARN_ICON);
			if(pIconMain)
			{
				XUI::CXUI_Wnd* pIconWnd = pIconMain->GetControl(L"JS_Icon");
				if(pIconWnd)
				{
					XUI::CXUI_Icon* pIcon = dynamic_cast<XUI::CXUI_Icon*>(pIconWnd);
					if(pIcon)
					{
						SIconInfo kIconInfo = pIcon->IconInfo();
						kIconInfo.iIconKey = 0;
						kIconInfo.iIconResNumber = 0;
						pIcon->SetIconInfo(kIconInfo);
					}
				}
			}
			XUI::CXUI_Wnd* pText = pLearn->GetControl(LEARN_TEXT);
			if(pText)
			{
				XUI::CXUI_Wnd* pTitle = pText->GetControl(LEARN_TEXT_TITLE);
				if(pTitle)
				{
					pTitle->Text(L"");
				}
				XUI::CXUI_Wnd* pInfo = pText->GetControl(LEARN_TEXT_INFO);
				if(pInfo)
				{
					pInfo->Text(L"");
				}
			}
			pLearn->ClearCustomData();
		}
		lua_tinker::call<void>("JobSkillLearn_Select_Clear");
	}

	void lwClearAll()
	{
		XUI::CXUI_Wnd* pMain = XUIMgr.Activate(JOBSKILL_MAIN);
		if(!pMain)
		{
			return;
		}
		XUI::CXUI_Wnd* pViewSD = pMain->GetControl(LEARN_SHADOW);
		if(!pViewSD)
		{
			return;
		}
		PgPlayer* pPlayer = g_kPilotMan.GetPlayerUnit();
		if(!pPlayer)
		{
			return;
		}
		SetPageUI(0,0);
		g_kCurrentPage = 1;

		int iListCount = 0;
		while( iListCount < MAX_SHOW_LIST )
		{
			BM::vstring kLearn = BM::vstring(LEARN_MAIN) + iListCount;
			XUI::CXUI_Wnd* pLearn = pViewSD->GetControl(kLearn);
			if(!pLearn)
			{
				return;
			}
			lwClearList(pLearn);
			++iListCount;
		}
		return;
	}
	
	void SetPageUI(int const iCurrent, int const iMax)
	{
		XUI::CXUI_Wnd* pMain = XUIMgr.Get(JOBSKILL_MAIN);
		if(!pMain)
		{
			return;
		}
		XUI::CXUI_Wnd* pPage = pMain->GetControl(FRM_PAGE_BG);
		if(pPage)
		{
			wchar_t szBuf[200] ={0,};
			wsprintfW(szBuf, TTW(799506).c_str(), iCurrent, iMax);
			pPage->Text(szBuf);
		}
	}

	bool lwSetPrice(int const iJobSkillNo)
	{
		__int64 iCost = 0;
		PgPlayer* pPlayer = g_kPilotMan.GetPlayerUnit();
		if(!pPlayer)
		{
			return false;
		}
		if( 0 != iJobSkillNo )
		{
			CONT_DEF_JOBSKILL_SKILL const* pkContDefJobSkill;
			g_kTblDataMgr.GetContDef(pkContDefJobSkill);
			if(0 == pkContDefJobSkill->size())
			{
				return false;
			}
			CONT_DEF_JOBSKILL_SKILL::const_iterator iter_Skill = pkContDefJobSkill->find(iJobSkillNo);
			if( pkContDefJobSkill->end() == iter_Skill )
			{
				return false;
			}
			CONT_DEF_JOBSKILL_SKILL::mapped_type const &rkJobSkillInfo = (*iter_Skill).second;
			iCost = JobSkill_LearnUtil::GetLearnCost(pPlayer, iJobSkillNo);
			iCost = iCost / 10000;
		}
		XUI::CXUI_Wnd* pMain = XUIMgr.Activate(JOBSKILL_MAIN);
		if(!pMain)
		{
			return false;
		}
		XUI::CXUI_Wnd* pBillWnd = pMain->GetControl(BILL);
		if(!pBillWnd)
		{
			return false;
		}
		XUI::CXUI_Wnd* pGold = pBillWnd->GetControl(BILL_GOLD);
		if(!pGold)
		{
			return false;
		}
		pGold->Text( BM::vstring(iCost) );

		XUI::CXUI_Wnd* pSilver = pBillWnd->GetControl(BILL_SILVER);
		if(!pSilver)
		{
			return false;
		}
		pSilver->Text( L"0");

		return true;
	}

	void lwBuyJobSkill()
	{
		XUI::CXUI_Wnd* pMain = XUIMgr.Activate(JOBSKILL_MAIN);
		if(!pMain)
		{
			return;
		}
		XUI::CXUI_Wnd* pLearnSD = pMain->GetControl(LEARN_SHADOW);
		if(!pLearnSD)
		{
			return;
		}
		
		int iListCount = 0;
		while( iListCount < MAX_SHOW_LIST )
		{
			BM::vstring kSelect = BM::vstring(LEARN_SELECT) + iListCount;
			XUI::CXUI_Wnd* pSelect = pLearnSD->GetControl(kSelect);
			if(!pSelect)
			{
				break;
			}
			if( true == pSelect->Visible())
			{
				BM::vstring kLearn = BM::vstring(LEARN_MAIN) + iListCount;
				XUI::CXUI_Wnd* pLearn = pLearnSD->GetControl(kLearn);
				if(!pLearn)
				{
					break;
				}	
				int iJobSkillNo = 0;
				pLearn->GetCustomData(&iJobSkillNo, sizeof(iJobSkillNo));
				if( !iJobSkillNo )
				{
					return;
				}
				PgPlayer *pPlayer = g_kPilotMan.GetPlayerUnit();
				if( !pPlayer )
				{
					return;
				}
				EJOBSKILL_LEARN_RET const eRet = JobSkill_LearnUtil::IsEnableLearnJobSkill(pPlayer, iJobSkillNo);
				if( JSLR_OK != eRet)
				{
					ReturnResult(eRet, iJobSkillNo);
					return;
				}
				if( 0 == lwSetPrice(iJobSkillNo) )
				{ 
					return;
				}
				BM::vstring kMessage;
				wchar_t const* pName = NULL;
				if(GetDefString(iJobSkillNo,pName))
				{
					wchar_t szBuf[200] ={0,};
					wsprintfW(szBuf, TTW(799516).c_str(), pName);
					kMessage += BM::vstring(szBuf);
				}
				if( 0 != JobSkill_LearnUtil::GetHaveJobSkillTypeCount(pPlayer, iJobSkillNo) )
				{
					EJobSkillType eJobSkillType = JobSkill_Util::GetJobSkillType(iJobSkillNo);
					if( JST_1ST_SUB != eJobSkillType && JST_2ND_SUB != eJobSkillType )
					{
						BM::vstring kAdd = BM::vstring(L"\n") + TTW(799507);
						kMessage = kMessage + kAdd;
					}
				}
				CallYesNoMsgBox(kMessage, pPlayer->GetID() , MBT_BUY_JOBSKILL, iJobSkillNo);
				return;
			}
			++iListCount;
		}
		//선택한거 없음
		lua_tinker::call<void, int, bool>("CommonMsgBoxByTextTable", 799517, true);
	}

	void lwReqBuyJobSkill(int const iJobSkillNo)
	{
		BM::CPacket kPacket(PT_C_M_REQ_LEARN_JOBSKILL);
		kPacket.Push(iJobSkillNo);
		NETWORK_SEND(kPacket);
	}

	bool FilterLearnedSkill(CONT_DEF_JOBSKILL_SKILL& rkContLearnJobSkill)
	{
		PgPlayer* pkPlayer = g_kPilotMan.GetPlayerUnit();
		if(!pkPlayer)
		{
			return false;;
		}
		PgMySkill* pkMySkill = pkPlayer->GetMySkill();
		if(!pkMySkill)
		{
			return false;
		}

		CONT_DEF_JOBSKILL_SKILL const* pkContDefJobSkill;
		g_kTblDataMgr.GetContDef(pkContDefJobSkill);

		if(0 != pkContDefJobSkill->size())
		{//배운 스킬 걸러내기
			CONT_DEF_JOBSKILL_SKILL::const_iterator iter_JobSkill = pkContDefJobSkill->begin();
			for(iter_JobSkill; iter_JobSkill != pkContDefJobSkill->end(); ++iter_JobSkill)
			{
				bool const bLearned = pkMySkill->IsExist(iter_JobSkill->first);
				if(!bLearned)
				{//안배운 스킬일 경우 보여줄 직업스킬 컨테이너에 저장
					rkContLearnJobSkill.insert( std::make_pair((*iter_JobSkill).first, (*iter_JobSkill).second) );
				}
			}
		}
		else
		{
			return false;
		}
		return true;
	}

	bool FilterTypeSkill(CONT_DEF_JOBSKILL_SKILL& rkContLearnJobSkill, int const iJobType)
	{
		if(0 == iJobType)
		{//전부 표시
			return true;
		}
		CONT_DEF_JOBSKILL_SKILL::iterator iter_Job = rkContLearnJobSkill.begin();
		while(iter_Job != rkContLearnJobSkill.end() )
		{
			CONT_DEF_JOBSKILL_SKILL::mapped_type const &rkJobSkillInfo = (*iter_Job).second;
			EJobSkillType const eType = rkJobSkillInfo.eJobSkill_Type;

			switch(iJobType)
			{
			case JST_1ST_MAIN:
				{
					if(JST_1ST_MAIN != eType &&	JST_1ST_SUB != eType)
					{
						rkContLearnJobSkill.erase(iter_Job++);
						continue;
					}
				}break;
			case JST_2ND_MAIN:
				{
					if(JST_2ND_MAIN != eType &&	JST_2ND_SUB != eType)
					{
						rkContLearnJobSkill.erase(iter_Job++);
						continue;
					}
				}break;
			case JST_3RD_MAIN:
				{
					if(JST_3RD_MAIN != eType)
					{
						rkContLearnJobSkill.erase(iter_Job++);
						continue;
					}
				}break;
			}
			++iter_Job;
		}
		return true;
	}

	void SetFilterText(BM::vstring& kText, int const iJobType)
	{
		switch(iJobType)
		{
		case JST_1ST_MAIN:
		case JST_1ST_SUB:
			{
				kText = TTW(799502);
			}break;
		case JST_2ND_MAIN:
		case JST_2ND_SUB:
			{
				kText = TTW(799503);
			}break;
		case JST_3RD_MAIN:
			{
				kText = TTW(799504);
			}break;
		default:
			{
				kText = TTW(799501);
			}break;
		}
	}

	bool SetTitleText(int const iJobSkillNo, BM::vstring& kOutput)
	{
		CONT_DEF_JOBSKILL_SKILL::mapped_type kJobSkillInfo;
		GetJobSkillInfo(kJobSkillInfo, iJobSkillNo);
		GET_DEF(CSkillDefMgr, kSkillDefMgr);
		CSkillDef const* pSkillDef;
		pSkillDef = kSkillDefMgr.GetDef(iJobSkillNo);
		if(!pSkillDef)
		{
			return false;
		}
		wchar_t const* pName = NULL;
		if(!GetDefString(pSkillDef->NameNo(),pName))
		{
			return false;
		}
		wchar_t szBuf[200] ={0,};
		switch(kJobSkillInfo.eJobSkill_Type)
		{//기술 타입별 문구(1차 : [채집기술 ] / 2차 : [가공 기술] / 3차 : [생산 기술]
		case JST_1ST_MAIN:
			{
				wsprintfW(szBuf, TTW(799522).c_str(), TTW(799502).c_str());
				kOutput = BM::vstring(szBuf) + pName;
			}break;
		case JST_1ST_SUB:
			{//보조기술 추가
				wsprintfW(szBuf, TTW(799522).c_str(), TTW(799502).c_str());
				kOutput = BM::vstring(szBuf) + pName + TTW(799515);
			}break;
		case JST_2ND_MAIN:
			{
				wsprintfW(szBuf, TTW(799522).c_str(), TTW(799503).c_str());
				kOutput = BM::vstring(szBuf) + pName;
			}break;
		case JST_2ND_SUB:
			{//보조기술 추가
				wsprintfW(szBuf, TTW(799522).c_str(), TTW(799503).c_str());
				kOutput = BM::vstring(szBuf) + pName + TTW(799515);
			}break;
		case JST_3RD_MAIN:
			{
				wsprintfW(szBuf, TTW(799522).c_str(), TTW(799504).c_str());
				kOutput = BM::vstring(szBuf) + pName;
			}break;
		}
		return true;
	}

	bool SetInfoText(CONT_DEF_JOBSKILL_SKILL::mapped_type const &rkJobSkillInfo, BM::vstring& kOutput)
	{
		PgPlayer* pPlayer = g_kPilotMan.GetPlayerUnit();
		if(!pPlayer)
		{
			return false;
		}
		PgMySkill* pkMySkill = pPlayer->GetMySkill();
		if(!pkMySkill)
		{
			return false;
		}
		//if( JST_2ND_MAIN == rkJobSkillInfo.eJobSkill_Type)
		//{
		//	int const iHaveCount = JobSkill_LearnUtil::GetHaveJobSkillTypeCount(pPlayer, rkJobSkillInfo.eJobSkill_Type);
		//	if( EJobSkillEnum::JSE_LEARN_2ND_MAX <= iHaveCount)
		//	{//최대 갯수 만큼 배웠다.(2차 직업 전용)
		//		kOutput = TTW(799526);
		//		return true;
		//	}
		//}
		if( 0 == rkJobSkillInfo.i01NeedParent_JobSkill_No)
		{//필요기술이 없는 경우, 1차 전직 필요 문구
			kOutput = TTW(799508);
		}
		else
		{//필요스킬이름+'숙련도' + '수치' + 이상
			GET_DEF(CSkillDefMgr, kSkillDefMgr);
			CSkillDef const* pSkillDef;
			pSkillDef = kSkillDefMgr.GetDef(rkJobSkillInfo.i01NeedParent_JobSkill_No);
			if(pSkillDef)
			{
				wchar_t const* pNeedSkillName = NULL;
				GetDefString(pSkillDef->NameNo(),pNeedSkillName);
				wchar_t szBuf[MAX_PATH] ={0,};
				int const iNeedExpertness = rkJobSkillInfo.i01NeedParent_JobSkill_Expertness / JSE_EXPERTNESS_DEVIDE;
				wsprintfW(szBuf, TTW(799509).c_str(), iNeedExpertness);
				kOutput =  BM::vstring(pNeedSkillName) + BM::vstring(szBuf);
			}
		}
		return true;
	}

	void ReturnResult(EJOBSKILL_LEARN_RET const & eRet, int const iJobSkillNo)
	{
		CONT_DEF_JOBSKILL_SKILL::mapped_type kJobSkillInfo;
		GetJobSkillInfo(kJobSkillInfo, iJobSkillNo);
		int const iNeedJobSkillNo = kJobSkillInfo.i01NeedParent_JobSkill_No;
		int const iNeedJobSkillExpertness = kJobSkillInfo.i01NeedParent_JobSkill_Expertness;
		wchar_t const* pJobSkillName = NULL;
		if(!GetDefString(iJobSkillNo, pJobSkillName))
		{
			return;
		}
		wchar_t const* pNeedJobSkillName = NULL;
		if(!GetDefString(iNeedJobSkillNo, pNeedJobSkillName))
		{
			return;
		}
		switch(eRet)
		{
		case JSLR_OK:
			{
				wchar_t szBuf[200] ={0,};
				wsprintfW(szBuf, TTW(799518).c_str(), pJobSkillName);
				::Notice_Show( szBuf, EL_Normal );

				//lua_tinker::call<void, char const*, bool>("CommonMsgBox", MB(szBuf), true);
			}break;
		case JSLR_ALREADY_BUY:
			{
				lua_tinker::call<void, int, bool>("CommonMsgBoxByTextTable", 799519, true);
			}break;
		case JSLR_NOT_CLASS:
			{
				lua_tinker::call<void, int, bool>("CommonMsgBoxByTextTable", 799520, true);
			}break;
		case JSLR_NEED_SKILL:
			{
				lua_tinker::call<void, int, bool>("CommonMsgBoxByTextTable", 799521, true);
			}break;
		case JSLR_NEED_EXPERTNESS:
			{
				wchar_t szBuf[200] ={0,};
				int const iNeedExp = kJobSkillInfo.i01NeedParent_JobSkill_Expertness / JSE_EXPERTNESS_DEVIDE;
				wsprintfW(szBuf, TTW(799505).c_str(), pJobSkillName, pNeedJobSkillName, iNeedExp);
				lua_tinker::call<void, char const*, bool>("CommonMsgBox", MB(szBuf), true);
			}break;
		case JSLR_FULL_COUNT:
			{
				lua_tinker::call<void, int, bool>("CommonMsgBoxByTextTable", 799525, true);
			}break;
		case JSLR_NEED_MONEY:
			{
				lua_tinker::call<void, int, bool>("CommonMsgBoxByTextTable", 968, true);
			}break;
		case JSLR_ERR:
			{
				lua_tinker::call<void, int, bool>("CommonMsgBoxByTextTable", 799523, true);
			}break;
		default:
			{
			}break;
		}
		if( false == lwJobSkillLearn::lwShowList(g_kCurrentPage, g_kCurrentFilterType))
		{
			lwClearAll();
		}
	}

	void GetJobSkillInfo(CONT_DEF_JOBSKILL_SKILL::mapped_type &rkOutput, int const iJobSkillNo)
	{
		CONT_DEF_JOBSKILL_SKILL const* pkContDefJobSkill;
		g_kTblDataMgr.GetContDef(pkContDefJobSkill);
		if(0 != pkContDefJobSkill->size())
		{
			CONT_DEF_JOBSKILL_SKILL::const_iterator iter_Job = pkContDefJobSkill->find(iJobSkillNo);
			if( iter_Job != pkContDefJobSkill->end() )
			{
				rkOutput = (*iter_Job).second;
			}
		}
	}

	bool lwCloseJobSkillLearnUI()
	{
		g_kCurrentPage = 1;
		g_kCurrentFilterType = 0;
		XUIMgr.Close(JOBSKILL_MAIN);
		return true;
	}
}