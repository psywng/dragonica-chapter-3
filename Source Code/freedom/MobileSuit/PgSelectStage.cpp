#include "stdafx.h"
#include "PgSelectStage.h"
#include "PgWorld.h"
#include "PgActor.h"
#include "PgPilotMan.h"
#include "PgPilot.h"
#include "PgBaseItemSet.h"
#include "PgItemMan.h"
#include "ServerLib.h"
#include "PgNetwork.h"
#include "lwPilot.h"
#include "lwActor.h"
#include "Variant/PgTotalObjectMgr.h"
#include "Variant/PgPlayer.h"
#include "Variant/PgControlDefMgr.h"
#include "lwUI.h"
#include "PgNifMan.h"
#include "PgUIScene.h"
#include "PgSoundMan.h"
#include "lwUIQuest.h"
#include "PgCommandMgr.h"

PgSelectStage::PgSelectStage() 
:	m_pkNewActor(NULL)
,	m_iSelectUISelectedSlot(0)
,	m_iSelectUIDrawStartSlot(0)

{
	m_kContBaseItem.clear();
	m_kContSpawnSlotMap.clear();
	m_kContDeleteSlotMap.clear();

	m_kSelectedActorGuid = BM::GUID::NullData();
}

PgSelectStage::~PgSelectStage()
{
	Terminate();
}

void PgSelectStage::Terminate()
{
	CONT_BASE_ITEM::iterator iter = m_kContBaseItem.begin();
	while (iter != m_kContBaseItem.end())
	{
		SAFE_DELETE_NI(*iter);
		++iter;
	}
	m_kContBaseItem.clear();
	m_kContSpawnSlotMap.clear();
	m_kContDeleteSlotMap.clear();
	m_pkNewActor = NULL;
	m_kSelectedActorGuid = BM::GUID::NullData();
}

void PgSelectStage::Clear()
{
	m_kContSpawnSlotMap.clear();
	m_kContDeleteSlotMap.clear();
	m_pkNewActor = NULL;
	m_kSelectedActorGuid = BM::GUID::NullData();
}

void PgSelectStage::SetSelectedActor(int iSelect)
{
	m_iSelectUISelectedSlot = iSelect;
	lua_tinker::call<void>("BeforeSelectUI");
	OnCallCharList();
	lua_tinker::call<void>("OnSelectUI");
}

BM::GUID &PgSelectStage::GetSelectedActor()
{
	return m_kSelectedActorGuid;
}

void PgSelectStage::SetNewActor(PgActor *pkActor)
{
	PG_ASSERT_LOG(pkActor);
	m_pkNewActor = pkActor;
}

PgActor *PgSelectStage::GetNewActor()
{
	return m_pkNewActor;
}

int PgSelectStage::EquipBaseItem(int iType, int iExposedPos)
{
	if(!m_pkNewActor || !m_pkNewActor->GetPilot()) 
	{
		return 0;
	}

	int iGender = m_pkNewActor->GetPilot()->GetAbil(AT_GENDER);

	PgBaseItemSet *pkBaseItemSet = GetBaseItemSet(iType, iGender);
	if(!pkBaseItemSet) { return 0; }

	PgBaseItemSet::PgBaseItem *pkItem = pkBaseItemSet->GetBaseItem(iExposedPos);
	if(!pkItem) { return 0; }

	int iItemNo = pkItem->GetItemNo(0);
	if(!iItemNo) { return 0; }

	if(iType == 0)
	{
		// item type이 0이면 머리 색깔이다.
		m_pkNewActor->SetItemColor(EQUIP_LIMIT_HAIR, iItemNo);
	}
	else
	{
		m_pkNewActor->AddEquipItem(iItemNo, true, PgItemEx::LOAD_TYPE_INSTANT);
	}

	return iItemNo;
}

int PgSelectStage::EquipBaseItemBySeq(int iType, int iSeq)
{
	return EquipBaseItemBySeq(iType, iSeq, m_pkNewActor);
}

int PgSelectStage::EquipBaseItemBySeq(int iType, int iSeq, PgActor* pkActor, bool bDefault, int iFindThisItemFirst)
{
	if(!pkActor || !pkActor->GetPilot()) 
	{
		return 0;
	}

	int const iClass = pkActor->GetPilot()->GetBaseClassID();
	if (iClass < 0)
	{
		return 0;
	}

	int iGender = pkActor->GetPilot()->GetAbil(AT_GENDER);
	int iItemNo = 0;

	if ((iType != 0 && (iFindThisItemFirst == 0 || g_kItemMan.FindItemInCache(iFindThisItemFirst, iGender, iClass) == NULL)) ||
		(iType == 0 && iFindThisItemFirst == 0))
	{
		iGender = pkActor->GetPilot()->GetAbil(AT_GENDER);

		PgBaseItemSet *pkBaseItemSet = GetBaseItemSet(iType, iGender);
		if(!pkBaseItemSet) { return 0; }

		PgBaseItemSet::PgBaseItem *pkItem = pkBaseItemSet->GetItem(iSeq);
		if(!pkItem) { return 0; }

		iItemNo = pkItem->GetItemNo(0);
	}
	else
	{
		iItemNo = iFindThisItemFirst;
		bDefault = true;
	}

	if(!iItemNo) { return 0; }

	if(iType == 0)
	{
		// item type이 0이면 머리 색깔이다.
		pkActor->SetItemColor(EQUIP_LIMIT_HAIR, iItemNo, bDefault);
	}
	else
	{
		pkActor->AddEquipItem(iItemNo, bDefault, PgItemEx::LOAD_TYPE_INSTANT);
	}
	return iItemNo;
}

HRESULT PgSelectStage::SetFiveElement(int Element)
{
	PgActor* pkActor = m_pkNewActor;
	if(!pkActor || !pkActor->GetPilot()) 
	{
		return 0;
	}

	pkActor->GetPilot()->SetAbil(AT_FIVE_ELEMENT_TYPE_AT_BODY, Element);

	return S_OK;
}

int PgSelectStage::GetFiveElement()
{
	PgActor* pkActor = m_pkNewActor;
	if(!pkActor || !pkActor->GetPilot()) 
	{
		return 0;
	}

	return pkActor->GetPilot()->GetAbil(AT_FIVE_ELEMENT_TYPE_AT_BODY);
}

HRESULT PgSelectStage::SetDefaultFiveElement()
{
	PgActor* pkActor = m_pkNewActor;
	if(!pkActor || !pkActor->GetPilot()) 
	{
		return 0;
	}

	SYSTEMTIME st = {0, };
	::GetLocalTime(&st);

	int const defaultElement = st.wDay % 5 + 1;

	pkActor->GetPilot()->SetAbil(AT_FIVE_ELEMENT_TYPE_AT_BODY, defaultElement);

	return S_OK;
}

void PgSelectStage::EquipBaseItemSet(int iClass, int iSet)
{
	EquipBaseItemSet(iClass, iSet, m_pkNewActor);
}

void PgSelectStage::EquipBaseItemSet(int iClass, int iSet, PgActor* pkActor, bool bDefault)
{
	if (pkActor == NULL)
	{
		return;
	}

	PgBaseItemSet *pkBaseItemSet = GetBaseItemSets(true);
	if(!pkBaseItemSet)
	{
		return;
	}

	// 상의, 하의, 장갑, 신발, [기본 무기]를 입힌다.
	PgBaseItemSet::PgBaseItem *pkItems = pkBaseItemSet->GetItemSet(iClass, iSet);
	if(!pkItems)
	{
		return;
	}

	int const iNbItems = pkItems->GetNbItemNo();
	for(int iIndex = 0; iIndex < iNbItems; ++iIndex)
	{
		pkActor->AddEquipItem(pkItems->GetItemNo(iIndex), bDefault, PgItemEx::LOAD_TYPE_INSTANT);
	}
}

// rkPlayerAbil에 있는 것이 현재 ItemCache에 들어있다면 입고, 그게 아니라면 Default 아이템을 입는다.
void PgSelectStage::EquipBaseItemFullSet(PgActor* pkActor, PLAYER_ABIL& rkPlayerAbil)
{
	return;
	if (pkActor == NULL || pkActor->GetPilot() == NULL)
	{
		return;
	}

	if (pkActor->EquipDefaultItem() == true)
		return;

	int iClass = pkActor->GetPilot()->GetBaseClassID();
	if (iClass < 0)
	{
		return;
	}
	int iGender = pkActor->GetPilot()->GetAbil(AT_GENDER);

	PgBaseItemSet *pkBaseItemSet = GetBaseItemSets(true);
	if(!pkBaseItemSet)
	{
		return;
	}

	PgBaseItemSet::PgBaseItem *pkItems = pkBaseItemSet->GetItemSet(iClass, 1);
	if(!pkItems)
	{
		return;
	}

	GET_DEF(CItemDefMgr, kItemDefMgr);

	EquipBaseItemBySeq(4, 1, pkActor, true, rkPlayerAbil.iFace); // face
	EquipBaseItemBySeq(2, 1, pkActor, true, rkPlayerAbil.iHairStyle); // hair
	EquipBaseItemBySeq(0, 1, pkActor, true, rkPlayerAbil.iHairColor); // hair color	

	int iNbItems = pkItems->GetNbItemNo();
	for(int iIndex = 0; iIndex < iNbItems; ++iIndex)
	{
		int iItemNo = pkItems->GetItemNo(iIndex);
		int iFindFirstItemNo = 0;
		if (iItemNo == 0)
			continue;

		CItemDef const *pkItemDef = kItemDefMgr.GetDef(iItemNo);
		if (pkItemDef)
		{
			switch(pkItemDef->GetAbil(AT_EQUIP_LIMIT))
			{
			case EQUIP_LIMIT_SHIRTS:
				iFindFirstItemNo = rkPlayerAbil.iJacket;
				break;
			case EQUIP_LIMIT_PANTS:
				iFindFirstItemNo = rkPlayerAbil.iPants;
				break;
			case EQUIP_LIMIT_BOOTS:
				iFindFirstItemNo = rkPlayerAbil.iShoes;
				break;
			case EQUIP_LIMIT_GLOVE:
				iFindFirstItemNo = rkPlayerAbil.iGloves;
				break;
			default:
				break;
			}
		}
		else
		{	// 없으면 안된다;
		}

		bool bDefault = false;

		if (iFindFirstItemNo != 0 && g_kItemMan.FindItemInCache(iFindFirstItemNo, iGender, iClass) != NULL)
		{
			iItemNo = iFindFirstItemNo;
			bDefault = true;
		}
		pkActor->AddEquipItem(iItemNo, bDefault, PgItemEx::LOAD_TYPE_INSTANT);
	}
	pkActor->EquipDefaultItem(true);
}

void PgSelectStage::PrepareBaseItemFullSet()
{
	PgBaseItemSet *pkBaseItemSet = GetBaseItemSets(true);
	if(!pkBaseItemSet)
	{
		return;
	}

	int const iSetCount = pkBaseItemSet->GetSize();

	for (int i = 0; i < iSetCount; ++i)
	{
		PgBaseItemSet::PgBaseItem* pkItems = pkBaseItemSet->GetItem(i);

		if (pkItems)
		{
			int const iItemCount = pkItems->GetNbItemNo();
			for (int j = 0; j < iItemCount; ++j)
			{
				g_kItemMan.PrepareItem(pkItems->GetItemNo(j));
			}
		}
	}
}

void PgSelectStage::AddBaseItemSetFromTableData()
{
	const CONT_DEFCHARACTER_BASEWEAR* pkContBaseWear = NULL;
	g_kTblDataMgr.GetContDef(pkContBaseWear);

	GET_DEF(CItemDefMgr, kItemDefMgr);
	for(CONT_DEFCHARACTER_BASEWEAR::const_iterator itr = pkContBaseWear->begin();
		itr != pkContBaseWear->end();
		++itr)
	{
		int iItemNo = itr->first;
		const TBL_DEF_CHARACTER_BASEWEAR *pkBaseWear = &itr->second;
		std::wstring kIconPath(pkBaseWear->strIconPath);
		CItemDef const *pkItemDef = kItemDefMgr.GetDef(iItemNo);

		int iItemType = pkBaseWear->iWearType;
		int const iClassNo = pkBaseWear->iClassNo;
		int iGenderLimit = 3;

		if(pkItemDef)
		{
			iItemType = pkItemDef->GetAbil(AT_EQUIP_LIMIT);
			iGenderLimit = pkItemDef->GetAbil(AT_GENDERLIMIT);
		}
		
		//_PgOutputDebugString("ItemNo : %d\t ItemType : %d\t ClassLimit : %d\t GenderLimit : %d\n", iItemNo, iItemType, iClassNo, iGenderLimit);

		PgBaseItemSet *pkBaseItemSet = 0;
		PgBaseItemSet::PgBaseItem *pkBaseItem = 0;
		if(pkBaseWear->iSetNo == 0)
		{
			// 여기서 등록되는 BaseItem은 얼굴, 머리모양,
			// Set No가 0이면 일반 아이템이므로, ItemType으로 BaseItem을 찾아서 추가한다.
			pkBaseItemSet = GetBaseItemSet(iItemType, iGenderLimit);
			if(!pkBaseItemSet)
			{
				pkBaseItemSet = NiNew PgBaseItemSet(iItemType, iGenderLimit);
				m_kContBaseItem.push_back(pkBaseItemSet);
			}
		}
		else if(pkBaseWear->iClassNo != 0)
		{
			// Set No와 Class No가 있으면, 셋트 아이템이므로 SetNo와 Class No로 BaseItem을 찾아서 추가한다.
			pkBaseItemSet = GetBaseItemSets(true);
			if(!pkBaseItemSet)
			{
				pkBaseItemSet = NiNew PgBaseItemSet(iItemType, iGenderLimit, true);
				m_kContBaseItem.push_back(pkBaseItemSet);
			}

			pkBaseItem = pkBaseItemSet->GetItemSet(iClassNo, pkBaseWear->iSetNo);
		}
		else
		{
			// SetNo가 0이 아니고, ClassNo가 0이면 그 아이템은 어디에도 속하지 않는다.
			continue;
		}

		if(!pkBaseItem)
		{
			// SetItem은 모두 Icon Path를 가지고 있어야 한다.
			pkBaseItem = new PgBaseItemSet::PgBaseItem(std::string(MB(kIconPath)), iClassNo, pkBaseWear->iSetNo);
			pkBaseItemSet->AddItem(pkBaseItem);
		}
		
		pkBaseItem->AddItem(iItemNo);
	}
}

PgBaseItemSet *PgSelectStage::AddBaseItemSet(int iType, int iSetCnt, int iNbExposedSlot)
{
	PgBaseItemSet *pkBaseItemSet = NiNew PgBaseItemSet(iType, iSetCnt, iNbExposedSlot);
	m_kContBaseItem.push_back(pkBaseItemSet);
	return pkBaseItemSet;
}

PgBaseItemSet *PgSelectStage::GetBaseItemSetByPos(int iPos)
{
	if((int)m_kContBaseItem.size() <= iPos)
	{
		return NULL;
	}
	return m_kContBaseItem.at(iPos);
}

PgBaseItemSet *PgSelectStage::GetBaseItemSet(int iType, int iGender)
{
	CONT_BASE_ITEM::iterator itr = m_kContBaseItem.begin();
	while(itr != m_kContBaseItem.end())
	{
		PgBaseItemSet *pkItemSet = *itr;
		if(pkItemSet->GetType() == iType)
		{
			// 공용 아이템이 아니면 Gender를 체크하자.
			if(iGender == 3)
			{
				return pkItemSet;
			}
			else if((pkItemSet->GetGenderLimit() & iGender) == iGender)
			{
				return pkItemSet;
			}
		}
		++itr;
	}

	return 0;
}

PgBaseItemSet *PgSelectStage::GetBaseItemSets(int iSetGroup)
{
	// 현재는 Set로 가지는 ItemSet는 단 하나 뿐이다. (상의, 하의, 장갑, 신발 세트)
	CONT_BASE_ITEM::iterator itr = m_kContBaseItem.begin();
	while(itr != m_kContBaseItem.end())
	{
		if((*itr)->GetSetGroup() == iSetGroup)
		{
			return *itr;
		}
		++itr;
	}

	return 0;
}

int PgSelectStage::GetSpawnSlot(BM::GUID const &rkGuid)const
{
	CONT_SPAWN_SLOT_MAP::const_iterator itr = m_kContSpawnSlotMap.begin();

	for(int i = 0; i < m_kContSpawnSlotMap.size(); ++i)
	{
		if( itr != m_kContSpawnSlotMap.end() )
		{
			if( itr->CharacterGuid() == rkGuid )
			{
				return i;
			}
			++itr;
		}
		else
		{
			break;
		}
	}
	
	return -1;
}

void PgSelectStage::RemoveSpawnSlot(BM::GUID const &rkGuid)
{
	CONT_SPAWN_SLOT_MAP::iterator itr = m_kContSpawnSlotMap.begin();

	while(itr != m_kContSpawnSlotMap.end())
	{
		if((*itr).CharacterGuid() == rkGuid)
		{
			m_kContSpawnSlotMap.erase(itr);
			OnCallCharList();
			return;
		}
		++itr;
	}
}

NiPoint3 PgSelectStage::GetSpawnPoint(BM::GUID const &rkGuid)
{
	CONT_SPAWN_SLOT_MAP::iterator itr = m_kContSpawnSlotMap.begin();

	int i = 0;

	while(itr != m_kContSpawnSlotMap.end())
	{
		if((*itr).CharacterGuid() == rkGuid)
		{
			NiPoint3 kPoint3(0, 0, 0);
			if(g_pkWorld)
			{
				g_pkWorld->FindSpawnLoc("char_spawn_1", kPoint3);
			}
			return kPoint3;
		}
		++i;
		++itr;
	}

	return NiPoint3::ZERO;
}

BM::GUID PgSelectStage::GetSpawnActor(int iSpawnSlot)
{
	int i = 0;
	CONT_SPAWN_SLOT_MAP::iterator itr = m_kContSpawnSlotMap.begin();
	while(itr != m_kContSpawnSlotMap.end())
	{
		if(iSpawnSlot == i)
		{
			return itr->CharacterGuid();
		}
		++i;
		++itr;
	}

	return BM::GUID::NullData();
}

bool PgSelectStage::SelectDefaultCharacter()
{
	m_iSelectUISelectedSlot = 0;
	m_iSelectUIDrawStartSlot = 0;
	SetSelectedActor(m_iSelectUISelectedSlot);
	return true;
}

bool PgSelectStage::AddToSlot(SSelectCharKey const &kKey, bool const bDeleteWait)
{
	if( bDeleteWait )
	{
		CONT_SPAWN_SLOT_MAP::iterator Ret_iter = m_kContDeleteSlotMap.insert(m_kContDeleteSlotMap.end(), kKey);
		if( Ret_iter == m_kContDeleteSlotMap.end() )
		{
			return false;
		}
	}
	else
	{
		CONT_SPAWN_SLOT_MAP::iterator Ret_iter = m_kContSpawnSlotMap.insert(m_kContSpawnSlotMap.end(), kKey);
		if( Ret_iter == m_kContSpawnSlotMap.end() )
		{
			return false;
		}
	}

	return true;
}

void PgSelectStage::ResetSpawnSlot()
{
	CONT_SPAWN_SLOT_MAP::iterator itr = m_kContSpawnSlotMap.begin();
	while(itr != m_kContSpawnSlotMap.end())
	{
		BM::GUID kGuid;
		kGuid = itr->CharacterGuid();
		g_pkWorld->RemoveObject(kGuid);
		++itr;
	}
	m_kContSpawnSlotMap.clear();
}

PgActor *PgSelectStage::AddNewActor(int const iClass, int const iGender, char const *pcSpawnLoc)
{
	if(g_pkWorld == NULL)
	{
		PG_ASSERT_LOG(NULL);
		return NULL;
	}

	BM::GUID kTempGuid = BM::GUID::Create();
	CUnit *pkUnit = g_kTotalObjMgr.CreateUnit(UT_PLAYER, kTempGuid);
	
	PgPlayer *pkPlayer = dynamic_cast<PgPlayer *>(pkUnit);
	SPlayerDBData kDBInfo; 
	SPlayerBasicInfo kBasicInfo;
	SPlayerBinaryData kBinaryData;
	
	kDBInfo.guidCharacter = kTempGuid;
	kDBInfo.byGender = iGender;
	kDBInfo.iClass = iClass;
	kDBInfo.wLv = 1;
	kDBInfo.byFiveElementBody = 0;
	kDBInfo.iHP = 10;

	kBasicInfo.iMaxHP = kDBInfo.iHP;
	pkPlayer->Create( BM::GUID::Create(), kDBInfo, kBasicInfo, kBinaryData);

	NiPoint3 kSpawnLoc;
	if(!g_pkWorld->FindSpawnLoc(pcSpawnLoc, kSpawnLoc))
	{
		return 0;
	}
	pkUnit->SetPos(POINT3(kSpawnLoc.x, kSpawnLoc.y, kSpawnLoc.z));

	PgActor *pkActor = dynamic_cast<PgActor *>(g_pkWorld->AddUnit(pkUnit));
	
	return pkActor;
}

int PgSelectStage::GetSpawnSlotCount()
{
	return (int)m_kContSpawnSlotMap.size();
}

void PgSelectStage::OnCallCharList(bool const bCallScript)
{
	XUI::CXUI_Wnd *pkCharList = XUIMgr.Activate(std::wstring(L"FRM_CHAR_LIST"));
	if ( !pkCharList )
	{
		return;
	}
	
	m_kContSpawnSlotMap.sort();
	CONT_SPAWN_SLOT_MAP::iterator slot_itor = m_kContSpawnSlotMap.begin();
	int iNewCharacterEffectLv = -1;

	{
		int index = 0;

		CONT_SPAWN_SLOT_MAP::iterator slot_temp_itr = slot_itor;
		for ( ; slot_temp_itr != m_kContSpawnSlotMap.end() ; ++slot_temp_itr )
		{
			if ( index < m_iSelectUIDrawStartSlot )
			{
				++slot_itor;
				++index;
			}

			if ( 1 < slot_temp_itr->Level() )
			{
				++iNewCharacterEffectLv;
			}
		}

		if( !lwUseLevelRank() )
		{//캐릭터 유도가 비활성화 이면 효과 제거
			iNewCharacterEffectLv = -1;
		}
	}

	for (int i = 0; i < 4; ++i)
	{
		BM::vstring kName(L"FRM_CHAR_ITEM");
		kName += i;
		XUI::CXUI_Wnd *pkCard = pkCharList->GetControl( static_cast<std::wstring>(kName) );
		if(pkCard)
		{
			XUI::CXUI_Wnd *pkSelect = pkCard->GetControl(L"IMG_OUT_LINE");
			XUI::CXUI_Wnd *pkEmptyWnd = pkCard->GetControl(L"FRM_CHAR_ITEM_EMPTY");
			XUI::CXUI_Wnd *pkCardTextWnd = pkCard->GetControl(L"FRM_CHAR_CARD");
			if(!pkSelect || !pkEmptyWnd || !pkCardTextWnd)
			{
				return;
			}

			if( slot_itor == m_kContSpawnSlotMap.end() )
			{
				pkSelect->Visible(false);
				pkEmptyWnd->Visible(true);
				pkCardTextWnd->Visible(false);
				XUI::CXUI_Wnd *pkDisabler = pkCard->GetControl( std::wstring( L"FRM_CARD_DISABLER") );
				if( pkDisabler )
				{
					pkDisabler->Visible(false);
				}

				XUI::CXUI_Wnd *pkEmptyText = pkEmptyWnd->GetControl( std::wstring(L"FRM_TEXT") );
				if ( pkEmptyText )
				{
					bool const bVisible = ( 0 <= iNewCharacterEffectLv );
					pkEmptyText->Visible( bVisible );
					if ( true == bVisible )
					{
						GET_DEF(CEffectDefMgr, kEffectDefMgr);
						CEffectDef const* pkEffectDef = kEffectDefMgr.GetDef( EFFECTNO_CHARACTER_BONUS_EFFECT_BASENO + iNewCharacterEffectLv );
						if ( pkEffectDef )
						{
							int const iPer = pkEffectDef->GetAbil(AT_ADD_EXP_PER) + pkEffectDef->GetAbil(AT_ADD_EXP_CAN_DUPLICATE) + 100;

							BM::vstring vstr( TTW(120000) );
							vstr.Replace( L"#PER#", iPer );
							pkEmptyText->Text( static_cast<std::wstring>(vstr) );
						}
						iNewCharacterEffectLv = -1;
					}
				}

				XUI::CXUI_Wnd *pkExpWnd = pkCard->GetControl( L"FRM_EXP" );
				if ( pkExpWnd )
				{
					pkExpWnd->Visible(false);
				}
			}
			else
			{
				pkSelect->Visible(false);
				pkEmptyWnd->Visible(false);
				pkCardTextWnd->Visible(true);

				if( m_iSelectUISelectedSlot == i )
				{
					pkSelect->Visible(true);
					m_kSelectedActorGuid = slot_itor->CharacterGuid();
				}

				SetCharInfoToCard(pkCardTextWnd, slot_itor->CharacterGuid());
				pkCard->SetCustomData(&slot_itor->CharacterGuid(), sizeof(slot_itor->CharacterGuid()));

				XUI::CXUI_Wnd *pkExpWnd = pkCard->GetControl( L"FRM_EXP" );
				if ( pkExpWnd )
				{
					size_t iLevelRank = 0;

					if( lwUseLevelRank() )
					{
						CONT_SPAWN_SLOT_MAP::iterator slot_temp_itr = m_kContSpawnSlotMap.begin();
						for ( ; slot_temp_itr != m_kContSpawnSlotMap.end() ; ++slot_temp_itr )
						{
							if ( slot_temp_itr->Level() > slot_itor->Level() )
							{
								++iLevelRank;
							}
						}
					}

					if ( iLevelRank )
					{
						pkExpWnd->Visible( true );

						pkExpWnd = pkExpWnd->GetControl( L"IMG_EXP" );
						if ( pkExpWnd )
						{
							pkExpWnd->UVUpdate( iLevelRank );
						}
					}
					else
					{
						pkExpWnd->Visible( false );
					}
				}

				++slot_itor;
			}
		}
	}	

	XUI::CXUI_Wnd *pkTotalSlot = pkCharList->GetControl( L"IMG_TOTAL_SLOT" );
	if (pkTotalSlot)
	{
		BM::vstring str(TTW(600023));
		str+=(int)m_kContSpawnSlotMap.size();
		pkTotalSlot->Text((std::wstring)str);
	}

	IsUpButtonVisible(pkCharList);
	IsDownButtonVisible(pkCharList);
}

void PgSelectStage::OnCallDeleteWait()
{
	XUI::CXUI_List* pParent = dynamic_cast<XUI::CXUI_List*>(XUIMgr.Activate(L"LST_REALM_COMBINE_CHAR_INFO"));
	if( !pParent )
	{
		return;
	}

	if( m_kContDeleteSlotMap.empty() )
	{
		pParent->DeleteAllItem();
		return;
	}

	int const iListCount = pParent->GetTotalItemCount();
	if( iListCount < m_kContDeleteSlotMap.size() )
	{
		for(int i = iListCount; i < m_kContDeleteSlotMap.size(); ++i)
		{
			pParent->AddItem(L"");
		}
	}
	else if( iListCount > m_kContDeleteSlotMap.size() )
	{
		for(int i = iListCount; i > m_kContDeleteSlotMap.size(); --i)
		{
			pParent->DeleteItem(pParent->FirstItem());
		}		
	}

	XUI::SListItem* pItem = pParent->FirstItem();

	CONT_SPAWN_SLOT_MAP::iterator	c_iter = m_kContDeleteSlotMap.begin();
	while( c_iter != m_kContDeleteSlotMap.end() )
	{
		if( pItem && pItem->m_pWnd )
		{
			SetCharInfoToCard(pItem->m_pWnd, c_iter->CharacterGuid());
			pItem->m_pWnd->SetCustomData(&c_iter->CharacterGuid(), sizeof(c_iter->CharacterGuid()));
			pItem = pParent->NextItem(pItem);
		}
		++c_iter;
	}
}

void PgSelectStage::SetCharInfoToCard(XUI::CXUI_Wnd* pWnd, BM::GUID const & rkGuid, wchar_t const* pName)
{
	if(!pWnd)
	{
		return;
	}

	pWnd->Visible(true);

	PgPilot* pkPilot = g_kPilotMan.FindPilot(rkGuid);
	if (pkPilot == NULL)
	{
		return;
	}

	PgPlayer const *pkPlayer = dynamic_cast< PgPlayer const * >(pkPilot->GetUnit());

	if(pkPlayer)
	{
		int const iClass = pkPlayer->GetAbil(AT_CLASS);
		BM::vstring const OPTION(_T("{C=0xFF333333/T=Font_Text/}"));
		BM::vstring kText((pName == NULL)?(pkPlayer->Name()):(pName));	//이름
		kText+=L"\n";
		kText+=OPTION;
		kText+=TTW(224);
		kText+=pkPlayer->GetAbil(AT_LEVEL);
		kText+=L"\n";
		kText+=TTW(30000+iClass);
		kText+=L"\n";
		
		XUI::CXUI_Wnd *pkWndMapText = pWnd->GetControl(std::wstring(_T("FRM_MAP_TEXT")));
		if( NULL != pkWndMapText )
		{//너무 길면 말 줄임표를 쓴다.
			int const iMap = pkPlayer->GetRecentMapNo(GATTR_DEFAULT);
			if( iMap )
			{//맵 이름 
				BM::vstring kMapName(OPTION);
				kMapName+=UNI(lwGetMapNameW(iMap).GetStr());			

				int const iWidth = pkWndMapText->Width();
				Quest::SetCutedTextLimitLength(pkWndMapText, static_cast<std::wstring>(kMapName), _T("..."), iWidth);
			}
			else
			{
				pkWndMapText->Text(L"");
			}
		}
		XUI::CXUI_Wnd *pkBirthText = pWnd->GetControl(std::wstring(_T("FRM_BIRTH_TEXT")));
		if (pkBirthText)
		{//생성날짜.
			BM::PgPackedTime const &kBirth = pkPlayer->BirthDate();
			BM::vstring kBirthText((int)kBirth.Year()+BM::PgPackedTime::BASE_YEAR);
			kBirthText+=L"/";
			kBirthText+=kBirth.Month();
			kBirthText+=L"/";
			kBirthText+=kBirth.Day();
			pkBirthText->Text((std::wstring)(kBirthText));
		}

		std::wstring wstrImg = _T("../Data/6_ui/chaMake/chaCd01.tga");

		switch(pkPilot->GetBaseClassID())
		{
		case 1:	{wstrImg = _T("../Data/6_ui/chaMake/chaCd01.tga");}break;//전사계열
		case 2:	{wstrImg = _T("../Data/6_ui/chaMake/chaCd02.tga");}break;//법사계열
		case 3:	{wstrImg = _T("../Data/6_ui/chaMake/chaCd03.tga");}break;//궁수계열
		case 4:	{wstrImg = _T("../Data/6_ui/chaMake/chaCd04.tga");}break;//도둑계열
		}
		pWnd->DefaultImgName(wstrImg);
		pWnd->Text((std::wstring)(kText));

		XUI::CXUI_Wnd *pkClass = pWnd->GetControl(std::wstring(_T("IMG_CLASS")));
		if (pkClass)
		{
			SUVInfo kInfo = pkClass->UVInfo();
			kInfo.Index = iClass;
			pkClass->UVInfo(kInfo);
		}

		XUI::CXUI_Wnd *pkCard = pWnd->Parent();
		if (pkCard)
		{
			XUI::CXUI_Wnd *pkEmpty = pkCard->GetControl(std::wstring(_T("FRM_CHAR_ITEM_EMPTY")));
			if (pkEmpty)
			{
				pkEmpty->Visible(false);
			}

			XUI::CXUI_Wnd *pkDisabler = pkCard->GetControl(std::wstring(_T("FRM_CARD_DISABLER")));
			if( pkDisabler )
			{
				pkDisabler->SetCustomData(&rkGuid, sizeof(rkGuid));

				BYTE const kState = pkPlayer->GetDBPlayerState();
				pkDisabler->Visible((kState & CIDBS_NeedRename) == CIDBS_NeedRename);
			}
		}
	}
}

bool PgSelectStage::OnUpButton(int const iPlus)
{
	static int const MAX_VIEW_SLOT = 4;

	int const iMax_Character = m_kContSpawnSlotMap.size();
	if( !iMax_Character )
	{
		return false;
	}

	if(m_iSelectUISelectedSlot > 0)
	{
		m_iSelectUISelectedSlot += iPlus;
		if( 0 > m_iSelectUISelectedSlot )
		{
			m_iSelectUISelectedSlot = 0;
		}
	}
	else if(m_iSelectUIDrawStartSlot > 0)
	{
		m_iSelectUIDrawStartSlot += iPlus;
		if( 0 > m_iSelectUIDrawStartSlot )
		{
			m_iSelectUIDrawStartSlot = 0;
		}
	}

	SetSelectedActor(m_iSelectUISelectedSlot);
	IsUpButtonVisible(NULL);
	IsDownButtonVisible(NULL);

	return true;
}

bool PgSelectStage::OnDownButton(int const iPlus)
{
	static int const MAX_VIEW_SLOT = 4;

	int const iMax_Character = m_kContSpawnSlotMap.size();
	if( !iMax_Character )
	{
		return false;
	}

	m_iSelectUISelectedSlot += iPlus;
	if(m_iSelectUISelectedSlot < MAX_VIEW_SLOT)
	{
		if( m_kContSpawnSlotMap.size() <= m_iSelectUISelectedSlot )
		{
			m_iSelectUISelectedSlot = m_kContSpawnSlotMap.size() - 1;
		}
	}
	else
	{
		if(m_kContSpawnSlotMap.size() > MAX_VIEW_SLOT)
		{
			m_iSelectUIDrawStartSlot += m_iSelectUISelectedSlot - (MAX_VIEW_SLOT - 1);
		}
		m_iSelectUISelectedSlot = MAX_VIEW_SLOT - 1;

		if( m_kContSpawnSlotMap.size() <= (m_iSelectUIDrawStartSlot + m_iSelectUISelectedSlot) )
		{
			m_iSelectUIDrawStartSlot = m_kContSpawnSlotMap.size() - MAX_VIEW_SLOT;
		}
	}

	SetSelectedActor(m_iSelectUISelectedSlot);
	IsUpButtonVisible(NULL);
	IsDownButtonVisible(NULL);

	return true;
}

void PgSelectStage::OnClickCombineCharacter(XUI::CXUI_Wnd* pSelf)
{
	if( !pSelf )
	{
		return;
	}

	XUI::CXUI_Wnd* pParent = pSelf->Parent();
	if( !pParent )
	{
		return;
	}

	BM::GUID kGuid;
	if( !pParent->GetCustomDataSize() )
	{
		return;
	}
	pParent->GetCustomData(&kGuid, sizeof(kGuid));

	PgPilot* pkPilot = g_kPilotMan.FindPilot(kGuid);
	if( !pkPilot )
	{
		return;
	}

	CUnit*	pUnit = pkPilot->GetUnit();
	if( !pUnit )
	{
		return;
	}

	PgPlayer* pPlayer = dynamic_cast<PgPlayer*>(pUnit);
	if( !pPlayer )
	{
		return;
	}

	// 뒷 배경의 다른 창이 선택되지 않게 하기 위해 보이지 않는 창을 만듬
	XUI::CXUI_Wnd const* const pkBarrierWnd = XUIMgr.Call( _T("BarrierForDeleteConfirm") );
	if( NULL == pkBarrierWnd ) 
	{
		return;
	}

	XUI::CXUI_Wnd* pRealmRequest = XUIMgr.Call(L"SFRM_COMBINE_REQUEST");
	if( !pRealmRequest )
	{
		return;
	}

	XUI::CXUI_Wnd* pBefore = pRealmRequest->GetControl(L"FRM_CARD_BEFORE");
	XUI::CXUI_Wnd* pAfter = pRealmRequest->GetControl(L"FRM_CARD_AFTER");
	XUI::CXUI_Wnd* pEditBg = pRealmRequest->GetControl(L"FRM_BG_SM");

	XUI::CXUI_Button* pRenameOk = dynamic_cast<XUI::CXUI_Button*>(pRealmRequest->GetControl(L"BTN_RENAME_OK"));
	XUI::CXUI_Button* pOk = dynamic_cast<XUI::CXUI_Button*>(pRealmRequest->GetControl(L"BTN_OK"));
	XUI::CXUI_Edit* pEdit = dynamic_cast<XUI::CXUI_Edit*>(pRealmRequest->GetControl(L"EDT_NAME"));
	if( !pBefore || !pAfter || !pEditBg || !pEdit || !pRenameOk || !pOk )
	{
		return;
	}

	SetCharInfoToCard(pBefore, kGuid);
	pOk->OwnerGuid(kGuid);
	BYTE const kState = pPlayer->GetDBPlayerState();
	if( (kState & CIDBS_NeedRename) == CIDBS_NeedRename )
	{
		pRenameOk->Disable(false);
		pRenameOk->Visible(true);
		pEditBg->Visible(true);
		pEdit->Visible(true);
		pEdit->EditText(L"");
		SetCharInfoToCard(pAfter, kGuid, L" ");
	}
	else
	{
		pRenameOk->Disable(true);
		pRenameOk->Visible(false);
		pEditBg->Visible(false);
		pEdit->Visible(false);
		pOk->Disable(false);
		SetCharInfoToCard(pAfter, kGuid);
		pOk->SetCustomData(pUnit->Name().c_str(), sizeof(std::wstring::value_type)*pUnit->Name().size());
	}
}

void PgSelectStage::OnInputCombineChangeName(XUI::CXUI_Wnd* pSelf)
{
	if( !pSelf )
	{
		return;
	}

	XUI::CXUI_Edit* pEdit = dynamic_cast<XUI::CXUI_Edit*>(pSelf);
	if( !pEdit )
	{
		return;
	}

	std::wstring kInputText = pEdit->EditText();
	
	if( kInputText.empty() )
	{
		pEdit->EditText(L"");
		lua_tinker::call<void, int, bool>("CommonMsgBoxByTextTable", 50520, true);
		return;
	}

	if(g_kClientFS.Filter(kInputText, false, FST_ALL)
		|| !g_kUnicodeFilter.IsCorrect(UFFC_CHARACTER_NAME, kInputText)
		)
	{//욕설 등이 있으면
		pEdit->EditText(L"");
		lua_tinker::call<void, int, bool>("CommonMsgBoxByTextTable", 600037, true);
		return;
	}

	// 캐릭터명 중복 확인 요청
	BM::CPacket kPacket( PT_C_N_REQ_CHECK_CHARACTERNAME_OVERLAP );
	kPacket.Push( kInputText );
	NETWORK_SEND( kPacket );

	XUI::CXUI_Wnd* pParent = pEdit->Parent();
	if( !pParent )
	{
		return;
	}
	
	XUI::CXUI_Wnd* pOk = pParent->GetControl(L"BTN_OK");
	if( !pOk )
	{
		return;
	}

	lwUIWnd(pOk).SetCustomDataAsStr(MB(pEdit->EditText()));
}

void PgSelectStage::OnClickCombineOK(XUI::CXUI_Wnd* pSelf)
{
	if( !pSelf )
	{
		return;
	}

	BM::GUID const kGuid = pSelf->OwnerGuid();
	std::wstring kName = lwUIWnd(pSelf).GetCustomDataAsStr().GetWString();

	BM::CPacket kPacket(PT_C_N_REQ_REALM_MERGE);
	kPacket.Push(kGuid);
	kPacket.Push(kName);
	NETWORK_SEND(kPacket);
	pSelf->Parent()->Close();
}

void PgSelectStage::OnClickDeleteCharacter(XUI::CXUI_Wnd* pSelf)
{
	if( !pSelf )
	{
		return;
	}

	XUI::CXUI_Wnd* pParent = pSelf->Parent();
	if( !pParent )
	{
		return;
	}

	BM::GUID kGuid;
	if( pParent->GetCustomDataSize() )
	{
		pParent->GetCustomData(&kGuid, sizeof(kGuid));
		SetDeleteCharacterInfo(kGuid);
	}
}

bool PgSelectStage::OnDeleteWaitCharacter(BM::GUID const& kGuid)
{
	CONT_SPAWN_SLOT_MAP::iterator iter = m_kContDeleteSlotMap.begin();
	while( iter != m_kContDeleteSlotMap.end() )
	{
		if( kGuid == iter->CharacterGuid() )
		{
			m_kContDeleteSlotMap.erase(iter);
			OnCallDeleteWait();
			return true;
		}
		++iter;
	}
	return false;
}

void PgSelectStage::SetDeleteCharacterInfo(BM::GUID const& kGuid)
{

	// 캐릭터 삭제 UI 호출시, 뒷 배경의 다른 창이 선택되지 않게 하기 위해 보이지 않는 창을 만듬
	XUI::CXUI_Wnd const* const pkBarrierWnd = XUIMgr.Call( _T("BarrierForDeleteConfirm") );
	if( NULL == pkBarrierWnd ) 
	{
		return;
	}
	
	// 캐릭터 삭제 UI를 xml으로 부터 부름
	XUI::CXUI_Wnd* pParentWnd = XUIMgr.Call( _T("DeleteConfirm") );	
	if( NULL == pParentWnd ) 
	{
		return;
	}
	pParentWnd->SetCustomData(&kGuid, sizeof(kGuid));
	
	// 캐릭터 정보를 표시할 창을 찾음
	XUI::CXUI_Wnd* const pChildWnd = pParentWnd->GetControl( _T("SFRM_CHARACTER_INFO_BOX") );
	if( NULL == pChildWnd ) 
	{
		return;
	}
	
	// 선택된 캐릭터 GUID를 얻어옴
	PgPilot const* const pkPilot = g_kPilotMan.FindPilot( kGuid );	
	if( NULL == pkPilot ) 
	{
		return;
	}

	PgPlayer const* const pkPlayer = dynamic_cast< PgPlayer const* >(pkPilot->GetUnit());
	if( NULL == pkPlayer ) 
	{
		return;
	}
	
	// 캐릭터 클래스, 레벨 이름을 얻어와 캐릭터 정보 창에 표시
	int const iClass = pkPlayer->GetAbil(AT_CLASS);
	BM::vstring kText(pkPlayer->Name());
	kText+=_T( "\n" );	
	kText+=pkPlayer->GetAbil( AT_LEVEL );
	kText+=TTW( 224 );
	kText+=_T( " " );
	kText+=TTW( 30000 + iClass );	
	pChildWnd->Text( std::wstring(kText) );
	
	// 삭제 UI가 호출 될때마다 삭제확인 edit 박스의 문장을 지움
	XUI::CXUI_Edit* const pkEdt = dynamic_cast<XUI::CXUI_Edit *>( pParentWnd->GetControl(_T("EDITBOX_CERTIFICATE_STRING")) );
	if( NULL == pkEdt ) 
	{
		return;
	}
	pkEdt->EditText( _T("") );
	
	// 삭제 확인 버튼이 최초 호출시에는 비활성화
	XUI::CXUI_Button* const pkButton = dynamic_cast<XUI::CXUI_Button*>(pParentWnd->GetControl(_T("BTN_DO_DELETE")));
	if( NULL == pkButton ) 
	{
		return;
	}
	pkButton->Enable( false );
}

bool PgSelectStage::MoveSlotDelToSpawn(BM::GUID const& kGuid)
{
	CONT_SPAWN_SLOT_MAP::iterator iter = m_kContDeleteSlotMap.begin();
	while( iter != m_kContDeleteSlotMap.end() )
	{
		if( kGuid == iter->CharacterGuid() )
		{
			break;
		}
		++iter;
	}

	if( iter != m_kContDeleteSlotMap.end() )
	{
		CONT_SPAWN_SLOT_MAP::iterator Spawn_iter = m_kContSpawnSlotMap.insert(m_kContSpawnSlotMap.end(), (*iter));
		if( Spawn_iter != m_kContSpawnSlotMap.end() )
		{
			m_kContDeleteSlotMap.erase(iter);
		}
	}

	OnCallDeleteWait();
	OnCallCharList();
	return false;
}

bool PgSelectStage::CheckDeleteWaitSlot(BM::GUID const& kGuid)
{
	CONT_SPAWN_SLOT_MAP::iterator iter = m_kContDeleteSlotMap.begin();
	while( iter != m_kContDeleteSlotMap.end() )
	{
		if( kGuid == iter->CharacterGuid() )
		{
			return true;
		}
		++iter;
	}
	return false;
}

bool PgSelectStage::CheckNameConflict(BM::GUID const& kGuid)
{
	PgPilot* pkPilot = g_kPilotMan.FindPilot(kGuid);
	if( !pkPilot )
	{
		return true;
	}

	CUnit*	pUnit = pkPilot->GetUnit();
	if( !pUnit )
	{
		return true;
	}

	PgPlayer* pPlayer = dynamic_cast<PgPlayer*>(pUnit);
	if( !pPlayer )
	{
		return true;
	}

	BYTE const kState = pPlayer->GetDBPlayerState();
	return ( (kState & CIDBS_NeedRename) == CIDBS_NeedRename );
}

bool PgSelectStage::IsUpButtonVisible(XUI::CXUI_Wnd* pWnd)
{
	XUI::CXUI_Wnd *pkCharList = XUIMgr.Get(std::wstring(_T("FRM_CHAR_LIST")));

	bool bRet = false;
	if(m_kContSpawnSlotMap.size() > 4)
	{
		bRet = true;
	}
	
	pkCharList->GetControl(std::wstring(_T("BTN_UP")))->Visible(bRet);
	return bRet;
}

bool PgSelectStage::IsDownButtonVisible(XUI::CXUI_Wnd* pWnd)
{
	XUI::CXUI_Wnd *pkCharList = XUIMgr.Get(std::wstring(_T("FRM_CHAR_LIST")));

	bool bRet = false;
	if(m_kContSpawnSlotMap.size() > 4)
	{
		bRet = true;
	}
	
	pkCharList->GetControl(std::wstring(_T("BTN_DOWN")))->Visible(bRet);
	return bRet;
}

int PgSelectStage::GetNotNewbiePlayerCount() const
{
	int iCount = 0;
	CONT_SPAWN_SLOT_MAP::const_iterator iter = m_kContSpawnSlotMap.begin();
	while( m_kContSpawnSlotMap.end() != iter )
	{
		CONT_SPAWN_SLOT_MAP::value_type const &rkCharKey = (*iter);
		PgPilot const* pkPilot = g_kPilotMan.FindPilot(rkCharKey.CharacterGuid());
		if( pkPilot )
		{
			PgPlayer const *pkPlayer = dynamic_cast<PgPlayer*>(pkPilot->GetUnit());
			if( pkPlayer )
			{
				if( !PgPlayerUtil::IsNewbiePlayer(pkPlayer) )
				{
					++iCount;
				}
			}
		}
		++iter;
	}

	return iCount;
}