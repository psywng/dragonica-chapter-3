#ifndef FREEDOM_DRAGONICA_SCRIPTING_UI_LWUIGEMSTORE_H
#define FREEDOM_DRAGONICA_SCRIPTING_UI_LWUIGEMSTORE_H
#include "lwUI.h"
#include "lwGUID.h"
#include "lwPacket.h"
#include "PgScripting.h"
#include "PgPage.h"

class PgGemStore
{
public:
	enum E_GEMSTORE_TYPE
	{
		EGT_OTHER	 = 0,
		EGT_JEWEL_1	 = 1,
		EGT_JEWEL_2  = 2,
		EGT_JOBSKILL_1  = 3,
		EGT_JOBSKILL_2  = 4,
	};

	enum E_CHECK_ITEM_RESULT
	{
		EIR_FAIL = -1,
		EIR_OK = 0,
		EIR_LACK_CP,
		EIR_LACK_JEWEL,
		EIR_LACK_HERO_MARK,
	};

	typedef struct tagDefGemItemInfo
	{
		int				iItemNo;
		int				iOrderIndex;
		tagDefGemItemInfo()
		{
			Init();
		}
		void Init()
		{
			iItemNo = 0;
			iOrderIndex = 0;
		}
	}SDefGemItemInfo;	
	typedef std::vector<SDefGemItemInfo> VEC_GEMITEM_INFO;

	PgGemStore();
	virtual ~PgGemStore();
		
	void SendReqGemStoreInfo(BM::GUID& rkNpcGuid);	// 상점정보를 요청하고	
	void RecvGemstoreInfo(BM::CPacket& rkPacket);	// 상정정보를 받아 UI를 구성하고
	void ClickSlotItem(lwUIWnd& kListItem);			// UI내의 아이템을 클릭하면, 해당 표현을 하고	
	bool SendReqGemTrade();							// 선택한 아이템의 교환 정보를 보낸 후
	void RecvGemTradeResult(BM::CPacket& rkPacket); // 에러 결과가 있다면 처리 한다

	void BeginPage(lwUIWnd kSelf);
	void EndPage(lwUIWnd kSelf);
	void JumpPrevPage(lwUIWnd kSelf);
	void JumpNextPage(lwUIWnd kSelf);
	void Page(lwUIWnd kSelf);

	void Clear();
	void DrawIconImage(lwUIWnd& UISelf);
	void DrawItemTooltip(lwUIWnd& UISelf);

	PgPage const& GetPage() const { return m_kPage; };
	
	void ClearNeedItemImg(XUI::CXUI_Wnd* pMainWnd);



	void SendReqDefGemStoreInfo(BM::GUID& rkNpcGuid, int const iMenu);	// 상점정보를 요청하고	
	void RecvDefGemstoreInfo(BM::CPacket& rkPacket);	
	bool IsOtherMode() {return (EGT_OTHER == eGemStoreType);};
	void ClickDefSlotItem(lwUIWnd& kListItem);
	void RecvDefGemTradeResult(BM::CPacket& rkPacket); // 에러 결과가 있다면 처리 한다
	int GetGemStoreTitleMenu();
	void ResetCursor(lwUIWnd& kSelf);

protected:
	XUI::CXUI_Wnd* CallGemStoreUIandGetListWnd();
	XUI::CXUI_Wnd* CallCPStoreUIandGetListWnd();
	bool CheckMyClassItem(int const iItemNo);
	bool SetStoreSlot(XUI::CXUI_Wnd* pkMain);
	bool SetSlotItem(XUI::CXUI_Wnd* pkIcon, int const iItemNo);
	int GetAmountItemPlayerHave(int const iItemNo);
	int GetItemPlayerHave(int const iItemNo);
	E_CHECK_ITEM_RESULT isEnoughTrade();


	bool SetDefStoreSlot(XUI::CXUI_Wnd* pkMain);
	bool SetDefSlotItem(XUI::CXUI_Wnd* pkIcon, int const iItemNo);
	E_CHECK_ITEM_RESULT isDefEnoughTrade();
protected:
	CONT_GEMSTORE_ARTICLE m_kContAtricle;
	int m_iEnoughCnt;
	BM::GUID m_kNpcGuid;
	int m_iSeletedItemNo;
	PgPage m_kPage;
	bool m_bIsCPStore;
	int m_iOrderIndex;


	mutable Loki::Mutex m_kMutex;
	E_GEMSTORE_TYPE	eGemStoreType;
	CONT_DEFGEMSTORE_ARTICLE m_kDefContAtricle;
};

#define g_kGemStore SINGLETON_STATIC(PgGemStore)
#endif // FREEDOM_DRAGONICA_SCRIPTING_UI_LWUIGEMSTORE_H