#ifndef FREEDOM_DRAGONICA_SCENE_WORLD_PGWORLDTIMECONDITION_H
#define FREEDOM_DRAGONICA_SCENE_WORLD_PGWORLDTIMECONDITION_H

#include <NiMain.H>

class	PgWorldTimeCondition	:	public	NiObject
{
public:

	void	SetConditionAttributeName(
		std::string const &kFromConditionAttributeName,
		std::string const &kToConditionAttributeName)
	{
		m_kFromConditionAttributeName = kFromConditionAttributeName;
		m_kToConditionAttributeName = kToConditionAttributeName;
	}

	bool	CheckTheElementHasTimeConditionAttribute(TiXmlElement const *pkElement)	const;
	bool	ReadFromXmlElement(TiXmlElement const *pkElement);

public:

	bool	CheckTimeIsInsideDuration(SYSTEMTIME const &kTime)	const;

private:

	SYSTEMTIME	m_kDurationFrom;
	SYSTEMTIME	m_kDurationTo;

	std::string	m_kFromConditionAttributeName;
	std::string	m_kToConditionAttributeName;
};

#endif // FREEDOM_DRAGONICA_SCENE_WORLD_PGWORLDTIMECONDITION_H