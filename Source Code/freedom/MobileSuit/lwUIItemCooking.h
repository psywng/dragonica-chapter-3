#ifndef FREEDOM_DRAGONICA_SCRIPTING_UI_LWUIITEMCOOKING_H
#define FREEDOM_DRAGONICA_SCRIPTING_UI_LWUIITEMCOOKING_H

class lwUIItemCooking
{
public:
	lwUIItemCooking(lwUIWnd kWnd);
	static bool RegisterWrapper(lua_State *pkState);

public:
	void DisplaySrcIcon();
	void DisplayNeedItemIcon();
	void DisplayResultItem();
	void ClearCookingData();
	bool SendReqItemCooking(bool bIsTrueSend);
	int GetCookingNeedMoney();
	void CallComfirmMessageBox();
	void Clear();
	int CheckOK();	//클라에서 먼저 한번 체크해 주자
	int const GetNowNeedItemCount(int const iNeed) const;
	void SetNowCooking(bool bValue);
	bool NowCooking();
	int AddMakingCount(int i);
	int GetMakingCount();

protected:
	XUI::CXUI_Wnd *self;
};

class PgItemCookingMgr
{
public:
	PgItemCookingMgr();
	virtual ~PgItemCookingMgr(){}

public:
	void Clear();
	int CallComfirmMessageBox();
	int GetCookingNeedMoney();
	bool SetSrcItem(const SItemPos &rkItemPos);
	void DisplayNeedItemIcon(int const iNeedIndex, XUI::CXUI_Wnd *pWnd);
	void DisplaySrcItem(XUI::CXUI_Wnd *pWnd);
	void DisplayResultItem(XUI::CXUI_Wnd *pWnd);

	bool SendReqItemCooking(bool bIsTrueSend);
	int CheckNeedItem();
	int const GetNowNeedItemCount(int const iNeed) const;
	int MaximumCount();

public:
	CLASS_DECLARATION_S( int, CurMakingNo );
	CLASS_DECLARATION_S( bool, NowCooking );
	CLASS_DECLARATION_S( int, MakingCount );	//한번에 몇개 만들거냐
	CLASS_DECLARATION_S( int, RecipeCount );	//같고있는 총 래시피 갯수
	
protected:
	BM::GUID m_guidSrcItem;//아이템 위치가 변경되거나 할 수 있으므로.
	SItemPos m_kSrcItemPos;

	PgBase_Item m_kItem;
	//SItem	m_kResultItem;
	int m_iResultItem;

	SDefItemMakingData m_kMakingData;		// 요리의 데이터.
	//SNeedItemPlusUpgrade m_kItemArray[MAX_ITEM_PLUS_UPGRADE_NEED_ARRAY+1];
};

#define g_kItemCookingMgr SINGLETON_STATIC(PgItemCookingMgr)
#endif // FREEDOM_DRAGONICA_SCRIPTING_UI_LWUIITEMCOOKING_H