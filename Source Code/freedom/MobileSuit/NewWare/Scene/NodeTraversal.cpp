
// ****************************************************************************************
//
//     Project : Dragonica Optimize and Refactoring
//   Copyright : Copyright (C) 2009 Barunson Interactive, Inc
//        Name : NodeTraversal.cpp
// Description : Node traversal for Gamebryo scene-graph node.
//      Author : Jae-Ryoung, Lee
//
// Remarks :
//   * .
// 
// Revisions :
//  09/09/30 LeeJR First Created
//

#include "stdafx.h"
#include "NodeTraversal.h"


namespace NewWare
{

namespace Scene
{


namespace NodeTraversal
{

/////////////////////////////////////////////////////////////////////////////////////////////
//

//-----------------------------------------------------------------------------------

//
/////////////////////////////////////////////////////////////////////////////////////////////

} //namespace NodeTraversal


} //namespace Scene

} //namespace NewWare
