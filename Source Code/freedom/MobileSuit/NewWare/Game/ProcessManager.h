
// ****************************************************************************************
//
//     Project : Dragonica Optimize and Refactoring
//   Copyright : Copyright (C) 2009 - 2010 Barunson Interactive, Inc
//        Name : ProcessManager.h
// Description : .
//      Author : Jae-Ryoung, Lee
//
// Remarks :
//   * PgAVObjectAlphaProcessManager 같은 클래스/객체의 다형성 처리를 위해 필요한 인터페이스.
// 
// Revisions :
//  10/01/14 LeeJR First Created
//

#ifndef _GAME_PROCESSMANAGER_H__
#define _GAME_PROCESSMANAGER_H__


namespace NewWare
{

namespace Game
{


class ProcessManager : public NiMemObject
{
    /* Nothing */
};


} //namespace Game

} //namespace NewWare


#endif //_GAME_PROCESSMANAGER_H__
