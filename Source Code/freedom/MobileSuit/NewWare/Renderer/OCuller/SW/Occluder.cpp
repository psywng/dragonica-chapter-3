
// ****************************************************************************************
//
//     Project : Dragonica Optimize and Refactoring
//   Copyright : Copyright (C) 2009 Barunson Interactive, Inc
//        Name : Occluder.cpp
// Description : .
//      Author : Jae-Ryoung, Lee
//
// Remarks :
//   * .
// 
// Revisions :
//  09/12/08 LeeJR First Created
//

#include "stdafx.h"
#include "Occluder.h"


namespace NewWare
{

namespace Renderer
{

namespace OCuller
{

namespace SW
{


/////////////////////////////////////////////////////////////////////////////////////////////
//

NiImplementRTTI(Occluder, NiObject);

//-----------------------------------------------------------------------------------

Occluder::Occluder()
{
    /* Nothing */
}

//-----------------------------------------------------------------------------------

Occluder::~Occluder()
{
    /* Nothing */
}

//
/////////////////////////////////////////////////////////////////////////////////////////////


} //namespace SW

} //namespace OCuller

} //namespace Renderer

} //namespace NewWare
