#include "stdafx.h"
#include "Variant/DefAbilType.h"
#include "Variant/PgStringUtil.h"
#include "PgWorld.h"
#include "PgAction.h"
#include "lwBase.h"
#include "lwDeathSnatchSkillScriptHelpFunc.h"
#include "PgPilotMan.h"
#include "lwPilot.h"
#include "lwPilotMan.h"
#include "lwActor.h"
#include "lwAction.h"
#include "lwGuid.h"
#include "lwPoint3.h"
#include "lwFindTargetParam.h"
#include "lwQuaternion.h"
#include "lwActionTargetInfo.h"
#include "lwActionTargetList.h"
#include "lwPacket.H"
#include "PgActionFSMFuncMgr.H"
#include "PgCommonSkillActionFSM.h"
#include "lwCommonSkillUtilFunc.h"

PgCommonSkillAction::PgCommonSkillAction()
:m_bNoUseTLM(false)
{}
	
bool PgCommonSkillAction::OnCheckCanEnter(lwActor kActor, lwAction kAction)	const
{
	if( kActor.IsNil() )	{ lwSetBreak(); }
	if( kAction.IsNil() )	{ lwSetBreak(); }
	//int const iWeaponType = kActor.GetEquippedWeaponType();	// 기본적으로 Action 전이 할때 Weapon 체크함
	

	return true;
}
// ------------------------------------------------------------------------------------------------
bool PgCommonSkillAction::OnEnter(lwActor kActor, lwAction kAction)	const
{
	if( kActor.IsNil() )	{ lwSetBreak(); }
	if( kAction.IsNil() )	{ lwSetBreak(); }
	
	if( ESS_CASTTIME  == kAction.GetActionParam() )
	{//캐스팅이라면 리턴(AP_CASTING)
		return	true;
	}

//	kAction.SetNoBroadCastTargetList(true);
	{//스킬 사용 중간에 다시 때릴수 
		if( std::string("TRUE") == kAction.GetScriptParam("NO_USE_TLM") )
		{// 없다면
			m_bNoUseTLM = true;
		}
		else
		{// 다시 때릴수 있다면
			m_bNoUseTLM = false;
		}
	}
	
	OnCastingCompleted(kActor, kAction);
	return true;
}

void PgCommonSkillAction::OnCastingCompleted(lwActor kActor, lwAction kAction)	const
{
	if( kActor.IsNil() )	{ lwSetBreak(); }
	if( kAction.IsNil() )	{ lwSetBreak(); }
//	kAction.SetSlot(1);
//	kActor.PlayCurrentSlot(false);
}

void PgCommonSkillAction::OnOverridePacket(lwActor kActor, lwAction kAction, lwPacket kPacket)	const
{
	if( kActor.IsNil() )	{ lwSetBreak(); }
	if( kAction.IsNil() )	{ lwSetBreak(); }

}

bool PgCommonSkillAction::OnUpdate(lwActor kActor, lwAction kAction, float accumTime, float frameTime)	const
{
	if( kActor.IsNil() )	{ lwSetBreak(); }
	if( kAction.IsNil() )	{ lwSetBreak(); }
	
	if( kActor.IsAnimationDone() )
	{
		if( !kActor.PlayNext() )
		{
			kAction.SetParam(1, "end");
			return false;
		}
	}

	return true;
}
bool PgCommonSkillAction::OnCleanUp(lwActor kActor, lwAction kAction)	const
{
	if( kActor.IsNil() )	{ lwSetBreak(); }
	if( kAction.IsNil() )	{ lwSetBreak(); }

	return true;
}

bool PgCommonSkillAction::OnLeave(lwActor kActor, lwAction kNextAction, bool bCancel)	const
{// 다른 액션으로 전이
	if( kActor.IsNil() )	{ lwSetBreak(); }
	if( kNextAction.IsNil() )	{ lwSetBreak(); }

	lwAction kCurAction =  kActor.GetAction();
	if( kCurAction.IsNil() )	{ lwSetBreak(); }
	
	if( false == kActor.IsUnderMyControl() ) 
	{// 타인이라면 추가적으로 확인할 필요가 없고
		return true;
	}
	// 내 제어 하에 있는 Actor이고
	
	if( true ==  kActor.IsUnitType(UT_SUB_PLAYER) )
	{// SubPlayer(쌍둥이등)이라면 기본 액션으로 돌려주고
		lwCommonSkillUtilFunc::TryMustChangeActorAction(kActor, "a_twin_sub_trace_ground");
		return true;
	}

	std::string const kEndParam = kCurAction.GetParam(1);
	std::string const kNextActionID = kNextAction.GetID();
	
	if("end" == kEndParam) 
	{// 정상적인 과정을 통해 전이 되는것이라면
		return true;
	}

/*	
	if( 반드시 예정된 Action으로 진행 되어야 한다면 
		&& kNextActionID != kCurAction.GetNextActionName()
		)
	{
		ODS("다른게 들어옴:"..actionID.."\n",false, 912)
		return false;
	}
*/

/*	//화면을 검게 하는 옵션이 있었다면
	if action:GetActionType()=="EFFECT" then
		if actor:IsMyActor() then
			g_world:SetShowWorldFocusFilterColorAlpha(0xFFFFFF, g_world:GetWorldFocusFilterAlpha(), 0, 1, false,false);
			g_world:ClearDrawActorFilter()
		end
		return true;
	end
*/
	return false;
}
// ------------------------------------------------------------------------------------------------
bool PgCommonSkillAction::OnEvent(lwActor kActor, std::string kTextKey, int iSeqID)	const
{
	if( kActor.IsNil() )	{ lwSetBreak(); }
	lwAction kCurAction = kActor.GetAction();
	if( kCurAction.IsNil() )	{ lwSetBreak(); }

	if( std::string("hit") == kTextKey )
	{
		if( kActor.IsUnderMyControl() )
		{// 내 제어하에 있는 Actor 이면, 타겟을 잡아 서버로 타격 요청을 하고
			kCurAction.CreateActionTargetList(kActor, true, false);
			if(!m_bNoUseTLM)
			{// 스킬 사용 중간에 다시 때릴수 있다면
				kCurAction.BroadCastTargetListModify(kActor.GetPilot(), false);
			}
		}
		if(0 < kCurAction.GetTargetList().size() )
		{
			lua_tinker::call<void, lwActor, lwAction>("SkillHelpFunc_DefaultHitOneTime",kActor, kCurAction);
		}
	}
	return true;
}

bool PgCommonSkillAction::OnTimer(lwActor kActor, lwAction kAction, float fCallTime, int iTimerID)	const
{
	if( kActor.IsNil() )	{ lwSetBreak(); }
	if( kAction.IsNil() )	{ lwSetBreak(); }

	return true;
}

// ------------------------------------------------------------------------------------------------
int PgCommonSkillAction::OnFindTarget(lwActor kActor, lwAction kAction, lwActionTargetList kTargetList)
{// 일반적으로 USE_CFUNC_ONTARGET="TRUE"로 사용하게끔하고 아주 특수한 경우 커스텀하게 타겟을 찾아야할때만 이것을 사용
	if( kActor.IsNil() )	{ lwSetBreak(); }
	if( kAction.IsNil() )	{ lwSetBreak(); }

	return 0;
}

void PgCommonSkillAction::OnTargetListModified(lwActor kActor, lwAction kAction, bool bIsBefore)	const
{
	if( kActor.IsNil() )	{ lwSetBreak(); }
	if( kAction.IsNil() )	{ lwSetBreak(); }
//	if(0 == kAction.GetParamInt(100) )
//	{
		if( false == bIsBefore )
		{// hit 키 이후라면, 바로 대미지 적용시켜 준다
			lua_tinker::call<void, lwActor, lwAction>("SkillHelpFunc_DefaultHitOneTime",kActor, kAction);
		}
//	}
}

/*
SkillFire 부분일때 -= 화면을 어두어지게 하는 부분이 있다면, // 이것도 시점이 시작, Fire 부분등 나뉘어 질수 있지만 보통은 시작이니까 일단은 시작으로 해두자
	//if actor:IsMyActor() then
	//-- 화면 어두워지게 처리
	//	g_world:SetShowWorldFocusFilterColorAlpha(0x000000, 0.0, 1.0, 0.4,true,true);
	//	g_world:AddDrawActorFilter(actor:GetPilotGuid());
	//end

*/