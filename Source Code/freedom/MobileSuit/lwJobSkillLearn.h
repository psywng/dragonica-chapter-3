#ifndef FREEDOM_DRAGONICA_CONTENTS_JOBSKILL_LWJOBSKILLLEARN_H
#define FREEDOM_DRAGONICA_CONTENTS_JOBSKILL_LWJOBSKILLLEARN_H

#include "Variant/PgJobSkill.h"

namespace lwJobSkillLearn
{
	void RegisterWrapper(lua_State *pkState);
	void lwCallJobSkillLearnUI();
	void SetFilterItemType();
	bool lwShowList(int iPage, int const iFilterType);
	bool lwNextPage();
	bool lwBeforePage();
	void lwClearList(XUI::CXUI_Wnd* pLearn);
	void lwClearAll();
	void SetPageUI(int const iCurrent, int const iMax);
	bool lwSetPrice(int const iJobSkillNo);
	void lwBuyJobSkill();
	void lwReqBuyJobSkill(int const iJobSkillNo);
	void RecvJobSkillLearn_Packet(WORD const wPacketType, BM::CPacket& rkPacket);
	void ReturnResult(EJOBSKILL_LEARN_RET const& eRet, int const iJobSkillNo);
	bool FilterLearnedSkill(CONT_DEF_JOBSKILL_SKILL& rkContLearnJobSkill);
	void lwSetFilterJobType(int const iJobType);
	bool FilterTypeSkill(CONT_DEF_JOBSKILL_SKILL& rkContLearnJobSkill, int const iJobType);
	bool lwCloseJobSkillLearnUI();
	void SetFilterText(BM::vstring& kText, int const iJobType);
	bool SetTitleText(int const iJobSkillNo, BM::vstring& kOutput);
	bool SetInfoText(CONT_DEF_JOBSKILL_SKILL::mapped_type const &rkJobSkillInfo, BM::vstring& kOutput);
	void GetJobSkillInfo(CONT_DEF_JOBSKILL_SKILL::mapped_type &rkOutput, int const iJobSkillNo);
}

#endif // FREEDOM_DRAGONICA_CONTENTS_JOBSKILL_LWJOBSKILLLEARN_H