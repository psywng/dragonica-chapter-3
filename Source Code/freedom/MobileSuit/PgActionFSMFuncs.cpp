#include "stdafx.h"
#include "PgActionFSMFuncs.h"
#include "PgAction.H"
#include "PgActor.H"
#include "lwActor.H"
#include "lwAction.H"
#include "lwPilot.H"
#include "PgPilot.H"
#include "lwComboAdvisor.H"
#include "PgWorld.H"
#include "lwInputSlotInfo.H"
#include "lwBase.H"
#include "lwCommonSkillUtilFunc.h"

////////////////////////////////////////////////////////////////////////////////////////////////
//	Common Functions 
////////////////////////////////////////////////////////////////////////////////////////////////
extern	bool lwKeyIsDown(int iKeyNum, bool bIsNotUKey);
extern	float	lwGetAccumTime();
extern	lwComboAdvisor	lwGetComboAdvisor();

bool Act_Melee_IsToUpAttack(lwActor actor,lwAction action)
{
	if (action.IsNil())
	{
		return	false;
	}
	
	if	(lwKeyIsDown(ACTIONKEY_UP,false))
	{
		return	true;
	}
	
	lwInputSlotInfo	kInputSlotInfo = action.GetInputSlotInfo();
	if (kInputSlotInfo.IsNil() == false)
	{
		if	(kInputSlotInfo.GetUKey() == ACTIONKEY_UP_ATTACK)
		{
			return	true;
		}
	}
	
	return	false;
	
}

bool Act_Melee_IsToDownAttack(lwActor actor,lwAction action)
{
	if (action.IsNil())
	{
		return	false;
	}
	
	if	(lwKeyIsDown(ACTIONKEY_DOWN,false))
	{
		return	true;
	}
	
	lwInputSlotInfo	kInputSlotInfo = action.GetInputSlotInfo();
	if (kInputSlotInfo.IsNil() == false)
	{
	
		if	(kInputSlotInfo.GetUKey() == ACTIONKEY_DOWN_ATTACK)
		{
			return	true;
		}
	
	}
	
	return	false;
	
}

////////////////////////////////////////////////////////////////////////////////////////////////
//	class	PgActionFSM_Act_Idle
////////////////////////////////////////////////////////////////////////////////////////////////
bool	PgActionFSM_Act_Idle::OnEnter(lwActor actor,lwAction action)	const
{
	lwCheckNil(actor.IsNil());
	lwCheckNil(action.IsNil());

	std::string actorID = actor.GetID();

	//	만약 stun 상태라면, stun 액션으로 전이시킨다.
	if(actor.IsStun())
	{
		actor.ReserveTransitAction("a_stun",DIR_NONE);
		return	false;
	}
	
	int const iSpecificIdleActionNo = actor.GetSpecificIdle();
	if(iSpecificIdleActionNo)
	{
		action.SetParamInt(100, 1);		// SpecificIdle
		switch(iSpecificIdleActionNo)
		{
		case ESIT_BOSS_MONSTER_IDLE:
			{
				actor.ResetAnimation();
				actor.ReserveTransitAction("a_SpecificIdle", DIR_NONE);
				return false;
			}break;
		case ESIT_NONE:
			{
			}break;
		default:
			{
				if(iSpecificIdleActionNo != action.GetActionNo())
				{
					actor.ReserveTransitActionByActionNo(actor.GetSpecificIdle(), actor.GetDirection());
					return false;
				}
			}break;
		}
	}

	if (actor.IsCheckMeetFloor())
	{
		if (actor.IsMeetFloor() && actor.IsMyActor())
		{
			if (actor.IsUnitType(UT_PLAYER) || actor.IsUnitType(UT_PET))
			{
				BYTE	byDir = actor.GetDirection();
				if (byDir != DIR_NONE)
				{
					actor.ReserveTransitAction(ACTIONNAME_RUN,byDir);
					return false;
				}
			}
		}
	}


	if(std::string(action.GetID()) == ACTIONNAME_BIDLE && actor.IsUnitType(UT_PLAYER))
	{
		action.SetSlot(1);
	}

	actor.Stop();

	if(EMGRADE_UPGRADED >= actor.GetAbil(AT_GRADE) || actor()->IsUseBattleIdle())
	{
		if(actor.IsUnitType(UT_MONSTER) && actor.HasTarget())
		{
			action.SetSlot(2);
		}
	}

	if(actor.IsUnitType(UT_PLAYER))
	{
		action.SetParamFloat(2,15);
	}                                           
	else if(actor.IsUnitType(UT_MONSTER))
	{
		action.SetParamFloat(2,4);
	}
	else if(actor.IsUnitType(UT_PET))
	{
		action.SetParamFloat(2,8);
	}

	action.SetParamInt(4, 4 + BM::Rand_Index(4));
	action.SetParamInt(5, 0);
	action.SetParamInt(6, 0);
	action.SetParamFloat(13,-1);

	return	true;
}


float	g_fLastAutoFireTime = 0;
void DoAutoFire(lwActor actor)
{
	lwCheckNil(actor.IsNil());

	if (actor.IsMyActor())
	{
		if (lwKeyIsDown(ACTIONKEY_ATTACK,false))
		{
			int	const	iBaseActorType = actor.GetPilot().GetBaseClassID();
			float	fAutoFireDelayTime = lua_tinker::call<float,int>("GetAutoFireDelayTime",iBaseActorType);
			float	fNowTime = lwGetAccumTime();
			float	fElapsedTime = fNowTime - g_fLastAutoFireTime;
			if (fElapsedTime>fAutoFireDelayTime)
			{
				char const*	pkAutoFireActionID = actor.GetNormalAttackActionID();

				if(PgAction::CheckCanEnter(actor(),pkAutoFireActionID,false) == false)
				{
					return;
				}

				g_fLastAutoFireTime = fNowTime;
				actor.ReserveTransitAction(pkAutoFireActionID,DIR_NONE);
			}
		}
	}
}
bool	PgActionFSM_Act_Idle::OnUpdate(lwActor actor,lwAction action,float accumTime,float frameTime)	const
{

	lwCheckNil(actor.IsNil());
	lwCheckNil(action.IsNil());

	int	currentSlot = action.GetCurrentSlot();
	std::string param = action.GetParam(0);
	int	iIdleType = action.GetParamInt(6);
	
	if (actor.IsCheckMeetFloor())
	{
		if(actor.IsMeetFloor() == false && actor.IsMyActor())
		{
			lwAction kAction = actor.ReserveTransitAction(ACTIONNAME_JUMP,0);
			kAction.SetSlot(2);
			kAction.SetDoNotBroadCast(true);
			return false;
		}
	}

	if (action.GetParamFloat(13) == -1)
	{
		action.SetParamFloat(13,accumTime);
	}

	if (actor.IsMyActor() == false)
	{
		BYTE dir = actor.GetDirection();
		if (dir != DIR_NONE)
		{
			actor.ReserveTransitAction(ACTIONNAME_RUN, dir);
			return true;
		}
	}
	
	if (actor.IsCheckMeetFloor())
	{
		if (actor.IsMeetFloor() && actor.IsMyActor())
		{
			if (actor.IsUnitType(UT_PLAYER) || actor.IsUnitType(UT_PET))
			{
				BYTE	byDir = actor.GetDirection();
				if (byDir != DIR_NONE)
				{
					actor.ReserveTransitAction(ACTIONNAME_RUN,byDir);
					return true;
				}
			}
		}
	}

	DoAutoFire(actor);

	if (actor.IsCheckMeetFloor())
	{
		if (actor.IsMeetFloor() == false 
			&& actor.GetAbil(AT_MONSTER_TYPE) != 1 
			&& actor.IsUnitType(UT_OBJECT) == false)
		{
			if (strcmp(action.GetParam(119),"jump_trap") == 0 || actor.GetVelocity().GetZ() < 0 )
			{
				action.SetNextActionName(ACTIONNAME_JUMP);
				action.SetParam(3, "fall_down");
				return false;
			}
		}
	}

	if (actor.IsAnimationDone() == true)
	{
		actor.ResetAnimation();
	
		if (iIdleType == 0)
		{
		
			int iBaseIdleLoopNum = action.GetParamInt(4);
			int iBaseIdleLoopCurNum = action.GetParamInt(5);
			
			iBaseIdleLoopCurNum=iBaseIdleLoopCurNum+1;
			
			if (iBaseIdleLoopCurNum>= iBaseIdleLoopNum)
			{
				action.SetParamInt(6,1);
				actor.PlayCurrentSlot(false);
			}
			else
			{
				action.SetParamInt(5,iBaseIdleLoopCurNum);
				actor.PlayCurrentSlot(true);	
			}
		}
		else if( iIdleType == 1 )
		{
			
			action.SetParamInt(6, 0);
			action.SetParamInt(4, 4 + BM::Rand_Index(4));
			action.SetParamInt(5, 0);
			
			actor.PlayCurrentSlot(true);			
		}
	

		return true;
	}

	if (currentSlot == 1)
	{
		if (accumTime - action.GetParamFloat(13) > 3.0)
		{
			action.SetNextActionName(ACTIONNAME_IDLE);
			return false;
		}
	}
	else if(actor.GetPilot().GetAbil(AT_IDLEACTION_TYPE) != 101 && accumTime - action.GetParamFloat(13) > 3.0 )
	{
		action.SetParamFloat(13,accumTime);
	}
	
	int const iSpecificIdleActionNo = actor.GetSpecificIdle();
	switch(iSpecificIdleActionNo)
	{
	case ESIT_BOSS_MONSTER_IDLE:
		{
			actor.ResetAnimation();
			actor.ReserveTransitAction("a_SpecificIdle", DIR_NONE);
			return false;
		}break;
	case ESIT_NONE:
		{
			if(0 < action.GetParamInt(100))
			{
				actor.ReserveTransitAction("a_idle", actor.GetDirection());
				return false;
			}
		}break;
	default:
		{
			if(iSpecificIdleActionNo != action.GetActionNo())
			{
				actor.ReserveTransitActionByActionNo(actor.GetSpecificIdle(), actor.GetDirection());
				return false;
			}
		}break;
	}

	return true;
}
bool	PgActionFSM_Act_Idle::OnLeave(lwActor actor,lwAction action,bool bCancel)	const
{
	lwCheckNil(actor.IsNil());
	lwCheckNil(action.IsNil());
	lwCheckNil(actor.GetAction().IsNil());

	if (strcmp(action.GetID(),ACTIONNAME_JUMP)==0 && strcmp(actor.GetAction().GetParam(3),"fall_down") == 0 )
	{
		// 절벽에서 떨어질 때는, ActionPacket을 보내지 않는다.
		action.SetSlot(2);
		action.SetDoNotBroadCast(true);
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////
//	class	PgActionFSM_Act_Walk
////////////////////////////////////////////////////////////////////////////////////////////////
bool	PgActionFSM_Act_Walk::OnEnter(lwActor actor,lwAction action)	const
{
	lwCheckNil(actor.IsNil());
	lwCheckNil(action.IsNil());

	lwAction prevAction = actor.GetAction();
	if (prevAction.IsNil() == false)
	{
		std::string prevActionID = prevAction.GetID();
		if (prevActionID == ACTIONNAME_JUMP)
		{
			// 현재 액션이 점프이고 슬롯이 3번이 아니거나,
			// 바닥이 아니면, 전이 불가능
			int prevSlot = prevAction.GetCurrentSlot();
			if (prevSlot != 3 ||
				actor.IsMeetFloor() == false)
			{
				action.SetDoNotBroadCast(true);
				return false;
			}
		}
		else if(prevActionID == "a_walk_left" ||
			prevActionID == "a_walk_right" ||
			prevActionID == "a_walk_up" ||
			prevActionID == "a_walk_down")
		{
			action.SetDoNotBroadCast(true);
			return false;
		}
		// 그 외에는 전이 가능
	}

	actor.FindPathNormal();
	if (strcmp(action.GetID(),"a_walk_left") == 0)
	{
		actor.ToLeft(true,true);
	}
	else if(strcmp(action.GetID(),"a_walk_right") == 0)
	{
		actor.ToLeft(false,true);
	}
	
	//actor.ResetAnimation();
	
	return true;
}
bool	PgActionFSM_Act_Walk::OnUpdate(lwActor actor,lwAction action,float accumTime,float frameTime)	const
{
	lwCheckNil(actor.IsNil());
	lwCheckNil(action.IsNil());

	std::string actorID = actor.GetID();
	
	float fOriginalMoveSpeed = static_cast<float>(actor.GetAbil(AT_MOVESPEED));
	float movingSpeed = static_cast<float>(actor.GetAbil(AT_C_MOVESPEED));
	float fCorrectedSpeed = static_cast<float>(actor.GetAbil(AT_ADD_MOVESPEED_BY_DELAY));

	if(g_pkWorld)
	{
		if(g_pkWorld->GetAttr() & GATTR_VILLAGE)
		{
			fOriginalMoveSpeed += static_cast<float>(actor.GetAbil(AT_VILLAGE_MOVESPEED));
			movingSpeed += static_cast<float>(actor.GetAbil(AT_C_VILLAGE_MOVESPEED));
		}
	}
	
	if(0<fCorrectedSpeed)
	{
		movingSpeed = fCorrectedSpeed;	//보정된 값
	}

	if (fOriginalMoveSpeed == 0)
	{
		fOriginalMoveSpeed = movingSpeed;
	}
		
	float	fAnimSpeed = 0.0f;
	
	if (0 < fOriginalMoveSpeed)
	{
		fAnimSpeed = movingSpeed/fOriginalMoveSpeed;
	}
	
	actor.SetAnimSpeed(fAnimSpeed);
	
	if (actor.IsUnitType(UT_PLAYER))
	{
		movingSpeed = movingSpeed * 0.6f;
	}

#ifndef EXTERNAL_RELEASE
	if (lua_tinker::call<bool>("IsSingleMode") == true)
	{
		movingSpeed = 120.0f * 0.6f;
	}
#endif

	BYTE dir = actor.GetDirection() ;

    //ODS("Act_Walk_OnUpdate actor."..actorID.." GUID."..actor.GetPilotGuid().GetString().." movingSpeed."..movingSpeed.."\n");
	if (actor.Walk(dir, movingSpeed, frameTime,false) == false)
	{
	    return  false;
	}

	lwPoint3 vel = actor.GetVelocity();
	float z = vel.GetZ();

	// 뛰어가다가 발이 땅에서 떨어졌을 경우
	// 올라가는 점프를 해야 할지, 내려오는 점프를 해야 할지 결정
	if (actor.IsMeetFloor() == false)
	{
		if (z < -2)
		{
			action.SetNextActionName(ACTIONNAME_JUMP);
			action.SetParam(2, "fall_down");
			return false;
		}
		else if (z > 2)
		{
			action.SetNextActionName(ACTIONNAME_JUMP);
			action.SetParam(2, "fall_up");
			return false;
		}
	}


	return true;
}
bool	PgActionFSM_Act_Walk::OnLeave(lwActor actor,lwAction action,bool bCancel)	const
{
	lwCheckNil(actor.IsNil());
	lwCheckNil(action.IsNil());

	lwAction curAction = actor.GetAction();

	if (curAction.IsNil() == false && (strcmp(action.GetID(),ACTIONNAME_JUMP) ==0) )
	{
		std::string param = curAction.GetParam(2);
		if (param == "fall_down")
		{
			action.SetSlot(2);
		}
		else if (param == "fall_up" || param == "null")
		{
			action.SetSlot(1);
		}
	}

	return true;
}
bool	PgActionFSM_Act_Walk::OnEvent(lwActor actor,std::string kTextKey,int iSeqID)	const
{
	lwCheckNil(actor.IsNil());

	if (kTextKey == "start")
	{
		actor.AttachParticle(BM::Rand_Range(-200, -100),"char_root", "e_run");
	}

	return	true;
}

////////////////////////////////////////////////////////////////////////////////////////////////
//	class	PgActionFSM_Act_Run
////////////////////////////////////////////////////////////////////////////////////////////////

bool	PgActionFSM_Act_Run::OnCheckCanEnter(lwActor actor,lwAction action)	const
{
	lwCheckNil(actor.IsNil());
	lwCheckNil(action.IsNil());

//	if action.GetEnable() == false then
		// 음.. 일단 주석처리..
//		local up = actor.GetActionState("a_run_up")
//		local down = actor.GetActionState("a_run_down")
//		local right = actor.GetActionState("a_run_right")
//		local left = actor.GetActionState("a_run_left")		
	
//		if up ~=0 or down ~=0 or right ~=0 or left ~=0 then
//			return false
//		end
//	end
	
	lwAction curAction = actor.GetAction();
	lwCheckNil(curAction.IsNil());

	std::string curActionID = curAction.GetID();
	if (curActionID == ACTIONNAME_RUN && actor.IsNowFollowing() == false)
	{
		_PgOutputDebugString("Current Action is \"a_run\" . transit failed!\n");
		return false ;
	}

	return true;
}
bool	PgActionFSM_Act_Run::OnEnter(lwActor actor,lwAction action)	const
{
	lwCheckNil(actor.IsNil());
	lwCheckNil(action.IsNil());

	lwAction prevAction = actor.GetAction();

	//if (actor.IsMyActor())
	//{
	//	_PgOutputDebugString("Run is Entered PrevAction . "..actor.GetAction().GetID().." \n");
	//}

	if (prevAction.IsNil() == false)
	{
		std::string prevActionID = prevAction.GetID();
		int prevSlot = prevAction.GetCurrentSlot();
		if (prevActionID == ACTIONNAME_JUMP)
		{
			// 현재 액션이 점프이고 슬롯이 3번이 아니거나,
			// 바닥이 아니면, 전이 불가능
			if (prevSlot != 3 || actor.IsMeetFloor() == false)
			{
				action.SetDoNotBroadCast(true);
				//ODS("__________________return false on run action _________________\n")
				return false;
			}
		}
		else if (prevActionID == ACTIONNAME_RUN)
		{
			return false;
		}
		// 그 외에는 전이 가능
	}

	actor.UseSkipUpdateWhenNotVisible(false);
	action.SetParamInt(1,0);

	lwPoint3	kTargetPos = action.GetParamAsPoint(0);
	if (kTargetPos.IsZero() == false )
	{
		action.SetParamInt(3,1);
		
		lwPoint3	kMoveDirection = kTargetPos.Subtract2(actor.GetPos());
		kMoveDirection.Unitize();
		action.SetParamAsPoint(1,kMoveDirection);
		action.SetParamAsPoint(2,actor.GetPos());
		
		actor.SetMovingDir(kMoveDirection);
		actor.LookAt(kTargetPos,true,true,false);
	}

	if (action.GetParamInt(5) == 1)
	{
		actor.BackMoving(true);
	}

	//시작 위치와 시작 시간을 기록
	action.SetParamAsPoint(7, actor.GetPos());
	action.SetParamFloat(8, static_cast<float>(g_kEventView.GetServerElapsedTime()));
	
	//lwCommonSkillUtilFunc::TryMustChangeSubPlayerAction(actor, "a_twin_sub_trace_ground", prevAction.GetDirection());

	return true;
}
bool	PgActionFSM_Act_Run::OnUpdate(lwActor actor,lwAction action,float accumTime,float frameTime)	const
{
	lwCheckNil(actor.IsNil());
	lwCheckNil(action.IsNil());

	std::string actorID = actor.GetID();
	float movingSpeed = 0.0f;
	bool	bMoveToPos = (action.GetParamInt(3) == 1);
	float	fCustomSpeed = static_cast<float>(action.GetParamInt(4));

#ifndef EXTERNAL_RELEASE
	if (lua_tinker::call<bool>("IsSingleMode") == true)
	{
		movingSpeed = 120.0f ;
	}
	else
#endif
	{
		movingSpeed = static_cast<float>(actor.GetAbil(AT_C_MOVESPEED));
	}

	if(0.0f != fCustomSpeed)
	{
		movingSpeed = fCustomSpeed;
	}
	
	float fOriginalMoveSpeed = static_cast<float>(actor.GetAbil(AT_MOVESPEED));
	if (0.0f == fOriginalMoveSpeed)
	{
		fOriginalMoveSpeed = movingSpeed;
	}

	if(g_pkWorld)
	{
		if(g_pkWorld->GetAttr() & GATTR_VILLAGE)
		{
			movingSpeed += static_cast<float>(actor.GetAbil(AT_C_VILLAGE_MOVESPEED));
		}
	}
	
	float fAnimSpeed = 0.0f;
	
	if (0.0f < fOriginalMoveSpeed)
	{
		fAnimSpeed = movingSpeed/fOriginalMoveSpeed;
	}
	
	actor.SetAnimSpeed(fAnimSpeed);	
	
	DoAutoFire(actor);
	
	if (actor.IsMyActor() && action.GetParamInt(1) == 0 )
	{
		if (accumTime - action.GetActionEnterTime() > 0.1f)
		{
			actor.SetComboCount(0);
			action.SetParamInt(1,1);
		}
	}
	
	if (bMoveToPos)
	{
		lwPoint3	kMoveTargetPos = action.GetParamAsPoint(0);
		lwPoint3	kMoveDirection = action.GetParamAsPoint(1);	
		lwPoint3	kMoveStartPos = action.GetParamAsPoint(2);
		
		lwPoint3	kDir1 = actor.GetPos().Subtract2(kMoveTargetPos);
		kDir1.Unitize();
		lwPoint3	kDir2 = kMoveStartPos.Subtract2(kMoveTargetPos);
		kDir2.Unitize();
		
		if (0 > kDir1.Dot(kDir2) || 5 > actor.GetPos().Distance(kMoveTargetPos))
		{
			actor.SetTranslate(kMoveTargetPos,false);
			return	false;
		}
		
		kMoveDirection.Multiply(movingSpeed);
		actor.SetMovingDelta(kMoveDirection);
	
		return	true;
	}
	
	BYTE dir = actor.GetDirection();
	
	if (dir == DIR_NONE)
	{
		if (actor.GetWalkingToTarget() == false)
		{
			//ODS("[Act_Run_OnUpdate] Direction is None . transit Next Action\n")
			//--actor.FindPathNormal()
			return false ;
		}
	}
	
	if (movingSpeed == 0)
	{
	    return  false;
	}

	//NiPoint3 kNewPos = actor.GetPos()();
	//NiPoint3 kOldPos = action.GetParamAsPoint(7)();
	//kOldPos.z = kNewPos.z = 0.0f; //Z를 무시하고 계산한다. 떨어지는 것은 중력에 의한 것

	//float const fDistance = (kNewPos - kOldPos).Length();

	////이동한 거리가 있을 경우
	//if(0.0f < fDistance)
	//{
	//	NiPoint3 kDirection = kNewPos - kOldPos;
	//	kDirection.Unitize();
	//	kDirection *= movingSpeed * frameTime;
	//	kDirection += kNewPos;

	//	//지금까지 이동한 거리
	//	float const fDistance2 = (kDirection - kOldPos).Length();

	//	//시뮬레이션
	//	float const fElapsedTime = (static_cast<float>(g_kEventView.GetServerElapsedTime()) - action.GetParamFloat(8)) / 1000.0f;
	//	float const fMovingSpeed2 = static_cast<float>(actor.GetAbil(AT_MOVESPEED)) * fElapsedTime;
	//	NiPoint3 kDirection2 = kNewPos - kOldPos;
	//	kDirection2.Unitize();
	//	kDirection2 *= fMovingSpeed2;
	//	kDirection2 += kNewPos;

	//	float const fDistance3 = (kDirection2 - kOldPos).Length();

	//	if(fDistance2 > fDistance3)
	//	{
	//		int i = 0;
	//		movingSpeed += movingSpeed * (fElapsedTime-frameTime);
	//	}
	//}

	actor.Walk(dir, movingSpeed,0,false);

	lwPoint3 vel = actor.GetVelocity();
	float z = vel.GetZ();

	// 뛰어가다가 발이 땅에서 떨어졌을 경우
	// 올라가는 점프를 해야 할지, 내려오는 점프를 해야 할지 결정
	if (actor.IsMeetFloor() == false)
	{
		if (z < -2)
		{
			action.SetNextActionName(ACTIONNAME_JUMP);
			action.SetParam(2, "fall_down");
			//ODS("Jump(fall down)\n")
			return false;
		}
		else if (z > 2)
		{
			action.SetNextActionName(ACTIONNAME_JUMP);
			action.SetParam(2, "fall_up");
			//ODS("Jump(fall up)\n")
			return false;
		}
	}

	// 사다리 체크
	if (actor.ContainsDirection(DIR_UP) && 
		actor.IsMyActor() &&
		actor.ClimbUpLadder())
	{
		action.SetNextActionName("a_ladder_idle");
		return false;
	}
	
	return true;
}
bool	PgActionFSM_Act_Run::OnLeave(lwActor actor,lwAction action,bool bCancel)	const
{
	lwCheckNil(actor.IsNil());
	lwCheckNil(action.IsNil());
	lwAction curAction = actor.GetAction();

	lwCheckNil(curAction.IsNil());
	std::string kActionID = action.GetID();
	//ODS("Act_Run_OnLeave nextid . "..action.GetID().."\n");

	if (kActionID == ACTIONNAME_JUMP)
	{
		std::string param = curAction.GetParam(2);
		if (param == "fall_down")
		{
			action.SetSlot(2);
			action.SetDoNotBroadCast(true);
		}
		else if (param == "fall_up")
		{
			action.SetSlot(1);
			action.SetDoNotBroadCast(true);
		}
		else if (param == "null")
		{
			if(actor.IsMeetFloor())
				action.SetSlot(1);
			else
				action.SetSlot(2);
		}
	}
	else if (kActionID == "a_ladder_idle" ||
		kActionID == "a_ladder_down" ||
		kActionID == "a_ladder_up" )
	{
		actor.HideParts(6, true);
		actor.SetParam("LADDER_WEAPON_HIDE","TRUE");
	}
	else if (kActionID == ACTIONNAME_IDLE)
	{
		//action.SetDoNotBroadCast(true)
		return true;
	}

	actor.UseSkipUpdateWhenNotVisible(true);

	return true;
}
bool	PgActionFSM_Act_Run::OnCleanUp(lwActor actor,lwAction action)	const
{
	lwCheckNil(actor.IsNil());

	actor.BackMoving(false);
	return	true;
}

bool	PgActionFSM_Act_Run::OnEvent(lwActor actor,std::string textKey,int iSeqID)	const
{
	lwCheckNil(actor.IsNil());
	if (textKey == "start")
	{
		actor.AttachParticle(BM::Rand_Range(-200, -100), "char_root", "e_run");
	}
	return	true;
}

////////////////////////////////////////////////////////////////////////////////////////////////
//	class	PgActionFSM_Act_Dash
////////////////////////////////////////////////////////////////////////////////////////////////

bool	PgActionFSM_Act_Dash::OnCheckCanEnter(lwActor actor,lwAction action)	const
{
	lwCheckNil(actor.IsNil());

	// 기획팀장님이 공중에서 대쉬 할 수 있게 해달랍니다;;(단 한 번만)
	if (actor.IsMeetFloor() == false)
	{
		if (actor.GetJumpAccumHeight()<50)
		{
			return false;
		}
	}
	
	if (actor.IsOnlyMoveAction())
	{
		return false;
	}
#ifndef EXTERNAL_RELEASE
	if (lua_tinker::call<bool>("IsSingleMode") == false)
#endif
	{
		//	이동 속도가  0이면 안된다.
		if (actor.GetAbil(AT_C_MOVESPEED) == 0)
		{
			return	false;
		}
	}

	return true;
}
bool	PgActionFSM_Act_Dash::OnEnter(lwActor actor,lwAction action)	const
{

	lwCheckNil(actor.IsNil());
	lwCheckNil(action.IsNil());

	//현재 사용 하지 않는다
	//if (actor.IsMyActor())
	//{
	//	if (actor.GetPilot().IsCorrectClass(UCLASS_ASSASSIN,false))
	//	{
	//		if (action.CheckCanEnter(actor,"a_ass_attk_dash",false))
	//		{
	//		
	//			if (action.GetDirection() != DIR_LEFT &&
	//				action.GetDirection() != DIR_RIGHT)
	//			{
	//		
	//				action.SetNextActionName("a_ass_attk_dash");
	//				action.ChangeToNextActionOnNextUpdate(true, true);
	//				return	true;
	//			
	//			}
	//		}
	//	}
	//}


	if (actor.IsMyActor() == false)
	{
//		ODS("======================== Other actor's dash Begin =====================\n")
		actor.ClearReservedAction();
	}

	float	fDashSpeed = lua_tinker::call<float>("GetDashSpeed");

	action.SetParamFloat(0, g_pkWorld->GetAccumTime());	// Start Time
	action.SetParamFloat(1, fDashSpeed);	// Start Velocity
	action.SetParam(2,"");

	action.SetParamAsPoint(7, actor.GetPos());
	action.SetParamInt(8, static_cast<int>(g_kEventView.GetServerElapsedTime()));
	
	actor.StartBodyTrail("../Data/5_Effect/9_Tex/swift_01.tga",500,0,0);
	
	actor.FindPathNormal();
	lwPoint3	pt = actor.GetTranslate();
	pt.SetZ(pt.GetZ()-30);
	//actor.AttachParticleToPoint(201, pt, "e_special_transform")	
	actor.SetComboCount(0);
	
	if (actor.IsMyActor() == false)
	{
		actor.SetTranslate(action.GetActionStartPos(),false);
	}
	
	actor.UseSkipUpdateWhenNotVisible(false);
	action.SetParamInt(4,0);
	
	if (actor.IsMyActor())
	{
	
        lwGetComboAdvisor().OnNewActionEnter(action.GetID());	
        lwGetComboAdvisor().OnNewActionEnter(action.GetID());	
        
		std::string kNormalAttackActionID = actor.GetNormalAttackActionID();
        lwGetComboAdvisor().AddNextAction("a_dash_attack");    
        lwGetComboAdvisor().AddNextAction("a_dash_jump");    
        lwGetComboAdvisor().AddNextAction("a_dash_blowup");    
        lwGetComboAdvisor().AddNextAction("a_clown_sliding_tackle");    
	}
	
	// actor.SetDirection(action.GetDirection());

	return true;
}
bool	PgActionFSM_Act_Dash::OnUpdate(lwActor actor,lwAction action,float accumTime,float frameTime)	const
{
	lwCheckNil(actor.IsNil());
	lwCheckNil(action.IsNil());

	int	iState = action.GetParamInt(4);

	std::string jumping = "TRUE";
	if (actor.IsJumping() == false)
	{
		jumping = "FALSE";
	}

	//ODS("__________Jumping . " .. jumping .. "________\n")

	
	if (std::string(action.GetParam(2)) == "" && 
	
		( std::string(action.GetNextActionName()) == "a_dash_attack" || 
		std::string(action.GetNextActionName()) == "a_magician_dash_attack" || 
		std::string(action.GetNextActionName()) == "a_thi_dash_attack" || 
		std::string(action.GetNextActionName()) == "a_archer_dash_attack") )
	{
		
	    action.SetNextActionName(ACTIONNAME_IDLE);
	    
	}
	
	if (std::string(action.GetParam(2)) == "ToDashAttack!")
	{
	    action.SetParam(2,"end");
		return false;
	};
	
	if (iState == 0)
	{
		actor.FindPathNormal();		
		
		float fAccel = -1000 * frameTime;
		float fVelocity = action.GetParamFloat(1);
		
//		local kMovingDir = actor.GetLookingDir()
//		kMovingDir.Multiply(fVelocity)
		
		BYTE dir = action.GetDirection();

		//ODS("______________Direction . " .. dir .. "\n")

		
		//현재는 일단 막아 둔다.
		//최대 대쉬 거리보다 많이 가는 경우가 생길 수 있으므로 시뮬레이션 후 최대값 이상 가지 못하도록 한다.
		NiPoint3 kNewPos = actor.GetPos()();
		NiPoint3 kOldPos = action.GetParamAsPoint(7)();
		kOldPos.z = kNewPos.z = 0.0f; //Z를 무시하고 계산한다. 떨어지는 것은 중력에 의한 것

		float const fDistance = (kNewPos - kOldPos).Length();

		//이동한 거리가 있을 경우
		if(0.0f < fDistance)
		{
			float const fMaxDistance = 130.0f;
			//남은 이동 거리
			float const fDistance2 = fMaxDistance - fDistance;

			//두 벡터를 이용하여 방향 벡터를 구하고
			//방향 벡터를 이용하여 실제로 이동하는 곳의 위치를 구한다.
			NiPoint3 kDirection = kNewPos - kOldPos;
			kDirection.Unitize();
			kDirection *= fVelocity * frameTime;
			kDirection += kNewPos;

			// 이번 프레임에 이동 하는 거리
			float const fDistance3 = (kDirection - kOldPos).Length();

			//이미 최대 대쉬 이동거리 거리를 벗어난 경우
			if(fMaxDistance - fDistance3 < 0)
			{
				//fVelocity값을 다시 세팅해야 한다.				
				fVelocity = fDistance2 / frameTime;
			}
		}

		actor.Walk(dir, fVelocity,0,false);
		
		fVelocity = fVelocity + fAccel;
		fVelocity = __max(0, fVelocity);
		action.SetParamFloat(1, fVelocity);
		
		float fElapsedTime = accumTime - action.GetParamFloat(0);
		if (0.3f < fElapsedTime)
		{
			// TODO . ElapsedTime이 0.3에서 얼마나 벗어났는지 확인 후에, 그만큼 뒤로 당겨 주어야 한다.
			action.SetParamInt(4,1);
			action.SetParamFloat(5, accumTime);
		}
	}
	else
	{
		float fElapsedTime = accumTime - action.GetParamFloat(5);
		float	fDashFreezeTime = lua_tinker::call<float>("GetDashFreezeTime");
		if (fElapsedTime>=fDashFreezeTime)
		{
			action.SetParam(2,"end");
			if (actor.IsMeetFloor() == false)
			{
				action.SetNextActionName(ACTIONNAME_JUMP);
			}
			return false;
		}
	}
	
	if (std::string(action.GetParam(3)) == "EndNow")
	{
		return false;
	}
		
	return	true;
}
bool	PgActionFSM_Act_Dash::OnCleanUp(lwActor actor,lwAction action)	const
{
	lwCheckNil(actor.IsNil());

	actor.UseSkipUpdateWhenNotVisible(true);
	actor.EndBodyTrail();

	int iStartTime = actor.GetAction().GetParamInt(8);
	int iEndTime = static_cast<int>(g_kEventView.GetServerElapsedTime());

	_PgOutputDebugString("a_Dash Moving Time : %d\n", iEndTime - iStartTime);
	NILOG(PGLOG_WARNING, "a_Dash Moving Time : %d\n", iEndTime - iStartTime);

	return true;
}
bool	PgActionFSM_Act_Dash::OnLeave(lwActor actor,lwAction action,bool bCancel)	const
{
	lwCheckNil(actor.IsNil());
	lwCheckNil(action.IsNil());

	std::string kNextActionID = action.GetID();
	if("a_rest" == kNextActionID)
	{
		return true;
	}
	
	lwAction kCurAction = actor.GetAction();

	lwCheckNil(kCurAction.IsNil());
	std::string newActionID = action.GetID();

	lwCheckNil(actor.GetPilot().IsNil());
    int	iBaseActorType = actor.GetPilot().GetBaseClassID();
    
    if (newActionID == ACTIONNAME_JUMP)
	{
		//action.SetDoNotBroadCast(true)
		
		action.SetSlot(2);
	}

	if (std::string(action.GetActionType())=="EFFECT")
	{
		return true;
	};
	
	int weapontype = actor.GetEquippedWeaponType();
	
	if( newActionID == "a_dash_attack" ||
		newActionID == "a_thi_dash_attack" ||
		newActionID == "a_nj_dash_attack" ||
		newActionID == "a_archer_dash_attack" ||
		newActionID == "a_magician_dash_attack" ||
		newActionID == "a_clown_sliding_tackle" ||
		newActionID == "a_kni_dashdownblow" ||
		newActionID == "a_dash_blowup" ||
		newActionID == "a_trap")
	{
		action.SetParamFloat(1,kCurAction.GetParamFloat(1));
		action.SetParamAsPoint(7, kCurAction.GetParamAsPoint(7)); // 대쉬의 시작 위치를 세팅
		action.SetParamFloat(8,130.0f); //Max이동 거리
	
		return true;
	}
	
	if ((newActionID.substr(0,7) == "a_melee" || 
		newActionID.substr(0,11) == "a_thi_melee" || 
		newActionID.substr(0,10) == "a_nj_melee" ||
		newActionID == "a_archer_shot_01" || 
		newActionID == "a_MagicianShot_01" ) 
		&& weapontype!=0)
	{
		
		// Dash Attack!
		
		
		lwPilot	kPilot = actor.GetPilot();
		lwCheckNil(kPilot.IsNil());

		std::string	kNextActionName = "";
		
		if (kPilot.IsCorrectClass(UCLASS_CLOWN,false) &&
			Act_Melee_IsToDownAttack(actor,action))
		{
			
			kNextActionName = "a_clown_sliding_tackle";
		}
		else if (kPilot.IsCorrectClass(UCLASS_KNIGHT,false) &&
			 actor.IsMeetFloor() == false)
		{
			kNextActionName = "a_kni_dashdownblow";
		}
		else if (kPilot.IsCorrectClass(UCLASS_WARRIOR,false) && 
			Act_Melee_IsToUpAttack(actor,action))
		{
			kNextActionName = "a_dash_blowup";
		}
		else
		{
			
			if (actor.CheckStatusEffectExist("se_transform_to_metamorphosis"))
			{
				kNextActionName = "a_nj_dash_attack";
			}
			else if (iBaseActorType == UCLASS_FIGHTER)
			{
				kNextActionName = "a_dash_attack";
			}
			else if (iBaseActorType == UCLASS_MAGICIAN)
			{
				kNextActionName = "a_magician_dash_attack";
			}
			else if (iBaseActorType == UCLASS_ARCHER)
			{
				kNextActionName = "a_archer_dash_attack";
			}
			else if (iBaseActorType == UCLASS_THIEF)
			{
				kNextActionName = "a_thi_dash_attack";
			}
		}
		
		if (kNextActionName !="" && action.CheckCanEnter(actor,kNextActionName.c_str(),true))
		{
			kCurAction.SetParam(2,"ToDashAttack!");
			kCurAction.SetNextActionName(kNextActionName.c_str());
		}
		return false;
	}

	if (std::string(kCurAction.GetParam(2)) == "end")
	{
		if (newActionID == ACTIONNAME_JUMP)
		{
			action.SetSlot(2);
		}
		return	true;
	}
	else if (newActionID == ACTIONNAME_JUMP && actor.IsJumping() == false)
	{
		if (std::string(action.GetParam(5)) == "HiJump" ) // hiJump면 그냥 jump로 넘어감.
		{
			action.SetSlot(1);
			kCurAction.SetParam(3, "EndNow");
			return true;
		}

		if(action.CheckCanEnter(actor,"a_dash_jump",false) == false)
		{
			return	false;
		}
		
		//ODS("____________To Dash Jump______________\n")
		kCurAction.SetNextActionName("a_dash_jump");
		kCurAction.SetParam(3, "EndNow");
		return false ;
	}
	else if(newActionID == "a_dash_jump")
	{
		return	action.CheckCanEnter(actor,newActionID.c_str(),false);
	}
	else if( newActionID == "a_telejump" ||
		newActionID == "a_lock_move" ||
		newActionID == "a_trap" ||
		newActionID == "a_teleport")
	{
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////
//	class	PgActionFSM_Act_Dash_Jump
////////////////////////////////////////////////////////////////////////////////////////////////
bool	PgActionFSM_Act_Dash_Jump::OnEnter(lwActor actor,lwAction action)	const
{

	lwCheckNil(actor.IsNil());
	lwCheckNil(action.IsNil());

	lwAction prevAction = actor.GetAction();
	lwCheckNil(prevAction.IsNil());

	std::string actionName = prevAction.GetID();

	if (actor.IsMyActor() == false)
	{
		action.SetSlot(0);
	}
	//else if( actionName != "a_dash" && actionName != "a_ass_attk_dash" )
	//{
	//	return false;
	//}

	float	jumpForce = lua_tinker::call<float>("GetJumpForce");

	actor.StartJump(jumpForce);
	actor.StartBodyTrail("../Data/5_Effect/9_Tex/swift_01.tga",500,0,0);
	
//	if actor.IsMyActor() == false then
//		actor.SetTranslate(action.GetActionStartPos());
//	end

	action.SetParamInt(911, prevAction.GetDirection());
	actor.UseSkipUpdateWhenNotVisible(false);

	if (actor.IsMyActor())
	{
        lwGetComboAdvisor().OnNewActionEnter(action.GetID());	
	}
	
	//if( IsClass_OwnSubPlayer(actor.GetAbil(AT_CLASS)) )
	//{// 쌍둥이이면
	//	lwActor kSubActor = actor.GetSubPlayer();
	//	if( !kSubActor.IsNil() )
	//	{
	//		lwAction kSubActorCurAction = kSubActor.GetAction();
	//		if( !kSubActorCurAction.IsNil() )
	//		{
	//			kSubActorCurAction.SetDirection( actor.GetAction().GetDirection() );
	//			kSubActorCurAction.SetNextActionName("a_dash_jump");
	//			kSubActorCurAction.ChangeToNextActionOnNextUpdate(true,true);
	//		}
	//	}
	//}
	lwCommonSkillUtilFunc::TryMustChangeSubPlayerAction( actor, action.GetID(), prevAction.GetDirection() );

	return true;
}
bool	PgActionFSM_Act_Dash_Jump::OnUpdate(lwActor actor,lwAction action,float fAccumTime,float fFrameTime)	const
{
	lwCheckNil(action.IsNil());
	lwCheckNil(actor.IsNil());
	int curAnimSlot = action.GetCurrentSlot(); // Current Animation Slot

	float	fDashJumpSpeed = lua_tinker::call<float>("GetDashJumpSpeed");
	float movingSpeed = fDashJumpSpeed;

	bool IsAnimDone = actor.IsAnimationDone();
	BYTE dir = action.GetParamInt(911);
	
	if (curAnimSlot != 2)
	{
		actor.Walk(dir, movingSpeed,0,false);
	}

	if (curAnimSlot == 0)
	{
		if (IsAnimDone == true )
		{
			actor.PlayNext();
		}
	}
	else if (curAnimSlot == 1)
	{
		if (actor.IsMeetFloor())
		{
			dir = actor.GetDirection();
			if (dir == DIR_NONE)
			{
				actor.PlayNext();
			}
			else
			{
				action.SetNextActionName(ACTIONNAME_RUN);
				action.SetParam(4808, "end");
			}
		}
	}
	else if (curAnimSlot == 2)
	{
		if (IsAnimDone == true)
		{
			action.SetNextActionName(ACTIONNAME_IDLE);
			action.SetParam(4808, "end");
		}
		else if (actor.GetDirection() != DIR_NONE)
		{
			action.SetNextActionName(ACTIONNAME_RUN);
			action.SetParam(4808, "end");
		}
	}

	if ( std::string(action.GetParam(4808)) == "end" )
	{
		if( UT_SUB_PLAYER == lwCommonSkillUtilFunc::GetUnitType( actor ) )
		{
			lwCommonSkillUtilFunc::TryMustChangeActorAction(actor, "a_twin_sub_trace_ground");
		}
		return false;
	}

	return true;
}
bool	PgActionFSM_Act_Dash_Jump::OnCleanUp(lwActor actor,lwAction action)	const
{
	lwCheckNil(actor.IsNil());

	actor.UseSkipUpdateWhenNotVisible(true);
	actor.EndBodyTrail();

	return	true;
}
bool	PgActionFSM_Act_Dash_Jump::OnLeave(lwActor actor,lwAction action,bool bCancel)	const
{
	lwCheckNil(actor.IsNil());
	lwCheckNil(action.IsNil());

	lwAction kCurAction = actor.GetAction();
	lwCheckNil(kCurAction.IsNil());

	std::string newActionID = action.GetID();
	int iCurrnetSlot = kCurAction.GetCurrentSlot();

	//ODS("__________Dash_Jump's NextAction . " .. newActionID .. "\n")
	
	if (actor.IsMyActor() == false || 
		std::string(action.GetActionType()) == "EFFECT")
	{
		return true;
	}
	
	if ( std::string(kCurAction.GetParam(4808)) == "end" &&
		(iCurrnetSlot == 1 ||
		iCurrnetSlot == 2))
	{
		if (newActionID == ACTIONNAME_RUN ||
			newActionID == ACTIONNAME_IDLE)
		{
			return true;
		}
	}
	else if (newActionID == "a_telejump" ||
		newActionID == "a_lock_move" ||
		newActionID == "a_trap" || 
		newActionID == "a_teleport")
	{
		return true;
	}
	else if (newActionID == ACTIONNAME_JUMP)
	{
		if (std::string(action.GetParam(5)) == "HiJump") // hiJump면 그냥 jump로 넘어감.
		{
			kCurAction.SetParam(4808, "EndNow");
			return true;
		}
	}
	
	return false;
}