#include "StdAfx.h"
#include "PgNetwork.h"
#include "ServerLib.h"
#include "Lohengrin/ErrorCode.h "
#include "Variant/Inventory.H"
#include "Variant/PgJobSkill.h"
#include "PgPilotMan.h"
#include "PgUIScene.h"
#include "lwUIGemStore.h"

std::wstring const WSTR_GEM_STORE(_T("FRM_GEM_STORE"));
std::wstring const WSTR_CP_STORE(_T("FRM_CP_STORE"));
std::wstring const WSTR_STORE_LIST(_T("LIST_TRADE_LIST"));
std::wstring const WSTR_ITEM_NAME(_T("SFRM_ITEM_NAME"));
std::wstring const WSTR_LV_CLASS(_T("FRM_LV_CLASS"));
std::wstring const WSTR_CHKBTN(_T("CBTN_SELECT_ITEM"));
std::wstring const WSTR_JEWEL(_T("FRM_JEWEL"));
std::wstring const WSTR_ICON_JEWEL(_T("ICON_JEWEL"));
std::wstring const WSTR_ICON_ITEM(_T("ICON_ITEM"));
std::wstring const WSTR_FRM_GET_ITEM(_T("FRM_GET_ITEM"));
std::wstring const WSTR_FRM_MSG(_T("FRM_MSG"));
std::wstring const WSTR_ICON(_T("ICON"));
std::wstring const WSTR_FRM_CP(_T("FRM_CP"));

int const JEWEL_SLOT_CNT = 5;
__int64 const ALL_CLASS = 137438953470i64;

PgGemStore::PgGemStore()
: m_iEnoughCnt(0)
, m_bIsCPStore(false)
{}

PgGemStore::~PgGemStore()
{}

void PgGemStore::SendReqGemStoreInfo(BM::GUID& rkNpcGuid)
{// 상점정보를 요청하고
	m_kNpcGuid = rkNpcGuid;
	BM::CPacket kPacket(PT_C_M_REQ_GEMSTOREINFO);
	kPacket.Push(rkNpcGuid);
	NETWORK_SEND(kPacket)
}

void PgGemStore::RecvGemstoreInfo(BM::CPacket& rkPacket)
{// 보석상점 정보를 받은 후	
	SGEMSTORE kStoreInfo;
	kStoreInfo.ReadFromPacket(rkPacket);

	eGemStoreType = static_cast<E_GEMSTORE_TYPE>(EGT_OTHER);

	m_kContAtricle = kStoreInfo.kContArticles;

	// 상점에서 파는 아이템들 중
	CONT_GEMSTORE_ARTICLE::iterator itor_Article = m_kContAtricle.begin();
	while( itor_Article != m_kContAtricle.end() )
	{//아무것도 없네.
		CONT_GEMSTORE_ARTICLE::key_type const& kArticleItemNo = itor_Article->first;
		CONT_GEMSTORE_ARTICLE::mapped_type const& kArticle = itor_Article->second;

		//CP 상점인가.
		m_bIsCPStore = (kArticle.iCP != 0
			&& JobSkill_Util::IsJobSkill_Item(static_cast<int>(kArticleItemNo) ));
		if( !CheckMyClassItem( kArticleItemNo ) )
		{
			itor_Article = m_kContAtricle.erase(itor_Article);
			continue;
		}
		++itor_Article;
	}

	XUI::CXUI_Wnd* pkParentWnd = NULL;
	if( m_bIsCPStore )
	{
		pkParentWnd = CallCPStoreUIandGetListWnd();
	}
	else
	{
		pkParentWnd = CallGemStoreUIandGetListWnd();
	}

	if(!pkParentWnd)
	{
		return;
	}

	XUI::CXUI_Builder* pkBuild = dynamic_cast<XUI::CXUI_Builder*>(pkParentWnd->GetControl(L"BLD_SLOT"));
	if( !pkBuild )
	{
		return;
	}

	int const MAX_ITEM_SLOT = pkBuild->CountX() * pkBuild->CountY();
	m_kPage.SetPageAttribute(MAX_ITEM_SLOT, 3);
	m_kPage.SetMaxItem(m_kContAtricle.size());
	UIPageUtil::PageControl(pkParentWnd->GetControl(L"FRM_PAGE"), m_kPage);
	SetStoreSlot(pkParentWnd);
}

bool PgGemStore::SendReqGemTrade()
{// 아이템의 교환 요청을 보내고

	PgGemStore::E_CHECK_ITEM_RESULT eType;
	if( IsOtherMode() )
	{
		eType = isEnoughTrade();
	}
	else
	{
		eType = isDefEnoughTrade();
	}

	switch(eType)
	{// 1차 구매 가능여부
	case EIR_LACK_CP:
		{
			lwAddWarnDataTT(700127), EL_Warning;
		}return false;
	case EIR_LACK_JEWEL:
		{
			::Notice_Show(TTW(790125), EL_Warning);
		}return false;
	case EIR_LACK_HERO_MARK:
		{
			lwAddWarnDataTT(460058), EL_Warning;
		}return false;
	case EIR_FAIL:
		return false;
	}

	PgPlayer* pPlayer = g_kPilotMan.GetPlayerUnit();
	if(!pPlayer)
	{ 
		return false; 
	}
	PgInventory* pInv = pPlayer->GetInven();
	if(!pInv)
	{ 
		return false; 
	}
	SItemPos kTempPos;
	if(!pInv->GetNextEmptyPos(IT_EQUIP, kTempPos))
	{// 인벤토리가 가득 찼다면 메세지를 보냄
		::Notice_Show(TTW(20023), EL_Warning);
		return false;
	}

	if( IsOtherMode() )
	{
		BM::CPacket kPacket(PT_C_M_REQ_GEMSTORE_BUY);
		kPacket.Push(m_kNpcGuid);
		kPacket.Push(m_iSeletedItemNo);
		NETWORK_SEND(kPacket)
	}
	else
	{
		BM::CPacket kPacket(PT_C_M_REQ_DEFGEMSTORE_BUY);
		kPacket.Push(m_kNpcGuid);
		kPacket.Push(m_iSeletedItemNo);
		kPacket.Push(static_cast<int>(eGemStoreType));
		kPacket.Push(m_iOrderIndex);
		NETWORK_SEND(kPacket)
	}
	return true;
}

void PgGemStore::RecvGemTradeResult(BM::CPacket& rkPacket)
{// 교환 결과를 받아 처리 
	HRESULT kResult;
	rkPacket.Pop(kResult);
	switch(kResult)
	{
	case E_NOT_FOUND_GEMSTORE:
		{// 보석교환 상인을 찾을 수 없음
			::Notice_Show(TTW(790123), EL_Warning);
		}break;
	case E_NOT_FOUND_ARTICLE:
		{// 교환물품을 찾을 수 없음
			::Notice_Show(TTW(790124), EL_Warning);
		}break;
	case E_NOT_ENOUGH_GEMS:
		{// 보석이 부족함
			::Notice_Show(TTW(790125), EL_Warning);
		}break;
	case S_OK:
		{
			XUI::CXUI_Wnd* pkParentWnd = NULL;
			if( !m_bIsCPStore )
			{
				pkParentWnd = CallGemStoreUIandGetListWnd();
			}

			if(pkParentWnd)
			{
				XUI::CXUI_Wnd* pkPage = dynamic_cast<XUI::CXUI_Wnd*>(pkParentWnd->GetControl(L"FRM_PAGE"));
				if( !pkPage )
				{
					return;
				}				
				ResetCursor(lwUIWnd(pkPage));
			}
		}break;
	default:
		{
		}break;
	}
}

void PgGemStore::BeginPage(lwUIWnd kSelf)
{
	if( kSelf.IsNil() ){ return; }

	lwUIWnd kParent = kSelf.GetParent();
	if( kParent.IsNil() ){ return; }

	lwUIWnd kMainUI = kParent.GetParent();
	if( kMainUI.IsNil() ){ return; }

	int const NowPage = m_kPage.Now();
	if( NowPage == m_kPage.PageBegin() )
	{
		return;
	}
	UIPageUtil::PageControl(kParent.GetSelf(), m_kPage);

	if( IsOtherMode() )
	{
		SetStoreSlot(kMainUI.GetSelf());
	}
	else
	{
		SetDefStoreSlot(kMainUI.GetSelf());
	}

	ResetCursor(kParent);
}

void PgGemStore::EndPage(lwUIWnd kSelf)
{
	if( kSelf.IsNil() ){ return; }

	lwUIWnd kParent = kSelf.GetParent();
	if( kParent.IsNil() ){ return; }

	lwUIWnd kMainUI = kParent.GetParent();
	if( kMainUI.IsNil() ){ return; }

	int const NowPage = m_kPage.Now();
	if( NowPage == m_kPage.PageEnd() )
	{
		return;
	}
	UIPageUtil::PageControl(kParent.GetSelf(), m_kPage);

	if( IsOtherMode() )
	{
		SetStoreSlot(kMainUI.GetSelf());
	}
	else
	{
		SetDefStoreSlot(kMainUI.GetSelf());
	}

	ResetCursor(kParent);
}

void PgGemStore::JumpPrevPage(lwUIWnd kSelf)
{
	if( kSelf.IsNil() ){ return; }

	lwUIWnd kParent = kSelf.GetParent();
	if( kParent.IsNil() ){ return; }

	lwUIWnd kMainUI = kParent.GetParent();
	if( kMainUI.IsNil() ){ return; }

	int const NowPage = m_kPage.Now() + 1;
	if( NowPage == m_kPage.PagePrevJump() )
	{
		return;
	}
	UIPageUtil::PageControl(kParent.GetSelf(), m_kPage);

	if( IsOtherMode() )
	{
		SetStoreSlot(kMainUI.GetSelf());
	}
	else
	{
		SetDefStoreSlot(kMainUI.GetSelf());
	}

	ResetCursor(kParent);
}

void PgGemStore::JumpNextPage(lwUIWnd kSelf)
{
	if( kSelf.IsNil() ){ return; }

	lwUIWnd kParent = kSelf.GetParent();
	if( kParent.IsNil() ){ return; }

	lwUIWnd kMainUI = kParent.GetParent();
	if( kMainUI.IsNil() ){ return; }

	int const NowPage = m_kPage.Now() + 1;
	if( NowPage == m_kPage.PageNextJump() )
	{
		return;
	}
	UIPageUtil::PageControl(kParent.GetSelf(), m_kPage);

	if( IsOtherMode() )
	{
		SetStoreSlot(kMainUI.GetSelf());
	}
	else
	{
		SetDefStoreSlot(kMainUI.GetSelf());
	}

	ResetCursor(kParent);
}

void PgGemStore::Page(lwUIWnd kSelf)
{
	if( kSelf.IsNil() ){ return; }

	lwUIWnd kParent = kSelf.GetParent();
	if( kParent.IsNil() ){ return; }

	lwUIWnd kMainUI = kParent.GetParent();
	if( kMainUI.IsNil() ){ return; }


	int const NowPage = m_kPage.Now();
	int iNewPage = (NowPage / m_kPage.GetMaxViewPage()) * m_kPage.GetMaxViewPage() + kSelf.GetBuildIndex();
	if( NowPage == iNewPage )
	{
		return;
	}

	if( iNewPage >= m_kPage.Max() )
	{
		iNewPage = m_kPage.Max() - 1;
	}

	m_kPage.PageSet(iNewPage);
	UIPageUtil::PageControl(kParent.GetSelf(), m_kPage);

	if( IsOtherMode() )
	{
		SetStoreSlot(kMainUI.GetSelf());
	}
	else
	{
		SetDefStoreSlot(kMainUI.GetSelf());
	}

	ResetCursor(kParent);
}

XUI::CXUI_Wnd* PgGemStore::CallGemStoreUIandGetListWnd()
{// 보석상점 UI를 열고, 이것의 List Wnd를 반환한다
	XUI::CXUI_Wnd* pkStoreWnd = XUIMgr.Get(WSTR_GEM_STORE.c_str());
	if(!pkStoreWnd)
	{
		pkStoreWnd = XUIMgr.Call(WSTR_GEM_STORE.c_str());
		if(!pkStoreWnd)	
		{
			return NULL;
		}
	}
	return pkStoreWnd;
}

XUI::CXUI_Wnd* PgGemStore::CallCPStoreUIandGetListWnd()
{
	XUI::CXUI_Wnd* pkStoreWnd = XUIMgr.Get(WSTR_CP_STORE.c_str());
	if(!pkStoreWnd)
	{
		pkStoreWnd = XUIMgr.Call(WSTR_CP_STORE.c_str());
		if(!pkStoreWnd)	
		{
			return NULL;
		}
	}
	return pkStoreWnd;
}

bool PgGemStore::CheckMyClassItem(int const iItemNo)
{
	GET_DEF(CItemDefMgr, kItemDefMgr);
	CItemDef const* pItemDef = kItemDefMgr.GetDef(iItemNo);
	__int64 const iClassLimit_Item = pItemDef->GetAbil64(AT_CLASSLIMIT);

	int iClassNo=0;
	if(ALL_CLASS != iClassLimit_Item)
	{// 모든 클래스 사용이 아니면
		for(int iClass=UCLASS_FIGHTER; iClass<UCLASS_MAX; ++iClass)
		{// 아이템 착용 클래스
			if(iClassLimit_Item & 1i64<<iClass)
			{
				iClassNo = iClass;
				break;
			}
		}
	}
	
	CUnit* pkPlayer = g_kPilotMan.GetPlayerUnit();
	if(!pkPlayer)
	{
		return false;
	}
	__int64 iPlayerClass = 1i64 << pkPlayer->GetAbil(AT_CLASS);
	if( 0 == (iPlayerClass & iClassLimit_Item) )
	{
		return false;
	}

	return true;
}

bool PgGemStore::SetStoreSlot(XUI::CXUI_Wnd* pkMain)
{
	if(!pkMain)
	{
		return false;
	}


	CONT_GEMSTORE_ARTICLE::const_iterator itor_Article = m_kContAtricle.begin();

	int const iIgnoreItemCount = m_kPage.Now() * m_kPage.GetMaxItemSlot();
	for(int i = 0; i < iIgnoreItemCount; ++i)
	{
		if( itor_Article == m_kContAtricle.end() )
		{
			return false;
		}
		++itor_Article;
	}

	for( int i = 0; i < m_kPage.GetMaxItemSlot(); ++i )
	{
		BM::vstring kStr(L"ICON_SLOT");
		kStr += i;

		XUI::CXUI_Wnd* pkImg = pkMain->GetControl(kStr);
		if( !pkImg )
		{
			continue;
		}

		BM::vstring kStrCount(L"ICON_SLOT_COUNT");
		kStrCount += i;
		
		XUI::CXUI_Wnd* pkCount = pkMain->GetControl(kStrCount);
		if( pkCount )
		{
			pkCount->Text(L"");
		}		

		pkImg->ClearCustomData();
		if(itor_Article != m_kContAtricle.end())
		{// 이 아이템을 ( SGEMSTORE_ARTICLE(아이템번호, CONT_GEM-필요보석들) )
			CONT_GEMSTORE_ARTICLE::key_type kArticleItemNo = itor_Article->first;
			SetSlotItem(pkImg, kArticleItemNo);
			++itor_Article;
		}
 	}

	return true;
}

bool PgGemStore::SetSlotItem(XUI::CXUI_Wnd* pkIcon, int const iItemNo)
{
	if( pkIcon )
	{
		if( CheckMyClassItem(iItemNo) )
		{
			pkIcon->SetCustomData(&iItemNo, sizeof(iItemNo));
			return true;
		}
	}
	return false;
}

void PgGemStore::ClickSlotItem(lwUIWnd& klwChkBtn)
{// 해당 버튼만 체크 상태로 만들고, 나머지 list item의 체크를 푼다
	XUI::CXUI_Wnd* pkIconWnd = klwChkBtn.GetSelf();
	if( !pkIconWnd ){ return; }

	XUI::CXUI_Wnd* pMainWnd = pkIconWnd->Parent();
	if(!pMainWnd){ return; }

	pkIconWnd->GetCustomData(&m_iSeletedItemNo, sizeof(m_iSeletedItemNo));

	CONT_GEMSTORE_ARTICLE::const_iterator itor_Article = m_kContAtricle.find(m_iSeletedItemNo);
	if(itor_Article == m_kContAtricle.end())
	{
		return;
	}
	
	SGEMSTORE_ARTICLE const& kItem = (*itor_Article).second;		
	CONT_GEMS const& kContGems = kItem.kContGems;
	CONT_GEMS::const_iterator itor_ReqGem = kContGems.begin();

	if( kItem.iCP != 0 )
	{//CP상점이다.
		XUI::CXUI_Wnd* pkCPWnd = pMainWnd->GetControl(WSTR_FRM_CP);
		if( pkCPWnd )
		{
			pkCPWnd->FontColor(0xFFFFF568);
			PgPlayer* pkPlayer = g_kPilotMan.GetPlayerUnit();
			if( pkPlayer )
			{
				int const iPlayerCP = pkPlayer->GetAbil(AT_CP);
				if( iPlayerCP < kItem.iCP )
				{
					pkCPWnd->FontColor(0xFFFF0000);
				}
			}
			pkCPWnd->Text(BM::vstring(kItem.iCP*10).operator const std::wstring &());
		}
	}

	for(int i=0; i<JEWEL_SLOT_CNT; ++i)
	{// 필요한		
		BM::vstring vstr(WSTR_JEWEL);
		vstr+=i;
		XUI::CXUI_Wnd* pJewelWnd = pMainWnd->GetControl(vstr);
		if(pJewelWnd)
		{
			XUI::CXUI_Icon* pIconJewel = dynamic_cast<XUI::CXUI_Icon*>(pJewelWnd->GetControl(WSTR_ICON_JEWEL));
			if(pIconJewel)
			{
				pIconJewel->ClearCustomData();
				pJewelWnd->Text(_T(""));
				if(itor_ReqGem != kContGems.end())
				{
					// 보석 번호와
					int const iGemNo = (*itor_ReqGem).first;
					pIconJewel->SetCustomData(&iGemNo, sizeof(iGemNo));

					// 필요 갯수를 찾고
					short const sReqCnt = (*itor_ReqGem).second;

					std::wstring kJewelNumStrForm = TTW(790121);
					// 플레이어가 소지한 해당 보석의 갯수를 찾고
					int iJewelNumIhave = GetAmountItemPlayerHave(iGemNo);
					if(sReqCnt <= iJewelNumIhave )
					{
						iJewelNumIhave = sReqCnt;
					}
					// 하단부 UI에 수량을 표현하고
					wchar_t buf[100] = {0,};
					wsprintfW(buf, kJewelNumStrForm.c_str(), iJewelNumIhave, sReqCnt);
					pJewelWnd->Text(buf);
					{// 아이템의 갯수가 부족하면 회색, 아니면 정상 출력한다				
						pIconJewel->GrayScale(iJewelNumIhave<sReqCnt);
					}
					++itor_ReqGem;
				}
			}
		}
	}
}

void PgGemStore::Clear()
{
	m_iSeletedItemNo = 0;
	m_iOrderIndex = 0;
	m_kNpcGuid.Clear();
	m_kContAtricle.clear();
	m_kDefContAtricle.clear();
	eGemStoreType = EGT_OTHER;
	XUI::CXUI_Wnd* pMainWnd = XUIMgr.Get(WSTR_GEM_STORE.c_str());
	if(pMainWnd)
	{
		for(int i=0; i<JEWEL_SLOT_CNT; ++i)
		{
			BM::vstring vstr(WSTR_JEWEL);
			vstr+=i;
			XUI::CXUI_Wnd* pJewelWnd = pMainWnd->GetControl(vstr);
			if(pJewelWnd)
			{
				XUI::CXUI_Icon* pIconJewel = dynamic_cast<XUI::CXUI_Icon*>(pJewelWnd->GetControl(WSTR_ICON_JEWEL));
				if(pIconJewel)
				{
					pIconJewel->ClearCustomData();
				}
				pJewelWnd->Text(_T(""));
			}
		}	
	}
}

void PgGemStore::DrawIconImage(lwUIWnd& UISelf)
{
	XUI::CXUI_Image* pIcon = dynamic_cast<XUI::CXUI_Image*>(UISelf.GetSelf());
	if( !pIcon )
	{ 
		return; 
	}

	int iItemNo;
	pIcon->GetCustomData(&iItemNo, sizeof(iItemNo));

	GET_DEF(CItemDefMgr, kItemDefMgr);
	const CItemDef* pItemDef = kItemDefMgr.GetDef(iItemNo);
	PgUISpriteObject* pkSprite = g_kUIScene.GetIconTexture(iItemNo);
	if( !pkSprite )
	{
		pIcon->DefaultImgTexture(NULL);
		pIcon->SetInvalidate();
		return;
	}

	PgUIUVSpriteObject* pkUVSprite = dynamic_cast<PgUIUVSpriteObject*>(pkSprite);
	if( !pkUVSprite ){ return; }
	
	pIcon->DefaultImgTexture(pkUVSprite);
	SUVInfo& rkUV = pkUVSprite->GetUVInfo();
	pIcon->UVInfo(rkUV);
	POINT2	kPoint(pIcon->Width()*rkUV.U, pIcon->Height()*rkUV.V);
	pIcon->ImgSize(kPoint);
	pIcon->SetInvalidate();
}

void PgGemStore::DrawItemTooltip(lwUIWnd& UISelf)
{
	int iItemNo = UISelf.GetCustomDataAsInt();	
	CallToolTip_ItemNo(iItemNo,UISelf.GetTotalLocation());
}

int PgGemStore::GetAmountItemPlayerHave(int const iItemNo)
{
	PgPlayer* pPlayer = g_kPilotMan.GetPlayerUnit();
	if(!pPlayer)
	{ 
		return 0; 
	}
	PgInventory* pInv = pPlayer->GetInven();
	if(!pInv)
	{ 
		return 0; 
	}
	return pInv->GetTotalCount(iItemNo);
}

int PgGemStore::GetItemPlayerHave(int const iItemNo)
{
	PgPlayer* pPlayer = g_kPilotMan.GetPlayerUnit();
	if(!pPlayer)
	{ 
		return 0; 
	}
	PgInventory* pInv = pPlayer->GetInven();
	if(!pInv)
	{ 
		return 0; 
	}

	return pInv->GetInvTotalCount(iItemNo);
}

PgGemStore::E_CHECK_ITEM_RESULT PgGemStore::isEnoughTrade()
{// 보석 교환이 가능한가?
	XUI::CXUI_Wnd* pMainWnd = XUIMgr.Get(WSTR_GEM_STORE.c_str());
	if(!pMainWnd)
	{
		pMainWnd = XUIMgr.Get(WSTR_CP_STORE.c_str());
		if( !pMainWnd )
		{
			return EIR_FAIL;
		}
	}
	CONT_GEMSTORE_ARTICLE::const_iterator itor_Article = m_kContAtricle.find(m_iSeletedItemNo);
	if(itor_Article == m_kContAtricle.end())
	{// 플레이어가 교환하고자 하는 아이템에 필요한
		return EIR_FAIL;
	}

	E_CHECK_ITEM_RESULT isEnough = EIR_OK;
	SGEMSTORE_ARTICLE const& kItem = (*itor_Article).second;		
	CONT_GEMS const& kContGems = kItem.kContGems;
	CONT_GEMS::const_iterator itor_ReqGem = kContGems.begin();	

	if( kItem.iCP != 0 )
	{
		PgPlayer* pkPlayer = g_kPilotMan.GetPlayerUnit();
		if( pkPlayer )
		{
			if( pkPlayer->GetAbil(AT_CP) < kItem.iCP )
			{
				return EIR_LACK_CP;
			}
		}
	}

	for(int i=0; i<JEWEL_SLOT_CNT; ++i)
	{// 충분한 보석이 있는지를 검사 하고
		BM::vstring vstr(WSTR_JEWEL);
		vstr+=i;
		XUI::CXUI_Wnd* pJewelWnd = pMainWnd->GetControl(vstr);
		if(pJewelWnd)
		{
			XUI::CXUI_Icon* pIconJewel = dynamic_cast<XUI::CXUI_Icon*>(pJewelWnd->GetControl(WSTR_ICON_JEWEL));
			if(pIconJewel)
			{
				// 더불어 보석 요구량 아이콘을 갱신하고 (수량 변화가 있었을수 있으므로)
				pIconJewel->ClearCustomData();
				pJewelWnd->Text(_T(""));
				if(itor_ReqGem != kContGems.end())
				{
					int const iGemNo = (*itor_ReqGem).first;
					pIconJewel->SetCustomData(&iGemNo, sizeof(iGemNo));
					short const sReqCnt = (*itor_ReqGem).second;
					std::wstring kJewelNumStrForm = TTW(790121);				
					int iJewelNumIhave = GetAmountItemPlayerHave(iGemNo);
					if(sReqCnt <= iJewelNumIhave )
					{
						iJewelNumIhave = sReqCnt;
					}
					else
					{// 보석이 하나라도 부족하다면, 트레이드 불가 설정하여
						isEnough = (m_bIsCPStore)?(EIR_LACK_HERO_MARK):(EIR_LACK_JEWEL);
					}
					wchar_t buf[100] = {0,};
					wsprintfW(buf, kJewelNumStrForm.c_str(), iJewelNumIhave, sReqCnt);
					pJewelWnd->Text(buf);
					{
						pIconJewel->GrayScale(iJewelNumIhave<sReqCnt);
					}
					++itor_ReqGem;
				}
			}
		}
	}
	// 교환 가능 여부를 전달한다
	return isEnough;
}

void PgGemStore::ClearNeedItemImg(XUI::CXUI_Wnd* pMainWnd)
{// 해당 버튼만 체크 상태로 만들고, 나머지 list item의 체크를 푼다
	if(!pMainWnd){ return; }

	for(int i=0; i<JEWEL_SLOT_CNT; ++i)
	{// 필요한		
		BM::vstring vstr(WSTR_JEWEL);
		vstr+=i;
		XUI::CXUI_Wnd* pJewelWnd = pMainWnd->GetControl(vstr);
		if(pJewelWnd)
		{
			XUI::CXUI_Icon* pIconJewel = dynamic_cast<XUI::CXUI_Icon*>(pJewelWnd->GetControl(WSTR_ICON_JEWEL));
			if(pIconJewel)
			{
				pIconJewel->ClearCustomData();
			}
			pJewelWnd->Text(_T(""));
		}
	}
}

void PgGemStore::SendReqDefGemStoreInfo(BM::GUID& rkNpcGuid, int const iMenu)
{// 상점정보를 요청하고
	m_kNpcGuid = rkNpcGuid;
	BM::CPacket kPacket(PT_C_M_REQ_DEFGEMSTOREINFO);
	kPacket.Push(rkNpcGuid);
	kPacket.Push(iMenu);
	NETWORK_SEND(kPacket)
}

void PgGemStore::RecvDefGemstoreInfo(BM::CPacket& rkPacket)
{// 보석상점 정보를 받은 후	
	SDEFGEMSTORE kStoreInfo;
	int iMenu = 0;

	kStoreInfo.ReadFromPacket(rkPacket);
	rkPacket.Pop( iMenu );

	eGemStoreType = static_cast<E_GEMSTORE_TYPE>(iMenu);

	m_kDefContAtricle = kStoreInfo.kContArticles;

	// 상점에서 파는 아이템들 중
	CONT_DEFGEMSTORE_ARTICLE::iterator itor_Article = m_kDefContAtricle.begin();
	while( itor_Article != m_kDefContAtricle.end() )
	{//아무것도 없네.
		CONT_DEFGEMSTORE_ARTICLE::key_type const& kArticleKey = itor_Article->first;
		CONT_DEFGEMSTORE_ARTICLE::mapped_type const& kArticle = itor_Article->second;

		if( iMenu != kArticle.iMenu )
		{
			itor_Article = m_kDefContAtricle.erase(itor_Article);
			continue;
		}
		++itor_Article;
	}

	XUI::CXUI_Wnd* pkParentWnd = NULL;

	pkParentWnd = CallGemStoreUIandGetListWnd();

	if(!pkParentWnd)
	{
		return;
	}

	XUI::CXUI_Builder* pkBuild = dynamic_cast<XUI::CXUI_Builder*>(pkParentWnd->GetControl(L"BLD_SLOT"));
	if( !pkBuild )
	{
		return;
	}

	int const MAX_ITEM_SLOT = pkBuild->CountX() * pkBuild->CountY();
	m_kPage.SetPageAttribute(MAX_ITEM_SLOT, 3);
	m_kPage.SetMaxItem(m_kDefContAtricle.size());
	UIPageUtil::PageControl(pkParentWnd->GetControl(L"FRM_PAGE"), m_kPage);
	SetDefStoreSlot(pkParentWnd);
}

bool PgGemStore::SetDefStoreSlot(XUI::CXUI_Wnd* pkMain)
{
	BM::CAutoMutex kLock(m_kMutex);

	if(!pkMain)
	{
		return false;
	}

	VEC_GEMITEM_INFO kTemp;
	kTemp.clear();

	int iIndex = 1;

	for(int i=0; i<m_kDefContAtricle.size(); ++i)
	{
		CONT_DEFGEMSTORE_ARTICLE::iterator itor_Article = m_kDefContAtricle.begin();
		while( itor_Article != m_kDefContAtricle.end() )
		{
			CONT_DEFGEMSTORE_ARTICLE::key_type const& kArticleKey = itor_Article->first;
			CONT_DEFGEMSTORE_ARTICLE::mapped_type const& kArticle = itor_Article->second;

			if( kArticle.iOrderIndex == iIndex )
			{
				VEC_GEMITEM_INFO::value_type kValue;
				kValue.iItemNo = kArticleKey.iItemNo;
				kValue.iOrderIndex = kArticle.iOrderIndex; 

				kTemp.push_back(kValue);
				break;
			}

			++itor_Article;
		}
		++iIndex;
	}	


	VEC_GEMITEM_INFO::const_iterator itor_Article = kTemp.begin();

	int const iIgnoreItemCount = m_kPage.Now() * m_kPage.GetMaxItemSlot();
	for(int i = 0; i < iIgnoreItemCount; ++i)
	{
		if( itor_Article == kTemp.end() )
		{
			return false;
		}
		++itor_Article;
	}

	for( int i = 0; i < m_kPage.GetMaxItemSlot(); ++i )
	{
		BM::vstring kStr(L"ICON_SLOT");
		kStr += i;

		XUI::CXUI_Wnd* pkImg = pkMain->GetControl(kStr);
		if( !pkImg )
		{
			continue;
		}

		BM::vstring kStrCount(L"ICON_SLOT_COUNT");
		kStrCount += i;
		
		XUI::CXUI_Wnd* pkCount = pkMain->GetControl(kStrCount);
		if( !pkCount )
		{
			continue;
		}

		BM::vstring kStrOrderIndex(L"ICON_SLOT_ORDER_INDEX");
		kStrOrderIndex += i;

		XUI::CXUI_Wnd* pkOrderIndex = pkMain->GetControl(kStrOrderIndex);
		if( !pkOrderIndex )
		{
			continue;
		}		

		pkImg->ClearCustomData();
		pkCount->Text(L"");
		pkOrderIndex->ClearCustomData();
		pkOrderIndex->Text(L"");
		if(itor_Article != kTemp.end())
		{// 이 아이템을 ( SGEMSTORE_ARTICLE(아이템번호, CONT_GEM-필요보석들) )	
			VEC_GEMITEM_INFO::value_type kValue = (*itor_Article);
			SetDefSlotItem(pkImg, kValue.iItemNo);
			SetDefSlotItem(pkOrderIndex, kValue.iOrderIndex);

			++itor_Article;

			CONT_DEFGEMSTORE_ARTICLE::key_type kKey(kValue.iItemNo, static_cast<int>(eGemStoreType), kValue.iOrderIndex);
			CONT_DEFGEMSTORE_ARTICLE::const_iterator itor = m_kDefContAtricle.find(kKey);
			if( itor != m_kDefContAtricle.end() )
			{
				int const iCount = itor->second.iItemCount;

				if( 1 < iCount )
				{
					BM::vstring const kCount( itor->second.iItemCount );
					pkCount->Text(kCount);
					continue;
				}
			}
			pkCount->Text(L"");
		}
 	}

	return true;
}

bool PgGemStore::SetDefSlotItem(XUI::CXUI_Wnd* pkIcon, int const iItemNo)
{
	if( pkIcon )
	{
		//if( CheckMyClassItem(iItemNo) )
		{
			pkIcon->SetCustomData(&iItemNo, sizeof(iItemNo));
			return true;
		}
	}
	return false;
}

void PgGemStore::ClickDefSlotItem(lwUIWnd& klwChkBtn)
{// 해당 버튼만 체크 상태로 만들고, 나머지 list item의 체크를 푼다
	XUI::CXUI_Wnd* pkIconWnd = klwChkBtn.GetSelf();
	if( !pkIconWnd ){ return; }

	XUI::CXUI_Wnd* pMainWnd = pkIconWnd->Parent();
	if(!pMainWnd){ return; }

	pkIconWnd->GetCustomData(&m_iSeletedItemNo, sizeof(m_iSeletedItemNo));

	
	BM::vstring kStrOrderIndex(L"ICON_SLOT_ORDER_INDEX");
	kStrOrderIndex += pkIconWnd->BuildIndex();

	XUI::CXUI_Wnd* pkOrderIndex = pMainWnd->GetControl(kStrOrderIndex);
	if( !pkOrderIndex )
	{
		return;
	}		
	
	pkOrderIndex->GetCustomData(&m_iOrderIndex, sizeof(m_iOrderIndex));

	CONT_DEFGEMSTORE_ARTICLE::key_type kKey(m_iSeletedItemNo, static_cast<int>(eGemStoreType), m_iOrderIndex);
	CONT_DEFGEMSTORE_ARTICLE::const_iterator itor_Article = m_kDefContAtricle.find(kKey);
	if(itor_Article == m_kDefContAtricle.end())
	{
		return;
	}

	SDEFGEMSTORE_ARTICLE const& kItem = (*itor_Article).second;		
	CONT_DEFGEMS const& kContGems = kItem.kContGems;
	CONT_DEFGEMS::const_iterator itor_ReqGem = kContGems.begin();

	for(int i=0; i<JEWEL_SLOT_CNT; ++i)
	{// 필요한		
		BM::vstring vstr(WSTR_JEWEL);
		vstr+=i;
		XUI::CXUI_Wnd* pJewelWnd = pMainWnd->GetControl(vstr);
		if(pJewelWnd)
		{
			XUI::CXUI_Icon* pIconJewel = dynamic_cast<XUI::CXUI_Icon*>(pJewelWnd->GetControl(WSTR_ICON_JEWEL));
			if(pIconJewel)
			{
				pIconJewel->ClearCustomData();
				pJewelWnd->Text(_T(""));
				if(itor_ReqGem != kContGems.end())
				{
					// 보석 번호와
					int const iGemNo = (*itor_ReqGem).first;
					pIconJewel->SetCustomData(&iGemNo, sizeof(iGemNo));

					// 필요 갯수를 찾고
					short const sReqCnt = (*itor_ReqGem).second;

					std::wstring kJewelNumStrForm = TTW(790121);
					// 플레이어가 소지한 해당 보석의 갯수를 찾고
					int iJewelNumIhave = GetItemPlayerHave(iGemNo);
					if(sReqCnt <= iJewelNumIhave )
					{
						iJewelNumIhave = sReqCnt;
					}
					// 하단부 UI에 수량을 표현하고
					wchar_t buf[100] = {0,};
					wsprintfW(buf, kJewelNumStrForm.c_str(), iJewelNumIhave, sReqCnt);
					pJewelWnd->Text(buf);
					{// 아이템의 갯수가 부족하면 회색, 아니면 정상 출력한다				
						pIconJewel->GrayScale(iJewelNumIhave<sReqCnt);
					}
					++itor_ReqGem;
				}
			}
		}
	}
}

PgGemStore::E_CHECK_ITEM_RESULT PgGemStore::isDefEnoughTrade()
{// 보석 교환이 가능한가?
	XUI::CXUI_Wnd* pMainWnd = XUIMgr.Get(WSTR_GEM_STORE.c_str());
	if(!pMainWnd)
	{
		pMainWnd = XUIMgr.Get(WSTR_CP_STORE.c_str());
		if( !pMainWnd )
		{
			return EIR_FAIL;
		}
	}

	CONT_DEFGEMSTORE_ARTICLE::key_type kKey(m_iSeletedItemNo, static_cast<int>(eGemStoreType), m_iOrderIndex);
	CONT_DEFGEMSTORE_ARTICLE::const_iterator itor_Article = m_kDefContAtricle.find(kKey);
	if(itor_Article == m_kDefContAtricle.end())
	{// 플레이어가 교환하고자 하는 아이템에 필요한
		return EIR_FAIL;
	}

	E_CHECK_ITEM_RESULT isEnough = EIR_OK;
	SDEFGEMSTORE_ARTICLE const& kItem = (*itor_Article).second;		
	CONT_DEFGEMS const& kContGems = kItem.kContGems;
	CONT_DEFGEMS::const_iterator itor_ReqGem = kContGems.begin();	

	/*if( kItem.iCP != 0 )
	{
	PgPlayer* pkPlayer = g_kPilotMan.GetPlayerUnit();
	if( pkPlayer )
	{
	if( pkPlayer->GetAbil(AT_CP) < kItem.iCP )
	{
	return EIR_LACK_CP;
	}
	}
	}*/

	for(int i=0; i<JEWEL_SLOT_CNT; ++i)
	{// 충분한 보석이 있는지를 검사 하고
		BM::vstring vstr(WSTR_JEWEL);
		vstr+=i;
		XUI::CXUI_Wnd* pJewelWnd = pMainWnd->GetControl(vstr);
		if(pJewelWnd)
		{
			XUI::CXUI_Icon* pIconJewel = dynamic_cast<XUI::CXUI_Icon*>(pJewelWnd->GetControl(WSTR_ICON_JEWEL));
			if(pIconJewel)
			{// 더불어 보석 요구량 아이콘을 갱신하고 (수량 변화가 있었을수 있으므로)
				pIconJewel->ClearCustomData();
				pJewelWnd->Text(_T(""));
				if(itor_ReqGem != kContGems.end())
				{
					int const iGemNo = (*itor_ReqGem).first;
					pIconJewel->SetCustomData(&iGemNo, sizeof(iGemNo));
					short const sReqCnt = (*itor_ReqGem).second;
					std::wstring kJewelNumStrForm = TTW(790121);				
					int iJewelNumIhave = GetItemPlayerHave(iGemNo);
					if(sReqCnt <= iJewelNumIhave )
					{
						iJewelNumIhave = sReqCnt;
					}
					else
					{// 보석이 하나라도 부족하다면, 트레이드 불가 설정하여
						isEnough = (m_bIsCPStore)?(EIR_LACK_HERO_MARK):(EIR_LACK_JEWEL);
					}
					wchar_t buf[100] = {0,};
					wsprintfW(buf, kJewelNumStrForm.c_str(), iJewelNumIhave, sReqCnt);
					pJewelWnd->Text(buf);
					{
						pIconJewel->GrayScale(iJewelNumIhave<sReqCnt);
					}
					++itor_ReqGem;
				}
			}
		}
	}
	// 교환 가능 여부를 전달한다
	return isEnough;
}

void PgGemStore::RecvDefGemTradeResult(BM::CPacket& rkPacket)
{// 교환 결과를 받아 처리 
	HRESULT kResult;
	rkPacket.Pop(kResult);
	switch(kResult)
	{
	case E_NOT_FOUND_GEMSTORE:
		{// 보석교환 상인을 찾을 수 없음
			::Notice_Show(TTW(790123), EL_Warning);
		}break;
	case E_NOT_FOUND_ARTICLE:
		{// 교환물품을 찾을 수 없음
			::Notice_Show(TTW(790124), EL_Warning);
		}break;
	case E_NOT_ENOUGH_GEMS:
		{// 보석이 부족함
			::Notice_Show(TTW(790125), EL_Warning);
		}break;
	case S_OK:
		{
			XUI::CXUI_Wnd* pkParentWnd = NULL;
			if( !m_bIsCPStore )
			{
				pkParentWnd = CallGemStoreUIandGetListWnd();
			}

			if(pkParentWnd)
			{
				XUI::CXUI_Wnd* pkPage = dynamic_cast<XUI::CXUI_Wnd*>(pkParentWnd->GetControl(L"FRM_PAGE"));
				if( !pkPage )
				{
					return;
				}				
				ResetCursor(lwUIWnd(pkPage));
			}
		}break;
	default:
		{
		}break;
	}
}

int PgGemStore::GetGemStoreTitleMenu()
{
	if( true == IsOtherMode() )
	{
		return static_cast<int>(EGT_OTHER);
	}

	if( EGT_JEWEL_1 == eGemStoreType )
	{
		return static_cast<int>(EGT_JEWEL_1);
	}
	else if( EGT_JOBSKILL_1 == eGemStoreType )
	{
		return static_cast<int>(EGT_JOBSKILL_1);
	}
	else if( EGT_JOBSKILL_2 == eGemStoreType )
	{
		return static_cast<int>(EGT_JOBSKILL_2);
	}

	return static_cast<int>(EGT_JEWEL_2);

}

void PgGemStore::ResetCursor(lwUIWnd& kSelf)
{
	if( kSelf.IsNil() ){ return; }

	XUI::CXUI_Wnd* pSelf = kSelf.GetSelf();
	if( !pSelf ){ return; }

	XUI::CXUI_Wnd* pParent = pSelf->Parent();
	if( !pParent ){ return; }

	XUI::CXUI_Wnd* pSelectImg = pParent->GetControl(L"IMG_SELECT");
	if( !pSelectImg ){ return; }

	int const iNewBuildIndex = pSelf->BuildIndex();
	int const iOldBuildIndex = lwUIWnd(pSelectImg).GetCustomDataAsInt();

	if( pSelectImg->Visible() )
	{
		pSelectImg->Visible(false);
		pSelectImg->ClearCustomData();

		g_kGemStore.ClearNeedItemImg(pParent);
	}

	m_iSeletedItemNo = 0;
	m_iOrderIndex = 0;
}