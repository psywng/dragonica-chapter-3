#include "stdafx.h"
#include "Variant/DefAbilType.h"
#include "Variant/PgStringUtil.h"
#include "Variant/Global.h"
#include "PgWorld.h"
#include "PgAction.h"
#include "PgPilot.h"
#include "PgPilotMan.h"
#include "lwBase.h"
#include "lwDeathSnatchSkillScriptHelpFunc.h"
#include "lwPilot.h"
#include "lwPilotMan.h"
#include "lwActor.h"
#include "lwAction.h"
#include "lwGuid.h"
#include "lwPoint3.h"
#include "lwFindTargetParam.h"
#include "lwQuaternion.h"
#include "lwActionTargetInfo.h"
#include "lwActionTargetList.h"
#include "lwCommonSkillUtilFunc.h"

namespace lwCommonSkillUtilFunc
{
	void RegisterWrapper(lua_State *pkState)
	{
		using namespace lua_tinker;
		def(pkState, "TryMustChangeSubPlayerAction", TryMustChangeSubPlayerAction);
		def(pkState, "GetUnitType", GetUnitType);
	}
	
	int GetUnitType(lwActor kActor)
	{
		if( kActor.IsNil() )
		{
			return 0;
		}
		PgPilot* pkPilot = kActor()->GetPilot();
		if(!pkPilot)
		{
			return 0;
		}
		CUnit* pkUnit = pkPilot->GetUnit();
		if(!pkUnit)
		{
			return 0;
		}
		return pkUnit->UnitType();
	}

	bool TryMustChangeSubPlayerAction(lwActor kOwnerActor, char const* pcActionName, BYTE byDir)
	{
		if( kOwnerActor.IsNil() )
		{
			return false;
		}

		if( !IsClass_OwnSubPlayer(kOwnerActor.GetAbil(AT_CLASS)) )
		{
			return false;
		}
		lwActor kSubActor = kOwnerActor.GetSubPlayer();
		if( kSubActor.IsNil() )
		{
			return false;
		}
		lwAction kSubActorCurAction = kSubActor.GetAction();
		if( kSubActorCurAction.IsNil() )
		{
			return false;
		}
		if( 0 != byDir )
		{
			kSubActorCurAction.SetDirection( byDir );
		}
		kSubActorCurAction.SetNextActionName( pcActionName );
		kSubActorCurAction.ChangeToNextActionOnNextUpdate( true,true );

		return true;
	}

	bool TryMustChangeActorAction(lwActor kActor, char const* pcActionName)
	{
		if(kActor.IsNil())
		{
			return false;
		}
		lwAction kCurAction = kActor.GetAction();
		if( kCurAction.IsNil() )
		{
			return false;
		}
		//PgPilot* pkPilot = kActor.GetPilot();
		//if(!pkPilot)
		//{
		//	return false;
		//}
		//CUnit* pkUnit = pkPilot->GetUnit();
		//if(pkUnit)
		//{
		//	return false;
		//}
		//if( UT_SUB_PLAYER != pkUnit->GetUnitType() )
		//{
		//	return false;
		//}// �ֵ��� ĳ���� sub ĳ���� ���
		kCurAction.SetNextActionName(pcActionName);
		kCurAction.ChangeToNextActionOnNextUpdate(true,true);
		return true;
	}
}