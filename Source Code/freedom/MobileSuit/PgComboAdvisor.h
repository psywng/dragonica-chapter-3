#ifndef FREEDOM_DRAGONICA_UI_PGCOMBOADVISOR_H
#define FREEDOM_DRAGONICA_UI_PGCOMBOADVISOR_H
#include <nimain.h>
#include "PgRenderer.H"

const	int MAX_COMBO_NODE_CHILD =10;
const	int MAX_COMBO_NODE	=5;

class	PgMobileSuit;
class	PgComboAdvisor	
{
	friend	class	PgMobileSuit;

	enum	ComboTreeNodeState
	{
		CTNS_DISABLED=0,	//	화면에 나오지 않음
		CTNS_PASSED,	//	이미 입력되었음
		CTNS_NEXT	//	다음에 입력가능함
	};

	struct	stComboIconData
	{
		std::string	m_kActionID;
		std::string	m_kDir;
		RECT	m_kIconRect;
	};

	typedef std::vector<stComboIconData> ContIconData;


	struct	stComboTreeNode
	{
		std::string m_kActionID;

		int	m_iChildCount;
		stComboTreeNode	*m_pkChilds[MAX_COMBO_NODE_CHILD];

		int	m_iChildIndex;
		stComboTreeNode	*m_pkParent;

		ComboTreeNodeState	m_State;
		NiPoint3	m_ptPosition;

		int	m_iScreenElementIndex;
		NiScreenElementsPtr	m_spScreenElement[2];

		float	m_fTargetScale;
		float	m_fStartScale;
		float	m_fScaleStartTime;

		float	m_fCreationTime;

		stComboTreeNode()
		{
			m_State = CTNS_DISABLED;

			m_pkParent = NULL;

			m_iChildCount = 0;
			m_iChildIndex=0;

			m_iScreenElementIndex = 0;

			m_spScreenElement[0]=m_spScreenElement[1]= NULL;

			m_fTargetScale=1.0;
			m_fStartScale = 1.0;
			m_fScaleStartTime = 0.0;

			m_fCreationTime = 0;
			

			memset(m_pkChilds,0,sizeof(stComboTreeNode*)*MAX_COMBO_NODE_CHILD);
		}
		~stComboTreeNode()
		{
			ClearChilds();

			m_spScreenElement[0]=m_spScreenElement[1]= NULL;
		}

		void	ClearChilds()
		{
			for(int i=0;i<m_iChildCount;i++)
				SAFE_DELETE(m_pkChilds[i]);

			m_iChildCount = 0;
		}
	};

	typedef std::list<stComboTreeNode*> NodeList;

	ContIconData	m_vIconData;
	NodeList m_NodeList;

	NiSourceTexturePtr	m_spComboTexture;

	NiMaterialPropertyPtr	m_spMaterialProperty;
	NiAlphaPropertyPtr	m_spAlphaProperty;
	NiVertexColorPropertyPtr	m_spVertexColorProperty;
	NiTexturingPropertyPtr	m_spTexturingProperty;

public:



	void Update(float fAccumTime,float fFrameTime);
	void DrawImmediate(PgRenderer *pkRenderer);

	void	OnNewActionEnter(char const* strActionID);
	void	AddNextAction(char const* strActionID);
	void	ClearNextAction();
	void	ResetComboAdvisor();

protected:

	void	Init();
	void	Destroy();

private:

	void	UpdateComboNode(stComboTreeNode *pkNode,float fAccumTime,float fFrameTime);

	void	OnNewActionEnter(char const* strActionID,stComboTreeNode *pkNode,bool bIsRoot=false);
	void	ResetComboAdvisor(stComboTreeNode *pkNode);

	void	ParseXML();

	void	ParseComboTreeIcon(const TiXmlElement *pkElement);
	//void	ParseComboTreeData(const TiXmlElement *pkElement,stComboTreeNode *pkParentNode);

	int	CountChildNode(const TiXmlElement *pkElement);

	NiScreenElementsPtr	CreateScreenElement(std::string kActionID,std::string kDir);

	void	SetComboNodeState(stComboTreeNode *pkNode,ComboTreeNodeState kState,stComboTreeNode *pkPrevNode);
	void	GetComboNodeRect(stComboTreeNode *pkNode,float &fLeft,float &fTop,float &fWidth,float &fHeight);
};

extern	PgComboAdvisor	g_kComboAdvisor;
#endif // FREEDOM_DRAGONICA_UI_PGCOMBOADVISOR_H