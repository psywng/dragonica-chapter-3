#ifndef FREEDOM_DRAGONICA_RENDER_WORDOBJECT_ACTOR_PGPILOT_H
#define FREEDOM_DRAGONICA_RENDER_WORDOBJECT_ACTOR_PGPILOT_H

#include "PgIXmlObject.h"
#include "Variant/Unit.h"
#include "Variant/Inventory.H"
#include "PgActionTargetList.h"

class PgIWorldObject;
class PgInput;
class PgDropBox;
class PgAction;

class PgRemoteInput
{
friend class PgPilot;

public:
	PgRemoteInput(SActionInfo& rkActionInfo, PgActionTargetList& rkTargetList, DWORD dwSyncTime);
	
private:
	SActionInfo	m_kActionInfo;
	PgActionTargetList m_kTargetList;

#ifdef PG_SYNC_ENTIRE_TIME
	DWORD m_dwSyncTime;
#endif
};

class PgInputSlotInfo
{
private:
	std::string		m_kActionID;
	unsigned int	m_uiKey;
	void*	m_pkUserData;
	bool	m_bRecord;
	bool	m_bEnable;

public:
	PgInputSlotInfo(char const* pcActionID, unsigned int uiKey, void* pkUserData = 0, bool bRecord = false,bool bEnable = true);

	// getter
	std::string&	GetActionID();
	unsigned int	GetUKey() const;
	void*			GetUserData() const;
	bool			IsRecord() const;
	bool			GetEnable()	const { return m_bEnable; }

	// setter
	void SetActionID(std::string const& rkActionID);

	// operator
	bool operator == (PgInputSlotInfo const& rhs);
};

class PgPilot 
	:	public PgIXmlObject
	,	public BM::CObserver< BM::CPacket* >
	
{
public:
	PgPilot();
	virtual ~PgPilot();

public:
	typedef std::list<PgRemoteInput*>	RemoteActionContainer;
	typedef std::list<PgInputSlotInfo>	InputSlotContainer;
	typedef std::vector<std::string>	DamageBlinkContainer;

	//! Create PgPilot
	static PgPilot* Create(BM::GUID& rkPilotGuid);

	PgPilot* CreateCopy();

	//! Process Action
	bool ProcessAction(PgAction* pkAction, bool bFromServer = false);

	//! Parse XML
	virtual bool ParseXml(TiXmlNode const* pkNode, void* pArg = 0, bool bUTF8 = false);

	void	ChangeXml(int iNewClassNo);

	//! Set Direciton
	void SetDirection(BYTE byDirection, DWORD dwDirectionTerm, NiPoint3& rkCurPos);
	
	//! Create Action
	PgAction *CreateAction(std::string const &kActionName);
	PgAction *CreateAction(PgInput *pkInput);
	PgAction *CreateAction(PgRemoteInput *pkRemoteInput);
	bool	CanExcuteByCommandKey(PgAction *pkAction);

	//! U-Key에 해당하는 SlotInfo를 되돌려 준다.
	PgInputSlotInfo* FindAction(unsigned int uiUKey);

	//! U-Key에 해당하는 Action-ID를 찾아준다.
	char const* FindActionID(unsigned int uiUKey);
	
	//! U-Key에 해당하는 Action-ID를 설정한다(U-Key 가 없다면 추가한다.)
	void SetKeyActionID(unsigned int uiUKey, std::string const& ActionID);
//	void SetKeyActionID_By_QuickSlotDB();

	//! Abil을 셋팅한다.
	void SetAbil(int AbilType, int Value);
	void SetAbil64(int AbilType, __int64 Value);

	//! Abil을 가져온다.
	int	GetAbil(int iAbilType) const;
	__int64	GetAbil64(int iAbilType) const;
	
	//! U-Key를 컨테이너에서 제거한다.
	void RemoveActionKey(unsigned int uiUKey);

	//! 키보드로 조종이 가능한가?
	bool IsControllable();

	//! Set Object
	void SetWorldObject(PgIWorldObject* pkObject);

	//! Set Object
	PgIWorldObject* GetWorldObject();

	//! Get Pilot Guid
	BM::GUID const& GetGuid() const;
	void SetGuid(BM::GUID const& rkGuid);

	//!	Base Class (전사,법사,궁수,도적) 을 얻어온다.
	int	GetBaseClassID(int iReqClassID=-1);
	bool IsCorrectClass(int const iReqClassID, bool bNotIncludeSelf=false);	//	이 캐릭터가 iReqClassID 에 해당하는 캐릭터가 맞는지 체크한다.
	bool IsOverClass(int const iReqClassID);	//	이 캐릭터가 iReqClassID 의 하위 클래스 캐릭터인가?

	void SetName(std::wstring const& wName);
	std::wstring const GetName() const;

	CUnit* GetUnit() const { return m_pkUnit; }
	void SetUnit(CUnit* pkUnit);
	void SetUnit(BM::GUID const& kGUID,int iUnitType,int iClassNo,int iLevel,int iGender);

	//! Pilot을 Frozen상태를 설정한다.
	void SetFreeze(bool bFreeze);

	//! Pilot이 Frozen상태인지 반환한다.
	bool IsFrozen();

	//! 들어오는 패킷을 처리하는 Method들
	void RecvNfyAction(SActionInfo& rkActionInfo, BM::CPacket* pkPacket);

	void SetLastHPSetTimeStamp(DWORD dwTimeStamp)	{	m_dwLastHPSetTimeStamp = dwTimeStamp;	}
	DWORD GetLastHPSetTimeStamp()	{	return	m_dwLastHPSetTimeStamp;	}

	InputSlotContainer const& GetInputSlotContainer()	const	{	return	m_kInputSlotContainer;	}

	virtual void VUpdate(BM::CSubject< BM::CPacket* > *const pChangedSubject, BM::CPacket* pkNfy);

	static BM::GUID const& PlayerPilotGuid() {return ms_kPlayerPilotGuid;}
	static void PlayerPilotGuid(BM::GUID const& rkInGuid) {ms_kPlayerPilotGuid = rkInGuid;}

	void	ActivateExtendedSlot();
	
private:
	void	ReloadXml();	
	//size_t DisplayHeadTransformEffect(bool const bShow);

protected:
	static BM::GUID ms_kPlayerPilotGuid;

	unsigned long m_ulLastRemoteActionTime;
	RemoteActionContainer m_kRemoteActionList;

	bool m_bFrozen;
	std::string m_kActorID;
		

	CUnit* m_pkUnit;
	PgIWorldObject* m_pkWorldObject;
	InputSlotContainer m_kInputSlotContainer;

	DWORD m_dwLastHPSetTimeStamp;
};
#endif// FREEDOM_DRAGONICA_RENDER_WORDOBJECT_ACTOR_PGPILOT_H