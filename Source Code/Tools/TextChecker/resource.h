//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TextChecker.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TEXTCHECKER_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDC_RADIO_ERRCHK                1000
#define IDC_RADIO_COMPARE               1001
#define IDC_BUTTON_SET_XML1             1002
#define IDC_EDIT_XML1                   1003
#define IDC_BUTTON_SETXML2              1006
#define IDC_BUTTON_SET_XML2             1006
#define IDC_EDIT_XML2                   1007
#define IDC_BUTTON_SETFOLDER            1010
#define IDC_EDIT_FOLDER                 1011
#define IDC_BUTTON_RUN                  1012
#define IDC_EDIT_RESULT_FILE            1013
#define IDC_BUTTON_EXIT                 1014

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
