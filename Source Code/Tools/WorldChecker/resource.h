//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by WorldChecker.rc
//
#define IDI_MAINFRAME                   102
#define IDD_WORLDCHECKER                103
#define IDD_TEXTVIEW                    104
#define IDC_TXT_DEVFOLDER               1002
#define IDC_BTN_DEVCHANGE               1003
#define IDC_TXT_QUESTID                 1004
#define IDC_BTN_VIEWTEXTLIST            1005
#define IDC_TXT_QUESTNAME               1006
#define IDC_TXT_QUESTFINDER             1007
#define IDC_CHK_QPTS_OPTION             1008
#define IDC_BTN_SIMULATION              1009
#define IDC_BTN_QUEST_PTS               1010
#define IDC_TXT_PORT                    1011
#define IDC_TXT_LOGINID                 1012
#define IDC_TXT_LOGINPW                 1013
#define IDC_LIST_QUESTTABLE             1014
#define IDC_LIST_ERRORTABLE             1014
#define IDC_IPADDRESS                   1015
#define IDC_STATUS                      1016
#define IDC_LIST_TEXTTABLE              1019
#define IDC_BUTTON1                     1020
#define IDC_BUTTON_SELECT               1020
#define IDC_LIST_LOG                    1021
#define IDC_SELECTTEXT                  1022
#define IDC_LIST_SYSTEM                 1022
#define IDC_CMB_KIND                    1023

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        105
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1025
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
