//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PatchExe.rc
//
#define IDC_MYICON                      2
#define IDD_PATCHEXE_DIALOG             102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_PATCHEXE                    107
#define IDI_SMALL                       108
#define IDC_PATCHEXE                    109
#define IDR_MAINFRAME                   128
#define IDB_BITMAP_PROGRESS1            138
#define IDB_BITMAP_PROGRESS2            139
#define IDB_BITMAP_BUTTON               140
#define IDB_BITMAP_BGCHS                143
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        144
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
