//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MPPack.rc
//
#define IDD_MPPACK_DIALOG               102
#define IDI_MPPACK                      107
#define IDI_SMALL                       108
#define IDR_MAINFRAME                   128
#define IDD_MPPACK                      129
#define IDR_RT_MANIFEST1                132
#define IDC_RDO_DIFF                    1008
#define IDC_RDO_PACK                    1009
#define IDC_RDO_CREATEHEADER            1010
#define IDC_RDO_HDMERGE                 1011
#define IDC_RDO_BINDPACK                1012
#define IDC_RDO_UNPACK                  1013
#define IDC_RDO_MAKEINB                 1014
#define IDC_RDO_EXPORT_DATLIST          1015
#define IDC_RDO_MAKEID                  1016
#define IDC_RDO_UPDATE_DAT_VER          1017
#define IDC_RDO_DAT_VER_CHECK           1018
#define IDC_RDO_CONVERT_DAT             1019
#define IDC_RDO_MAKEMANUALPATCH         1020
#define IDC_BTN_OPEN1                   1021
#define IDC_BTN_OPEN2                   1022
#define IDC_BTN_OPEN3                   1023
#define IDC_EDT_TEXT2                   1024
#define IDC_EDT_TEXT3                   1025
#define IDC_BTN_START                   1026
#define IDC_BTN_FIXID                   1027
#define IDC_EDT_CONTENTS                1028
#define IDC_LST_OPTION                  1029
#define IDC_PRG_STATE                   1030
#define IDC_EDT_MAJOR                   1031
#define IDC_EDT_MINOR                   1032
#define IDC_EDT_TINY                    1033
#define IDC_STC_PATCHID                 1034
#define IDC_STC_STATE                   1035
#define IDC_STC_TEXT1                   1036
#define IDC_STC_TEXT2                   1037
#define IDC_STC_TEXT3                   1038
#define IDC_STC_OPTION                  1039
#define IDC_EDT_TEXT1                   1040
#define IDC_RDO_MAKEAUTOPATCH           1041
#define IDC_STC_PREVPATCHID             1042
#define IDC_STC_JOB                     1043
#define IDC_STC_CONTENTS                1044
#define IDC_STC_PATCHOUT                1045
#define IDC_STC_MAJOR                   1046
#define IDC_STC_MINOR                   1047
#define IDC_STC_TINY                    1048
#define IDC_STC_FIXEDID                 1049
#define IDC_STC_LOG                     1050
#define IDC_LIST1                       1051
#define IDC_LST_LOG                     1051
#define IDC_STC_PREVID                  1052
#define IDC_RDO_PREVID                  1053
#define IDC_RDO_PATCHID                 1055
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1054
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
