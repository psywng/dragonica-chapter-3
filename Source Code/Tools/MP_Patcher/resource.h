//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Patcher.rc
//
#include "BuildNumber.h"

#define IDD_DIALOG1                     101
#define IDI_ICON1                       105
#define IDR_MAINFRAME                   105
#define IDD_OPTION                      106
#define IDD_DIALOG2                     107
#define IDD_PATCHPROC                   107
#define IDC_BTN_RESET                   1012
#define IDC_BTN_OK                      1013
#define IDC_BTN_CANCEL                  1014
#define IDC_RDO_WINDOW                  1015
#define IDC_RDO_FULL                    1016
#define IDC_BGM_MUTE                    1017
#define IDC_SD_BMG_VOLUME               1018
#define IDC_SD_BGM_VOLUME               1018
#define IDC_RDO_HIGH                    1021
#define IDC_RDO_LOW                     1022
#define IDC_SE_MUTE                     1023
#define IDC_SD_SE_VOLUME                1024

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        108
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1019
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
