#include "stdafx.h"
#include "Variant/PgJobSkillWorkBench.h"
#include "Variant/PgJobSkillWorkBenchMgr.h"
#include "Global.h"
#include "JobDispatcher.h"
#include "PgJobSkilLHomeWorkBenchMgr.h"
#include "item/PgPostManager.h"
#include "Variant/Global.h"

namespace JobSkillHomeWorkBenchMgrUtil
{
	__int64 const iDefaultSaveTerm = 60 * 5; // 5 minute
	
	PgSaveTimer::PgSaveTimer(__int64 const iDefaultTeam)
		: m_iDefaultTerm(iDefaultTeam)
	{
	}
	PgSaveTimer::~PgSaveTimer()
	{
	}
	bool PgSaveTimer::Tick(__int64 const iEleapsedTime, CONT_OUT& rkOut)
	{
		CONT_TIMER::iterator iter = m_kCont.begin();
		while( m_kCont.end() != iter )
		{
			(*iter).second -= iEleapsedTime;
			if( 0 >= (*iter).second )
			{
				rkOut.push_back( (*iter).first );
				(*iter).second = m_iDefaultTerm;
			}
			++iter;
		}
		return !rkOut.empty();
	}
	void PgSaveTimer::Add(BM::GUID const& rkFirst, BM::GUID const& rkSecond)
	{
		m_kCont.insert( std::make_pair( std::make_pair(rkFirst, rkSecond), m_iDefaultTerm) );
	}
	void PgSaveTimer::Del(BM::GUID const& rkFirst, BM::GUID const& rkSecond)
	{
		m_kCont.erase( std::make_pair(rkFirst, rkSecond) );
	}
	bool PgSaveTimer::Reset(BM::GUID const& rkFirst, BM::GUID const& rkSecond)
	{
		CONT_TIMER::iterator find_iter = m_kCont.find( std::make_pair(rkFirst, rkSecond) );
		if( m_kCont.end() != find_iter )
		{
			(*find_iter).second = m_iDefaultTerm;
			return true;
		}
		return false;
	}

	//
	PgWorkBenchLogHelper::PgWorkBenchLogHelper()
		: m_kMngGuid(), m_kContEvent()
	{
		m_kMngGuid.Generate();
	}
	PgWorkBenchLogHelper::~PgWorkBenchLogHelper()
	{
	}
	void PgWorkBenchLogHelper::Log(BM::GUID const& rkHomeGuid, BM::GUID const& rkItemGuid, SWorkBenchLog const& rkLog)
	{
		if( JobSkillWorkBenchUtil::IsSystemLog(rkLog.eEventType) )
		{
			return;
		}

		CEL::DB_QUERY kQuery(DT_PLAYER, DQT_UPDATE_USER_JOBSKILL_INFO_NO_OP, L"dbo.UP_Update_JobSkill_WorkBench_Log");
		kQuery.PushStrParam(rkItemGuid);
		rkLog.WriteToDB(kQuery);
		kQuery.InsertQueryTarget(m_kMngGuid);
		g_kCoreCenter.PushQuery(kQuery);

		CONT_JS_WORKBENCH_LOG& rkContLog = m_kContEvent[rkHomeGuid][rkItemGuid].m_kContLog;
		rkContLog.push_front( rkLog );
		size_t const iLimitLogCount = 20;
		bool bTruncateRun = false;
		if( iLimitLogCount < rkContLog.size() )
		{
			CONT_JS_WORKBENCH_LOG::iterator iter = rkContLog.begin();
			size_t iCur = 0;
			while( rkContLog.end() != iter )
			{
				++iCur;
				if( iLimitLogCount < iCur )
				{
					if( false == bTruncateRun )
					{
						Truncate(rkItemGuid, (*iter));
						bTruncateRun = true;
					}
					iter = rkContLog.erase(iter);
				}
				else
				{
					++iter;
				}
			}
		}
	}
	void PgWorkBenchLogHelper::Load(BM::GUID const& rkHomeGuid, BM::GUID const& rkItemGuid)
	{
		CEL::DB_QUERY kQuery(DT_PLAYER, DQT_LOAD_JOBSKILL_WORKBENCH_LOG, L"dbo.UP_Selete_JobSkill_WorkBench_Log");
		kQuery.PushStrParam(rkItemGuid);
		kQuery.QueryOwner(rkHomeGuid);
		kQuery.InsertQueryTarget(m_kMngGuid);
		g_kCoreCenter.PushQuery(kQuery);

	}
	void PgWorkBenchLogHelper::Truncate(BM::GUID const& rkItemGuid, SWorkBenchLog const& rkBaseLog)
	{
		CEL::DB_QUERY kQuery(DT_PLAYER, DQT_UPDATE_USER_JOBSKILL_INFO_NO_OP, L"dbo.UP_Delete_JobSkill_WorkBench_Log");
		kQuery.PushStrParam(rkBaseLog.kDateTime);
		kQuery.PushStrParam(rkItemGuid);
		kQuery.InsertQueryTarget(m_kMngGuid);
		g_kCoreCenter.PushQuery(kQuery);
	}
	void PgWorkBenchLogHelper::ReadFromPacket(BM::GUID const& rkHomeGuid, BM::CPacket& rkPacket)
	{
		BM::GUID kItemGuid;
		rkPacket.Pop(kItemGuid);
		m_kContEvent[rkHomeGuid][kItemGuid].ReadFromPacket(rkPacket);
	}
	bool PgWorkBenchLogHelper::WriteToPacket(BM::GUID const& rkHomeGuid, BM::CPacket& rkPacket) const
	{
		CONT_JS_WORKBENCH_EVENT_LOG::const_iterator find_iter = m_kContEvent.find(rkHomeGuid);
		if( m_kContEvent.end() != find_iter )
		{
			PU::TWriteTable_AM(rkPacket, (*find_iter).second);
			return true;
		}
		return false;
	}
	bool PgWorkBenchLogHelper::WriteToPacket(BM::GUID const& rkHomeGuid, BM::GUID const& rkItemGuid, BM::CPacket& rkPacket) const
	{
		CONT_JS_WORKBENCH_EVENT_LOG::const_iterator find_iter = m_kContEvent.find(rkHomeGuid);
		if( m_kContEvent.end() != find_iter )
		{
			CONT_JS_WORKBENCH_EVENT::const_iterator wb_iter = (*find_iter).second.find(rkItemGuid);
			if( (*find_iter).second.end() != wb_iter )
			{
				(*wb_iter).second.WriteToPacket(rkPacket);
				return true;
			}
		}
		return false;
	}
	void PgWorkBenchLogHelper::Del(BM::GUID const& rkHomeGuid)
	{
		m_kContEvent.erase(rkHomeGuid);
	}
	void PgWorkBenchLogHelper::Del(BM::GUID const& rkHomeGuid, BM::GUID const& rkItemGuid)
	{
		m_kContEvent[rkHomeGuid].erase(rkHomeGuid);
	}

	//
	PgWorkBenchOfflineHistoryHelper::PgWorkBenchOfflineHistoryHelper()
		: m_kContGuid()
	{
	}
	PgWorkBenchOfflineHistoryHelper::~PgWorkBenchOfflineHistoryHelper()
	{
	}
	void PgWorkBenchOfflineHistoryHelper::Load(BM::GUID const& rkCharGuid)
	{
		if( m_kContGuid.end() == m_kContGuid.find(rkCharGuid) )
		{
			m_kContGuid.insert(rkCharGuid);
			CEL::DB_QUERY kQuery(DT_PLAYER, DQT_LOAD_JOBSKILL_WORKBENCH_OFFLINE_HISTORY, L"dbo.UP_Select_UserJobSkill_WB_OfflineHistory");
			kQuery.PushStrParam(rkCharGuid);
			kQuery.InsertQueryTarget(rkCharGuid);
			kQuery.QueryOwner(rkCharGuid);
			g_kCoreCenter.PushQuery(kQuery);
		}
	}
	void PgWorkBenchOfflineHistoryHelper::AddComplete(BM::GUID const& rkCharGuid, int const iWorkBenchItemNo, int const iItemNo)
	{
		Add(JSWBHT_WORK_COMPLETE, rkCharGuid, iWorkBenchItemNo, iItemNo);
	}
	void PgWorkBenchOfflineHistoryHelper::AddBless(BM::GUID const& rkCharGuid, int const iBlessSkillNo, int const iItemNo)
	{
		Add(JSWBHT_BLESS, rkCharGuid, iBlessSkillNo, iItemNo);
	}
	void PgWorkBenchOfflineHistoryHelper::Add(EJobSkillWorkBenchHistoryType const eType, BM::GUID const& rkCharGuid, int const iWorkBenchItemNo, int const iItemNo)
	{
		CEL::DB_QUERY kQuery(DT_PLAYER, DQT_UPDATE_USER_JOBSKILL_INFO_NO_OP, L"dbo.UP_Update_UserJobSkill_WB_OfflineHistory");
		kQuery.PushStrParam(rkCharGuid);
		kQuery.PushStrParam( static_cast< int >(eType) );
		kQuery.PushStrParam(iWorkBenchItemNo);
		kQuery.PushStrParam(iItemNo);
		kQuery.InsertQueryTarget(rkCharGuid);
		kQuery.QueryOwner(rkCharGuid);
		g_kCoreCenter.PushQuery(kQuery);
	}
	void PgWorkBenchOfflineHistoryHelper::Delete(BM::GUID const& rkCharGuid)
	{
		CEL::DB_QUERY kQuery(DT_PLAYER, DQT_UPDATE_USER_JOBSKILL_INFO_NO_OP, L"dbo.UP_Delete_UserJobSkill_WB_OfflineHistory");
		kQuery.PushStrParam(rkCharGuid);
		kQuery.InsertQueryTarget(rkCharGuid);
		kQuery.QueryOwner(rkCharGuid);
		g_kCoreCenter.PushQuery(kQuery);
	}
	void PgWorkBenchOfflineHistoryHelper::Logout(BM::GUID const& rkCharGuid)
	{
		m_kContGuid.erase(rkCharGuid);
	}
};

////
PgJobSkillHomeWorkBenchMgr_Impl::PgJobSkillHomeWorkBenchMgr_Impl()
	: m_kCont(), m_kSaveTimer(JobSkillHomeWorkBenchMgrUtil::iDefaultSaveTerm), m_kLogHelper(), m_kHistoryHelper()
{
	m_iOldTime = g_kEventView.GetLocalSecTime(CGameTime::SECOND);
}
PgJobSkillHomeWorkBenchMgr_Impl::~PgJobSkillHomeWorkBenchMgr_Impl()
{
}

void PgJobSkillHomeWorkBenchMgr_Impl::ProcessPacket( BM::CPacket& rkPacket )
{
	BM::GUID kCharGuid;
	PACKET_ID_TYPE wType = 0;
	rkPacket.Pop( wType );
	rkPacket.Pop( kCharGuid );
	switch( wType )
	{
	case PT_N_N_NFY_LOAD_WORKBENCH_STATUS:
		{
			BM::GUID kHomeGuid(kCharGuid);
			BM::GUID kItemGuid;
			rkPacket.Pop( kItemGuid );

			CONT_MYHOME_WORKBENCH::iterator find_iter = m_kCont.find( kHomeGuid );
			if( m_kCont.end() != find_iter )
			{
				(*find_iter).second.ReadFromPacket(kItemGuid, rkPacket);
				m_kSaveTimer.Add(kHomeGuid, kItemGuid);
				m_kLogHelper.Load(kHomeGuid, kItemGuid); // Log 로딩
			}
		}break;
	case PT_N_M_NFY_JS_WORKBENCH_LOG_LOAD:
		{
			BM::GUID const kHomeGuid(kCharGuid);
			m_kLogHelper.ReadFromPacket(kHomeGuid, rkPacket);
		}break;
	case PT_C_M_REQ_INSERT_TO_WORKBENCH:
		{// 작업대에 아이템 넣기
			EWorkBenchResult const eRet = Recv_PT_C_M_REQ_INSERT_TO_WORKBENCH(kCharGuid, rkPacket);
			{
				BM::CPacket kPacket( PT_N_C_ANS_INSERT_TO_WORKBENCH, kCharGuid );
				kPacket.Push( eRet );
				kPacket.Push( rkPacket );
				if( WBR_SUCCESS == eRet )
				{
					g_kRealmUserMgr.Locked_SendToUserGround(kCharGuid, kPacket, false, true);
				}
				else
				{
					g_kRealmUserMgr.Locked_SendToUser( kCharGuid, kPacket, false );
				}
			}
		}break;
	case PT_C_M_REQ_GET_ITEM_FROM_WORKBENCH:
		{// 아이템 꺼낼래
			EWorkBenchResult const eRet = Recv_PT_C_M_REQ_GET_ITEM_FROM_WORKBENCH(kCharGuid, rkPacket);
			{
				BM::CPacket kPacket( PT_N_C_ANS_GET_ITEM_FROM_WORKBENCH, kCharGuid );
				kPacket.Push( eRet );
				kPacket.Push( rkPacket );
				if( WBR_SUCCESS == eRet )
				{
					g_kRealmUserMgr.Locked_SendToUserGround(kCharGuid, kPacket, false, true);
				}
				else
				{
					g_kRealmUserMgr.Locked_SendToUser( kCharGuid, kPacket, false );
				}
			}
		}break;
	case PT_M_N_REQ_DEL_WORKBENCH:
		{
			EWorkBenchResult const eRet = Recv_PT_M_N_REQ_DEL_WORKBENCH(kCharGuid, rkPacket);
			{
				BM::CPacket kPacket( PT_N_C_ANS_DEL_WORKBENCH, kCharGuid );
				kPacket.Push( eRet );
				kPacket.Push( rkPacket );
				if( WBR_SUCCESS == eRet )
				{
					g_kRealmUserMgr.Locked_SendToUserGround(kCharGuid, kPacket, false, true);
				}
				else
				{
					g_kRealmUserMgr.Locked_SendToUser( kCharGuid, kPacket, false );
				}
			}
		}break;
	case PT_M_N_REQ_ADD_WORKBENCH:
		{
			EWorkBenchResult const eRet = Recv_PT_M_N_REQ_ADD_WORKBENCH(kCharGuid, rkPacket );
			{
				BM::CPacket kPacket( PT_N_C_ANS_ADD_WORKBENCH, kCharGuid );
				kPacket.Push( eRet );
				kPacket.Push( rkPacket );
				if( WBR_SUCCESS == eRet )
				{
					g_kRealmUserMgr.Locked_SendToUserGround(kCharGuid, kPacket, false, true);
				}
				else
				{
					g_kRealmUserMgr.Locked_SendToUser( kCharGuid, kPacket, false );
				}
			}
		}break;
	case PT_M_N_REQ_SET_WORKBENCH_PUBLIC_ALTER:
		{
			EWorkBenchResult const eRet = Recv_PT_M_N_REQ_SET_WORKBENCH_PUBLIC_ALTER( kCharGuid, rkPacket );
			{
				if( WBR_ALREADY_EXIST_UNIQUE_ITEM == eRet )
				{// 이미 존재하면
					BM::CPacket kPacket( PT_N_C_NFY_WORKBENCH_MSG );
					kPacket.Push( WBMTO_ALREADY_EXIST_PUBLIC_ALTER );
					g_kRealmUserMgr.Locked_SendToUser( kCharGuid, kPacket, false );
				}
			}
		}break;
	case PT_M_N_REQ_CLEAR_WORKBENCH_PUBLIC_ALTER:
		{
			EWorkBenchResult const eRet = Recv_PT_M_N_REQ_CLEAR_WORKBENCH_PUBLIC_ALTER(kCharGuid, rkPacket );
		}break;
	case PT_N_N_NFY_REG_HOME_WORKBENCH:
		{
			BM::GUID kHomeGuid(kCharGuid);
			BM::GUID kOwnerGuid;
			rkPacket.Pop( kOwnerGuid );

			CONT_MYHOME_WORKBENCH::_Pairib kRet = m_kCont.insert( std::make_pair(kHomeGuid, CONT_MYHOME_WORKBENCH::mapped_type(kHomeGuid, kOwnerGuid)) );
			if( !kRet.second )
			{
				CONT_MYHOME_WORKBENCH::iterator iter = kRet.first;
				(*iter).second.OwnerGuid(kOwnerGuid);
			}
			if( kOwnerGuid.IsNotNull() )
			{
				BM::CPacket kRetPacket(PT_N_N_NFY_REG_HOME_WORKBENCH_MEMBERID, kHomeGuid);
				CEL::DB_QUERY kQuery(DT_PLAYER, DQT_SELECT_MEMBERID, L"dbo.UP_Select_Character_MemberID");
				kQuery.InsertQueryTarget(kOwnerGuid);
				kQuery.PushStrParam(kOwnerGuid);
				kQuery.contUserData.Push( PMET_JS_WORKBENCH );
				kQuery.contUserData.Push( kRetPacket );
				g_kCoreCenter.PushQuery(kQuery);
			}
		}break;
	case PT_N_N_NFY_REG_HOME_WORKBENCH_MEMBERID:
		{
			BM::GUID const kHomeGuid(kCharGuid);
			BM::GUID kMemberGuid;
			rkPacket.Pop( kMemberGuid );

			CONT_MYHOME_WORKBENCH::iterator find_iter = m_kCont.find( kHomeGuid );
			if( m_kCont.end() != find_iter )
			{
				(*find_iter).second.OwnerMemberGuid(kMemberGuid);
			}
		}break;
	case PT_N_N_NFY_UNREG_HOME_WORKBENCH:
		{
			BM::GUID kHomeGuid(kCharGuid);
			BM::GUID kPreOwnerGuid;
			rkPacket.Pop(kPreOwnerGuid);
			// 가공중인 아이템이 있다면 메일로 쏴야한다
			CONT_MYHOME_WORKBENCH::const_iterator find_iter = m_kCont.find(kHomeGuid);
			if( m_kCont.end() != find_iter )
			{
				std::wstring kFromText, kTitleText, kText;
				GetDefString(MMC_SELL_NOTI_MAIL_FROM,kFromText);
				GetDefString(MMC_SELL_NOTI_MAIL_TITLE,kTitleText);

				typedef PgJobSkillWorkBenchMgr::CONT_JS_WORK_BENCH CONT_JS_WORK_BENCH;
				CONT_JS_WORK_BENCH kContWorkBench = (*find_iter).second.CopyAllWorkBenchInfo();
				CONT_JS_WORK_BENCH::const_iterator wb_iter = kContWorkBench.begin();
				while( kContWorkBench.end() != wb_iter )
				{
					m_kLogHelper.Del(kHomeGuid, (*wb_iter).first);
					CONT_JSWB_UPGRADE_ITEM kContSlotItem = (*wb_iter).second.GetContAll();
					CONT_JSWB_UPGRADE_ITEM::const_iterator slot_iter = kContSlotItem.begin();
					while( kContSlotItem.end() != slot_iter )
					{
						PgBase_Item kItem;
						if( S_OK == CreateSItem((*slot_iter).second.iItemNo, 1, GIOT_NONE, kItem) )
						{
							g_kPostMgr.PostSystemMailByGuid(kPreOwnerGuid, kFromText, kTitleText, kText, kItem, 0);
						}
						++slot_iter;
					}
					++wb_iter;
				}
			}
			m_kCont.erase( kHomeGuid );
			m_kLogHelper.Del(kHomeGuid);
		}break;
	case PT_C_N_REQ_HOME_WORKBENCH_INFO:
		{
			BM::GUID kHomeGuid;
			rkPacket.Pop( kHomeGuid );

			CONT_MYHOME_WORKBENCH::const_iterator find_iter = m_kCont.find( kHomeGuid );
			if( m_kCont.end() != find_iter )
			{
				BM::CPacket kPacket(PT_N_C_ANS_HOME_WORKBENCH_INFO);
				(*find_iter).second.WriteToPacket(kPacket);
				m_kLogHelper.WriteToPacket(kHomeGuid, kPacket);
				g_kRealmUserMgr.Locked_SendToUser( kCharGuid, kPacket, false );
			}
		}break;
	case PT_M_N_GMCMD_WORKBENCH_TURNOVER:
		{// 강제로 모든 작업을 완료 시킨다(GM 커맨드)
			BM::GUID const& rkHomeGuid = kCharGuid;
			CONT_MYHOME_WORKBENCH::iterator find_iter = m_kCont.find( rkHomeGuid );
			if( m_kCont.end() != find_iter )
			{
				PgJobSkillWorkBenchMgr& kWorkBenchMgr = (*find_iter).second;
				kWorkBenchMgr.MakeCompleteAllWorkBenchOnNextTick();
			}
		}break;
	case PT_M_N_GMCMD_WORKBENCH_MAKE_TROUBLE:
		{// 강제로 모든 작업대를 고장낸다(GM 커맨드)
			BM::GUID const& rkHomeGuid = kCharGuid;
			CONT_MYHOME_WORKBENCH::iterator find_iter = m_kCont.find( rkHomeGuid );
			if( m_kCont.end() != find_iter )
			{
				PgJobSkillWorkBenchMgr& kWorkBenchMgr = (*find_iter).second;
				kWorkBenchMgr.MakeTroubleAllWorkBenchOnNextTick();
			}
		}break;
	case PT_M_N_GMCMD_WORKBENCH_SET_TRY_BLESS_TIME:
		{// 강제로 남은 버프 축복 시간을 조정(GM커맨드)
			BM::GUID kHomeGuid;
			rkPacket.Pop(kHomeGuid);
			int iHour = 0;
			rkPacket.Pop(iHour);
			int iMin = 0;
			rkPacket.Pop(iMin);
			int iSec = 0;
			rkPacket.Pop(iSec);
			
			CONT_MYHOME_WORKBENCH::iterator find_iter = m_kCont.find( kHomeGuid );
			if( m_kCont.end() != find_iter )
			{
				PgJobSkillWorkBenchMgr& kWorkBenchMgr = (*find_iter).second;
				__int64 const i64Time = iHour*3600 + iMin*60 + iSec;
				kWorkBenchMgr.SetBlessRemainTime_DelBlessGuid(kCharGuid, i64Time);
				
				//보냄
				BM::CPacket kPacket(PT_N_C_ANS_HOME_WORKBENCH_INFO);
				(*find_iter).second.WriteToPacket(kPacket);
				m_kLogHelper.WriteToPacket(kHomeGuid, kPacket);
				//g_kRealmUserMgr.Locked_SendToUser( kCharGuid, kPacket, false );
				g_kRealmUserMgr.Locked_SendToUserGround(kCharGuid, kPacket, false, true);
			}
		}break;
	case PT_M_N_GMCMD_WORKBENCH_CONTROL_ELAPSTIME:
		{// 강제로 현재 남은 작업 시간을 조절함
			BM::GUID kHomeGuid;
			rkPacket.Pop(kHomeGuid);

			BM::GUID kWorkBenchGuid;
			rkPacket.Pop(kWorkBenchGuid);

			int iHour = 0;
			rkPacket.Pop(iHour);
			int iMin = 0;
			rkPacket.Pop(iMin);
			int iSec = 0;
			rkPacket.Pop(iSec);
			
			CONT_MYHOME_WORKBENCH::iterator find_iter = m_kCont.find( kHomeGuid );
			if( m_kCont.end() != find_iter )
			{
				PgJobSkillWorkBenchMgr& kWorkBenchMgr = (*find_iter).second;
				__int64 const i64Time = iHour*3600 + iMin*60 + iSec;
				kWorkBenchMgr.SetCurUpgradeSlotRemainTime(kWorkBenchGuid, i64Time);
				
				BM::CPacket kPacket(PT_N_C_ANS_HOME_WORKBENCH_INFO);
				(*find_iter).second.WriteToPacket(kPacket);
				m_kLogHelper.WriteToPacket(kHomeGuid, kPacket);
				//g_kRealmUserMgr.Locked_SendToUser( kCharGuid, kPacket, false );
				g_kRealmUserMgr.Locked_SendToUserGround(kCharGuid, kPacket, false, true);
			}
		}break;
	case PT_M_N_GMCMD_WORKBENCH_CONTROL_ITEM_DESTROY_TIME:
		{
			BM::GUID kHomeGuid;
			rkPacket.Pop(kHomeGuid);

			BM::GUID kWorkBenchGuid;
			rkPacket.Pop(kWorkBenchGuid);
			
			int iSlot = 0;
			rkPacket.Pop(iSlot);
			int iHour = 0;
			rkPacket.Pop(iHour);
			int iMin = 0;
			rkPacket.Pop(iMin);
			int iSec = 0;
			rkPacket.Pop(iSec);
			
			CONT_MYHOME_WORKBENCH::iterator find_iter = m_kCont.find( kHomeGuid );
			if( m_kCont.end() != find_iter )
			{
				PgJobSkillWorkBenchMgr& kWorkBenchMgr = (*find_iter).second;
				__int64 const i64Time = iHour*3600 + iMin*60 + iSec;
				kWorkBenchMgr.SetCompleteSlotRemainTime(kWorkBenchGuid, static_cast<size_t>(iSlot), i64Time);
				
				BM::CPacket kPacket(PT_N_C_ANS_HOME_WORKBENCH_INFO);
				(*find_iter).second.WriteToPacket(kPacket);
				m_kLogHelper.WriteToPacket(kHomeGuid, kPacket);
				//g_kRealmUserMgr.Locked_SendToUser( kCharGuid, kPacket, false );
				g_kRealmUserMgr.Locked_SendToUserGround(kCharGuid, kPacket, false, true);
			}
		}break;
	case PT_C_M_REQ_REPAIR_WORKBENCH:
		{// 수리 요청
			std::wstring kCharName;
			BM::GUID kHomeGuid;
			rkPacket.Pop( kCharName );
			rkPacket.Pop( kHomeGuid );
			BM::GUID kWorkBenchGuid;
			rkPacket.Pop( kWorkBenchGuid );

			CONT_MYHOME_WORKBENCH::iterator find_iter = m_kCont.find( kHomeGuid );
			if( m_kCont.end() != find_iter )
			{
				PgJobSkillWorkBenchMgr& kWorkBenchMgr = (*find_iter).second;
				if( kWorkBenchMgr.RepairWorkBench( kWorkBenchGuid ) )
				{
					BM::DBTIMESTAMP_EX kLocalTime;
					g_kEventView.GetLocalTime(kLocalTime);
					SWorkBenchLog const kWorkBenchLog(kLocalTime, WBET_REPAIR, kCharGuid, kCharName);
					m_kLogHelper.Log(kHomeGuid, kWorkBenchGuid, kWorkBenchLog);

					BM::CPacket kPacket(PT_N_C_NFY_HOME_WORKBENCH_INFO);
					kWorkBenchMgr.WriteToPacket(kWorkBenchGuid, kPacket);
					kWorkBenchLog.WriteToPacket(kPacket);
					g_kRealmUserMgr.Locked_SendToUserGround(kCharGuid, kPacket, false, true);

					
					{// 작업대 소유자에게 알림
						BM::CPacket kMsgPacket(PT_N_C_NFY_WORKBENCH_MSG);
						kMsgPacket.Push(WBMTO_REPAIR);
						kMsgPacket.Push(kCharName);
						g_kRealmUserMgr.Locked_SendToUser(kWorkBenchMgr.OwnerGuid(), kMsgPacket, false);	// 클라로 바로감
					}
				}
			}
		}break;
	case PT_C_M_REQ_BLESS_WORKBENCH:
		{
			std::wstring kCharName;
			BM::GUID kHomeGuid, kMemberGuid, kWorkBenchGuid;
			int iHelpJobSkillNo = 0, iUseBlessPoint = 0;
			rkPacket.Pop( kMemberGuid );
			rkPacket.Pop( kCharName );
			rkPacket.Pop( kHomeGuid );
			rkPacket.Pop( kWorkBenchGuid );
			rkPacket.Pop( iHelpJobSkillNo );
			rkPacket.Pop( iUseBlessPoint );
			
			CONT_MYHOME_WORKBENCH::iterator find_iter = m_kCont.find( kHomeGuid );
			if( m_kCont.end() != find_iter )
			{
				PgJobSkillWorkBenchMgr& kWorkBenchMgr = (*find_iter).second;
				if( kCharGuid == kWorkBenchMgr.OwnerGuid()
				||	kMemberGuid == kWorkBenchMgr.OwnerMemberGuid() )
				{// 자기가 자신의 작업대에 축복을 내릴수는 없고
					break;
				}
				if(!kWorkBenchMgr.BlessWorkBench( kWorkBenchGuid, kCharGuid, iHelpJobSkillNo ) )
				{// 버프를 줄 조건이 안되면
					break;
				}
				
				BM::DBTIMESTAMP_EX kLocalTime;
				g_kEventView.GetLocalTime(kLocalTime);
				SWorkBenchLog const kWorkBenchLog(kLocalTime, WBET_BLESS, kCharGuid, kCharName);
				m_kLogHelper.Log(kHomeGuid, kWorkBenchGuid, kWorkBenchLog);
				
				// kCharGuid 숙련도 증가 요청
				

				BM::CPacket kMapPacket(PT_N_M_NFY_BLESS_WORKBENCH);
				kMapPacket.Push( kCharGuid );
				kMapPacket.Push( iHelpJobSkillNo );
				kMapPacket.Push( iUseBlessPoint );
				
				BM::CPacket kPacket(PT_N_C_NFY_HOME_WORKBENCH_INFO);
				kWorkBenchMgr.WriteToPacket(kWorkBenchGuid, kPacket);
				kWorkBenchLog.WriteToPacket(kPacket);

				kMapPacket.Push( kPacket ); // 제작한 패킷을 끼워넣기
				g_kRealmUserMgr.Locked_SendToUserGround(kCharGuid, kMapPacket, false, true);

				SaveToDB(kWorkBenchMgr, kWorkBenchGuid);
				{
					BM::CPacket kMsgPacket(PT_N_C_ANS_BLESS_WORKBENCH_MSG);
					kMsgPacket.Push(kCharGuid);
					g_kRealmUserMgr.Locked_SendToUserGround(kCharGuid, kMsgPacket, false, true);
				}

				{// 작업대 소유자에게 알림
					BM::CPacket kMsgPacket(PT_N_C_NFY_WORKBENCH_MSG);
					kMsgPacket.Push(WBMTO_HELP);
					kMsgPacket.Push(kCharName);
					g_kRealmUserMgr.Locked_SendToUser(kWorkBenchMgr.OwnerGuid(), kMsgPacket, false);	// 클라로 바로감
				}
			}
		}break;
	case PT_T_N_NFY_USER_ENTER_GROUND:
		{
			SContentsUser kUserInfo;
			kUserInfo.ReadFromPacket(rkPacket);
			m_kHistoryHelper.Load(kUserInfo.kCharGuid);
		}break;
	case PT_A_NFY_USER_DISCONNECT:
		{
			SContentsUser kUserInfo;
			kUserInfo.ReadFromPacket(rkPacket);
			m_kHistoryHelper.Logout(kUserInfo.kCharGuid);
		}break;
	case PT_N_N_NFY_JS_WORKBENCH_OFFLINE_HISTORY:
		{
			CONT_WORKBENCH_COMPLETE_INFO kContTempInfo;

			PU::TLoadArray_M(rkPacket, kContTempInfo);

			CONT_WORKBENCH_COMPLETE_INFO::iterator iter = kContTempInfo.begin();
			while( kContTempInfo.end() != iter )
			{
				CONT_WORKBENCH_COMPLETE_INFO::value_type const& rkEventLog = (*iter);
				switch( rkEventLog.eLogType )
				{
				case WBET_BLESS:
					{
						BM::CPacket kNfyPacket(PT_N_M_NFY_JS_WORKBENCH_BLESS_EXP, kCharGuid);
						kNfyPacket.Push( rkEventLog.iBlessSkillNo );
						kNfyPacket.Push( rkEventLog.iWorkItemNo );
						g_kRealmUserMgr.Locked_SendToUserGround(kCharGuid, kNfyPacket, false, true);

						iter = kContTempInfo.erase(iter);
					}break;
				default:
					{
						++iter;
					}break;
				}
			}
			
			BM::CPacket kMapPacket(PT_N_M_NFY_JS_WORKBENCH_COMPLETE_USER, kCharGuid);
			PU::TWriteArray_M(kMapPacket, kContTempInfo);
			g_kRealmUserMgr.Locked_SendToUserGround(kCharGuid, kMapPacket, false, true);

			m_kHistoryHelper.Delete(kCharGuid);
		}break;
	case PT_N_N_REQ_COMMUNITY_STATE_WORKBENCH_FRIEND:
		{
			CONT_MYHOME_WORKBENCH::iterator find_iter = m_kCont.begin();
			while( m_kCont.end() != find_iter )
			{
				if((*find_iter).second.OwnerGuid() == kCharGuid)
				{
					(*find_iter).second.CheckMajorWorkBenchStatus();
					BM::CPacket kRefresh(PT_N_N_NFY_COMMUNITY_STATE_WORKBENCH_FRIEND);
					kRefresh.Push(kCharGuid);
					kRefresh.Push((*find_iter).second.OldMajorStatus());
					::SendToFriendMgr(kRefresh);
					break;
				}
				++find_iter;
			}
		}break;
	case PT_N_N_REQ_COMMUNITY_STATE_WORKBENCH_GUILD:
		{
			CONT_MYHOME_WORKBENCH::iterator find_iter = m_kCont.begin();
			while( m_kCont.end() != find_iter )
			{
				if((*find_iter).second.OwnerGuid() == kCharGuid)
				{
					(*find_iter).second.CheckMajorWorkBenchStatus();
					BM::CPacket kRefresh(PT_N_N_NFY_COMMUNITY_STATE_WORKBENCH_GUILD);
					kRefresh.Push(kCharGuid);
					kRefresh.Push((*find_iter).second.OldMajorStatus());
					::SendToGuildMgr(kRefresh);
					return;
				}
				++find_iter;
			}
		}break;
	case PT_N_N_REQ_COMMUNITY_STATE_WORKBENCH_COUPLE:
		{
			CONT_MYHOME_WORKBENCH::iterator find_iter = m_kCont.begin();
			while( m_kCont.end() != find_iter )
			{
				if((*find_iter).second.OwnerGuid() == kCharGuid)
				{
					(*find_iter).second.CheckMajorWorkBenchStatus();
					BM::CPacket kRefresh(PT_N_N_NFY_COMMUNITY_STATE_WORKBENCH_COUPLE);
					kRefresh.Push(kCharGuid);
					kRefresh.Push((*find_iter).second.OldMajorStatus());
					::SendToCoupleMgr(kRefresh);
					return;
				}
				++find_iter;
			}
		}break;
	default:
		{
			VERIFY_INFO_LOG( false, BM::LOG_LV1, __FL__ << _T(" unknown packet: ") << wType );			
		}break;
	}
}

EWorkBenchResult PgJobSkillHomeWorkBenchMgr_Impl::Recv_PT_M_N_REQ_ADD_WORKBENCH(BM::GUID const& rkCharGuid, BM::CPacket& rkPacket )
{
	BM::GUID kHomeGuid;
	BM::GUID kItemGuid;
	int iItemNo = 0;
	int iDuration = 0;

	rkPacket.Pop( iItemNo );
	rkPacket.Pop( kHomeGuid );
	rkPacket.Pop( kItemGuid );
	rkPacket.Pop( iDuration );

	CONT_MYHOME_WORKBENCH::iterator find_iter = m_kCont.find(kHomeGuid);
	if( m_kCont.end() != find_iter )
	{
		if( (*find_iter).second.OwnerGuid() == rkCharGuid )
		{
			bool bIsAlterWorkBench = false;
			if( (*find_iter).second.Add(kItemGuid, iItemNo, iDuration, bIsAlterWorkBench) )
			{
				m_kSaveTimer.Add(kHomeGuid, kItemGuid);
				(*find_iter).second.WriteToPacket(kItemGuid, rkPacket);
				SaveToDB((*find_iter).second, kItemGuid);
				
				if( true == bIsAlterWorkBench )
				{// 관리작업대라면, 클라이언트 UI 갱신을 위해 모든 정보를 다시 보내줌
					BM::CPacket kNotifyPacket(PT_N_C_ANS_HOME_WORKBENCH_INFO);
					(*find_iter).second.WriteToPacket(kNotifyPacket);
					m_kLogHelper.WriteToPacket(kHomeGuid, kNotifyPacket);
					g_kRealmUserMgr.Locked_SendToUser( rkCharGuid, kNotifyPacket, false );
				}

				return WBR_SUCCESS;
			}
			else
			{
				return WBR_NOT_OWNER;
			}
		}
		else
		{
			return WBR_NOT_OWNER;
		}
	}
	return WBR_SYSTEM_ERROR;
}

EWorkBenchResult PgJobSkillHomeWorkBenchMgr_Impl::Recv_PT_M_N_REQ_DEL_WORKBENCH(BM::GUID const& rkCharGuid, BM::CPacket& rkPacket )
{
	BM::GUID kItemGuid, kHomeGuid;

	rkPacket.Pop( kHomeGuid );
	rkPacket.Pop( kItemGuid );

	CONT_MYHOME_WORKBENCH::iterator find_iter = m_kCont.find(kHomeGuid);
	if( m_kCont.end() != find_iter )
	{
		if( (*find_iter).second.OwnerGuid() == rkCharGuid )
		{
			bool bIsAlterWorkBench = false;
			EWorkBenchResult const eRet = (*find_iter).second.Del(kItemGuid, bIsAlterWorkBench);
			if( WBR_SUCCESS == eRet )
			{
				m_kSaveTimer.Del(kHomeGuid, kItemGuid);
				BM::CPacket kMapPacket(PT_N_M_NFY_DEL_WORKBENCH);
				kMapPacket.Push( rkPacket );
				g_kRealmUserMgr.Locked_SendToUserGround(kHomeGuid, kMapPacket, false, true);

				rkPacket.RdPos( rkPacket.Size() );
				rkPacket.Push( kItemGuid );
				{
					CEL::DB_QUERY kQuery(DT_PLAYER, DQT_UPDATE_USER_JOBSKILL_INFO_NO_OP, L"dbo.up_Delete_WorkBench_status");
					kQuery.PushStrParam( kItemGuid );
					g_kCoreCenter.PushQuery( kQuery );
				}
				m_kLogHelper.Del(kHomeGuid, kItemGuid);

				if( true == bIsAlterWorkBench )
				{// 관리작업대라면, 클라이언트 UI 갱신을 위해 모든 정보를 다시 보내줌
					BM::CPacket kNotifyPacket(PT_N_C_ANS_HOME_WORKBENCH_INFO);
					(*find_iter).second.WriteToPacket(kNotifyPacket);
					m_kLogHelper.WriteToPacket(kHomeGuid, kNotifyPacket);
					g_kRealmUserMgr.Locked_SendToUser( rkCharGuid, kNotifyPacket, false );
				}
			}
			return eRet;
		}
		else
		{
			return WBR_NOT_OWNER;
		}
	}
	return WBR_SYSTEM_ERROR;
}

EWorkBenchResult PgJobSkillHomeWorkBenchMgr_Impl::Recv_PT_C_M_REQ_INSERT_TO_WORKBENCH(BM::GUID const& rkCharGuid, BM::CPacket& rkPacket)
{// 작업대에 아이템 넣기
	BM::GUID kHomeGuid;
	BM::GUID kItemGuid;
	size_t iSlotNo = 0;
	int iItemNo = 0;

	rkPacket.Pop( kHomeGuid );
	rkPacket.Pop( kItemGuid );
	rkPacket.Pop( iSlotNo );
	rkPacket.Pop( iItemNo );

	CONT_MYHOME_WORKBENCH::iterator find_iter = m_kCont.find(kHomeGuid);
	if( m_kCont.end() != find_iter )
	{
		if( (*find_iter).second.OwnerGuid() == rkCharGuid )
		{
			EWorkBenchResult const eRet = (*find_iter).second.Push(kItemGuid, iSlotNo, iItemNo);
			if( WBR_SUCCESS == eRet )
			{
				BM::CPacket kMapPacket(PT_N_M_NFY_INSERT_TO_WORKBENCH);
				kMapPacket.Push( rkPacket );
				g_kRealmUserMgr.Locked_SendToUserGround(kHomeGuid, kMapPacket, false, true);

				rkPacket.RdPos(rkPacket.Size());

				(*find_iter).second.WriteToPacket(kItemGuid, rkPacket);
				SaveToDB((*find_iter).second, kItemGuid);
			}
			return eRet;
		}
		else
		{
			return WBR_NOT_OWNER;
		}
	}
	return WBR_SYSTEM_ERROR;
}
EWorkBenchResult PgJobSkillHomeWorkBenchMgr_Impl::Recv_PT_C_M_REQ_GET_ITEM_FROM_WORKBENCH(BM::GUID const& rkCharGuid, BM::CPacket& rkPacket)
{
	BM::GUID kHomeGuid;
	BM::GUID kItemGuid;
	size_t iSlotNo = 0;
	
	rkPacket.Pop( kHomeGuid );
	rkPacket.Pop( kItemGuid );
	rkPacket.Pop( iSlotNo );

	CONT_MYHOME_WORKBENCH::iterator iter = m_kCont.find( kHomeGuid );
	if( m_kCont.end() != iter )
	{
		if( (*iter).second.OwnerGuid() == rkCharGuid )
		{
			int const iItemNo = (*iter).second.Pop(kItemGuid, iSlotNo);
			if( iItemNo )
			{
				int const iDefaultCount = 1;
				PgBase_Item kItem;
				if( S_OK == CreateSItem( iItemNo, iDefaultCount, GIOT_NONE, kItem ) )
				{
					SActionOrder* pkActionOrder = PgJobWorker::AllocJob();
					pkActionOrder->InsertTarget( rkCharGuid );
					pkActionOrder->kContOrder.push_back( SPMO( IMET_INSERT_FIXED, rkCharGuid, SPMOD_Insert_Fixed( kItem, SItemPos(), true ) ) );
					g_kJobDispatcher.VPush( pkActionOrder );

					(*iter).second.WriteToPacket(kItemGuid, rkPacket);
					return WBR_SUCCESS;
				}
				SaveToDB((*iter).second, kItemGuid);
			}
			else
			{
				return WBR_NOT_EXIST_ITEM;
			}
		}
		else
		{
			return WBR_NOT_OWNER;
		}
	}
	return WBR_SYSTEM_ERROR;
}

EWorkBenchResult PgJobSkillHomeWorkBenchMgr_Impl::Recv_PT_M_N_REQ_SET_WORKBENCH_PUBLIC_ALTER(BM::GUID const& rkCharGuid, BM::CPacket& rkPacket )
{// 관리작업대 정보 설정
	BM::GUID kHomeGuid;
	int iItemNo = 0;
	rkPacket.Pop( iItemNo );
	rkPacket.Pop( kHomeGuid );

	CONT_MYHOME_WORKBENCH::iterator find_iter = m_kCont.find(kHomeGuid);
	if( m_kCont.end() != find_iter )
	{
		if( (*find_iter).second.OwnerGuid() == rkCharGuid )
		{
			if( (*find_iter).second.SetPublicAlterInfo(iItemNo) )
			{// 관리작업대라면, 클라이언트 UI 갱신을 위해 모든 정보를 다시 보내줌
				BM::CPacket kNotifyPacket(PT_N_C_ANS_HOME_WORKBENCH_INFO);
				(*find_iter).second.WriteToPacket(kNotifyPacket);
				m_kLogHelper.WriteToPacket(kHomeGuid, kNotifyPacket);
				g_kRealmUserMgr.Locked_SendToUser( rkCharGuid, kNotifyPacket, false );
				return WBR_SUCCESS;
			}
			return WBR_ALREADY_EXIST_UNIQUE_ITEM;
		}
		else
		{
			return WBR_NOT_OWNER;
		}
	}
	return WBR_SYSTEM_ERROR;
}

EWorkBenchResult PgJobSkillHomeWorkBenchMgr_Impl::Recv_PT_M_N_REQ_CLEAR_WORKBENCH_PUBLIC_ALTER(BM::GUID const& rkCharGuid, BM::CPacket& rkPacket)
{// 관리작업대 정보 설정
	BM::GUID kHomeGuid;
	BM::GUID kItemGuid;
	int iItemNo = 0;

	rkPacket.Pop( iItemNo );
	rkPacket.Pop( kHomeGuid );

	CONT_MYHOME_WORKBENCH::iterator find_iter = m_kCont.find(kHomeGuid);
	if( m_kCont.end() != find_iter )
	{
		if( (*find_iter).second.OwnerGuid() == rkCharGuid )
		{
			if( (*find_iter).second.ClearPublicAlterInfo(iItemNo) )
			{// 관리작업대라면, 클라이언트 UI 갱신을 위해 모든 정보를 다시 보내줌
				BM::CPacket kNotifyPacket(PT_N_C_ANS_HOME_WORKBENCH_INFO);
				(*find_iter).second.WriteToPacket(kNotifyPacket);
				m_kLogHelper.WriteToPacket(kHomeGuid, kNotifyPacket);
				g_kRealmUserMgr.Locked_SendToUser( rkCharGuid, kNotifyPacket, false );
				return WBR_SUCCESS;
			}
		}
		else
		{
			return WBR_NOT_OWNER;
		}
	}
	return WBR_SYSTEM_ERROR;
}

void PgJobSkillHomeWorkBenchMgr_Impl::OnProcess10s()
{
	__int64 const iNowTime = g_kEventView.GetLocalSecTime(CGameTime::SECOND);
	__int64 const iEleapsedTime = BM::DiffTime(m_iOldTime, iNowTime);
	m_iOldTime = iNowTime;

	BM::DBTIMESTAMP_EX kLocalTime;
	g_kEventView.GetLocalTime(kLocalTime);

	CONT_MYHOME_WORKBENCH::iterator iter = m_kCont.begin();
	while( m_kCont.end() != iter )
	{
		CONT_WORKBENCH_DELETE_ITEM kContDeleteItemWorkBench;
		CONT_WORKBENCH_COMPLETE_INFO kContCompleteInfo;
		size_t stWorkedAtThisTimeSlotNo = 0;
		BM::GUID const& rkHomeGuid = (*iter).first;
		(*iter).second.Process(iEleapsedTime, kContCompleteInfo, kContDeleteItemWorkBench, stWorkedAtThisTimeSlotNo); // Tick

		ContGuidSet kSyncItemGuid;
		if( false == kContDeleteItemWorkBench.empty() )
		{
			CONT_WORKBENCH_DELETE_ITEM::iterator itemguid_iter = kContDeleteItemWorkBench.begin();
			while( kContDeleteItemWorkBench.end() != itemguid_iter )
			{
				BM::GUID const& rkItemGuid = (*itemguid_iter).first;
				SWorkBenchLog const kDeleteLog(kLocalTime, WBET_MISSING_ITEM, (*itemguid_iter).second, 0);
				BM::CPacket kPacket(PT_N_C_NFY_HOME_WORKBENCH_INFO);
				(*iter).second.WriteToPacket(rkItemGuid, kPacket);
				kDeleteLog.WriteToPacket(kPacket);
				m_kLogHelper.Log(rkHomeGuid, rkItemGuid, kDeleteLog);
				g_kRealmUserMgr.Locked_SendToUserGround(rkHomeGuid, kPacket, false, true);
				kSyncItemGuid.insert(rkItemGuid);

				SaveToDB((*iter).second, rkItemGuid); // 상태 변환시 저장

				++itemguid_iter;
			}
		}
		if( false == kContCompleteInfo.empty() )
		{
			BM::CPacket kMapPacket(PT_N_M_NFY_JS_WORKBENCH_COMPLETE_USER, (*iter).second.OwnerGuid());
			PU::TWriteArray_M(kMapPacket, kContCompleteInfo);
			bool const bProcessOnline = g_kRealmUserMgr.Locked_SendToUserGround((*iter).second.OwnerGuid(), kMapPacket, false, true);

			BM::CPacket kMapPacket2(PT_N_M_NFY_JS_WORKBENCH_COMPLETE_HOME, rkHomeGuid);
			PU::TWriteArray_M(kMapPacket2, kContCompleteInfo);
			g_kRealmUserMgr.Locked_SendToUserGround(rkHomeGuid, kMapPacket2, false, true);

			CONT_WORKBENCH_COMPLETE_INFO::const_iterator comp_iter = kContCompleteInfo.begin();
			while( kContCompleteInfo.end() != comp_iter )
			{
				CONT_WORKBENCH_COMPLETE_INFO::value_type const& rkEndTurnInfo = (*comp_iter);
				switch( rkEndTurnInfo.eLogType )
				{
				case WBET_BLESS_END:
				case WBET_TROUBLE:
				case WBET_FIND_NEXT_SLOT:
					{
						SWorkBenchLog const kTroubleLog(kLocalTime, rkEndTurnInfo.eLogType);
						m_kLogHelper.Log(rkHomeGuid, rkEndTurnInfo.kWorkBenchGuid, kTroubleLog);
						{
							if( kSyncItemGuid.end() == kSyncItemGuid.find(rkEndTurnInfo.kWorkBenchGuid))
							{ // 정보 동기화되지 않은 객채면 로그와 함께 동기화
								kSyncItemGuid.insert(rkEndTurnInfo.kWorkBenchGuid);
								BM::CPacket kPacket(PT_N_C_NFY_HOME_WORKBENCH_INFO);
								(*iter).second.WriteToPacket(rkEndTurnInfo.kWorkBenchGuid, kPacket);
								kTroubleLog.WriteToPacket(kPacket);
								g_kRealmUserMgr.Locked_SendToUserGround(rkHomeGuid, kPacket, false, true);
							}
							else
							{ // 이미 정보 동기화된 객채면 로그만 전송
								if( false == JobSkillWorkBenchUtil::IsSystemLog(kTroubleLog.eEventType) )
								{
									BM::CPacket kPacket(PT_N_C_NFY_JS_WORKBENCH_LOG, rkEndTurnInfo.kWorkBenchGuid);
									kTroubleLog.WriteToPacket(kPacket);
									g_kRealmUserMgr.Locked_SendToUserGround(rkHomeGuid, kPacket, false, true);
								}
							}
							if( WBET_TROUBLE ==  rkEndTurnInfo.eLogType )
							{// 작업대 소유자에게 알림
								BM::CPacket kMsgPacket(PT_N_C_NFY_WORKBENCH_MSG);
								kMsgPacket.Push(WBMTO_TROUBLE);
								g_kRealmUserMgr.Locked_SendToUser(iter->second.OwnerGuid(), kMsgPacket, false);	// 클라로 바로감
							}
						}
					}break;
				case WBET_AUTO_REPAIR:
				case WBET_MGR_AUTO_REPAIR:
					{//자동 수리 로그 남기고
						BM::DBTIMESTAMP_EX kLocalTime;
						g_kEventView.GetLocalTime(kLocalTime);
						SWorkBenchLog const kAutoRepairLog(kLocalTime, rkEndTurnInfo.eLogType);
						m_kLogHelper.Log(rkHomeGuid, rkEndTurnInfo.kWorkBenchGuid, kAutoRepairLog);
						// 마이홈 존에 알리고
						BM::CPacket kPacket(PT_N_C_NFY_JS_WORKBENCH_LOG, rkEndTurnInfo.kWorkBenchGuid);
						kAutoRepairLog.WriteToPacket(kPacket);
						g_kRealmUserMgr.Locked_SendToUserGround(rkHomeGuid, kPacket, false, true);
						{// 작업대 소유자에게 알림
							BM::CPacket kMsgPacket(PT_N_C_NFY_WORKBENCH_MSG);
							EWorkBenchMsgToOwner eLog = (rkEndTurnInfo.eLogType == WBET_AUTO_REPAIR) ? WBMTO_AUTO_REPAIR : WBMTO_MGR_AUTO_REPAIR;
							kMsgPacket.Push(eLog);// 자동수리
							kMsgPacket.Push(rkEndTurnInfo.kWorkBenchGuid);
							g_kRealmUserMgr.Locked_SendToUser(iter->second.OwnerGuid(), kMsgPacket, false);	// 클라로 바로감
						}
					}break;
				default:
					{
						SWorkBenchLog const kEventLog(kLocalTime, rkEndTurnInfo.eLogType, rkEndTurnInfo.iWorkItemNo, rkEndTurnInfo.iResultItemNo);
						m_kLogHelper.Log(rkHomeGuid, rkEndTurnInfo.kWorkBenchGuid, kEventLog);
						{
							if( kSyncItemGuid.end() == kSyncItemGuid.find(rkEndTurnInfo.kWorkBenchGuid))
							{ // 정보 동기화되지 않은 객채면 로그와 함께 동기화
								kSyncItemGuid.insert(rkEndTurnInfo.kWorkBenchGuid);
								BM::CPacket kPacket(PT_N_C_NFY_HOME_WORKBENCH_INFO);
								(*iter).second.WriteToPacket(rkEndTurnInfo.kWorkBenchGuid, kPacket);
								kEventLog.WriteToPacket(kPacket);
								g_kRealmUserMgr.Locked_SendToUserGround(rkHomeGuid, kPacket, false, true);
							}
							else
							{ // 이미 정보 동기화된 객채면 로그만 전송
								BM::CPacket kPacket(PT_N_C_NFY_JS_WORKBENCH_LOG, rkEndTurnInfo.kWorkBenchGuid);
								kEventLog.WriteToPacket(kPacket);
								g_kRealmUserMgr.Locked_SendToUserGround(rkHomeGuid, kPacket, false, true);
							}
						}
						
						if( JobSkillWorkBenchUtil::IsExpertnessLog(rkEndTurnInfo.eLogType) )
						{
							if( 0 == JobSkillWorkBenchUtil::GetRemainUpgradeCount(rkEndTurnInfo.iResultItemNo) )
							{// 아이템 업그레이드가 완료되었을 경우
								bool const bAutoMailSend = (*iter).second.PublicAlterInfo().IsCompleteItemSendMail()	// 모든 기계에 완료시 메일보내기가 있는가
									|| (*iter).second.IsCompleteItemSendMail( rkEndTurnInfo.kWorkBenchGuid );			// 기계에 완료시 메일보내기가 있는가
								
								SWorkBenchLog kEndLog(kLocalTime, (bAutoMailSend ? WBET_COMPLETE_ITEM_SEND_MAIL : WBET_END), rkEndTurnInfo.iResultItemNo, 0);
								m_kLogHelper.Log(rkHomeGuid, rkEndTurnInfo.kWorkBenchGuid, kEndLog);

								EWorkBenchMsgToOwner eMsgType = WBMTO_UPGRADE_COMPLETE;
								if( bAutoMailSend )
								{// 완료 아이템 자동 메일 발송 기능이 존재하면
									(*iter).second.Pop(rkEndTurnInfo.kWorkBenchGuid, stWorkedAtThisTimeSlotNo);	// 아이템 pop 시켜서 작업대로부터 제거하고

									{// 완료 아이템을 메일로 발송
										short const siDefaultItemCnt = 1;
										PgJobSkillWorkBench kWorkBench;
										if(true == (*iter).second.GetWorkBench(rkEndTurnInfo.kWorkBenchGuid, kWorkBench) )
										{
											std::wstring kWorkBenchName;
											::GetItemName(kWorkBench.ItemNo(), kWorkBenchName);

											std::wstring kItemName;
											::GetItemName(rkEndTurnInfo.iResultItemNo, kItemName);

											BM::vstring kMailFrom;
											GetDefString(JOB_SKILL_ITEM_UPGRADE_RESULT_ITEM_FROM,kMailFrom);
											kMailFrom.Replace(L"#NAME#", kWorkBenchName);

											BM::vstring kMailTitle;
											GetDefString(JOB_SKILL_ITEM_UPGRADE_RESULT_ITEM_TITLE, kMailTitle);

											BM::vstring kMailText;
											GetDefString(JOB_SKILL_ITEM_UPGRADE_RESULT_ITEM_TEXT, kMailText);
											kMailText.Replace(L"#MONTH#", static_cast<int>(kLocalTime.month));
											kMailText.Replace(L"#DAY#", static_cast<int>(kLocalTime.day));
											kMailText.Replace(L"#HOUR#", static_cast<int>(kLocalTime.hour));
											kMailText.Replace(L"#MIN#", static_cast<int>(kLocalTime.minute));
											kMailText.Replace(L"#ITEM_NAME#", kItemName);

											SActionOrder* pkActionOrder = PgJobWorker::AllocJob();
											pkActionOrder->kCause = CNE_POST_SYSTEM_MAIL;
											pkActionOrder->kPacket2nd.Push(iter->second.OwnerGuid());
											pkActionOrder->kPacket2nd.Push(std::wstring());
											pkActionOrder->kPacket2nd.Push(static_cast<std::wstring>(kMailTitle));
											pkActionOrder->kPacket2nd.Push(static_cast<std::wstring>(kMailText));
											pkActionOrder->kPacket2nd.Push(rkEndTurnInfo.iResultItemNo);
											pkActionOrder->kPacket2nd.Push(siDefaultItemCnt);
											pkActionOrder->kPacket2nd.Push(static_cast<int>(0));
											pkActionOrder->kPacket2nd.Push(static_cast<std::wstring>(kMailFrom));
											g_kJobDispatcher.VPush(pkActionOrder);
										}
									}
									eMsgType = WBMTO_COMPLETE_ITEM_SEND_MAIL;									// 작업대 소유자에게 메일 발송됐다고 알림

									BM::CPacket kPacket(PT_N_C_NFY_HOME_WORKBENCH_INFO);
									(*iter).second.WriteToPacket(rkEndTurnInfo.kWorkBenchGuid, kPacket);
									kEndLog.WriteToPacket(kPacket);
									g_kRealmUserMgr.Locked_SendToUserGround(rkHomeGuid, kPacket, false, true);
								}
								else
								{
									BM::CPacket kPacket(PT_N_C_NFY_JS_WORKBENCH_LOG, rkEndTurnInfo.kWorkBenchGuid);
									kEndLog.WriteToPacket(kPacket);
									g_kRealmUserMgr.Locked_SendToUserGround(rkHomeGuid, kPacket, false, true);		// 로그 갱신 알림
								}

								BM::CPacket kMsgPacket(PT_N_C_NFY_WORKBENCH_MSG);
								kMsgPacket.Push(eMsgType);
								kMsgPacket.Push(rkEndTurnInfo.iResultItemNo);
								g_kRealmUserMgr.Locked_SendToUser(iter->second.OwnerGuid(), kMsgPacket, false);	// 클라로 바로 알림 메세지 전달
							}
							if( false == bProcessOnline )
							{ // 온라인 처리에 실패 했으면 오프라인 처리
								m_kHistoryHelper.AddComplete(rkEndTurnInfo.kOwnerGuid, rkEndTurnInfo.iWorkBenchItemNo, rkEndTurnInfo.iWorkItemNo);
							}
							if( 0 == rkEndTurnInfo.iDuration )
							{// 내구도 0 이되었을때
								SWorkBenchLog const kStopLog(kLocalTime, WBET_STOP);
								m_kLogHelper.Log(rkHomeGuid, rkEndTurnInfo.kWorkBenchGuid, kStopLog);
								{ // 내구도는 성공 메시지와 항상 함께 발생되어 이미 정보가 동기화 되었기 때문에 항상 로그만 전송
									BM::CPacket kPacket(PT_N_C_NFY_JS_WORKBENCH_LOG, rkEndTurnInfo.kWorkBenchGuid);
									kStopLog.WriteToPacket(kPacket);
									g_kRealmUserMgr.Locked_SendToUserGround(rkHomeGuid, kPacket, false, true);
								}
								{// 작업대 소유자에게 알림
									BM::CPacket kMsgPacket(PT_N_C_NFY_WORKBENCH_MSG);
									kMsgPacket.Push(WBMTO_DURATION_ZERO);
									g_kRealmUserMgr.Locked_SendToUser(iter->second.OwnerGuid(), kMsgPacket, false);	// 클라로 바로감
								}
							}
							if( BM::GUID::IsNotNull(rkEndTurnInfo.kBlessCharGuid)
							&&	0 < rkEndTurnInfo.iBlessSkillNo )
							{// 한턴 끝나서 대상자에게 도움스킬로 준 숙련도 증가시키는 부분
								int iAdjustExertnessRate = 0;
								{// 추가적으로 숙련도를 올려주는 기능이 있는가
									PgJobSkillWorkBenchAlterInfo const& kPublicAlter = (*iter).second.PublicAlterInfo();
									PgJobSkillWorkBench kWorkBench;
									(*iter).second.GetWorkBench(rkEndTurnInfo.kWorkBenchGuid, kWorkBench);
									iAdjustExertnessRate = kPublicAlter.GetExertnessRate() + kWorkBench.AlterInfo().GetExertnessRate();
								}

								BM::CPacket kBlessExpPacket(PT_N_M_NFY_JS_WORKBENCH_BLESS_EXP, rkEndTurnInfo.kBlessCharGuid);
								kBlessExpPacket.Push( rkEndTurnInfo.iBlessSkillNo );
								kBlessExpPacket.Push( rkEndTurnInfo.iWorkItemNo );
								kBlessExpPacket.Push( iAdjustExertnessRate );
								if( false == g_kRealmUserMgr.Locked_SendToUserGround(rkEndTurnInfo.kBlessCharGuid, kBlessExpPacket, false, true) )
								{
									m_kHistoryHelper.AddBless(rkEndTurnInfo.kBlessCharGuid, rkEndTurnInfo.iBlessSkillNo, rkEndTurnInfo.iWorkItemNo);
								}
							}
						}
					}
				}
				++comp_iter;
			}
		}
		//마이홈의 작업대 대표 상태값 받아오기;
		if( iter->second.CheckMajorWorkBenchStatus() )
		{//이전 상태값과 비교하여, 다르면 커뮤니티에 메세지 보내기.
			SendToCommunity_MyHome_Workbench_MajorStatus(iter->second.OwnerGuid(), static_cast<int>(iter->second.OldMajorStatus()));
		}
		++iter;
	}
	JobSkillHomeWorkBenchMgrUtil::CONT_OUT kOut;
	if( m_kSaveTimer.Tick(iEleapsedTime, kOut) )
	{
		JobSkillHomeWorkBenchMgrUtil::CONT_OUT::iterator ret_iter = kOut.begin();
		while( kOut.end() != ret_iter )
		{
			SaveToDB((*ret_iter).first, (*ret_iter).second, false);
			++ret_iter;
		}
	}
}

void PgJobSkillHomeWorkBenchMgr_Impl::SendToCommunity_MyHome_Workbench_MajorStatus(BM::GUID const &rkOwnerGuid, int const eStatusOut)//대표 상태 변화 여부 확인(변했으면, 상태값 리턴)
{
	//친구 갱신 요청
	{
		BM::CPacket kRefresh(PT_N_N_NFY_COMMUNITY_STATE_WORKBENCH_FRIEND);
		kRefresh.Push(rkOwnerGuid);
		kRefresh.Push(eStatusOut);
		::SendToFriendMgr(kRefresh);
	}
	{//길드 갱신 요청
		BM::CPacket kRefresh(PT_N_N_NFY_COMMUNITY_STATE_WORKBENCH_GUILD);
		kRefresh.Push(rkOwnerGuid);
		kRefresh.Push(eStatusOut);
		::SendToGuildMgr(kRefresh);
	}
	{//커플 갱신 요청
		BM::CPacket kRefresh(PT_N_N_NFY_COMMUNITY_STATE_WORKBENCH_COUPLE);
		kRefresh.Push(rkOwnerGuid);
		kRefresh.Push(eStatusOut);
		::SendToCoupleMgr(kRefresh);
	}
}

void PgJobSkillHomeWorkBenchMgr_Impl::SaveToDB(BM::GUID const& rkHomeGuid, BM::GUID const& rkItemGuid, bool const bResetSaveTime)
{
	CONT_MYHOME_WORKBENCH::const_iterator find_iter = m_kCont.find( rkHomeGuid );
	if( m_kCont.end() != find_iter )
	{
		SaveToDB( (*find_iter).second, rkItemGuid, bResetSaveTime );
	}
}

void PgJobSkillHomeWorkBenchMgr_Impl::SaveToDB(CONT_MYHOME_WORKBENCH::mapped_type const& rkWorkBenchMgr, BM::GUID const& rkItemGuid, bool const bResetSaveTime)
{
	CEL::DB_QUERY kQuery(DT_PLAYER, DQT_UPDATE_USER_JOBSKILL_INFO_NO_OP, L"dbo.UP_Update_WorkBench_Status");
	if( rkWorkBenchMgr.WriteToDB(rkItemGuid, kQuery) )
	{
		g_kCoreCenter.PushQuery(kQuery);
		if( bResetSaveTime )
		{
			m_kSaveTimer.Reset(rkWorkBenchMgr.HomeGuid(), rkItemGuid);
		}
	}
}

////

PgJobSkillHomeWorkBenchMgr::PgJobSkillHomeWorkBenchMgr()
{
}
PgJobSkillHomeWorkBenchMgr::~PgJobSkillHomeWorkBenchMgr()
{
}
void PgJobSkillHomeWorkBenchMgr::ProcessPacket( BM::CPacket& rkPacket )
{
	BM::CAutoMutex kLock(m_kMutex_Wrapper_);
	Instance()->ProcessPacket(rkPacket);
}
void PgJobSkillHomeWorkBenchMgr::OnProcess10s()
{
	BM::CAutoMutex kLock(m_kMutex_Wrapper_);
	Instance()->OnProcess10s();
}