#ifndef CONTENTS_CONTENTSSERVER_CONTENTS_JOBSKILL_PGJOBSKILLHOMEWORKBENCHMGR_H
#define CONTENTS_CONTENTSSERVER_CONTENTS_JOBSKILL_PGJOBSKILLHOMEWORKBENCHMGR_H

namespace JobSkillHomeWorkBenchMgrUtil
{
	typedef std::list< std::pair< BM::GUID, BM::GUID > > CONT_OUT;

	//
	class PgSaveTimer
	{
		typedef std::map< std::pair< BM::GUID, BM::GUID >, __int64 > CONT_TIMER; // < Home, Item >, 시간
	public:
		PgSaveTimer(__int64 const iDefaultTeam);
		~PgSaveTimer();

		bool Tick(__int64 const iEleapsedTime, CONT_OUT& rkOut);
		void Add(BM::GUID const& rkFirst, BM::GUID const& rkSecond);
		void Del(BM::GUID const& rkFirst, BM::GUID const& rkSecond);
		bool Reset(BM::GUID const& rkFirst, BM::GUID const& rkSecond);

	private:
		CONT_TIMER m_kCont;
		__int64 const m_iDefaultTerm;
	};

	//
	class PgWorkBenchLogHelper
	{
		typedef std::map< BM::GUID, CONT_JS_WORKBENCH_EVENT > CONT_JS_WORKBENCH_EVENT_LOG; // HomeGuid, Item Event
	public:
		PgWorkBenchLogHelper();
		~PgWorkBenchLogHelper();

		void Log(BM::GUID const& rkHomeGuid, BM::GUID const& rkItemGuid, SWorkBenchLog const& rkLog);
		void Load(BM::GUID const& rkHomeGuid, BM::GUID const& rkItemGuid);
		void Truncate(BM::GUID const& rkItemGuid, SWorkBenchLog const& rkBaseLog);
		void ReadFromPacket(BM::GUID const& rkHomeGuid, BM::CPacket& rkPacket);
		bool WriteToPacket(BM::GUID const& rkHomeGuid, BM::CPacket& rkPacket) const;
		bool WriteToPacket(BM::GUID const& rkHomeGuid, BM::GUID const& rkItemGuid, BM::CPacket& rkPacket) const;

		void Del(BM::GUID const& rkHomeGuid);
		void Del(BM::GUID const& rkHomeGuid, BM::GUID const& rkItemGuid);

	private:
		CONT_JS_WORKBENCH_EVENT_LOG m_kContEvent;
		BM::GUID m_kMngGuid;
	};

	//
	enum EJobSkillWorkBenchHistoryType : int
	{
		JSWBHT_WORK_COMPLETE	= 0,
		JSWBHT_BLESS			= 1,
	};
	class PgWorkBenchOfflineHistoryHelper
	{
	public:
		PgWorkBenchOfflineHistoryHelper();
		~PgWorkBenchOfflineHistoryHelper();

		void Load(BM::GUID const& rkCharGuid);
		void AddComplete(BM::GUID const& rkCharGuid, int const iWorkBenchItemNo, int const iItemNo);
		void AddBless(BM::GUID const& rkCharGuid, int const iBlessSkillNo, int const iItemNo);
		void Logout(BM::GUID const& rkCharGuid);
		void Delete(BM::GUID const& rkCharGuid);
	protected:
		void Add(EJobSkillWorkBenchHistoryType const eType, BM::GUID const& rkCharGuid, int const iWorkBenchItemNo, int const iItemNo);

	private:
		ContGuidSet m_kContGuid;
	};
};

class PgJobSkillWorkBenchMgr;
//
class PgJobSkillHomeWorkBenchMgr_Impl
{
	typedef std::map< BM::GUID, PgJobSkillWorkBenchMgr > CONT_MYHOME_WORKBENCH; // HomeGuid, All WorkBench
public:
	PgJobSkillHomeWorkBenchMgr_Impl();
	~PgJobSkillHomeWorkBenchMgr_Impl();

	void ProcessPacket( BM::CPacket& rkPacket );// 패킷처리
	void OnProcess10s(); // call 10second
	void SendToCommunity_MyHome_Workbench_MajorStatus(BM::GUID const &rkOwnerGuid, int const eStatusOut);//대표 상태 변화 여부 확인(변했으면, 상태값 리턴)

protected:
	EWorkBenchResult Recv_PT_M_N_REQ_ADD_WORKBENCH(BM::GUID const& rkCharGuid, BM::CPacket& rkPacket );			// 작업대 설치
	EWorkBenchResult Recv_PT_M_N_REQ_DEL_WORKBENCH(BM::GUID const& rkCharGuid, BM::CPacket& rkPacket );			// 작업대 제거
	EWorkBenchResult Recv_PT_C_M_REQ_GET_ITEM_FROM_WORKBENCH(BM::GUID const& rkCharGuid, BM::CPacket& rkPacket);	// 아이템 꺼내기
	EWorkBenchResult Recv_PT_C_M_REQ_INSERT_TO_WORKBENCH(BM::GUID const& rkCharGuid, BM::CPacket& rkPacket);		// 아이템 넣기
	
	EWorkBenchResult Recv_PT_M_N_REQ_SET_WORKBENCH_PUBLIC_ALTER(BM::GUID const& rkCharGuid, BM::CPacket& rkPacket);	// 관리 작업대 정보 설정
	EWorkBenchResult Recv_PT_M_N_REQ_CLEAR_WORKBENCH_PUBLIC_ALTER(BM::GUID const& rkCharGuid, BM::CPacket& rkPacket);	// 관리 작업대 정보 삭제

	void SaveToDB(BM::GUID const& rkHomeGuid, BM::GUID const& rkItemGuid, bool const bResetSaveTime = true);
	void SaveToDB(CONT_MYHOME_WORKBENCH::mapped_type const& rkWorkBenchMgr, BM::GUID const& rkItemGuid, bool const bResetSaveTime = true);
private:
	CONT_MYHOME_WORKBENCH m_kCont;
	__int64 m_iOldTime;
	JobSkillHomeWorkBenchMgrUtil::PgSaveTimer m_kSaveTimer;
	JobSkillHomeWorkBenchMgrUtil::PgWorkBenchLogHelper m_kLogHelper;
	JobSkillHomeWorkBenchMgrUtil::PgWorkBenchOfflineHistoryHelper m_kHistoryHelper;
};

// 작업대 관련 모든 작업은 이클래스를 통해서 할 것.
//class PgJobSkillWorkBench;
//class PgJobSkillWorkBenchMgr
//{
//	typedef std::map< BM::GUID, PgJobSkillWorkBench > CONT_JOBSKILL_WORKBENCH; // key는 작업대의 GUID	
//public:
//	PgJobSkillWorkBenchMgr();
//	~PgJobSkillWorkBenchMgr();
//
//	void ProcessPacket( BM::CPacket& rkPacket );// 패킷처리
//	void OnProcess10s(); // call 10second
//	void SaveDB1Min(); // 1분마다 가공중인 아이템, 작업대 상태 저장
//
//private:
//	void Recv_PT_M_N_REQ_WORKBENCH_START( BM::CPacket& rkPacket );			// 가공 시작
//	void Recv_PT_M_N_REQ_WORKBENCH_END( BM::CPacket& rkPacket );			// 가공 종료(멈춤)
//	
//	
//
//private:
//	CONT_JOBSKILL_WORKBENCH m_kContWorkBench;
//};

class PgJobSkillHomeWorkBenchMgr : public TWrapper< PgJobSkillHomeWorkBenchMgr_Impl, Loki::Mutex >
{
public:
	PgJobSkillHomeWorkBenchMgr();
	~PgJobSkillHomeWorkBenchMgr();

	void ProcessPacket( BM::CPacket& rkPacket );
	void OnProcess10s();
};

#endif // CONTENTS_CONTENTSSERVER_CONTENTS_JOBSKILL_PGJOBSKILLHOMEWORKBENCHMGR_H