#ifndef CONTENTS_CONTENTSSERVER_DATABASE_PGGUILDDB_H
#define CONTENTS_CONTENTSSERVER_DATABASE_PGGUILDDB_H

// In Cneter

namespace DBR_Community
{
	//New (���� Procedure)
	bool Q_DQT_GUILD_SELECT_BASIC_INFO(CEL::DB_RESULT &rkResult);//One Guild Info
	bool Q_DQT_GUILD_PROC(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_INV_AUTHORITY(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_MEMBER_PROC(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_CHECK_NAME(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_RENAME(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_SELECT_OWNER_LAST_LOGIN_DAY(CEL::DB_RESULT &rkResult, bool const bInit);
	bool Q_DQT_GUILD_SELECT_NEXT_OWNER(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_COMMON(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_MERCENARY_SAVE(CEL::DB_RESULT &rkResult);
	
	bool Q_DQT_GUILD_ENTRANCEOPEN_SAVE(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_ENTRANCEOPEN_LIST(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_REQ_ENTRANCE(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_REQ_ENTRANCE_CANCEL(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_APPLICANT_LIST(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_ENTRANCE_PROCESS(CEL::DB_RESULT &rkResult);
	//bool Q_DQT_GUILD_ENTRANCE_REJECT(CEL::DB_RESULT &rkResult);

	//Old (�� ���� Procedure)
	bool Q_DQT_GUILD_UPDATE_MEMBER_GRADE(CEL::DB_RESULT &rkResult);
	//bool Q_DQT_GUILD_UPDATE_TAX_RATE(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_UPDATE_NOTICE(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_UPDATE_EXP_LEVEL(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_INVENTORY_CREATE(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_INVENTORY_LOAD(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_INVENTORY_LOG_INSERT(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_INVENTORY_LOG_SELECT(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_INVENTORY_LOG_DELETE(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_INVENTORY_EXTEND(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_CHANGE_OWNER(CEL::DB_RESULT &rkResult);
	bool Q_DQT_GUILD_SENDMAIL(CEL::DB_RESULT &rkResult);

	// Couple
	bool Q_DQT_COUPLE_INFO_SELECT(CEL::DB_RESULT &rkResult);
	bool Q_DQT_COUPLE_UPDATE(CEL::DB_RESULT &rkResult);
};

#endif // CONTENTS_CONTENTSSERVER_DATABASE_PGGUILDDB_H