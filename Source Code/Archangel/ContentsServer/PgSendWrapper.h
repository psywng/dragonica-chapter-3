#ifndef CONTENTS_CONTENTSSERVER_NETWORK_PGSENDWRAPPER_H
#define CONTENTS_CONTENTSSERVER_NETWORK_PGSENDWRAPPER_H

extern bool SetSendWrapper(SERVER_IDENTITY const &kRecvSI);

extern bool SendToServerType(CEL::E_SESSION_TYPE const eServerType, BM::CPacket const &rkPacket);
extern bool SendToServer(SERVER_IDENTITY const &kSI, BM::CPacket const &rkPacket);

extern bool SendToImmigration(BM::CPacket const &rkPacket);
extern bool SendToCenter(short const nChannel, BM::CPacket const &rkPacket);
extern bool SendToLog(BM::CPacket const &rkPacket);
extern bool SendToRealmContents(EContentsMessageType eType, BM::CPacket const &rkPacket);
extern bool SendToItem(SERVER_IDENTITY const &kSI, SGroundKey const &kGndKey, BM::CPacket const &rkPacket);

extern bool SendToGround(short const sChannel, SGroundKey const &kKey, BM::CPacket const &rkPacket, bool const bIsGndWrap=true);
extern bool SendToChannel( short const sChannelNo, BM::CPacket const &rkPacket );
extern bool SendToPacketHandler(BM::CPacket const &rkPacket);

extern bool SendToFriendMgr(BM::CPacket const &rkPacket);
extern bool SendToRankMgr(BM::CPacket const &rkPacket);
extern bool SendToGuildMgr(BM::CPacket const &rkPacket);
extern bool SendToCoupleMgr(BM::CPacket const &rkPacket);
extern bool SendToRealmChatMgr(BM::CPacket const &rkPacket);
extern bool SendToNotice(BM::CPacket const &rkPacket);
extern bool SendToOXGuizEvent(BM::CPacket const &rkPacket);
extern bool SendToLuckyStarEvent(BM::CPacket const &rkPacket);
extern bool SendToCouponEventDoc(BM::CPacket const &rkPacket);
extern bool SendToCouponEventView(BM::CPacket const &rkPacket);
extern bool SendToMissionMgr(short sChannel, BM::CPacket const &rkPacket);
extern bool SendToGlobalPartyMgr(short sChannel, BM::CPacket const &rkPacket);
extern bool SendToChannelContents(short sChannel, EContentsMessageType eType, BM::CPacket const &rkPacket, int const iSecondType = 0 );
extern bool SendToGround(SERVER_IDENTITY const &kSI, SGroundKey const &kGndKey, BM::CPacket const &rkPacket);
extern bool SendToMyhomeMgr(BM::CPacket const &rkPacket);

#endif // CONTENTS_CONTENTSSERVER_NETWORK_PGSENDWRAPPER_H