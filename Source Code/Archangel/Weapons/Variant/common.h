#pragma once

#include <stack>

#include "def.h"
#include "Effect.h"
#include "EnchantInfo.h"
#include "IDObject.h"
#include "IGObject.h"
#include "Item.h"
//#include "ItemDropper.h"
#include "MonsterDef.h"
#include "Skill.h"
#include "ItemDefMgr.h"
#include "ItemSetDefMgr.h"
#include "Unit.h"
#include "TableDataManager.h"
