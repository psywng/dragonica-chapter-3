#include "stdafx.h"
#include "Variant/tabledatamanager.h"
//#include "Variant/PetSkillDataConverter.h"
#include "Global.h"
#include "PgControlDefMgr.h"
#include "PgPlayer.h"
#include "PgSubPlayer.h"

PgSubPlayer::PgSubPlayer()
{
}

PgSubPlayer::~PgSubPlayer()
{
}

void PgSubPlayer::Init()
{
	PgControlUnit::Init();
}

//HRESULT Create( CUnit * pkOwner, CONT_PET_MAPMOVE_DATA::mapped_type &kPetData )
HRESULT PgSubPlayer::Create( CUnit * pkOwner )
{
	if ( pkOwner )
	{
//		PgItem_PetInfo *pkPetInfo = NULL;
//		if ( true == kPetItem.GetExtInfo( pkPetInfo ) )
		{
			CUnit::SetAbil( AT_LEVEL, pkOwner->GetAbil(AT_LEVEL) );
			CUnit::SetAbil( AT_CLASS, 10000 );
			CUnit::SetAbil( AT_GENDER, pkOwner->GetAbil(AT_GENDER) );
			
			SetAbil( AT_C_STR, pkOwner->GetAbil(AT_C_STR) );
			SetAbil( AT_C_INT, pkOwner->GetAbil(AT_C_INT) );
			SetAbil( AT_C_CON, pkOwner->GetAbil(AT_C_CON) );
			SetAbil( AT_C_DEX, pkOwner->GetAbil(AT_C_DEX) );

			SetAbil( AT_C_ATTACK_SPEED, pkOwner->GetAbil(AT_C_ATTACK_SPEED) );
			
			SetAbil( AT_C_PHY_ATTACK, pkOwner->GetAbil(AT_C_PHY_ATTACK) );
			SetAbil( AT_C_MAGIC_ATTACK, pkOwner->GetAbil(AT_C_MAGIC_ATTACK) );
			SetAbil( AT_C_HITRATE, pkOwner->GetAbil(AT_C_HITRATE) );
			SetAbil( AT_C_CRITICAL_RATE, pkOwner->GetAbil(AT_C_CRITICAL_RATE) );
			SetAbil( AT_C_CRITICAL_POWER, pkOwner->GetAbil(AT_C_CRITICAL_POWER) );
			
			SetAbil( AT_C_PHY_ATTACK_MIN, pkOwner->GetAbil(AT_C_PHY_ATTACK_MIN) );
			SetAbil( AT_C_PHY_ATTACK_MAX, pkOwner->GetAbil(AT_C_PHY_ATTACK_MAX) );
			SetAbil( AT_PHY_DMG_PER2, pkOwner->GetAbil(AT_PHY_DMG_PER2) );

			SetAbil( AT_C_MAGIC_ATTACK_MIN, pkOwner->GetAbil(AT_C_MAGIC_ATTACK_MIN) );
			SetAbil( AT_C_MAGIC_ATTACK_MAX, pkOwner->GetAbil(AT_C_MAGIC_ATTACK_MAX) );
			SetAbil( AT_MAGIC_DMG_PER2, pkOwner->GetAbil(AT_MAGIC_DMG_PER2) );

			//GET_DEF( PgClassPetDefMgr, kClassPetDefMgr);
			//PgClassPetDef kPetDef;
			//SClassKey const kPetClassKey(GetClassKey());
//			if ( true == kClassPetDefMgr.GetDef( kPetClassKey, &kPetDef ) )
			{
				//Name( pkPetInfo->Name() );
				//m_kInv.OwnerGuid( GetID() );
				//m_kInv.OwnerName( Name() );

				//SetBasicAbil(kPetDef);

				//SetAbil64( AT_EXPERIENCE, pkPetInfo->Exp() );
				//m_i64PeriodSecTime = ( (kPetItem.EnchantInfo().IsTimeLimit()) ? kPetItem.GetDelTime() : _I64_MAX );

				//SetAbil( AT_MP, ( -1 < kPetData.iMP ? kPetData.iMP : pkPetInfo->GetAbil(AT_MP)) );
				//SetAbil( AT_COLOR_INDEX, pkPetInfo->GetAbil(AT_COLOR_INDEX) );

				//PgItem_PetInfo::SStateValue const kHealth( pkPetInfo->GetState_Health() );
				//PgItem_PetInfo::SStateValue const kMental( pkPetInfo->GetState_Mental() );
				//SetState_Health( kHealth );
				//SetState_Mental( kMental );

				//// Pet Skill
				//CONT_DEFCLASS_PET_SKILL const *pkDefPetSkill = NULL;
				//g_kTblDataMgr.GetContDef(pkDefPetSkill);

				//m_kMySkill.Init();
				//CONT_DEFCLASS_PET_SKILL::const_iterator skill_itr = pkDefPetSkill->find( kPetDef.GetSkillDefID() );
				//if ( skill_itr != pkDefPetSkill->end() )
				//{// 여긴 뭘까..?????????????????????????????????????????????.
				//	CONT_DEFCLASS_PET_SKILL::mapped_type const &kContElement = skill_itr->second;
				//	size_t const index = kContElement.find( PgDefClassPetSkillFinder(kPetClassKey.nLv) );
				//	if ( BM::PgApproximate::npos != index )
				//	{
				//		CONT_DEFCLASS_PET_SKILL::mapped_type::value_type const &kElement = kContElement.at(index);
				//		PgPetSkillDataSet::InitSkill( kElement, this, pkPetInfo );
				//	}
				//}
				//m_Skill.InitCoolTime( kPetData.kSkillCoolTime );

				//m_kInv.Swap( kPetData.kInventory );
				//InvenRefreshAbil();
				//	
				//if ( kPetData.kAddOnPacket.Size() )
				//{
				//	kPetData.kAddOnPacket.Pop( m_i64NextExpUpSecTime );
				//	m_kEffect.ReadFromPacket( kPetData.kAddOnPacket, false );
				//}
				//else
				//{
				//	switch( GetPetType() )
				//	{
				//	case EPET_TYPE_1:
				//		{
				//			m_i64NextExpUpSecTime = _I64_MAX;
				//		}break;
				//	case EPET_TYPE_2:
				//		{
				//			__int64 const i64CurTime = g_kEventView.GetLocalSecTime( CGameTime::SECOND );
				//			m_i64NextExpUpSecTime = i64CurTime + ms_i64ExpUpPeriodSecTime;
				//		}break;
				//	}
				//}

				SetState(US_IDLE);
				SetPos( pkOwner->GetPos() );
				FrontDirection( pkOwner->FrontDirection() );
				Caller( pkOwner->GetID() );
				SetAbil( AT_OWNER_TYPE, pkOwner->GetAbil(AT_OWNER_TYPE) );
				
				return S_OK;
			}
		}	
	}

	return E_FAIL;
}

void PgSubPlayer::WriteToPacket(BM::CPacket &rkPacket, EWRITETYPE const kWriteType)const
{
	CUnit::WriteToPacket( rkPacket, kWriteType );

	rkPacket.Push( m_kCommon.sLevel );// AT_LEVEL
	rkPacket.Push( m_kCommon.byGender);// AT_GENDER
	rkPacket.Push( m_kCommon.iClass);// AT_CLASS

	rkPacket.Push( m_kCaller );
//	rkPacket.Push( m_i64PeriodSecTime );
//	rkPacket.Push( m_kName );

	rkPacket.Push( GetAbil(AT_C_ATTACK_SPEED) );

	rkPacket.Push( GetAbil(AT_C_STR) );
	rkPacket.Push( GetAbil(AT_C_INT) );
	rkPacket.Push( GetAbil(AT_C_CON) );
	rkPacket.Push( GetAbil(AT_C_DEX) );

	rkPacket.Push( GetAbil(AT_C_PHY_ATTACK) );
	rkPacket.Push( GetAbil(AT_C_MAGIC_ATTACK) );
	rkPacket.Push( GetAbil(AT_C_HITRATE) );
	rkPacket.Push( GetAbil(AT_C_CRITICAL_RATE) );
	rkPacket.Push( GetAbil(AT_C_CRITICAL_POWER) );
	
	rkPacket.Push( GetAbil(AT_C_PHY_ATTACK_MIN) );
	rkPacket.Push( GetAbil(AT_C_PHY_ATTACK_MAX) );
	rkPacket.Push( GetAbil(AT_PHY_DMG_PER2) );
	
	rkPacket.Push( GetAbil(AT_C_MAGIC_ATTACK_MIN) );
	rkPacket.Push( GetAbil(AT_C_MAGIC_ATTACK_MAX) );
	rkPacket.Push( GetAbil(AT_MAGIC_DMG_PER2) );

	rkPacket.Push( static_cast<BYTE>(GetAbil(AT_TEAM)) );

	rkPacket.Push( GetPos().x );
	rkPacket.Push( GetPos().y );
	rkPacket.Push( GetPos().z );



	m_kMySkill.WriteToPacket( WT_DEFAULT, rkPacket );
	m_Skill.WriteToPacket( rkPacket, kWriteType );

	switch( WTCHK_TYPE & kWriteType )
	{
	case WT_SIMPLE:
		{
			m_kInv.WriteToPacket( IT_FIT, rkPacket );
		}break;
	case WT_DEFAULT:
	case WT_DEFAULT_WITH_EXCLUDE:
	default:
		{
			m_kInv.WriteToPacket( rkPacket, kWriteType );
		}break;
	}
}

EWRITETYPE PgSubPlayer::ReadFromPacket(BM::CPacket &rkPacket)
{
	EWRITETYPE const kWriteType = CUnit::ReadFromPacket(rkPacket);
	
	rkPacket.Pop( m_kCommon.sLevel );// AT_LEVEL
	rkPacket.Pop( m_kCommon.byGender);// AT_GENDER
	rkPacket.Pop( m_kCommon.iClass);// AT_CLASS
	SetBasicAbil();

	rkPacket.Pop( m_kCaller );
//	rkPacket.Pop( m_i64PeriodSecTime );
//	rkPacket.Pop( m_kName );
	
	int iValue = 0;
	rkPacket.Pop( iValue );	SetAbil( AT_C_ATTACK_SPEED, iValue );
	
	rkPacket.Pop( iValue );	SetAbil( AT_C_STR, iValue );
	rkPacket.Pop( iValue );	SetAbil( AT_C_INT, iValue );
	rkPacket.Pop( iValue );	SetAbil( AT_C_CON, iValue );
	rkPacket.Pop( iValue );	SetAbil( AT_C_DEX, iValue );

	rkPacket.Pop( iValue );	SetAbil( AT_C_PHY_ATTACK, iValue );
	rkPacket.Pop( iValue );	SetAbil( AT_C_MAGIC_ATTACK, iValue );
	rkPacket.Pop( iValue );	SetAbil( AT_C_HITRATE, iValue );
	rkPacket.Pop( iValue );	SetAbil( AT_C_CRITICAL_RATE, iValue );
	rkPacket.Pop( iValue );	SetAbil( AT_C_CRITICAL_POWER, iValue );
	
	rkPacket.Pop( iValue );	SetAbil( AT_C_PHY_ATTACK_MIN, iValue );
	rkPacket.Pop( iValue );	SetAbil( AT_C_PHY_ATTACK_MAX, iValue );
	rkPacket.Pop( iValue );	SetAbil( AT_PHY_DMG_PER2, iValue );
	
	rkPacket.Pop( iValue );	SetAbil( AT_C_MAGIC_ATTACK_MIN, iValue );
	rkPacket.Pop( iValue );	SetAbil( AT_C_MAGIC_ATTACK_MAX, iValue );
	rkPacket.Pop( iValue );	SetAbil( AT_MAGIC_DMG_PER2, iValue );

	BYTE byTeam = 0;
	rkPacket.Pop( byTeam );	SetAbil( AT_TEAM, static_cast<int>(byTeam) );

	rkPacket.Pop( m_Pos.x );
	rkPacket.Pop( m_Pos.y );
	rkPacket.Pop( m_Pos.z );

	m_kMySkill.ReadFromPacket( rkPacket );
	m_Skill.ReadFromPacket( rkPacket, kWriteType );
	m_kInv.OwnerGuid( GetID() );
	m_kInv.OwnerName( Name() );

	switch( WTCHK_TYPE & kWriteType )
	{
	case WT_SIMPLE:
		{
			m_kInv.ReadFromPacket( IT_FIT, rkPacket );
		}break;
	case WT_DEFAULT:
	case WT_DEFAULT_WITH_EXCLUDE:
	default:
		{
			m_kInv.ReadFromPacket( rkPacket, kWriteType );
		}break;
	}

	InvenRefreshAbil();
	return kWriteType;
}

int PgSubPlayer::GetAbil(WORD const Type) const
{
	int iValue = 0;
	switch ( Type )
	{
	default:
		{
			iValue = CUnit::GetAbil( Type );
		}break;
	}
	return iValue;
}

bool PgSubPlayer::SetAbil(WORD const Type, int const iInValue, bool const bIsSend, bool const bBroadcast)
{
	switch ( Type )
	{
	case AT_CLASS:
	case AT_LEVEL:
	case AT_BATTLE_LEVEL:
		{
			return PgControlUnit::SetAbil( Type, iInValue, bIsSend, bBroadcast );
		}break;
	default:
		{
			return CUnit::SetAbil( Type, iInValue, bIsSend, bBroadcast );
		}break;
	}
	return true;
}

bool PgSubPlayer::AddAbil(WORD const Type, int const iValue)
{
	if ( 0 == iValue )
	{
		return false;
	}

	switch ( Type )
	{
	default:
		{
			return CUnit::AddAbil( Type, iValue );
		}break;
	}

	return true;
}

// SubPlayer가 Effect를 거는 경우가 있다면, PgPet의 m_bAddPetToOwner을 참고하고 작업 할껄
//protected:
bool PgSubPlayer::SetBasicAbil()
{
	return true;
}

void PgSubPlayer::SetBasicAbil(PgClassPetDef const &kPetDef)
{
}

bool PgSubPlayer::DoLevelup( SClassKey const& rkNewLevelKey )
{
	return true;
}

bool PgSubPlayer::DoBattleLevelUp( short const nBattleLv )
{
	return true;
}

void PgSubPlayer::VOnRefreshAbil()
{
//	WORD const wType[] = {	AT_LEVEL, AT_CLASS, AT_BATTLE_LEVEL };
//	SendAbiles(wType, 3, E_SENDTYPE_BROADALL);//레벨업/전직시 스탯을 모두에게 보내주어라
}