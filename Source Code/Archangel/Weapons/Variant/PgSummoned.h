#ifndef WEAPON_VARIANT_PGSUMMONED_H
#define WEAPON_VARIANT_PGSUMMONED_H

#include "PgControlUnit.h"

typedef SCreateEntity	CreateSummoned_;
typedef SEntityInfo		SummonedInfo_;

class CMonsterDef;
class PgSummoned
	: public PgControlUnit
{
public:
	PgSummoned();
	virtual ~PgSummoned();

	virtual HRESULT Create(const void* pInfo);
	virtual EUnitType UnitType() const;
	virtual void Init();

	unsigned long LifeTime() const;
	void LifeTime(unsigned long const kTime);

	virtual void WriteToPacket(BM::CPacket &rkPacket, EWRITETYPE const kWriteType=WT_DEFAULT)const;
	virtual EWRITETYPE ReadFromPacket(BM::CPacket &rkPacket);

	virtual bool SetAbil(WORD const Type, int const iValue, bool const bIsSend = false, bool const bBroadcast = false);
	virtual int GetAbil(WORD const Type) const;

protected:
	virtual bool DoLevelup( SClassKey const& rkNewLevelKey ) { return true; }
	virtual bool DoBattleLevelUp( short const ) { return true; }

	void CopyAbilFromClassDef( CLASS_DEF_BUILT const *pDef);
private:
	bool SetInfo(SummonedInfo_ const * pInfo);
	void SetInfoFromDefaultSummonedInfo(DefaultSummonedInfo_ const &rkInfo);
//	void SetCommonAbil():

	unsigned long m_kLifeTime;
};

#endif //WEAPON_VARIANT_PGSUMMONED_H