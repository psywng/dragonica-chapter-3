#ifndef WEAPON_VARIANT_QUEST_PGBASEQUESTMNG_H
#define WEAPON_VARIANT_QUEST_PGBASEQUESTMNG_H

typedef std::map< int, ContQuestID > ContQuestGroup;
class PgBaseQuestMng
{
public:
	PgBaseQuestMng();
	virtual ~PgBaseQuestMng();

	void Clear();
	bool Build(CONT_DEF_QUEST_REWARD const* pkDefCont, CONT_DEF_QUEST_RESET_SCHEDULE const* pkDefResetSchedule);

	ContQuestID const* GetGroup(int const iGroupNo)const;
	bool IsHaveResetSchedule(int const iQuestID) const;

	CLASS_DECLARATION_S_NO_SET(ContQuestID, DayQuestAll);				// 1�� ����Ʈ
	CLASS_DECLARATION_S_NO_SET(ContQuestID, DeletedQuestAll);			// ���� ���� ����Ʈ
	CLASS_DECLARATION_S_NO_SET(ContQuestResetSchedule, ResetSchedule);	// ����Ʈ ���� ������
	CLASS_DECLARATION_S_NO_SET(ContQuestGroup, QuestGroup);				// �׷� ����Ʈ
	CLASS_DECLARATION_S_NO_SET(ContQuestID, CoupleQuestAll);			// Ŀ�� ����Ʈ ���
private:
};

#endif // WEAPON_VARIANT_QUEST_PGBASEQUESTMNG_H