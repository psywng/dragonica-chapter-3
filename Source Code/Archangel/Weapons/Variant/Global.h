#ifndef WEAPON_VARIANT_BASIC_GLOBAL_H
#define WEAPON_VARIANT_BASIC_GLOBAL_H

#include "loki/typemanip.h"
#include "BM/Point.h"
#include "idobject.h"
#include "unit.h"

#include "Global.inl"

class PgActionResult;
class PgActionResultVector;
class UNIT_PTR_ARRAY;
class CSkillDef;

// 액티브 스킬 사용시 영향을 받는 자식 스킬의 최대치
int const CHILD_SKILL_MAX = 10;

extern int GetGroggyPoint(CSkillDef const* pkSkillDef);
extern bool CheckBalanceWStringLen( std::wstring const &wstr, size_t const iLen );// 영문의경우 iLen사이즈의 2배를 적용한다.
extern int CS_GetReflectDamage(CUnit* pkCaster, CUnit* pkTarget, int const iDamage, bool bPhysicSkill);
extern int CS_GetReflectDamage_FromItem(CUnit* pkCaster, CUnit* pkTarget, int const iDamage, bool bPhysicSkill);
extern int CS_CheckDamage(CUnit* pkCaster, CUnit* pkTarget, int iDamage, bool bPhysical, PgActionResult* pkResult, int const iSkillRate, bool const bProjectileDmg = false, CSkillDef const* pkSkillDef = NULL);
extern int CS_DoReflectDamage(CUnit const* pkCaster, CUnit const* pkTarget, int const iSkillNo, int const iReflected);
extern bool CS_GetDmgResult(int const iSkillNo, CUnit* pkCaster, CUnit* pkTarget, int const iDmgRate, int const iDmg, PgActionResult* pkResult);
extern bool CS_GetSkillResultDefault(int const iSkillNo, CUnit* pkCaster, UNIT_PTR_ARRAY& rkTargetArray, PgActionResultVector* pkResultVector);
extern WORD GetBasicAbil(WORD const wAbil);
extern WORD GetCalculateAbil(WORD const wAbil);
extern bool IsCalculateAbilRange(WORD const wAbil);
extern bool IsRateAbil(WORD const wAbil);
extern bool IsCalculatedAbil(WORD const wAbil);
extern bool IsMonsterCardAbil(WORD const wAbil);
extern bool IsHitAbilFromEquipItem(WORD const wAbil);	// 장비에서 세팅해주는 '타격시 걸리는 이펙트'에 사용되는 어빌인가?
extern bool IsCountAbil( WORD const wAbil );
extern bool IsItemActionAbil(WORD const wAbil);
extern int GetCountAbil( CAbilObject const *pkAbilObj, WORD const wType, int const iFlag );
extern bool AddCountAbil( CAbilObject *pkAbilObj, WORD const wType, int const iFlag, bool const bAdd );
extern bool RemoveCountAbil( CAbilObject *pkAbilObj, WORD const wType, int const iFlag );
extern int GetCountAbilCheckFlag( int const iFlag );
extern void POINT3_2_POINT3BY(POINT3 const &rkPos, POINT3BY& rkOut);
extern WORD GetRateAbil(WORD const wAbil);
extern DWORD DifftimeGetTime(DWORD const& dwStart, DWORD const& dwEnd);
extern int SetDamageDelay(CSkillDef const *pkSkillDef, CUnit const* pkCaster, CUnit* pkTarget);
extern int CalcDecHitRate(CUnit const* pkCaster, CUnit const* pkTarget, int const iHitRate, int const iDecDodgeAbs = 0, int const iDecDodgeRate = 0);
extern void AddDamageEffect(CUnit* pkCaster, CUnit* pkTarget, CSkillDef const* pkSkillDef, PgActionResult* pkResult);
extern int CS_CheckDmgPer(CUnit* pkTarget, int const iDamage, bool bPhysicSkill);
extern int CS_CheckItemDamage( CUnit *pkCaster, CUnit *pkTarget, bool const bIsCritical, int iDamage );
extern bool IsHaveSkillDamage(CSkillDef const* pkSkillDef);
extern bool IsDamageAction(CUnit const * const pkUnit, CSkillDef const * const pkSkillDef);

//타겟 리스트르 참고하여 데미지를 대신 입는 유닛이 있을 경우 ActionResultVec를 갱신한다.
extern void CheckActionResultVec(UNIT_PTR_ARRAY& rkTargetArray, PgActionResultVector& kActionResultVec);

//도둑 기본 공격 2/4타 
extern bool CS_GetSkillResult103201201(int const iSkillNo, CUnit* pkCaster, UNIT_PTR_ARRAY& rkTargetArray, PgActionResultVector* pkResultVector); 
//투사 룰렛
extern bool CS_GetSkillResult106000101(int const iSkillNo, CUnit* pkCaster, UNIT_PTR_ARRAY& rkTargetArray, PgActionResultVector* pkResultVector); 
//전사 조인트 브레이크 (데미지없이 AT_DAM_EFFECT_S 만 추가 하고 싶을 경우 사용 조인트/플래시뱅 등)
extern bool CS_GetSkillResult105300501(int const iSkillNo, CUnit* pkCaster, UNIT_PTR_ARRAY& rkTargetArray, PgActionResultVector* pkResultVector); 
//트랩퍼 MP제로 폭발
extern bool CS_GetSkillResult1100028011(int const iSkillNo, CUnit* pkCaster, UNIT_PTR_ARRAY& rkTargetArray, PgActionResultVector* pkResultVector); 

bool GetItemName(int const iItemNo, std::wstring & rkOut);

namespace PgNpcTalkUtil
{
	extern float const fNPC_TALK_ENABLE_RANGE;
}

namespace PgArrayUtil
{
	template< typename _TYPE, size_t _ARRAY_SIZE >
	size_t GetArrayCount(_TYPE (&abyArray)[_ARRAY_SIZE])
	{
		return _ARRAY_SIZE;
	}

	template< typename _TYPE, size_t _ARRAY_SIZE >
	_TYPE* GetEndArray(_TYPE (&abyArray)[_ARRAY_SIZE]) //주의 Last Element가 아닌, End Point다
	{
		return abyArray + _ARRAY_SIZE;
	}

	template< typename _TYPE, size_t _ARRAY_SIZE >
	bool IsInArray(_TYPE* pCur, _TYPE (&abyArray)[_ARRAY_SIZE])
	{
		return abyArray <= pCur && &abyArray[_ARRAY_SIZE] > pCur;
	}
}

namespace PgHometownPortalUtil
{
	__int64 UsePortalCost(int const iLevel);
}

template< typename T > struct SCalcValueRate;

template< >
struct SCalcValueRate<int>{typedef __int64 TypeCalcValueRate;};

template< >
struct SCalcValueRate<__int64>{typedef __int64 TypeCalcValueRate;};

struct SRateControl
{
	typedef enum
	{
		E_RATE_VALUE,
		E_RATE_RATE,
	};

	static bool IsActivate( WORD const wType, CUnit *pkCaster )
	{
		int const iRate = pkCaster->GetAbil( wType );
		if ( 0 < iRate )
		{
			int const iRand = static_cast<int>(pkCaster->GetRandom() % static_cast<DWORD>(ABILITY_RATE_VALUE));
			return (iRand <= iRate);
		}

		return false;
	}

	template< int iType >
	static int Get( WORD const wType, CUnit *pkCaster, int const iValue )
	{
		if ( true == IsActivate( wType, pkCaster ) )
		{
			return GetResult( Loki::Int2Type<iType>(), wType+1, pkCaster, iValue );
		}
		return 0;
	}

	template< int iType >
	static int GetResult( WORD const wType, CUnit *pkCaster, int const iValue )
	{	
		return GetResult( Loki::Int2Type<iType>(), wType, pkCaster, iValue );
	}

	static int GetResult( Loki::Int2Type<E_RATE_VALUE>, WORD const wType, CUnit *pkCaster, int const iValue )
	{
		return pkCaster->GetAbil( wType );
	}

	static int GetResult( Loki::Int2Type<E_RATE_RATE>, WORD const wType, CUnit *pkCaster, int const iValue )
	{
		int const iAddRate = pkCaster->GetAbil( wType );
		return GetValueRate( iValue, iAddRate );
	}

	template< typename T >
	static T GetValueRate( T const iBasicValue, T const iAddRate )
	{
		typedef typename SCalcValueRate<T>::TypeCalcValueRate TYPE_CAST;

		TYPE_CAST const iCastValue = static_cast<TYPE_CAST>(iBasicValue) * static_cast<TYPE_CAST>(iAddRate);
		return static_cast<T>( iCastValue / static_cast<TYPE_CAST>(ABILITY_RATE_VALUE64) );
	}
};

namespace PgMyHomeFuncRate
{
	int		GetHomeFuncItemValue(eMyHomeSideJob const kSideJob, eMyHomeSideJobRateType const kJobRateType, CUnit * pkUnit);
	double	GetCostRate(eMyHomeSideJob const kSideJob, CUnit * pkUnit);
	double	GetSoulRate(eMyHomeSideJob const kSideJob, CUnit * pkUnit);
	int		GetSuccessRate(eMyHomeSideJob const kSideJob, CUnit * pkUnit);
}

namespace PgSkillHelpFunc
{
	// 반사 데미지 값 얻어오는 함수
	int CalcReflectDamage(CUnit* pkCaster, CUnit* pkTarget,int const iDamage, PgActionResult* pkActionResult, CSkillDef const* pkSkillDef, bool const bPhysicDmg, bool const bOffRandomSeed, bool const bCompelCalc=false);
	
	// 스킬 조정 데미지
	int CalcAdjustSkillValue(EAdjustSkillCalcType const eCalcType, int const iSkillNo, CUnit* pkCaster, int const iOriginValue);
	bool IsApplySkill(int const iSkillNo, CEffect* pkEffect);	// CalcAdjustSkillValue 함수에서 iSkillNo을 pkEffect에서 제어하는지 확인해주는 함수
	int GetAddtionalAdjustSkillValue(CUnit* pkUnit, int const iAbilType, CEffect* pkEffect, int iValue);	// CalcAdjustSkillValue 함수에서 어빌값에 따라 실값을 계산해주는 함수
	int GetAttackRange(CUnit * pkUnit, CSkillDef const* pkDef);
	int CalcAttackRange(CUnit * pkUnit, int const iSkillRange);
}

namespace LOCAL_MGR
{
	class CLocal; // 클래스 전방 선언
}

namespace PgXmlLocalUtil
{
	extern char const* LOCAL_ELEMENT_NAME;
	TiXmlNode const* FindInLocal(LOCAL_MGR::CLocal& rkLocalMgr, TiXmlNode const* pkLocalNode, char const* szContents = "NULL");
	TiXmlElement const* FindInLocalResult(LOCAL_MGR::CLocal& rkLocalMgr, TiXmlElement const* pkLocalNode);
}

// SubPlayer(쌍둥이와 같은 보조 캐릭터)를 가지고 있는 class인가
bool IsClass_OwnSubPlayer(__int64 const i64Class);
#endif // WEAPON_VARIANT_BASIC_GLOBAL_H