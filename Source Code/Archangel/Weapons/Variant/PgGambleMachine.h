#ifndef WEAPON_VARIANT_ITEM_PGGAMBLEMACHINE_H
#define WEAPON_VARIANT_ITEM_PGGAMBLEMACHINE_H

#include "Lohengrin/packetstruct.h"
#include "BM/twrapper.h"

typedef enum : BYTE
{
	GCG_NONE	= 0,
	GCG_GOLD	= 1,
	GCG_SILVER	= 2,
	GCG_BRONZE	= 3,
	GCG_CASHSHOP = 4,// 캐시샵에서 사용하는 겜블 코인
	
	// 코스튬 조합
	GCG_COSTUMEMIX_NORMAL = 11,	// 노멀
	GCG_COSTUMEMIX_ADV	  = 12,	// 고급
	GCG_COSTUMEMIX_SPCL	  = 13,	// 스페셜
	GCG_COSTUMEMIX_LEGND  = 14,	// 레전드(현재 미사용)
}eGambleCoinGrade;

typedef struct tagGAMBLEMACHINEMIXUPELEMENT
{
	tagGAMBLEMACHINEMIXUPELEMENT():iGradeNo(0),iRate(0){}
	int iGradeNo;
	int iRate;
}SGAMBLEMACHINEMIXUPELEMENT;

typedef std::vector<SGAMBLEMACHINEMIXUPELEMENT> CONT_GAMEBLEMACHINEMIXUPLELEMENT;

typedef std::map<int,int> CONT_GAMBLEMACHINEMIXUPPOINT;

typedef struct tagGAMBLEMACHINEMIXUP
{
	tagGAMBLEMACHINEMIXUP():iMixPoint(0){}

	int iMixPoint;
	CONT_GAMEBLEMACHINEMIXUPLELEMENT kCont;

	size_t min_size() const
	{
		return sizeof(int) + sizeof(size_t);
	}

	void WriteToPacket(BM::CPacket & kPacket) const
	{
		kPacket.Push(iMixPoint);
		PU::TWriteArray_A(kPacket, kCont);
	}

	void ReadFromPacket(BM::CPacket & kPacket)
	{
		kPacket.Pop(iMixPoint);
		PU::TLoadArray_A(kPacket, kCont);
	}
}SGAMBLEMACHINEMIXUP;

typedef std::map<int,SGAMBLEMACHINEMIXUP> CONT_GAMBLEMACHINEMIXUP;

typedef struct tagGAMBLEMACHINEITEM
{
	tagGAMBLEMACHINEITEM():
	iItemNo(0),
	siCount(0),
	iRate(0),
	bTimeType(0),
	siUseTime(0),
	bBroadcast(false),
	iGroupNo(0){}

	int iItemNo;
	short siCount;
	int iRate;
	BYTE bTimeType;
	short siUseTime;
	bool bBroadcast;
	int iGroupNo;
}SGAMBLEMACHINEITEM;

typedef struct tagGAMBLEMACHINEGROUPRES
{
	int iGroupNo;
	BYTE bU,bV;
	short siUVIndex;
	std::wstring kTitle;
	std::wstring kDescription;
	std::wstring kIconPath;
	int	iDisplayRank;

	tagGAMBLEMACHINEGROUPRES():iGroupNo(0),bU(0),bV(0),siUVIndex(0),iDisplayRank(0){}

	size_t min_size() const
	{
		return sizeof(int) + sizeof(BYTE) + sizeof(BYTE) + sizeof(short) + sizeof(size_t) + sizeof(size_t) + sizeof(int);
	}

	void WriteToPacket(BM::CPacket & kPacket) const
	{
		kPacket.Push(iGroupNo);
		kPacket.Push(bU);
		kPacket.Push(bV);
		kPacket.Push(siUVIndex);
		kPacket.Push(kTitle);
		kPacket.Push(kDescription);
		kPacket.Push(kIconPath);
		kPacket.Push(iDisplayRank);
	}

	void ReadFromPacket(BM::CPacket & kPacket)
	{
		kPacket.Pop(iGroupNo);
		kPacket.Pop(bU);
		kPacket.Pop(bV);
		kPacket.Pop(siUVIndex);
		kPacket.Pop(kTitle);
		kPacket.Pop(kDescription);
		kPacket.Pop(kIconPath);
		kPacket.Pop(iDisplayRank);
	}

}SGAMBLEMACHINEGROUPRES;

typedef std::vector<SGAMBLEMACHINEITEM> CONT_GAMBLEITEM;

typedef struct tagGAMBLEITEMBAG
{
	tagGAMBLEITEMBAG():iTotalRate(0){}
	int iTotalRate;
	CONT_GAMBLEITEM	kCont;

	void Clear()
	{
		kCont.clear();
		iTotalRate = 0;
	}

	size_t min_size() const
	{
		return sizeof(int) + sizeof(size_t);
	}

	void WriteToPacket(BM::CPacket & kPacket) const
	{
		PU::TWriteArray_A(kPacket,kCont);
	}
	void ReadFromPacket(BM::CPacket & kPacket)
	{
		Clear();
		PU::TLoadArray_A(kPacket,kCont);
		for(CONT_GAMBLEITEM::const_iterator iter = kCont.begin();iter != kCont.end();++iter)
		{
			iTotalRate += (*iter).iRate;
		}
	}
}SGAMBLEITEMBAG;

typedef std::map<eGambleCoinGrade,SGAMBLEITEMBAG>	CONT_GAMBLEMACHINE;
typedef std::map<BYTE,int>							CONT_GAMBLEMACHINECOST;
typedef std::vector<PgBase_Item>					CONT_GAMBLEMACHINERESULT;
typedef std::map<int,SGAMBLEMACHINEGROUPRES>		CONT_GAMBLEMACHINEGROUPRES;

class PgGambleMachineImpl
{
	CONT_GAMBLEMACHINE	m_kCont;
	CONT_GAMBLEMACHINECOST m_kContCost;
	CONT_GAMBLEMACHINEGROUPRES m_kContGroup;
	CONT_GAMBLEMACHINEMIXUP m_kContMixup;
	CONT_GAMBLEMACHINEMIXUPPOINT m_kContMixupPoint;

public:

	PgGambleMachineImpl();
	~PgGambleMachineImpl();

public:

	void WriteToPacket(BM::CPacket & kPacket);
	void ReadFromPacket(BM::CPacket & kPacket);
	HRESULT GetGambleResult(eGambleCoinGrade const kGcg, CONT_GAMBLEMACHINERESULT & kContResult, bool & bBroadcast, SGAMBLEMACHINEITEM & kGambleItem = SGAMBLEMACHINEITEM());
	HRESULT GetGambleRoulette(eGambleCoinGrade const kGcg, CONT_GAMBLEMACHINERESULT & kContResult, CONT_GAMBLEITEM & kContDumy, bool & bBroadcast);
	HRESULT GetMixupRoulette(int const iMixPoint, CONT_GAMBLEMACHINERESULT & kContResult, CONT_GAMBLEITEM & kContDumy, bool & bBroadcast);
	bool GetGambleMachineCost(eGambleCoinGrade const kGcg,int & iCost);
	void GetCont(CONT_GAMBLEMACHINE & kCont){kCont = m_kCont;}
	void GetContRes(CONT_GAMBLEMACHINEGROUPRES & kCont){kCont = m_kContGroup;}
	void GetContMixup(CONT_GAMBLEMACHINEMIXUP & kCont){kCont = m_kContMixup;}
	void GetContMixupPoint(CONT_GAMBLEMACHINEMIXUPPOINT & kCont){kCont = m_kContMixupPoint;}
	int GetMixupPoint(int const iGrade) const;
	int	GetMaxMixupPoint() const;
	bool CheckEnableMixupPoint(int const iMixPoint) const;
	bool MakeEnablePoint(int const iMixPoint,int & iEnablePoint) const;
	bool IsCanCostumeMixup();

private:

	HRESULT MakeGambleResult(SGAMBLEMACHINEITEM const & kResultItem, CONT_GAMBLEMACHINERESULT & kContResult);
};

class PgGambleMachine : public TWrapper<PgGambleMachineImpl>
{
public:
	PgGambleMachine(){}
	~PgGambleMachine(){}
public:
	void WriteToPacket(BM::CPacket & kPacket);
	void ReadFromPacket(BM::CPacket & kPacket);
	HRESULT GetGambleResult(eGambleCoinGrade const kGcg, CONT_GAMBLEMACHINERESULT & kContResult, bool & bBroadcast);
	HRESULT GetGambleRoulette(eGambleCoinGrade const kGcg, CONT_GAMBLEMACHINERESULT & kContResult, CONT_GAMBLEITEM & kContDumy, bool & bBroadcast);
	HRESULT GetMixupRoulette(int const iMixPoint, CONT_GAMBLEMACHINERESULT & kContResult, CONT_GAMBLEITEM & kContDumy, bool & bBroadcast);
	bool GetGambleMachineCost(eGambleCoinGrade const kGcg,int & iCost);
	void GetCont(CONT_GAMBLEMACHINE & kCont);
	void GetContRes(CONT_GAMBLEMACHINEGROUPRES & kCont);
	void GetContMixup(CONT_GAMBLEMACHINEMIXUP & kCont);
	void GetContMixupPoint(CONT_GAMBLEMACHINEMIXUPPOINT & kCont);
	int GetMixupPoint(int const iGrade);
	bool CheckEnableMixupPoint(int const iMixPoint);
	int	GetMaxMixupPoint() const;
	bool MakeEnablePoint(int const iMixPoint,int & iEnablePoint);
	bool IsCanCostumeMixup();
};

#define g_kGambleMachine SINGLETON_STATIC(PgGambleMachine)

#endif // WEAPON_VARIANT_ITEM_PGGAMBLEMACHINE_H