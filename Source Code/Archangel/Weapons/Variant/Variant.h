#pragma once

#include "Common.h"

#ifdef _MD_
	#pragma comment (lib, "variant_MD.lib" )
#endif

#ifdef _MDd_
	#pragma comment (lib, "variant_MDd.lib" )
#endif

#ifdef _MDo_
	#pragma comment (lib, "variant_MDo.lib" )
#endif

#ifdef _MT_
	#pragma comment (lib, "variant_MT.lib" )
#endif

#ifdef _MTd_
	#pragma comment (lib, "variant_MTd.lib" )
#endif

#ifdef _MTo_
	#pragma comment (lib, "variant_MTo.lib" )
#endif
