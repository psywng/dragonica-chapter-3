#ifndef WEAPON_VARIANT_UNIT_PGGROUNDITEMBOX_H
#define WEAPON_VARIANT_UNIT_PGGROUNDITEMBOX_H

#include <vector>

#include "BM/ClassSupport.h"
#include "BM/point.h"
#include "BM/guid.h"
#include "item.h"
#include "Unit.h"

class PgGroundItemBox
	:	public CUnit
{
public:
	static DWORD const ms_GROUNDITEMBOX_DURATION_TIME = 30000;

	PgGroundItemBox(void);
	virtual ~PgGroundItemBox(void);
public:
	bool AddItem(PgBase_Item const &kItem);
	HRESULT PopItem(PgBase_Item & kItem, bool const bIsTest);
	bool AddMoney(int const iMoney);

	size_t ElementSize()const{return m_kItemArray.size();}

	bool Owner(VEC_GUID const& rkOwners);
	bool IsOwner(BM::GUID const &rkGuid);

protected:
	virtual void Init();
	virtual HRESULT Create(const void* pkInfo) { return E_FAIL; };
	virtual EUnitType UnitType()const{ return UT_GROUNDBOX; }
	virtual void WriteToPacket(BM::CPacket &rkPacket, EWRITETYPE const kWriteType=WT_DEFAULT)const;
	virtual EWRITETYPE ReadFromPacket(BM::CPacket &rkPacket);
//	virtual void WriteToPacket(BM::CPacket &rkPacket, bool const bIsSimple, bool const bIsForSave)const;
//	virtual void ReadFromPacket(BM::CPacket &rkPacket, bool &bIsSimple, bool &bIsForSave);
	virtual void Send(BM::CPacket const &Packet, E_SENDTYPE_TYPE eType = E_SENDTYPE_SELF);
	virtual void Invalidate();
	virtual bool IsCheckZoneTime(DWORD dwElapsed);
	virtual void SendAbil(const EAbilType eAT, E_SENDTYPE_TYPE eType = E_SENDTYPE_SELF);
	virtual int Tick(unsigned long ulElapsedTime, SActArg *pActArg);
	virtual void SendAbiles(WORD const* pkAbils, BYTE byNum, E_SENDTYPE_TYPE eType = E_SENDTYPE_SELF);
	virtual int CallAction(WORD wActCode, SActArg *pActArg);
	virtual void SendAbil64(const EAbilType eAT, E_SENDTYPE_TYPE eType = E_SENDTYPE_SELF);

	int AdjustImage();


	virtual void VOnDie(){CUnit::VOnDie();}
protected:
	CLASS_DECLARATION_S(int, ActionInstanceID);//아이템을 떨어뜨리게 된 원인이 되는 ActionInstanceID (클라이언트에서 DropItem의 연출을 위해 필요하다.)
	CLASS_DECLARATION_S(BYTE, OwnerType);//아래 Guid 주인의 타입
	VEC_GUID m_kOwners;//주인들
	CLASS_DECLARATION_S(DWORD, CreateDate);//생성 날짜.
	std::list< PgBase_Item > m_kItemArray;

	CLASS_DECLARATION_S(int, Money);//떨어진돈.
};

#endif // WEAPON_VARIANT_UNIT_PGGROUNDITEMBOX_H