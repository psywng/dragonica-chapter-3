#ifndef WEAPON_VARIANT_JOBSKILL_PGJOBSKILLWORKBENCH_H
#define WEAPON_VARIANT_JOBSKILL_PGJOBSKILLWORKBENCH_H

//
typedef enum eWorkBenchResult
{
	WBR_SUCCESS					= 0,	// 성공
	WBR_SYSTEM_ERROR			= 1,	// 시스템 에러
	WBR_WORONG_TYPE,					// 작업대 타입과 맞지 않는 재료 아이템
	WBR_NOT_EXIST_ITEM,					// 아이템이 존재하지 않는다.
	WBR_DURATION_ZERO,					// 내구도가 0이다.
	WBR_NOT_OWNER,						// 소유자가 아니다
	WBR_SLOT_IS_NOT_EMPTY,				// 빈 슬롯이 아니다
	WBR_IS_NOT_EMPTY,					// 슬롯에 뭔가 있어서 삭제할 수 없다
	WBR_IS_NOT_UPGRADE_ITEM,			// 채집 업그레이드 가능한 아이템이 아니다
	WBR_CANT_UPGRADE_COUNT,				// 아이테 업그레이드가 0인 아이템은 넣을 수 없다
	WBR_EMPTY_BLESS_POINT,				// 축복을 모두 소비했다
	WBR_ALREADY_EXIST_UNIQUE_ITEM,		// 하나만 설치할수 있는데 이미 존재 한다
}EWorkBenchResult;


//
namespace JobSkillWorkBenchUtil
{
	extern size_t const iStartSlotNo;
	extern int const iBlessBaseValue;
	bool IsWorkBenchItem(int const iItemNo, CONT_DEF_JOBSKILL_MACHINE const& rkContDefJSMachine);
	bool IsWorkBenchItem(int const iItemNo);
	bool GetWorkbenchInfo(int const iItemNo, CONT_DEF_JOBSKILL_MACHINE::mapped_type& rkOut);
	bool GetUpgradeInfo(int const iItemNo, CONT_DEF_JOBSKILL_ITEM_UPGRADE::mapped_type& rkOut);
	int GetWorkBenchSlotCount(int const iItemNo);
	int GetRemainUpgradeCount(int const iItemNo);
	bool IsCanUpgrade(int const iItemNo);
	int GetFailItem(int const iItemNo);
	int GetTurnTime(int const iItemNo);
	
	bool IsWorkBenchPublicAlterItem(int const iItemNo);

	//
	bool IsSystemLog(EWorkBenchEventType const eType);
	bool IsExpertnessLog(EWorkBenchEventType const eType);
};

//
typedef struct tagJobSkillWorkBenchSlotItemStatus
{
	tagJobSkillWorkBenchSlotItemStatus();
	tagJobSkillWorkBenchSlotItemStatus(tagJobSkillWorkBenchSlotItemStatus const& rhs);
	tagJobSkillWorkBenchSlotItemStatus(int const ItemNo, __int64 const TurnSec);
	tagJobSkillWorkBenchSlotItemStatus(int const ItemNo, __int64 const TurnSec, __int64 const EleapsedSec);

	bool IsEndTurn() const			{ return iTurnSec <= iEleapsedSec; }
	bool IsRemainUpgrade() const	{ return JobSkillWorkBenchUtil::IsCanUpgrade(iItemNo); }
	bool IsHaveNoUpgrade() const	{ return false == IsRemainUpgrade(); }

	__int64	CalcProgressPercent1000() const { return (iEleapsedSec*1000) / iTurnSec; }
	int		CalcRemainHour() const	{ return static_cast<int>( (static_cast<float>(iTurnSec-iEleapsedSec)/3600.0f)+0.9f ); }

	int iItemNo;
	__int64 iTurnSec;		// 1회 업글에 걸리는 시간 (1 == 1초)
	__int64 iEleapsedSec;

	DEFAULT_TBL_PACKET_FUNC();
} SJobSkillWorkBenchSlotItemStatus;
typedef std::map< size_t, SJobSkillWorkBenchSlotItemStatus > CONT_JSWB_UPGRADE_ITEM; // Slot IDX, ItemNo
typedef std::list< int > CONT_JSWB_DELETE_ITEM;

//
class PgJobSkillWorkBenchAlterInfo
{// 작업대 처리시 변동값
public:
	PgJobSkillWorkBenchAlterInfo();
	PgJobSkillWorkBenchAlterInfo(int const iWorkTimeRate, int const iExpertnessRate, int const iDecDurRate, bool const bAutoRepair, bool const bCompleteItemSendMail);
	~PgJobSkillWorkBenchAlterInfo();
	
	int GetWorkTimeRate()			const { return m_iWorkTimeRate;			}
	int GetExertnessRate()			const { return m_iExpertnessRate;		}
	int GetDecDurRate()				const { return m_iDecDurRate;			}
	bool IsAutoRepair()				const { return m_bAutoRepair;			}
	bool IsCompleteItemSendMail()	const { return m_bCompleteItemSendMail;	}

	int SetWorkTimeRate(int const iWorkTimeRate)					{ return m_iWorkTimeRate = iWorkTimeRate;					}
	int SetExertnessRate(int const iExpertnessRate)					{ return m_iExpertnessRate = iExpertnessRate;				}
	int SetDecDurRate(int const iDecDurRate)						{ return m_iDecDurRate = iDecDurRate;						}
	bool SetAutoRepair(bool const bAutoRepair)						{ return m_bAutoRepair = bAutoRepair;						}
	bool SetCompleteItemSendMail(bool const bCompleteItemSendMail)	{ return m_bCompleteItemSendMail = bCompleteItemSendMail;	}

	void Clear() { Init(); };
private:
	void Init();

private:	
	int m_iWorkTimeRate;	// 가공 속도 변동 상대값
	int m_iExpertnessRate;	// 숙련도 상승 상대값
	int m_iDecDurRate;		// 내구도 감소량 상대값
	bool m_bAutoRepair;		// 자동 수리
	bool m_bCompleteItemSendMail;	// 아이템 메일로 쏘기
};

//
class PgJobSkillWorkBench
{
public:
	typedef std::vector<int> CONT_INT;
public:
	PgJobSkillWorkBench();
	~PgJobSkillWorkBench();

	EWorkBenchStatus GetStatus() const;
	EWorkBenchStatus GetMajorStatus() const;
	bool IsHaveComplete() const;
	bool IsHaveAllComplete() const;
	void GetCompleteSlotNum(CONT_INT& kOut) const;

	EWorkBenchEventType Update(__int64 const iEleapsedTime, int& iEndItemNo, int& iResultItemNo, PgJobSkillWorkBenchAlterInfo const& kPublicAlterInfo, size_t& stWorkedAtThisTimeSlotNo);
	void UpdateCompleteItem(__int64 const iEleapsedTime, CONT_JSWB_DELETE_ITEM& rkOut);

	int Pop(size_t const iSlotNo);
	bool Push(size_t const iSlotNo, int const iItemNo);

	void WriteToPacket(BM::CPacket& rkPacket) const;
	void ReadFromPacket(BM::CPacket& rkPacket);
	void ReadFromDB(CEL::DB_DATA_ARRAY::const_iterator& iter);
	void WriteToDB(BM::GUID const& rkItemGuid, CEL::DB_QUERY& rkQuery) const;

	bool IsEmpty() const	{ return m_kContSlotItem.empty(); }
	
	bool GetCurrentUpgradeSlotItemStatus(SJobSkillWorkBenchSlotItemStatus& kOut) const;
	bool GetUpgradeSlotItemStatus(int const iSlot, SJobSkillWorkBenchSlotItemStatus& kOut) const;
	CONT_JSWB_UPGRADE_ITEM GetContAll() const	{ return m_kContSlotItem; }


	size_t GetEmptySlotNo() const;	// 비어있는 슬롯의 번호를 얻어옴
	
	size_t GetEmptySlotCnt() const;
	size_t GetNotEmptySlotCnt() const;
	size_t GetCompleteSlotCnt() const; 

	bool GetItemUpgradeInfo(size_t const iSlotNo, SJobSkillItemUpgrade& kOut);

	int const& ItemNo()const{return m_iItemNo;}
	void ItemNo(int const& iItemNo);

	bool IsAbleAutoRepair() const;					// 고장나면 스스로 자동 수리가 가능한가?
	bool IsCompleteItemSendMail() const;			// 아이템 가공이 완료되면 메일 발송이 가능한가?
//GM Command 
	void MakeCompleteOnNextTick();	// 강제로 다음틱에 아이템 작업을 완료 시킴
	void MakeTroubleOnNextTick();		// 강제로 다음틱에 작업대를 고장나게 함
	
	bool SetCurUpgradeSlotRemainTime(__int64 iRemainSec);
	bool SetCompleteSlotRemainTime(size_t stSlot, __int64 iRemainSec);
protected:
	EWorkBenchEventType Work(__int64 iEleapsedTime, int& iEndItemNo, int& iResultItemNo, PgJobSkillWorkBenchAlterInfo const& kPublicAlterInfo, size_t& stWorkedAtThisTimeSlotNo);
	void NextSlot();

private:
	__int64 m_iTroubleTime;
	int m_iItemNo;									// 작업대 ItemNo
	CLASS_DECLARATION(int, m_iDuration, Duration);	// 내구도

	CONT_JSWB_UPGRADE_ITEM m_kContSlotItem;
	CLASS_DECLARATION_NO_SET(size_t, m_iCurUpgradeSlot, CurUpgradeSlot); // 현재 작업중인 슬롯 번호
	CLASS_DECLARATION(bool, m_bHasTrouble, HasTrouble);

	CLASS_DECLARATION(__int64, m_iBlessDurationSec, BlessDurationSec);
	CLASS_DECLARATION(BM::GUID, m_kBlessCharGuid, BlessCharGuid);
	CLASS_DECLARATION(int, m_iBlessSkillNo, BlessSkillNo);
	CLASS_DECLARATION(PgJobSkillWorkBenchAlterInfo, m_kAlterInfo, AlterInfo);
};

#endif // WEAPON_VARIANT_JOBSKILL_PGJOBSKILLWORKBENCH_H