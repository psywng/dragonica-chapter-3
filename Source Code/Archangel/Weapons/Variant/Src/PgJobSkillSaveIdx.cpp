#include "stdafx.h"
#include "Item.h"
#include "PgPlayer.h"
#include "PgJobSkillSaveIdx.h"

namespace JobSkillSaveIdxUtil
{
	bool Check(PgJobSkillSaveIdx const& rkJobSkillSaveIdx, int const iSaveIdx)
	{
		if( 0 < iSaveIdx )
		{
			return rkJobSkillSaveIdx.Get(iSaveIdx);
		}
		return true; // Def 설정이 없으면 체크도 통과
	}

	//
	bool Set(PgJobSkillSaveIdx& rkJobSkillSaveIdx, int const iSaveIdx, bool const bSet)
	{
		if( !rkJobSkillSaveIdx.IsSafe(iSaveIdx) )
		{
			return false;
		}
		if( bSet )
		{
			rkJobSkillSaveIdx.Set(iSaveIdx);
		}
		else
		{
			rkJobSkillSaveIdx.Deset(iSaveIdx);
		}
		return true;
	}

	//
	bool Update(DB_ITEM_STATE_CHANGE_ARRAY::value_type const& rkItemChange, BM::CPacket& rkAddonData, PgPlayer& rkPlayer) // Recv From Contents Server(Map / Client)
	{
		switch( rkItemChange.State() )
		{
		case DISCT_JOBSKILL_SET_SAVEIDX:
			{
				SPMOD_JobSkillSaveIdx kData;
				kData.ReadFromPacket( rkAddonData );
				Set(rkPlayer.JobSkillSaveIdx(), kData.SaveIdx(), kData.Set());
			}break;
		default:
			{
				return false;
			}break;
		}
		return true;
	}
}