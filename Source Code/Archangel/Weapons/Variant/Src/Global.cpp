#include "stdafx.h"
#include "BM/Localmgr.h"
#include "Lohengrin/LogGroup.h"
#include "Global.h"
#include "DefAbilType.h"
#include "PgActionResult.h"
#include "PgTotalObjectMgr.h"
#include "PgControlDefMgr.h"
#include "tabledatamanager.h"

//#ifndef BATTLE_DEBUG
//#define BATTLE_DEBUG
//#endif
inline int GetGroggyPoint(CSkillDef const* pkSkillDef)
{
	if( pkSkillDef )
	{
		int const iCount = std::max(pkSkillDef->GetAbil(AT_COMBO_HIT_COUNT),1);
		return iCount * pkSkillDef->GetAbil(AT_ADD_GROGGYPOINT);
	}
	return 0;
}

inline void GetCasterLevelAndType(CUnit const* pkCaster, int & iCasterLevel, EUnitType & eCasterType)
{
	if( !pkCaster )
	{
		return;
	}

	iCasterLevel = pkCaster->GetBattleLevel();
	eCasterType = pkCaster->UnitType();

	//캐스터가 엔티티 일 경우
	if(UT_ENTITY ==  eCasterType)
	{
		//캐스터를 호출 한 것이 플래이어일 경우
		if(UT_PLAYER == pkCaster->GetAbil(AT_CALLER_TYPE))
		{
			//캐스터 타입과 레벨을 Player의 것으로 세팅(가디언의 경우 예외)
			eCasterType = UT_PLAYER;
			iCasterLevel = (ENTITY_GUARDIAN!=pkCaster->GetAbil(AT_ENTITY_TYPE)) ? pkCaster->GetAbil(AT_CALLER_LEVEL) : iCasterLevel;
		}
	}
}

bool CheckBalanceWStringLen( std::wstring const &wstr, size_t const iLen )
{
	int iRemainLen = static_cast<int>(iLen) * 2;
	std::wstring::const_iterator itr = wstr.begin();
	for ( ; itr != wstr.end() ; ++itr )
	{
		if ( 0xFF >= *itr )
		{
			iRemainLen -= sizeof(char);
		}
		else
		{
			iRemainLen -= sizeof(wchar_t);
		}

		if ( 0 > iRemainLen )
		{
			return false;
		}
	}
	return true;
}

WORD GetBasicAbil(WORD const wAbil)
{
	if (wAbil < AT_CALCUATEABIL_MIN || wAbil > AT_CALCUATEABIL_MAX)
	{
		return wAbil;
	}

	return (wAbil / 10 * 10 + 1);
}

bool IsCalculatedAbil(WORD const wAbil)
{
	if (wAbil < AT_CALCUATEABIL_MIN || wAbil > AT_CALCUATEABIL_MAX)
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}
	
	return ((wAbil % 10) == 3) ? true : false;
}

bool IsMonsterCardAbil(WORD const wAbil)
{
	if (wAbil < AT_ATTACK_EFFECTNUM01 || (wAbil > AT_ATTACK_EFFECTNUM10_TARGET_TYPE_IS_ME && wAbil < AT_MONSTER_CARD_ABIL_MIN) || wAbil > AT_MONSTER_CARD_ABIL_MAX)
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}
	
	return true;
}

bool IsHitAbilFromEquipItem(WORD const wAbil)
{// 장비에서 세팅해주는 '타격시 걸리는 이펙트'에 사용되는 어빌인가?
	if (wAbil < AT_ATTACK_EFFECTNUM01_FROM_EQUIP 
		|| (wAbil > AT_ATTACK_EFFECTNUM50_TARGET_TYPE_IS_ME_FROM_EQUIP)
		)
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}
	return true;
}

bool IsItemActionAbil(WORD const wAbil)
{
	if((wAbil < AT_N_ATTACK_EFFECT_NO_MIN) || (AT_BLOCKED_EFFECT_TARGET_MAX < wAbil))
	{
		return false;
	}
	return true;
}

bool IsRateAbil(WORD const wAbil)
{
	if (wAbil < AT_CALCUATEABIL_MIN || wAbil > AT_CALCUATEABIL_MAX)
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}
	return ((wAbil % 10) == 2) ? true : false;
}

WORD GetRateAbil(WORD const wAbil)
{
	return (wAbil / 10 * 10 + 2);
}

WORD GetCalculateAbil(WORD const wAbil)
{
	if (wAbil < AT_CALCUATEABIL_MIN || wAbil > AT_CALCUATEABIL_MAX)
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return 0"));
		return 0;
	}

	return (wAbil / 10 * 10 + 3);
}


bool IsCalculateAbilRange(WORD const wAbil)
{
	bool bRet = false;
	if( AT_CALCUATEABIL_MIN < wAbil && AT_CALCUATEABIL_MAX > wAbil)				{ bRet = true; }
	if(	AT_I_PHY_DEFENCE_ADD_RATE <= wAbil && AT_I_MAGIC_DEFENCE_ADD_RATE >= wAbil) { bRet = true; }

	if( !bRet )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	}

	 return bRet;
}

bool IsCountAbil( WORD const wAbil )
{
	switch ( wAbil )
	{
	case AT_CANNOT_ATTACK:
	case AT_CANNOT_DAMAGE:
//	case AT_UNIT_HIDDEN:
		{
			return true;
		}break;
	default:
		{

		}break;
	}
	return false;
}

int GetCountAbilCheckFlag( int const iFlag )
{
	switch ( iFlag )
	{
	case AT_CF_EFFECT:{return AT_CF_EFFECT_CHK;}break;
	case AT_CF_QUEST:{return AT_CF_QUEST_CHK;}break;
	case AT_CF_EVENTSCRIPT:{return AT_CF_EVENTSCRIPT_CHK;}break;
	case AT_CF_HYPERMOVE:{return AT_CF_HYPERMOVE_CHK;}break;
	case AT_CF_FAKEREMOVE:{return AT_CF_FAKEREMOVE_CHK;}break;
	case AT_CF_ELITEPATTEN:{return AT_CF_ELITEPATTEN_CHK;}break;
	case AT_CF_GM:{return AT_CF_GM_CHK;}break;
	case AT_CF_BS:{return AT_CF_BS_CHK;}break;
	case AT_CF_ALL:{return AT_CF_ALL;}break;
	}

	return AT_CF_NONE;
}

int GetCountAbil( CAbilObject const *pkAbilObj, WORD const wType, int const iFlag )
{
	if ( pkAbilObj && (true == IsCountAbil( wType )) )
	{
		int const iChkType = GetCountAbilCheckFlag( iFlag );
		if ( AT_CF_NONE != iChkType )
		{
			int iValue = pkAbilObj->GetAbil( wType );
			iValue &= iChkType;
			return iValue;
		}
	}
	return 0;
}

bool AddCountAbil( CAbilObject *pkAbilObj, WORD const wType, int const iFlag, bool const bAdd )
{
	if ( pkAbilObj && (true == IsCountAbil( wType )) )
	{
		int const iChkType = GetCountAbilCheckFlag( iFlag );
		if ( AT_CF_NONE == iChkType )
		{
			VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("Error Flag<") << iFlag << _T("> Type<") << wType << _T(">") );
			return false;
		}

		int iValue = pkAbilObj->GetAbil( wType );
		int const iTypeValue = ( iChkType & iValue );

		if ( true == bAdd )
		{
			if ( iTypeValue && (iChkType == iTypeValue) )
			{
				CAUTION_LOG( BM::LOG_LV5, __FL__ << _T("ADD Over Type<") << wType << _T("> Flag <") << iFlag << _T(">") );
			}
			else
			{
				iValue += iFlag;
				pkAbilObj->SetAbil( wType, iValue );
			}
		}
		else
		{
			if ( iTypeValue )
			{
				iValue -= iFlag;
				pkAbilObj->SetAbil( wType, iValue );
			}
			else
			{
				CAUTION_LOG( BM::LOG_LV5, __FL__ << _T("MINUS Over Type<") << wType << _T("> Flag <") << iFlag << _T(">") );
			}
		}
		return true;
	}

	return false;
}

bool RemoveCountAbil( CAbilObject *pkAbilObj, WORD const wType, int const iFlag )
{
	if ( pkAbilObj )
	{
		if ( true == IsCountAbil( wType ) )
		{
			int const iChkType = GetCountAbilCheckFlag( iFlag );
			if ( AT_CF_NONE != iChkType )
			{
				int iValue = pkAbilObj->GetAbil( wType );
				iValue &= (~iChkType);
				pkAbilObj->SetAbil( wType, iValue );
				return true;
			}
		}
	}
	return false;
}

void POINT3_2_POINT3BY(POINT3 const &rkPos, POINT3BY& rkOut)
{
	int const iNormalVectorMultiply = 120;
	// rkPos 값은 normalize 된 상태로 입력되어야 한다.
	assert(rkPos.x<=1 && rkPos.y<=1 && rkPos.z <=1);
	rkOut.x = (char)(rkPos.x * iNormalVectorMultiply);
	rkOut.y = (char)(rkPos.y * iNormalVectorMultiply);
	rkOut.z = (char)(rkPos.z * iNormalVectorMultiply);
	//INFO_LOG(BM::LOG_LV9, _T("[%s] Pos[%6.2f, %6.2f, %6.2f]->[%d, %d, %d]"), __FUNCTIONW__, rkPos.x, rkPos.y,
	//	rkPos.z, (int)rkOut.x, (int)rkOut.y, (int)rkOut.z);
}


//확률 적용이 없는 부분 수퍼아머 타입 검사(참: 액션한다, 거짓: 액션안한다)
inline bool IsNotRatePartDamageAction(CUnit const * const pkUnit)
{
	if(!pkUnit)
	{
		return false;
	}

	int const iDamageActionType = pkUnit->GetAbil(AT_DAMAGEACTION_TYPE);
	if(iDamageActionType&E_DMGACT_TYPE_PART_SUPERARMOUR)
	{
		if(pkUnit->GetAbil(AT_POSTURE_STATE) || EAI_ACTION_BLOWUP==pkUnit->GetAI()->eCurrentAction)
		{
			return false;
		}

		if(EAI_ACTION_NONE==pkUnit->GetAI()->eCurrentAction && pkUnit->IsState(US_IDLE) && pkUnit->GetDelay())
		{//클라용, 인공지능이 없고 IDEL상태에서 Delay가 있으면 Fire 중임
			return false;
		}

		if(pkUnit->IsState(US_SKILL_CAST) || pkUnit->IsState(US_SKILL_FIRE) || pkUnit->IsState(US_FIRE_WAITING))
		{
			return false;
		}
	}
	return true;
}
inline bool IsDamageAction(CUnit const * const pkUnit, CSkillDef const * const pkSkillDef)
{
	if(!pkUnit)
	{
		return false;
	}

	if(!pkSkillDef)
	{
		return false;
	}

	if(pkSkillDef->GetAbil(AT_DAMAGEACTION_FORCE))
	{
		// 스킬에서 강제로 Damage Action 이 들어갈수 있게 한 경우
		return true;
	}

	int const iDamageActionType = pkUnit->GetAbil(AT_DAMAGEACTION_TYPE);
	if((iDamageActionType&E_DMGACT_CHECK) || (pkSkillDef->GetAbil(AT_DAMAGEACTION_TYPE)!=0))
	{
		return false;
	}

	if(iDamageActionType&E_DMGACT_TYPE_PART_SUPERARMOUR)
	{
		if( !IsNotRatePartDamageAction(pkUnit) )
		{
			return false;
		}

		int const iRate = pkUnit->GetAbil(AT_DAMAGEACTION_RATE);
		if( iRate )
		{
			return false;
		}
	}

	return true;
}

void AddDamageEffect(CUnit* pkCaster, CUnit* pkTarget, CSkillDef const* pkSkillDef, PgActionResult* pkResult)
{
	if(!pkCaster || !pkTarget || !pkSkillDef || !pkResult)
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("AddDamageEffect Failed!"));
		return;
	}

	int const iRate = pkTarget->GetAbil(AT_DAMAGEACTION_RATE);//임시
	DWORD const dwRand = pkCaster->GetRandom();
	bool bDamageAction = false;

	if (IsDamageAction(pkTarget,pkSkillDef))
	{
		int const iDmgEffect = pkSkillDef->GetAbil(AT_DAM_EFFECT_S + pkTarget->GetAbil(AT_UNIT_SIZE) -1);
		if (iDmgEffect > 0)
		{
			if(pkTarget->CheckSkillFilter(iDmgEffect, NULL, ESFilter_Ignore_Action_Effect))
			{
				pkResult->AddEffect(iDmgEffect);
			}
		}
		bDamageAction = true;
	}
	else	//데미지액션 무시 타입이 있으면
	{
		if(!pkSkillDef->IsSkillAtt(SAT_CLIENT_CTRL_PROJECTILE) || 0 == pkSkillDef->GetAbil(AT_DAMAGEACTION_TYPE) )	//발사체일 경우 데미지 액션을 하지 않음
		{
			if(iRate && IsNotRatePartDamageAction(pkTarget))
			{
				if(iRate > int(dwRand % ABILITY_RATE_VALUE))	//데미지 액션을 한다
				{
					int const iDmgEffect = pkSkillDef->GetAbil(AT_DAM_EFFECT_S + pkTarget->GetAbil(AT_UNIT_SIZE) -1);
					if (0 < iDmgEffect)
					{
						if(pkTarget->CheckSkillFilter(iDmgEffect, NULL, ESFilter_Ignore_Action_Effect))
						{
							pkResult->AddEffect(iDmgEffect);
						}
					}
					SetDamageDelay(pkSkillDef, pkCaster, pkTarget);	//여기에서 EAI_EVENT_DAMAGED 해주니까
					bDamageAction = true;
				}
				else
				{
					int iRate = pkTarget->GetAbil(AT_AI_TARGETTING_RATE);
					if(0 == iRate)
					{
						iRate = 3000;//기본적으로 30%
					}

					if (iRate > BM::Rand_Index(iRate) && pkTarget->GetReserveTarget().IsNull())
					{
						pkTarget->SetReserveTarget(pkCaster->GetID());
					}	
					pkResult->SetEndure(true);
				}
			}
		}
	}

	pkTarget->SetDamageAction(bDamageAction);
	pkResult->SetDamageAction(bDamageAction);
}

bool CS_GetSkillResultDefault(int const iSkillNo, CUnit* pkCaster, UNIT_PTR_ARRAY& rkTargetArray, PgActionResultVector* pkResultVector)
{
	if(rkTargetArray.empty())
	{
		return true;
	}

#ifdef BATTLE_DEBUG
	INFO_LOG(BM::LOG_LV9, __FL__<<L"SkillNo["<<iSkillNo<<L"]");
#endif
	GET_DEF(CSkillDefMgr, kSkillDefMgr);
	CSkillDef const* pkSkillDef = kSkillDefMgr.GetDef(iSkillNo);
	if (NULL == pkSkillDef)
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L"Cannot Get SkillDef, SkillNo["<<iSkillNo<<L"]");
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}
	bool bPhysicSkill = pkSkillDef->IsSkillAtt(SAT_PHYSICS);
	int iMin = 0, iMax = 0, iSkillPRate = 0, iSkillPower = 0;
	int iInvenAttackAddRate = 0;
	int iAddPwoer = 0;
	if (bPhysicSkill)
	{
#ifdef BATTLE_DEBUG
		INFO_LOG(BM::LOG_LV9, __FL__<<L"\t\t** PHYSICAL ATTACK");
#endif
		iMin = pkCaster->GetAbil(AT_C_PHY_ATTACK_MIN);
		iMax = pkCaster->GetAbil(AT_C_PHY_ATTACK_MAX);
		iSkillPower = pkSkillDef->GetAbil(AT_PHY_ATTACK);
		iSkillPRate = pkSkillDef->GetAbil(AT_PHY_DMG_PER);
		//iSkillPRate += pkSkillDef->GetAbil(AT_PHY_DMG_PER2);
		iInvenAttackAddRate = pkSkillDef->GetAbil(AT_I_PHY_ATTACK_ADD_RATE);

		//캐스터가 PgPlayer라면
		if(UT_PLAYER == pkCaster->UnitType())
		{
			if(PgPlayer* pkPlayer = dynamic_cast<PgPlayer*>(pkCaster))
			{
				int iSkillindex = 0;
				while(CHILD_SKILL_MAX > iSkillindex)
				{
					//영향 받는 베이스 스킬
					int const iChildBaseSkillNo = pkSkillDef->GetAbil(AT_CHILD_SKILL_NUM_01 + iSkillindex);
					if(0 < iChildBaseSkillNo)
					{
						if(PgMySkill* pkPlayerSkill = pkPlayer->GetMySkill())
						{
							//해당 스킬의 실제 레벨에 해당되는 스킬을 얻어 온다.
							int const iLearnedChildSkillNo = pkPlayerSkill->GetLearnedSkill(iChildBaseSkillNo);
							if(0 < iLearnedChildSkillNo)
							{
								if(CSkillDef const* pkLearnedSkillDef = kSkillDefMgr.GetDef(iLearnedChildSkillNo))
								{
									iSkillPower += pkLearnedSkillDef->GetAbil(AT_PHY_ATTACK);
									iSkillPRate += pkLearnedSkillDef->GetAbil(AT_PHY_DMG_PER);
									//iSkillPRate += pkLearnedSkillDef->GetAbil(AT_PHY_DMG_PER2);
									iInvenAttackAddRate += pkLearnedSkillDef->GetAbil(AT_I_PHY_ATTACK_ADD_RATE);
								}
							}
						}
					}
					else
					{
						break;
					}
					++iSkillindex;
				}			
			}
		}
		else if(UT_ENTITY == pkCaster->UnitType())
		{
			int iSkillindex = 0;
			while(CHILD_SKILL_MAX > iSkillindex)
			{
				//Entity는 Entity에 미리 저장되어 있다.
				//영향 받는 베이스 스킬 실제 배운 스킬 번호가 저장되어 있다.
				int const iLearnedChildSkillNo = pkCaster->GetAbil(AT_CHILD_SKILL_NUM_01 + iSkillindex);
				if(0 < iLearnedChildSkillNo)
				{
					if(CSkillDef const* pkLearnedSkillDef = kSkillDefMgr.GetDef(iLearnedChildSkillNo))
					{
						iSkillPower += pkLearnedSkillDef->GetAbil(AT_PHY_ATTACK);
						iSkillPRate += pkLearnedSkillDef->GetAbil(AT_PHY_DMG_PER);
						//iSkillPRate += pkLearnedSkillDef->GetAbil(AT_PHY_DMG_PER2);
						iInvenAttackAddRate += pkLearnedSkillDef->GetAbil(AT_I_PHY_ATTACK_ADD_RATE);
					}
				}
				else
				{
					break;
				}
				++iSkillindex;
			}
		}
	}
	else
	{
#ifdef BATTLE_DEBUG
		INFO_LOG(BM::LOG_LV9, __FL__<<L"\t\t__ MAGIC ATTACK");
#endif
		iMin = pkCaster->GetAbil(AT_C_MAGIC_ATTACK_MIN);
		iMax = pkCaster->GetAbil(AT_C_MAGIC_ATTACK_MAX);
		iSkillPower = pkSkillDef->GetAbil(AT_MAGIC_ATTACK);
		iSkillPRate = pkSkillDef->GetAbil(AT_MAGIC_DMG_PER);
		//iSkillPRate += pkSkillDef->GetAbil(AT_MAGIC_DMG_PER2);
		iInvenAttackAddRate = pkSkillDef->GetAbil(AT_I_MAGIC_ATTACK_ADD_RATE);

		//캐스터가 PgPlayer라면
		if(UT_PLAYER == pkCaster->UnitType())
		{
			if(PgPlayer* pkPlayer = dynamic_cast<PgPlayer*>(pkCaster))
			{
				int iSkillindex = 0;
				while(CHILD_SKILL_MAX > iSkillindex)
				{
					int const iChildBaseSkillNo = pkSkillDef->GetAbil(AT_CHILD_SKILL_NUM_01 + iSkillindex);
					if(0 < iChildBaseSkillNo)
					{
						if(PgMySkill* pkPlayerSkill = pkPlayer->GetMySkill())
						{
							//해당 스킬의 실제 레벨에 해당되는 스킬을 얻어 온다.
							int const iLearnedChildSkillNo = pkPlayerSkill->GetLearnedSkill(iChildBaseSkillNo);
							if(0 < iLearnedChildSkillNo)
							{
								if(CSkillDef const* pkLearnedSkillDef = kSkillDefMgr.GetDef(iLearnedChildSkillNo))
								{
									iSkillPower += pkLearnedSkillDef->GetAbil(AT_MAGIC_ATTACK);
									iSkillPRate += pkLearnedSkillDef->GetAbil(AT_MAGIC_DMG_PER);
									//iSkillPRate += pkLearnedSkillDef->GetAbil(AT_MAGIC_DMG_PER2);
									iInvenAttackAddRate += pkLearnedSkillDef->GetAbil(AT_I_MAGIC_ATTACK_ADD_RATE);
								}
							}
						}
					}
					else
					{
						break;
					}
					++iSkillindex;
				}			
			}
		}

		

	}

	if (0 == iSkillPRate)
	{
		iSkillPRate = ABILITY_RATE_VALUE;
	}

	if (bPhysicSkill)
	{
		// + 인 경우는 스킬의 최종 데미지를 감소 시키는 경우
		if(0 < pkCaster->GetAbil(AT_PHY_DMG_PER))
		{
			iSkillPRate = static_cast<int>(iSkillPRate * static_cast<float>(pkCaster->GetAbil(AT_PHY_DMG_PER)) / ABILITY_RATE_VALUE_FLOAT);
		}
		// - 인 경우는 스킬의 최종 데미지를 1로 만들어 버리는 경우
		else if(0 > pkCaster->GetAbil(AT_PHY_DMG_PER))
		{
			iSkillPRate += pkCaster->GetAbil(AT_PHY_DMG_PER);
		}
		// pkCaster AT_PHY_DMG_PER가 저렇게 동작해야 하는지 이해할수 없지만 사이드 이펙트 최소화를 위해 다시 계산해주는 부분을 만듬
		iSkillPRate += ( iSkillPRate * pkCaster->GetAbil(AT_PHY_DMG_PER2) ) / ABILITY_RATE_VALUE;
	}
	else
	{
		// + 인 경우는 스킬의 최종 데미지를 감소 시키는 경우
		if(0 < pkCaster->GetAbil(AT_MAGIC_DMG_PER))
		{
			iSkillPRate = static_cast<int>(iSkillPRate * static_cast<float>(pkCaster->GetAbil(AT_MAGIC_DMG_PER)) / ABILITY_RATE_VALUE_FLOAT);
		}
		// - 인 경우는 스킬의 최종 데미지를 1로 만들어 버리는 경우
		else if(0 > pkCaster->GetAbil(AT_MAGIC_DMG_PER))
		{
			iSkillPRate += pkCaster->GetAbil(AT_MAGIC_DMG_PER);
		}
		// pkCaster AT_MAGIC_DMG_PER가 저렇게 동작해야 하는지 이해할수 없지만 사이드 이펙트 최소화를 위해 다시 계산해주는 부분을 만듬
		iSkillPRate += ( iSkillPRate * pkCaster->GetAbil(AT_MAGIC_DMG_PER2) ) / ABILITY_RATE_VALUE;
	}

	DWORD const iInvenAttackRandomValue = pkCaster->GetRandom();
	if( iInvenAttackAddRate )
	{
		int iInvenAttackMin = 0;
		int iInvenAttackMax = 0;
		if (bPhysicSkill)
		{
			iInvenAttackMin = pkCaster->GetAbil(AT_EQUIP_WEAPON_PHY_ATTACK_MIN);
			iInvenAttackMax = pkCaster->GetAbil(AT_EQUIP_WEAPON_PHY_ATTACK_MAX);			
		}
		else
		{
			iInvenAttackMin = pkCaster->GetAbil(AT_EQUIP_WEAPON_MAGIC_ATTACK_MIN);
			iInvenAttackMax = pkCaster->GetAbil(AT_EQUIP_WEAPON_MAGIC_ATTACK_MAX);			
		}

		int const iInvenAttack = abs(iInvenAttackMax - iInvenAttackMin);
		
		int iResult = 0;
		if(0 != iInvenAttack)
		{
			iResult = iInvenAttackRandomValue % iInvenAttack;
		}

		iAddPwoer = iResult + iInvenAttackMin;
		iAddPwoer = int(iAddPwoer * (float)iInvenAttackAddRate / ABILITY_RATE_VALUE_FLOAT);
	}
#ifdef BATTLE_DEBUG
	INFO_LOG(BM::LOG_LV9, __FL__<<L"bPhysic["<<bPhysicSkill<<L"], Min["<<iMin<<L"], Max["<<iMax<<L"], SkillPower["<<iSkillPower<<L"], SkillRate["<<iSkillPRate<<L"]");
#endif

	int iCriticalRate = pkCaster->GetAbil(AT_C_CRITICAL_RATE);
	int const iOneHit = pkCaster->GetAbil(AT_CRITICAL_ONEHIT);
	if (0 < iOneHit)
	{
		iCriticalRate = ABILITY_RATE_VALUE;
		pkCaster->SetAbil(AT_CRITICAL_ONEHIT, iOneHit-1);
	}
	int iCriticalPowerRate = pkCaster->GetAbil(AT_C_CRITICAL_POWER);
	if (0 == iCriticalPowerRate)
	{
		iCriticalPowerRate = ABILITY_RATE_VALUE * 2;	// 200%
	}
	int const iHitRate = pkCaster->GetAbil(AT_C_HITRATE);
	int const iDecDodgeAbs = pkCaster->GetAbil(AT_DEC_TARGETDODGE_ABS);
	int const iDecDodgeRate = pkCaster->GetAbil(AT_DEC_TARGETDODGE_RATE);

	int iDamage = 0;
	UNIT_PTR_ARRAY::const_iterator itor = rkTargetArray.begin();
	while (rkTargetArray.end()  != itor)
	{
		//참고해야하는 유닛 이면
		if(true == (*itor).bReference)
		{
			//나중에 처리
			++itor;
			continue;
		}
		CUnit* pkTarget = itor->pkUnit;

		PgActionResult* pkResult = pkResultVector->GetResult(pkTarget->GetID(), true);
		if( NULL == pkResult )
		{
			++itor;
			VERIFY_INFO_LOG(false, BM::LOG_LV5, __FL__<<L"ActionResult is NULL Unit Guid["<<itor->pkUnit->GetID()<<L"]");
			continue;
		}

		// Target이 실제로 Damage를 입든 안입든, Target이 다시 잡히지 않도록 해야 한다.
		pkCaster->OnTargetDamaged(pkTarget->GetID());
		pkResult->SetValue(0);
		if(	(NULL == pkTarget) 
		||	(pkTarget->IsDead()))
		{
			++itor;
			pkResult->SetInvalid(true);
			INFO_LOG(BM::LOG_LV9, __FL__<<L"Invalid Target");
			continue;
		}

		// 절대 Block Rate 계산
		int iRandValue = pkCaster->GetRandom() % ABILITY_RATE_VALUE;
		if (iRandValue < pkTarget->GetAbil(AT_100PERECNT_BLOCK_RATE))
		{
			pkResult->SetBlocked(true);
			++itor;
			continue;
		}
		
		// Block Rate 계산
		iRandValue = pkCaster->GetRandom() % ABILITY_RATE_VALUE;
		if (iRandValue < pkTarget->GetAbil(AT_C_BLOCK_RATE))
		{
			pkResult->SetBlocked(true);
			++itor;
			continue;
		}
			
		iRandValue = pkCaster->GetRandom() % ABILITY_RATE_VALUE;
		// 2007-12-05 변경 후의 공식
		// Caster:HitRate - Target:Dodge => 타격성공 여부 결정
#ifdef BATTLE_DEBUG
		INFO_LOG(BM::LOG_LV9, __FL__<<L"\tHitRate["<<iHitRate<<L"] - Dodge["<<pkTarget->GetAbil(AT_C_DODGE_RATE)<<L"], Rand["<<iRandValue<<L"]");
#endif
		int iDecHitRate = CalcDecHitRate(pkCaster, pkTarget, iHitRate, iDecDodgeAbs, iDecDodgeRate);

#ifdef BATTLE_DEBUG
		INFO_LOG(BM::LOG_LV7, _T("HitRate=") << iDecHitRate);
#endif
		if (iDecHitRate < iRandValue)
		{
			// 타격 실패 (Missed)
			pkResult->SetMissed(true);
#ifdef BATTLE_DEBUG
			INFO_LOG(BM::LOG_LV9, __FL__<<L"--Missed");
#endif
			++itor;
			continue;
		}
		// 2007-12-05 변경되기 전의 공식
		/*
		if (iRandValue < pkTarget->GetAbil(AT_C_DODGE_RATE))
		{
			// 아싸 회피 성공
			pkResult->SetDodged(true);
#ifdef BATTLE_DEBUG
			INFO_LOG(BM::LOG_LV9, __FL__<<L"--Dodged");
#endif
			++itor;
			continue;
		}
		if (iRandValue > iHitRate)
		{
			// 명중율 실패
			pkResult->SetMissed(true);
#ifdef BATTLE_DEBUG
			INFO_LOG(BM::LOG_LV9, __FL__<<L"--Missed");
#endif
			++itor;
			continue;
		}
		*/
		
		//기본적인 공격자의 공격력 + 스킬 공격력
		iRandValue = pkCaster->GetRandom() % ABILITY_RATE_VALUE;

		// 최종공격력(물리) = 기본공격력 + 아이템공격력 + 물리후방공격력
		// 최종공격력(마법) = (기본공격력 + 아이템공격력) * 1.5		
		int const iAddDmg = (bPhysicSkill ? (pkCaster->GetAbil(AT_C_ABS_ADDED_DMG_PHY)) : (pkCaster->GetAbil(AT_C_ABS_ADDED_DMG_MAGIC)));

		iDamage = int((iMin + iRandValue % __max(1, iMax-iMin)) * ((bPhysicSkill) ? 1.0f : 1.5f)) + iAddDmg;
		iDamage += iSkillPower;	// Skill 증가값은 공격력에 더하고, Skill증가률은 Damage 나온 값에 곱해준다.
		iDamage += iAddPwoer;
		{// 스킬 데미지 조정
			iDamage =  PgSkillHelpFunc::CalcAdjustSkillValue(EASCT_DMG, iSkillNo, pkCaster, iDamage);
			{
				int const iOriginDmamge = iDamage;
				// 누적 데미지 값을 계산하고
				iDamage = PgSkillHelpFunc::CalcAdjustSkillValue(EASCT_INC_PHASE_DMG, iSkillNo, pkCaster, iDamage);
				// 스킬 데미지를 계산한 시점의 값으로,최저 최대 값 얻어와
				int const iMinDmg = PgSkillHelpFunc::CalcAdjustSkillValue(EASCT_RESULT_MIN_DMG_BY_PHASE, iSkillNo, pkCaster, iOriginDmamge);
				int const iMaxDmg = PgSkillHelpFunc::CalcAdjustSkillValue(EASCT_RESULT_MAX_DMG_BY_PHASE, iSkillNo, pkCaster, iOriginDmamge);
				//보정해준다.
				if(0 != iMinDmg
					&& iOriginDmamge > iMinDmg	// min값 적용하는데 원래 데미지보다 더 늘어나면 말이 안됨 
					&& iMinDmg > iDamage
					)
				{
					iDamage = iMinDmg;
				}
				if(0 != iMaxDmg
					&& iOriginDmamge < iMaxDmg	// max값 적용하는데 원래 데미지가 줄어들면 말이 안됨
					&& iMaxDmg < iDamage
					)
				{ 
					iDamage = iMaxDmg; 
				}
			}

		}
		
#ifdef BATTLE_DEBUG
	INFO_LOG(BM::LOG_LV9, __FL__<<L"AttackPower["<<iDamage<<L"] = AttackPower + 2ndPower["<<iAddDmg<<L"]");
#endif
#ifdef BATTLE_DEBUG
		INFO_LOG(BM::LOG_LV9, __FL__<<L"Final AttackPower : "<<iDamage<<L"");
#endif
		int iExtraDamageRate = pkCaster->GetAbil(AT_1ST_ATTACK_ADDED_RATE);
		if (0 != iExtraDamageRate)
		{
#ifdef BATTLE_DEBUG
			INFO_LOG(BM::LOG_LV9, __FL__<<L"ExtraDamageRate["<<iExtraDamageRate<<L"]");
#endif
			iDamage = iDamage + iDamage * iExtraDamageRate / ABILITY_RATE_VALUE;
			// 1방에만 적용하는 기능을 여러대 적용하는 어빌이
			int const iApplyNum = pkCaster->GetAbil(AT_1ST_ATTACK_ADDED_RATE_APPLY_NUM);
			if(1 < iApplyNum)
			{// 충분하게 존재하면 횟수만 감소 시키고
				pkCaster->SetAbil(AT_1ST_ATTACK_ADDED_RATE_APPLY_NUM, iApplyNum-1);
			}
			else
			{// 이번이 마지막 적용이면 0으로 횟수와, 증가값을 초기화 시킨다 
				pkCaster->SetAbil(AT_1ST_ATTACK_ADDED_RATE_APPLY_NUM, 0);
				pkCaster->SetAbil(AT_1ST_ATTACK_ADDED_RATE, 0);
			}
		}
		iDamage = CS_CheckDamage(pkCaster, pkTarget, iDamage, bPhysicSkill, pkResult, iSkillPRate, pkSkillDef->IsSkillAtt(SAT_CLIENT_CTRL_PROJECTILE), pkSkillDef);
#ifdef BATTLE_DEBUG
		INFO_LOG(BM::LOG_LV9, __FL__<<L"iDamage["<<iDamage<<L"]");
#endif
		iRandValue = pkCaster->GetRandom() % ABILITY_RATE_VALUE;
		if (iRandValue < iCriticalRate)
		{
			iDamage = iDamage * iCriticalPowerRate / ABILITY_RATE_VALUE;
#ifdef BATTLE_DEBUG
			INFO_LOG(BM::LOG_LV9, __FL__<<L"--Ciritical");
#endif
			pkResult->SetCritical(true);
			iDamage = CS_CheckItemDamage( pkCaster, pkTarget, true, iDamage );
		}
		else
		{
			iDamage = CS_CheckItemDamage( pkCaster, pkTarget, false, iDamage );
		}

		// 반사 데미지 계산 및 ActionResult에 설정
		PgSkillHelpFunc::CalcReflectDamage(pkCaster, pkTarget, iDamage, pkResult, pkSkillDef, bPhysicSkill, false, false);

		AddDamageEffect(pkCaster, pkTarget, pkSkillDef, pkResult);
#ifdef BATTLE_DEBUG
		INFO_LOG(BM::LOG_LV9, __FL__<<L"Final Damage["<<iDamage<<L"]");
#endif
		if(0 == pkTarget->GetAbil(AT_CANNOT_DAMAGE)) //데미지를 입을 수 있는 상태일때만 세팅
		{
			pkResult->SetValue(iDamage);
		}

		++itor;
	}

	CheckActionResultVec(rkTargetArray, *pkResultVector);

	return true;
}


// bPhysicalAttack : 물리공격이냐?
// caster : 공격하는 unit
// target : 공격 받는 unit
// iDmgRate : 최종스킬의 Damage에서 몇%를 Damage로 계산할 것인가?
//		룰렛같은 스킬에서 최종공격력의 %로 Damage를 줘야 한다.
// iDmg : 최종스킬의 Damage에서 추가적인 Damage를 더해줘라-- aresult : 결과값
bool CS_GetDmgResult(int const iSkillNo, CUnit* pkCaster, CUnit* pkTarget, int const iDmgRate, int const iDmg, PgActionResult* pkResult)
{
	GET_DEF(CSkillDefMgr, kSkillDefMgr);
	CSkillDef const* pkSkillDef = kSkillDefMgr.GetDef(iSkillNo);
	if (NULL == pkSkillDef)
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L"Cannot Get SkillDef, SkillNo["<<iSkillNo<<L"]");
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}
	bool bPhysicSkill = pkSkillDef->IsSkillAtt(SAT_PHYSICS);
	int iMin = 0, iMax = 0, iSkillPRate = 0, iSkillPower = 0;
	int iInvenAttackAddRate = 0;
	int iAddPwoer = 0;
	if (bPhysicSkill)
	{
		iMin = pkCaster->GetAbil(AT_C_PHY_ATTACK_MIN);
		iMax = pkCaster->GetAbil(AT_C_PHY_ATTACK_MAX);
		iSkillPower = pkSkillDef->GetAbil(AT_PHY_ATTACK);
		iSkillPRate = pkSkillDef->GetAbil(AT_PHY_DMG_PER);
		//iSkillPRate += pkSkillDef->GetAbil(AT_PHY_DMG_PER2);
		iInvenAttackAddRate = pkSkillDef->GetAbil(AT_I_PHY_ATTACK_ADD_RATE);

		//캐스터가 PgPlayer라면
		if(UT_PLAYER == pkCaster->UnitType())
		{
			if(PgPlayer* pkPlayer = dynamic_cast<PgPlayer*>(pkCaster))
			{
				int iSkillindex = 0;
				while(CHILD_SKILL_MAX > iSkillindex)
				{
					//영향 받는 베이스 스킬
					int const iChildBaseSkillNo = pkSkillDef->GetAbil(AT_CHILD_SKILL_NUM_01 + iSkillindex);
					if(0 < iChildBaseSkillNo)
					{
						if(PgMySkill* pkPlayerSkill = pkPlayer->GetMySkill())
						{
							//해당 스킬의 실제 레벨에 해당되는 스킬을 얻어 온다.
							int const iLearnedChildSkillNo = pkPlayerSkill->GetLearnedSkill(iChildBaseSkillNo);
							if(0 < iLearnedChildSkillNo)
							{
								if(CSkillDef const* pkLearnedSkillDef = kSkillDefMgr.GetDef(iLearnedChildSkillNo))
								{
									iSkillPower += pkLearnedSkillDef->GetAbil(AT_PHY_ATTACK);
									iSkillPRate += pkLearnedSkillDef->GetAbil(AT_PHY_DMG_PER);
									//iSkillPRate += pkLearnedSkillDef->GetAbil(AT_PHY_DMG_PER2);
									iInvenAttackAddRate += pkLearnedSkillDef->GetAbil(AT_I_PHY_ATTACK_ADD_RATE);
								}
							}
						}
					}
					else
					{
						break;
					}
					++iSkillindex;
				}			
			}
		}
	}
	else
	{
		iMin = pkCaster->GetAbil(AT_C_MAGIC_ATTACK_MIN);
		iMax = pkCaster->GetAbil(AT_C_MAGIC_ATTACK_MAX);
		iSkillPower = pkSkillDef->GetAbil(AT_MAGIC_ATTACK);
		iSkillPRate = pkSkillDef->GetAbil(AT_MAGIC_DMG_PER);
		//iSkillPRate += pkSkillDef->GetAbil(AT_MAGIC_DMG_PER2);
		iInvenAttackAddRate = pkSkillDef->GetAbil(AT_I_MAGIC_ATTACK_ADD_RATE);

		//캐스터가 PgPlayer라면
		if(UT_PLAYER == pkCaster->UnitType())
		{
			if(PgPlayer* pkPlayer = dynamic_cast<PgPlayer*>(pkCaster))
			{
				int iSkillindex = 0;
				while(CHILD_SKILL_MAX > iSkillindex)
				{
					int const iChildBaseSkillNo = pkSkillDef->GetAbil(AT_CHILD_SKILL_NUM_01 + iSkillindex);
					if(0 < iChildBaseSkillNo)
					{
						if(PgMySkill* pkPlayerSkill = pkPlayer->GetMySkill())
						{
							//해당 스킬의 실제 레벨에 해당되는 스킬을 얻어 온다.
							int const iLearnedChildSkillNo = pkPlayerSkill->GetLearnedSkill(iChildBaseSkillNo);
							if(0 < iLearnedChildSkillNo)
							{
								if(CSkillDef const* pkLearnedSkillDef = kSkillDefMgr.GetDef(iLearnedChildSkillNo))
								{
									iSkillPower += pkLearnedSkillDef->GetAbil(AT_MAGIC_ATTACK);
									iSkillPRate += pkLearnedSkillDef->GetAbil(AT_MAGIC_DMG_PER);
									//iSkillPRate += pkLearnedSkillDef->GetAbil(AT_MAGIC_DMG_PER2);
									iInvenAttackAddRate += pkLearnedSkillDef->GetAbil(AT_I_MAGIC_ATTACK_ADD_RATE);
								}
							}
						}
					}
					else
					{
						break;
					}
					++iSkillindex;
				}			
			}
		}
	}

	if (0 == iSkillPRate)
	{
		iSkillPRate = ABILITY_RATE_VALUE;
	}

	if (bPhysicSkill)
	{
		// + 인 경우는 스킬의 최종 데미지를 감소 시키는 경우
		if(0 < pkCaster->GetAbil(AT_PHY_DMG_PER))
		{
			iSkillPRate = static_cast<int>(iSkillPRate * static_cast<float>(pkCaster->GetAbil(AT_PHY_DMG_PER)) / ABILITY_RATE_VALUE_FLOAT);
		}
		// - 인 경우는 스킬의 최종 데미지를 1로 만들어 버리는 경우
		else if(0 > pkCaster->GetAbil(AT_PHY_DMG_PER))
		{
			iSkillPRate += pkCaster->GetAbil(AT_PHY_DMG_PER);
		}
		// pkCaster AT_PHY_DMG_PER가 저렇게 동작해야 하는지 이해할수 없지만 사이드 이펙트 최소화를 위해 다시 계산해주는 부분을 만듬
		iSkillPRate += ( iSkillPRate * pkCaster->GetAbil(AT_PHY_DMG_PER2) ) / ABILITY_RATE_VALUE;
	}
	else
	{
		// + 인 경우는 스킬의 최종 데미지를 감소 시키는 경우
		if(0 < pkCaster->GetAbil(AT_MAGIC_DMG_PER))
		{
			iSkillPRate = static_cast<int>(iSkillPRate * static_cast<float>(pkCaster->GetAbil(AT_MAGIC_DMG_PER)) / ABILITY_RATE_VALUE_FLOAT);
		}
		// - 인 경우는 스킬의 최종 데미지를 1로 만들어 버리는 경우
		else if(0 > pkCaster->GetAbil(AT_MAGIC_DMG_PER))
		{
			iSkillPRate += pkCaster->GetAbil(AT_MAGIC_DMG_PER);
		}
		// pkCaster AT_MAGIC_DMG_PER가 저렇게 동작해야 하는지 이해할수 없지만 사이드 이펙트 최소화를 위해 다시 계산해주는 부분을 만듬
		iSkillPRate += ( iSkillPRate * pkCaster->GetAbil(AT_MAGIC_DMG_PER2) ) / ABILITY_RATE_VALUE;
	}

	DWORD const iInvenAttackRandomValue = pkCaster->GetRandom();
	if( iInvenAttackAddRate )
	{
		int iInvenAttackMin = 0;
		int iInvenAttackMax = 0;
		if (bPhysicSkill)
		{
			iInvenAttackMin = pkCaster->GetAbil(AT_EQUIP_WEAPON_PHY_ATTACK_MIN);
			iInvenAttackMax = pkCaster->GetAbil(AT_EQUIP_WEAPON_PHY_ATTACK_MAX);			
		}
		else
		{
			iInvenAttackMin = pkCaster->GetAbil(AT_EQUIP_WEAPON_MAGIC_ATTACK_MIN);
			iInvenAttackMax = pkCaster->GetAbil(AT_EQUIP_WEAPON_MAGIC_ATTACK_MAX);			
		}

		int const iInvenAttack = abs(iInvenAttackMax - iInvenAttackMin);
		
		int iResult = 0;
		if(0 != iInvenAttack)
		{
			iResult = iInvenAttackRandomValue % iInvenAttack;
		}

		iAddPwoer = iResult + iInvenAttackMin;
		iAddPwoer = int(iAddPwoer * (float)iInvenAttackAddRate / ABILITY_RATE_VALUE_FLOAT);
	}

	int iCriticalRate = pkCaster->GetAbil(AT_C_CRITICAL_RATE);
	int iCriticalPowerRate = pkCaster->GetAbil(AT_C_CRITICAL_POWER);
	if (iCriticalPowerRate == 0)
	{
		iCriticalPowerRate = ABILITY_RATE_VALUE * 2;	// 200%
	}
	int const iHitRate = pkCaster->GetAbil(AT_C_HITRATE);
	int iDamage = 0;

	// Target이 실제로 Damage를 입든 안입든, Target이 다시 잡히지 않도록 해야 한다.
	pkCaster->OnTargetDamaged(pkTarget->GetID());
	pkResult->SetValue(0);
	if(	(pkTarget == NULL) 
	||	(pkTarget->IsDead()))
	{
		pkResult->SetInvalid(true);
		return true;
	}

	int iRandValue = pkCaster->GetRandom() % ABILITY_RATE_VALUE;
	int const iDecDodgeAbs = pkCaster->GetAbil(AT_DEC_TARGETDODGE_ABS);
	int const iDecDodgeRate = pkCaster->GetAbil(AT_DEC_TARGETDODGE_RATE);

	// 2007-12-05 변경 후의 공식
	// Caster:HitRate - Target:Dodge => 타격성공 여부 결정
#ifdef BATTLE_DEBUG
	INFO_LOG(BM::LOG_LV9, __FL__<<L"\tHitRate["<<iHitRate<<L"] - Dodge["<<pkTarget->GetAbil(AT_C_DODGE_RATE)<<L"], Rand["<<iRandValue<<L"]");
#endif

	int iDecHitRate = CalcDecHitRate(pkCaster, pkTarget, iHitRate, iDecDodgeAbs, iDecDodgeRate);

#ifdef BATTLE_DEBUG
	INFO_LOG(BM::LOG_LV9, _T("iDecHitRate =") << iDecHitRate);
#endif
	if (iDecHitRate < iRandValue)
	{
		// 타격 실패 (Missed)
		pkResult->SetMissed(true);
#ifdef BATTLE_DEBUG
		INFO_LOG(BM::LOG_LV9, __FL__<<L"--Missed");
#endif
		return true;
	}
	// 2007-12-05 변경되기 전의 공식
	/*
	if (iRandValue < pkTarget->GetAbil(AT_C_DODGE_RATE))
	{
		// 아싸 회피 성공
		pkResult->SetDodged(true);
		return true;
	}
	if (iRandValue > iHitRate)
	{
		// 명중율 실패
		pkResult->SetMissed(true);
		return true;
	}
	*/
		
	//기본적인 공격자의 공격력 + 스킬 공격력
	iRandValue = pkCaster->GetRandom() % ABILITY_RATE_VALUE;
	// 최종공격력(물리) = 기본공격력 + 아이템공격력 + 물리후방공격력
	// 최종공격력(마법) = (기본공격력 + 아이템공격력) * 1.5
	int const iAddDmg = (bPhysicSkill ? (pkCaster->GetAbil(AT_C_ABS_ADDED_DMG_PHY)) : (pkCaster->GetAbil(AT_C_ABS_ADDED_DMG_MAGIC)));
	
	iDamage = int((iMin + iRandValue % __max(1, iMax-iMin)) * ((bPhysicSkill) ? 1.0 : 1.5)) + iAddDmg;
	iDamage += iSkillPower;
	iDamage += iAddPwoer;
#ifdef BATTLE_DEBUG
	INFO_LOG(BM::LOG_LV9, __FL__<<L"AttackPower["<<iDamage<<L"] = AttackPower + 2ndPower["<<iAddDmg<<L"]");
#endif

	// 여기가 위에 함수와 다른 부분 /////////////////////////
	iDamage = iDamage * iDmgRate / ABILITY_RATE_VALUE + iDmg;
	/////////////////////////////////////////////////////////

	int iExtraDamageRate = pkCaster->GetAbil(AT_1ST_ATTACK_ADDED_RATE);
	if (iExtraDamageRate != 0)
	{
		iDamage = iDamage + iDamage * iExtraDamageRate / ABILITY_RATE_VALUE;

		// 1방에만 적용하는 기능을 여러대 적용하는 어빌이
		int const iApplyNum = pkCaster->GetAbil(AT_1ST_ATTACK_ADDED_RATE_APPLY_NUM);
		if(1 < iApplyNum)
		{// 충분하게 존재하면 횟수만 감소 시키고
			pkCaster->SetAbil(AT_1ST_ATTACK_ADDED_RATE_APPLY_NUM, iApplyNum-1);
		}
		else
		{// 이번이 마지막 적용이면 0으로 횟수와, 증가값을 초기화 시킨다 
			pkCaster->SetAbil(AT_1ST_ATTACK_ADDED_RATE_APPLY_NUM, 0);
			pkCaster->SetAbil(AT_1ST_ATTACK_ADDED_RATE, 0);
		}
	}
	iDamage = CS_CheckDamage(pkCaster, pkTarget, iDamage, bPhysicSkill, pkResult, iSkillPRate, pkSkillDef->IsSkillAtt(SAT_CLIENT_CTRL_PROJECTILE), pkSkillDef);

	iRandValue = pkCaster->GetRandom() % ABILITY_RATE_VALUE;
	if (iRandValue < iCriticalRate)
	{
		iDamage = iDamage * iCriticalPowerRate / ABILITY_RATE_VALUE;
		pkResult->SetCritical(true);
		iDamage = CS_CheckItemDamage( pkCaster, pkTarget, true, iDamage );
	}
	else
	{
		iDamage = CS_CheckItemDamage( pkCaster, pkTarget, false, iDamage );
	}
	
	// 반사 데미지 계산 및 ActionResult에 설정
	PgSkillHelpFunc::CalcReflectDamage(pkCaster, pkTarget, iDamage, pkResult, pkSkillDef, bPhysicSkill, false, false);

	AddDamageEffect(pkCaster, pkTarget, pkSkillDef, pkResult);

	if(0 == pkTarget->GetAbil(AT_CANNOT_DAMAGE)) //데미지를 입을 수 있는 상태일때만 세팅
	{
		pkResult->SetValue(iDamage);
	}
	return true;
}

//어떤 Abil에 따라 Damage를 조정해 주어야 할 필요가 있다.
int CS_CheckDamage(CUnit* pkCaster, CUnit* pkTarget, int iDamage, bool bPhysical, PgActionResult* pkResult, int const iSkillRate, bool const bProjectileDmg, CSkillDef const* pkSkillDef)
{
	int iDecPer = 0;
	int iDefence = 0;
	int iDefenceIgnoreRate = 0;

	if (bPhysical)
	{
		// 최종물리방어력 = (기본 물리 방어력+방어구의 방어력 합)*0.4
		//iDefence = (int) pkTarget->GetAbil(AT_C_PHY_DEFENCE) * (1 + pkTarget->GetAbil(AT_PHY_DEFENCE)/1000.0);
		iDefence = static_cast<int>(pkTarget->GetAbil(AT_C_PHY_DEFENCE) * 0.4f);
		iDecPer = pkTarget->GetAbil(AT_C_PHY_DMG_DEC);
		iDefenceIgnoreRate = pkCaster->GetAbil(AT_PHY_DEFENCE_IGNORE_RATE);
	}
	else
	{
		iDefence = static_cast<int>(pkTarget->GetAbil(AT_C_MAGIC_DEFENCE) * 0.5f);
		iDecPer = pkTarget->GetAbil(AT_C_MAGIC_DMG_DEC);
		iDefenceIgnoreRate = pkCaster->GetAbil(AT_MAGIC_DEFENCE_IGNORE_RATE);
	}

	int iRandValue = pkCaster->GetRandom() % ABILITY_RATE_VALUE;
	
	//방어 무시 확률이 있을 경우
	if(0 < iDefenceIgnoreRate)
	{
		if(iRandValue < iDefenceIgnoreRate)
		{
			iDecPer = 0;
			iDefence = 0;			
		}
	}

	// 방어력으로 인한 Damage감소
#ifdef BATTLE_FORMULA_081120_ICE
	int iDecPerDamage = static_cast<int>(iDamage * (static_cast<float>(iDecPer) / ABILITY_RATE_VALUE_FLOAT));
	// 감소하는 데미지가 음수 이면 모든 데미지가 감소한 것이다.
	if(0 > iDecPerDamage)
	{
		iDamage = 1;
	}
	else
	{
		iDamage -= iDecPerDamage;
	}

	if (UT_PLAYER == pkTarget->UnitType())
	{
		int iCasterLevel = 0;
		EUnitType eCasterType = UT_NONETYPE;
		GetCasterLevelAndType(pkCaster, iCasterLevel, eCasterType);

		// Player 일때만 적용
		float const fRate1 = (eCasterType == UT_PLAYER) ? (0.5f) : (12.0f);
		float const fRate2 = (eCasterType == UT_PLAYER) ? (1.0f) : (1.2f);
		int const iBaseValue = (eCasterType == UT_PLAYER) ? (100) : (10);
		//int const iTargetLevel = pkTarget->GetAbil(AT_LEVEL);
		
		// Player 방어력이 0보다 작다면 0으로 두고 계산한다. (그렇지 않다면 아래 공식으로는 데미지 1이 나온다)
		int const iTempDefence = iDefence < 0 ? 0 : iDefence;
		float fTempReductionRate = (iTempDefence - iCasterLevel*fRate2) / (iTempDefence - iCasterLevel * fRate2 + iBaseValue + fRate1 * iCasterLevel);
		fTempReductionRate = __max(0.0f, fTempReductionRate); //데미지 감쇄율이 0% 미만이 될 수 없다.
		fTempReductionRate = __min(1.0f, fTempReductionRate); //데미지 감쇄율이 100%를 초과 할 수 없다.
		float const fReductionRate = fTempReductionRate;
		iDamage = static_cast<int>(iDamage * (1.0f - fReductionRate));
	}
	else
	{
		iDamage -= iDefence;
	}

	iDamage = __max(1, iDamage);

	if(1 == iDamage)
	{
		// 데미지가 최소치가 되었을 경우, 한번 데미지로 연타로 나누어 연출하는 스킬의 경우 데미지를 나누는만큼 증가시켜야 한다.
		if(pkSkillDef)
		{
			int const iComBoHit = pkSkillDef->GetAbil(AT_COMBO_HIT_COUNT);
			if(0 < iComBoHit)
			{
				iDamage *= iComBoHit;
			}
		}
	}
#else
	iDamage = iDamage - iDamage * iDecPer / ABILITY_RATE_VALUE - iDefence;
#endif
#ifdef BATTLE_DEBUG
	INFO_LOG(BM::LOG_LV9, __FL__<<L"Damage["<<iDamage<<L"] = BeforeDmg - BeforeDmg * ["<<iDecPer<<L"] - ["<<iDefence<<L"]");
#endif
	//GET_DEF(PgDefPropertyMgr, kPropertyMgr);
	//float const fPropertyRate = static_cast<float>(kPropertyMgr.GetRate(pkCaster->GetAbil(AT_OFFENSE_PROPERTY),pkCaster->GetAbil(AT_OFFENSE_PROPERTY_LEVEL), pkTarget->GetAbil(AT_DEFENCE_PROPERTY),pkTarget->GetAbil(AT_DEFENCE_PROPERTY_LEVEL)) / ABILITY_RATE_VALUE);
	//iDamage = static_cast<int>(iDamage * fPropertyRate);
#ifdef BATTLE_DEBUG
		//INFO_LOG(BM::LOG_LV9, _T("Damage[%d] = Damage * fPropertyRate[%.4f]"), iDamage, fPropertyRate);
#endif
	iDamage = static_cast<int>(iDamage * static_cast<float>(iSkillRate) / ABILITY_RATE_VALUE_FLOAT);
#ifdef BATTLE_DEBUG
	INFO_LOG(BM::LOG_LV9, __FL__<<L"Damage["<<iDamage<<L"] = Damage * SkillPRate["<<iSkillRate<<L"]");
#endif

	iDamage += static_cast<int>(iDamage * static_cast<float>(pkCaster->GetAbil(AT_SKILL_EFFICIENCY)) / ABILITY_RATE_VALUE_FLOAT);

	// 이펙트에 의해서 추가되는 절대 데미지
	int const iAddDmg2 = (bPhysical ? (pkCaster->GetAbil(AT_ABS_ADDED_DMG_PHY_2)) : (pkCaster->GetAbil(AT_ABS_ADDED_DMG_MAGIC_2)));
	iDamage += iAddDmg2;

	// 절대 데미지 감소
	int const iSubtractDmg = (bPhysical ? (pkTarget->GetAbil(AT_PHY_DMG_SUBTRACT)) : (pkTarget->GetAbil(AT_MAGIC_DMG_SUBTRACT)));
	iDamage -= iSubtractDmg;

	// 발사체 데미지 감소가 있을 경우
	if(bProjectileDmg)
	{
		int const iSubtractProjectileDmg =  pkTarget->GetAbil(AT_PROJECTILE_DMG_SUBTRACT);
		iDamage -= iSubtractProjectileDmg;
	};

	//Plyer일 때는 그레이드에 따라 데미지 증폭은 적용되지 않아야 함.
	if (UT_PLAYER != pkTarget->UnitType())
	{
		// 몬스터 타격시 그레이드에 따라 데미지 증가 어빌이 있을 경우
		int iGradeAddDmgRate = 0;
		switch(pkTarget->GetAbil(AT_GRADE))
		{
		case EMGRADE_NORMAL:
			{
				iGradeAddDmgRate = pkCaster->GetAbil(AT_GRADE_NORMAL_DMG_ADD_RATE);
			}break;
		case EMGRADE_UPGRADED:
			{
				iGradeAddDmgRate = pkCaster->GetAbil(AT_GRADE_UPGRADED_DMG_ADD_RATE);
			}break;
		case EMGRADE_ELITE:
			{
				iGradeAddDmgRate = pkCaster->GetAbil(AT_GRADE_ELITE_DMG_ADD_RATE);
			}break;
		case EMGRADE_BOSS:
			{
				iGradeAddDmgRate = pkCaster->GetAbil(AT_GRADE_BOSS_DMG_ADD_RATE);
			}break;
		default:
			{
				iGradeAddDmgRate = 0;
			}break;
		}

		if(0 < iGradeAddDmgRate)
		{
			iDamage += static_cast<int>(iDamage * static_cast<float>(iGradeAddDmgRate) / ABILITY_RATE_VALUE_FLOAT);
		}
	}

	//Caster가 Monster일 때만 적용
	if (UT_MONSTER == pkCaster->UnitType())
	{
		// 몬스터 타격시 그레이드에 따라 데미지 감소 어빌이 있을 경우
		int iGradeAddDmgRate = 0;

		//물리 데미지 감소
		if(bPhysical)
		{
			switch(pkCaster->GetAbil(AT_GRADE))
			{
			case EMGRADE_NORMAL:
				{
					iGradeAddDmgRate = pkTarget->GetAbil(AT_GRADE_NORMAL_PHY_DMG_DEC_RATE);
				}break;
			case EMGRADE_UPGRADED:
				{
					iGradeAddDmgRate = pkTarget->GetAbil(AT_GRADE_UPGRADED_PHY_DMG_DEC_RATE);
				}break;
			case EMGRADE_ELITE:
				{
					iGradeAddDmgRate = pkTarget->GetAbil(AT_GRADE_ELITE_PHY_DMG_DEC_RATE);
				}break;
			case EMGRADE_BOSS:
				{
					iGradeAddDmgRate = pkTarget->GetAbil(AT_GRADE_BOSS_PHY_DMG_DEC_RATE);
				}break;
			default:
				{
					iGradeAddDmgRate = 0;
				}break;
			}

			if(0 < iGradeAddDmgRate)
			{
				iDamage -= static_cast<int>(iDamage * static_cast<float>(iGradeAddDmgRate) / ABILITY_RATE_VALUE_FLOAT);
			}
		}
		// 마법 데미지 감소
		else
		{
			switch(pkCaster->GetAbil(AT_GRADE))
			{
			case EMGRADE_NORMAL:
				{
					iGradeAddDmgRate = pkTarget->GetAbil(AT_GRADE_NORMAL_MAGIC_DMG_DEC_RATE);
				}break;
			case EMGRADE_UPGRADED:
				{
					iGradeAddDmgRate = pkTarget->GetAbil(AT_GRADE_UPGRADED_MAGIC_DMG_DEC_RATE);
				}break;
			case EMGRADE_ELITE:
				{
					iGradeAddDmgRate = pkTarget->GetAbil(AT_GRADE_ELITE_MAGIC_DMG_DEC_RATE);
				}break;
			case EMGRADE_BOSS:
				{
					iGradeAddDmgRate = pkTarget->GetAbil(AT_GRADE_BOSS_MAGIC_DMG_DEC_RATE);
				}break;
			default:
				{
					iGradeAddDmgRate = 0;
				}break;
			}

			if(0 < iGradeAddDmgRate)
			{
				iDamage -= static_cast<int>(iDamage * static_cast<float>(iGradeAddDmgRate) / ABILITY_RATE_VALUE_FLOAT);
			}
		}
	}

	iDamage = __max(1, iDamage);

	if(pkTarget->GetAbil(AT_INVINCIBLE)) // 무적 어빌이 적용되어 있다면
	{
		iDamage = 0;
	}

	if (iDamage > 0)
	{
		int iBarrierCount = pkTarget->GetAbil(AT_BARRIER_100PERECNT_COUNT);
		int iBarrerAmount = pkTarget->GetAbil(AT_BARRIER_AMOUNT);

		// 방어 스킬 우선 순위 CANNOT_DAMAGE > AT_BARRIER_100PERECNT_COUNT > AT_BARRIER_AMOUNT > AT_DMG_CONSUME_MP
		//100% 데미지 흡수 배리어가 발동 순서가 우선순위 이다.

		if(iBarrierCount)
		{
			int iBarrier1 = pkTarget->GetAbil(AT_BARRIER_100PERECNT_COUNT);
			int iBarrier2 = pkTarget->GetAbil(AT_PREV_BARRIER_100PERECNT_COUNT);
			int iValue = iBarrier1 - iBarrier2;

			pkTarget->GetEffectMgr().AddAbil(AT_BARRIER_100PERECNT_COUNT, -1);
			pkTarget->NftChangedAbil(AT_BARRIER_100PERECNT_COUNT, E_SENDTYPE_EFFECTABIL);

			pkTarget->GetEffectMgr().AddAbil(AT_PREV_BARRIER_100PERECNT_COUNT, iValue);
			pkTarget->NftChangedAbil(AT_PREV_BARRIER_100PERECNT_COUNT, E_SENDTYPE_EFFECTABIL);

			iDamage = 0;
		}
		//데미지 흡수 배리어가 발동한다.
		else if(iBarrerAmount)
		{
			int nAmount = iBarrerAmount - iDamage;

			if(nAmount > 0)
			{
				// 데미지만큼 배리어를 감소 하고 데미지는 0으로 한다.
				pkTarget->GetEffectMgr().AddAbil(AT_BARRIER_AMOUNT, -iDamage);
				pkTarget->NftChangedAbil(AT_BARRIER_AMOUNT, E_SENDTYPE_EFFECTABIL);
				if(pkResult)
				{
					pkResult->SetAbsorbValue(iDamage);
				}
				iDamage = 0;
			}
			else
			{
				// 배리어를 0으로 하고 남은 만큼을 데미지로 한다.
				pkTarget->GetEffectMgr().AddAbil(AT_BARRIER_AMOUNT, -iBarrerAmount);
				pkTarget->NftChangedAbil(AT_BARRIER_AMOUNT, E_SENDTYPE_EFFECTABIL);
				if(pkResult)
				{
					pkResult->SetAbsorbValue(iBarrerAmount);
				}
				iDamage = -nAmount;
			}
		}
		else
		{	
			// 마나쉴드 같이 Damage를 다시 조절해 주어야 할 스킬
			int iMPPer = pkTarget->GetAbil(AT_DMG_CONSUME_MP);
			int iDmgDec = 0;
			if (bPhysical)
			{
				iDmgDec = iDamage * pkTarget->GetAbil(AT_PDMG_DEC_RATE_2ND) / ABILITY_RATE_VALUE;
			}
			else
			{
				iDmgDec = iDamage * pkTarget->GetAbil(AT_MDMG_DEC_RATE_2ND) / ABILITY_RATE_VALUE;
			}
			if (iMPPer > 0)
			{
				int iCount = pkTarget->GetAbil(AT_DMG_DEC_RATE_COUNT);
				if (iCount > 0)
				{
					pkTarget->SetAbil(AT_DMG_DEC_RATE_COUNT, iCount-1);
				}
				if (iCount != 0)	// >0 이면 개수를 줄여주는 의미, -1 이면 회수는 unlimited
				{
					int iMPNeed = iDmgDec / iMPPer;
					int iMP = pkTarget->GetAbil(AT_MP);
					if (iMP < iMPNeed)
					{
						pkTarget->SetAbil(AT_MP, 0, true);
						iDmgDec = iMP * iMPPer;
					}
					else
					{
						pkTarget->SetAbil(AT_MP, iMP - iMPNeed, true);
					}
				}
			}
			iDamage = __max(1, iDamage - iDmgDec);
		}
	}
	if(0 < pkTarget->GetAbil(AT_AWAKE_CHARGE_STATE))
	{// 각성 챠지 스킬을 사용중이고
		if(0 < iDamage)
		{// 데미지가 있다면
			pkTarget->SetAbil(AT_IS_DAMAGED_DURING_AWAKE_CHARGE, 1);
		}
	}

	// 암살자 : 베놈크래셔 스킬은 최종Damage에서 추가적인 Dmg를 더해 주어야 한다.
	//iDamage = iDamage + pkCaster->GetAbil(AT_ABS_ADDED_DMG);
	return iDamage;
}

int CS_GetReflectDamage(CUnit* pkCaster, CUnit* pkTarget, int const iDamage, bool bPhysicSkill)
{	
	int iResult = 0;
	{//스킬로 인한
		int const iHitRate = pkTarget->GetAbil(AT_C_DMG_REFLECT_HITRATE);
		if (0 < iHitRate)
		{// 반사가 일어날 확률이 존재하고
			int const iValue = int(pkCaster->GetRandom() % ABILITY_RATE_VALUE);
			if(iHitRate > iValue)
			{// 발동 한다면
				int const iReflectRate = pkTarget->GetAbil(AT_C_DMG_REFLECT_RATE);
				if (0 < iReflectRate)
				{// 받은 데미지의 %만큼 반사하는 값을 계산해 적용해주고
					iResult = iDamage * iReflectRate / ABILITY_RATE_VALUE;
				}
			}
		}
	}

	{// 아이템으로 인한
		iResult += CS_GetReflectDamage_FromItem(pkCaster, pkTarget, iDamage, bPhysicSkill);
	}	
	
	if(0 < iResult )
	{// 카오스맵이라면 반사 데미지도 1이 될수 있는 체크를 해야한다.
		iResult = CS_CheckDmgPer(pkTarget, iResult, bPhysicSkill);
	}

	if(	UT_PLAYER == pkCaster->UnitType() )
	{
		int const iRemainHP = pkCaster->GetAbil(AT_HP) - iResult;
		int const iMaxHP = pkCaster->GetAbil(AT_C_MAX_HP);
		int const iRemainHP_10000Per = iRemainHP*ABILITY_RATE_VALUE;
		static int _1000Per = ABILITY_RATE_VALUE / 10;
		if(_1000Per  >= iRemainHP_10000Per / iMaxHP )
		{// 이번 반사로 인해 10%(만분률이므로 1000) HP가 된다면 반사 데미지는 0으로 한다
			return 0;
		}
	}

	return iResult;
}

int CS_GetReflectDamage_FromItem(CUnit* pkCaster, CUnit* pkTarget, int const iDamage, bool bPhysicSkill)
{	
	PgInventory* pkInv = pkTarget->GetInven();
	if(!pkInv)
	{
		return 0;
	}

	int iResult = 0;
	for(int i=AT_DMG_REFLECT_RATE_FROM_ITEM01; i <= AT_DMG_REFLECT_RATE_FROM_ITEM10; ++i)
	{
		int const iReflectRateType_FromItemAbilType = i;
		int const iReflectDmg_FromItemAbilType = i+10;
		
		int const iHitRate_FromItem = pkInv->GetAbil(iReflectRateType_FromItemAbilType);
		if(0 < iHitRate_FromItem)
		{
			int const iRandValue = pkCaster->GetRandom() % ABILITY_RATE_VALUE;
			if(iRandValue < iHitRate_FromItem)
			{// 반사 데미지 절대값이 있다면 추가적으로 더해주고
				iResult += pkInv->GetAbil(iReflectDmg_FromItemAbilType);
			}
		}
	}

	if(0 < iResult )
	{// 카오스맵이라면 반사 데미지도 1이 될수 있는 체크를 해야한다.
		iResult = CS_CheckDmgPer(pkTarget, iResult, bPhysicSkill);
	}

	return iResult;
}

// pkCaster : 최초 Caster (즉 Reflected 당해서 Damage 받을 unit)
// pkTarget : 최초 Target (즉 Reflect 효과를 가지고서 반사 데미지를 발생시킨 Unit)
// [RETURN]
//	Caster의 남은 HP값
int CS_DoReflectDamage(CUnit const* pkCaster, CUnit const* pkTarget, int const iSkillNo, int const iReflected)
{
	int const iHP = pkCaster->GetAbil(AT_HP);
	return __max(0, iHP-iReflected);
}

DWORD DifftimeGetTime(DWORD const& dwStart, DWORD const& dwEnd)
{
	return dwEnd - dwStart;//오버플로우를 고려 해도. 테스트 해본결과 이게 맞음.

/*	if( dwStart < dwEnd )
	{
		return dwEnd - dwStart;
	}
	else if( dwStart > dwEnd )//dwEnd Overflow
	{
		return (ULONG_MAX - dwStart) + dwEnd;
	}
	//else//dwStart == dwEnd
*/	return 0;
}

int SetDamageDelay(CSkillDef const *pkSkillDef, CUnit const* pkCaster, CUnit* pkTarget)
{
	if(!pkSkillDef || !pkCaster || !pkTarget)
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L"Cannot Get SkillDef");
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return 0"));
		return 0;
	}

	BM::GUID const& rkCasterGuid = (pkCaster != NULL) ? pkCaster->Caller() : BM::GUID::NullData();
	//if(0==(UT_MONSTER & pkTarget->UnitType()))
	{
		pkTarget->SetState(US_PUSHED_BY_DAMAGE);
	}
	int iDelayTime = 0;

	switch(pkSkillDef->GetAbil(AT_DMG_DELAY_TYPE))
	{
	case 1: {iDelayTime = pkSkillDef->GetAbil(AT_DAMAGED_DELAY); break;}	// Skill의 DamagedDley값
	case 2: {iDelayTime = pkSkillDef->GetAbil(AT_DAMAGED_DELAY) + pkTarget->GetAbil(AT_DAMAGED_DELAY); break;} // Skill+Unit
	case 0:
	default:
		{
			iDelayTime = pkTarget->GetAbil(AT_DAMAGED_DELAY);
			break;
		}
	}

	if(0 == iDelayTime)
	{
		// Dmg Ani 할 시간을 줘야 한다.
		iDelayTime = 450;
	}

	// Desc : 맞으면 처음 부터 캐스팅 하게 됨.
	pkTarget->SetDelay(iDelayTime);	// Monster가 피격 애니를 하는 시간(그동안 서버에서도 가만히 있어야 함)
	pkTarget->GetAI()->SetEvent(rkCasterGuid, EAI_EVENT_DAMAGED);

	return iDelayTime;
}

void CheckActionResultVec(UNIT_PTR_ARRAY& rkTargetArray, PgActionResultVector& kActionResultVec)
{
	//대신 맞을 유닛이 있는지 검사 하고 처리하는 루틴

	int iIndex = 0;

	//임시로 사용하는 구조체 이곳에서만 쓰인다.
	typedef struct _tagGuidValue
	{
		_tagGuidValue() : m_iValue(0)
		{}

		int			m_iValue;	// ActionResult.GetValue 
		BM::GUID	m_kGuid;	// DeliverDamageUnit Guid
	} sGuidValue;

	std::list<sGuidValue> kGuidList; 
	kGuidList.clear();

	for(UNIT_PTR_ARRAY::iterator itor = rkTargetArray.begin(); itor != rkTargetArray.end(); ++itor)
	{
		// 참고용 유닛은 리스트에서 지나친다.
		if(true == (*itor).bReference)
		{
			continue;
		}

		// 복구해야하는 유닛이면 복구 처리 한다.
		if(true == (*itor).bRestore)
		{
			CUnit* pkTarget = (*itor).pkUnit;
			if(NULL != pkTarget)
			{
				PgActionResult* pkActionResult = kActionResultVec.GetResult(pkTarget->GetID());
				if(NULL != pkActionResult)
				{
					//남은 HP는 서버에 있는 유닛의 현재 HP로 세팅한다.
					pkActionResult->Init();
					pkActionResult->SetRemainHP(pkTarget->GetAbil(AT_HP));
					pkActionResult->SetRestore(true);					
				}
			}

			continue;
		}

		BM::GUID kGuid = (*itor).pkUnit->GetDeliverDamageUnit();

		//데미지를 대신 맞는 유닛이 있다.
		if(!BM::GUID::IsNull(kGuid))
		{
			if((*itor).pkUnit->GetID() == kGuid) // 대신 맞는 유닛이 자신 일 경우
			{
				continue;
			}

			//원래의 데미지와 / 대신 맞을 유닛의 guid를 저장하고 / 원래 유닛의 Damage를 0으로 한다.
			PgActionResult* pkActionResult = kActionResultVec.GetResult((*itor).pkUnit->GetID()); //원래 유닛의 ActionResult를 얻어온다.
			int iValue = 0;
			if(pkActionResult)
			{
				iValue = pkActionResult->GetValue();
				pkActionResult->SetValue(0);
				pkActionResult->AddAbil(AT_NO_DAMAGE_USE_DAMAGEACTION,1); // 데미지는 없으나 모션만 취한다.
			}

			// 데미지가 있을 경우에만 처리
			if(0 < iValue)
			{
				sGuidValue kValue;
				// 10000분률 
				kValue.m_iValue = iValue - static_cast<int>(static_cast<float>(iValue) * (static_cast<float>((*itor).pkUnit->GetAbil(AT_DELIVER_DAMAGE_BLOCK_RATE)) / ABILITY_RATE_VALUE_FLOAT)); //대신 입는 블럭 데미지 감소율을 적용
				kValue.m_kGuid = kGuid;
				//대신 맞는 경우 데미지가 있을 시에만 저장한다.
				kGuidList.push_back(kValue);
			}
		}
	}

	if(!kGuidList.empty())
	{
		// 실제로 데미지를 입는 유닛은 따로 있다.
		for(std::list<sGuidValue>::iterator itor2 = kGuidList.begin(); itor2 != kGuidList.end(); ++itor2)
		{
			PgActionResult* pkActionResult = kActionResultVec.GetResult((*itor2).m_kGuid, true);

			if(pkActionResult)
			{
				pkActionResult->SetCollision(true);
				pkActionResult->SetValue((*itor2).m_iValue);
				pkActionResult->AddAbil(AT_DAMAGEACTION_TYPE, 101); // 데미지 모션을 취하지 않으면서 데미지만 입는다.
			}
		}
	}
}

int CalcDecHitRate(CUnit const* pkCaster, CUnit const* pkTarget, int const iHitRate, int const iDecDodgeAbs, int const iDecDodgeRate)
{
	int iDodge = pkTarget->GetAbil(AT_C_DODGE_RATE);
#ifdef BATTLE_DEBUG
	INFO_LOG(BM::LOG_LV7, _T("Before iDodge=") << iDodge << _T(", DecDodgeAbs=") << iDecDodgeAbs << _T(", DecDodgeRate=") << iDecDodgeRate);
#endif
	iDodge = iDodge - static_cast<int>(iDecDodgeAbs + iDodge / ABILITY_RATE_VALUE_FLOAT * iDecDodgeRate);
#ifdef BATTLE_DEBUG
	INFO_LOG(BM::LOG_LV7, _T("After iDodge=") << iDodge);
#endif

	int iDecHitRate = std::max(iHitRate - iDodge,3000); // 아이템으로 증가시킨 회피율은 최소 30% 이하로 내려가지 않는다.

	//{ // 인챈트 차이에 의한 명중률 변화
	//	if( 0 != pkTarget->GetAbil(AT_MON_ENCHANT_LEVEL)
	//	||	0 != pkCaster->GetAbil(AT_MON_ENCHANT_LEVEL) )
	//	{
	//		int const iDiffEnchantLevel = pkCaster->GetAbil(AT_OFFENCE_ENCHANT_LEVEL) - pkTarget->GetAbil(AT_DEFENCE_ENCHANT_LEVEL);
	//		iDecHitRate += int(MAKE_ABIL_RATE(iDiffEnchantLevel / 2.0f));
	//	}
	//}

#ifdef BATTLE_FORMULA_081120_ICE
	int iCasterLevel = 0;
	EUnitType eCasterType = UT_NONETYPE;
	GetCasterLevelAndType(pkCaster, iCasterLevel, eCasterType);
	
	EUnitType const eTargetType = pkTarget->UnitType();
	
	int const iLevelDiff = iCasterLevel - pkTarget->GetAbil(AT_LEVEL);
#ifdef BATTLE_DEBUG
	INFO_LOG(BM::LOG_LV7, _T("PLAYER->MONSTER : LevelDiff=") << iLevelDiff << _T(", HitRate=") << iHitRate);
#endif
	if (eCasterType == UT_PLAYER)
	{
		if (eTargetType == UT_MONSTER)
		{
			if (iLevelDiff < 0)
			{
				// Target_Level(Monster) > Caster_Level(Player)
				iDecHitRate -= int(MAKE_ABIL_RATE(pow(float(iLevelDiff), 2.0f) * 0.6f - 2.4f));
			}
			else
			{
				iDecHitRate += int(MAKE_ABIL_RATE(iLevelDiff / 2.0f));
			}
		}
		else
		{
			iDecHitRate += int(MAKE_ABIL_RATE(iLevelDiff / 2.0f));
		}
	}
#endif

	return iDecHitRate;
}

int CS_CheckItemDamage( CUnit *pkCaster, CUnit *pkTarget, bool const bIsCritical, int iDamage )
{
	if ( 0 < iDamage )
	{
		if ( true == bIsCritical )
		{
			iDamage += SRateControl::Get<SRateControl::E_RATE_VALUE>( AT_CRITICAL_DAMAGE_ACTIVATE, pkCaster, iDamage );
			iDamage += SRateControl::Get<SRateControl::E_RATE_RATE>( AT_CRITICAL_DAMAGE_RATE_ACTIVATE, pkCaster, iDamage );
		}
		else
		{
			int iHP = pkTarget->GetAbil( AT_HP );
			if ( iHP > iDamage )
			{
				iDamage += SRateControl::Get<SRateControl::E_RATE_RATE>( AT_HIT_DAMAGE_HP_RATE_ACTIVATE, pkCaster, iHP - iDamage );
			}

			int iMP = pkTarget->GetAbil( AT_MP );
			if ( 0 < iMP )
			{
				int iRemoveMP = SRateControl::Get<SRateControl::E_RATE_RATE>( AT_HIT_DECREASE_MP_RATE_ACTIVATE, pkCaster, iMP );
				if ( 0 < iRemoveMP )
				{
					iMP = std::max( 0, iMP - iRemoveMP );
					pkTarget->SetAbil( AT_MP, iMP, true );
				}
			}
		}

		int const iCheckRate = pkTarget->GetAbil( AT_ATTACKED_DECREASE_DAMAGE_ACTIVATE_HP_RATE );
		if ( 0 < iCheckRate )
		{
			double const fMaxHP = static_cast<double>(pkTarget->GetAbil( AT_C_MAX_HP ));
			if ( 0.0 < fMaxHP )
			{
				if ( (static_cast<double>(iCheckRate) / ABILITY_RATE_VALUE_FLOAT) >= (pkTarget->GetAbil(AT_HP) / fMaxHP) )
				{
					iDamage -= SRateControl::GetResult<SRateControl::E_RATE_RATE>( AT_ATTACKED_DECREASE_DAMAGE_RATE, pkTarget, iDamage );
				}
			}
		}
	}
	return iDamage;
}

int CS_CheckDmgPer(CUnit* pkTarget, int const iDamage, bool bPhysicSkill)
{
	int iNewDamage = iDamage;

	bool bChosMap = false; // 데미지가 1이 되는 카오스 맵인가?
	int iDmgPer = 0;

	if(bPhysicSkill)
	{
		iDmgPer = pkTarget->GetAbil(AT_PHY_DMG_PER);
		if(0 > iDmgPer)
		{
			bChosMap = true;				
		}
	}
	else
	{
		iDmgPer = pkTarget->GetAbil(AT_MAGIC_DMG_PER);
		if(0 > iDmgPer)
		{
			bChosMap = true;
		}
	}

	if(bChosMap)
	{
		iNewDamage = static_cast<int>(iNewDamage * static_cast<float>(iDmgPer) / ABILITY_RATE_VALUE_FLOAT);
		iNewDamage = __max(1, iNewDamage);
	}

	return iNewDamage;
}

bool IsHaveSkillDamage(CSkillDef const* pkSkillDef)
{
	if(pkSkillDef)
	{
		bool bSkillDamage = false;
		bSkillDamage |= (0 < pkSkillDef->GetAbil(AT_PHY_ATTACK)) ? (true) : (false);
		bSkillDamage |= (0 < pkSkillDef->GetAbil(AT_PHY_DMG_PER)) ? (true) : (false);
		bSkillDamage |= (0 < pkSkillDef->GetAbil(AT_I_PHY_ATTACK_ADD_RATE)) ? (true) : (false);
		bSkillDamage |= (0 < pkSkillDef->GetAbil(AT_MAGIC_ATTACK)) ? (true) : (false);
		bSkillDamage |= (0 < pkSkillDef->GetAbil(AT_MAGIC_DMG_PER)) ? (true) : (false);
		bSkillDamage |= (0 < pkSkillDef->GetAbil(AT_I_MAGIC_ATTACK_ADD_RATE)) ? (true) : (false);

		return bSkillDamage;
	}

	return false;
}


// 도둑 기본 공격 2타 : a_thi_melee_02
bool CS_GetSkillResult103201201(int const iSkillNo, CUnit* pkCaster, UNIT_PTR_ARRAY& rkTargetArray, PgActionResultVector* pkResultVector)
{
	if(rkTargetArray.empty())
	{
		return true;
	}

	bool bReturn = CS_GetSkillResultDefault(iSkillNo, pkCaster, rkTargetArray, pkResultVector);
	// 도둑 2/4타 공격에 있어서 Double Attack이 될 가능성이 있다.
	int const iRate = pkCaster->GetAbil(AT_CLAW_DBL_ATTK_RATE);
	UNIT_PTR_ARRAY::const_iterator itor = rkTargetArray.begin();
	while (itor != rkTargetArray.end())
	{
		//참고용 일때
		if(true == (*itor).bReference)
		{
			//나중에 처리
			++itor;
			continue;
		}

		CUnit* pkTarget = itor->pkUnit;
		PgActionResult* pkAResult = pkResultVector->GetResult(pkTarget->GetID(), true);
		if( NULL == pkAResult )
		{
			++itor;
			VERIFY_INFO_LOG(false, BM::LOG_LV5, __FL__<<L"ActionResult is NULL Unit Guid["<<itor->pkUnit->GetID()<<L"]");
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkAResult is NULL"));
			continue;
		}

		if(pkAResult && !pkAResult->GetInvalid() && !pkAResult->GetMissed())
		{
			float fDamageRate = 1.0f;
			int iValue = int(pkCaster->GetRandom() % ABILITY_RATE_VALUE);
			
			if(iRate > iValue)
			{
				pkAResult->SetAbil(AT_HIT_COUNT, 2);
				int const iDmgRate = pkCaster->GetAbil(AT_CLAW_DBL_ATTK_DMG_RATE);
				fDamageRate = (iDmgRate * 2) / ABILITY_RATE_VALUE_FLOAT;

				//더블 어택이 동작할 경우에만 데미지를 계산해야한다.
				bReturn = CS_GetDmgResult(iSkillNo, pkCaster, pkTarget, (int)(ABILITY_RATE_VALUE_FLOAT * fDamageRate), 0, pkAResult);
			}			
		}
		++itor;
	}

	CheckActionResultVec(rkTargetArray, *pkResultVector);

	return bReturn;
}

//투사 룰렛
bool CS_GetSkillResult106000101(int const iSkillNo, CUnit* pkCaster, UNIT_PTR_ARRAY& rkTargetArray, PgActionResultVector* pkResultVector)
{
	if(rkTargetArray.empty())
	{
		return true;
	}

	//룰렛
	int iRand = pkCaster->GetRandom() % 12000;//	-- 1에서 6사이의 값 랜덤 얻기
	if(10000 < iRand)
	{
		iRand = 0;
	}
	else if(8000 < iRand)
	{
		iRand = 1;
	}
	else if(6000 < iRand)
	{
		iRand = 2;
	}
	else if(4000 < iRand)
	{
		iRand = 3;
	}
	else if(2000 < iRand)
	{
		iRand = 4;
	}
	else
	{
		iRand = 5;
	}
		
	UNIT_PTR_ARRAY::iterator unit_itor = rkTargetArray.begin();
	while(rkTargetArray.end() != unit_itor)
	{
		//참고용 일때
		if(true == (*unit_itor).bReference)
		{
			//나중에 처리
			++unit_itor;
			continue;
		}

		CUnit* pkTarget = (*unit_itor).pkUnit;
		PgActionResult* pkAResult = pkResultVector->GetResult(pkTarget->GetID(), true);
		pkAResult->SetAbil(AT_DICE_VALUE, iRand);
		
		++unit_itor;
	}

	CheckActionResultVec(rkTargetArray, *pkResultVector);

	return true;
}

// 전사 조인트 브레이크
bool CS_GetSkillResult105300501(int const iSkillNo, CUnit* pkCaster, UNIT_PTR_ARRAY& rkTargetArray, PgActionResultVector* pkResultVector)
{
	if(rkTargetArray.empty())
	{
		return true;
	}

	GET_DEF(CSkillDefMgr, kSkillDefMgr);
	CSkillDef const* pkSkillDef = kSkillDefMgr.GetDef(iSkillNo);
	if (pkSkillDef == NULL)
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L"Cannot Get SkillDef, SkillNo["<<iSkillNo<<L"]");
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	bool bSkillDamage = IsHaveSkillDamage(pkSkillDef);
	if(bSkillDamage)
	{
		return CS_GetSkillResultDefault(iSkillNo, pkCaster, rkTargetArray, pkResultVector);		
	}

	int const iHitRate = pkCaster->GetAbil(AT_C_HITRATE);

	UNIT_PTR_ARRAY::const_iterator itor = rkTargetArray.begin();
	while (itor != rkTargetArray.end())
	{
		//참고용 일때
		if(true == (*itor).bReference)
		{
			//나중에 처리
			++itor;
			continue;
		}

		CUnit* pkTarget = itor->pkUnit;
		if(pkTarget)
		{
			PgActionResult* pkResult = pkResultVector->GetResult(pkTarget->GetID(), true);
			if(!pkResult)
			{				
				++itor;
				VERIFY_INFO_LOG(false, BM::LOG_LV5, __FL__<<L"ActionResult is NULL Unit Guid["<<itor->pkUnit->GetID()<<L"]");
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkResult is NULL"));
				continue;
			}

			int iRandValue = pkCaster->GetRandom() % ABILITY_RATE_VALUE;
			int const iDecDodgeAbs = pkCaster->GetAbil(AT_DEC_TARGETDODGE_ABS);
			int const iDecDodgeRate = pkCaster->GetAbil(AT_DEC_TARGETDODGE_RATE);

			int iDecHitRate = CalcDecHitRate(pkCaster, pkTarget, iHitRate, iDecDodgeAbs, iDecDodgeRate);
			if (iDecHitRate < iRandValue)
			{
				// 타격 실패 (Missed)
				pkResult->SetMissed(true);
				++itor;
				continue;
			}

			AddDamageEffect(pkCaster, pkTarget, pkSkillDef, pkResult);
		}
		++itor;
	}

	CheckActionResultVec(rkTargetArray, *pkResultVector);

	return true;
}

//트랩퍼 MP제로 폭발
bool CS_GetSkillResult1100028011(int const iSkillNo, CUnit* pkCaster, UNIT_PTR_ARRAY& rkTargetArray, PgActionResultVector* pkResultVector)
{
	if(rkTargetArray.empty())
	{
		return true;
	}

	GET_DEF(CSkillDefMgr, kSkillDefMgr);
	CSkillDef const* pkSkillDef = kSkillDefMgr.GetDef(iSkillNo);
	if (pkSkillDef == NULL)
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L"Cannot Get SkillDef, SkillNo["<<iSkillNo<<L"]");
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	UNIT_PTR_ARRAY::const_iterator itor = rkTargetArray.begin();
	while (itor != rkTargetArray.end())
	{
		CUnit* pkTarget = itor->pkUnit;
		if(pkTarget)
		{
			PgActionResult* pkResult = pkResultVector->GetResult(pkTarget->GetID(), true);
			if(!pkResult)
			{
				++itor;
				VERIFY_INFO_LOG(false, BM::LOG_LV5, __FL__<<L"ActionResult is NULL Unit Guid["<<itor->pkUnit->GetID()<<L"]");
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkResult is NULL"));
				continue;
			}

			int const iMP = pkTarget->GetAbil(AT_C_MAX_MP);
			int iValue = (int)((float)iMP * (float)pkSkillDef->GetAbil(AT_PERCENTAGE) / ABILITY_RATE_VALUE);
			iValue = __max(0, iValue);
			pkResult->SetValue(iValue / 2);			
		}
		++itor;
	}

	CheckActionResultVec(rkTargetArray, *pkResultVector);

	return true;
}

namespace PgNpcTalkUtil
{
	float const fNPC_TALK_ENABLE_RANGE = 200.f;
}

bool GetItemName(int const iItemNo, std::wstring & rkOut)
{
	CONT_DEFITEM const* pkDefItem = NULL;
	g_kTblDataMgr.GetContDef(pkDefItem);
	if( !pkDefItem )
	{
		return false;
	}

	CONT_DEFITEM::const_iterator def_item = pkDefItem->find( iItemNo );
	if( pkDefItem->end() == def_item )
	{
		return false;
	}

	CONT_DEFSTRINGS const* pContDefStrings = NULL;
	g_kTblDataMgr.GetContDef(pContDefStrings);

	CONT_DEFSTRINGS::const_iterator str_itor = pContDefStrings->find( (def_item->second).NameNo );
	if(str_itor == pContDefStrings->end())
	{
		return false;
	}

	rkOut = (*str_itor).second.strText;
	return true;
}

namespace PgHometownPortalUtil
{
	__int64 UsePortalCost(int const iLevel)
	{
		CONT_HOMETOWNTOMAPCOST const * pkCont = NULL;
		g_kTblDataMgr.GetContDef(pkCont);
		if(NULL == pkCont)
		{
			return 0;
		}

		CONT_HOMETOWNTOMAPCOST::const_iterator iter = pkCont->find(iLevel);
		if(iter == pkCont->end())
		{
			return 0;
		}

		return static_cast<__int64>((*iter).second); // 고정 금액으로 수정
	}
}

namespace PgMyHomeFuncRate
{
	int GetHomeFuncItemValue(eMyHomeSideJob const kSideJob, eMyHomeSideJobRateType const kJobRateType, CUnit * pkUnit)
	{
		PgInventory * pkInv = pkUnit->GetInven();
		ContHaveItemNoCount kContHaveItem;
		GET_DEF(CItemDefMgr, kItemDefMgr);

		pkInv->GetItems(IT_HOME, UICT_HOME_SIDEJOB_NPC, kContHaveItem);

		for(ContHaveItemNoCount::const_iterator iter = kContHaveItem.begin();iter != kContHaveItem.end();++iter)
		{
			CItemDef const * pkItem = kItemDefMgr.GetDef((*iter).first);
			if(!pkItem || (pkItem->GetAbil(AT_USE_ITEM_CUSTOM_VALUE_1) != kSideJob) || (pkItem->GetAbil(AT_USE_ITEM_CUSTOM_VALUE_2) != kJobRateType))
			{
				continue;
			}
			return pkItem->GetAbil(AT_USE_ITEM_CUSTOM_VALUE_3);
		}

		return 0;
	}

	bool GetSideJobInfo(eMyHomeSideJob const kSideJob, TBL_DEFSIDEJOBRATE & kJobInfo)
	{
		CONT_DEFSIDEJOBRATE const *pkContDef = NULL;
		g_kTblDataMgr.GetContDef(pkContDef);

		if(!pkContDef)
		{
			return false;
		}

		CONT_DEFSIDEJOBRATE::const_iterator iter = pkContDef->find(kSideJob);
		if(iter == pkContDef->end())
		{
			return false;
		}

		kJobInfo = (*iter).second;

		return true;
	}

	double GetCostRate(eMyHomeSideJob const kSideJob, CUnit * pkUnit)
	{
		double dValue = 1.0;

		if(!pkUnit)
		{
			return dValue;
		}

		int iFuncValue = GetHomeFuncItemValue(kSideJob, MSJRT_GOLD, pkUnit);

		if(0 >= iFuncValue)
		{
			TBL_DEFSIDEJOBRATE kJobInfo;
			if(false == GetSideJobInfo(kSideJob, kJobInfo))
			{
				return dValue;
			}
			iFuncValue = kJobInfo.iCostRate;
		}

		if(0 >= iFuncValue)
		{
			return dValue;
		}

		dValue = 1.0 - (static_cast<double>(iFuncValue)/static_cast<double>(ABILITY_RATE_VALUE)) * pkUnit->GetInven()->GetHomeEquipItemCount();

		dValue = std::max(0.0,dValue); // 혹시라도 100% 이상 넘으면 - 값이 되는것은 막아주자

		return dValue;
	}

	double GetSoulRate(eMyHomeSideJob const kSideJob, CUnit * pkUnit)
	{
		double dValue = 1.0;

		if(!pkUnit)
		{
			return dValue;
		}

		int iFuncValue = GetHomeFuncItemValue(kSideJob, MSJRT_SOUL, pkUnit);

		if(0 >= iFuncValue)
		{
			TBL_DEFSIDEJOBRATE kJobInfo;
			if(false == GetSideJobInfo(kSideJob, kJobInfo))
			{
				return dValue;
			}
			iFuncValue = kJobInfo.iCostRate;
		}

		if(0 >= iFuncValue)
		{
			return dValue;
		}

		dValue = 1.0 - (static_cast<double>(iFuncValue)/static_cast<double>(ABILITY_RATE_VALUE)) * pkUnit->GetInven()->GetHomeEquipItemCount();

		dValue = std::max(0.0,dValue); // 혹시라도 100% 이상 넘으면 - 값이 되는것은 막아주자

		return dValue;
	}

	int GetSuccessRate(eMyHomeSideJob const kSideJob, CUnit * pkUnit)
	{
		if(!pkUnit)
		{
			return 0;
		}

		int iFuncValue = GetHomeFuncItemValue(kSideJob, MSJRT_RATE, pkUnit);

		if(0 >= iFuncValue)
		{
			TBL_DEFSIDEJOBRATE kJobInfo;
			if(false == GetSideJobInfo(kSideJob, kJobInfo))
			{
				return 0;
			}
			iFuncValue = kJobInfo.iSuccessRate;
		}

		return iFuncValue * static_cast<int>(pkUnit->GetInven()->GetHomeEquipItemCount());
	}
}

bool CheckExistAdjustSkillNoInEffect(int const iSkillNo, CEffect* pkEffect)
{// IsApplySkill에서 사용하는 함수(해당 스킬 번호가 이펙트에 포함되는지 확인한다)
	int iTargetSkillNo=0;
	int i = AT_ADJUST_SKILL_BEGIN;
	while(i <= AT_ADJUST_SKILL_END)
	{
		iTargetSkillNo = pkEffect->GetAbil(i);
		if(iTargetSkillNo) 
		{// 현재 값이 있으면
			if(iSkillNo == iTargetSkillNo)
			{
				return true;	// 존재함
			}
			++i;
		}
		else
		{//현재 값이 없으면 10단위로 체크 한다.(1이 없으면 11, 21, 31 순으로 검색하게 함)
			i = ((i/10)+1)*10+1;
		}
	}
	return false;	// 존재 하지 않음
}
namespace PgSkillHelpFunc
{
	bool IsApplySkill(int const iSkillNo, CEffect* pkEffect)
	{// CalcAdjustSkillValue에서 해당 스킬을 조정하는 이펙트가 걸려있는지 확인하기 위한 함수
		if(!pkEffect)
		{
			return false;
		}

		GET_DEF(CSkillDefMgr, kSkillDefMgr);
		CSkillDef const *pkSkillDef = kSkillDefMgr.GetDef(iSkillNo);
		if(!pkSkillDef)
		{
			return false;
		}

		int iParentCastSkillNo = 0;
		int const iParentSkill = pkSkillDef->GetParentSkill() ? pkSkillDef->GetParentSkill() : iSkillNo;
		{
			CSkillDef const *pkParentSkillDef = kSkillDefMgr.GetDef(iParentSkill);
			if(pkParentSkillDef)
			{// 이 스킬이 SkillActor등으로 사용되는 스킬이라면, 플레이어가 캐스팅 하는 스킬의 레벨로 얻어온다
				iParentCastSkillNo = pkParentSkillDef->GetAbil(AT_PARENT_CAST_SKILL_NO);
			}
		}
		// 이 스킬을 발동 시키기 위한 스킬이 있었다면 그 스킬로 DB에 입력되기 때문에 그 스킬의 부모 스킬로 비교해야 한다.
		int const iApplySkillNo = iParentCastSkillNo ? iParentCastSkillNo : iParentSkill;

		int const iCheckType = pkEffect->GetAbil(AT_ADJUST_SKILL_CHECK_TYPE);
		if(0 == iCheckType)
		{// 지금 사용한 스킬이 적용되는지 확인하고
			return false;
		}
		// 스킬 체크 타입, EAdjustSkillCheckType 1:모든 스킬, 2:일부 스킬만, 3:일부 스킬 제외하고 모두
		switch(iCheckType)
		{// 해당 스킬 번호
		case EASC_ALL:
			{// 모두가 대상
				return true;
			}break;
		case EASC_CHECK_SOME_SECTION:
			{// 존재하면 이 스킬은 영향을 받음
				if(CheckExistAdjustSkillNoInEffect(iApplySkillNo, pkEffect))
				{
					return true;
				}
				return false;
			}break;
		case EASC_CHECK_EXCLUSIVE_SECTION:
			{// 존재하지 않아야 스킬은 영향을 받음
				if(!CheckExistAdjustSkillNoInEffect(iApplySkillNo, pkEffect))
				{
					return true;
				}
				return false;
			}break;
		default:
			{
				return false;
			}break;
		}

		return false;
	}

	int GetAddtionalAdjustSkillValue(CUnit* pkUnit, int const iAbilType, CEffect* pkEffect, int iValue)
	{// CalcAdjustSkillValue에서 사용하기 위한 함수
		int iResult = 0;
		switch(iAbilType)
		{
		case AT_E_ADJUST_SKILL_RANGE:
		case AT_E_ADJUST_SKILL_RANGE2:
		case AT_E_ADJUST_SKILL_DMG:
		case AT_E_ADJUST_SKILL_RESULT_MIN_DMG_BY_PHASE:
		case AT_E_ADJUST_SKILL_RESULT_MAX_DMG_BY_PHASE:
		case AT_E_ADJUST_SKILL_INC_PHASE_DMG:
			//case AT_ADJUST_SKILL_E_COOLTIME:
			{// 더해야 되는값
				iResult = pkEffect->GetAbil(iAbilType);
			}break;
		case AT_R_ADJUST_SKILL_RANGE:
		case AT_R_ADJUST_SKILL_RANGE2:
		case AT_R_ADJUST_SKILL_DMG:
		case AT_R_ADJUST_SKILL_RESULT_MIN_DMG_BY_PHASE:
		case AT_R_ADJUST_SKILL_RESULT_MAX_DMG_BY_PHASE:
		case AT_R_ADJUST_SKILL_INC_PHASE_DMG:
			//case AT_ADJUST_SKILL_R_COOLTIME:
			{// 곱해야 되는값
				iResult = iValue * pkEffect->GetAbil(iAbilType)/ABILITY_RATE_VALUE;
			}break;
		default:
			{
				return iResult;
			}break;
		}

		return iResult;
	}

	int CalcAdjustSkillValue(EAdjustSkillCalcType const eCalcType, int const iSkillNo, CUnit* pkUnit, int const iOriginValue)
	{// 곱하기와 더하기 계산이 같이 이루어지기 때문에, 한 계통에서 이함수가 여러번 호출할 경우가 생긴다면 다시 생각해봐야함
		if(!pkUnit)
		{// 대상을 얻어와서 
			return iOriginValue;
		}

		PgUnitEffectMgr& rkEffectMgr = pkUnit->GetEffectMgr();
		if(0 == rkEffectMgr.Size())
		{// 걸려있는 이펙트(버프)가 있다면
			return iOriginValue;
		}

		int iResultValue = iOriginValue;

		ContEffectItor kItor;	
		rkEffectMgr.GetFirstEffect(kItor);
		CEffect* pkEffect = NULL;

		typedef std::vector<int>  CONT_INT;
		CONT_INT kCont;

		switch(eCalcType)
		{// 계산을 위해 계산하기 위해 체크해야할 어빌들을 넣어놓고
			//	(_R_ 먼저 넣어 곱하고 난후, _E_의 더하기 계산이 되게 해야한다)
		case EASCT_RANGE:
			{// 범위
				kCont.push_back(AT_R_ADJUST_SKILL_RANGE);
				kCont.push_back(AT_E_ADJUST_SKILL_RANGE);
			}break;
		case EASCT_RANGE2:
			{// 범위2
				kCont.push_back(AT_R_ADJUST_SKILL_RANGE2);
				kCont.push_back(AT_E_ADJUST_SKILL_RANGE2);
			}break;
		case EASCT_DMG:
			{// 데미지
				kCont.push_back(AT_R_ADJUST_SKILL_DMG);
				kCont.push_back(AT_E_ADJUST_SKILL_DMG);
			}break;
			//case EASCT_ADJUST_SKILLCOOLTIME:
			//	{// 쿨타임
			//		kCont.push_back(AT_ADJUST_SKILL_R_COOLTIME);
			//		kCont.push_back(AT_ADJUST_SKILL_E_COOLTIME);
			//	}break;
			/// 이하는 단계별 스킬 데미지 조절 값 계산 부분
		case EASCT_RESULT_MIN_DMG_BY_PHASE:
			{// 최소값
				kCont.push_back(AT_R_ADJUST_SKILL_RESULT_MIN_DMG_BY_PHASE);
				kCont.push_back(AT_E_ADJUST_SKILL_RESULT_MIN_DMG_BY_PHASE);
			}break;
		case EASCT_RESULT_MAX_DMG_BY_PHASE:
			{// 최대값
				kCont.push_back(AT_R_ADJUST_SKILL_RESULT_MAX_DMG_BY_PHASE);
				kCont.push_back(AT_E_ADJUST_SKILL_RESULT_MAX_DMG_BY_PHASE);
			}break;
		case EASCT_INC_PHASE_DMG:
			{// 증가값
				kCont.push_back(AT_R_ADJUST_SKILL_INC_PHASE_DMG);
				kCont.push_back(AT_E_ADJUST_SKILL_INC_PHASE_DMG);
			}break;
		default:
			{
				return iOriginValue;
			}break;
		}


		std::set<CEffect*> ContEff;//이펙트 번호.
		int iAccCalcedValue = 0;
		while ((pkEffect = rkEffectMgr.GetNextEffect(kItor)) != NULL)
		{// Unit에 걸려있는 이펙트중에서
			if(pkEffect)
			{
				if(IsApplySkill(iSkillNo, pkEffect))
				{// 적용 되는 스킬이고
					if(EASCT_INC_PHASE_DMG == eCalcType)
					{// 점진적 데미지 감소 이펙트 이면 따로 저장해두고
						ContEff.insert(pkEffect);
					}

					CONT_INT::iterator itor = kCont.begin();
					while(kCont.end() != itor)
					{// kCont안의 어빌들로 값을 계산을 하여
						int const iCalcedVal = GetAddtionalAdjustSkillValue(pkUnit, (*itor), pkEffect, iResultValue);
						iAccCalcedValue+=iCalcedVal; // 최종적으로 적용할 값을 구한후
						++itor;
					}
				}
			}
		}
		
		
		{/// eCalcType 타입 별로, 최종 적용값인 iAccCalcedValue를 사용한다
			if(EASCT_INC_PHASE_DMG == eCalcType)
			{// 점진적 데미지 가감값 계산이라면
				int iReduceDamage = 0;
				std::set<CEffect*>::iterator kEffItor = ContEff.begin();
				while(ContEff.end() != kEffItor)
				{// 점진적 감소이펙트들에 감소된 스킬 데미지 값을 저장하고
					pkEffect = *kEffItor;
					if(pkEffect)
					{
						SActArg* pkEffectActArg = pkEffect->GetActArg();
						if(pkEffectActArg)
						{
							DWORD const dwTime = BM::GetTime32();
							{// 때린 시간 갱신
								pkEffectActArg->Set(ACT_ARG_DMG_BY_PASE_BEGIN_TIME, dwTime);
								pkEffectActArg->Set(ACT_ARG_CUSTOMDATA1, true);
							}
							{// 데미지 감소량 증가
								//BM::vstring vStr; vStr+="계산전 원데미지 :"; vStr+=iOriginValue; vStr+="\n"; 
								pkEffectActArg->Get(ACT_ARG_DMG_BY_PASE_ACC_DMG, iReduceDamage);
								//if(0 == iReduceDamage) { vStr+="처음임\n"; }
								//vStr+="저장된 가감 값 :";vStr+=iReduceDamage; vStr+="\n"; vStr+="1차 계산된 가감값 :";	vStr+=iAccCalcedValue;	vStr+="\n";
								iAccCalcedValue+=iReduceDamage;
								//vStr+="합계 :"; vStr+=iReduceDamage;vStr+="\n";

								if(iAccCalcedValue < -iOriginValue)
								{// 언더플로 방지 (적용되는 최소 값이 되므로)
									iAccCalcedValue = -iOriginValue;
								}
								{// 오버플로는?
								}
								pkEffectActArg->Set(ACT_ARG_DMG_BY_PASE_ACC_DMG, iAccCalcedValue);
								//vStr+="\n";	OutputDebugStringA(static_cast<std::string>(vStr).c_str());
							}
						}
					}
					++kEffItor;
				}
				//이펙트에 저장된 데미지만 적용해준다.(따라서 두번째 타격 부터 데미지 감소가 적용됨)
				iResultValue+=iReduceDamage;
			}
			else
			{// 누적된 계산값을 합해주고
				iResultValue+=iAccCalcedValue;
			}
		}
		
		if(0 > iResultValue)
		{/// 최소값을 벗어났을 경우
			switch(eCalcType)
			{// 타입에 맞는 최소값들을 설정한다
			case EASCT_DMG:	// 데미지 계열이라면 최소 값은 1
			case EASCT_INC_PHASE_DMG:
				{
					iResultValue= 1;
				}break;
			default:
				{
					iResultValue= 0;
				}break;
			}
		}
		return iResultValue;
	}

	int CalcReflectDamage(CUnit* pkCaster, CUnit* pkTarget,int const iDamage, PgActionResult* pkActionResult, CSkillDef const* pkSkillDef, bool const bPhysicDmg, bool const bOffRandomSeed, bool const bCompelCalc)
	{// 반사 데미지를 계산한다. (이펙트에서 주는 데미지에서도 반사 데미지를 계산 하게 됨에 따라 함수로 분리시킴)
		int iResultReflectDamage = 0;
		if(bCompelCalc 
			|| (pkSkillDef && 0 == pkSkillDef->GetAbil(AT_IGNORE_REFLECTED_DAMAGE_SKILL))
			)
		{// 강제로 계산이거나, 반사 스킬 무시 어빌이 없다면
			bool const bCurSeed = pkCaster->UseRandomSeedType();
			if(bCurSeed && bOffRandomSeed)
			{// 기존의 설정이 시드를 사용하게 되어있으면 시드를 끄고
				pkCaster->UseRandomSeedType(false);
			}

			//물리 / 마법 100% 반사 확률이 적용이 될 경우
			int const i100PerReflectedRate = (bPhysicDmg ? (pkTarget->GetAbil(AT_PHY_DMG_100PERECNT_REFLECT_RATE)) : (pkTarget->GetAbil(AT_MAGIC_DMG_100PERECNT_REFLECT_RATE)));
			int	const iRandValue = pkCaster->GetRandom() % ABILITY_RATE_VALUE;

			// 물리 / 마법 데미지 반사할 확률
			int const iDmgReflectRate = (bPhysicDmg ? (pkTarget->GetAbil(AT_C_PHY_REFLECT_RATE)) : (pkTarget->GetAbil(AT_C_MAGIC_REFLECT_RATE)));
			int const iRandValue2 = pkCaster->GetRandom() % ABILITY_RATE_VALUE;

			// 발사체 반사할 확률
			int const iProjectileReflectRate = pkTarget->GetAbil(AT_C_PROJECTILE_REFLECT_RATE);
			int const iRandValue3 = pkCaster->GetRandom() % ABILITY_RATE_VALUE;
			
			if(iRandValue < i100PerReflectedRate)
			{
				iResultReflectDamage = CS_CheckDmgPer(pkTarget, iDamage, bPhysicDmg);
				if(pkActionResult && pkSkillDef)
				{
					pkActionResult->AddAbil(AT_REFLECTED_DAMAGE, iResultReflectDamage);
					//처음이면 세팅, 이미 값이 세팅되어있으면 누적되어야 한다.
					int const iDmgHp = (pkActionResult->GetAbil(AT_REFLECT_DMG_HP) == 0) ? (CS_DoReflectDamage(pkCaster, pkTarget, pkSkillDef->No(), iResultReflectDamage)) : (__max(0, pkActionResult->GetAbil(AT_REFLECT_DMG_HP) - iResultReflectDamage));
					pkActionResult->SetAbil(AT_REFLECT_DMG_HP, iDmgHp);
				}
			}
			else if(iRandValue2 < iDmgReflectRate)
			{//물리 / 마법 반사할 확률
				// 반사할 데미지 량
				int const iDmgReflectDmg = (bPhysicDmg ? (pkTarget->GetAbil(AT_C_PHY_REFLECT_DMG_RATE)) : (pkTarget->GetAbil(AT_C_MAGIC_REFLECT_DMG_RATE)));

				iResultReflectDamage = static_cast<int>(iDamage * static_cast<float>(iDmgReflectDmg) / ABILITY_RATE_VALUE_FLOAT);

				iResultReflectDamage = CS_CheckDmgPer(pkTarget, iResultReflectDamage, bPhysicDmg);
				if(pkActionResult && pkSkillDef)
				{
					pkActionResult->AddAbil(AT_REFLECTED_DAMAGE, iResultReflectDamage);
					//처음이면 세팅, 이미 값이 세팅되어있으면 누적되어야 한다.
					int const iDmgHp = (pkActionResult->GetAbil(AT_REFLECT_DMG_HP) == 0) ? (CS_DoReflectDamage(pkCaster, pkTarget, pkSkillDef->No(), iResultReflectDamage)) : (__max(0, pkActionResult->GetAbil(AT_REFLECT_DMG_HP) - iResultReflectDamage));
					pkActionResult->SetAbil(AT_REFLECT_DMG_HP, iDmgHp);
				}
			}
			else if(pkSkillDef
				&& pkSkillDef->IsSkillAtt(SAT_CLIENT_CTRL_PROJECTILE)
				)
			{// 발사체 반사할 확률
				if(iRandValue3 < iProjectileReflectRate )
				{// 스킬로 인한 발사체 반사 확률
					int const iProjectileReflectDmg = pkTarget->GetAbil(AT_C_PROJECTILE_REFLECT_DMG_RATE);
					iResultReflectDamage = static_cast<int>(iDamage * static_cast<float>(iProjectileReflectDmg) / ABILITY_RATE_VALUE_FLOAT);
				}
				
				{// 아이템으로 인한 발사체의 반사확률(근접 공격도 같은 어빌로 CS_GetReflectDamage에서 계산함)
					iResultReflectDamage += CS_GetReflectDamage_FromItem(pkCaster, pkTarget, iDamage, bPhysicDmg);
				}

				iResultReflectDamage = CS_CheckDmgPer(pkTarget, iResultReflectDamage, bPhysicDmg);
				if(0 < iResultReflectDamage
					&& pkActionResult
					&& pkSkillDef
					)
				{
					pkActionResult->AddAbil(AT_REFLECTED_DAMAGE, iResultReflectDamage);
					//처음이면 세팅, 이미 값이 세팅되어있으면 누적되어야 한다.
					int const iDmgHp = (pkActionResult->GetAbil(AT_REFLECT_DMG_HP) == 0) ? (CS_DoReflectDamage(pkCaster, pkTarget, pkSkillDef->No(), iResultReflectDamage)) : (__max(0, pkActionResult->GetAbil(AT_REFLECT_DMG_HP) - iResultReflectDamage));
					pkActionResult->SetAbil(AT_REFLECT_DMG_HP, iDmgHp);
				}
			}
			else
			{//기본 반사가 적용될 경우
				iResultReflectDamage = CS_GetReflectDamage(pkCaster, pkTarget, iDamage, bPhysicDmg);
				if(pkActionResult && pkSkillDef)
				{
					if(!pkSkillDef->IsSkillAtt(SAT_CLIENT_CTRL_PROJECTILE)		//발사체 스킬이 아니고
						&& 0 < iResultReflectDamage								//반사 데미지가 있고
						)
					{
						pkActionResult->AddAbil(AT_REFLECTED_DAMAGE, iResultReflectDamage);

						//	Damage Reflect 값이 있다면 여기서 Damage를 주자.
						//	local iHP = DoReflectDamage(caster, target, iReflect, iSkill, actarg)
						//	aresult:SetAbil(AT_REFLECT_DMG_HP, iHP)
						//	InfoLog(9, "AT_REFLECTED_DAMAGE Reflected Damage:" .. iReflect .. ", iHP:" .. aresult:GetAbil(AT_REFLECT_DMG_HP))
						//end
						//처음이면 세팅, 이미 값이 세팅되어있으면 누적되어야 한다.
						int const iDmgHp = (pkActionResult->GetAbil(AT_REFLECT_DMG_HP) == 0) ? (CS_DoReflectDamage(pkCaster, pkTarget, pkSkillDef->No(), iResultReflectDamage)) : (__max(0, pkActionResult->GetAbil(AT_REFLECT_DMG_HP) - iResultReflectDamage));
						pkActionResult->SetAbil(AT_REFLECT_DMG_HP, iDmgHp);
					}
				}
			}
		
			if(bCurSeed && bOffRandomSeed)
			{// 기존의 설정이 시드를 사용하게 되어있었다면 끄고 사용했으니 다시 켜준다
				pkCaster->UseRandomSeedType(true);
			}
		}

		return iResultReflectDamage;
	}

	int GetAttackRange(CUnit * pkUnit, CSkillDef const* pkSkillDef)
	{
		if(!pkUnit || !pkSkillDef)
		{
			return 0;
		}

		EAttackRangeSource const eRangeSource = static_cast<EAttackRangeSource>(pkSkillDef->GetAbil(AT_RANGE_TYPE));
		int iSkillRange = 0;
		switch(eRangeSource)
		{
		case EAttackRange_ItemRange:
		case EAttackRange_Skill_Item:
			{
				if(eRangeSource == EAttackRange_Skill_Item)
				{
					iSkillRange = pkSkillDef->GetAbil(AT_ATTACK_RANGE);
				}

				PgBase_Item kSItem;
				if( pkUnit->GetInven()
				&& (S_OK == pkUnit->GetInven()->GetItem(IT_FIT, EQUIP_POS_WEAPON, kSItem)) )
				{
					GET_DEF(CItemDefMgr, kItemDefMgr);
					CItemDef const *pDef = kItemDefMgr.GetDef(kSItem.ItemNo());
					if(pDef)
					{
						iSkillRange += pDef->GetAbil(AT_ATTACK_RANGE);
					}
				}
			}break;
		case EAttackRange_UnitRange:
			{
				iSkillRange = pkUnit->GetAbil(AT_ATTACK_RANGE);
			}break;
		case EAttackRange_SkillRange:
		case EAttackRange_SkillRange2:
		default:
			{
				iSkillRange = pkSkillDef->GetAbil(AT_ATTACK_RANGE);
			}break;
		}
		if(iSkillRange <= 0)
		{
			INFO_LOG(BM::LOG_LV0, __FL__<<L"AttackRange is <0 SkillNo["<<pkSkillDef->No()<<L"], RangeType["<<eRangeSource<<L"], Range["<<iSkillRange<<L"]");
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return 0;
		}

		return CalcAttackRange(pkUnit, iSkillRange);
	}

	int CalcAttackRange(CUnit * pkUnit, int const iSkillRange)
	{
		if(pkUnit)
		{
			int iRate = pkUnit->GetAbil(AT_ATTACK_RANGE_RATE);
			if(pkUnit->GetInven())
			{
				//사정거리 증가 옵션이 있을 경우에만 계산
				iRate += pkUnit->GetInven()->GetAbil(AT_R_ATTACK_RANGE);
			}

			if(0 != iRate)
			{
				//기본값이 0 이므로 일단 100%로 맞춘다음 거기에 추가 해야 한다.
				iRate += ABILITY_RATE_VALUE;			
				return static_cast<int>(iSkillRange * (static_cast<float>(iRate) / ABILITY_RATE_VALUE_FLOAT));
			}
		}
		return iSkillRange;
	}
}

//
namespace PgXmlLocalUtil
{
	char const* LOCAL_ELEMENT_NAME = "LOCAL";
	bool IsContainAbleServiceName(LOCAL_MGR::CLocal& rkLocalMgr, std::wstring const & rServiceName, char const* szContents = "NULL")
	{
		typedef std::list< std::wstring > Contstr;
		Contstr kList;
		BM::vstring::CutTextByKey(rServiceName, std::wstring(_T("/")), kList);
#ifdef _MD_
		if( 1 < kList.size()
			&&	kList.end() != std::find( kList.begin(), kList.end(), std::wstring(L"DEFAULT")) )
		{
			std::wstring const& kTempStr = std::wstring(L"'DEFAULT' is can't same use, other local tag in ") + UNI(szContents);
			::MessageBox(NULL, kTempStr.c_str(), L"Error", MB_OK);
		}
#endif

		Contstr::const_iterator it = kList.begin();
		while(kList.end()!=it)
		{
			if(true==rkLocalMgr.IsAbleServiceName((*it).c_str()))
			{
				return true;
			}
			++it;
		}

		return false;
	}
	TiXmlNode const* FindInLocal(LOCAL_MGR::CLocal& rkLocalMgr, TiXmlNode const* pkLocalNode, char const* szContents)
	{
		if( !pkLocalNode )
		{
			return NULL;
		}

		TiXmlNode const* pkFindLocalNode = NULL;
		TiXmlNode const* pkDefaultLocalNode = NULL;
		TiXmlNode const* pkSubChild = pkLocalNode->FirstChild();
		while( pkSubChild )
		{
			TiXmlElement const* pkSubElement = dynamic_cast<TiXmlElement const*>(pkSubChild);
			if( pkSubElement )
			{
				TiXmlAttribute const* pkAttr = pkSubElement->FirstAttribute();
				if( pkAttr )
				{
					char const *pcAttrValue = pkAttr->Value();
					if( pcAttrValue )
					{
						std::string const kCurLocal(pcAttrValue);
						std::wstring const wstrLocalName(BM::vstring::ConvToUnicode(kCurLocal));

						if(true==IsContainAbleServiceName(rkLocalMgr, wstrLocalName, szContents))
						{
#ifdef _MD_
							if(pkFindLocalNode)
							{
								std::wstring const& kTempStr = std::wstring(wstrLocalName) + L"  local is duplicated. " + UNI(szContents);
								::MessageBox(NULL, kTempStr.c_str(), L"Error", MB_OK);
							}
#endif
							pkFindLocalNode = pkSubChild;
						}
						else if("DEFAULT" == kCurLocal)
						{
							pkDefaultLocalNode = pkSubChild;
						}
					}
				}
			}
			pkSubChild = pkSubChild->NextSibling();
		}

		if( pkFindLocalNode )
		{
			return pkFindLocalNode;
		}
		else
		{
			if( NULL == pkFindLocalNode
			&&	NULL != pkDefaultLocalNode )
			{
				return pkDefaultLocalNode;
			}
		}
		return NULL;
	}

	TiXmlElement const* FindInLocalResult(LOCAL_MGR::CLocal& rkLocalMgr, TiXmlElement const* pkLocalNode)
	{
		TiXmlNode const* pkFindLocalNode = FindInLocal(rkLocalMgr, pkLocalNode);
		if( pkFindLocalNode )
		{
			TiXmlElement const* pkResultNode = pkFindLocalNode->FirstChildElement();
			if( pkResultNode )
			{
				return pkResultNode;
			}
		}
		return NULL;
	}
}

bool IsClass_OwnSubPlayer(__int64 const i64Class)
{
	if(52 == i64Class)
	{// ClassLimit에 대한 함수가 잘 만들어질때까지는 이렇게 사용
		return true;
	}
	return false;
}