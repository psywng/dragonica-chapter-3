#include "stdafx.h"
#include "PgPlayer.h"
#include "PgQuestInfo.h"
#include "PgPortalAccess.h"

//============================================================================
// PgMapMoveChecker
//----------------------------------------------------------------------------
PgMapMoveChecker::PgMapMoveChecker( CONT_DEFMAP const &kContDetMap )
:	m_kContDefMap(kContDetMap)
{
}

EPartyMoveType PgMapMoveChecker::GetMoveType( int const iGroundNo )const
{
	CONT_DEFMAP::const_iterator defmap_itr = m_kContDefMap.find( iGroundNo );
	if ( defmap_itr != m_kContDefMap.end() )
	{
		if ( GATTR_INSTANCE & (defmap_itr->second.iAttr)  )
		{
			return E_MOVE_PARTYMASTER;
		}
		else if( GATTR_STATIC_DUNGEON & (defmap_itr->second.iAttr)  )
		{
			return E_MOVE_ANY_PARTYMEMMBER;
		}
		return E_MOVE_PERSONAL;
	}

	return E_MOVE_LOCK;
}

//============================================================================
// PgPortalAccess
//----------------------------------------------------------------------------
PgPortalAccess::PgPortalAccess(void)
:	m_nTargetPortal(0)
,	m_byMoveType(E_MOVE_LOCK)
,	m_iName(0)
,	m_iErrorMessage(0)
,	m_iLevelLimit_Min(0)
,	m_iLevelLimit_Max(INT_MAX)
,	m_bLevelParty(false)
{
}

PgPortalAccess::~PgPortalAccess(void)
{
}

bool PgPortalAccess::Build( TiXmlElement const *pkElement, CONT_DEFMAP const &kContDerMap )
{
	if ( !pkElement )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	bool bMoveTypeParsed = false;

	char const *pcAttrName = NULL;
	char const *pcAttrValue = NULL;
	TiXmlAttribute const *pkAttribute = pkElement->FirstAttribute();
	while ( pkAttribute )
	{
		pcAttrName = pkAttribute->Name();
		pcAttrValue = pkAttribute->Value();

		if( !::strcmp(pcAttrName, "PARAM") )
		{
			m_kTargetGroundKey.GroundNo( ::atoi( pcAttrValue ) );
		}
		else if ( !::strcmp(pcAttrName, "PARAM2" ) )
		{
			m_nTargetPortal = static_cast<short>( ::atoi( pcAttrValue ) );
		}
		else if ( !::strcmp(pcAttrName, "MOVETYPE") )
		{
			bMoveTypeParsed = true;
			m_byMoveType = static_cast<BYTE>( ::atoi( pcAttrValue ) );
		}
		else if ( !::strcmp(pcAttrName, "NAME") )
		{
			m_iName = ::atoi( pcAttrValue );
		}

		pkAttribute = pkAttribute->Next();
	}

	if ( !m_kTargetGroundKey.IsEmpty() )
	{
		CONT_DEFMAP::const_iterator map_itr = kContDerMap.find( m_kTargetGroundKey.GroundNo() );
		if ( map_itr != kContDerMap.end() )
		{
			T_GNDATTR kAttr = (T_GNDATTR)(map_itr->second.iAttr);
			if (	(kAttr & GATTR_FLAG_PUBLIC_CHANNEL)
				||	(kAttr & GATTR_FLAG_MISSION) 
				)
			{
				VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T(" TargetGoundNo=") << m_kTargetGroundKey.GroundNo() << _T(" GroundAttribute=") << kAttr );
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
				return false;
			}

			if ( !bMoveTypeParsed )
			{
				if ( kAttr & GATTR_INSTANCE )
				{
					m_byMoveType = E_MOVE_PERSONAL|E_MOVE_PARTYMASTER;
				}
				else if( kAttr & GATTR_STATIC_DUNGEON )
				{
					m_byMoveType = E_MOVE_PERSONAL|E_MOVE_ANY_PARTYMEMMBER;
				}
				else
				{
					m_byMoveType = E_MOVE_PERSONAL;
				}		
			}

			bool bChildOfChild = false;
			TiXmlElement const * pkChildElement = pkElement->FirstChildElement();
			while ( pkChildElement )
			{
				if ( !::strcmp( pkChildElement->Value(), "ACCESS") )
				{
					pkAttribute = pkChildElement->FirstAttribute();
					while ( pkAttribute )
					{
						pcAttrName = pkAttribute->Name();
						pcAttrValue = pkAttribute->Value();

						if( !::strcmp(pcAttrName, "ERROR_MSG") )
						{
							m_iErrorMessage = ::atoi(pcAttrValue);
						}

						pkAttribute = pkAttribute->Next();
					}

					bChildOfChild = true;
					pkChildElement = pkChildElement->FirstChildElement();
					continue;
				}
				else if ( bChildOfChild )
				{
					if ( !::strcmp( pkChildElement->Value(), "LEVEL") )
					{
						pkAttribute = pkChildElement->FirstAttribute();
						while ( pkAttribute )
						{
							pcAttrName = pkAttribute->Name();
							pcAttrValue = pkAttribute->Value();

							if( !::strcmp(pcAttrName, "MIN") )
							{
								m_iLevelLimit_Min = ::atoi(pcAttrValue);
							}
							else if ( !::strcmp(pcAttrName, "MAX") )
							{
								m_iLevelLimit_Max = ::atoi(pcAttrValue);
							}
							else if ( !::strcmp(pcAttrName, "PARTY") )
							{
								m_bLevelParty = ( 0 != ::atoi(pcAttrValue) );
							}

							pkAttribute = pkAttribute->Next();
						}
					}
					else if ( !::strcmp( pkChildElement->Value(), "USEITEM") )
					{
						SKey kKey;

						pkAttribute = pkChildElement->FirstAttribute();
						while ( pkAttribute )
						{
							pcAttrName = pkAttribute->Name();
							pcAttrValue = pkAttribute->Value();

							if( !::strcmp(pcAttrName, "INV") )
							{
								kKey.kInvType = static_cast<EInvType>( ::atoi(pcAttrValue) );
							}
							else if( !::strcmp(pcAttrName, "NO") )
							{
								kKey.iItemNo = ::atoi(pcAttrValue);
							}
							else if( !::strcmp(pcAttrName, "COUNT") )
							{
								kKey.iItemCount = static_cast<size_t>( ::atoi(pcAttrValue) );
							}
							else if( !::strcmp(pcAttrName, "CONSUME") )
							{
								kKey.bConsume = ( 0 != ::atoi(pcAttrValue) );
							}
							else if ( !::strcmp(pcAttrName, "PARTY") )
							{
								kKey.bParty = ( 0 != ::atoi(pcAttrValue) );
							}
							else if ( !::strcmp(pcAttrName, "GROUP") )
							{
								kKey.iGroup = ::atoi(pcAttrValue);
							}

							pkAttribute = pkAttribute->Next();
						}

						if ( !kKey.IsEmpty() )
						{
							m_kConKey.push_back( kKey );
							m_kConKeyGroup.insert( kKey.iGroup );
						}
					}
					else if ( !::strcmp( pkChildElement->Value(), "QUEST") )
					{
						SQKey kQKey;

						pkAttribute = pkChildElement->FirstAttribute();
						while ( pkAttribute )
						{
							pcAttrName = pkAttribute->Name();
							pcAttrValue = pkAttribute->Value();

							if( !::strcmp(pcAttrName, "NO") )
							{
								kQKey.sQuestNo = static_cast<short int>( ::atoi(pcAttrValue) );
							}
							else if( !::strcmp(pcAttrName, "CHECK") )
							{
								kQKey.byState = static_cast<BYTE>( ::atoi(pcAttrValue) );
							}
							else if ( !::strcmp(pcAttrName, "PARTY") )
							{
								kQKey.bParty = ( 0 != ::atoi(pcAttrValue) );
							}
							else if ( !::strcmp(pcAttrName, "GROUP") )
							{
								kQKey.iGroup = ::atoi(pcAttrValue);
							}

							pkAttribute = pkAttribute->Next();
						}

						if ( !kQKey.IsEmpty() )
						{
							m_kConQuestKey.push_back( kQKey );
							m_kConQuestKeyGroup.insert( kQKey.iGroup );
						}
					}
					else if ( !::strcmp( pkChildElement->Value(), "TIME") )
					{
						STimeKey kTimeKey;
						bool bEveryDay = false;
						WORD wMinDay = 0;
						WORD wValidityMin = 0;

						pkAttribute = pkChildElement->FirstAttribute();
						while ( pkAttribute )
						{
							pcAttrName = pkAttribute->Name();
							pcAttrValue = pkAttribute->Value();

							if( !::strcmp(pcAttrName, "DAYOFWEEK") )
							{
								wMinDay = static_cast<WORD>( ::atoi(pcAttrValue) );
								if ( CGameTime::EVERY_WEEK_CHK > wMinDay )
								{
									wMinDay *= 1440;//60*24
								}
								else
								{
									bEveryDay = true;
								}
							}
							else if ( !::strcmp(pcAttrName, "HOUR") )
							{
								WORD wHour = static_cast<WORD>( ::atoi(pcAttrValue) );
								if ( wHour > 23 )
								{
									wHour = 23;
								}

								wHour *= 60;
								kTimeKey.wMin_Open += wHour;
							}
							else if ( !::strcmp(pcAttrName, "MIN") )
							{
								WORD wMin = static_cast<WORD>( ::atoi(pcAttrValue) );
								if ( wMin > 59 )
								{
									kTimeKey.wMin_Open += 60;
									wMin = 0;
								}

								kTimeKey.wMin_Open += wMin;
							}
							else if ( !::strcmp(pcAttrName, "VALIDITY_MIN") )
							{
								wValidityMin = static_cast<WORD>( ::atoi(pcAttrValue) );
							}

							pkAttribute = pkAttribute->Next();
						}

						if ( wValidityMin )
						{
							kTimeKey.wMin_Close = kTimeKey.wMin_Open + wValidityMin;

							if ( true == bEveryDay )
							{
								WORD wValue = CGameTime::EVERY_SUNDAY;
								for ( ; wValue < CGameTime::EVERY_WEEK_CHK ; ++wValue )
								{
									STimeKey kEveryTimeKey( kTimeKey.wMin_Open + ( 1440 * wValue ), kTimeKey.wMin_Close + ( 1440 * wValue ) );
									m_kContTimekey.push_back( kEveryTimeKey );
								}
							}
							else
							{
								m_kContTimekey.push_back( kTimeKey );
							}
						}	
					}
					else if ( !::strcmp( pkChildElement->Value(), "USEMONEY") )
					{
						SKey kKey;

						pkAttribute = pkChildElement->FirstAttribute();
						while ( pkAttribute )
						{
							pcAttrName = pkAttribute->Name();
							pcAttrValue = pkAttribute->Value();

							kKey.kInvType = IT_NONE;
							kKey.iItemNo = -1;

							if( !::strcmp(pcAttrName, "MONEY") )
							{
								kKey.iItemCount = static_cast<size_t>( ::atoi(pcAttrValue) );
							}
							else if( !::strcmp(pcAttrName, "CONSUME") )
							{
								kKey.bConsume = ( 0 != ::atoi(pcAttrValue) );
							}
							else if ( !::strcmp(pcAttrName, "PARTY") )
							{
								kKey.bParty = ( 0 != ::atoi(pcAttrValue) );
							}
							else if ( !::strcmp(pcAttrName, "GROUP") )
							{
								kKey.iGroup = ::atoi(pcAttrValue);
							}

							pkAttribute = pkAttribute->Next();
						}

						if ( !kKey.IsEmpty() )
						{
							m_kConKey.push_back( kKey );
							m_kConKeyGroup.insert( kKey.iGroup );
						}
					}
				}
				
				pkChildElement = pkChildElement->NextSiblingElement();
			}
			
		}
		else
		{
			VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("Bad TargetGoundNo=") <<  m_kTargetGroundKey.GroundNo() );
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}
	}
	else
	{
		m_byMoveType = E_MOVE_LOCK;
	}

	std::sort( m_kContTimekey.begin(), m_kContTimekey.end() );
	return true;
}

bool PgPortalAccess::IsAccess( PgPlayer *pkPlayer, bool bIsPartyMaster, CONT_PLAYER_MODIFY_ORDER *pContOrder )const
{
	if ( E_MOVE_LOCK == GetMoveType() )
	{
		pkPlayer->SendWarnMessage( 99 );//입장권한이 없습니다.
		return false;
	}

	// 레벨 체크
	if ( bIsPartyMaster || !m_bLevelParty )
	{
		int const iLevel = pkPlayer->GetAbil(AT_LEVEL);
		if ( m_iLevelLimit_Min > iLevel )
		{
			pkPlayer->SendWarnMessage2( 800, m_iLevelLimit_Min );
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}

		if ( m_iLevelLimit_Max < iLevel )
		{
			pkPlayer->SendWarnMessage2( 801, m_iLevelLimit_Max );
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}
	}

	// 시간 체크
	if ( (true == bIsPartyMaster) && m_kContTimekey.size() )
	{
		SYSTEMTIME kNowTime;
		g_kEventView.GetLocalTime( &kNowTime );

		WORD const wNowMinTime = (kNowTime.wHour * 60) + kNowTime.wMinute + ( 1440 * kNowTime.wDayOfWeek );
	
		int iRet = STimeKey::TIME_YET;
		ContTimeKey::const_iterator time_itr = m_kContTimekey.begin();
		while ( (time_itr != m_kContTimekey.end()) && (STimeKey::TIME_YET == iRet)  )
		{
			iRet = time_itr->Check(wNowMinTime);
			++time_itr;
		}

		if ( STimeKey::TIME_IN != iRet )
		{
			pkPlayer->SendWarnMessage( 803 );
			return false;
		}
	}
	// Quest Check
	ConKey_Base	kTempBase = m_kConQuestKeyGroup;
	ConKey_Base::iterator chk_itr = kTempBase.end();
	
	int iCanPassConditionCount = 0;
	ConQKey::const_iterator Qkey_itr = m_kConQuestKey.begin();
	for( ; Qkey_itr!=m_kConQuestKey.end() ; ++Qkey_itr )
	{
		chk_itr = kTempBase.find( Qkey_itr->iGroup );
		if ( chk_itr != kTempBase.end() )
		{
			if ( bIsPartyMaster || !(Qkey_itr->bParty) )
			{
				iCanPassConditionCount = 0;

				BYTE const& rkState = Qkey_itr->byState;
				if( 0 != (rkState & (GWQT_Ing| GWQT_IngAny)) )
				{
					SUserQuestState const* pkQState = pkPlayer->GetQuestState(Qkey_itr->sQuestNo);
					if( pkQState )
					{
						if( 0 != (rkState & GWQT_Ing)
							&&	pkQState->byQuestState != QS_Failed )
						{
							++iCanPassConditionCount;
						}
						if( 0 != (rkState & GWQT_IngAny) )
						{
							++iCanPassConditionCount;
						}
					}
				}

				if( 0 != (rkState & GWQT_Ended) )
				{
					PgMyQuest const* pkMyQuest = pkPlayer->GetMyQuest();
					if( pkMyQuest
						&&	pkMyQuest->IsEndedQuest(Qkey_itr->sQuestNo) )
					{
						++iCanPassConditionCount;
					}
				}

				if( 0 == iCanPassConditionCount )
				{
					continue;
				}
			}

			kTempBase.erase( chk_itr );
			if ( kTempBase.empty() )
			{
				// Check할 Group이 비었으니까 break
				break;
			}
		}
	}

	if ( kTempBase.size() )
	{
		if ( m_iErrorMessage )
		{
			pkPlayer->SendWarnMessage( m_iErrorMessage );
		}
		else
		{
			pkPlayer->SendWarnMessage( 11 );
		}

		return false;
	}
	// -------> Quest Check


	// Item & Money Check
	PgInventory *pkInv = pkPlayer->GetInven();
	if ( !pkInv )
	{
		VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("Inv is NULL : CharacterGUID = ") << pkPlayer->GetID() );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	kTempBase = m_kConKeyGroup;
	chk_itr = kTempBase.end();

	ConKey::const_iterator key_itr = m_kConKey.begin();
	for( ; key_itr!=m_kConKey.end() ; ++key_itr )
	{
		chk_itr = kTempBase.find( key_itr->iGroup );
		if ( chk_itr != kTempBase.end() )
		{
			if ( bIsPartyMaster || !(key_itr->bParty) )
			{
				HRESULT hRet = E_FAIL;
				if ( key_itr->bConsume )
				{
					switch(key_itr->kInvType)
					{
					case IT_NONE:	// 돈을 처리 할땐 인벤타입을 0 으로 해줘야 한다.
						{
							__int64 const i64Money = -static_cast<__int64>((*key_itr).iItemCount);
							hRet = pkInv->AddMoney(i64Money,true); // 실제로 빼지 않고 검사만 한다.
							if(pContOrder && S_OK == hRet)
							{
								pContOrder->push_back(SPMO(IMET_ADD_MONEY, pkPlayer->GetID(),SPMOD_Add_Money(i64Money)));
							}
						}break;
					default:
						{
							hRet = pkInv->GetItemModifyOrder( key_itr->kInvType, key_itr->iItemNo, key_itr->iItemCount, pContOrder );
						}break;
					}
				}
				else
				{
					switch(key_itr->kInvType)
					{
					case IT_NONE:	// 돈을 처리 할땐 인벤타입을 0 으로 해줘야 한다.
						{
							__int64 const i64Money = -static_cast<__int64>((*key_itr).iItemCount);
							hRet = pkInv->AddMoney(i64Money,true); // 실제로 빼지 않고 검사만 한다.
						}break;
					default:
						{
							hRet = pkInv->GetItemModifyOrder( key_itr->kInvType, key_itr->iItemNo, key_itr->iItemCount, NULL );
						}break;
					}
				}

				if ( FAILED(hRet) )
				{
					continue;
				}
			}

			kTempBase.erase( chk_itr );
			if ( kTempBase.empty() )
			{
				// Check할 Group이 비었으니까 break
				break;
			}
		}
	}

	if ( kTempBase.size() )
	{
		if ( m_iErrorMessage )
		{
			pkPlayer->SendWarnMessage( m_iErrorMessage );
		}
		else
		{
			BM::CPacket kCPacket( PT_M_C_NFY_NEEDITEM_MESSAGE, static_cast<int>(0) );
			pkPlayer->Send( kCPacket );
		}

		if ( pContOrder )
		{
			pContOrder->clear();
		}

		return false;
	}
	// ----> Item Check

	return true;
}

void PgPortalAccess::Get( SReqMapMove_MT &rkRMM )const
{
	rkRMM.kTargetKey = m_kTargetGroundKey;
	rkRMM.nTargetPortal = m_nTargetPortal;
}