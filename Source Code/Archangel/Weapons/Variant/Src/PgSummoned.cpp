#include "stdafx.h"
#include "PgControlDefMgr.h"
#include "PgSummoned.h"

PgSummoned::PgSummoned() : m_kLifeTime(0)
{
}

PgSummoned::~PgSummoned()
{
}

EUnitType PgSummoned::UnitType() const
{
	return UT_SUMMONED;
}

void PgSummoned::Init()
{
	Caller(BM::GUID::NullData());
	LifeTime(0);
	CUnit::Init();
}

unsigned long PgSummoned::LifeTime() const
{
	return m_kLifeTime;
}
void PgSummoned::LifeTime(unsigned long const kTime)
{
	m_kLifeTime = kTime;
}

void PgSummoned::CopyAbilFromClassDef( CLASS_DEF_BUILT const *pDef)
{
	if (pDef == NULL)
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkDef is NULL. Caller ")<< Caller());
		return;
	}

	
	CLASSDEF_ABIL_CONT::const_iterator itor = pDef->kAbil.begin();
	while (itor != pDef->kAbil.end())
	{
		SetAbil(itor->first, itor->second);

		++itor;
	}
	if(0 == GetAbil(AT_MAX_HP))
	{
		SetAbil(AT_MAX_HP, static_cast<int>(pDef->kDef.sMaxHP) );	//임시
	}
	if(0 == GetAbil(AT_HP))
	{
		SetAbil(AT_HP, GetAbil(AT_MAX_HP) );
	}
	if(0 == GetAbil(AT_AI_TYPE))
	{
		m_kBasic.iAIType = 1;//없으면 선공형 몹 AI
	}

	SetAbil(AT_MOVESPEED, 0==pDef->kDef.sMoveSpeed ? 50 : static_cast<int>(pDef->kDef.sMoveSpeed) );
}

bool PgSummoned::SetInfo(SummonedInfo_ const * pInfo)
{
	if( !pInfo )
	{
		VERIFY_INFO_LOG( false, BM::LOG_LV5, __FL__ << _T(" pInfo is NULL. Caller is ") << Caller());
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	// ClassDef로 부터 모든 Abil 세팅하기
	GET_DEF(PgClassDefMgr, kClassDefMgr);
	CLASS_DEF_BUILT const* pDef = kClassDefMgr.GetDef(pInfo->kClassKey);

	if( !pDef )
	{
		VERIFY_INFO_LOG( false, BM::LOG_LV5, __FL__ << _T(" Cannot Get ClassDef(Class:") << pInfo->kClassKey.iClass << _T(", Level:") << pInfo->kClassKey.nLv << _T(") Caller is")  << Caller() );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CopyAbilFromClassDef(pDef);

	SetID( pInfo->kGuid );
	Caller(pInfo->kCaller);
	//m_kParty = pInfo->kParty;

	SetAbil(AT_CLASS, pInfo->kClassKey.iClass);//PgControlUnit으로 들어가면 안됨.
	SetAbil(AT_LEVEL, pInfo->kClassKey.nLv);
	GoalPos(POINT3(0, 0, 0));

	SetState(US_OPENING);
	
	int const iOpeningDelay = GetAbil(AT_MON_OPENING_TIME);
	if(0<iOpeningDelay)
	{
		SetDelay(iOpeningDelay);
	}

	std::map<WORD, int>::const_iterator itor = pInfo->kAbil.begin();
	while (itor != pInfo->kAbil.end())
	{
		SetAbil(itor->first, itor->second);
		++itor;
	}

	int iValue = GetAbil(AT_AI_TYPE);
	m_kAI.SetPattern((iValue>0) ? iValue : 1);
	m_kAI.SetCurrentAction(EAI_ACTION_OPENING);
	for (int i=1; i<EAI_ACTION_MAX; i++)
	{
		iValue = GetAbil(AT_AI_ACTIONTYPE_MIN + i);
		if (iValue != 0)
		{
			m_kAI.AddActionType((EAIActionType)i, iValue);
		}
	}

	return true;
}

HRESULT PgSummoned::Create(void const *pInfo)
{
	if(NULL==pInfo)
	{
		return E_FAIL;
	}
	Init();

	SummonedInfo_ const *pSummonInfo = static_cast<SummonedInfo_ const *>(pInfo);

	if(false==SetInfo( pSummonInfo ))
	{
		return E_FAIL;
	}

	return S_OK;
}

void PgSummoned::WriteToPacket(BM::CPacket &rkPacket, EWRITETYPE const kWriteType)const
{
	CUnit::WriteToPacket(rkPacket, kWriteType);
	DefaultSummonedInfo_ kInfo;
	kInfo.kGuid = GetID();
	kInfo.iClassNo = GetAbil(AT_CLASS);
	kInfo.ptPos = GetPos();
	kInfo.iHP = GetAbil(AT_HP);
	kInfo.iMaxHP = GetAbil(AT_C_MAX_HP);
	kInfo.WriteToPacket(rkPacket);
}
EWRITETYPE PgSummoned::ReadFromPacket(BM::CPacket &rkPacket)
{
	EWRITETYPE kWriteType = CUnit::ReadFromPacket(rkPacket);
	DefaultSummonedInfo_ kInfo;
	kInfo.ReadFromPacket(rkPacket);
	SetInfoFromDefaultSummonedInfo(kInfo);
	return kWriteType;
}

bool PgSummoned::SetAbil( WORD const Type, int const iValue, bool const bIsSend, bool const bBroadcast)
{//펫과 달리 레벨, 클래스가 변경되지 않는다.
	return CUnit::SetAbil( Type, iValue, bIsSend, bBroadcast );
}

int PgSummoned::GetAbil(WORD const Type) const
{
	switch(Type)
	{
	case AT_OWNER_TYPE:
		{
			return CAbilObject::GetAbil(AT_OWNER_TYPE);
		}break;
	case AT_CALLER_TYPE:
		{
			return CAbilObject::GetAbil(AT_CALLER_TYPE);
		}break;
	}
	return CUnit::GetAbil(Type);
}

void PgSummoned::SetInfoFromDefaultSummonedInfo(DefaultSummonedInfo_ const &rkInfo)
{
	SetID(rkInfo.kGuid);
	SetAbil(AT_CLASS, rkInfo.iClassNo);
	SetAbil(AT_C_MAX_HP, rkInfo.iMaxHP);
	SetAbil(AT_HP, rkInfo.iHP);
	SetPos(rkInfo.ptPos);
}

/*void PgSummoned::SetCommonAbil()
{
	int const iClassNo = GetAbil(AT_CLASS);

}*/