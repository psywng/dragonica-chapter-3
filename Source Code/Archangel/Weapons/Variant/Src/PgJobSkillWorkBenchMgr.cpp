#include "StdAfx.h"
#include "Lohengrin/packetstruct.h"
#include "TableDataManager.h"
#include "Item.h"
#include "PgJobSkillSaveIdx.h"
#include "PgProbability.h"
#include "PgJobSkillWorkBench.h"
#include "PgJobSkillWorkBenchMgr.h"
#include "ItemDefMgr.h"
#include "PgControlDefMgr.h"

namespace JobSkilLWorkBenchMgrUtil
{
	void AddHelperGuid(ContGuidSet& rkOut, PgJobSkillWorkBench const& rkWorkBench)
	{
		if( 0 < rkWorkBench.BlessDurationSec() )
		{
			rkOut.insert(rkWorkBench.BlessCharGuid());
		}
	}
	void DelHelperGuid(ContGuidSet& rkOut, PgJobSkillWorkBench const& rkWorkBench)
	{
		rkOut.erase(rkWorkBench.BlessCharGuid());
	}

	bool SetAlterInfoFromItemNo(int const iItemNo, PgJobSkillWorkBenchAlterInfo& rkOutAlter)
	{// 모든 작업대에 적용될 
		GET_DEF(CItemDefMgr, kItemDefMgr);
		CItemDef const* pkItemDef = kItemDefMgr.GetDef(iItemNo);
		if(!pkItemDef
			|| 0 == pkItemDef->GetAbil(AT_JS_MGR_WORKBENCH)
			)
		{
			return false;
		}

		int  const iWorkTimeRate = pkItemDef->GetAbil(AT_JS_ALL_WORKBENCH_UPGRADE_FAST_RATE);
		int const iExpertnessRate = pkItemDef->GetAbil(AT_JS_ALL_WORKBENCH_ADD_EXPERTNESS_RATE);
		int const iDecDurRate = pkItemDef->GetAbil(AT_JS_ALL_WORKBENCH_DEC_DURATION_RATE);
		bool const bAutoRepair = 0 < pkItemDef->GetAbil(AT_JS_ALL_WORKBENCH_AUTO_REPAIR);
		bool const bCompleteItemSendMail = 0 < pkItemDef->GetAbil(AT_JS_ALL_WORKBENCH_AUTO_MAIL);

		rkOutAlter.SetWorkTimeRate(iWorkTimeRate);
		rkOutAlter.SetExertnessRate(iExpertnessRate);
		rkOutAlter.SetDecDurRate(iDecDurRate);
		rkOutAlter.SetAutoRepair(bAutoRepair);
		rkOutAlter.SetCompleteItemSendMail(bCompleteItemSendMail);

		return true;
	}

	bool ClearAlterInfo(int const iItemNo, PgJobSkillWorkBenchAlterInfo& rkOutAlter)
	{// 관리 작업대라면
		GET_DEF(CItemDefMgr, kItemDefMgr);
		CItemDef const* pkItemDef = kItemDefMgr.GetDef(iItemNo);
		if(!pkItemDef
			|| 0 == pkItemDef->GetAbil(AT_JS_MGR_WORKBENCH)
			)
		{
			return false;
		}// 정보를 클리어
		rkOutAlter.Clear();
		return true;
	}

	bool AddAlterInfoFromItemNo(int const iItemNo, PgJobSkillWorkBenchAlterInfo& rkOutAlter)
	{// 모든 작업대에 적용될 
		GET_DEF(CItemDefMgr, kItemDefMgr);
		CItemDef const* pkItemDef = kItemDefMgr.GetDef(iItemNo);
		if(!pkItemDef
			|| 0 == pkItemDef->GetAbil(AT_JS_MGR_WORKBENCH)
			)
		{
			return false;
		}

		int  const iWorkTimeRate = pkItemDef->GetAbil(AT_JS_ALL_WORKBENCH_UPGRADE_FAST_RATE);
		int const iExpertnessRate = pkItemDef->GetAbil(AT_JS_ALL_WORKBENCH_ADD_EXPERTNESS_RATE);
		int const iDecDurRate = pkItemDef->GetAbil(AT_JS_ALL_WORKBENCH_DEC_DURATION_RATE);
		bool const bAutoRepair = 0 < pkItemDef->GetAbil(AT_JS_ALL_WORKBENCH_AUTO_REPAIR);
		bool const bCompleteItemSendMail = 0 < pkItemDef->GetAbil(AT_JS_ALL_WORKBENCH_AUTO_MAIL);
		
		int const iOrigWorkTimeRate = rkOutAlter.GetWorkTimeRate();
		int const iOrigExpertnessRate = rkOutAlter.GetExertnessRate();
		int const iOrigDecDurRate = rkOutAlter.GetDecDurRate();
		bool const bOrigAutoRepair = rkOutAlter.IsAutoRepair();
		bool const bOrigCompleteItemSendMail = rkOutAlter.IsCompleteItemSendMail();

		rkOutAlter.SetWorkTimeRate( iOrigWorkTimeRate+iWorkTimeRate );
		rkOutAlter.SetExertnessRate( iOrigExpertnessRate+iExpertnessRate );
		rkOutAlter.SetDecDurRate( iOrigDecDurRate+iDecDurRate );
		rkOutAlter.SetAutoRepair( bOrigAutoRepair|bAutoRepair );
		rkOutAlter.SetCompleteItemSendMail( bOrigCompleteItemSendMail|bCompleteItemSendMail );
		return true;
	}

	bool SubtractAlterInfoFromItemNo(int const iItemNo, PgJobSkillWorkBenchAlterInfo& rkOutAlter)
	{// 모든 작업대에 적용될 
		GET_DEF(CItemDefMgr, kItemDefMgr);
		CItemDef const* pkItemDef = kItemDefMgr.GetDef(iItemNo);
		if(!pkItemDef
			|| 0 == pkItemDef->GetAbil(AT_JS_MGR_WORKBENCH)
			)
		{
			return false;
		}

		int  const iWorkTimeRate = pkItemDef->GetAbil(AT_JS_ALL_WORKBENCH_UPGRADE_FAST_RATE);
		int const iExpertnessRate = pkItemDef->GetAbil(AT_JS_ALL_WORKBENCH_ADD_EXPERTNESS_RATE);
		int const iDecDurRate = pkItemDef->GetAbil(AT_JS_ALL_WORKBENCH_DEC_DURATION_RATE);
		bool const bAutoRepair = 0 < pkItemDef->GetAbil(AT_JS_ALL_WORKBENCH_AUTO_REPAIR);
		bool const bCompleteItemSendMail = 0 < pkItemDef->GetAbil(AT_JS_ALL_WORKBENCH_AUTO_MAIL);
		
		int const iOrigWorkTimeRate = rkOutAlter.GetWorkTimeRate();
		int const iOrigExpertnessRate = rkOutAlter.GetExertnessRate();
		int const iOrigDecDurRate = rkOutAlter.GetDecDurRate();
		bool const bOrigAutoRepair = rkOutAlter.IsAutoRepair();
		bool const bOrigCompleteItemSendMail = rkOutAlter.IsCompleteItemSendMail();

		rkOutAlter.SetWorkTimeRate( iOrigWorkTimeRate-iWorkTimeRate );
		rkOutAlter.SetExertnessRate( iOrigExpertnessRate-iExpertnessRate );
		rkOutAlter.SetDecDurRate( iOrigDecDurRate-iDecDurRate );
		rkOutAlter.SetAutoRepair( bOrigAutoRepair&(!bAutoRepair) );									// 기존 T, 현재 F 일경우만 true
		rkOutAlter.SetCompleteItemSendMail( bOrigCompleteItemSendMail&(!bCompleteItemSendMail) );	// 기존 T, 현재 F 일경우만 true
		return true;
	}
};

///
PgJobSkillWorkBenchMgr::PgJobSkillWorkBenchMgr()
	: m_kCont(), m_kHomeGuid(), m_kOwnerGuid(), m_kOwnerMemberGuid(), m_kContBlessGuid(), m_bPublicAlterSeted(false)
{
}
PgJobSkillWorkBenchMgr::PgJobSkillWorkBenchMgr(BM::GUID const& rkHomeGuid, BM::GUID const& rkOwnerGuid)
	: m_kCont(), m_kHomeGuid(rkHomeGuid), m_kOwnerGuid(rkOwnerGuid), m_kOwnerMemberGuid(), m_kContBlessGuid(), m_kOldMajorStatus(WBS_NONE), m_bPublicAlterSeted(false)
{
}
PgJobSkillWorkBenchMgr::~PgJobSkillWorkBenchMgr()
{
}
int PgJobSkillWorkBenchMgr::Pop(BM::GUID const& rkItemGuid, size_t const iSlotNo)
{
	CONT_JS_WORK_BENCH::iterator find_iter = m_kCont.find( rkItemGuid );
	if( m_kCont.end() != find_iter )
	{
		return (*find_iter).second.Pop(iSlotNo);
	}
	return 0;
}
EWorkBenchResult PgJobSkillWorkBenchMgr::Push(BM::GUID const& rkItemGuid, size_t const iSlotNo, int const iItemNo)
{
	CONT_JS_WORK_BENCH::iterator find_iter = m_kCont.find( rkItemGuid );
	if( m_kCont.end() != find_iter )
	{
		if( 0 < (*find_iter).second.Duration() )
		{
			if( (*find_iter).second.Push(iSlotNo, iItemNo) )
			{
				return WBR_SUCCESS;
			}
			else
			{
				return WBR_SLOT_IS_NOT_EMPTY;
			}
		}
		else
		{
			return WBR_DURATION_ZERO;
		}
	}
	return WBR_SYSTEM_ERROR;;
}

EWorkBenchResult PgJobSkillWorkBenchMgr::Del(BM::GUID const& rkItemGuid, bool& bIsPublicAlterWorkBench_out)
{
	bIsPublicAlterWorkBench_out = false;
	CONT_JS_WORK_BENCH::iterator find_iter = m_kCont.find( rkItemGuid );
	if( m_kCont.end() != find_iter )
	{
		if( (*find_iter).second.IsEmpty() )
		{
			JobSkilLWorkBenchMgrUtil::DelHelperGuid(m_kContBlessGuid,  (*find_iter).second);
			if( ClearPublicAlterInfo( (*find_iter).second.ItemNo()) )
			{
				bIsPublicAlterWorkBench_out = true;
			}
			m_kCont.erase(find_iter);
			return WBR_SUCCESS;
		}
		else
		{
			return WBR_IS_NOT_EMPTY;
		}
	}

	return WBR_SYSTEM_ERROR;
}
bool PgJobSkillWorkBenchMgr::Add(BM::GUID const& rkItemGuid, int const iItemNo, int const iDuration, bool& bIsPublicAlterWorkBench_out)
{
	CONT_JS_WORK_BENCH::_Pairib kRet = m_kCont.insert( std::make_pair(rkItemGuid, CONT_JS_WORK_BENCH::mapped_type()) );
	if( kRet.second )
	{
		(*kRet.first).second.ItemNo(iItemNo);
		(*kRet.first).second.Duration(iDuration);
	}

	if( SetPublicAlterInfo(iItemNo) )
	{// 관리 작업대
		bIsPublicAlterWorkBench_out = true;
	}
	else
	{
		bIsPublicAlterWorkBench_out = false;
	}
	
	return kRet.second;
}
void PgJobSkillWorkBenchMgr::WriteToPacket(BM::CPacket& rkPacket) const
{
	rkPacket.Push(m_kHomeGuid);
	rkPacket.Push(m_kOwnerGuid);
	rkPacket.Push(m_kOwnerMemberGuid);
	rkPacket.Push( m_kCont.size() );
	CONT_JS_WORK_BENCH::const_iterator iter = m_kCont.begin();
	while( m_kCont.end() != iter )
	{
		rkPacket.Push( (*iter).first );
		(*iter).second.WriteToPacket(rkPacket);
		++iter;
	}
	rkPacket.Push(m_kOldMajorStatus);
	rkPacket.Push(m_kPublicAlterInfo);
	rkPacket.Push(m_bPublicAlterSeted);
}
void PgJobSkillWorkBenchMgr::WriteToPacket(BM::GUID const& rkItemGuid, BM::CPacket& rkPacket) const
{
	CONT_JS_WORK_BENCH::const_iterator iter = m_kCont.find(rkItemGuid);
	if( m_kCont.end() != iter )
	{
		rkPacket.Push( (*iter).first );
		(*iter).second.WriteToPacket(rkPacket);
	}
}
void PgJobSkillWorkBenchMgr::ReadFromPacket(BM::CPacket& rkPacket)
{
	m_kCont.clear();
	m_kContBlessGuid.clear();
	BM::GUID kItemGuid;
	size_t iCount = 0;
	rkPacket.Pop(m_kHomeGuid);
	rkPacket.Pop(m_kOwnerGuid);
	rkPacket.Pop(m_kOwnerMemberGuid);
	rkPacket.Pop( iCount );

	CONT_JS_WORK_BENCH::mapped_type kNewElement;
	while( iCount-- )
	{
		rkPacket.Pop( kItemGuid );
		ReadFromPacket(kItemGuid, rkPacket);
	}
	rkPacket.Pop(m_kOldMajorStatus);
	rkPacket.Pop(m_kPublicAlterInfo);
	rkPacket.Pop(m_bPublicAlterSeted);
}
void PgJobSkillWorkBenchMgr::ReadFromPacket(BM::GUID const& rkItemGuid, BM::CPacket& rkPacket)
{
	int iItemNo = 0;
	CONT_JS_WORK_BENCH::iterator iter = m_kCont.find(rkItemGuid);
	if( m_kCont.end() != iter )
	{
		JobSkilLWorkBenchMgrUtil::DelHelperGuid(m_kContBlessGuid, (*iter).second);
		(*iter).second.ReadFromPacket(rkPacket);
		JobSkilLWorkBenchMgrUtil::AddHelperGuid(m_kContBlessGuid, (*iter).second);
		iItemNo = (*iter).second.ItemNo();
	}
	else
	{
		CONT_JS_WORK_BENCH::mapped_type kNewElement;
		kNewElement.ReadFromPacket(rkPacket);
		m_kCont.insert( std::make_pair(rkItemGuid, kNewElement) );
		JobSkilLWorkBenchMgrUtil::AddHelperGuid(m_kContBlessGuid, kNewElement);
		iItemNo = kNewElement.ItemNo();
	}
	SetPublicAlterInfo(iItemNo);
}

void PgJobSkillWorkBenchMgr::ReadFromDB(BM::GUID const& rkItemGuid, CEL::DB_DATA_ARRAY::const_iterator& iter)
{
	int iItemNo = 0;
	CONT_JS_WORK_BENCH::iterator find_iter = m_kCont.find(rkItemGuid);
	if( m_kCont.end() != find_iter )
	{
		JobSkilLWorkBenchMgrUtil::DelHelperGuid(m_kContBlessGuid, (*find_iter).second);
		(*find_iter).second.ReadFromDB(iter);
		JobSkilLWorkBenchMgrUtil::AddHelperGuid(m_kContBlessGuid, (*find_iter).second);
		iItemNo = (*find_iter).second.ItemNo();
	}
	else
	{
		CONT_JS_WORK_BENCH::mapped_type kNewElement;
		kNewElement.ReadFromDB(iter);
		m_kCont.insert( std::make_pair(rkItemGuid, kNewElement) );
		JobSkilLWorkBenchMgrUtil::AddHelperGuid(m_kContBlessGuid, kNewElement);
		iItemNo = kNewElement.ItemNo();
	}
	SetPublicAlterInfo(iItemNo);
}
bool PgJobSkillWorkBenchMgr::WriteToDB(BM::GUID const& rkItemGuid, CEL::DB_QUERY& rkQuery) const
{
	CONT_JS_WORK_BENCH::const_iterator iter = m_kCont.find(rkItemGuid);
	if( m_kCont.end() != iter )
	{
		(*iter).second.WriteToDB(rkItemGuid, rkQuery);
		return true;
	}
	return false;
}
void PgJobSkillWorkBenchMgr::Process(__int64 const iEleapsedTime, CONT_WORKBENCH_COMPLETE_INFO& rkOutCompleteInfo, CONT_WORKBENCH_DELETE_ITEM& rkContDeleteItem, size_t& stWorkedAtThisTimeSlotNo)
{
	CONT_JS_WORK_BENCH::iterator iter = m_kCont.begin();
	while( m_kCont.end() != iter )
	{
		CONT_JS_WORK_BENCH::key_type const& rkWorkBenchGuid = (*iter).first;
		CONT_JS_WORK_BENCH::mapped_type& rkWorkBench = (*iter).second;
		CONT_JSWB_DELETE_ITEM kDeleteItem;

		rkWorkBench.UpdateCompleteItem(iEleapsedTime, kDeleteItem);

		int const iOldDuration = rkWorkBench.Duration();
		bool const bOldHasTrouble = rkWorkBench.HasTrouble();
		__int64 iOldBlessDurationSec = rkWorkBench.BlessDurationSec();
		int iEndTurnItemNo = 0, iResultItemNo = 0;
		EWorkBenchEventType eRet = rkWorkBench.Update(iEleapsedTime, iEndTurnItemNo, iResultItemNo, m_kPublicAlterInfo, stWorkedAtThisTimeSlotNo);

		if( WBET_NONE != eRet
		||	false == kDeleteItem.empty()
		||	(false == bOldHasTrouble && true == rkWorkBench.HasTrouble())
		||	(0 != iOldBlessDurationSec && 0 == rkWorkBench.BlessDurationSec()) )
		{
			CONT_JSWB_DELETE_ITEM::const_iterator delete_iter = kDeleteItem.begin();
			while(kDeleteItem.end() != delete_iter)
			{
				rkContDeleteItem.push_back( std::make_pair(rkWorkBenchGuid, (*delete_iter)) );
				++delete_iter;
			}
			
			if( WBET_NONE != eRet )
			{
				if(WBET_TROUBLE == eRet	//rkWorkBench.HasTrouble() 
					&& m_kPublicAlterInfo.IsAutoRepair()
					)
				{// 관리 기계로 인해 자동수리가 가능하고, 고장이 났다면
					rkWorkBench.HasTrouble(false);
					eRet = WBET_MGR_AUTO_REPAIR;
				}

				rkOutCompleteInfo.push_back( SWorkBenchCompleteInfo(eRet, OwnerGuid(), rkWorkBenchGuid, rkWorkBench, iEndTurnItemNo, iResultItemNo, iOldDuration) );
			}
			if( (0 != iOldBlessDurationSec && 0 == rkWorkBench.BlessDurationSec()) )
			{
				rkOutCompleteInfo.push_back( SWorkBenchCompleteInfo(WBET_BLESS_END, OwnerGuid(), rkWorkBenchGuid, rkWorkBench, iEndTurnItemNo, iResultItemNo, iOldDuration) );
				JobSkilLWorkBenchMgrUtil::DelHelperGuid(m_kContBlessGuid, rkWorkBench);
				rkWorkBench.BlessCharGuid(BM::GUID::NullData());
				rkWorkBench.BlessSkillNo(0);
			}
		}
		++iter;
	}
}

bool PgJobSkillWorkBenchMgr::GetWorkBench(BM::GUID const& rkWorkBenchGuid, PgJobSkillWorkBench& kOut) const
{
	CONT_JS_WORK_BENCH::const_iterator iter = m_kCont.find(rkWorkBenchGuid);
	if( m_kCont.end() != iter )
	{
		kOut = (*iter).second;
		return true;
	}
	return false;
}

void PgJobSkillWorkBenchMgr::Clear()
{
	m_kCont.clear();
	m_kPublicAlterInfo.Clear();
}

PgJobSkillWorkBenchMgr::CONT_JS_WORK_BENCH const PgJobSkillWorkBenchMgr::CopyAllWorkBenchInfo() const
{// 복사해서 넘김
	return m_kCont;
}

bool PgJobSkillWorkBenchMgr::RepairWorkBench(BM::GUID const& rkWorkBenchGuid)
{
	CONT_JS_WORK_BENCH::iterator iter = m_kCont.find(rkWorkBenchGuid);
	if( m_kCont.end() != iter )
	{
		PgJobSkillWorkBench& kWorkBench = (*iter).second;
		kWorkBench.HasTrouble(false);
		return true;
	}
	return false;
}

bool PgJobSkillWorkBenchMgr::BlessWorkBench(BM::GUID const& rkWorkBenchGuid, BM::GUID const& rkCharGuid, int const iBlessSkillNo)
{
	CONT_JS_WORK_BENCH::iterator iter = m_kCont.find(rkWorkBenchGuid);
	if( m_kCont.end() != iter )
	{
		PgJobSkillWorkBench& kWorkBench = (*iter).second;
		if( 0 == kWorkBench.CurUpgradeSlot()
			|| 0 < kWorkBench.BlessDurationSec()
			|| m_kContBlessGuid.end() != m_kContBlessGuid.find(rkCharGuid) )
		{// 작업중인 상태가 아니고, 축복 상태, 이미 다른곳에 축복을 주었으면 줄수 없다.
			return false;
		}
		
		kWorkBench.BlessDurationSec(g_kEventView.VariableCont().iJobSkill_BlessRate_DurationSec);
		kWorkBench.BlessCharGuid(rkCharGuid);
		kWorkBench.BlessSkillNo(iBlessSkillNo);
		JobSkilLWorkBenchMgrUtil::AddHelperGuid(m_kContBlessGuid, kWorkBench);
		return true;
	}
	return false;
}

size_t PgJobSkillWorkBenchMgr::GetEmptySlotCnt(int const iGatherType)
{
	size_t stCnt = 0;
	CONT_JS_WORK_BENCH::iterator iter = m_kCont.begin();
	while( m_kCont.end() != iter )
	{
		PgJobSkillWorkBench& kWorkBench = (*iter).second;
		SJobSkillMachine kMachineInfo;
		if(JobSkillWorkBenchUtil:: GetWorkbenchInfo(kWorkBench.ItemNo(), kMachineInfo))
		{
			if(iGatherType == kMachineInfo.iGatherType)
			{
				stCnt += kWorkBench.GetEmptySlotCnt();
			}
		}
		++iter;
	}
	return stCnt;
}
size_t PgJobSkillWorkBenchMgr::GetNotEmptySlotCnt(int const iGatherType)
{
	size_t stCnt = 0;
	CONT_JS_WORK_BENCH::iterator iter = m_kCont.begin();
	while( m_kCont.end() != iter )
	{
		PgJobSkillWorkBench& kWorkBench = (*iter).second;
		SJobSkillMachine kMachineInfo;
		if(JobSkillWorkBenchUtil:: GetWorkbenchInfo(kWorkBench.ItemNo(), kMachineInfo))
		{
			if(iGatherType == kMachineInfo.iGatherType)
			{
				stCnt += kWorkBench.GetNotEmptySlotCnt();
			}
		}
		++iter;
	}
	return stCnt;
}

size_t PgJobSkillWorkBenchMgr::GetCompleteSlotCnt(int const iGatherType)
{
	size_t stCnt = 0;
	CONT_JS_WORK_BENCH::iterator iter = m_kCont.begin();
	while( m_kCont.end() != iter )
	{
		PgJobSkillWorkBench& kWorkBench = (*iter).second;
		SJobSkillMachine kMachineInfo;
		if(JobSkillWorkBenchUtil:: GetWorkbenchInfo(kWorkBench.ItemNo(), kMachineInfo))
		{
			if(iGatherType == kMachineInfo.iGatherType)
			{
				stCnt += kWorkBench.GetCompleteSlotCnt();
			}
		}
		++iter;
	}
	return stCnt;
}

bool PgJobSkillWorkBenchMgr::IsExistWorkBenchInThisStatus(int const iGatherType, EWorkBenchStatus const eStatus)
{
	CONT_JS_WORK_BENCH::iterator iter = m_kCont.begin();
	while( m_kCont.end() != iter )
	{
		PgJobSkillWorkBench& kWorkBench = (*iter).second;
		SJobSkillMachine kMachineInfo;
		if(JobSkillWorkBenchUtil:: GetWorkbenchInfo(kWorkBench.ItemNo(), kMachineInfo)
			&& iGatherType == kMachineInfo.iGatherType
			&& eStatus ==  kWorkBench.GetStatus()
			)
		{
			return true;
		}
		++iter;
	}
	return false;
}

bool PgJobSkillWorkBenchMgr::IsExistBlessedWorkBench(int const iGatherType)
{
	CONT_JS_WORK_BENCH::iterator iter = m_kCont.begin();
	while( m_kCont.end() != iter )
	{
		PgJobSkillWorkBench& kWorkBench = (*iter).second;
		SJobSkillMachine kMachineInfo;
		if(JobSkillWorkBenchUtil:: GetWorkbenchInfo(kWorkBench.ItemNo(), kMachineInfo)
			&& iGatherType == kMachineInfo.iGatherType
			&& 0 < kWorkBench.BlessDurationSec()
			)
		{
			return true;
		}
		++iter;
	}
	return false;
}

bool PgJobSkillWorkBenchMgr::IsExistWorkBench(int const iGatherType)
{
	CONT_JS_WORK_BENCH::iterator iter = m_kCont.begin();
	while( m_kCont.end() != iter )
	{
		PgJobSkillWorkBench& kWorkBench = (*iter).second;
		SJobSkillMachine kMachineInfo;
		if(JobSkillWorkBenchUtil:: GetWorkbenchInfo(kWorkBench.ItemNo(), kMachineInfo)
			&& iGatherType == kMachineInfo.iGatherType
			)
		{
			return true;
		}
		++iter;
	}
	return false;
}

size_t PgJobSkillWorkBenchMgr::GetWorkBenchCnt() const
{
	return m_kCont.size();
}

bool PgJobSkillWorkBenchMgr::IsBlessedWorkBench(BM::GUID const& rkWorkBenchGuid) const
{
	CONT_JS_WORK_BENCH::const_iterator find_iter = m_kCont.find( rkWorkBenchGuid );
	if( m_kCont.end() != find_iter )
	{
		PgJobSkillWorkBench const& kWorkBench = (*find_iter).second;
		if(0 < kWorkBench.BlessDurationSec())
		{
			return true;
		}
	}
	return false;
}
__int64 PgJobSkillWorkBenchMgr::GetBlessRemainTime(BM::GUID const& rkCharGuid) const
{
	if( m_kContBlessGuid.end() != m_kContBlessGuid.find(rkCharGuid) )
	{
		CONT_JS_WORK_BENCH::const_iterator iter = m_kCont.begin();
		while( m_kCont.end() != iter )
		{
			if( (*iter).second.BlessCharGuid() == rkCharGuid )
			{
				return (*iter).second.BlessDurationSec();
			}
			++iter;
		}
	}
	return 0;
}

bool PgJobSkillWorkBenchMgr::IsExistWorkBench() const
{
	return !m_kCont.empty();
}

bool PgJobSkillWorkBenchMgr::SetPublicAlterInfo(int const iItemNo)
{
	if(!m_bPublicAlterSeted
		&& JobSkilLWorkBenchMgrUtil::SetAlterInfoFromItemNo(iItemNo, m_kPublicAlterInfo) 
		)
	{
		m_bPublicAlterSeted = true;
		return true;
	}
	return false;
}

bool PgJobSkillWorkBenchMgr::ClearPublicAlterInfo(int const iItemNo)
{
	if( JobSkilLWorkBenchMgrUtil::ClearAlterInfo(iItemNo, m_kPublicAlterInfo) )
	{
		m_bPublicAlterSeted = false;
		return true;
	}
	return false;
}

bool PgJobSkillWorkBenchMgr::IsAbleAutoRepair(BM::GUID const& rkWorkBenchGuid) const
{// 자동수리가 가능한가
	CONT_JS_WORK_BENCH::const_iterator find_iter = m_kCont.find( rkWorkBenchGuid );
	if( m_kCont.end() != find_iter )
	{
		return (*find_iter).second.IsAbleAutoRepair();
	}
	return false;
}

bool PgJobSkillWorkBenchMgr::IsCompleteItemSendMail(BM::GUID const& rkWorkBenchGuid) const
{
	CONT_JS_WORK_BENCH::const_iterator find_iter = m_kCont.find( rkWorkBenchGuid );
	if( m_kCont.end() != find_iter )
	{
		return (*find_iter).second.IsCompleteItemSendMail();
	}
	return false;
}

// GM 커맨드
void PgJobSkillWorkBenchMgr::MakeCompleteAllWorkBenchOnNextTick()
{//강제로 모든 작업 완료 시킴
	CONT_JS_WORK_BENCH::iterator iter = m_kCont.begin();
	while( m_kCont.end() != iter )
	{
		PgJobSkillWorkBench& kWorkBench =  (*iter).second;
		kWorkBench.MakeCompleteOnNextTick();
		++iter;
	}
}

void PgJobSkillWorkBenchMgr::MakeTroubleAllWorkBenchOnNextTick()
{// 강제로 모든 작업대를 다음틱에 고장나게함
	CONT_JS_WORK_BENCH::iterator iter = m_kCont.begin();
	while( m_kCont.end() != iter )
	{
		PgJobSkillWorkBench& kWorkBench =  (*iter).second;
		kWorkBench.MakeTroubleOnNextTick();
		++iter;
	}
}

void PgJobSkillWorkBenchMgr::SetBlessRemainTime_DelBlessGuid(BM::GUID const& rkCharGuid, __int64 const iSec)
{// 버프 주기 남은 시간을 조정함
	CONT_JS_WORK_BENCH::iterator iter = m_kCont.begin();
	while( m_kCont.end() != iter )
	{
		(*iter).second.BlessDurationSec(iSec);
		++iter;
	}
	m_kContBlessGuid.erase(rkCharGuid);
}

bool PgJobSkillWorkBenchMgr::SetCurUpgradeSlotRemainTime(BM::GUID const& rkItemGuid, __int64 const iSec)
{
	CONT_JS_WORK_BENCH::iterator find_iter = m_kCont.find( rkItemGuid );
	if( m_kCont.end() != find_iter )
	{
		PgJobSkillWorkBench& rkWorkBench = (*find_iter).second;
		rkWorkBench.SetCurUpgradeSlotRemainTime(iSec);
		return true;
	}
	return false;
}

bool PgJobSkillWorkBenchMgr::SetCompleteSlotRemainTime(BM::GUID const& rkItemGuid, size_t stSlot, __int64 const iSec)
{
	CONT_JS_WORK_BENCH::iterator find_iter = m_kCont.find( rkItemGuid );
	if( m_kCont.end() != find_iter )
	{
		PgJobSkillWorkBench& rkWorkBench = (*find_iter).second;
		return rkWorkBench.SetCompleteSlotRemainTime(stSlot, iSec);
	}
	return false;
}

bool PgJobSkillWorkBenchMgr::CheckMajorWorkBenchStatus()
{
	CONT_JS_WORK_BENCH::const_iterator find_iter = m_kCont.begin();
	EWorkBenchStatus eCurrentStatus = WBS_NONE;	//기본 대표상태는 아무것도 안함
	while(find_iter != m_kCont.end())
	{
		EWorkBenchStatus const eStatus = find_iter->second.GetMajorStatus();
		if( WBS_WORKING_TROUBLE == eStatus)
		{//1순위 체크 : 수리필요
			eCurrentStatus = eStatus;
			break;
		}
		if( WBS_WORKING == eStatus)
		{//2순위 체크 : 작업중
			eCurrentStatus = eStatus;
		}
		else if( WBS_WORKING_COMPLETE == eStatus)
		{//3순위 체크 : 작업 완료
			if( eCurrentStatus != WBS_WORKING_TROUBLE
				&& eCurrentStatus != WBS_WORKING )
			{
				eCurrentStatus = eStatus;
			}
		}
		++find_iter;
	}

	if(eCurrentStatus != m_kOldMajorStatus)
	{
		m_kOldMajorStatus = eCurrentStatus;
		return true;
	}

	return false;
}