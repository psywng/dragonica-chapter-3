#include "StdAfx.h"
#include "BM/LocalMgr.h"
#include "Lohengrin/packetstruct.h"
#include "Lohengrin/LogGroup.h"
#include "Lohengrin/ProcessConfig.h"
#include "item.h"
#include "PgStringUtil.h"
#include "TableDataManager.h"
#include "PgQuestInfo.h"
#include "PgDBCache.h"
#include "PgMyQuestUtil.h"

namespace DBCacheUtil
{
	//
	std::wstring const kDefaultNationCode( L"0" );
	int iForceNationCode = 0;

	//
	typedef std::list< BM::vstring > CONT_ERROR_MSG;
	Loki::Mutex kErrorMutex;
	CONT_ERROR_MSG kContErrorMsg;
	void AddErrorMsg(BM::vstring const& rkErrorMsg)
	{
		BM::CAutoMutex kLock(kErrorMutex);
		kContErrorMsg.push_back( rkErrorMsg );
	}
	bool DisplayErrorMsg()
	{
		BM::CAutoMutex kLock(kErrorMutex);

		CONT_ERROR_MSG kTemp;
		kContErrorMsg.swap(kTemp);

		CONT_ERROR_MSG::const_iterator iter = kTemp.begin();
		while( kTemp.end() != iter )
		{
			CAUTION_LOG(BM::LOG_LV1, (*iter));
			++iter;
		}
		return !kTemp.empty();
	}

	//
	std::wstring const kKeyPrifix( DBCACHE_KEY_PRIFIX );

	bool IsDefaultNation(std::wstring const& rkNationCodeStr)
	{
		return kDefaultNationCode == rkNationCodeStr;
	}

	struct ConvertPair
	{
		template< typename _T_LEFT, typename _T_RIGHT >
		ConvertPair(std::pair< _T_LEFT, _T_RIGHT > const& rhs)
		{
			m_kStr << L"(" << rhs.first << L", " << rhs.second << L")";
		}
		operator std::wstring const& () const
		{
			return m_kStr;
		}
		BM::vstring m_kStr;
	};
	struct ConvertTriple
	{
		template< typename _T_K1, typename _T_K2, typename _T_K3 >
		ConvertTriple(tagTripleKey< _T_K1, _T_K2, _T_K3 > const& rhs)
		{
			m_kStr << L"(K1:" << rhs.kPriKey << L", K2:" << rhs.kSecKey << L", K3:" << rhs.kTrdKey << L")";
		}
		operator std::wstring const& () const
		{
			return m_kStr;
		}
		BM::vstring m_kStr;
	};
	struct ConvertDefenceAddMonsterKey
	{
		ConvertDefenceAddMonsterKey(CONT_DEF_DEFENCE_ADD_MONSTER::key_type const& rhs)
		{
			m_kStr << L"Key(GroupNo:" << rhs.iAddMonster_GroupNo << L", SuccessCount:" << rhs.iSuccess_Count << L")";
		}
		operator std::wstring const& () const
		{
			return m_kStr;
		}
		BM::vstring m_kStr;
	};
	struct AddError
	{
		AddError()
		{
		}
		void operator()(BM::vstring const& rhs)
		{
			AddErrorMsg(rhs);
		}
	};
}

bool PgDBCache::m_bIsForTool = false;

PgDBCache::PgDBCache(void)
{
}

PgDBCache::~PgDBCache(void)
{
}

bool PgDBCache::Init()
{
	BM::CAutoMutex kLock(m_kMutex);
	g_kTblDataMgr.Clear(false);
	return true;
}

bool PgDBCache::DisplayErrorMsg()
{
	return DBCacheUtil::DisplayErrorMsg();
}
void PgDBCache::AddErrorMsg(BM::vstring const& rkErrorMsg)
{
	DBCacheUtil::AddErrorMsg(rkErrorMsg);
}

//Load
bool PgDBCache::Q_DQT_DEF_ABIL_TYPE( CEL::DB_RESULT &rkResult )
{//select [AbilNo], [Name] from dbo.TB_DefAbilType
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEF_ABIL_TYPE map;

	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEF_ABIL_TYPE::mapped_type element;

		(*itor).Pop( element.AbilNo );			++itor;
		(*itor).Pop( element.NameNo );			++itor;

		map.insert( std::make_pair(element.AbilNo, element) );
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}



/*
bool PgDBCache::Q_DQT_DEFAIPATTERN( CEL::DB_RESULT &rkResult )
{//	select [AINo] from dbo.TB_DefAIPattern
CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

CONT_DEFAIPATTERN map;

while( rkResult.vecArray.end() != itor )
{
CONT_DEFAIPATTERN::mapped_type element;

(*itor).Pop( element.AINo );	++itor;

map.insert( std::make_pair(element.AINo, element) );
}

if( map.size() )
{
g_kCoreCenter.ClearQueryResult(rkResult);
g_kTblDataMgr.SetContDef(map);
return true;
}

VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
return false;
}
*/

bool PgDBCache::Q_DQT_DEFCLASS( CEL::DB_RESULT &rkResult )
{//select [ClassNo], [Name], [ParentClassNo] from dbo.TB_DefClass
	CONT_DEFCLASS map;

	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();
	int index = 0;
	while( rkResult.vecArray.end() != itor )
	{
		TBL_DEF_CLASS element;

		(*itor).Pop( element.iClass );					++itor;
		(*itor).Pop( element.sLevel );					++itor;
		(*itor).Pop( element.i64Experience );			++itor;
		(*itor).Pop( element.sMaxHP );					++itor;
		(*itor).Pop( element.sHPRecoveryInterval );		++itor;
		(*itor).Pop( element.sHPRecovery );				++itor;
		(*itor).Pop( element.sMaxMP );					++itor;
		(*itor).Pop( element.sMPRecoveryInterval );		++itor;
		(*itor).Pop( element.sMPRecovery );				++itor;
		(*itor).Pop( element.iStr );					++itor;
		(*itor).Pop( element.iInt );					++itor;
		(*itor).Pop( element.iCon );					++itor;
		(*itor).Pop( element.iDex );					++itor;
		(*itor).Pop( element.sMoveSpeed );				++itor;
		for (int i=0; i<MAX_ITEM_ABIL_LIST; i++)
		{
			(*itor).Pop( element.aiAbil[i] );			++itor;
		}
		(*itor).Pop( element.sBonusStatus );			++itor;
		(*itor).Pop( element.sLimitStr );				++itor;
		(*itor).Pop( element.sLimitInt );				++itor;
		(*itor).Pop( element.sLimitCon );				++itor;
		(*itor).Pop( element.sLimitDex );				++itor;

		map.insert(std::make_pair(index++,element));
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}
	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFCLASS_ABIL(CEL::DB_RESULT& rkResult)
{
	CONT_DEFCLASS_ABIL map;

	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();
	while( rkResult.vecArray.end() != itor )
	{
		TBL_DEF_CLASS_ABIL element;

		(*itor).Pop( element.iAbilNo );	++itor;
		for (int i=0; i<MAX_CLASS_ABIL_ARRAY; i++)
		{
			(*itor).Pop( element.iType[i] );++itor;
			(*itor).Pop( element.iValue[i] );++itor;
		}
		map.insert(std::make_pair(element.iAbilNo, element));
	}

	if(map.size())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}
	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFCLASS_PET( CEL::DB_RESULT &rkResult )
{//select [ClassNo], [Name], [ParentClassNo] from dbo.TB_DefClass
	CONT_DEFCLASS_PET map;

	CEL::DB_DATA_ARRAY::const_iterator itr = rkResult.vecArray.begin();

	while( rkResult.vecArray.end() != itr )
	{
		CONT_DEFCLASS_PET::mapped_type element;
		itr->Pop( element.iClass );					++itr;
		itr->Pop( element.iClassName );				++itr;
		itr->Pop( element.byPetType );				++itr;
		itr->Pop( element.iLevelIndex );			++itr;
		itr->Pop( element.iSkillIndex );			++itr;
		itr->Pop( element.iItemOptionIndex );		++itr;
		itr->Pop( element.iDefaultHair );			++itr;
		itr->Pop( element.iDefaultFace );			++itr;
		itr->Pop( element.iDefaultBody );			++itr;

		map.insert( std::make_pair( element.iClass, element ) );
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(map);
	return true;
}

bool PgDBCache::Q_DQT_DEFCLASS_PET_LEVEL( CEL::DB_RESULT &rkResult )
{
	CONT_DEFCLASS_PET_LEVEL map;

	CEL::DB_DATA_ARRAY::const_iterator itr = rkResult.vecArray.begin();

	while( rkResult.vecArray.end() != itr )
	{
		CONT_DEFCLASS_PET_LEVEL::key_type		kKey;
		CONT_DEFCLASS_PET_LEVEL::mapped_type	element;
		itr->Pop( kKey.iClass );					++itr;
		itr->Pop( element.sLevel );					++itr;
		itr->Pop( element.i64Experience );			++itr;
		itr->Pop( element.iTimeExperience );		++itr;
		itr->Pop( element.sMaxMP );					++itr;
		itr->Pop( element.sMPRecoveryInterval );	++itr;
		itr->Pop( element.sMPRecovery );			++itr;

		for ( int i = 0 ; i < MAX_CLASS_ABIL_LIST ; ++i )
		{
			itr->Pop( element.aiAbil[i] );			++itr;
		}

		kKey.nLv = element.sLevel;
		map.insert( std::make_pair( kKey, element ) );
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(map);
	return true;
}

bool PgDBCache::Q_DQT_DEFCLASS_PET_SKILL(CEL::DB_RESULT& rkResult)
{
	CONT_DEFCLASS_PET_SKILL map;

	CEL::DB_DATA_ARRAY::const_iterator itr = rkResult.vecArray.begin();
	while( rkResult.vecArray.end() != itr )
	{
		CONT_DEFCLASS_PET_SKILL::key_type					kKey;
		CONT_DEFCLASS_PET_SKILL::mapped_type::value_type	kElement;

		itr->Pop( kKey );							++itr;
		itr->Pop( kElement.nLevel );				++itr;
		for ( size_t i = 0 ; i < MAX_PET_SKILLCOUNT ; ++i )
		{
			itr->Pop( kElement.iSkillNo[i] );		++itr;
		}
		itr->Pop( kElement.iSkillUseTimeBit );		++itr;

		CONT_DEFCLASS_PET_SKILL::iterator map_itr = map.find( kKey );
		if ( map_itr == map.end() )
		{
			CONT_DEFCLASS_PET_SKILL::_Pairib kPair = map.insert( std::make_pair( kKey, CONT_DEFCLASS_PET_SKILL::mapped_type() ) );
			map_itr = kPair.first;
		}

		map_itr->second.insert( kElement );
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(map);
	return true;
}

bool PgDBCache::Q_DQT_DEFCLASS_PET_ITEMOPTION(CEL::DB_RESULT& rkResult)
{
	CONT_DEFCLASS_PET_ITEMOPTION map;

	CEL::DB_DATA_ARRAY::const_iterator itr = rkResult.vecArray.begin();
	while( rkResult.vecArray.end() != itr )
	{
		CONT_DEFCLASS_PET_ITEMOPTION::key_type		kKey;
		CONT_DEFCLASS_PET_ITEMOPTION::mapped_type	element;

		itr->Pop( kKey.iClass );					++itr;
		itr->Pop( kKey.nLv );						++itr;
		for ( size_t i = 0 ; i < MAX_PET_ITEMOPTION_COUNT ; ++i )
		{
			itr->Pop( element.iOptionType[i] );		++itr;
			itr->Pop( element.iOptionLevel[i] );	++itr;
			if ( !element.iOptionType[i] || !element.iOptionLevel[i] )
			{// 둘중에 하나라도 없으면 둘다 없는거
				element.iOptionType[i] = element.iOptionLevel[i] = 0;
			}
		}

		map.insert( std::make_pair( kKey, element ) );
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(map);
	return true;
}

bool PgDBCache::Q_DQT_DEFCLASS_PET_ABIL(CEL::DB_RESULT& rkResult)
{
	CONT_DEFCLASS_PET_ABIL map;

	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();
	while( rkResult.vecArray.end() != itor )
	{
		TBL_DEF_CLASS_PET_ABIL element;

		(*itor).Pop( element.iAbilNo );	++itor;
		for (int i=0; i<MAX_CLASS_ABIL_ARRAY; i++)
		{
			(*itor).Pop( element.iType[i] );++itor;
			(*itor).Pop( element.iValue[i] );++itor;
		}
		map.insert(std::make_pair(element.iAbilNo, element));
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(map);
	return true;
}

bool PgDBCache::Q_DQT_DEFITEM( CEL::DB_RESULT &rkResult )
{
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	std::wstring kNationCodeStr;
	DBCacheUtil::PgNationCodeHelper< CONT_DEFITEM::key_type, CONT_DEFITEM::mapped_type, CONT_DEFITEM > kNationCodeUtil( L"Duplicate ItemNo[" DBCACHE_KEY_PRIFIX L"]" );
	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEFITEM::mapped_type element;
		BYTE cIsCantShareRental = 0;

		(*itor).Pop( kNationCodeStr );	++itor;
		(*itor).Pop( element.ItemNo );	++itor;
		(*itor).Pop( element.NameNo );	++itor;
		(*itor).Pop( element.ResNo );	++itor;
		(*itor).Pop( element.sType );	++itor;
		(*itor).Pop( element.iPrice );	++itor;
		(*itor).Pop( element.iSellPrice );	++itor;
		(*itor).Pop( element.iAttribute );	++itor;

		(*itor).Pop( element.byGender );	++itor;
		(*itor).Pop( element.sLevel );	++itor;
		(*itor).Pop( element.i64ClassLimit );	++itor;
		(*itor).Pop( element.i64ClassLimitDisplayFilter );	++itor;
		//(*itor).Pop( element.i64DraClassLimit );	++itor;
		//(*itor).Pop( element.i64DraClassLimitDisplayFilter );	++itor;

		for(int i = 0; MAX_ITEM_ABIL_LIST > i ; i++)
		{
			(*itor).Pop( element.aAbil[i] );		++itor;
		}
		
		(*itor).Pop( element.iOrder1 );	++itor;
		(*itor).Pop( element.iOrder2 );	++itor;
		(*itor).Pop( element.iOrder3 );	++itor;
		(*itor).Pop( element.iCostumeGrade );	++itor;
		// 확장 설정
		(*itor).Pop( cIsCantShareRental ); 	++itor; if( 0 != cIsCantShareRental ) { element.iAttribute |= ICMET_Cant_UseShareRental; } // 계정 창고 사용 불가 옵션

		kNationCodeUtil.Add(kNationCodeStr, element.ItemNo, element, __FUNCTIONW__, __LINE__);
	}

	if( !kNationCodeUtil.IsEmpty() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kNationCodeUtil.GetResult());
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFITEM_RES_CONVERT( CEL::DB_RESULT &rkResult )
{
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEF_ITEM_RES_CONVERT map;

	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEF_ITEM_RES_CONVERT::mapped_type element;

		(*itor).Pop( element.ItemNo );	++itor;
		(*itor).Pop( element.NationCode );	++itor;
		(*itor).Pop( element.ResNo );	++itor;
		if( g_kLocal.IsServiceRegion(element.NationCode) )
		{
			map.insert( std::make_pair(element.ItemNo, element) );
		}
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFITEMABIL( CEL::DB_RESULT &rkResult )
{//	select [ItemAbilNo], [Type01], [Value01], [Type02], [Value02], [Type03], [Value03], [Type04], [Value04], [Type05], [Value05], [Type06], [Value06], [Type07], [Value07], [Type08], [Value08], [Type09], [Value09], [Type10], [Value10] from dbo.TB_DefItemAbil
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	std::wstring kNationCodeStr;
	DBCacheUtil::PgNationCodeHelper< CONT_DEFITEMABIL::key_type, CONT_DEFITEMABIL::mapped_type, CONT_DEFITEMABIL > kNationCodeUtil( L"Duplicate AbilNo[" DBCACHE_KEY_PRIFIX L"]" );
	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEFITEMABIL::mapped_type element;

		(*itor).Pop( kNationCodeStr );		++itor;
		(*itor).Pop( element.ItemAbilNo );	++itor;

		for(int i = 0; MAX_ITEM_ABIL_ARRAY > i ; i++)
		{
			(*itor).Pop( element.aType[i] );		++itor;
			(*itor).Pop( element.aValue[i] );		++itor;
		}

		kNationCodeUtil.Add( kNationCodeStr, element.ItemAbilNo, element, __FUNCTIONW__, __LINE__ );
	}

	if( !kNationCodeUtil.IsEmpty() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kNationCodeUtil.GetResult());
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFITEMRARE( CEL::DB_RESULT &rkResult )
{//select [RareNo], [Name] from dbo.TB_DefItemRare
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEFITEMRARE map;

	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEFITEMRARE::mapped_type element;

		(*itor).Pop( element.RareNo );	++itor;
		(*itor).Pop( element.NameNo );	++itor;

		map.insert( std::make_pair(element.RareNo, element) );
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFITEMRAREGROUP( CEL::DB_RESULT &rkResult )
{//select [GroupNo], [Name], [RareNo01], [RareNo02], [RareNo03], [RareNo04], [RareNo05], [RareNo06], [RareNo07], [RareNo08], [RareNo09], [RareNo10] from dbo.TB_DefItemRareGroup
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEFITEMRAREGROUP map;

	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEFITEMRAREGROUP::mapped_type element;

		(*itor).Pop( element.GroupNo );	++itor;
		(*itor).Pop( element.NameNo );	++itor;

		for(int i = 0; MAX_ITEM_RARE_KIND_ARRAY > i ; i++)
		{
			(*itor).Pop( element.aRareNo[i] );		++itor;
		}

		map.insert( std::make_pair(element.GroupNo, element) );
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFMAP( CEL::DB_RESULT &rkResult )
{
	if( CEL::DR_SUCCESS != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__ << _T("Cannot Load DefMap SP=") << rkResult.Command());
		return false;
	}

	std::set< int > kChkKey;
	std::set< int > kErrorKeyList;
	CONT_DEFMAP map;
	CONT_DEFMAP_ABIL map_abil;
	CONT_TOWN2GROUND kContTown2Ground;
	typedef std::vector<int> CONT_INT;
	CONT_INT kVecHiddenIndex;
	kVecHiddenIndex.clear();

	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();
	CEL::DB_RESULT_COUNT::const_iterator count_itor = rkResult.vecResultCount.begin();

	size_t const iMapCount = (*count_itor);	++count_itor;

	for (size_t i=0; i<iMapCount; i++)
	{
		CONT_DEFMAP::mapped_type element;
		int _iMissionNo = 0;

		(*itor).Pop(element.iMapNo);		++itor;
		(*itor).Pop(element.iKey);			++itor;
		(*itor).Pop(element.iAttr);			++itor;
		(*itor).Pop(element.NameNo);		++itor;
		(*itor).Pop(_iMissionNo);			++itor;
		(*itor).Pop(element.strXMLPath);	++itor;
		(*itor).Pop(element.sZoneCX);		++itor;
		(*itor).Pop(element.sZoneCY);		++itor;
		(*itor).Pop(element.sZoneCZ);		++itor;
		(*itor).Pop(element.sContinent);	++itor;
		(*itor).Pop(element.sHometownNo);	++itor;
		(*itor).Pop(element.sHiddenIndex);	++itor;

		if(0 < element.sHometownNo)
		{
			if(false == kContTown2Ground.insert(std::make_pair(element.sHometownNo,element.iMapNo)).second)
			{
				VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__<<L"TABLE [TB_DefMap] HometownNo Error !! MapNo["<<element.iMapNo<<L"]");
			}
		}

		for (int iAbilCount = 0; iAbilCount<MAX_MAP_ABIL_COUNT; iAbilCount++)
		{
			(*itor).Pop(element.iAbil[iAbilCount]);	++itor;
		}

		CONT_DEFMAP::_Pairib kPair = map.insert( std::make_pair(element.iMapNo, element) );
		if ( _iMissionNo )
		{
			CONT_MISSION_NO &kContMissionNo = kPair.first->second.kContMissionNo;
			CONT_MISSION_NO::iterator itr = std::find( kContMissionNo.begin(), kContMissionNo.end(), _iMissionNo);
			if ( itr == kContMissionNo.end() )
			{
				kContMissionNo.push_back(_iMissionNo);
			}
			else
			{
				VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__<<L"TABLE [TB_DefMapToMission] No Unique MapNo["<<element.iMapNo<<L"] And MissionNo["<<_iMissionNo<<L"]");
			}
		}
		if ( element.iKey )
		{
			std::set< int >::_Pairib kChkKey_Pair = kChkKey.insert( element.iKey );
			if ( !kChkKey_Pair.second )
			{
				kErrorKeyList.insert( element.iKey );
			}
		}

		int iHiddenIndex = static_cast<int>(element.sHiddenIndex);

		if( 0 != iHiddenIndex )
		{
			if( kVecHiddenIndex.end() == std::find(kVecHiddenIndex.begin(), kVecHiddenIndex.end(), iHiddenIndex) )
			{
				std::back_inserter(kVecHiddenIndex) = iHiddenIndex;
			}
			else
			{
				VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("TABLE [TB_DefMap] HiddenIndex No Unique! Index[") << iHiddenIndex << _T("]"));
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
				return false;
			}
		}
	}

	size_t const iMapAbilCount = (*count_itor);	++count_itor;
	for (size_t j=0; j<iMapAbilCount; j++)
	{
		CONT_DEFMAP_ABIL::mapped_type element;
		(*itor).Pop(element.iAbilNo);		++itor;
		for (int k=0; k<MAX_MAP_ABIL; k++)
		{
			(*itor).Pop(element.iType[k]);	++itor;
			(*itor).Pop(element.iValue[k]);	++itor;
		}
		map_abil.insert(std::make_pair(element.iAbilNo, element));
	}

	if ( kErrorKeyList.size() )
	{
		VERIFY_INFO_LOG( false, BM::LOG_LV4, __FL__ << L"Overlapping CONT_DEFMAP's Map KEY!!! Next List " );

		std::set< int >::const_iterator error_itr = kErrorKeyList.begin();
		for ( ; error_itr != kErrorKeyList.end() ; ++error_itr )
		{
			VERIFY_INFO_LOG( false, BM::LOG_LV4, __FL__ << L" KEY : " << *error_itr );
		}
	}
	else if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		g_kTblDataMgr.SetContDef(kContTown2Ground);
		g_kTblDataMgr.SetContDef(map_abil);
		return true;
	}	

	//VERIFY_INFO_LOG(false, BM::LOG_LV0, _T("[%s]-[%d] row count 0"), __FUNCTIONW__, __LINE__);
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}


bool PgDBCache::Q_DQT_DEFMAPITEM( CEL::DB_RESULT &rkResult )
{//select [PosID], [MapNo], [ItemNo], [PosX], [PosY], [PosZ] from dbo.TB_DefMapItem
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEFMAPITEM map;

	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEFMAPITEM::mapped_type element;

		(*itor).Pop( element.PosID );	++itor;
		(*itor).Pop( element.MapNo );	++itor;
		(*itor).Pop( element.ItemNo );	++itor;

		int x=0,y=0,z=0;
		(*itor).Pop( x );	++itor;
		(*itor).Pop( y );	++itor;
		(*itor).Pop( y );	++itor;

		element.ptPos.x = (float)x;
		element.ptPos.y = (float)y;
		element.ptPos.z = (float)z;

		map.insert( std::make_pair(element.PosID, element) );
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFMAPMONSTERREGEN( CEL::DB_RESULT &rkResult )
{//SELECT     [index], PosGUID, MapNo, MonBagNo, RegenPeriod, PosX, PosY, PosZ FROM TB_DefMapMonsterRegen
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEF_MAP_REGEN_POINT map;

	int index = 0;
	while(rkResult.vecArray.end() != itor)
	{
		CONT_DEF_MAP_REGEN_POINT::mapped_type element;

		(*itor).Pop( element.iMapNo );			++itor;
		(*itor).Pop( element.cBagControlType );	++itor;
		(*itor).Pop( element.iBagControlNo );	++itor;
		(*itor).Pop( element.kPosGuid );		++itor;
		(*itor).Pop( element.iPointGroup);		++itor;
		(*itor).Pop( element.dwPeriod );		++itor;

		double x=0,y=0,z=0;
		(*itor).Pop( x );	++itor;
		(*itor).Pop( y );	++itor;
		(*itor).Pop( z );	++itor;

		element.pt3Pos.x = (float)x;
		element.pt3Pos.y = (float)y;
		element.pt3Pos.z = (float)z;

		(*itor).Pop(element.iMoveRange); ++itor;
		(*itor).Pop(element.cDirection); ++itor;

		map.insert(std::make_pair(index++,element));
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFMONSTER( CEL::DB_RESULT &rkResult )
{//select [MonsterNo], [Name], [Abil01], [Abil02], [Abil03], [Abil04], [Abil05], [Abil06], [Abil07], [Abil08], [Abil09], [Abil10] from dbo.TB_DefMonster
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEFMONSTER map;

	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEFMONSTER::mapped_type element;

		(*itor).Pop( element.MonsterNo );	++itor;
		(*itor).Pop( element.Name );	++itor;

		for(int i = 0; MAX_MONSTER_ABIL_LIST> i ; i++)
		{
			(*itor).Pop( element.aAbil[i] );		++itor;
		}

		map.insert( std::make_pair(element.MonsterNo, element) );
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFMONSTERABIL( CEL::DB_RESULT &rkResult )
{//select [MonsterAbilNo], [Type01], [Value01], [Type02], [Value02], [Type03], [Value03], [Type04], [Value04], [Type05], [Value05], [Type06], [Value06], [Type07], [Value07], [Type08], [Value08], [Type09], [Value09], [Type10], [Value10] from dbo.TB_DefMonsterAbil
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	std::wstring kNationCodeStr;
	DBCacheUtil::PgNationCodeHelper< CONT_DEFMONSTERABIL::key_type, CONT_DEFMONSTERABIL::mapped_type, CONT_DEFMONSTERABIL > kNationCodeUtil( L"duplicate MonsterAbilNo[" DBCACHE_KEY_PRIFIX L"], in [TB_DefMonsterAbil, TB_DefMonsterAbilTunning]" );
	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEFMONSTERABIL::mapped_type element;

		(*itor).Pop( kNationCodeStr );			++itor;
		(*itor).Pop( element.MonsterAbilNo );	++itor;
		for(int i = 0; MAX_MONSTER_ABIL_ARRAY > i ; i++)
		{
			(*itor).Pop( element.aType[i] );		++itor;
			(*itor).Pop( element.aValue[i] );		++itor;
		}

		kNationCodeUtil.Add(kNationCodeStr, element.MonsterAbilNo, element, __FUNCTIONW__, __LINE__);
	}

	if( !kNationCodeUtil.IsEmpty() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kNationCodeUtil.GetResult());
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFMONSTERTUNNING( CEL::DB_RESULT &rkResult )
{//select [KeyNo], [Grade], [Level], [Type01], [Value01], [Type02], [Value02], [Type03], [Value03], [Type04], [Value04], [Type05], [Value05], [Type06], [Value06], [Type07], [Value07], [Type08], [Value08], [Type09], [Value09], [Type10], [Value10] from dbo.TB_DefMonsterTunningAbil
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEFMONSTERTUNNINGABIL map;

	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEFMONSTERTUNNINGABIL::mapped_type element;

		(*itor).Pop( element.kKey.iNo );	++itor;
		(*itor).Pop( element.kKey.iGrade );	++itor;
		(*itor).Pop( element.kKey.iLevel );	++itor;
		for(int i = 0; MAX_MONSTER_ABIL_ARRAY > i ; i++)
		{
			(*itor).Pop( element.iAbil[i] );		++itor;
		}

		map.insert( std::make_pair(element.kKey, element) );
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFNPC( CEL::DB_RESULT &rkResult )
{//	select [NPCNo], [Name], [Abil01], [Abil02], [Abil03], [Abil04], [Abil05], [Abil06], [Abil07], [Abil08], [Abil09], [Abil10]  from dbo.TB_DefNPC
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEFNPC map;

	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEFNPC::mapped_type element;

		(*itor).Pop( element.NPCNo );	++itor;
		(*itor).Pop( element.Name );	++itor;

		for(int i = 0; MAX_NPC_ABIL_LIST> i ; i++)
		{
			(*itor).Pop( element.aAbil[i] );		++itor;
		}

		map.insert( std::make_pair(element.NPCNo, element) );
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFNPCABIL( CEL::DB_RESULT &rkResult )
{//select [NPCAbilNo], [Type01], [Value01], [Type02], [Value02], [Type03], [Value03], [Type04], [Value04], [Type05], [Value05], [Type06], [Value06], [Type07], [Value07], [Type08], [Value08], [Type09], [Value09], [Type10], [Value10] from dbo.TB_DefNPCAbil
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEFNPCABIL map;

	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEFNPCABIL::mapped_type element;

		(*itor).Pop( element.NPCAbilNo );	++itor;
		for(int i = 0; MAX_NPC_ABIL_ARRAY > i ; i++)
		{
			(*itor).Pop( element.aType[i] );		++itor;
			(*itor).Pop( element.aValue[i] );		++itor;
		}

		map.insert( std::make_pair(element.NPCAbilNo, element) );
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;

}





bool PgDBCache::Q_DQT_DEFSKILL( CEL::DB_RESULT &rkResult )
{
	/*
	SELECT [SkillNo] ,[Level] ,[NameNo] ,[RscNameNo] ,[ActionName]
	,[Memo] ,[Type] ,[ClassLimit] ,[LevelLimit] ,[WeaponLimit]
	,[StateLimit] ,[ParentSkill] ,[Target] ,[Range] ,[CastTime]
	,[CoolTime] ,[AnimationTime] ,[MP] ,[HP] ,[EffectID]
	,[Abil01] ,[Abil02] ,[Abil03] ,[Abil04] ,[Abil05]
	,[Abil06] ,[Abil07] ,[Abil08] ,[Abil09] ,[Abil10]
	FROM [DR2_Def].[dbo].[TB_DefSkill2]
	*/
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEFSKILL map;

	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEFSKILL::mapped_type element;
		std::wstring kActionName = _T("");

		(*itor).Pop( element.SkillNo );	++itor;
		(*itor).Pop( element.Lv );	++itor;
		(*itor).Pop( element.NameNo );	++itor;
		(*itor).Pop( element.RscNameNo );	++itor;
		//(*itor).Pop( element.iActionCategoryNo );	++itor;
		(*itor).Pop( kActionName ); ++itor;
		ZeroMemory(element.chActionName,sizeof(TCHAR) * 30 );
		wsprintf(element.chActionName,kActionName.c_str());
		(*itor).Pop( element.byType );	++itor;
		(*itor).Pop( element.i64ClassLimit );	++itor;
		//(*itor).Pop( element.i64DraClassLimit );	++itor;
		(*itor).Pop( element.sLevelLimit );	++itor;
		(*itor).Pop( element.iWeaponLimit );	++itor;
		(*itor).Pop( element.iStateLimit );	++itor;
		(*itor).Pop( element.iParentSkill );	++itor;
		(*itor).Pop( element.iTarget );	++itor;
		//(*itor).Pop( element.bySelect );	++itor;
		(*itor).Pop( element.sRange );	++itor;
		(*itor).Pop( element.sCastTime );	++itor;
		(*itor).Pop( element.sCoolTime );	++itor;
		(*itor).Pop( element.iAnimationTime); ++itor;
		//(*itor).Pop( element.sHate );	++itor;
		(*itor).Pop( element.sMP );	++itor;
		(*itor).Pop( element.sHP );	++itor;
		//(*itor).Pop( element.iNeedSkill );	++itor;
		//(*itor).Pop( element.iToggleSkill );	++itor;
		//(*itor).Pop( element.sHitorate );	++itor;
		(*itor).Pop( element.iEffectID );	++itor;
		//(*itor).Pop( element.iEventID );	++itor;
		(*itor).Pop( element.iCmdStringNo);	++itor;

		for(int i = 0; MAX_SKILL_ABIL_LIST> i ; i++)
		{
			(*itor).Pop( element.aAbil[i] );		++itor;
		}

		CONT_DEFSKILL::_Pairib kRet = map.insert( std::make_pair(element.SkillNo, element) );
		if(!kRet.second)
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"Skill ["<<element.SkillNo<<L"] is dupulication");
		}
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFSKILLABIL( CEL::DB_RESULT &rkResult )
{//select [SkillAbilID], [Type01], [Value01], [Type02], [Value02], [Type03], [Value03], [Type04], [Value04], [Type05], [Value05], [Type06], [Value06], [Type07], [Value07], [Type08], [Value08], [Type09], [Value09], [Type10], [Value10] from dbo.TB_DefSkillAbil
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEFSKILLABIL map;

	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEFSKILLABIL::mapped_type element;

		(*itor).Pop( element.SkillAbilNo );	++itor;
		for(int i = 0; MAX_NPC_ABIL_ARRAY > i ; i++)
		{
			(*itor).Pop( element.aType[i] );		++itor;
			(*itor).Pop( element.aValue[i] );		++itor;
		}

		CONT_DEFSKILLABIL::_Pairib kRet = map.insert( std::make_pair(element.SkillAbilNo, element) );
		if(!kRet.second)
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"Skill abil ["<<element.SkillAbilNo<<L"] is duplication");
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Insert Failed Data"));
		}
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFSKILLSET( CEL::DB_RESULT &rkResult )
{//select [SetNo],[Limit],[ConditionType],[ConditionValue],[ResNo] from dbo.TB_DefSkillSet
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEFSKILLSET map;

	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEFSKILLSET::mapped_type element;

		(*itor).Pop( element.bySetNo );				++itor;
		(*itor).Pop( element.byLimit );				++itor;
		(*itor).Pop( element.byConditionType );		++itor;
		(*itor).Pop( element.byConditionValue );	++itor;
		(*itor).Pop( element.iResNo );				++itor;

		CONT_DEFSKILLSET::_Pairib kRet = map.insert( std::make_pair(element.bySetNo, element) );
		if(!kRet.second)
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"Skill set ["<<element.bySetNo<<L"] is duplication");
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Insert Failed Data"));
		}
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFRES( CEL::DB_RESULT &rkResult )
{
	//	SELECT [ResNo], [IconPath], [XmlPath] FROM [TB_DefRes]
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEFRES map;

	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEFRES::mapped_type element;

		(*itor).Pop( element.ResNo );	++itor;

		(*itor).Pop( element.U );	++itor;
		(*itor).Pop( element.V );	++itor;
		(*itor).Pop( element.UVIndex );	++itor;

		(*itor).Pop( element.strIconPath );	++itor;
		(*itor).Pop( element.strXmlPath );	++itor;

		map.insert( std::make_pair(element.ResNo, element) );
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_LOAD_DEF_CHANNEL_EFFECT( CEL::DB_RESULT &rkResult )
{
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEF_CHANNEL_EFFECT map;

	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEF_CHANNEL_EFFECT::mapped_type element;

		(*itor).Pop( element.iEffectNo );	++itor;
		(*itor).Pop( element.sRealm );		++itor;
		(*itor).Pop( element.sChannel );	++itor;
		(*itor).Pop( element.sMinLv );		++itor;
		(*itor).Pop( element.sMaxLv );		++itor;

		CONT_DEF_CHANNEL_EFFECT::_Pairib ibRet = map.insert( std::make_pair( std::pair<short,short>( element.sRealm, element.sChannel ), element ) );
		ASSERT_LOG( ibRet.second, BM::LOG_LV4, __FL__<<L"ChannelEffect information duplicate Realm["<<element.sRealm<<L"], Channel["<<element.sChannel<<L"]");
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFSTRINGS( CEL::DB_RESULT &rkResult )
{//select [StringNo], [Text], [TextEng] from dbo.TB_DefStrings
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEFSTRINGS map;

	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEFSTRINGS::mapped_type element;

		(*itor).Pop( element.StringNo );	++itor;
		(*itor).Pop( element.strText );			++itor;
		(*itor).Pop( element.strTextEng );			++itor;

		CONT_DEFSTRINGS::_Pairib ibRet = map.insert( std::make_pair(element.StringNo, element) );
		ASSERT_LOG(ibRet.second, BM::LOG_LV4, __FL__<<L"DefString Duplicated StringNo["<<element.StringNo<<L"]");
		//if ( !ibRet.second )
		//{
		//	INFO_LOG(BM::LOG_LV4, _T("[%s] DefString Duplicated StringNo[%d]"), __FUNCTIONW__, element.StringNo);
		//}
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}


bool PgDBCache::Q_DQT_DEFEFFECT(CEL::DB_RESULT& rkResult )
{
	CONT_DEFEFFECT map;

	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();
	while( rkResult.vecArray.end() != itor )
	{
		TBL_DEF_EFFECT element;

		(*itor).Pop( element.iEffectID );	++itor;
		(*itor).Pop( element.iName );		++itor;
		(*itor).Pop( element.strActionName );++itor;

		(*itor).Pop( element.sType );	++itor;
		(*itor).Pop( element.sInterval );	++itor;
		(*itor).Pop( element.iDurationTime );	++itor;
		(*itor).Pop( element.byToggle ); ++itor;

		for (int i=0; i<MAX_EFFECT_ABIL_LIST; i++)
		{
			(*itor).Pop( element.aiAbil[i] );++itor;
		}
		map.insert(std::make_pair(element.iEffectID, element));
	}

	if(map.size())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}
	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFEFFECTABIL(CEL::DB_RESULT& rkResult )
{
	CONT_DEFEFFECTABIL map;

	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();
	while( rkResult.vecArray.end() != itor )
	{
		TBL_DEF_EFFECTABIL element;

		(*itor).Pop( element.iAbilNo );	++itor;
		for (int i=0; i<MAX_EFFECT_ABIL_ARRAY; i++)
		{
			(*itor).Pop( element.iType[i] );++itor;
			(*itor).Pop( element.iValue[i] );++itor;
		}
		map.insert(std::make_pair(element.iAbilNo, element));
	}

	if(map.size())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;	
	}
	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFUPGRADECLASS(CEL::DB_RESULT& rkResult )
{
	CONT_DEFUPGRADECLASS map;

	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();
	while( rkResult.vecArray.end() != itor )
	{
		TBL_DEF_EFFGRADECLASS element;

		(*itor).Pop( element.iClass ); ++itor;
		(*itor).Pop( element.iParentClass ); ++itor;
		(*itor).Pop( element.byGrade ); ++itor;
		(*itor).Pop( element.sMinLevel ); ++itor;
		(*itor).Pop( element.sMaxLevel ); ++itor;
		(*itor).Pop( element.i64Bitmask ); ++itor;
		(*itor).Pop( element.byKind ); ++itor;

		map.insert(std::make_pair(element.iClass, element));
	}

	if(map.size())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}
	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

/*	
[dbo].[UP_LoadDefItemEnchant]

select [TypeNo], [Level], [NameNo], 
[Abil01], [Abil02], [Abil03], [Abil04], [Abil05], 
[Abil06], [Abil07], [Abil08], [Abil09], [Abil10]
from [dbo].[TB_DefItemEnchant]
*/
bool PgDBCache::Q_DQT_DEFITEMENCHANT( CEL::DB_RESULT& rkResult)
{//	select [ItemAbilNo], [Type01], [Value01], [Type02], [Value02], [Type03], [Value03], [Type04], [Value04], [Type05], [Value05], [Type06], [Value06], [Type07], [Value07], [Type08], [Value08], [Type09], [Value09], [Type10], [Value10] from dbo.TB_DefItemAbil
	
	if ( 3 != rkResult.vecResultCount.size() )
	{
		VERIFY_INFO_LOG( false, BM::LOG_LV4, __FL__ << L"Error ResultCount<" << rkResult.vecResultCount.size() << L">" );
		return false;
	}

	int const iMaxType = 127;
	bool bRet = true;
	
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();
	CEL::DB_RESULT_COUNT::const_iterator count_iter = rkResult.vecResultCount.begin();

	CONT_DEFITEMENCHANT map;

	{// Default
		int const iEnchantCount = (*count_iter);	++count_iter;
		for(int i = 0;i < iEnchantCount;++i)
		{
			CONT_DEFITEMENCHANT::mapped_type element;

			(*itor).Pop( element.Type );				++itor;
			(*itor).Pop( element.Lv );					++itor;
			(*itor).Pop( element.NameNo );				++itor;

			for(int i = 0; MAX_ITEM_ENCHANT_ABIL_LIST > i ; i++)
			{
				(*itor).Pop( element.aAbil[i] );		++itor;
			}

			if ( 0 > element.Type || iMaxType < element.Type )
			{
				VERIFY_INFO_LOG( false, BM::LOG_LV4, __FL__ << L"Type<" << element.Type << L">" );
				bRet = false;
			}

			map.insert( std::make_pair(element.Key(), element) );
		}
	}

	{// Pet Enchant
		int const iEnchantCount = (*count_iter);	++count_iter;
		for(int i = 0;i < iEnchantCount;++i)
		{
			CONT_DEFITEMENCHANT::mapped_type element;

			(*itor).Pop( element.Type );				++itor;
			(*itor).Pop( element.Lv );					++itor;
			(*itor).Pop( element.NameNo );				++itor;

			for(int i = 0; MAX_ITEM_ENCHANT_ABIL_LIST > i ; i++)
			{
				(*itor).Pop( element.aAbil[i] );		++itor;
			}

			if ( 0 > element.Type || iMaxType < element.Type )
			{
				VERIFY_INFO_LOG( false, BM::LOG_LV4, __FL__ << L"Type<" << element.Type << L">" );
				bRet = false;
			}

			element.Type += SItemEnchantKey::ms_iPetBaseType;
			map.insert( std::make_pair(element.Key(), element) );
		}
	}

	CONT_DEFITEMENCHANTABILWEIGHT kWeightMap;

	int const iWeightCount = (*count_iter);		++count_iter;

	for(int i = 0;i < iWeightCount;++i)
	{
		CONT_DEFITEMENCHANTABILWEIGHT::key_type kKey;
		CONT_DEFITEMENCHANTABILWEIGHT::mapped_type kValue;
		(*itor).Pop( kKey );					++itor;
		(*itor).Pop( kValue );					++itor;
		kWeightMap.insert(std::make_pair(kKey,kValue));
	}

	g_kCoreCenter.ClearQueryResult(rkResult);

	if( true == bRet )
	{	
		g_kTblDataMgr.SetContDef(map);
		g_kTblDataMgr.SetContDef(kWeightMap);
		return true;
	}

	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFCHARACTER_BASEWEAR(CEL::DB_RESULT& rkResult)
{
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEFCHARACTER_BASEWEAR map;
	while(rkResult.vecArray.end() != itor)
	{
		CONT_DEFCHARACTER_BASEWEAR::mapped_type kElement;	

		std::wstring wstrIconPath;

		(*itor).Pop(kElement.iWearNo);		++itor;
		(*itor).Pop(kElement.iWearType);	++itor;
		(*itor).Pop(kElement.iSetNo);		++itor;
		(*itor).Pop(kElement.iClassNo);		++itor;
		(*itor).Pop(kElement.strIconPath);			++itor;

		CONT_DEFCHARACTER_BASEWEAR::_Pairib ret = map.insert(std::make_pair(kElement.iWearNo, kElement));
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

/*	select 	[EquipPos],	[PlusLevel],	[SuccessRate],	[RareGroupNo],	[NeedItemNo01],	[NeedItemCount01],	[NeedItemNo02],	[NeedItemCount02],	[NeedItemNo03],	[NeedItemCount03]	 from dbo.TB_DefItemPlusUpgrade */
bool PgDBCache::Q_DQT_DEF_ITEM_PLUS_UPGRADE(CEL::DB_RESULT& rkResult)
{
	CEL::DB_RESULT_COUNT::iterator count_itr = rkResult.vecResultCount.begin();
	if ( count_itr != rkResult.vecResultCount.end() )
	{
		CONT_DEF_ITEM_PLUS_UPGRADE map;
		CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

		int iCount = *count_itr;
		for ( int count = 0; count < iCount ; ++count )
		{
			if( rkResult.vecArray.end() != itor )
			{
				CONT_DEF_ITEM_PLUS_UPGRADE::mapped_type kElement;	
				CONT_DEF_ITEM_PLUS_UPGRADE::key_type kKey(false, 0, 0);

				(*itor).Pop(kKey.kSecKey);						++itor;
				(*itor).Pop(kKey.kTrdKey);						++itor;

				(*itor).Pop(kElement.SuccessRate);				++itor;
				(*itor).Pop(kElement.SuccessRateBonus);			++itor;
				(*itor).Pop(kElement.iNeedMoney);				++itor;

				(*itor).Pop(kElement.RareGroupNo);				++itor;
				(*itor).Pop(kElement.RareGroupSuccessRate);		++itor;

				for(int i = 0; MAX_ITEM_PLUS_UPGRADE_NEED_ARRAY > i ; i++)
				{
					(*itor).Pop(kElement.aNeedItemNo[i]);		++itor;
					(*itor).Pop(kElement.aNeedItemCount[i]);	++itor;
				}

				map.insert(std::make_pair(kKey, kElement));
			}
			else
			{
				VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << L"Critical Error!!!" );
			}
		}

		++count_itr;
		if ( count_itr != rkResult.vecResultCount.end() )
		{
			iCount = *count_itr;
			for ( int count = 0; count < iCount ; ++count )
			{
				if( rkResult.vecArray.end() != itor )
				{
					CONT_DEF_ITEM_PLUS_UPGRADE::mapped_type kElement;	
					CONT_DEF_ITEM_PLUS_UPGRADE::key_type kKey(true, 0, 0);

					(*itor).Pop(kKey.kSecKey);						++itor;
					(*itor).Pop(kKey.kTrdKey);						++itor;

					(*itor).Pop(kElement.SuccessRate);				++itor;
					(*itor).Pop(kElement.SuccessRateBonus);			++itor;
					(*itor).Pop(kElement.iNeedMoney);				++itor;

					(*itor).Pop(kElement.RareGroupNo);				++itor;
					(*itor).Pop(kElement.RareGroupSuccessRate);		++itor;

					for(int i = 0; MAX_ITEM_PLUS_UPGRADE_NEED_ARRAY > i ; i++)
					{
						(*itor).Pop(kElement.aNeedItemNo[i]);		++itor;
						(*itor).Pop(kElement.aNeedItemCount[i]);	++itor;
					}

					map.insert(std::make_pair(kKey, kElement));
				}
				else
				{
					VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << L"Critical Error!!!" );
				}
			}
		}

		if(!map.empty())
		{
			g_kCoreCenter.ClearQueryResult(rkResult);
			g_kTblDataMgr.SetContDef(map);
			return true;
		}	
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

/*	select [EquipType], [LevelLimit], [EnchnatGrade], ... from dbo.TB_DefItemEnchantShift */
bool PgDBCache::Q_DQT_DEF_ITEM_ENCHANT_SHIFT(CEL::DB_RESULT& rkResult)
{
	CEL::DB_RESULT_COUNT::iterator count_itr = rkResult.vecResultCount.begin();
	if ( count_itr != rkResult.vecResultCount.end() )
	{
		CONT_DEF_ITEM_ENCHANT_SHIFT map;
		CONT_DEF_ITEM_ENCHANT_SHIFT::key_type kKey(0,0,0);
		CONT_DEF_ITEM_ENCHANT_SHIFT::mapped_type kElement;
		CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

		int iCount = *count_itr;
		for ( int count = 0; count < iCount ; ++count )
		{
			if( rkResult.vecArray.end() != itor )
			{
				(*itor).Pop(kElement.EquipType);				++itor;
				(*itor).Pop(kElement.LevelLimit);				++itor;
				(*itor).Pop(kElement.Grade);					++itor;

				(*itor).Pop(kElement.EnchantItemNo);			++itor;
				(*itor).Pop(kElement.EnchantItemCount);			++itor;
				(*itor).Pop(kElement.EnchantShiftCost);			++itor;
				(*itor).Pop(kElement.EnchantShiftGemNo);		++itor;
				(*itor).Pop(kElement.EnchantShiftGemCount);		++itor;
				(*itor).Pop(kElement.InsuranceItemNo);			++itor;

				for(int i = 0; ESR_NUM > i ; i++)
				{
					(*itor).Pop(kElement.EnchantShiftRate[i]);	++itor;
				}

				kKey.kPriKey = kElement.EquipType;
				kKey.kSecKey = kElement.Grade;
				kKey.kTrdKey = kElement.LevelLimit;

				map.insert(std::make_pair(kKey, kElement));
			}
			else
			{
				VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << L"Critical Error!!!" );
			}
		}

		if(!map.empty())
		{
			g_kCoreCenter.ClearQueryResult(rkResult);
			g_kTblDataMgr.SetContDef(map);
			return true;
		}	
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

//SELECT [No], [Rate01] ,[Rate02],[Rate03],[Rate04],[Rate05],[Rate06],[Rate07],[Rate08],[Rate09],[Rate10] FROM         TB_DefSuccessRateControl
bool PgDBCache::Q_DQT_DEF_SUCCESS_RATE_CONTROL(CEL::DB_RESULT& rkResult)
{
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	std::wstring kNationCodeStr;
	DBCacheUtil::PgNationCodeHelper< CONT_DEF_SUCCESS_RATE_CONTROL::key_type, CONT_DEF_SUCCESS_RATE_CONTROL::mapped_type, CONT_DEF_SUCCESS_RATE_CONTROL > kNationCodeUtil( L"duplicate SuccessRateControlNo[" DBCACHE_KEY_PRIFIX L"], in [TB_DefSuccessRateControl]" );
	while(rkResult.vecArray.end() != itor)
	{
		CONT_DEF_SUCCESS_RATE_CONTROL::mapped_type kElement;	
		int iNo= 0;
		(*itor).Pop(kNationCodeStr);		++itor;
		(*itor).Pop(iNo);		++itor;

		for(int i = 0; MAX_SUCCESS_RATE_ARRAY> i ; i++)
		{
			(*itor).Pop(kElement.aRate[i]);		++itor;
			kElement.iTotal += kElement.aRate[i];
		}

		kNationCodeUtil.Add(kNationCodeStr, iNo, kElement, __FUNCTIONW__, __LINE__);
	}

	if(!kNationCodeUtil.IsEmpty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kNationCodeUtil.GetResult());
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFMONSTERBAG(CEL::DB_RESULT &rkResult)
{//	select [BagNo], [Rate] from dbo.TB_DefMonsterBag
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEF_MONSTER_BAG map;

	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEF_MONSTER_BAG::mapped_type element;

		(*itor).Pop( element.iBagNo );		++itor;
		(*itor).Pop( element.iElementNo );	++itor;
		(*itor).Pop( element.iRateNo );		++itor;
		(*itor).Pop( element.iTunningNo );	++itor;

		map.insert( std::make_pair(element.iBagNo, element) );
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}



bool PgDBCache::Q_DQT_DEFITEMBAG( CEL::DB_RESULT &rkResult )
{//select [BagNo] from dbo.TB_DefItemBag
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	std::wstring kNationCodeStr;
	DBCacheUtil::PgNationCodeHelper< CONT_DEF_ITEM_BAG::key_type, CONT_DEF_ITEM_BAG::mapped_type, CONT_DEF_ITEM_BAG, DBCacheUtil::ConvertPair > kNationCodeUtil( L"duplicate ItemBagNo[" DBCACHE_KEY_PRIFIX L"], in [TB_DefItemBag]" );
	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEF_ITEM_BAG::key_type key;
		CONT_DEF_ITEM_BAG::mapped_type element;
		(*itor).Pop( kNationCodeStr );		++itor;
		(*itor).Pop( key.first );			++itor;
		(*itor).Pop( key.second );			++itor;
		(*itor).Pop( element.iElementsNo );	++itor;
		(*itor).Pop( element.iRaseRateNo );	++itor;
		(*itor).Pop( element.iCountControlNo );	++itor;
		(*itor).Pop( element.iDropMoneyControlNo );	++itor;

		kNationCodeUtil.Add(kNationCodeStr, key, element, __FUNCTIONW__, __LINE__);
	}

	if( !kNationCodeUtil.IsEmpty() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kNationCodeUtil.GetResult());
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFITEMCONTAINER( CEL::DB_RESULT &rkResult )
{//	Select [ContainerNo], [SuccessRateControlNo], [BagNo01], [BagNo02], [BagNo03], [BagNo04], [BagNo05]
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	std::wstring kNationCodeStr;
	DBCacheUtil::PgNationCodeHelper< CONT_DEF_ITEM_CONTAINER::key_type, CONT_DEF_ITEM_CONTAINER::mapped_type, CONT_DEF_ITEM_CONTAINER > kNationCodeUtil( L"duplicate ContainerNo[" DBCACHE_KEY_PRIFIX L"], in [TB_DefItemContainer]" );
	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEF_ITEM_CONTAINER::key_type kKey;
		CONT_DEF_ITEM_CONTAINER::mapped_type kElement;
		itor->Pop( kNationCodeStr );				++itor;
		itor->Pop( kKey );							++itor;
		itor->Pop( kElement.iRewordItem );				++itor;
		itor->Pop( kElement.iSuccessRateControlNo );	++itor;
		for( int i=0; i!=MAX_ITEM_CONTAINER_LIST; ++i )
		{
			itor->Pop(kElement.aiItemBagGrpNo[i]);		++itor;
		}

		kNationCodeUtil.Add(kNationCodeStr, kKey, kElement, __FUNCTIONW__, __LINE__);
	}

	if ( !kNationCodeUtil.IsEmpty() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kNationCodeUtil.GetResult());
	}
	return true;
}

bool PgDBCache::Q_DQT_DEF_DROP_MONEY_CONTROL( CEL::DB_RESULT &rkResult )
{//select [BagNo] from dbo.TB_DefItemBag
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEF_DROP_MONEY_CONTROL map;

	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEF_DROP_MONEY_CONTROL::mapped_type element;
		(*itor).Pop( element.iNo );	++itor;
		(*itor).Pop( element.iRate);	++itor;
		(*itor).Pop( element.iMin );	++itor;
		(*itor).Pop( element.iMax );	++itor;

		map.insert( std::make_pair(element.iNo, element) );
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_COUNT_CONTROL(CEL::DB_RESULT& rkResult)
{
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	std::wstring kNationCodeStr;
	DBCacheUtil::PgNationCodeHelper< CONT_DEF_COUNT_CONTROL::key_type, CONT_DEF_COUNT_CONTROL::mapped_type, CONT_DEF_COUNT_CONTROL > kNationCodeUtil( L"duplicate CountControlNo[" DBCACHE_KEY_PRIFIX L"], in [TB_DefCountControl]" );
	while(rkResult.vecArray.end() != itor)
	{
		CONT_DEF_COUNT_CONTROL::mapped_type kElement;	
		CONT_DEF_COUNT_CONTROL::key_type kKey;

		(*itor).Pop(kNationCodeStr);	++itor;
		(*itor).Pop(kKey);		++itor;

		for(int i = 0; MAX_SUCCESS_RATE_ARRAY > i ; i++)
		{
			(*itor).Pop(kElement.aCount[i]);		++itor;
			kElement.iTotal += kElement.aCount[i];
		}

		kNationCodeUtil.Add(kNationCodeStr, kKey, kElement, __FUNCTIONW__, __LINE__);
	}

	if(!kNationCodeUtil.IsEmpty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kNationCodeUtil.GetResult());
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_ITEM_BAG_ELEMENTS(CEL::DB_RESULT& rkResult)
{
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	std::wstring kNationCodeStr;
	DBCacheUtil::PgNationCodeHelper< CONT_DEF_ITEM_BAG_ELEMENTS::key_type, CONT_DEF_ITEM_BAG_ELEMENTS::mapped_type, CONT_DEF_ITEM_BAG_ELEMENTS > kNationCodeUtil( L"duplicate ItemBagElementsNo[" DBCACHE_KEY_PRIFIX L"], in [TB_DefItemBagElements]" );
	while(rkResult.vecArray.end() != itor)
	{
		CONT_DEF_ITEM_BAG_ELEMENTS::mapped_type kElement;	
		CONT_DEF_ITEM_BAG_ELEMENTS::key_type kKey;

		(*itor).Pop(kNationCodeStr);	++itor;
		(*itor).Pop(kKey);		++itor;

		for(int i = 0; MAX_SUCCESS_RATE_ARRAY > i ; i++)
		{
			(*itor).Pop(kElement.aElement[i]);		++itor;
		}

		itor->Pop( kElement.nTypeFlag );		++itor;

		kNationCodeUtil.Add(kNationCodeStr, kKey, kElement, __FUNCTIONW__, __LINE__);
	}

	if(!kNationCodeUtil.IsEmpty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kNationCodeUtil.GetResult());
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_MONSTER_BAG_ELEMENTS(CEL::DB_RESULT& rkResult)
{
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEF_MONSTER_BAG_ELEMENTS map;

	while(rkResult.vecArray.end() != itor)
	{
		CONT_DEF_MONSTER_BAG_ELEMENTS::mapped_type kElement;	
		CONT_DEF_MONSTER_BAG_ELEMENTS::key_type kKey;

		(*itor).Pop(kKey);		++itor;

		for(int i = 0; MAX_SUCCESS_RATE_ARRAY > i ; i++)
		{
			(*itor).Pop(kElement.aElement[i]);		++itor;
			//			kElement.iTotal += kElement.aElement[i];
		}

		CONT_DEF_MONSTER_BAG_ELEMENTS::_Pairib ret = map.insert(std::make_pair(kKey, kElement));
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_MONSTER_BAG_CONTROL(CEL::DB_RESULT& rkResult)
{
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEF_MONSTER_BAG_CONTROL map;

	while(rkResult.vecArray.end() != itor)
	{
		CONT_DEF_MONSTER_BAG_CONTROL::mapped_type kElement;	
		//CONT_DEF_MONSTER_BAG_CONTROL::key_type kKey;

		(*itor).Pop(kElement.iParentBagNo);		++itor;

		for(int i = 0; MAX_MONSTERBAG_ELEMENT > i ; i++)
		{
			(*itor).Pop(kElement.aBagElement[i]);		++itor;
		}

		CONT_DEF_MONSTER_BAG_CONTROL::_Pairib ret = map.insert(std::make_pair(kElement.iParentBagNo, kElement));
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_OBJECT(CEL::DB_RESULT& rkResult)
{
	CONT_DEF_OBJECT map;

	CEL::DB_DATA_ARRAY::const_iterator itr = rkResult.vecArray.begin();
	while(rkResult.vecArray.end() != itr)
	{
		CONT_DEF_OBJECT::mapped_type kElement;	

		itr->Pop(kElement.iObjectNo);		++itr;
		//		itr->Pop(kElement.Name);			++itr;
		itr->Pop(kElement.fHeightValue);	++itr;
		for ( int i=0; i!=MAX_OBJECT_ABIL_LIST; ++i )
		{
			itr->Pop(kElement.aAbil[i]);	++itr;
		}

		CONT_DEF_OBJECT::_Pairib ret = map.insert(std::make_pair(kElement.iObjectNo, kElement));
		if ( !ret.second )
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L"ObjectNo["<<kElement.iObjectNo<<L"] Error!!" );
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
	}	
	return true;
}

bool PgDBCache::Q_DQT_DEF_OBJECTABIL(CEL::DB_RESULT& rkResult)
{
	CONT_DEF_OBJECTABIL map;

	CEL::DB_DATA_ARRAY::const_iterator itr = rkResult.vecArray.begin();
	while(rkResult.vecArray.end() != itr)
	{
		CONT_DEF_OBJECTABIL::mapped_type kElement;	

		itr->Pop(kElement.iObjectAbilNo);	++itr;
		for ( int i=0; i!=MAX_OBJECT_ABIL_ARRAY; ++i )
		{
			itr->Pop(kElement.aType[i]);	++itr;
			itr->Pop(kElement.aValue[i]);	++itr;
		}

		CONT_DEF_OBJECTABIL::_Pairib ret = map.insert(std::make_pair(kElement.iObjectAbilNo, kElement));
		if ( !ret.second )
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L"ObjectAbilNo["<<kElement.iObjectAbilNo<<L"] Error!!");
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
	}	
	return true;
}

bool PgDBCache::Q_DQT_DEF_ITEM_BY_LEVEL(CEL::DB_RESULT& rkResult)
{
	CONT_DEF_ITEM_BY_LEVEL map;

	CEL::DB_DATA_ARRAY::const_iterator itr = rkResult.vecArray.begin();
	while(rkResult.vecArray.end() != itr)
	{
		int iIndex = 0;
		int iLevel = 0;
		int iItemNo = 0;
		itr->Pop( iIndex );				++itr;
		itr->Pop( iLevel );				++itr;
		itr->Pop( iItemNo );			++itr;

		CONT_DEF_ITEM_BY_LEVEL::_Pairib kPair = map.insert( std::make_pair( iIndex, CONT_DEF_ITEM_BY_LEVEL::mapped_type() ) );
		kPair.first->second.insert( TBL_DEF_ITEM_BY_LEVEL_ELEMENT( iLevel, iItemNo ) );
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(map);
	return true;
}

bool PgDBCache::Q_DEF_MISSION_DEFENCE_STAGE(CEL::DB_RESULT& rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_MISSION_DEFENCE_STAGE_BAG map;
	CEL::DB_RESULT_COUNT::const_iterator count_iter = rkResult.vecResultCount.begin();

	if(count_iter == rkResult.vecResultCount.end())
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	int const iCount = (*count_iter); ++count_iter;

	for(int i = 0;i < iCount;++i)
	{
		CONT_MISSION_DEFENCE_STAGE_BAG::key_type		kKey;
		SMISSION_DEFENCE_STAGE							kValue;

		result_iter->Pop( kKey.iMissionNo );			++result_iter;
		result_iter->Pop( kKey.iParty_Number );			++result_iter;
		result_iter->Pop( kKey.iStageNo );				++result_iter;
		result_iter->Pop( kValue.iStage_Time );			++result_iter;
		result_iter->Pop( kValue.iWave_GroupNo );		++result_iter;
		result_iter->Pop( kValue.iAddMonster_GroupNo );	++result_iter;
		result_iter->Pop( kValue.iTimeToExp_Rate );		++result_iter;
		//result_iter->Pop( kValue.iStage_Delay );		++result_iter;
		result_iter->Pop( kValue.iResultNo );			++result_iter;
		result_iter->Pop( kValue.iResultCount );		++result_iter;

		CONT_MISSION_DEFENCE_STAGE_BAG::iterator find_itr = map.find( kKey );
		if( map.end() != find_itr )
		{
			(*find_itr).second.kCont.push_back(kValue);
		}
		else
		{			
			CONT_MISSION_DEFENCE_STAGE_BAG::mapped_type kTypeValue;

			kTypeValue.kCont.push_back(kValue);

			CONT_MISSION_DEFENCE_STAGE_BAG::_Pairib kRet = map.insert( std::make_pair(kKey, kTypeValue) );
			if( !kRet.second )
			{
				(*kRet.first).second.kCont.push_back(kValue);
			}
		}		
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DEF_MISSION_DEFENCE_WAVE(CEL::DB_RESULT& rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_MISSION_DEFENCE_WAVE_BAG map;
	CEL::DB_RESULT_COUNT::const_iterator count_iter = rkResult.vecResultCount.begin();

	if(count_iter == rkResult.vecResultCount.end())
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	int const iCount = (*count_iter); ++count_iter;

	for(int i = 0;i < iCount;++i)
	{
		CONT_MISSION_DEFENCE_WAVE_BAG::key_type		kKey;
		SMISSION_DEFENCE_WAVE						kValue;

		result_iter->Pop( kKey.iWave_GroupNo );				++result_iter;
		result_iter->Pop( kKey.iWave_No );					++result_iter;
		result_iter->Pop( kValue.iWave_Delay );				++result_iter;		

		for(int iMonCount = 0; iMonCount<MAX_WAVE_MONSTER_NUM; ++iMonCount)
		{
			result_iter->Pop( kValue.iMonster[iMonCount] );		++result_iter;
		}

		result_iter->Pop( kValue.iAddMoveSpeedPercent );	++result_iter;
		result_iter->Pop( kValue.iTunningNo );				++result_iter;
		result_iter->Pop( kValue.iAddExpPercent );			++result_iter;
		result_iter->Pop( kValue.iAddHPPercent );			++result_iter;
		result_iter->Pop( kValue.iAddDamagePercent );		++result_iter;

		CONT_MISSION_DEFENCE_WAVE_BAG::iterator find_itr = map.find( kKey );
		if( map.end() != find_itr )
		{
			(*find_itr).second.kCont.push_back(kValue);
		}
		else
		{			
			CONT_MISSION_DEFENCE_WAVE_BAG::mapped_type kTypeValue;

			kTypeValue.kCont.push_back(kValue);

			CONT_MISSION_DEFENCE_WAVE_BAG::_Pairib kRet = map.insert( std::make_pair(kKey, kTypeValue) );
			if( !kRet.second )
			{
				(*kRet.first).second.kCont.push_back(kValue);
			}
		}		
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DEF_MISSION_DEFENCE7_MISSION(CEL::DB_RESULT& rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_MISSION_DEFENCE7_MISSION_BAG map;
	CEL::DB_RESULT_COUNT::const_iterator count_iter = rkResult.vecResultCount.begin();

	if(count_iter == rkResult.vecResultCount.end())
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	int const iCount = (*count_iter); ++count_iter;

	for(int i = 0;i < iCount;++i)
	{
		CONT_MISSION_DEFENCE7_MISSION_BAG::key_type		kKey;
		SMISSION_DEFENCE7_MISSION						kValue;

		result_iter->Pop( kKey.iMissionNo );			++result_iter;
		result_iter->Pop( kKey.iMissionType );			++result_iter;
		result_iter->Pop( kValue.iMin_Level );			++result_iter;
		result_iter->Pop( kValue.iMax_Level );			++result_iter;
		result_iter->Pop( kValue.iSlot_F1 );			++result_iter;
		result_iter->Pop( kValue.iSlot_F2 );			++result_iter;
		result_iter->Pop( kValue.iSlot_F3 );			++result_iter;
		result_iter->Pop( kValue.iSlot_F4 );			++result_iter;
		result_iter->Pop( kValue.iSlot_F5 );			++result_iter;
		result_iter->Pop( kValue.iSlot_F6 );			++result_iter;
		result_iter->Pop( kValue.iSlot_F7 );			++result_iter;
		result_iter->Pop( kValue.iSlot_F8 );			++result_iter;

		for(int iSkillCount = 0; iSkillCount<MAX_DEFENCE7_MISSION_SKILL; ++iSkillCount)
		{
			result_iter->Pop( kValue.iSkill[iSkillCount] );			++result_iter;
		}

		CONT_MISSION_DEFENCE7_MISSION_BAG::iterator find_itr = map.find( kKey );
		if( map.end() != find_itr )
		{
			(*find_itr).second.kCont.push_back(kValue);
		}
		else
		{			
			CONT_MISSION_DEFENCE7_MISSION_BAG::mapped_type kTypeValue;

			kTypeValue.kCont.push_back(kValue);

			CONT_MISSION_DEFENCE7_MISSION_BAG::_Pairib kRet = map.insert( std::make_pair(kKey, kTypeValue) );
			if( !kRet.second )
			{
				(*kRet.first).second.kCont.push_back(kValue);
			}
		}		
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DEF_MISSION_DEFENCE7_STAGE(CEL::DB_RESULT& rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_MISSION_DEFENCE7_STAGE_BAG map;
	CEL::DB_RESULT_COUNT::const_iterator count_iter = rkResult.vecResultCount.begin();

	if(count_iter == rkResult.vecResultCount.end())
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	int const iCount = (*count_iter); ++count_iter;

	for(int i = 0;i < iCount;++i)
	{
		CONT_MISSION_DEFENCE7_STAGE_BAG::key_type		kKey;
		SMISSION_DEFENCE7_STAGE							kValue;

		result_iter->Pop( kKey.iMissionNo );			++result_iter;
		result_iter->Pop( kKey.iMissionType );			++result_iter;
		result_iter->Pop( kKey.iParty_Number );			++result_iter;
		result_iter->Pop( kKey.iStageNo );				++result_iter;
		result_iter->Pop( kValue.iResult_ItemNo );		++result_iter;
		result_iter->Pop( kValue.iResult_ItemCount );	++result_iter;
		result_iter->Pop( kValue.iWin_ItemNo );			++result_iter;
		result_iter->Pop( kValue.iWin_ItemCount );		++result_iter;
		result_iter->Pop( kValue.iClear_StategicPoint );++result_iter;
		result_iter->Pop( kValue.iResult_No );			++result_iter;
		result_iter->Pop( kValue.iResult_Count );		++result_iter;

		CONT_MISSION_DEFENCE7_STAGE_BAG::iterator find_itr = map.find( kKey );
		if( map.end() != find_itr )
		{
			(*find_itr).second.kCont.push_back(kValue);
		}
		else
		{			
			CONT_MISSION_DEFENCE7_STAGE_BAG::mapped_type kTypeValue;

			kTypeValue.kCont.push_back(kValue);

			CONT_MISSION_DEFENCE7_STAGE_BAG::_Pairib kRet = map.insert( std::make_pair(kKey, kTypeValue) );
			if( !kRet.second )
			{
				(*kRet.first).second.kCont.push_back(kValue);
			}
		}		
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DEF_MISSION_DEFENCE7_WAVE(CEL::DB_RESULT& rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_MISSION_DEFENCE7_WAVE_BAG map;
	CEL::DB_RESULT_COUNT::const_iterator count_iter = rkResult.vecResultCount.begin();

	if(count_iter == rkResult.vecResultCount.end())
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	int const iCount = (*count_iter); ++count_iter;

	for(int i = 0;i < iCount;++i)
	{
		CONT_MISSION_DEFENCE7_WAVE_BAG::key_type	kKey;
		SMISSION_DEFENCE7_WAVE						kValue;

		result_iter->Pop( kKey.iMissionNo );				++result_iter;
		result_iter->Pop( kKey.iMissionType );				++result_iter;
		result_iter->Pop( kKey.iParty_Number );				++result_iter;
		result_iter->Pop( kKey.iStageNo );					++result_iter;
		result_iter->Pop( kKey.iWave_No );					++result_iter;

		result_iter->Pop( kValue.iWave_Delay );				++result_iter;
		result_iter->Pop( kValue.iGive_StrategicPoint );	++result_iter;

		for(int iMonCount = 0; iMonCount<MAX_DEFENCE7_WAVE_MONSTER; ++iMonCount)
		{
			result_iter->Pop( kValue.iMonster[iMonCount] );	++result_iter;
		}

		result_iter->Pop( kValue.iTunningNo );				++result_iter;
		result_iter->Pop( kValue.iAddMoveSpeedPercent );	++result_iter;		
		result_iter->Pop( kValue.iAddExpPercent );			++result_iter;
		result_iter->Pop( kValue.iAddHPPercent );			++result_iter;

		CONT_MISSION_DEFENCE7_WAVE_BAG::iterator find_itr = map.find( kKey );
		if( map.end() != find_itr )
		{
			(*find_itr).second.kCont.push_back(kValue);
		}
		else
		{			
			CONT_MISSION_DEFENCE7_WAVE_BAG::mapped_type kTypeValue;

			kTypeValue.kCont.push_back(kValue);

			CONT_MISSION_DEFENCE7_WAVE_BAG::_Pairib kRet = map.insert( std::make_pair(kKey, kTypeValue) );
			if( !kRet.second )
			{
				(*kRet.first).second.kCont.push_back(kValue);
			}
		}		
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DEF_MISSION_DEFENCE7_GUARDIAN(CEL::DB_RESULT& rkResult)
{	
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_MISSION_DEFENCE7_GUARDIAN_BAG map;
	CEL::DB_RESULT_COUNT::const_iterator count_iter = rkResult.vecResultCount.begin();

	if(count_iter == rkResult.vecResultCount.end())
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	int const iCount = (*count_iter); ++count_iter;

	for(int i = 0;i < iCount;++i)
	{
		CONT_MISSION_DEFENCE7_GUARDIAN_BAG::key_type	kKey;
		SMISSION_DEFENCE7_GUARDIAN						kValue;

		result_iter->Pop( kKey );							++result_iter;
		result_iter->Pop( kValue.iMonsterNo );				++result_iter;
		result_iter->Pop( kValue.iGuardian_Abil01 );		++result_iter;
		result_iter->Pop( kValue.iGuardian_Abil02 );		++result_iter;
		result_iter->Pop( kValue.iGuardian_TunningNo );		++result_iter;
		result_iter->Pop( kValue.iNeed_StrategicPoint );	++result_iter;
		result_iter->Pop( kValue.iUpgrade_Type );			++result_iter;
		result_iter->Pop( kValue.iUpgrade_Step );			++result_iter;

		for(int iUpgradeCount = 0; iUpgradeCount<MAX_DEFENCE7_GUARDIAN_UPGRADE; ++iUpgradeCount)
		{
			result_iter->Pop( kValue.iUpgrade[iUpgradeCount] );	++result_iter;
		}

		result_iter->Pop( kValue.Skill01_No );				++result_iter;
		result_iter->Pop( kValue.Skill01_StrategicPoint );	++result_iter;
		result_iter->Pop( kValue.Skill02_No );				++result_iter;
		result_iter->Pop( kValue.Skill02_StrategicPoint );	++result_iter;
		result_iter->Pop( kValue.Sell_StrategicPoint );		++result_iter;

		CONT_MISSION_DEFENCE7_GUARDIAN_BAG::iterator find_itr = map.find( kKey );
		if( map.end() != find_itr )
		{
			(*find_itr).second.kCont.push_back(kValue);
		}
		else
		{			
			CONT_MISSION_DEFENCE7_GUARDIAN_BAG::mapped_type kTypeValue;

			kTypeValue.kCont.push_back(kValue);

			CONT_MISSION_DEFENCE7_GUARDIAN_BAG::_Pairib kRet = map.insert( std::make_pair(kKey, kTypeValue) );
			if( !kRet.second )
			{
				(*kRet.first).second.kCont.push_back(kValue);
			}
		}		
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DEF_MISSION_BONUSMAP(CEL::DB_RESULT& rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_MISSION_BONUSMAP map;
	CEL::DB_RESULT_COUNT::const_iterator count_iter = rkResult.vecResultCount.begin();

	if(count_iter == rkResult.vecResultCount.end())
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	int const iCount = (*count_iter); ++count_iter;

	for(int i = 0;i < iCount;++i)
	{
		CONT_MISSION_BONUSMAP::key_type		kKey;
		CONT_MISSION_BONUSMAP::mapped_type	kValue;

		result_iter->Pop( kKey );							++result_iter;
		result_iter->Pop( kValue.iUse );					++result_iter;
		result_iter->Pop( kValue.iTimeLimit );				++result_iter;
		result_iter->Pop( kValue.iChanceRate );				++result_iter;
		result_iter->Pop( kValue.iBonus1stMapRate );		++result_iter;
		result_iter->Pop( kValue.iBonusMap1 );				++result_iter;
		result_iter->Pop( kValue.iBonusMap2 );				++result_iter;
		

		//if( 0 < kValue.iUse )
		{
			CONT_MISSION_BONUSMAP::_Pairib ret = map.insert(std::make_pair(kKey, kValue));
			if ( !ret.second )
			{
				VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L"LevelNo["<< kKey <<L"] Error!!");
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
				return false;
			}
		}	
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_MISSION_CANDIDATE(CEL::DB_RESULT& rkResult)
{
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEF_MISSION_CANDIDATE map;
	while(rkResult.vecArray.end() != itor)
	{
		CONT_DEF_MISSION_CANDIDATE::key_type kKey;
		CONT_DEF_MISSION_CANDIDATE::mapped_type kElement;
		itor->Pop(kKey);							++itor;
		itor->Pop(kElement.iType);					++itor;
		itor->Pop(kElement.iUseCount);				++itor;
		itor->Pop(kElement.iCandidate_End);		++itor;
		for(int i=0; i<MAX_MISSION_CANDIDATE ; ++i)
		{
			itor->Pop(kElement.aiCandidate[i]);		++itor;
		}

		for(int i=0; i<(MAX_MISSION_RANK_CLEAR_LV-1); ++i)
		{
			itor->Pop(kElement.aiRankPoint[i]);		++itor;
		}
		kElement.aiRankPoint[MAX_MISSION_RANK_CLEAR_LV-1] = 0;
		CONT_DEF_MISSION_CANDIDATE::_Pairib ret = map.insert(std::make_pair(kKey,kElement));
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_MISSION_ROOT(CEL::DB_RESULT& rkResult)
{
	//	SELECT [MissionNo], [MissionName], [Stage01], [Stage02], [Stage03], [Stage04], [Stage01_AbilRateBagID], [Stage02_AbilRateBagID], [Stage03_AbilRateBagID], [Stage04_AbilRateBagID], [ResultTableNo] FROM TB_DefMission_StageRoot	CONT_DEF_MISSION_ROOT map;
	CONT_DEF_MISSION_ROOT map;

	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();	
	while(rkResult.vecArray.end() != itor)
	{
		CONT_DEF_MISSION_ROOT::mapped_type kElement;

		itor->Pop(kElement.iMissionNo);						++itor;
		itor->Pop(kElement.iKey);							++itor;
		for(int i=0; i!=MAX_MISSION_LEVEL; ++i)
		{
			itor->Pop(kElement.aiLevel[i]);					++itor;
			itor->Pop(kElement.aiLevel_AbilRateBagID[i]);	++itor;
			itor->Pop(kElement.aiMissionResultNo[i]);		++itor;

			itor->Pop(kElement.aiLevel_Min[i]);				++itor;
			itor->Pop(kElement.aiLevel_Max[i]);				++itor;
			itor->Pop(kElement.aiLevel_AvgMin[i]);			++itor;
			itor->Pop(kElement.aiLevel_AvgMax[i]);			++itor;
		}
		itor->Pop(kElement.iDefence);						++itor;
		itor->Pop(kElement.iDefence7);						++itor;
		itor->Pop(kElement.iDefence8);						++itor;

		CONT_DEF_MISSION_ROOT::_Pairib ret = map.insert(std::make_pair(kElement.iMissionNo, kElement));
		if( !ret.second )
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L"Overlapping Data : Mission No["<<kElement.iMissionNo<<L"]");
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_MISSION_CLASS_REWARD(CEL::DB_RESULT& rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_MISSION_CLASS_REWARD_BAG map;

	CEL::DB_RESULT_COUNT::const_iterator count_iter = rkResult.vecResultCount.begin();

	if(count_iter == rkResult.vecResultCount.end())
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	int const iItemCount = (*count_iter); ++count_iter;

	for(int i = 0;i < iItemCount;++i)
	{
		CONT_MISSION_CLASS_REWARD_BAG::key_type		kKey;
		SMISSION_CLASS_REWARD_ITEM					kValue;

		result_iter->Pop( kKey.iMissionNo );	++result_iter;
		result_iter->Pop( kKey.iLevelNo );		++result_iter;
		result_iter->Pop( kKey.iClassNo );		++result_iter;
		result_iter->Pop( kValue.iItemNo );		++result_iter;
		result_iter->Pop( kValue.iRate );		++result_iter;

		CONT_MISSION_CLASS_REWARD_BAG::iterator find_itr = map.find( kKey );
		if( map.end() != find_itr )
		{
			(*find_itr).second.kCont.push_back(kValue);
		}
		else
		{			
			CONT_MISSION_CLASS_REWARD_BAG::mapped_type kTypeValue;

			kTypeValue.kCont.push_back(kValue);

			CONT_MISSION_CLASS_REWARD_BAG::_Pairib kRet = map.insert( std::make_pair(kKey, kTypeValue) );
			if( !kRet.second )
			{
				(*kRet.first).second.kCont.push_back(kValue);
			}
		}
		
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_MISSION_RANK_REWARD(CEL::DB_RESULT& rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_MISSION_RANK_REWARD_BAG map;

	CEL::DB_RESULT_COUNT::const_iterator count_iter = rkResult.vecResultCount.begin();

	if(count_iter == rkResult.vecResultCount.end())
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	int const iItemCount = (*count_iter); ++count_iter;

	for(int i = 0;i < iItemCount;++i)
	{
		CONT_MISSION_RANK_REWARD_BAG::key_type		kKey;
		SMISSION_RANK_REWARD_ITEM					kValue;

		result_iter->Pop( kKey.iMissionNo );	++result_iter;
		result_iter->Pop( kKey.iLevelNo );		++result_iter;
		result_iter->Pop( kKey.iRankNo );		++result_iter;
		result_iter->Pop( kValue.iItemNo );		++result_iter;
		result_iter->Pop( kValue.iRate );		++result_iter;

		CONT_MISSION_RANK_REWARD_BAG::iterator find_itr = map.find( kKey );
		if( map.end() != find_itr )
		{
			(*find_itr).second.kCont.push_back(kValue);
		}
		else
		{			
			CONT_MISSION_RANK_REWARD_BAG::mapped_type kTypeValue;

			kTypeValue.kCont.push_back(kValue);

			CONT_MISSION_RANK_REWARD_BAG::_Pairib kRet = map.insert( std::make_pair(kKey, kTypeValue) );
			if( !kRet.second )
			{
				(*kRet.first).second.kCont.push_back(kValue);
			}
		}
		
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_DEFENCE_ADD_MONSTER(CEL::DB_RESULT& rkResult)
{
	DBCacheUtil::PgNationCodeHelper< CONT_DEF_DEFENCE_ADD_MONSTER::key_type, CONT_DEF_DEFENCE_ADD_MONSTER::mapped_type, CONT_DEF_DEFENCE_ADD_MONSTER, DBCacheUtil::ConvertDefenceAddMonsterKey > kNationCodeUtil( L"duplicate DefenceAddMonster[" DBCACHE_KEY_PRIFIX L"], in [TB_DefDefenceAddMonster]" );

	CONT_DEF_DEFENCE_ADD_MONSTER map;

	CEL::DB_DATA_ARRAY::const_iterator result_itor = rkResult.vecArray.begin();	
	while(rkResult.vecArray.end() != result_itor)
	{
		CONT_DEF_DEFENCE_ADD_MONSTER::key_type kKey;
		CONT_DEF_DEFENCE_ADD_MONSTER::mapped_type kElement;
		std::wstring kNationCodeStr = L"0";		

		//result_itor->Pop(kNationCodeStr); ++result_itor;
		result_itor->Pop(kKey.iAddMonster_GroupNo); ++result_itor;
		result_itor->Pop(kKey.iSuccess_Count); ++result_itor;
		result_itor->Pop(kElement.iAdd_StageTime); ++result_itor;
		result_itor->Pop(kElement.iMonsterNo); ++result_itor;
		result_itor->Pop(kElement.iEnchant_Probability);	++result_itor;
		result_itor->Pop(kElement.iExpRate);		++result_itor;
		result_itor->Pop(kElement.iItemNo);		++result_itor;
		result_itor->Pop(kElement.iDropRate);		++result_itor;
		result_itor->Pop(kElement.wstrIconPath);	++result_itor;
		
		kNationCodeUtil.Add(kNationCodeStr, kKey, kElement, __FUNCTIONW__, __LINE__);
	}

	if( !kNationCodeUtil.IsEmpty() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kNationCodeUtil.GetResult());
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

/*
bool PgDBCache::Q_DQT_DEF_REVIVEPOS(CEL::DB_RESULT& rkResult)
{
	SELECT [FromMapNo] ,[ToMapNo] ,[PosX] ,[PosY] ,[PosZ]
	,[PosCX] ,[PosCY]  ,[PosCZ]
	FROM [DR2_Def_Eric].[dbo].[TB_DefRevivePos]

	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEF_REVIVEPOS map;

	while(rkResult.vecArray.end() != itor)
	{
		CONT_DEF_REVIVEPOS::mapped_type kElement;	

		(*itor).Pop(kElement.iFromMapNo);	++itor;
		(*itor).Pop(kElement.iToMapNo);		++itor;
		(*itor).Pop(kElement.ptToPos.x);	++itor;
		(*itor).Pop(kElement.ptToPos.y);	++itor;
		(*itor).Pop(kElement.ptToPos.z);	++itor;
		(*itor).Pop(kElement.sizePos.x);	++itor;
		(*itor).Pop(kElement.sizePos.y);	++itor;
		(*itor).Pop(kElement.sizePos.z);	++itor;

		CONT_DEF_REVIVEPOS::_Pairib ret = map.insert(std::make_pair(kElement.iFromMapNo, kElement));
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}
*/



bool PgDBCache::Q_DQT_DEF_ITEM_OPTION(CEL::DB_RESULT& rkResult)
{
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_TBL_DEF_ITEM_OPTION vec;
	CEL::DB_RESULT_COUNT::const_iterator count_itr = rkResult.vecResultCount.begin();
	if( count_itr != rkResult.vecResultCount.end() )
	{
		vec.reserve( *count_itr );
	}

	std::wstring kNationCodeStr = L"0";
	DBCacheUtil::PgNationCodeHelper< TBL_TRIPLE_KEY_INT, CONT_TBL_DEF_ITEM_OPTION::value_type, CONT_TBL_DEF_ITEM_OPTION, DBCacheUtil::ConvertTriple, DBCacheUtil::AddError, DBCacheUtil::VectorContainerInsert > kNationCodeUtil( L"Duplicate Option Key[" DBCACHE_KEY_PRIFIX L"]" );
	while( rkResult.vecArray.end() != itor )
	{
		CONT_TBL_DEF_ITEM_OPTION::value_type element;
		//(*itor).Pop( kNationCodeStr );			++itor;
		(*itor).Pop( element.iOptionType );		++itor;
		(*itor).Pop( element.iOutRate );		++itor;
		(*itor).Pop( element.iAbleEquipPos );	++itor;
		(*itor).Pop( element.iNameNo );			++itor;
		(*itor).Pop( element.iAbilType );		++itor;
		(*itor).Pop( element.iOptionGroup);		++itor;

		for(int i = 0; i < MAX_ITEM_OPTION_ABIL_NUM; ++i)
		{
			(*itor).Pop( element.aiValue[i] );	++itor;
		}
		kNationCodeUtil.Add( kNationCodeStr, TBL_TRIPLE_KEY_INT(element.iOptionType, element.iOptionGroup, element.iAbleEquipPos), element, __FUNCTIONW__, __LINE__ );
	}

	if( !kNationCodeUtil.IsEmpty() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kNationCodeUtil.GetResult());
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_ITEM_OPTION_ABIL(CEL::DB_RESULT& rkResult)
{
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_TBL_DEF_ITEM_OPTION_ABIL map;

	int iIDX = 0;
	while( rkResult.vecArray.end() != itor )
	{
		int iOptionAbilNo = 0;
		CONT_TBL_DEF_ITEM_OPTION_ABIL_PIECE::value_type element;

		(*itor).Pop( iOptionAbilNo );	++itor;
		(*itor).Pop( element.iPieceCount );	++itor;
		(*itor).Pop( element.iOrder );	++itor;

		for (int i=0; i<MAX_OPTION_ABIL_ARRAY; i++)
		{
			(*itor).Pop( element.aiType[i] );++itor;
			(*itor).Pop( element.aiValue[i] );++itor;
		}

		CONT_TBL_DEF_ITEM_OPTION_ABIL::iterator find_iter = map.find(iOptionAbilNo);
		if( map.end() == find_iter )
		{
			CONT_TBL_DEF_ITEM_OPTION_ABIL::_Pairib kRet = map.insert( std::make_pair(iOptionAbilNo, CONT_TBL_DEF_ITEM_OPTION_ABIL::mapped_type(iOptionAbilNo)) );
			find_iter = kRet.first;
		}
		if( map.end() != find_iter )
		{
			(*find_iter).second.kContPieceAbil.push_back(element);
			(*find_iter).second.kContPieceAbil.sort();
		}
		//map.insert( std::make_pair(iOptionAbilNo, element) );
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

#if 0
bool PgDBCache::Q_DQT_DEF_ITEM_RARITY_UPGRADE(CEL::DB_RESULT& rkResult)
{
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEF_ITEM_RARITY_UPGRADE map;

	int iIDX = 0;
	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEF_ITEM_RARITY_UPGRADE::mapped_type element;

		(*itor).Pop( element.iItemGrade );	++itor;
		(*itor).Pop( element.iNeedMoney );	++itor;

		for(int i=0; i!=MAX_ITEM_RARITY_UPGRADE_NEED_ARRAY; ++i)
		{
			itor->Pop(element.aiNeedItemNo[i]);	++itor;
			itor->Pop(element.anNeedItemCount[i]);	++itor;
		}
		itor->Pop(element.nSuccessRate);	++itor;

		map.insert( std::make_pair(element.iItemGrade, element) );
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	return false;
}
#else
bool PgDBCache::Q_DQT_DEF_ITEM_RARITY_UPGRADE(CEL::DB_RESULT& rkResult)
{
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEF_ITEM_RARITY_UPGRADE map;

	int iIDX = 0;
	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEF_ITEM_RARITY_UPGRADE::mapped_type element;

		(*itor).Pop( element.iItemGrade );			++itor;
		(*itor).Pop( element.iItemOptionLvMin );	++itor;
		(*itor).Pop( element.iItemOptionLvMax );	++itor;
		(*itor).Pop( element.iItemOptionCount );	++itor;
		(*itor).Pop( element.iItemRarityMin );		++itor;
		(*itor).Pop( element.iItemRarityMax );		++itor;

		map.insert( std::make_pair(element.iItemGrade, element) );
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}
#endif




bool PgDBCache::Q_DQT_DEF_ITEM_BAG_GROUP(CEL::DB_RESULT& rkResult)
{
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	DBCacheUtil::PgNationCodeHelper< CONT_DEF_ITEM_BAG_GROUP::key_type, CONT_DEF_ITEM_BAG_GROUP::mapped_type, CONT_DEF_ITEM_BAG_GROUP > kNationCodeUtil( L"duplicate BagGroupNo[" DBCACHE_KEY_PRIFIX L"], in [TB_DefItemBagGroup]" );
	while(rkResult.vecArray.end() != itor)
	{
		std::wstring kNationCodeStr;
		CONT_DEF_ITEM_BAG_GROUP::mapped_type element;

		(*itor).Pop( kNationCodeStr );			++itor;
		(*itor).Pop( element.iBagGroupNo );			++itor;
		(*itor).Pop( element.iSuccessRateNo );	++itor;

		for(int i = 0; MAX_ITEM_BAG_ELEMENT > i ; i++)
		{
			(*itor).Pop( element.aiBagNo[i] );	++itor;
		}

		kNationCodeUtil.Add(kNationCodeStr, element.iBagGroupNo, element, __FUNCTIONW__, __LINE__);
	}

	if(!kNationCodeUtil.IsEmpty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kNationCodeUtil.GetResult());
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}



bool PgDBCache::Q_DQT_DEF_PVP_GROUNDMODE( CEL::DB_RESULT& rkResult )
{
	int iIDX = 0;
	CONT_DEF_PVP_GROUNDGROUP kPvPGroundGroup;
	CONT_DEF_PVP_GROUNDMODE map;
	CEL::DB_DATA_ARRAY::const_iterator itr = rkResult.vecArray.begin();
	while ( itr != rkResult.vecArray.end() )
	{
		CONT_DEF_PVP_GROUNDMODE::mapped_type element;
		itr->Pop( element.iGroundNo );			++itr;
		itr->Pop( element.iName );				++itr;
		itr->Pop( element.iType );				++itr;
		itr->Pop( element.wstrPreviewImg );		++itr;
		itr->Pop( element.iUserCount );			++itr;
		map.insert( std::make_pair( element.iGroundNo, element ) );

		CONT_DEF_PVP_GROUNDGROUP::iterator group_itr = kPvPGroundGroup.find(element.iUserCount);
		if( kPvPGroundGroup.end() != group_itr )
		{
			group_itr->second.push_back(element.iGroundNo);
		}
		else
		{
			CONT_DEF_PVP_GROUNDGROUP::mapped_type kGround;
			kGround.push_back(element.iGroundNo);
			kPvPGroundGroup.insert( std::make_pair( element.iUserCount, kGround ) );
		}
	}

	if ( map.empty() )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"TABLE [CONT_TBL_DEF_PVP_GROUNDMODE] is row count 0");
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(map);
	g_kTblDataMgr.SetContDef(kPvPGroundGroup);
	return true;
}


bool PgDBCache::Q_DQT_DEFITEMMAKING( CEL::DB_RESULT &rkResult )
{
	//SELECT [Index] ,[No] ,[TitleStringNo] ,[ContentStringNo] ,[Memo] ,[NeedMoney] ,[NeedElementsNo] ,[NeedCountControlNo] ,[SuccessRateControlNo] ,[ResultControlNo]
	//		,[Elements01] ,[CountControl01] ...
	//  FROM [dbo].[TB_DefItemMaking]
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEFITEMMAKING map;

	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEFITEMMAKING::mapped_type element;

		(*itor).Pop( element.iNo );						++itor;
		(*itor).Pop( element.iTypeNo );					++itor;
		(*itor).Pop( element.iContentStringNo );		++itor;
		(*itor).Pop( element.iNeedMoney );				++itor;
		(*itor).Pop( element.iNeedElementsNo );			++itor;
		(*itor).Pop( element.iNeedCountControlNo );		++itor;
		(*itor).Pop( element.iSuccesRateControlNo );	++itor;
		(*itor).Pop( element.iResultControlNo );		++itor;
		for(int i = 0; MAX_ITEMMAKING_ARRAY > i ; i++)
		{
			(*itor).Pop( element.aiElements[i] );		++itor;
			(*itor).Pop( element.aiCountControl[i] );	++itor;
		}
		(*itor).Pop( element.iRarityControlNo );		++itor;

		map.insert( std::make_pair(element.iNo, element) );
	}

	if( map.size() )
	{
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	
	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"TABLE [TB_DefItemMaking] is row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEFRESULTCONTROL( CEL::DB_RESULT &rkResult )
{
	//SELECT [No] ,[Result01] ,[Result02] ,[Result03] ,[Result04] ,[Result05] ,[Result06] ,[Result07] ,[Result08] ,[Result09] ,[Result10]
	//  FROM [dbo].[TB_DefResultControl]
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	CONT_DEFRESULT_CONTROL map;

	while( rkResult.vecArray.end() != itor )
	{
		CONT_DEFRESULT_CONTROL::mapped_type element;

		(*itor).Pop( element.iNo );	++itor;
		for(int i = 0; MAX_RESULT_ARRAY > i ; i++)
		{
			(*itor).Pop( element.aiResult[i] );		++itor;
		}

		map.insert( std::make_pair(element.iNo, element) );
	}

	if( map.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	
	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"TABLE [TB_DefResultControl] is row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_QUEST_RESET_SCHEDULE(CEL::DB_RESULT& rkResult)
{
	CEL::DB_DATA_ARRAY const& rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_DEF_QUEST_RESET_SCHEDULE kList;

	while(rkVec.end() != result_iter)
	{
		BM::DBTIMESTAMP_EX kDate;
		CONT_DEF_QUEST_RESET_SCHEDULE::value_type kNew;
		(*result_iter).Pop(kNew.iQuestID);			++result_iter;
		(*result_iter).Pop(kDate);					++result_iter;
		(*result_iter).Pop(kNew.iResetLoopDay);		++result_iter;

		kDate.hour = kDate.minute = kDate.second = 0;
		kDate.fraction = 0;

		kNew.kResetBaseDate = kDate;
		kList.push_back( kNew );
	}

	if( kList.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kList);
		return true;//성공
	}
	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"TABLE [TB_DefQuest_ResetSchedule] is row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;//실패 (?)
}

bool PgDBCache::Q_DQT_DEF_QUEST_MIGRATION(CEL::DB_RESULT& rkResult)
{
	using namespace MyQuestMigrationTool;
	CONT_END_QUEST_MIGRATION_VER kCont;
	if( CEL::DR_SUCCESS == rkResult.eRet )
	{
		CEL::DB_DATA_ARRAY const& rkVec = rkResult.vecArray;
		CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

		while(rkVec.end() != result_iter)
		{
			int iMigrationRevision = 0;
			int iQuestID = 0;
			std::wstring kMigrationRule;
			int iTargetValue = 0;

			(*result_iter).Pop( iMigrationRevision );	++result_iter;
			(*result_iter).Pop( iQuestID );				++result_iter;
			(*result_iter).Pop( kMigrationRule );		++result_iter;
			(*result_iter).Pop( iTargetValue );			++result_iter;

			CONT_END_QUEST_MIGRATION_VER::iterator find_iter = kCont.find(iMigrationRevision);
			if( kCont.end() == find_iter )
			{
				CONT_END_QUEST_MIGRATION_VER::_Pairib kRet = kCont.insert( std::make_pair(iMigrationRevision, CONT_END_QUEST_MIGRATION_VER::mapped_type(iMigrationRevision)) );
				if( kRet.second )
				{
					find_iter = kRet.first;
				}
				else
				{
					AddErrorMsg( BM::vstring() << __FL__ << L"Can't make the memmory" );
				}
			}

			if( kCont.end() != find_iter )
			{
				if( !(*find_iter).second.Add(iQuestID, kMigrationRule, iTargetValue) )
				{
					AddErrorMsg( BM::vstring() << __FL__ << L"Wrong migration info[Revision="<<iMigrationRevision<<L", QuestID="<<iQuestID<<L", Rule="<<kMigrationRule<<L", TargetValue="<<iTargetValue<<L"]" );
				}
			}
		}
	}

	if( !kCont.empty() )
	{
		SetMigrationVer(kCont);
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	return true; // 항상 성공
}

bool PgDBCache::Q_DQT_DEF_QUEST_RANDOM_EXP(CEL::DB_RESULT& rkResult)
{
	CEL::DB_DATA_ARRAY const& rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_DEF_QUEST_RANDOM_EXP kMap;

	while(rkVec.end() != result_iter)
	{
		BM::DBTIMESTAMP_EX kDate;
		CONT_DEF_QUEST_RANDOM_EXP::mapped_type kNew;

		(*result_iter).Pop(kNew.iLevel);			++result_iter;

		for( int iCur = 0; MAX_QUEST_RANDOM_EXP_COUNT > iCur; ++iCur )
		{
			(*result_iter).Pop(kNew.aiExp[iCur]);	++result_iter;
		}

		CONT_DEF_QUEST_RANDOM_EXP::_Pairib kRet = kMap.insert( std::make_pair(kNew.iLevel, kNew) );
		if( !kRet.second )
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"Duplicate [TB_DefQuest_RandomExp] Level["<<kNew.iLevel<<L"]");
		}
	}

	if( kMap.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kMap);
		return true;//성공
	}
	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"TABLE [TB_DefQuest_ResetSchedule] is row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;//실패 (?)
}

bool PgDBCache::Q_DQT_DEF_QUEST_RANDOM_TACTICS_EXP(CEL::DB_RESULT& rkResult)
{
	CEL::DB_DATA_ARRAY const& rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_DEF_QUEST_RANDOM_TACTICS_EXP kMap;

	while(rkVec.end() != result_iter)
	{
		BM::DBTIMESTAMP_EX kDate;
		CONT_DEF_QUEST_RANDOM_TACTICS_EXP::mapped_type kNew;

		(*result_iter).Pop(kNew.iLevel);					++result_iter;
		for( int iCur = 0; MAX_QUEST_RANDOM_EXP_COUNT > iCur; ++iCur )
		{
			(*result_iter).Pop(kNew.aiExp[iCur]);			++result_iter;
		}
		for( int iCur = 0; MAX_QUEST_RANDOM_EXP_COUNT > iCur; ++iCur )
		{
			(*result_iter).Pop(kNew.aiGuildExp[iCur]);		++result_iter;
		}

		CONT_DEF_QUEST_RANDOM_TACTICS_EXP::_Pairib kRet = kMap.insert( std::make_pair(kNew.iLevel, kNew) );
		if( !kRet.second )
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"Duplicate [TB_DefQuest_RandomExp] Level["<<kNew.iLevel<<L"]");
		}
	}

	if( kMap.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kMap);
		return true;//성공
	}
	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"TABLE [TB_DefQuest_ResetSchedule] is row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;//실패 (?)
}

bool PgDBCache::Q_DQT_DEF_QUEST_REWARD(CEL::DB_RESULT& rkResult)
{
	using namespace PgQuestInfoVerifyUtil;

	CEL::DB_DATA_ARRAY const& rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_DEF_QUEST_REWARD kMap;
	DBCacheUtil::PgNationCodeHelper< CONT_DEF_QUEST_REWARD::key_type, CONT_DEF_QUEST_REWARD::mapped_type, CONT_DEF_QUEST_REWARD > kNationCodeUtil( L" ", false );	

	while(rkVec.end() != result_iter)
	{
		CONT_DEF_QUEST_REWARD::mapped_type kNew;
		(*result_iter).Pop(kNew.iQuestID);				++result_iter;
		(*result_iter).Pop(kNew.kNationCodeStr);		++result_iter;
		(*result_iter).Pop(kNew.kXmlPath);				++result_iter;
		(*result_iter).Pop(kNew.iGroupNo);				++result_iter;
		(*result_iter).Pop(kNew.iDBQuestType);			++result_iter;
		(*result_iter).Pop(kNew.iExp);					++result_iter;
		(*result_iter).Pop(kNew.iGold);					++result_iter;
		(*result_iter).Pop(kNew.iLevelMin);				++result_iter;
		(*result_iter).Pop(kNew.iLevelMax);				++result_iter;
		(*result_iter).Pop(kNew.iLimitMaxLevel);		++result_iter;
		(*result_iter).Pop(kNew.iTacticsLevel);			++result_iter;
		(*result_iter).Pop(kNew.iTacticsExp);			++result_iter;
		(*result_iter).Pop(kNew.iGuildExp);				++result_iter;
		(*result_iter).Pop(kNew.iDifficult);			++result_iter;
		(*result_iter).Pop(kNew.iOrderIndex);			++result_iter;
		(*result_iter).Pop(kNew.byIsCanRemoteComplete);	++result_iter;
		(*result_iter).Pop(kNew.bIsCanShare);			++result_iter;
		//(*result_iter).Pop(kNew.iTitleText);			++result_iter;
		//(*result_iter).Pop(kNew.iGroupName);			++result_iter;
		//(*result_iter).Pop(kNew.i64ClassFlag);			++result_iter;
		//(*result_iter).Pop(kNew.kPreQuestAnd);			++result_iter;
		//(*result_iter).Pop(kNew.kPreQuestOr);			++result_iter;
		//(*result_iter).Pop(kNew.kNotQuest);				++result_iter;
		//(*result_iter).Pop(kNew.iMinParty);				++result_iter;
		//(*result_iter).Pop(kNew.iMaxParty);				++result_iter;

		switch( kNew.iDBQuestType )
		{
		case QT_Normal:
		case QT_Scenario:
		case QT_ClassChange:
		case QT_Loop:
		case QT_Day:
		case QT_MissionQuest:
		case QT_GuildTactics:
		case QT_Deleted:
		case QT_Dummy:
		case QT_Normal_Day:
		case QT_Couple:
		case QT_SweetHeart:
		case QT_Random:
		case QT_RandomTactics:
		case QT_Soul:
		case QT_Wanted:
			{
			}break;
		case QT_None:
		default:
			{
				BM::vstring const kErrorMsg = BM::vstring(L"Invalid QuestType[")<<kNew.iDBQuestType<<L"] QuestID: "<<kNew.iQuestID;
				AddError( SQuestInfoError(ET_DataError, std::string("DB"), kNew.iQuestID, __FUNCTIONW__, __LINE__, kErrorMsg) );
			}break;
		}
		
		DBCacheUtil::EAddResult eResult = kNationCodeUtil.Add( kNew.kNationCodeStr, kNew.iQuestID, kNew, __FUNCTIONW__, __LINE__ );
		if( DBCacheUtil::E_ADD_FAIL_DUPLICATE == eResult )
		{
			BM::vstring const kErrorMsg = BM::vstring(L"Duplicate QuestID[")<<kNew.iQuestID<<L"] Reward Info";
			AddError( SQuestInfoError(ET_DataError, std::string("DB"), kNew.iQuestID, __FUNCTIONW__, __LINE__, kErrorMsg) );
		}
		else if( DBCacheUtil::E_ADD_FAIL_NATION == eResult )
		{// 해당한 국가 퀘스트만 로드 한다.
			continue;
		}

		if( false == m_bIsForTool
		&&	(QT_Random == kNew.iDBQuestType
		||	QT_RandomTactics == kNew.iDBQuestType 
		||  QT_Wanted == kNew.iDBQuestType) ) // 랜덤 퀘스트, 현상수배퀘스트는 DB상의 Min/Max 레벨 사용하지 않는다
		{
			int const iMinLevel = 1;
			int const iMaxLevel = 999;
			kNew.iLevelMin = iMinLevel;
			kNew.iLevelMax = iMaxLevel;
			kNew.iLimitMaxLevel = iMaxLevel;
		}

		if( kNew.kXmlPath.empty() )
		{
			BM::vstring const kErrorMsg = BM::vstring(L"QuestXML Path is Empty QuestID[") << kNew.iQuestID << L"] NationCodeStr[" << kNew.kNationCodeStr << L"]";
			AddError( SQuestInfoError(ET_DataError, std::string("DB"), kNew.iQuestID, __FUNCTIONW__, __LINE__, kErrorMsg) );
			continue;
		}
	}
	
	if( !kNationCodeUtil.IsEmpty() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kNationCodeUtil.GetResult());
		
		return true;
	}
	
	BM::vstring const kErrorMsg = BM::vstring()<<L"TABLE [TB_DefQuestReward] is row count 0";
	AddError( SQuestInfoError(ET_DataError, std::string("DB"), 0, __FUNCTIONW__, __LINE__, kErrorMsg) );
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;//실패 (?)
}



bool PgDBCache::Q_DQT_DEF_ITEM_SET(CEL::DB_RESULT& rkResult)
{
	CEL::DB_DATA_ARRAY const& rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	DBCacheUtil::PgNationCodeHelper< CONT_DEF_ITEM_SET::key_type, CONT_DEF_ITEM_SET::mapped_type, CONT_DEF_ITEM_SET > kNationCodeUtil( L"Duplicate ItemSet[" DBCACHE_KEY_PRIFIX L"] Reward Info" );
	while(rkVec.end() != result_iter)
	{
		CONT_DEF_ITEM_SET::mapped_type element;
		std::wstring kNationCodeStr;
		(*result_iter).Pop(kNationCodeStr);		++result_iter;
		(*result_iter).Pop(element.iSetNo);		++result_iter;
		(*result_iter).Pop(element.iNameNo);	++result_iter;

		for(int i = 0; MAX_ITEM_SET_ABIL > i ; i++)
		{
			(*result_iter).Pop( element.aiAbilNo[i] );		++result_iter;
		}

		for(int i = 0; MAX_ITEM_SET_NEED_ITEM_ARRAY > i ; i++)
		{
			(*result_iter).Pop( element.aiNeedItemNo[i] );		++result_iter;
		}

		kNationCodeUtil.Add(kNationCodeStr, element.iSetNo, element, __FUNCTIONW__, __LINE__);
	}

	if( !kNationCodeUtil.IsEmpty() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kNationCodeUtil.GetResult());
		return true;//성공
	}
	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"TABLE [TB_DefItemSet] is row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;//실패 (?)
}

bool PgDBCache::Q_DQT_DEF_SPEND_MONEY(CEL::DB_RESULT& rkResult)
{
	CEL::DB_DATA_ARRAY const& rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_itor = rkVec.begin();

	CONT_DEF_SPEND_MONEY kMap;
	while(rkVec.end() != result_itor)
	{
		CONT_DEF_SPEND_MONEY::mapped_type kElement;
		result_itor->Pop(kElement.iID);				++result_itor;
		result_itor->Pop(kElement.iType);			++result_itor;
		result_itor->Pop(kElement.iLevelMin);		++result_itor;
		result_itor->Pop(kElement.iLevelMax);		++result_itor;
		result_itor->Pop(kElement.iSpendMoneyValue);	++result_itor;
		if( kElement.iSpendMoneyValue <= 0 )
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L"[TB_DefSpendMoney] ID["<<kElement.iID<<L"] Value["<<kElement.iSpendMoneyValue<<L"] Error!!");
		}

		CONT_DEF_SPEND_MONEY::_Pairib kPair = kMap.insert(std::make_pair(kElement.iID,kElement));
		if( !kPair.second )
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L"[TB_DefSpendMoney] ID["<<kElement.iID<<L"] Overlapping");
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Insert Failed Data"));
		}
	}

	if ( kMap.size() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kMap);
		return true;
	}

	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_GUILD_LEVEL(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"Failed[Result: "<<rkResult.eRet<<L"] DB Load");
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CONT_DEF_GUILD_LEVEL kCont;
	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();
	while(rkVec.end() != result_iter)
	{
		typedef std::vector< int > CONT_DAY;
		CONT_DAY kTemp;

		CONT_DEF_GUILD_LEVEL::mapped_type kNewElement;

		result_iter->Pop(kNewElement.sGuildLv);					++result_iter;
		result_iter->Pop(kNewElement.iMaxMemberCount);			++result_iter;
		result_iter->Pop(kNewElement.iSkillPoint);				++result_iter;
		result_iter->Pop(kNewElement.iExp);						++result_iter;
		result_iter->Pop(kNewElement.iGold);					++result_iter;
		result_iter->Pop(kNewElement.iItemNo[0]);				++result_iter;
		result_iter->Pop(kNewElement.iCount[0]);				++result_iter;
		result_iter->Pop(kNewElement.iItemNo[1]);				++result_iter;
		result_iter->Pop(kNewElement.iCount[1]);				++result_iter;
		result_iter->Pop(kNewElement.iItemNo[2]);				++result_iter;
		result_iter->Pop(kNewElement.iCount[2]);				++result_iter;
		result_iter->Pop(kNewElement.iItemNo[3]);				++result_iter;
		result_iter->Pop(kNewElement.iCount[3]);				++result_iter;
		result_iter->Pop(kNewElement.iItemNo[4]);				++result_iter;
		result_iter->Pop(kNewElement.iCount[4]);				++result_iter;
		result_iter->Pop(kNewElement.iIntroText);				++result_iter;
		result_iter->Pop(kNewElement.iResultText);				++result_iter;
		result_iter->Pop(kNewElement.iAutoChangeOwner1);		++result_iter;
		result_iter->Pop(kNewElement.iAutoChangeOwner2);		++result_iter;
		result_iter->Pop(kNewElement.iAutoChangeOwnerRun);		++result_iter;

		{
			kTemp.push_back( kNewElement.iAutoChangeOwner1 );
			kTemp.push_back( kNewElement.iAutoChangeOwner2 );
			kTemp.push_back( kNewElement.iAutoChangeOwnerRun );
			std::sort(kTemp.begin(), kTemp.end());
			if( kTemp[0] != kNewElement.iAutoChangeOwner1 // 날자를 잘못 기입 했는가
			||	kTemp[1] != kNewElement.iAutoChangeOwner2
			||	kTemp[2] != kNewElement.iAutoChangeOwnerRun
			||	kTemp[0] == kTemp[1] // 같은 날자를 기입 하지는 않았는가
			||	kTemp[0] == kTemp[2]
			||	kTemp[1] == kTemp[2] )
			{
				AddErrorMsg( BM::vstring()<<__FL__<<L"wrong guild auto change owner day[1st="<<kNewElement.iAutoChangeOwner1<<L", 2nd="<<kNewElement.iAutoChangeOwner2<<L", run="<<kNewElement.iAutoChangeOwnerRun<<L"] from [TB_DefGuildLevel], right day[1st < 2nd < run]" );
				kNewElement.iAutoChangeOwner1 = kTemp[0];
				kNewElement.iAutoChangeOwner2 = kTemp[1];
				kNewElement.iAutoChangeOwnerRun = kTemp[2];
			}
		}

		if( kCont.end() != kCont.find(kNewElement.sGuildLv) )
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"Duplicate Guild Level["<<kNewElement.sGuildLv<<L"] from [TB_DefGuildLevel]");
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}

		std::inserter(kCont, kCont.end()) = std::make_pair(kNewElement.sGuildLv, kNewElement);
	}

	if( kCont.empty() )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(kCont);

	return true;
}

bool PgDBCache::Q_DQT_DEF_GUILD_SKILL(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"Failed[Result: "<<rkResult.eRet<<L"] DB Load");
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CONT_DEF_GUILD_SKILL kCont;
	const CEL::DB_DATA_ARRAY &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();
	while(rkVec.end() != result_iter)
	{
		CONT_DEF_GUILD_SKILL::mapped_type kNewElement;

		result_iter->Pop(kNewElement.sGuildLv);			++result_iter;
		result_iter->Pop(kNewElement.iSkillNo);			++result_iter;
		result_iter->Pop(kNewElement.iSkillPoint);		++result_iter;
		result_iter->Pop(kNewElement.iGold);			++result_iter;
		result_iter->Pop(kNewElement.iGuildExp);		++result_iter;
		result_iter->Pop(kNewElement.iItemNo[0]);		++result_iter;
		result_iter->Pop(kNewElement.iCount[0]);		++result_iter;
		result_iter->Pop(kNewElement.iItemNo[1]);		++result_iter;
		result_iter->Pop(kNewElement.iCount[1]);		++result_iter;


		CONT_DEF_GUILD_SKILL::_Pairib kRet = kCont.insert( std::make_pair(kNewElement.iSkillNo, kNewElement) );
		if( !kRet.second )
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"Duplicate Guild SkillNo["<<kNewElement.iSkillNo<<L"] from [TB_DefGuildSkill]");
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}
	}

	if( kCont.empty() )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(kCont);

	return true;
}
/*
bool PgDBCache::Q_DQT_DEF_PROPERTY(CEL::DB_RESULT &rkResult)
{
if( CEL::DR_SUCCESS != rkResult.eRet )
{
VERIFY_INFO_LOG(false, BM::LOG_LV1, _T("[%s]-[%d] Failed[Result: %d] DB Load"), __FUNCTIONW__, __LINE__, rkResult.eRet);
return false;
}

CONT_DEF_PROPERTY kCont;
const CEL::DB_DATA_ARRAY &rkVec = rkResult.vecArray;
CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();
while(rkVec.end() != result_iter)
{
CONT_DEF_PROPERTY::mapped_type kNewElement;

result_iter->Pop(kNewElement.kKey.iOffense);	++result_iter;
result_iter->Pop(kNewElement.kKey.iDefense);	++result_iter;
result_iter->Pop(kNewElement.iRate);			++result_iter;

CONT_DEF_PROPERTY::_Pairib kRet = kCont.insert( std::make_pair(kNewElement.kKey, kNewElement) );
if( !kRet.second )
{
VERIFY_INFO_LOG(false, BM::LOG_LV1, _T("[%s]-[%d] Duplicate Property[%d,%d] from [TB_DefProperty]"), __FUNCTIONW__, __LINE__, kNewElement.kKey.iOffense,kNewElement.kKey.iDefense);
return false;
}
}

if( kCont.empty() )
{
INFO_LOG(BM::LOG_LV1, _T("[%s]-[%d] failed [TB_DefProperty] row count is 0"), __FUNCTIONW__, __LINE__);
return false;
}

g_kCoreCenter.ClearQueryResult(rkResult);
g_kTblDataMgr.SetContDef(kCont);

return true;
}
*/
bool PgDBCache::Q_DQT_DEF_PROPERTY(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"Failed[Result: "<<rkResult.eRet<<L"] DB Load");
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CONT_DEF_PROPERTY kCont;
	const CEL::DB_DATA_ARRAY &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	int iRate = 0;

	while(rkVec.end() != result_iter)
	{
		CONT_DEF_PROPERTY::mapped_type kNewElement;

		result_iter->Pop(kNewElement.kKey.iProperty);		++result_iter;
		result_iter->Pop(kNewElement.kKey.iPropertyLevel);	++result_iter;
		for(int i = 0;i < PROPERTY_TABLE_LIMIT;i++)
		{
			result_iter->Pop(iRate);						++result_iter;
			kNewElement.kContRate.push_back(iRate);
		}

		CONT_DEF_PROPERTY::_Pairib kRet = kCont.insert( std::make_pair(kNewElement.kKey, kNewElement) );
		if( !kRet.second )
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"Duplicate Property["<<kNewElement.kKey.iProperty<<L","<<kNewElement.kKey.iPropertyLevel<<L"] from [TB_DefProperty]");
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}
	}

	if( kCont.empty() )
	{
		INFO_LOG(BM::LOG_LV1, __FL__<<L"failed [TB_DefProperty] row count is 0");
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(kCont);

	return true;
}




bool PgDBCache::Q_DQT_DEF_FIVE_ELEMENT_INFO(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet 
		&&	CEL::DR_NO_RESULT != rkResult.eRet)
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"Failed[Result: "<<rkResult.eRet<<L"] DB Load");
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CONT_FIVE_ELEMENT_INFO kCont;
	const CEL::DB_DATA_ARRAY &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();
	while(rkVec.end() != result_iter)
	{
		CONT_FIVE_ELEMENT_INFO::mapped_type kValue;

		result_iter->Pop(kValue.iAttrType);	++result_iter;
		result_iter->Pop(kValue.iNameNo);	++result_iter;
		result_iter->Pop(kValue.iCrystalStoneNo);	++result_iter;

		kCont.insert(std::make_pair(kValue.iAttrType, kValue));
	}

	if( kCont.empty() )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(kCont);

	return true;
}


bool PgDBCache::Q_DQT_DEF_ITEM_RARITY_UPGRADE_COST_RATE(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"Failed[Result: "<<rkResult.eRet<<L"] DB Load");
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CONT_ITEM_RARITY_UPGRADE_COST_RATE kCont;
	const CEL::DB_DATA_ARRAY &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();
	while(rkVec.end() != result_iter)
	{
		CONT_ITEM_RARITY_UPGRADE_COST_RATE::mapped_type kValue;

		result_iter->Pop(kValue.iEquipPos);	++result_iter;
		result_iter->Pop(kValue.iCostRate);	++result_iter;
		kCont.insert(std::make_pair(kValue.iEquipPos, kValue));
	}

	if( kCont.empty() )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(kCont);

	return true;
}

bool PgDBCache::Q_DQT_DEF_TACTICS_LEVEL(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"Failed[Can't load TB_DefTacticsLevel table] Result: "<<rkResult.eRet);
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CONT_DEF_TACTICS_LEVEL kCont;
	CONT_DEF_TACTICS_LEVEL::_Pairib kRet;
	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_DEF_TACTICS_LEVEL::mapped_type kNewElement;
	while( rkVec.end() != result_iter )
	{
		result_iter->Pop( kNewElement.sTacticsLevel );		++result_iter;
		result_iter->Pop( kNewElement.iTacticsExp );		++result_iter;
		result_iter->Pop( kNewElement.iTacticsLevelTextID);	++result_iter;

		kRet = kCont.insert( std::make_pair(kNewElement.sTacticsLevel, kNewElement) );
		if( !kRet.second )
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"Can't insert new element: "<<kNewElement.sTacticsLevel);
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}
	}

	if( kCont.empty() )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(kCont);

	return true;
}





bool PgDBCache::Q_DQT_DEF_EMPORIA( CEL::DB_RESULT& rkResult )
{
	if(		CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__<<L"Can't load [TB_DefGuildEmporia] table Result: "<<rkResult.eRet );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY::const_iterator itr = rkResult.vecArray.begin();
	if ( itr != rkResult.vecArray.end() )
	{
		int iCount = *(rkResult.vecResultCount.begin());

		CONT_DEF_EMPORIA map;

		while ( itr != rkResult.vecArray.end() )
		{
			if ( iCount > 0 )
			{
				CONT_DEF_EMPORIA::mapped_type kElement;

				itr->Pop( kElement.guidEmporiaID );				++itr;
				itr->Pop( kElement.iBaseMapNo );				++itr;

				for ( int i=0; i<MAX_EMPORIA_GRADE; ++i )
				{
					itr->Pop( kElement.iEmporiaGroundNo[i] );	++itr;
					itr->Pop( kElement.iBattleGroundNo[i] );	++itr;
				}

				itr->Pop( kElement.iChallengeBattleGroundNo );	++itr;
				itr->Pop( kElement.iLimitCost );	++itr;

				//Reward
				itr->Pop( kElement.kReward.iFromNo );			++itr;
				itr->Pop( kElement.kReward.iTitleNo );			++itr;
				itr->Pop( kElement.kReward.iBodyNo );			++itr;
				for(int i=0; i<TBL_DEF_EMPORIA_REWARD::ECG_END; ++i)
				{
					itr->Pop( kElement.kReward.kChallenge[i].iItem );				++itr;
					//itr->Pop( kElement.kReward.kChallenge[i].iMoney );				++itr;
					itr->Pop( kElement.kReward.kChallenge[i].iGradeTextNo );		++itr;
				}
				for(int i=0; i<MAX_EMPORIA_GRADE; ++i)
				{
					itr->Pop( kElement.kReward.kGrade[i].iItem );			++itr;
					//itr->Pop( kElement.kReward.kGrade[i].iMoney );			++itr;
					itr->Pop( kElement.kReward.kGrade[i].iGradeTextNo );	++itr;
				}
				itr->Pop( kElement.kReward.sLimitDay );			++itr;

				CONT_DEF_EMPORIA::_Pairib kPair = map.insert( std::make_pair( kElement.guidEmporiaID, kElement ) );
				if ( !kPair.second )
				{
					VERIFY_INFO_LOG( false, BM::LOG_LV4, __FL__ << _T("Overlapping EmporiaID<") << kElement.guidEmporiaID << _T(">") );
				}

				--iCount;
			}
			else
			{
				BM::GUID kEmporiaID;
				BYTE byGrade = 0;
				CONT_DEF_EMPORIA_FUNCTION::mapped_type kElement;

				itr->Pop( kEmporiaID );					++itr;
				itr->Pop( byGrade );					++itr;
				itr->Pop( kElement.nFuncNo );			++itr;
				itr->Pop( kElement.nPrice_ForWeek );	++itr;
				itr->Pop( kElement.iValue );		++itr;

				int const iGrade = static_cast<int>(byGrade-1);
				if ( iGrade < MAX_EMPORIA_GRADE )
				{
					CONT_DEF_EMPORIA::iterator itr = map.find( kEmporiaID );
					if ( itr != map.end() )
					{
						CONT_DEF_EMPORIA_FUNCTION::_Pairib kPair = itr->second.m_kContDefFunc[iGrade].insert( std::make_pair( kElement.nFuncNo, kElement ) );
					}
					else
					{
						VERIFY_INFO_LOG( false, BM::LOG_LV4, __FL__ << L"Not Found EmporiaID<" << kEmporiaID << L">" );
					}
				}
				else
				{
					VERIFY_INFO_LOG( false, BM::LOG_LV4, __FL__ << L"Emporia Grade<" << iGrade << L"> Error ID<" << kEmporiaID << L">" );
				}
			}
		}

		g_kCoreCenter.ClearQueryResult(rkResult);
		if ( !map.empty() )
		{
			g_kTblDataMgr.SetContDef(map);
		}

		return true;
	}

	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_MONSTER_KILL_COUNT_REWARD( CEL::DB_RESULT& rkResult )
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__<<L"Can't load [TB_DefGuildEmporia] table Result: "<<rkResult.eRet );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CONT_DEF_MONSTER_KILL_COUNT_REWARD kCont;
	CONT_DEF_MONSTER_KILL_COUNT_REWARD::iterator iter;
	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_DEF_MONSTER_KILL_COUNT_REWARD::mapped_type kNewElement;
	int iKillCount = 0;
	while( rkVec.end() != result_iter )
	{
		result_iter->Pop( iKillCount );				++result_iter;
		result_iter->Pop( kNewElement.iMinLevel );	++result_iter;
		result_iter->Pop( kNewElement.iMaxLevel );	++result_iter;
		result_iter->Pop( kNewElement.iItemNo );	++result_iter;
		result_iter->Pop( kNewElement.iCount );		++result_iter;

		iter = kCont.insert( std::make_pair(iKillCount, kNewElement) );
		//if( !kRet.second )
		//{
		//	VERIFY_INFO_LOG(false, BM::LOG_LV1, _T("[%s]-[%d] Can't insert new monster kill count: %d"), __FUNCTIONW__, __LINE__, iKillCount);
		//	return false;
		//}
	}

	if( kCont.empty() )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(kCont);

	return true;
}

void Q_DQT_DEF_ACHIEVEMENTS(CONT_DEF_ACHIEVEMENTS_SAVEIDX& kContAchievements_SaveIdx)
{
	CONT_DEF_CONT_ACHIEVEMENTS kCont;
	CONT_DEF_ACHIEVEMENTS kContAchievements;
	CONT_DEF_ITEM2ACHIEVEMENT kContItem2Achievement;

	CONT_DEF_ACHIEVEMENTS_SAVEIDX::iterator iter = kContAchievements_SaveIdx.begin();
	while( kContAchievements_SaveIdx.end() != iter )
	{
		CONT_DEF_ACHIEVEMENTS_SAVEIDX::mapped_type const& kNewElement = (*iter).second;
		CONT_DEF_ACHIEVEMENTS::_Pairib kRet = kCont[kNewElement.iType].insert(std::make_pair(kNewElement.iValue, kNewElement));
		if( kRet.second )
		{
			kContAchievements.insert(std::make_pair(kNewElement.iIdx,kNewElement));

			CONT_DEF_ITEM2ACHIEVEMENT::mapped_type kItem;
			kItem.iItemNo = kNewElement.iItemNo;
			kItem.iSaveIdx = kNewElement.iSaveIdx;

			kContItem2Achievement.insert(std::make_pair(kItem.iItemNo,kItem));
		}
		else
		{
			DBCacheUtil::AddErrorMsg( BM::vstring() << __FL__<<L"Already exists achievements: IDX"<<kNewElement.iIdx );
		}
		++iter;
	}

	g_kTblDataMgr.SetContDef(kCont);
	g_kTblDataMgr.SetContDef(kContAchievements);
	g_kTblDataMgr.SetContDef(kContAchievements_SaveIdx);
	g_kTblDataMgr.SetContDef(kContItem2Achievement);
}

bool PgDBCache::Q_DQT_DEF_ACHIEVEMENTS(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__<<L"Can't load [TB_DefAchievements] table Result: "<<rkResult.eRet );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	DBCacheUtil::PgNationCodeHelper< CONT_DEF_ACHIEVEMENTS_SAVEIDX::key_type, CONT_DEF_ACHIEVEMENTS_SAVEIDX::mapped_type, CONT_DEF_ACHIEVEMENTS_SAVEIDX > kNationCodeUtil(L"duplicate Achievement SaveIdx[" DBCACHE_KEY_PRIFIX L"]" );
	DBCacheUtil::PgNationCodeHelper< CONT_DEF_ACHIEVEMENTS::key_type, CONT_DEF_ACHIEVEMENTS::mapped_type, CONT_DEF_ACHIEVEMENTS > kNationCodeUtil2(L"duplicate Achievement Idx[" DBCACHE_KEY_PRIFIX L"]" );
	while( rkVec.end() != result_iter )
	{
		std::wstring kNationCodeStr;
		CONT_DEF_ACHIEVEMENTS_SAVEIDX::mapped_type kNewElement;

		result_iter->Pop( kNewElement.iIdx );			++result_iter;
		result_iter->Pop( kNationCodeStr );				++result_iter;
		result_iter->Pop( kNewElement.iCategory );		++result_iter;
		result_iter->Pop( kNewElement.iTitleNo );		++result_iter;
		result_iter->Pop( kNewElement.iDiscriptionNo );	++result_iter;
		result_iter->Pop( kNewElement.iType );			++result_iter;
		result_iter->Pop( kNewElement.iValue );			++result_iter;
		result_iter->Pop( kNewElement.iItemNo );		++result_iter;
		result_iter->Pop( kNewElement.iSaveIdx );		++result_iter;
		result_iter->Pop( kNewElement.iRankPoint );		++result_iter;
		result_iter->Pop( kNewElement.iLevel );			++result_iter;

		DWORD dwValue = 0;
		BYTE bValue = 0;

		kNewElement.kStartTime.Clear();
		result_iter->Pop( dwValue );		++result_iter;	kNewElement.kStartTime.Year(std::max(static_cast<int>(dwValue - BM::PgPackedTime::BASE_YEAR),0));
		result_iter->Pop( bValue );			++result_iter;	kNewElement.kStartTime.Month(bValue);
		result_iter->Pop( bValue );			++result_iter;	kNewElement.kStartTime.Day(bValue);
		result_iter->Pop( bValue );			++result_iter;	kNewElement.kStartTime.Hour(bValue);
		result_iter->Pop( bValue );			++result_iter;	kNewElement.kStartTime.Min(bValue);

		kNewElement.kEndTime.Clear();
		result_iter->Pop( dwValue );		++result_iter;	kNewElement.kEndTime.Year(std::max(static_cast<int>(dwValue - BM::PgPackedTime::BASE_YEAR),0));
		result_iter->Pop( bValue );			++result_iter;	kNewElement.kEndTime.Month(bValue);
		result_iter->Pop( bValue );			++result_iter;	kNewElement.kEndTime.Day(bValue);
		result_iter->Pop( bValue );			++result_iter;	kNewElement.kEndTime.Hour(bValue);
		result_iter->Pop( bValue );			++result_iter;	kNewElement.kEndTime.Min(bValue);

		result_iter->Pop( kNewElement.iUseTime );		++result_iter;

		BYTE bIsUse = false;
		result_iter->Pop( bIsUse );			++result_iter;

		//result_iter->Pop( kNewElement.iGroupNo );			++result_iter;
		//result_iter->Pop( kNewElement.i64CustomValue );		++result_iter;

		switch( kNewElement.iCategory )
		{
			case AC_TOTAL:
			case AC_CATEGORY_01:
			case AC_CATEGORY_02:
			case AC_CATEGORY_03:
			case AC_CATEGORY_04:
			case AC_CATEGORY_05:
			case AC_CATEGORY_06:
				{
				}break;
			default:
				{
					AddErrorMsg( BM::vstring() << __FL__ << L"Wrong Achievement Category[" << kNewElement.iCategory << L"] IDX[" << kNewElement.iIdx << L"]" );
				}break;
		}

		if( !bIsUse )
		{
			INFO_LOG(BM::LOG_LV6, __FL__<<L"Use Not Achievement SaveIdx["<<kNewElement.iSaveIdx<<L"]");
			continue;
		}

		kNationCodeUtil.Add( kNationCodeStr, kNewElement.iSaveIdx, kNewElement, __FUNCTIONW__, __LINE__);
		kNationCodeUtil2.Add( kNationCodeStr, kNewElement.iIdx, kNewElement, __FUNCTIONW__, __LINE__);
	}
	::Q_DQT_DEF_ACHIEVEMENTS( kNationCodeUtil.GetResult() ); // 한개만

	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}



bool PgDBCache::Q_DQT_DEF_FILTER_UNICODE(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet) // 이건 결과 없으면 안됨
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"No FIlter Unicode Range");
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! DB_RESULT=") << rkResult.eRet );
		return false;
	}

	CONT_DEF_FILTER_UNICODE kList;

	CEL::DB_DATA_ARRAY::const_iterator kIter = rkResult.vecArray.begin();
	while(rkResult.vecArray.end() != kIter)
	{
		CONT_DEF_FILTER_UNICODE::value_type kNewFilter;

		std::wstring kTemp;
		kIter->Pop(kNewFilter.iFuncCode);		++kIter;
		kIter->Pop(kNewFilter.cStart);			++kIter;
		kIter->Pop(kNewFilter.cEnd);			++kIter;
		kIter->Pop(kNewFilter.bFilterType);		++kIter;

		kList.push_back( kNewFilter );
	}

	if( !kList.empty() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kList);
		return true;
	}
	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__ << _T(" [TB_DefFIlterUnicode] table is row 0"));
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_MONSTERCARD(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet) // 이건 결과 없으면 안됨
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! DB_RESULT=") << rkResult.eRet );
		return false;
	}

	CONT_MONSTERCARD kCont;

	CEL::DB_DATA_ARRAY::const_iterator kIter = rkResult.vecArray.begin();
	while(rkResult.vecArray.end() != kIter)
	{
		CONT_MONSTERCARD::key_type kKey;
		CONT_MONSTERCARD::mapped_type kValue;

		//kIter->Pop(kKey.iOrderIndex);			++kIter;
		kKey.iOrderIndex = 1;
		kIter->Pop(kKey.iCardNo);				++kIter;
		kIter->Pop(kValue);						++kIter;

		if(false == kCont.insert(std::make_pair(kKey, kValue)).second)
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__ << _T(" [Q_DQT_DEF_MONSTERCARD] Table Data Error !! kKey[") << kKey.iOrderIndex << _T(", ") << kKey.iCardNo << _T("]"));
			return false;
		}
	}

	if( !kCont.empty() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kCont);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__ << _T(" [TB_DefMonsterCard] table is row 0"));
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_MARRYTEXT(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet) // 이건 결과 없으면 안됨
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! DB_RESULT=") << rkResult.eRet );
		return false;
	}

	CONT_MARRYTEXT kCont;

	CEL::DB_DATA_ARRAY::const_iterator kIter = rkResult.vecArray.begin();
	while(rkResult.vecArray.end() != kIter)
	{
		CONT_MARRYTEXT::key_type kKey;
		CONT_MARRYTEXT::mapped_type kValue;

		kIter->Pop(kKey);			++kIter;
		kIter->Pop(kValue);			++kIter;

		if(false == kCont.insert(std::make_pair(kKey,kValue)).second)
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__ << _T(" [Q_DQT_DEF_MARRYTEXT] Table Data Error !! kKey[") << kKey << _T("]"));
			return false;
		}
	}

	if( !kCont.empty() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kCont);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__ << _T(" [TB_DefMarryText] table is row 0"));
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_HIDDENREWORDITEM(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet) // 이건 결과 없으면 안됨
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! DB_RESULT=") << rkResult.eRet );
		return false;
	}

	CONT_HIDDENREWORDITEM kCont;

	CEL::DB_DATA_ARRAY::const_iterator kIter = rkResult.vecArray.begin();
	while(rkResult.vecArray.end() != kIter)
	{
		CONT_HIDDENREWORDITEM::key_type kKey;
		CONT_HIDDENREWORDITEM::mapped_type kValue;

		kIter->Pop(kKey);						++kIter;
		kIter->Pop(kValue.iMinLevel);			++kIter;
		kIter->Pop(kValue.iMaxLevel);			++kIter;
		kIter->Pop(kValue.iItemNo);				++kIter;

		CONT_HIDDENREWORDITEM::iterator iter;
		iter = kCont.insert(std::make_pair(kKey, kValue));
		/*if( !kPair.second )
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__ << _T(" [Q_DQT_DEF_HIDDENREWORDITEM] Table Data Error !! kKey[") << kKey << _T("]"));
			return false;
		}*/
	}

	if( !kCont.empty() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kCont);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__ << _T(" [TB_DefHiddenRewordItem] table is row 0"));
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_HIDDENREWORDBAG(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet) // 이건 결과 없으면 안됨
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! DB_RESULT=") << rkResult.eRet );
		return false;
	}

	CONT_HIDDENREWORDBAG kCont;

	CEL::DB_DATA_ARRAY::const_iterator kIter = rkResult.vecArray.begin();
	while(rkResult.vecArray.end() != kIter)
	{
		CONT_HIDDENREWORDBAG::key_type kKey;
		CONT_HIDDENREWORDBAG::mapped_type kValue;

		kIter->Pop(kKey);						++kIter;
		kIter->Pop(kValue.iContainerNo);		++kIter;
		kIter->Pop(kValue.iRarityControl);		++kIter;
		kIter->Pop(kValue.iPlusUpControl);		++kIter;

		kCont.insert(std::make_pair(kKey, kValue));
	}

	if( !kCont.empty() )
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kCont);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__ << _T(" [TB_DefHiddenRewordBag] table is row 0"));
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_EMOTION(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet) // 이건 결과 없으면 안됨
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! DB_RESULT=") << rkResult.eRet );
		return false;
	}

	CONT_EMOTION kCont;

	CONT_EMOTION_GROUP kContGroup;

	CEL::DB_RESULT_COUNT::const_iterator countiter = rkResult.vecResultCount.begin();
	if(countiter == rkResult.vecResultCount.end())
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! DB_RESULT=") << rkResult.eRet );
		return false;
	}

	int const iEmotionGroupCount = (*countiter); ++countiter;

	CEL::DB_DATA_ARRAY::const_iterator kIter = rkResult.vecArray.begin();

	for(int i = 0;i < iEmotionGroupCount;++i)
	{
		CONT_EMOTION_GROUP::key_type kKey;
		CONT_EMOTION_GROUP::mapped_type kValue;

		kIter->Pop(kKey.bType);					++kIter;
		kIter->Pop(kKey.iGroupNo);				++kIter;
		kIter->Pop(kValue);						++kIter;

		if(false == kContGroup.insert(std::make_pair(kKey,kValue)).second)
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__ << _T(" [TB_DefEmotionGroup] Table Data Error !! kKey[GroupNo:") << kKey.iGroupNo << _T(",Type:") << kKey.bType << _T("]"));
			return false;
		}
	}

	int const iEmotioncount = (*countiter);	++countiter;
	for(int i = 0;i < iEmotioncount;++i)
	{
		CONT_EMOTION::key_type kKey;
		CONT_EMOTION::mapped_type kValue;

		kIter->Pop(kKey.iEMotionNo);			++kIter;
		kIter->Pop(kKey.bType);					++kIter;
		kIter->Pop(kValue);						++kIter;

		if(false == kCont.insert(std::make_pair(kKey,kValue)).second)
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__ << _T(" [TB_DefEmotion] Table Data Error !! kKey[EmotionNo:") << kKey.iEMotionNo << _T(",Type:") << kKey.bType << _T("]"));
			return false;
		}
	}

	if(!kContGroup.empty() && !kCont.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kContGroup);
		g_kTblDataMgr.SetContDef(kCont);
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__ << _T(" [TB_DefEmotionGroup] table is row 0"));
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_PET_HATCH(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! DB_RESULT=") << rkResult.eRet );
		return false;
	}

	int i = 0;
	int j = 0;
	DBCacheUtil::PgNationCodeHelper< CONT_DEF_PET_HATCH::key_type, CONT_DEF_PET_HATCH::mapped_type, CONT_DEF_PET_HATCH > kNationCodeUtil( L"duplicate HatchID[" DBCACHE_KEY_PRIFIX L"], in [TB_DefPat_Hatch]" );
	CEL::DB_DATA_ARRAY::const_iterator itr = rkResult.vecArray.begin();
	while(itr != rkResult.vecArray.end())
	{
		std::wstring kNationCodeStr;
		CONT_DEF_PET_HATCH::mapped_type	kElement;

		itr->Pop( kNationCodeStr );								++itr;
		itr->Pop( kElement.iID );								++itr;

		for ( i = 0 ; i < PET_HATCH_MAX_CLASS ; ++i )
		{
			itr->Pop( kElement.iClass[i] );						++itr;
			itr->Pop( kElement.sLevel[i] );						++itr;
			itr->Pop( kElement.iClassRate[i] );					++itr;
			for ( j = 0 ; j < PET_HATCH_MAX_BONUSSTATUS ; ++j )
			{
				itr->Pop( kElement.iBonusStatusRateID[i][j] );	++itr;
			}
		}

		for ( i = 0; i < PET_HATCH_MAX_PERIOD ; ++i )
		{
			itr->Pop( kElement.sPeriod[i] );					++itr;
			itr->Pop( kElement.iPeriodRate[i] );				++itr;
		}

		kNationCodeUtil.Add(kNationCodeStr, kElement.iID, kElement, __FUNCTIONW__, __LINE__);
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(kNationCodeUtil.GetResult());
	return true;
}



bool PgDBCache::Q_DQT_DEF_LOAD_RECOMMENDATIONITEM(CEL::DB_RESULT &rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG( BM::LOG_LV0, __FL__ << _T("load failed ErrorCode = ") << rkResult.eRet );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CONT_DEF_RECOMMENDATION_ITEM kCont;

	CEL::DB_RESULT_COUNT::const_iterator return_iter = rkResult.vecResultCount.begin();
	CEL::DB_DATA_ARRAY::const_iterator itr = rkResult.vecArray.begin();

	if((itr!=rkResult.vecArray.end()) && (return_iter != rkResult.vecResultCount.end()))
	{
		while(itr != rkResult.vecArray.end())
		{
			CONT_DEF_RECOMMENDATION_ITEM::mapped_type kElement;

			(*itr).Pop(kElement.iIDX);			++itr;
			(*itr).Pop(kElement.iClass);		++itr;
			(*itr).Pop(kElement.iLvMin);		++itr;
			(*itr).Pop(kElement.iLvMax);		++itr;

			for(int i = 0;i < CONT_DEF_RECOMMENDATION_ITEM::mapped_type::E_MAX_RECOMMENDATION_ITEM;i++)
			{
				int iItemNo = 0;
				(*itr).Pop(iItemNo);			++itr;
				if(iItemNo)
				{
					kElement.kContItem.push_back(iItemNo);
				}
			}

			CONT_DEF_RECOMMENDATION_ITEM::_Pairib kPair = kCont.insert(std::make_pair(kElement.iIDX,kElement));
			if( !kPair.second )
			{
				VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Data Error !! [") << kElement.iIDX << _T("]"));
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Insert Failed Data"));
			}
		}

		g_kTblDataMgr.SetContDef(kCont);
		return true;
	}

	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}


bool PgDBCache::Q_DQT_DEF_RARE_MONSTER_SPEECH(CEL::DB_RESULT &rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG( BM::LOG_LV0, __FL__ << _T("load failed ErrorCode = ") << rkResult.eRet );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CONT_DEF_RARE_MONSTER_SPEECH::key_type kTalkNo = 0;
	CONT_DEF_RARE_MONSTER_SPEECH kCont;

	CEL::DB_DATA_ARRAY::const_iterator itr = rkResult.vecArray.begin();

	while(itr != rkResult.vecArray.end())
	{
		(*itr).Pop(kTalkNo);					++itr;
		for(int i = 0;i < CONT_DEF_RARE_MONSTER_SPEECH::mapped_type::MAX_SPEECH_NUM;++i)
		{
			int iSpeechNo = 0;
			(*itr).Pop(iSpeechNo);		++itr;
			if(iSpeechNo)
			{
				kCont[kTalkNo].push_back(iSpeechNo);
			}
		}
	}

	g_kTblDataMgr.SetContDef(kCont);
	return true;
}

bool PgDBCache::Q_DQT_LOAD_CARD_LOCAL(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_CARD_LOCAL::mapped_type kData;
	CONT_CARD_LOCAL kTable;
	while( rkVec.end() != result_iter )
	{
		result_iter->Pop( kData.iLocal );		++result_iter;
		result_iter->Pop( kData.kText);			++result_iter;
		if(false == kTable.insert(std::make_pair(kData.iLocal,kData)).second)
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Card Local Key Error!!") << kData.iLocal << kData.kText);
			return false;
		}
	}

	g_kTblDataMgr.SetContDef(kTable);
	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_HOMETOWNTOMAPCOST(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_HOMETOWNTOMAPCOST kTable;

	while( rkVec.end() != result_iter )
	{
		CONT_HOMETOWNTOMAPCOST::key_type kKey;
		CONT_HOMETOWNTOMAPCOST::mapped_type kValue;
		result_iter->Pop( kKey );			++result_iter;
		result_iter->Pop( kValue.iCost );	++result_iter;

		if(false == kTable.insert(std::make_pair(kKey,kValue)).second)
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ 
				<< _T("HOMETOWN MAP MOVE LEVEL LIMIT ERROR !!") 
				<< _T(" LEVEL LIMIT :") << kKey);
			return false;
		}
	}

	g_kTblDataMgr.SetContDef(kTable);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}


bool PgDBCache::Q_DQT_LOAD_DEF_MYHOME_TEX(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_MYHOME_TEX kTable;

	while( rkVec.end() != result_iter )
	{
		CONT_MYHOME_TEX::key_type kKey;
		CONT_MYHOME_TEX::mapped_type kValue;
		result_iter->Pop( kKey );			++result_iter;
		result_iter->Pop( kValue.iTex );	++result_iter;

		if(false == kTable.insert(std::make_pair(kKey,kValue)).second)
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ 
				<< _T("MYHOME AUCTION LEVEL LIMIT ERROR !!") 
				<< _T(" LEVEL LIMIT :") << kKey);
			return false;
		}
	}

	g_kTblDataMgr.SetContDef(kTable);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_SKILLIDX_TO_SKILLNO(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_IDX2SKILLNO kTable;

	while( rkVec.end() != result_iter )
	{
		CONT_IDX2SKILLNO::key_type kKey;
		CONT_IDX2SKILLNO::mapped_type kValue;
		result_iter->Pop( kKey );			++result_iter;
		result_iter->Pop( kValue );			++result_iter;

		if(false == kTable.insert(std::make_pair(kKey,kValue)).second)
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("SKILL IDX ERROR !!") << _T("Key:") << kKey);
			return false;
		}
	}

	g_kTblDataMgr.SetContDef(kTable);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}

bool PgDBCache::Q_DQT_DEF_LOAD_CARDABIL(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T(" Table is Empty!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_CARD_ABIL::key_type kKey;
	CONT_CARD_ABIL::mapped_type kValue;
	CONT_CARD_ABIL kCont;

	while( rkVec.end() != result_iter )
	{
		result_iter->Pop( kKey.kConstellation );		++result_iter;
		result_iter->Pop( kKey.kBlood );				++result_iter;
		result_iter->Pop( kKey.kHobby );				++result_iter;
		result_iter->Pop( kKey.kStyle );				++result_iter;
		result_iter->Pop( kValue.wAbilType );			++result_iter;
		result_iter->Pop( kValue.iValue);				++result_iter;

		if(false == kCont.insert(std::make_pair(kKey,kValue)).second)
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T(" Card Abil Key Error !! Key : ") 
				<< kKey.kConstellation << _T(" : ") << kKey.kBlood << _T(" : ") << kKey.kHobby << _T(" : ") << kKey.kStyle);
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(kCont);

	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_CARD_KEY_STRING(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_DEF_CARD_KEY_STRING::key_type kKey;
	CONT_DEF_CARD_KEY_STRING::mapped_type kData;
	CONT_DEF_CARD_KEY_STRING kTable;
	while( rkVec.end() != result_iter )
	{
		result_iter->Pop( kKey.bKeyType );		++result_iter;
		result_iter->Pop( kKey.bKeyValue);		++result_iter;
		result_iter->Pop( kData.iStringNo);		++result_iter;
		result_iter->Pop( kData.bCardKey);		++result_iter;

		if(false == kTable.insert(std::make_pair(kKey,kData)).second)
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Card Local Key Error!!") << _T("KeyType:") << kKey.bKeyType << _T("KeyValue:") << kKey.bKeyValue << _T("StringNo") << kData.iStringNo);
			return false;
		}
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(kTable);
	return true;
}

bool PgDBCache::Q_DQT_DEF_CONVERTITEM(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! DB_RESULT=") << rkResult.eRet );
		return false;
	}

	CONT_DEF_CONVERTITEM kCont;

	CEL::DB_DATA_ARRAY::const_iterator kIter = rkResult.vecArray.begin();

	while(kIter != rkResult.vecArray.end())
	{
		CONT_DEF_CONVERTITEM::mapped_type			kValue;

		kIter->Pop(kValue.iSourceItemNo);			++kIter;
		kIter->Pop(kValue.sSourceItemNum);			++kIter;
		kIter->Pop(kValue.iTargetItemNo);			++kIter;
		kIter->Pop(kValue.sTargetItemNum);			++kIter;

		kCont.insert(std::make_pair(kValue.iSourceItemNo,kValue));
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(kCont);
	return true;
}

bool PgDBCache::Q_DQT_DEF_GROUND_RARE_MONSTER(CEL::DB_RESULT &rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG( BM::LOG_LV0, __FL__ << _T("load failed ErrorCode = ") << rkResult.eRet );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CONT_DEF_GROUND_RARE_MONSTER::key_type kGroundNo = 0;
	CONT_DEF_GROUND_RARE_MONSTER kCont;

	CEL::DB_DATA_ARRAY::const_iterator itr = rkResult.vecArray.begin();

	while(itr != rkResult.vecArray.end())
	{
		CONT_DEF_GROUND_RARE_MONSTER::mapped_type::mapped_type kElement;

		(*itr).Pop(kGroundNo);					++itr;
		(*itr).Pop(kElement.iMonNo);			++itr;
		(*itr).Pop(kElement.iGenGroupNo);		++itr;
		(*itr).Pop(kElement.iDelayGenTime);		++itr;
		(*itr).Pop(kElement.iRegenTime);		++itr;

		CONT_DEF_GROUND_RARE_MONSTER::mapped_type::_Pairib kPair = kCont[kGroundNo].insert(std::make_pair(kElement.iMonNo,kElement));
		if( !kPair.second )
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Data Error !! GroundNo [") << kGroundNo << _T("] MonsterNo [") << kElement.iMonNo << _T("]"));
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Insert Failed Data"));
		}
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(kCont);
	return true;
}

bool PgDBCache::Q_DQT_DEF_TRANSTOWER(CEL::DB_RESULT &rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG( BM::LOG_LV0, __FL__ << _T("load failed ErrorCode = ") << rkResult.eRet );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CONT_DEF_TRANSTOWER	kCont;

	CEL::DB_DATA_ARRAY::const_iterator itr = rkResult.vecArray.begin();
	while(itr != rkResult.vecArray.end())
	{
		BM::GUID					kGuid;
		TBL_DEF_TRANSTOWER_TARGET	kElement;

		itr->Pop( kGuid );						++itr;
		itr->Pop( kElement.nSort );				++itr;
		itr->Pop( kElement.iGroundNo );			++itr;
		itr->Pop( kElement.nTargetSpawn );		++itr;
		itr->Pop( kElement.i64Price );			++itr;
		itr->Pop( kElement.iMemo );				++itr;
		
		CONT_DEF_TRANSTOWER::_Pairib kPair = kCont.insert( std::make_pair( kGuid, CONT_DEF_TRANSTOWER_TARGET() ) );
		CONT_DEF_TRANSTOWER_TARGET::_Pairib kPair2 = kPair.first->second.insert( kElement );
		if ( !kPair2.second )
		{
			VERIFY_INFO_LOG( false, BM::LOG_LV4, __FL__ << L"Data Error !! GUID<" << kGuid << L"> GroundNo<" << kElement.iGroundNo << L"> TargetSpawn<" << kElement.nTargetSpawn << L"> is Overlaping Data!!");
		}
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(kCont);
	return true;
}

bool PgDBCache::Q_DQT_DEF_PARTY_INFO(CEL::DB_RESULT& rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG( BM::LOG_LV0, __FL__ << _T("load failed ErrorCode = ") << rkResult.eRet );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CONT_DEF_PARTY_INFO	kCont;

	CEL::DB_DATA_ARRAY::const_iterator itr = rkResult.vecArray.begin();
	while(itr != rkResult.vecArray.end())
	{
		CONT_DEF_PARTY_INFO::key_type			iContinent;
		TBL_DEF_PARTY_INFO_TARGET				kElement;
		int	iUse = 0;

		itr->Pop( kElement.iAttribute );		++itr;
		itr->Pop( iContinent );					++itr;
		itr->Pop( kElement.iSort );				++itr;
		itr->Pop( kElement.iArea_NameNo );		++itr;
		itr->Pop( iUse );						++itr;
		itr->Pop( kElement.iGroundNo );			++itr;
		
		if( 0 < iUse )
		{
			CONT_DEF_PARTY_INFO::_Pairib kPair = kCont.insert( std::make_pair( iContinent, CONT_DEF_PARTY_INFO_TARGET() ) );
			CONT_DEF_PARTY_INFO_TARGET::_Pairib kPair2 = kPair.first->second.insert( kElement );
			if ( !kPair2.second )
			{
				VERIFY_INFO_LOG( false, BM::LOG_LV4, __FL__ << L"Data Error !! GUID<" << iContinent << L"> Attribute<" << kElement.iAttribute << L"> Sort<" << kElement.iSort << L"> Area<" << kElement.iArea_NameNo << L"> is Overlaping Data!!");
			}
		}
	}

	g_kCoreCenter.ClearQueryResult(rkResult);
	g_kTblDataMgr.SetContDef(kCont);
	return true;
}

bool PgDBCache::Q_DQT_DEF_MAP_ITEM_BAG(CEL::DB_RESULT& rkResult)
{
	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();

	DBCacheUtil::PgNationCodeHelper< CONT_DEF_MAP_ITEM_BAG::key_type, CONT_DEF_MAP_ITEM_BAG::mapped_type, CONT_DEF_MAP_ITEM_BAG > kNationCodeUtil( L"duplicate GroundNo[" DBCACHE_KEY_PRIFIX L"] in [TB_DefMapItemBag]" );
	while(rkResult.vecArray.end() != itor)
	{
		std::wstring kNationCodeStr;
		CONT_DEF_MAP_ITEM_BAG::mapped_type element;

		(*itor).Pop( kNationCodeStr );			++itor;
		(*itor).Pop( element.iMapNo );			++itor;
		(*itor).Pop( element.iSuccessRateNo );	++itor;

		for(int i = 0; MAP_ITEM_BAG_GRP > i ; i++)
		{
			(*itor).Pop( element.aiBagGrpNo[i] );	++itor;
		}

		kNationCodeUtil.Add(kNationCodeStr, element.iMapNo, element, __FUNCTIONW__, __LINE__);
	}

	if(!kNationCodeUtil.IsEmpty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kNationCodeUtil.GetResult());
		return true;
	}

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_MISSION_RESULT(CEL::DB_RESULT& rkResult)
{
	//	select [ResultNo], [RankSS], [RankS], [RankA], [RankB], [RankC], [RankD], [RankE] FROM TB_DefMission_Result
	CONT_DEF_MISSION_RESULT map;

	CEL::DB_DATA_ARRAY::const_iterator itor = rkResult.vecArray.begin();
	while(rkResult.vecArray.end() != itor)
	{
		CONT_DEF_MISSION_RESULT::mapped_type kElement;	
		//CONT_DEF_MISSION_RESULT::key_type kKey;

		(*itor).Pop(kElement.iResultNo);			++itor;
		for(int i = 0; MAX_MISSION_RANK_CLEAR_LV > i ; i++)
		{
			(*itor).Pop(kElement.aiResultContainer[i]);		++itor;
		}
		(*itor).Pop(kElement.iRouletteBagGrpNo);	++itor;

		for(int i = 0; MAX_MISSION_RANK_CLEAR_LV > i ; i++)
		{
			(*itor).Pop(kElement.aiResultCountMin[i]);		++itor;
			(*itor).Pop(kElement.aiResultCountMax[i]);		++itor;
		}

		CONT_DEF_MISSION_RESULT::_Pairib ret = map.insert(std::make_pair(kElement.iResultNo, kElement));
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_DEF_OBJECT_BAG_ELEMENTS(CEL::DB_RESULT& rkResult)
{
	CONT_DEF_OBJECT_BAG_ELEMENTS map;

	CEL::DB_DATA_ARRAY::const_iterator itr = rkResult.vecArray.begin();
	while(rkResult.vecArray.end() != itr)
	{
		CONT_DEF_OBJECT_BAG_ELEMENTS::mapped_type kElement;	

		itr->Pop(kElement.iElementNo);			++itr;
		itr->Pop(kElement.iObjectNo);			++itr;
		itr->Pop(kElement.pt3RelativePos.x);	++itr;
		itr->Pop(kElement.pt3RelativePos.y);	++itr;
		itr->Pop(kElement.pt3RelativePos.z);	++itr;

		CONT_DEF_OBJECT_BAG_ELEMENTS::_Pairib ret = map.insert(std::make_pair(kElement.iElementNo, kElement));
		if ( !ret.second )
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L"ElementNoo["<<kElement.iElementNo<<L"] Error!!");
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
	}	
	return true;
}

bool PgDBCache::Q_DQT_DEF_OBJECT_BAG(CEL::DB_RESULT& rkResult)
{
	CONT_DEF_OBJECT_BAG map;

	CEL::DB_DATA_ARRAY::const_iterator itr = rkResult.vecArray.begin();
	while(rkResult.vecArray.end() != itr)
	{
		CONT_DEF_OBJECT_BAG::mapped_type kElement;	

		itr->Pop(kElement.iBagNo);				++itr;
		for ( int i=0; i!=MAX_OBJECT_ELEMENT_NO; ++i )
		{
			itr->Pop(kElement.iElementNo[i]);	++itr;
		}

		CONT_DEF_OBJECT_BAG::_Pairib ret = map.insert(std::make_pair(kElement.iBagNo, kElement));
		if ( !ret.second )
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L"BagNo["<<kElement.iBagNo<<L"] Error!!");
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
	}	
	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_CHARCARDEFFECT(CEL::DB_RESULT &rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << _T(" DB Query failed ErrorCode=") << rkResult.eRet << _T(", Query=") << rkResult.Command());
		return false;
	}

	CONT_DEFCHARCARDEFFECT kCont;

	CEL::DB_DATA_ARRAY::iterator iter = rkResult.vecArray.begin();
	while(iter != rkResult.vecArray.end())
	{
		CONT_DEFCHARCARDEFFECT::key_type	kKey;

		(*iter).Pop(kKey);					++iter;

		int iValue = 0;

		(*iter).Pop(iValue);				++iter;

		kCont[kKey].push_back(iValue);
	}

	g_kTblDataMgr.SetContDef(kCont);
	g_kCoreCenter.ClearQueryResult(rkResult);

	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_SIDEJOBRATE(CEL::DB_RESULT &rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << _T(" DB Query failed ErrorCode=") << rkResult.eRet << _T(", Query=") << rkResult.Command());
		return false;
	}

	CONT_DEFSIDEJOBRATE kCont;

	CEL::DB_DATA_ARRAY::iterator iter = rkResult.vecArray.begin();
	while(iter != rkResult.vecArray.end())
	{
		CONT_DEFSIDEJOBRATE::key_type	kKey;
		CONT_DEFSIDEJOBRATE::mapped_type kValue;

		(*iter).Pop(kKey);					++iter;
		(*iter).Pop(kValue.iCostRate);		++iter;
		(*iter).Pop(kValue.iSoulRate);		++iter;
		(*iter).Pop(kValue.iSuccessRate);	++iter;
		(*iter).Pop(kValue.iPayRate);		++iter;

		if(false == kCont.insert(std::make_pair(kKey,kValue)).second)
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L"SIDE JOB KEY ["<<kKey<<L"] ERROR!!");
			return false;
		}
	}

	g_kTblDataMgr.SetContDef(kCont);
	g_kCoreCenter.ClearQueryResult(rkResult);

	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_EVENTITEMSET(CEL::DB_RESULT &rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << _T(" DB Query failed ErrorCode=") << rkResult.eRet << _T(", Query=") << rkResult.Command());
		return false;
	}

	CONT_DEF_EVENT_ITEM_SET kCont;

	CEL::DB_DATA_ARRAY::iterator iter = rkResult.vecArray.begin();
	while(iter != rkResult.vecArray.end())
	{
		CONT_DEF_EVENT_ITEM_SET::value_type kNewElement;
		(*iter).Pop(kNewElement.iEventItemSetID);				++iter;
		(*iter).Pop(kNewElement.bIsUse);						++iter;
		(*iter).Pop(kNewElement.bIsAbsoluteCount);				++iter;
		(*iter).Pop(kNewElement.iEquipItemNo);					++iter;
		(*iter).Pop(kNewElement.iItemCount);					++iter;
		for( int iCur = 0; MAX_EVENT_ITEM_SET_REWARD > iCur; ++iCur )
		{
			(*iter).Pop(kNewElement.aiRewardEffectID[iCur]);	++iter;
		}

		kCont.push_back( kNewElement );
	}

	g_kTblDataMgr.SetContDef(kCont);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_REDICEOPTIONCOST(CEL::DB_RESULT &rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << _T(" DB Query failed ErrorCode=") << rkResult.eRet << _T(", Query=") << rkResult.Command());
		return false;
	}

	CONT_DEFREDICEOPTIONCOST kCont;

	CEL::DB_DATA_ARRAY::iterator iter = rkResult.vecArray.begin();
	while(iter != rkResult.vecArray.end())
	{
		CONT_DEFREDICEOPTIONCOST::key_type kKey;
		CONT_DEFREDICEOPTIONCOST::mapped_type kValue;
		(*iter).Pop(kKey);							++iter;
		(*iter).Pop(kValue);						++iter;
		if(false == kCont.insert(std::make_pair(kKey,kValue)).second)
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L" KEY ["<<kKey<<L"] ERROR!!");
			return false;
		}
	}

	g_kTblDataMgr.SetContDef(kCont);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_MYHOMESIDEJOBTIME(CEL::DB_RESULT &rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << _T(" DB Query failed ErrorCode=") << rkResult.eRet << _T(", Query=") << rkResult.Command());
		return false;
	}

	CONT_DEFMYHOMESIDEJOBTIME kCont;

	CEL::DB_DATA_ARRAY::iterator iter = rkResult.vecArray.begin();
	while(iter != rkResult.vecArray.end())
	{
		CONT_DEFMYHOMESIDEJOBTIME::key_type kKey;
		CONT_DEFMYHOMESIDEJOBTIME::mapped_type kValue;
		(*iter).Pop(kKey);							++iter;
		(*iter).Pop(kValue);						++iter;
		if(false == kCont.insert(std::make_pair(kKey,kValue)).second)
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L" KEY ["<<kKey<<L"] ERROR!!");
			return false;
		}
	}

	g_kTblDataMgr.SetContDef(kCont);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_MONSTER_ENCHANT_GRADE(CEL::DB_RESULT &rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << _T(" DB Query failed ErrorCode=") << rkResult.eRet << _T(", Query=") << rkResult.Command());
		return false;
	}

	std::wstring kTemp;
	CONT_DEF_MONSTER_ENCHANT_GRADE kCont;
	CEL::DB_DATA_ARRAY::iterator iter = rkResult.vecArray.begin();
	while(iter != rkResult.vecArray.end())
	{
		CONT_DEF_MONSTER_ENCHANT_GRADE::key_type kKey;
		CONT_DEF_MONSTER_ENCHANT_GRADE::mapped_type kValue;
		(*iter).Pop(kKey);							++iter;
		(*iter).Pop(kValue.iEnchantLevel);			++iter;
		(*iter).Pop(kValue.iPrefixNameNo);			++iter;
		for( int iCur = 0; MAX_MONSTER_ENCHANT_EFFECT_COUNT > iCur; ++iCur )
		{
			(*iter).Pop(kTemp);						++iter;		kValue.akEffectName[iCur] = MB(kTemp);
			(*iter).Pop(kTemp);						++iter;		kValue.akEffectRoot[iCur] = MB(kTemp);
		}
		for( int iCur = 0; MAX_MONSTER_ENCHANT_ADD_ABIL_COUNT > iCur; ++iCur )
		{
			(*iter).Pop(kValue.aiAddAbilNo[iCur]);	++iter;
		}
		for( int iCur = 0; MAX_MONSTER_ENCHANT_SET_ABIL_COUNT > iCur; ++iCur )
		{
			(*iter).Pop(kValue.aiSetAbilNo[iCur]);	++iter;
		}
		if(false == kCont.insert(std::make_pair(kKey,kValue)).second)
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L" KEY ["<<kKey<<L"] ERROR!!");
			return false;
		}
	}

	{
		CONT_DEFMONSTERABIL const* pkDefMonAbil = NULL;
		g_kTblDataMgr.GetContDef(pkDefMonAbil);
		CONT_DEF_MONSTER_ENCHANT_GRADE::const_iterator grade_iter = kCont.begin();
		while( kCont.end() != grade_iter )
		{
			CONT_DEF_MONSTER_ENCHANT_GRADE::key_type const& rkKey = (*grade_iter).first;
			CONT_DEF_MONSTER_ENCHANT_GRADE::mapped_type const& rkVal = (*grade_iter).second;

			for( int iCur = 0; MAX_MONSTER_ENCHANT_ADD_ABIL_COUNT > iCur; ++iCur )
			{
				int const iMonAbilNo = rkVal.aiAddAbilNo[iCur];
				if( 0 < iMonAbilNo
				&&	pkDefMonAbil->end() == pkDefMonAbil->find(iMonAbilNo) )
				{
					AddErrorMsg(BM::vstring()<<__FL__<<L"Wrong MonsterEnchant[AddAbil"<<(iCur+1)<<L":"<<iMonAbilNo<<L", EnchantGradeNo: "<<rkKey<<L"], Can't find in [TB_DefMonsterAbil] table" );
				}
			}
			for( int iCur = 0; MAX_MONSTER_ENCHANT_SET_ABIL_COUNT > iCur; ++iCur )
			{
				int const iMonAbilNo = rkVal.aiSetAbilNo[iCur];
				if( 0 < iMonAbilNo
				&&	pkDefMonAbil->end() == pkDefMonAbil->find(iMonAbilNo) )
				{
					AddErrorMsg(BM::vstring()<<__FL__<<L"Wrong MonsterEnchant[SetAbil"<<(iCur+1)<<L":"<<iMonAbilNo<<L", EnchantGradeNo: "<<rkKey<<L"], Can't find in [TB_DefMonsterAbil] table" );
				}
			}
			++grade_iter;
		}
	}

	g_kTblDataMgr.SetContDef(kCont);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_BASICOPTIONAMP(CEL::DB_RESULT &rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << _T(" DB Query failed ErrorCode=") << rkResult.eRet << _T(", Query=") << rkResult.Command());
		return false;
	}

	CONT_DEFBASICOPTIONAMP kCont;
	CONT_DEFBASICOPTIONAMP::key_type		kKey;
	CONT_DEFBASICOPTIONAMP::mapped_type		kValue;

	CEL::DB_DATA_ARRAY::iterator iter = rkResult.vecArray.begin();
	while(iter != rkResult.vecArray.end())
	{
		(*iter).Pop(kValue.iEquipPos);			++iter;
		(*iter).Pop(kValue.iLevelLimit);		++iter;
		(*iter).Pop(kValue.iAmpLv);				++iter;
		(*iter).Pop(kValue.iAmpMaxLv);			++iter;
		(*iter).Pop(kValue.iAmpRate);			++iter;
		(*iter).Pop(kValue.iSuccessRate);		++iter;
		(*iter).Pop(kValue.iNeedCost);			++iter;
		(*iter).Pop(kValue.iNeedSoulCount);		++iter;
		(*iter).Pop(kValue.iAmpItemNo);			++iter;
		(*iter).Pop(kValue.iAmpItemCount);		++iter;
		(*iter).Pop(kValue.iInsuranceItemNo);	++iter;

		kKey.kPriKey = kValue.iEquipPos;
		kKey.kSecKey = kValue.iLevelLimit;
		kKey.kTrdKey = kValue.iAmpLv;

		if(false == kCont.insert(std::make_pair(kKey, kValue)).second)
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L" KEY ["<< kKey.kPriKey <<L","<< kKey.kSecKey <<L","<< kKey.kTrdKey <<L"] ERROR!!");
			return false;
		}
	}

	g_kTblDataMgr.SetContDef(kCont);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_MYHOMEBUILDINGS(CEL::DB_RESULT &rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << _T(" DB Query failed ErrorCode=") << rkResult.eRet << _T(", Query=") << rkResult.Command());
		return false;
	}

	CONT_DEFMYHOMEBUILDINGS kContHB;
	CONT_DEFGROUNDBUILDINGS kContGB;

	CEL::DB_DATA_ARRAY::iterator iter = rkResult.vecArray.begin();
	while(iter != rkResult.vecArray.end())
	{
		CONT_DEFMYHOMEBUILDINGS::mapped_type	kValue;
		(*iter).Pop(kValue.iBuildingNo);		++iter;
		(*iter).Pop(kValue.iGroundNo);			++iter;
		(*iter).Pop(kValue.iGrade);				++iter;
		(*iter).Pop(kValue.iMaxRoom);			++iter;

		if(false == kContHB.insert(std::make_pair(kValue.iBuildingNo,kValue)).second)
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L" KEY ["<<kValue.iBuildingNo<<L"] ERROR!!");
			return false;
		}

		kContGB[kValue.iGroundNo].kCont.push_back(kValue);
	}

	g_kTblDataMgr.SetContDef(kContHB);
	g_kTblDataMgr.SetContDef(kContGB);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_ALRAM_MISSION(CEL::DB_RESULT &rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << L" DB Query failed ErrorCode=" << rkResult.eRet << L", Query=" << rkResult.Command());
		return false;
	}

	if ( 2 != rkResult.vecResultCount.size() )
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << L" DB Query Result Count= " << rkResult.vecResultCount.size() << L" Error, Query=" << rkResult.Command() );
		return false;
	}

	CEL::DB_RESULT_COUNT::const_iterator count_itr = rkResult.vecResultCount.begin();
	int iCount = *count_itr;

	CONT_DEF_ALRAM_MISSION temp_map;

	CEL::DB_DATA_ARRAY::iterator itr = rkResult.vecArray.begin();
	while(itr != rkResult.vecArray.end())
	{
		if ( 0 < iCount )
		{
			TBL_DEF_ALRAM_MISSION kElement;
			itr->Pop( kElement.iAlramID );			++itr;
			itr->Pop( kElement.iType );				++itr;
			itr->Pop( kElement.iTitleNo );			++itr;
			itr->Pop( kElement.iAlramID_Next );		++itr;
			itr->Pop( kElement.i64ClassLimit );		++itr;
			itr->Pop( kElement.iTime );				++itr;
			itr->Pop( kElement.nPoint );			++itr;
			itr->Pop( kElement.iExp );				++itr;
			itr->Pop( kElement.iEffect );			++itr;

			for ( int i=0 ; i < MAX_ALRAM_MISSION_ITEMBAG; ++i )
			{
				itr->Pop( kElement.iItemBag[i] );	++itr;
			}

			temp_map.insert( std::make_pair( kElement.iAlramID, kElement ) );

			--iCount;
		}
		else
		{
			int iAlramID = 0;
			TBL_DEF_ALRAM_MISSION_ACTION kElement;
			itr->Pop( iAlramID );					++itr;
			itr->Pop( kElement.iDiscriptionNo );	++itr;
			itr->Pop( kElement.iActionType );		++itr;
			itr->Pop( kElement.iObjectType );		++itr;
			itr->Pop( kElement.i64ObjectValue );	++itr;
			itr->Pop( kElement.iCount );			++itr;
			itr->Pop( kElement.byFailType );		++itr;

			CONT_DEF_ALRAM_MISSION::iterator map_itr = temp_map.find( iAlramID );
			if ( map_itr != temp_map.end() )
			{
				map_itr->second.kActionList.push_back( kElement );
			}
		}
	}

	// def의 메모리 낭비가 없게 하기 위해
	CONT_DEF_ALRAM_MISSION map;
	map = temp_map;// 복사생성자에서 reserve를 한다.

	g_kTblDataMgr.SetContDef(map);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_DEATHPENALTY(CEL::DB_RESULT& rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << _T(" DB Query failed ErrorCode=") << rkResult.eRet << _T(", Query=") << rkResult.Command());
		return false;
	}
	
	CONT_DEFDEATHPENALTY kCont;
	CEL::DB_DATA_ARRAY::iterator iter = rkResult.vecArray.begin();
	while(iter != rkResult.vecArray.end())
	{
		VEC_DEFDEATHPENALTY::value_type kValue;
		(*iter).Pop(kValue.iGroundAttr);++iter;
		(*iter).Pop(kValue.iGroundNo);++iter;
		(*iter).Pop(kValue.sLevelMin);++iter;
		(*iter).Pop(kValue.sLevelMax);++iter;
		(*iter).Pop(kValue.bySuccessionalDeath);++iter;
		for (int i=0; i<VEC_DEFDEATHPENALTY::value_type::MAX_DEATH_PENALTY_EFFECTSIZE; i++)
		{
			(*iter).Pop(kValue.iEffect[i]);++iter;
		}
		
		CONT_DEFDEATHPENALTY::iterator itor_death = kCont.find(kValue.iGroundNo);
		if (itor_death == kCont.end())
		{
			CONT_DEFDEATHPENALTY::_Pairib ibRet = kCont.insert(std::make_pair(kValue.iGroundNo, VEC_DEFDEATHPENALTY()));
			itor_death = ibRet.first;
		}

		VEC_DEFDEATHPENALTY& kInfoVec = (*itor_death).second;
		kInfoVec.push_back(kValue);

		++itor_death;
	}

	g_kTblDataMgr.SetContDef(kCont);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_SKILLEXTENDITEM(CEL::DB_RESULT& rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << _T(" DB Query failed ErrorCode=") << rkResult.eRet << _T(", Query=") << rkResult.Command());
		return false;
	}
	
	CONT_DEFSKILLEXTENDITEM kCont;
	CEL::DB_DATA_ARRAY::iterator iter = rkResult.vecArray.begin();
	while(iter != rkResult.vecArray.end())
	{
		CONT_DEFSKILLEXTENDITEM::key_type kKey;
		CONT_DEFSKILLEXTENDITEM::mapped_type kValue;

		(*iter).Pop(kKey);				++iter;
		for(int i = 0;i < MAX_SKILLEXTENDITEM_NUM;++i)
		{
			int iSkillNo = 0;
			(*iter).Pop(iSkillNo);++iter;
			if(0 < iSkillNo)
			{
				kCont[kKey].kCont.insert(iSkillNo);
			}
		}
	}

	g_kTblDataMgr.SetContDef(kCont);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_NPC_TALK_MAP_MOVE(CEL::DB_RESULT& rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << _T(" DB Query failed ErrorCode=") << rkResult.eRet << _T(", Query=") << rkResult.Command());
		return false;
	}

	CONT_DEFMAP const* pkDefMap = NULL;
	g_kTblDataMgr.GetContDef(pkDefMap);
	
	CONT_TBL_DEF_NPC_TALK_MAP_MOVE kCont;
	CEL::DB_DATA_ARRAY::iterator iter = rkResult.vecArray.begin();
	while(iter != rkResult.vecArray.end())
	{
		CONT_TBL_DEF_NPC_TALK_MAP_MOVE::key_type kKey;
		CONT_TBL_DEF_NPC_TALK_MAP_MOVE::mapped_type::value_type kValue;

		(*iter).Pop(kKey);						++iter;
		(*iter).Pop(kValue.kNpcGuid);			++iter;
		(*iter).Pop(kValue.iTargetGroundNo);	++iter;
		(*iter).Pop(kValue.nTargetPortalNo);	++iter;

		bool const bFindOrg = pkDefMap->end() != pkDefMap->find(kKey);
		if( !bFindOrg )
		{
			AddErrorMsg( BM::vstring() << __FL__ << L"Error GroundNo, [TB_DefNpcTalkMapMove] table in [f_GroundNo]["<<kKey<<L"]" );
		}
		
		CONT_DEFMAP::const_iterator map_iter = pkDefMap->find(kValue.iTargetGroundNo);
		if( bFindOrg
		&&	pkDefMap->end() != map_iter )
		{
			switch( (*map_iter).second.iAttr )
			{
			case GATTR_DEFAULT:
			case GATTR_VILLAGE:
			case GATTR_HOMETOWN:
			case GATTR_STATIC_DUNGEON:
				{
					CONT_TBL_DEF_NPC_TALK_MAP_MOVE::_Pairib kRet = kCont.insert( std::make_pair(kKey, CONT_TBL_DEF_NPC_TALK_MAP_MOVE::mapped_type()) );
					(*kRet.first).second.push_back( kValue );
				}break;
			default:
				{
					AddErrorMsg( BM::vstring() << __FL__ << L"Wrong Attribute [Attribute]["<<(*map_iter).second.iAttr<<L"], [TB_DefNpcTalkMapMove] table in [f_GroundNo]["<<kKey<<L"] [f_TargetGroundNo]["<<kValue.iTargetGroundNo<<L"]"  );
				}break;
			}
		}
		else
		{
			if( pkDefMap->end() == map_iter )
			{
				AddErrorMsg( BM::vstring() << __FL__ << L"Error [f_TargetGroundNo]["<<kValue.iTargetGroundNo<<L"], [TB_DefNpcTalkMapMove] table in [f_GroundNo]["<<kKey<<L"]" );
			}
		}
	}

	g_kTblDataMgr.SetContDef(kCont);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_SPECIFIC_REWARD(CEL::DB_RESULT& rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << _T(" DB Query failed ErrorCode=") << rkResult.eRet << _T(", Query=") << rkResult.Command());
		return false;
	}

	CONT_DEF_SPECIFIC_REWARD kCont;
	CEL::DB_DATA_ARRAY::iterator iter = rkResult.vecArray.begin();
	while( iter != rkResult.vecArray.end() )
	{	
		CONT_DEF_SPECIFIC_REWARD::mapped_type kValue;
		
		(*iter).Pop(kValue.iType);			++iter;
		(*iter).Pop(kValue.iSubType);		++iter;
		(*iter).Pop(kValue.iRewardItemNo);	++iter;
		(*iter).Pop(kValue.i64RewardCount);	++iter;

		kCont.insert( std::make_pair( POINT2(kValue.iType, kValue.iSubType), kValue ) );
	}

	g_kTblDataMgr.SetContDef(kCont);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_JOBSKILL_PROBABILITY(CEL::DB_RESULT& rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << _T(" DB Query failed ErrorCode=") << rkResult.eRet << _T(", Query=") << rkResult.Command());
		return false;
	}

	CONT_PROBABILITY::value_type kNewProbability;
	CONT_DEF_JOBSKILL_PROBABILITY_BAG::mapped_type kNewElement;

	CONT_DEF_JOBSKILL_PROBABILITY_BAG kContMap;
	CEL::DB_DATA_ARRAY::iterator iter = rkResult.vecArray.begin();
	while( rkResult.vecArray.end() != iter )
	{
		int iType = 0;
		(*iter).Pop(kNewElement.iNo);					++iter;
		(*iter).Pop(iType);								++iter;
		(*iter).Pop(kNewElement.iTotalProbability);		++iter;

		kNewElement.kContProbability.clear();
		int const iMaxCount = 10;
		for( int iCur = 0; iMaxCount > iCur; ++iCur )
		{
			(*iter).Pop(kNewProbability.iResultNo);		++iter;
			(*iter).Pop(kNewProbability.iCount);		++iter;
			(*iter).Pop(kNewProbability.iProbability);	++iter;
			kNewElement.Add( kNewProbability );
		}
		(*iter).Pop(kNewElement.i01NeedSaveIdx);		++iter;

		kNewElement.eType = static_cast< EProbablityType >(iType);
		CONT_DEF_JOBSKILL_PROBABILITY_BAG::_Pairib kRet = kContMap.insert( std::make_pair(kNewElement.iNo, kNewElement) );
		if( !kRet.second )
		{
			DBCacheUtil::AddErrorMsg( BM::vstring() << __FL__ << L"Duplicate ProbabilityNo["<<kNewElement.iNo<<"] in [TB_DefJobSkill_Probability]");
		}
	}

	g_kTblDataMgr.SetContDef(kContMap);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_JOBSKILL_ITEMUPGRADE(CEL::DB_RESULT &rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << _T(" DB Query failed ErrorCode=") << rkResult.eRet << _T(", Query=") << rkResult.Command());
		return false;
	}
	
	CONT_DEF_JOBSKILL_ITEM_UPGRADE kContMap;
	CEL::DB_DATA_ARRAY::iterator iter = rkResult.vecArray.begin();
	while( rkResult.vecArray.end() != iter )
	{
		CONT_DEF_JOBSKILL_ITEM_UPGRADE::mapped_type kNewElement;
		(*iter).Pop(kNewElement.iItemNo);					++iter;
		(*iter).Pop(kNewElement.iGrade);					++iter;
		(*iter).Pop(kNewElement.iResourceGroupNo);			++iter;
		(*iter).Pop(kNewElement.iUpgradeCount);				++iter;
		(*iter).Pop(kNewElement.iUpgradeTime);				++iter;
		(*iter).Pop(kNewElement.iBasicExpertness);			++iter;
		(*iter).Pop(kNewElement.iGatherType);				++iter;
		(*iter).Pop(kNewElement.iMachine_UseDuration);		++iter;
		(*iter).Pop(kNewElement.iResultProbabilityNo);		++iter;
		(*iter).Pop(kNewElement.iResourceProbabilityUp);	++iter;
		(*iter).Pop(kNewElement.iErrorStateTimeAbsolute);	++iter;
		(*iter).Pop(kNewElement.iErrorStateMaxProbability);	++iter;
		(*iter).Pop(kNewElement.iErrorStateProbability);	++iter;
		(*iter).Pop(kNewElement.iNeedSaveIdx);				++iter;
		(*iter).Pop(kNewElement.iResourceType);				++iter;
		(*iter).Pop(kNewElement.iExpertnessUpVolume);		++iter;
		
		CONT_DEF_JOBSKILL_ITEM_UPGRADE::_Pairib kRet = kContMap.insert( std::make_pair(kNewElement.iItemNo, kNewElement) );
		if( !kRet.second )
		{
			DBCacheUtil::AddErrorMsg( BM::vstring() << __FL__ << L"Duplicate ItemNo["<<kNewElement.iItemNo<<"] in [TB_DefJobSkill_ItemUpgrade]");
		}
	}

	g_kTblDataMgr.SetContDef(kContMap);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_JOBSKILL_MACHINE(CEL::DB_RESULT &rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << _T(" DB Query failed ErrorCode=") << rkResult.eRet << _T(", Query=") << rkResult.Command());
		return false;
	}
	
	CONT_DEF_JOBSKILL_MACHINE kContMap;
	CEL::DB_DATA_ARRAY::iterator iter = rkResult.vecArray.begin();
	while( rkResult.vecArray.end() != iter )
	{
		CONT_DEF_JOBSKILL_MACHINE::mapped_type kNewElement;
		(*iter).Pop(kNewElement.iItemNo);								++iter;
		(*iter).Pop(kNewElement.iGatherType);							++iter;
		(*iter).Pop(kNewElement.iNeedJobSkillNo01);						++iter;
		(*iter).Pop(kNewElement.iNeedJobSkillExpertness01);				++iter;
		(*iter).Pop(kNewElement.iNeedJobSkillNo02);						++iter;
		(*iter).Pop(kNewElement.iNeedJobSkillExpertness02);				++iter;
		(*iter).Pop(kNewElement.iNeedJobSkillNo03);						++iter;
		(*iter).Pop(kNewElement.iNeedJobSkillExpertness03);				++iter;
		(*iter).Pop(kNewElement.iOptionTurnTime);						++iter;
		(*iter).Pop(kNewElement.iSlotCount);							++iter;

		CONT_DEF_JOBSKILL_MACHINE::_Pairib kRet = kContMap.insert( std::make_pair(kNewElement.iItemNo, kNewElement) );
		if( !kRet.second )
		{
			DBCacheUtil::AddErrorMsg( BM::vstring() << __FL__ << L"Duplicate ItemNo["<<kNewElement.iItemNo<<"] in [TB_DefJobSkill_Machine]");
		}
	}

	g_kTblDataMgr.SetContDef(kContMap);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_JOBSKILL_MACHINESTATUS(CEL::DB_RESULT &rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << _T(" DB Query failed ErrorCode=") << rkResult.eRet << _T(", Query=") << rkResult.Command());
		return false;
	}
	
	CONT_DEF_JOBSKILL_MACHINESTATUS kContMap;
	CEL::DB_DATA_ARRAY::iterator iter = rkResult.vecArray.begin();
	while( rkResult.vecArray.end() != iter )
	{
		CONT_DEF_JOBSKILL_MACHINESTATUS::key_type kkey;
		CONT_DEF_JOBSKILL_MACHINESTATUS::mapped_type kNewElement;
		(*iter).Pop(kkey.iGatherType);							++iter;
		(*iter).Pop(kkey.iItemGrade);							++iter;
		(*iter).Pop(kNewElement.iMachineStatus);				++iter;
		(*iter).Pop(kNewElement.iMachineFromStatus);			++iter;
		(*iter).Pop(kNewElement.iStatusProbability);			++iter;
		(*iter).Pop(kNewElement.iStatusTotalProbability);		++iter;
		(*iter).Pop(kNewElement.iStatusTickTime);				++iter;
		(*iter).Pop(kNewElement.kModelXmlPath);					++iter;
		(*iter).Pop(kNewElement.iUncommonTxt);					++iter;
		(*iter).Pop(kNewElement.iRepairTxt);					++iter;

		CONT_DEF_JOBSKILL_MACHINESTATUS::_Pairib kRet = kContMap.insert( std::make_pair(kkey, kNewElement) );
		if( !kRet.second )
		{
			DBCacheUtil::AddErrorMsg( BM::vstring() << __FL__ << L"Duplicate Key["<< kkey.iGatherType << L", " << kkey.iItemGrade <<"] in [TB_DefJobSkill_MachineStatus]");
		}
	}

	g_kTblDataMgr.SetContDef(kContMap);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}

bool PgDBCache::Q_DQT_LOAD_DEF_JOBSKILL_SAVEIDX(CEL::DB_RESULT &rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << _T(" DB Query failed ErrorCode=") << rkResult.eRet << _T(", Query=") << rkResult.Command());
		return false;
	}
	
	CONT_DEF_JOBSKILL_SAVEIDX kContMap;
	CEL::DB_DATA_ARRAY::iterator iter = rkResult.vecArray.begin();
	while( rkResult.vecArray.end() != iter )
	{
		CONT_DEF_JOBSKILL_SAVEIDX::mapped_type kNewElement;
		(*iter).Pop(kNewElement.iSaveIdx);						++iter;
		(*iter).Pop(kNewElement.iRecipeItemNo);					++iter;
		(*iter).Pop(kNewElement.iResourceGroupNo);				++iter;
		(*iter).Pop(kNewElement.iNeedSkillNo01);				++iter;
		(*iter).Pop(kNewElement.iNeedSkillExpertness01);		++iter;
		(*iter).Pop(kNewElement.iNeedSkillNo02);				++iter;
		(*iter).Pop(kNewElement.iNeedSkillExpertness02);		++iter;
		(*iter).Pop(kNewElement.iNeedSkillNo03);				++iter;
		(*iter).Pop(kNewElement.iNeedSkillExpertness03);		++iter;
		(*iter).Pop(kNewElement.iRecipePrice);					++iter;
		(*iter).Pop(kNewElement.iBookItemNo);					++iter;
		(*iter).Pop(kNewElement.iAlreadyLearn);					++iter;
		(*iter).Pop(kNewElement.iShowUser);						++iter;

		CONT_DEF_JOBSKILL_SAVEIDX::_Pairib kRet = kContMap.insert( std::make_pair(kNewElement.iSaveIdx, kNewElement) );
		if( !kRet.second )
		{
			DBCacheUtil::AddErrorMsg( BM::vstring() << __FL__ << L"Duplicate SaveIdx["<< kNewElement.iSaveIdx <<"] in [TB_DefJobSkill_SaveIdx]");
		}
	}

	g_kTblDataMgr.SetContDef(kContMap);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}

bool PgDBCache::Q_LOAD_DEF_JOBSKILL_LOCATIONITEM(CEL::DB_RESULT& rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_DEF_JOBSKILL_LOCATIONITEM map;
	CEL::DB_RESULT_COUNT::const_iterator count_iter = rkResult.vecResultCount.begin();

	if(count_iter == rkResult.vecResultCount.end())
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	int const iCount = (*count_iter); ++count_iter;

	for(int i = 0;i < iCount;++i)
	{
		CONT_DEF_JOBSKILL_LOCATIONITEM::key_type		kKey;
		CONT_DEF_JOBSKILL_LOCATIONITEM::mapped_type		kValue;

		result_iter->Pop( kKey );							++result_iter;
		result_iter->Pop( kValue.iGatherType );				++result_iter;
		result_iter->Pop( kValue.iSpot_Probability );		++result_iter;
		result_iter->Pop( kValue.iSpot_TotalProbability );	++result_iter;
		result_iter->Pop( kValue.iSpot_Cycle_Min );			++result_iter;
		result_iter->Pop( kValue.iSpot_Cycle_Max );			++result_iter;
		result_iter->Pop( kValue.iSpot_Dration_Min );		++result_iter;
		result_iter->Pop( kValue.iSpot_Dration_Max );		++result_iter;
		for(int iCount = 0; iCount < 10; iCount++)
		{
			result_iter->Pop( kValue.iResultProbability_No[iCount] );	++result_iter;
		}
		result_iter->Pop( kValue.iBase_Expertness );	++result_iter;		
		result_iter->Pop( kValue.i01Need_SaveIdx );		++result_iter;
		result_iter->Pop( kValue.i02Need_SaveIdx );		++result_iter;
		result_iter->Pop( kValue.i03Need_SaveIdx );		++result_iter;
		result_iter->Pop( kValue.i01Need_Skill_No );		++result_iter;
		result_iter->Pop( kValue.i01Need_Expertness );		++result_iter;
		result_iter->Pop( kValue.i02Need_Skill_No );		++result_iter;
		result_iter->Pop( kValue.i02Need_Expertness );		++result_iter;
		result_iter->Pop( kValue.i03Need_Skill_No );		++result_iter;
		result_iter->Pop( kValue.i03Need_Expertness );		++result_iter;
		result_iter->Pop( kValue.iProbability_UpRate );	++result_iter;

		CONT_DEF_JOBSKILL_LOCATIONITEM::_Pairib kRet = map.insert( std::make_pair(kKey, kValue) );
		if( !kRet.second )
		{
			VERIFY_INFO_LOG( false , BM::LOG_LV0, __FL__ << _T("Data Error!!!") );
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Insert Failed DefJobSkill_LocationItem"));
			return false;
		}
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_LOAD_DEF_JOBSKILL_SKILL(CEL::DB_RESULT& rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_DEF_JOBSKILL_SKILL map;
	CEL::DB_RESULT_COUNT::const_iterator count_iter = rkResult.vecResultCount.begin();

	if(count_iter == rkResult.vecResultCount.end())
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	int const iCount = (*count_iter); ++count_iter;

	for(int i = 0;i < iCount;++i)
	{
		CONT_DEF_JOBSKILL_SKILL::key_type			kKey;
		CONT_DEF_JOBSKILL_SKILL::mapped_type		kValue;
		int iTempValue = 0;

		result_iter->Pop( kKey );										++result_iter;
		result_iter->Pop( kValue.iGatherType );							++result_iter;
		result_iter->Pop( kValue.eJobSkill_Type );						++result_iter;
		result_iter->Pop( kValue.i01NeedParent_JobSkill_No );			++result_iter;
		result_iter->Pop( kValue.i01NeedParent_JobSkill_Expertness );	++result_iter;
		result_iter->Pop( kValue.i02NeedParent_JobSkill_No );			++result_iter;
		result_iter->Pop( kValue.i02NeedParent_JobSkill_Expertness );	++result_iter;
		result_iter->Pop( kValue.i03NeedParent_JobSkill_No );			++result_iter;
		result_iter->Pop( kValue.i03NeedParent_JobSkill_Expertness );	++result_iter;

		CONT_DEF_JOBSKILL_SKILL::_Pairib kRet = map.insert( std::make_pair(kKey, kValue) );
		if( !kRet.second )
		{
			VERIFY_INFO_LOG( false , BM::LOG_LV0, __FL__ << _T("Data Error!!!") );
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Insert Failed DefJobSkill_Skill"));
			return false;
		}
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_LOAD_DEF_JOBSKILL_SKILLEXPERTNESS(CEL::DB_RESULT& rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_DEF_JOBSKILL_SKILLEXPERTNESS kContList;
	CEL::DB_RESULT_COUNT::const_iterator count_iter = rkResult.vecResultCount.begin();

	if(count_iter == rkResult.vecResultCount.end())
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	int const iCount = (*count_iter); ++count_iter;

	for(int i = 0;i < iCount;++i)
	{
		CONT_DEF_JOBSKILL_SKILLEXPERTNESS::value_type		kValue;

		result_iter->Pop( kValue.iSkillNo );							++result_iter;
		result_iter->Pop( kValue.iSkill_Expertness_Min );				++result_iter;
		result_iter->Pop( kValue.iSkill_Expertness_Max );				++result_iter;
		result_iter->Pop( kValue.iExpertness_Gain_Min );				++result_iter;
		result_iter->Pop( kValue.iExpertness_Gain_Max );				++result_iter;
		result_iter->Pop( kValue.iExpertness_Gain_Probability );		++result_iter;
		result_iter->Pop( kValue.iExpertness_Gain_TotalProbability );	++result_iter;
		result_iter->Pop( kValue.iUse_Duration );						++result_iter;
		result_iter->Pop( kValue.iUse_Duration_Probability );			++result_iter;
		result_iter->Pop( kValue.iUse_Duration_TotalProbability );		++result_iter;
		result_iter->Pop( kValue.iBasic_Turn_Time );					++result_iter;
		result_iter->Pop( kValue.iMax_Exhaustion );						++result_iter;
		result_iter->Pop( kValue.iUse_Exhaustion );						++result_iter;
		result_iter->Pop( kValue.iMax_Machine );						++result_iter;
		result_iter->Pop( kValue.iNeed_CharLevel );						++result_iter;
		result_iter->Pop( kValue.iSkill_Probability );					++result_iter;

		if( kContList.end() != std::find(kContList.begin(), kContList.end(), kValue) )
		{
			DBCacheUtil::AddErrorMsg( BM::vstring() << __FL__ << _T("Warning!! Insert Failed DefJobSkill_SkillExpertness[SkillNo:"<<kValue.iSkillNo<<L", Min:"<<kValue.iSkill_Expertness_Min<<L",Max:"<<kValue.iExpertness_Gain_Max<<L"]") );
			return false;
		}
		else
		{
			kContList.push_back( kValue );
			kContList.sort();
		}
	}

	if(!kContList.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kContList);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_LOAD_DEF_JOBSKILL_TOOL(CEL::DB_RESULT& rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_DEF_JOBSKILL_TOOL map;
	CEL::DB_RESULT_COUNT::const_iterator count_iter = rkResult.vecResultCount.begin();

	if(count_iter == rkResult.vecResultCount.end())
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	int const iCount = (*count_iter); ++count_iter;

	for(int i = 0;i < iCount;++i)
	{
		CONT_DEF_JOBSKILL_TOOL::key_type			kKey;
		CONT_DEF_JOBSKILL_TOOL::mapped_type		kValue;

		result_iter->Pop( kKey );										++result_iter;
		result_iter->Pop( kValue.iToolType );							++result_iter;
		result_iter->Pop( kValue.iGatherType );							++result_iter;
		result_iter->Pop( kValue.i01Need_Skill_No );					++result_iter;
		result_iter->Pop( kValue.i01Need_Skill_Expertness );			++result_iter;
		result_iter->Pop( kValue.i02Need_Skill_No );					++result_iter;
		result_iter->Pop( kValue.i02Need_Skill_Expertness );			++result_iter;
		result_iter->Pop( kValue.i03Need_Skill_No );					++result_iter;
		result_iter->Pop( kValue.i03Need_Skill_Expertness );			++result_iter;
		result_iter->Pop( kValue.iGetCount );							++result_iter;
		result_iter->Pop( kValue.iGetCountMax );						++result_iter;
		result_iter->Pop( kValue.iOption_TurnTime );					++result_iter;
		result_iter->Pop( kValue.iOption_CoolTime );					++result_iter;
		result_iter->Pop( kValue.iOption_User_Exhaustion );				++result_iter;
		result_iter->Pop( kValue.iResult_No_Min );						++result_iter;
		result_iter->Pop( kValue.iResult_No_Max );						++result_iter;

		CONT_DEF_JOBSKILL_TOOL::_Pairib kRet = map.insert( std::make_pair( kKey, kValue ) );
		if( !kRet.second )
		{
			VERIFY_INFO_LOG( false , BM::LOG_LV0, __FL__ << _T("Data Error!!!") );
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Insert Failed DefJobSkill_Tool"));
			return false;
		}
	}

 	if(!map.empty())
 	{
 		g_kCoreCenter.ClearQueryResult(rkResult);
 		g_kTblDataMgr.SetContDef(map);
 		return true;
 	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_LOAD_DEF_JOBSKILL_SHOP(CEL::DB_RESULT& rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
	&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_DEF_JOBSKILL_SHOP kContMap;
	while( rkVec.end() != result_iter )
	{
		CONT_DEF_JOBSKILL_SHOP::mapped_type kNewElement;
		result_iter->Pop( kNewElement.iIDX );			++result_iter;
		result_iter->Pop( kNewElement.iCategory );		++result_iter;
		result_iter->Pop( kNewElement.iCP );			++result_iter;
		result_iter->Pop( kNewElement.iItemNo );		++result_iter;
		result_iter->Pop( kNewElement.iPrice );			++result_iter;
		result_iter->Pop( kNewElement.kShopGuid );		++result_iter;
		result_iter->Pop( kNewElement.bTimeType );		++result_iter;
		result_iter->Pop( kNewElement.iUseTime );		++result_iter;

		if( 0 < kNewElement.iIDX )
		{
			CONT_DEF_JOBSKILL_SHOP::_Pairib kRet = kContMap.insert( std::make_pair(kNewElement.iIDX, kNewElement) );
			if( !kRet.second )
			{
				VERIFY_INFO_LOG( false , BM::LOG_LV0, __FL__ << _T("Data Error!!!") );
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Insert Failed TB_DefJobSkill_Shop"));
				return false;
			}
		}
	}

	if(!kContMap.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kContMap);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_LOAD_DEF_JOBSKILL_RECIPE(CEL::DB_RESULT &rkResult)
{
	if(CEL::DR_SUCCESS != rkResult.eRet && CEL::DR_NO_RESULT != rkResult.eRet)
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << _T(" DB Query failed ErrorCode=") << rkResult.eRet << _T(", Query=") << rkResult.Command());
		return false;
	}
	
	CONT_DEF_JOBSKILL_RECIPE kContMap;
	CEL::DB_DATA_ARRAY::iterator iter = rkResult.vecArray.begin();
	while( rkResult.vecArray.end() != iter )
	{
		CONT_DEF_JOBSKILL_RECIPE::mapped_type kNewElement;
		(*iter).Pop(kNewElement.iItemNo);					++iter;
		(*iter).Pop(kNewElement.iOrderID);					++iter;
		(*iter).Pop(kNewElement.iNeedSkillNo);				++iter;
		(*iter).Pop(kNewElement.iNeedSkillExpertness);		++iter;
		for(int i=0; i<MAX_JS3_RECIPE_RES; ++i)
		{
			(*iter).Pop(kNewElement.kResource[i].iGroupNo);		++iter;
			(*iter).Pop(kNewElement.kResource[i].iGrade);		++iter;
			(*iter).Pop(kNewElement.kResource[i].iCount);		++iter;
		}
		(*iter).Pop(kNewElement.iNeedMoney);				++iter;
		(*iter).Pop(kNewElement.kProbability.iNo);			++iter;
		(*iter).Pop(kNewElement.kProbability.iRate);		++iter;
		(*iter).Pop(kNewElement.kProbability.iRateItem);	++iter;
		(*iter).Pop(kNewElement.iNeedProductPoint);			++iter;
		(*iter).Pop(kNewElement.iExpertnessGain);			++iter;

		CONT_DEF_JOBSKILL_RECIPE::_Pairib kRet = kContMap.insert( std::make_pair(kNewElement.iItemNo, kNewElement) );
		if( !kRet.second )
		{
			DBCacheUtil::AddErrorMsg( BM::vstring() << __FL__ << L"Duplicate ItemNo["<< kNewElement.iItemNo <<"] in [TB_DefJobSkill_Recipe]");
		}
	}

	g_kTblDataMgr.SetContDef(kContMap);
	g_kCoreCenter.ClearQueryResult(rkResult);
	return true;
}

bool PgDBCache::Q_LOAD_DEF_SOCKET_ITEM(CEL::DB_RESULT& rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_DEF_SOCKET_ITEM map;
	CEL::DB_RESULT_COUNT::const_iterator count_iter = rkResult.vecResultCount.begin();

	if(count_iter == rkResult.vecResultCount.end())
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	int const iCount = (*count_iter); ++count_iter;

	for(int i = 0;i < iCount;++i)
	{
		CONT_DEF_SOCKET_ITEM::key_type			kKey;
		CONT_DEF_SOCKET_ITEM::mapped_type		kValue;

		result_iter->Pop( kKey.iEquipPos );					++result_iter;
		result_iter->Pop( kKey.iLevelLimit );				++result_iter;
		result_iter->Pop( kKey.iSocket_Order );				++result_iter;
		result_iter->Pop( kValue.iSuccessRate );			++result_iter;
		result_iter->Pop( kValue.iSoulItemCount );			++result_iter;
		result_iter->Pop( kValue.iNeedMoney );				++result_iter;
		result_iter->Pop( kValue.iSocketItemNo );			++result_iter;
		result_iter->Pop( kValue.iRestorationItemNo );		++result_iter;
		result_iter->Pop( kValue.iResetItemNo );			++result_iter;		

		CONT_DEF_SOCKET_ITEM::_Pairib kRet = map.insert( std::make_pair(kKey, kValue) );
		if( !kRet.second )
		{
			VERIFY_INFO_LOG( false , BM::LOG_LV0, __FL__ << _T("Data Error!!!") );
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Insert Failed DefSocketItem"));
			return false;
		}
	}

	if(!map.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(map);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::Q_DQT_LOAD_DEF_JOBSKILL_ITEMSOULEXTRACT(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_DEF_JOBSKILL_SOUL_EXTRACT kContList;
	CEL::DB_RESULT_COUNT::const_iterator count_iter = rkResult.vecResultCount.begin();

	if(count_iter == rkResult.vecResultCount.end())
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	int const iCount = (*count_iter); ++count_iter;

	for(int i = 0;i < iCount;++i)
	{
		CONT_DEF_JOBSKILL_SOUL_EXTRACT::value_type		kValue;

		result_iter->Pop( kValue.iEquipPos );							++result_iter;
		result_iter->Pop( kValue.iLevelLimit );							++result_iter;
		result_iter->Pop( kValue.iSuccessRate );						++result_iter;
		result_iter->Pop( kValue.iExtractItemNo );						++result_iter;
		result_iter->Pop( kValue.iResultItemNo );						++result_iter;

		if( kContList.end() != std::find(kContList.begin(), kContList.end(), kValue) )
		{
			DBCacheUtil::AddErrorMsg( BM::vstring() << __FL__ << _T("Warning!! Insert Failed DefItemSoulExtract") );
			return false;
		}
		else
		{
			kContList.push_back( kValue );
			kContList.sort();
		}
	}

	if(!kContList.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kContList);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}
bool PgDBCache::Q_DQT_LOAD_DEF_JOBSKILL_ITEMSOULTRANSITION(CEL::DB_RESULT &rkResult)
{
	if( CEL::DR_SUCCESS != rkResult.eRet
		&&	CEL::DR_NO_RESULT != rkResult.eRet )
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CEL::DB_DATA_ARRAY const &rkVec = rkResult.vecArray;
	CEL::DB_DATA_ARRAY::const_iterator result_iter = rkVec.begin();

	CONT_DEF_JOBSKILL_SOUL_TRANSITION kContList;
	CEL::DB_RESULT_COUNT::const_iterator count_iter = rkResult.vecResultCount.begin();

	if(count_iter == rkResult.vecResultCount.end())
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Load Fail!!"));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	int const iCount = (*count_iter); ++count_iter;

	for(int i = 0;i < iCount;++i)
	{
		CONT_DEF_JOBSKILL_SOUL_TRANSITION::value_type		kValue;

		result_iter->Pop( kValue.iEquipType );					++result_iter;
		result_iter->Pop( kValue.iLevelLimit );					++result_iter;
		result_iter->Pop( kValue.iRairity_Grade );				++result_iter;
		result_iter->Pop( kValue.iSuccessRate );				++result_iter;
		result_iter->Pop( kValue.iSoulItemCount );				++result_iter;
		result_iter->Pop( kValue.iNeedMoney );					++result_iter;
		result_iter->Pop( kValue.iInsuranceitemNo );			++result_iter;
		result_iter->Pop( kValue.iProbabilityUpItemNo );		++result_iter;
		result_iter->Pop( kValue.iProbabilityUpRate );			++result_iter;
		result_iter->Pop( kValue.iProbabilityUpItemCount );		++result_iter;

		if( kContList.end() != std::find(kContList.begin(), kContList.end(), kValue) )
		{
			DBCacheUtil::AddErrorMsg( BM::vstring() << __FL__ << _T("Warning!! Insert Failed DefItemSoulTransition") );
			return false;
		}
		else
		{
			kContList.push_back( kValue );
			kContList.sort();
		}
	}

	if(!kContList.empty())
	{
		g_kCoreCenter.ClearQueryResult(rkResult);
		g_kTblDataMgr.SetContDef(kContList);
		return true;
	}	

	VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"row count 0");
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgDBCache::TableDataQuery( bool bReload )
{// 이코드 지저분하게 만들지 마시오.

	//CTableDataManager kTempTDM;
	//g_kTblDataMgr.swap(kTempTDM, bReload);	//이전 데이터 백업.
	//g_kTblDataMgr.Clear(bReload);//테이블 날리고

	std::wstring kStrQuery;
	bool bIsImmidiate = true;
	{	CEL::DB_QUERY kQuery( DT_LOCAL, DQT_LOAD_DEF_CHANNEL_EFFECT, _T("EXEC [dbo].[up_LoadChannelEffect]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_LOCAL, DQT_DEFSTRINGS, _T("EXEC [dbo].[UP_LoadDefStrings]"));							if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_LOCAL, DQT_LOAD_DEF_MYHOME_TEX, _T("EXEC [dbo].[up_LoadDefMyHomeTex]"));				if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_LOCAL, DQT_DEF_FILTER_UNICODE, _T("EXEC [dbo].[UP_LoadDefFilterUnicode]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_LOCAL, DQT_LOAD_CARD_LOCAL, _T("EXEC [dbo].[up_LoadCardLocal]"));						if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}	
	{	CEL::DB_QUERY kQuery( DT_LOCAL, DQT_LOAD_DEF_SPECIFIC_REWARD, _T("EXEC [dbo].[up_LoadDefSpecificReward]"));		if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_ABIL_TYPE, _T("EXEC [dbo].[UP_LoadDefAbilType]"));						if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFCLASS, _T("EXEC [dbo].[UP_LoadDefClass]"));								if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFCLASS_ABIL, _T("EXEC [dbo].[UP_LoadDefClassAbil]")); 						if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFCLASS_PET, _T("EXEC [dbo].[UP_LoadDefClassPet]"));							if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFCLASS_PET_LEVEL, _T("EXEC [dbo].[UP_LoadDefClassPetLevel]"));				if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFCLASS_PET_SKILL, _T("EXEC [dbo].[UP_LoadDefClassPetSkills]"));				if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFCLASS_PET_ITEMOPTION, _T("EXEC [dbo].[up_LoadDefClassPetItemOption]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFCLASS_PET_ABIL, _T("EXEC [dbo].[UP_LoadDefClassPetAbil]")); 				if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFITEM, _T("EXEC [dbo].[UP_LoadDefItem2]"));									if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFITEM_RES_CONVERT, _T("EXEC [dbo].[UP_LoadDefResConvert]"));				if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFITEMABIL, _T("EXEC [dbo].[UP_LoadDefItemAbil]"));							if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFITEMBAG, _T("EXEC [dbo].[UP_LoadDefItemBag]"));							if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFITEMCONTAINER, _T("EXEC [dbo].[UP_LoadDefItemContainer2]"));				if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_DROP_MONEY_CONTROL, _T("EXEC [dbo].[UP_LoadDefDropMoneyControl]"));		if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFITEMRARE, _T("EXEC [dbo].[UP_LoadDefItemRare]"));							if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFITEMRAREGROUP, _T("EXEC [dbo].[UP_LoadDefItemRareGroup]"));				if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFMAP, _T("EXEC [dbo].[UP_LoadDefMap]"));									if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFMAPITEM, _T("EXEC [dbo].[UP_LoadDefMapItem]"));							if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFMAPMONSTERREGEN, _T("EXEC [dbo].[UP_LoadDefMapRegenPoint2]"));				if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFMONSTER, _T("EXEC [dbo].[UP_LoadDefMonster]"));							if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFMONSTERABIL, _T("EXEC [dbo].[UP_LoadDefMonsterAbil]"));					if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFMONSTERTUNNING, _T("EXEC [dbo].[UP_LoadDefMonsterTunning]"));				if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFNPC, _T("EXEC [dbo].[UP_LoadDefNPC]"));									if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFNPCABIL, _T("EXEC [dbo].[UP_LoadDefNPCAbil]"));							if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFSKILL, _T("EXEC [dbo].[UP_LoadDefSkill]"));								if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFSKILLABIL, _T("EXEC [dbo].[UP_LoadDefSkillAbil2]"));						if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFSKILLSET, _T("EXEC [dbo].[UP_LoadDefSkillSet]"));							if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFEFFECT, _T("EXEC [dbo].[UP_LoadDefEffect2]")); 							if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFEFFECTABIL, _T("EXEC [dbo].[UP_LoadDefEffectAbil2]")); 					if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFRES, _T("EXEC [dbo].[UP_LoadDefRes]"));									if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFUPGRADECLASS, _T("EXEC [dbo].[UP_LoadDefUpgradeClass]")); 					if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFITEMENCHANT, _T("EXEC [dbo].[UP_LoadDefItemEnchant]"));					if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFCHARACTER_BASEWEAR, _T("EXEC [dbo].[UP_LoadDefCharacterBaseWear]"));		if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}	
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_ITEM_PLUS_UPGRADE, _T("EXEC [dbo].[UP_LoadDefItemPlusUpgrade]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_ITEM_ENCHANT_SHIFT, _T("EXEC [dbo].[UP_LoadDefItemEnchantShift]"));		if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_SUCCESS_RATE_CONTROL, _T("EXEC [dbo].[UP_LoadDefSuccessRateControl]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFMONSTERBAG, _T("EXEC [dbo].[UP_LoadDefMonsterBag]"));						if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}	
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_COUNT_CONTROL, _T("EXEC [dbo].[UP_LoadDefCountControl]"));				if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_ITEM_BAG_ELEMENTS, _T("EXEC [dbo].[UP_LoadDefItemBagElements]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_MONSTER_BAG_ELEMENTS, _T("EXEC [dbo].[UP_LoadDefMonsterBagElements]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}	
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_OBJECT, _T("EXEC [dbo].[UP_LoadDefObject]"));								if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_OBJECTABIL, _T("EXEC [dbo].[UP_LoadDefObjectAbil]"));						if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}	
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_ITEM_BY_LEVEL, _T("EXEC [dbo].[up_LoadDefItemByLevel]"));					if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_PVP_GROUNDMODE, _T("EXEC [dbo].[UP_LoadDefPvPGroundMode]"));				if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}	
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFITEMMAKING, _T("EXEC [dbo].[UP_LoadDefItemMaking]"));						if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEFRESULTCONTROL, _T("EXEC [dbo].[UP_LoadDefResultControl]"));				if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_QUEST_REWARD, _T("EXEC [dbo].[UP_LoadDefQuestReward]"));					if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_QUEST_MIGRATION, _T("EXEC [dbo].[UP_LoadDefQuest_Migration]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_QUEST_RANDOM_EXP, _T("EXEC [dbo].[UP_LoadDefQuestRandomExp]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_QUEST_RANDOM_TACTICS_EXP, _T("EXEC [dbo].[UP_LoadDefQuestRandomTacticsExp]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_QUEST_RESET_SCHEDULE, _T("EXEC [dbo].[UP_LoadDefQuestResetSchedule]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}		
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_ITEM_SET, _T("EXEC [dbo].[UP_LoadDefItemSet]"));							if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_SPEND_MONEY, _T("EXEC [dbo].[UP_LoadDefSpendMoney]"));					if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_GUILD_LEVEL, L"EXEC [dbo].[UP_LoadDefGuildLevel]");						if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_GUILD_SKILL, L"EXEC [dbo].[UP_LoadDefGuildSkill]");						if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_ITEM_OPTION, _T("EXEC [dbo].[UP_LoadDefItemOpt]"));						if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_ITEM_OPTION_ABIL, _T("EXEC [dbo].[UP_LoadDefItemOptionAbil]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_ITEM_RARITY_UPGRADE, _T("EXEC [dbo].[UP_LoadDefItemRarityUpgrade2]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}	
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_PROPERTY, _T("EXEC [dbo].[UP_LoadDefProperty2]"));						if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_ITEM_BAG_GROUP, _T("EXEC [dbo].[UP_LoadDefItemBagGroup]"));				if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}	
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_FIVE_ELEMENT_INFO, _T("EXEC [dbo].[UP_LoadDefFiveElementInfo]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}		
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_TACTICS_LEVEL, _T("EXEC [dbo].[UP_LoadDefTacticsLevel]"));				if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}	
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_ACHIEVEMENTS, _T("EXEC [dbo].[UP_LOADDEFACHIEVEMENTS]"));					if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_LOAD_RECOMMENDATIONITEM, _T("EXEC [dbo].[up_LoadDefRecommendationItem]"));if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}	
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_RARE_MONSTER_SPEECH, _T("EXEC [dbo].[UP_LoadDefRareMonsterSpeech]"));		if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}		
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_MONSTERCARD, _T("EXEC [dbo].[up_LoadDefMonsterCard]"));					if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_MARRYTEXT, _T("EXEC [dbo].[up_LoadDefMarryText]"));						if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_HIDDENREWORDITEM, _T("EXEC [dbo].[up_LoadDefHiddenRewordItem]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_HIDDENREWORDBAG, _T("EXEC [dbo].[up_LoadDefHiddenRewordBag]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_EMOTION, _T("EXEC [dbo].[UP_LoadDefEmotionGroup]"));						if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}	
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_CONVERTITEM, _T("EXEC [dbo].[up_LoadDefConvertItem]"));					if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_PET_HATCH, _T("EXEC [dbo].[up_LoadDefPet_Hatch]"));						if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_HOMETOWNTOMAPCOST, _T("EXEC [dbo].[up_LoadDefHometownToMapCost]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}	
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_SKILLIDX_TO_SKILLNO, _T("EXEC [dbo].[up_LoadDefSkillIdxToSkillNo]"));if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_LOAD_CARDABIL, _T("EXEC [dbo].[up_LoadDefCardAbil]"));					if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_CARD_KEY_STRING, _T("EXEC [dbo].[up_LoadDefCardAbilKey]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_GROUND_RARE_MONSTER, _T("EXEC [dbo].[UP_LoadDefRareMonsterRegen]"));		if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_MONSTER_BAG_CONTROL, _T("EXEC [dbo].[UP_LoadDefMapMonsterBagControl2]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_MISSION_ROOT, _T("EXEC [dbo].[UP_LoadDefMissionLevels]"));				if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_MISSION_CANDIDATE, _T("EXEC [dbo].[UP_LoadDefMissionLevelCandidate]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}	
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_MISSION_DEFENCE_STAGE, _T("EXEC [dbo].[up_LoadDefDefenceStage]"));		if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}	
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_MISSION_DEFENCE_WAVE, _T("EXEC [dbo].[up_LoadDefDefenceWave]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}		
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_MISSION_DEFENCE7_MISSION, _T("EXEC [dbo].[up_LoadDefDefence7_Mission]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}		
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_MISSION_DEFENCE7_STAGE, _T("EXEC [dbo].[up_LoadDefDefence7_Stage]"));		if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}		
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_MISSION_DEFENCE7_WAVE, _T("EXEC [dbo].[up_LoadDefDefence7_Wave]"));		if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}		
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_MISSION_DEFENCE7_GUARDIAN, _T("EXEC [dbo].[up_LoadDefDefence7_Guardian]"));		if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}		
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_MISSION_BONUSMAP, _T("EXEC [dbo].[up_LoadDefMissionBonusMap]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}		
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_EMPORIA, _T("EXEC [dbo].[UP_LoadDefEmporiaPack]"));						if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_ITEM_RARITY_UPGRADE_COST_RATE, _T("EXEC [dbo].[up_LoadDefItemRarityUpgradeCostRate]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_MONSTER_KILL_COUNT_REWARD, _T("EXEC [dbo].[UP_LoadDefMonsterKillCountReward]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_TRANSTOWER, _T("EXEC [dbo].[UP_LoadDefTransTower]"));						if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_PARTY_INFO, _T("EXEC [dbo].[up_LoadDefParty_Info]"));						if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_MAP_ITEM_BAG, _T("EXEC [dbo].[UP_LoadDefMapItemBag]"));					if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_OBJECT_BAG, _T("EXEC [dbo].[UP_LoadDefObjectBag]"));						if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_OBJECT_BAG_ELEMENTS, _T("EXEC [dbo].[UP_LoadDefObjectElement]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_MISSION_RESULT, _T("EXEC [dbo].[UP_LoadDefMissionResult2]"));				if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_MISSION_CLASS_REWARD, _T("EXEC [dbo].[up_LoadDefMission_ClassReward]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}	
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_MISSION_RANK_REWARD, _T("EXEC [dbo].[up_LoadDefMission_RankReward]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}	
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_CHARCARDEFFECT, _T("EXEC [dbo].[up_LoadDefCharCardEffect]"));		if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}	
	{	CEL::DB_QUERY kQuery( DT_LOCAL, DQT_LOAD_DEF_SIDEJOBRATE, _T("EXEC [dbo].[up_LoadDefSideJobRate]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}	
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_EVENTITEMSET, _T("EXEC [dbo].[UP_LoadDefEventItemSet]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_REDICEOPTIONCOST, _T("EXEC [dbo].[up_LoadDefRediceCost]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_MYHOMESIDEJOBTIME, _T("EXEC [dbo].[up_LoadDefMyHomeSidejobTime]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_MONSTER_ENCHANT_GRADE, _T("EXEC [dbo].[UP_LoadDefMonsterEnchantGrade]"));		if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_MYHOMEBUILDINGS, _T("EXEC [dbo].[up_LoadDefMyhomeBuildings]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_BASICOPTIONAMP, _T("EXEC [dbo].[up_LoadDefBasicOptionAmp]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_ALRAM_MISSION, _T("EXEC [dbo].[up_LoadDefAlramMission]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_DEATHPENALTY, _T("EXEC [dbo].[up_LoadDefDeathPenalty]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_DEF_DEFENCE_ADD_MONSTER, _T("EXEC [dbo].[up_LoadDefDefenceAddMonster]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_SKILLEXTENDITEM, _T("EXEC [dbo].[up_LoadDefSkillExtendItem]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_NPC_TALK_MAP_MOVE, _T("EXEC [dbo].[UP_LoadDefNpcTalkMapMove]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_JOBSKILL_PROBABILITY, _T("EXEC [dbo].[UP_LoadDefJobSkill_Probability]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_JOBSKILL_ITEMUPGRADE, _T("EXEC [dbo].[UP_LoadDefJobSkill_ItemUpgrade]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_JOBSKILL_MACHINE, _T("EXEC [dbo].[UP_LoadDefJobSkill_Machine]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_JOBSKILL_MACHINESTATUS, _T("EXEC [dbo].[UP_LoadDefJobSkill_MachineStatus]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_JOBSKILL_SAVEIDX, _T("EXEC [dbo].[UP_LoadDefJobSkill_SaveIdx]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_JOBSKILL_LOCATIONITEM, _T("EXEC [dbo].[up_LoadDefJobSkill_LocationItem]"));		if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_JOBSKILL_SKILL, _T("EXEC [dbo].[up_LoadDefJobSkill_Skill]"));		if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_JOBSKILL_SKILLEXPERTNESS, _T("EXEC [dbo].[up_LoadDefJobSkill_SkillExpertness]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_JOBSKILL_TOOL, _T("EXEC [dbo].[up_LoadDefJobSkill_Tool]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_JOBSKILL_RECIPE, _T("EXEC [dbo].[UP_LoadDefJobSkill_Recipe]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_SOCKET_ITEM, _T("EXEC [dbo].[up_LoadDefItemSocket]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_JOBSKILL_ITEMSOULEXTRACT, _T("EXEC [dbo].[up_LoadDefItemSoulExtract]"));			if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	{	CEL::DB_QUERY kQuery( DT_DEF, DQT_LOAD_DEF_JOBSKILL_ITEMSOULTRANSITION, _T("EXEC [dbo].[up_LoadDefItemSoulTransition]"));	if( S_OK != g_kCoreCenter.PushQuery(kQuery, bIsImmidiate) ){kStrQuery = kQuery.Command(); goto __RECORVER;}}
	return true;
__RECORVER:
	{
		//g_kTblDataMgr.Clear();
		//g_kTblDataMgr.swap(kTempTDM, bReload);	//백업한 넘으로 되돌린다.

		INFO_LOG( BM::LOG_LV1, _T("Query Fail Data Recovery... Cause = ") << kStrQuery );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}
}

bool PgDBCache::OnDBExcute(CEL::DB_RESULT &rkResult)
{
	switch( rkResult.QueryType() )
	{
	case DQT_LOAD_DEF_CHANNEL_EFFECT:			{ Q_DQT_LOAD_DEF_CHANNEL_EFFECT( rkResult ); }break;
	case DQT_DEFSTRINGS:						{ Q_DQT_DEFSTRINGS( rkResult );}break;
	case DQT_DEFCLASS:							{ Q_DQT_DEFCLASS(rkResult); }break;
	case DQT_DEFCLASS_ABIL:						{ Q_DQT_DEFCLASS_ABIL(rkResult); }break;
	case DQT_DEFCLASS_PET:						{ Q_DQT_DEFCLASS_PET(rkResult); }break;
	case DQT_DEFCLASS_PET_LEVEL:				{ Q_DQT_DEFCLASS_PET_LEVEL(rkResult); }break;
	case DQT_DEFCLASS_PET_SKILL:				{ Q_DQT_DEFCLASS_PET_SKILL(rkResult); }break;
	case DQT_DEFCLASS_PET_ITEMOPTION:			{ Q_DQT_DEFCLASS_PET_ITEMOPTION(rkResult); }break;
	case DQT_DEFCLASS_PET_ABIL:					{ Q_DQT_DEFCLASS_PET_ABIL(rkResult); }break;		
	case DQT_DEFITEMBAG:						{ Q_DQT_DEFITEMBAG(rkResult); }break;
	case DQT_DEF_DROP_MONEY_CONTROL:			{ Q_DQT_DEF_DROP_MONEY_CONTROL(rkResult); }break;
	case DQT_DEF_ABIL_TYPE:						{ Q_DQT_DEF_ABIL_TYPE(rkResult); }break;	
	case DQT_DEFITEMCONTAINER:					{ Q_DQT_DEFITEMCONTAINER(rkResult);	}break;
	case DQT_DEFITEM:							{ Q_DQT_DEFITEM(rkResult); }break;
	case DQT_DEFITEM_RES_CONVERT:				{ Q_DQT_DEFITEM_RES_CONVERT(rkResult); }break;
	case DQT_DEFITEMABIL:						{ Q_DQT_DEFITEMABIL(rkResult); }break;
	case DQT_DEFITEMRARE:						{ Q_DQT_DEFITEMRARE(rkResult); }break;
	case DQT_DEFITEMRAREGROUP:					{ Q_DQT_DEFITEMRAREGROUP(rkResult); }break;
	case DQT_DEFMAP:							{ Q_DQT_DEFMAP(rkResult); }break;
	case DQT_DEFMAPITEM:						{ Q_DQT_DEFMAPITEM(rkResult); }break;
	case DQT_DEFMAPMONSTERREGEN:				{ Q_DQT_DEFMAPMONSTERREGEN(rkResult ); }break;
	case DQT_DEFMONSTER:						{ Q_DQT_DEFMONSTER(rkResult); }break;
	case DQT_DEFMONSTERABIL:					{ Q_DQT_DEFMONSTERABIL(rkResult); }break;
	case DQT_DEFMONSTERTUNNING:					{ Q_DQT_DEFMONSTERTUNNING(rkResult); }break;
	case DQT_DEFNPC:							{ Q_DQT_DEFNPC(rkResult); }break;
	case DQT_DEFNPCABIL:						{ Q_DQT_DEFNPCABIL(rkResult); }break;	
	case DQT_DEFSKILL:							{ Q_DQT_DEFSKILL(rkResult); }break;
	case DQT_DEFSKILLABIL:						{ Q_DQT_DEFSKILLABIL(rkResult); }break;
	case DQT_DEFSKILLSET:						{ Q_DQT_DEFSKILLSET(rkResult); }break;
	case DQT_DEFRES:							{ Q_DQT_DEFRES( rkResult );}break;	
	case DQT_DEFITEMMAKING:						{ Q_DQT_DEFITEMMAKING(rkResult); }break;
	case DQT_DEFRESULTCONTROL:					{ Q_DQT_DEFRESULTCONTROL(rkResult); }break;
	case DQT_DEFEFFECT:							{ Q_DQT_DEFEFFECT(rkResult); }break;
	case DQT_DEFEFFECTABIL:						{ Q_DQT_DEFEFFECTABIL(rkResult); }break;
	case DQT_DEFUPGRADECLASS:					{ Q_DQT_DEFUPGRADECLASS(rkResult); }break;
	case DQT_DEFCHARACTER_BASEWEAR:				{ Q_DQT_DEFCHARACTER_BASEWEAR(rkResult); }break;
	case DQT_DEFITEMENCHANT:					{ Q_DQT_DEFITEMENCHANT(rkResult); }break;
	case DQT_DEF_ITEM_PLUS_UPGRADE:				{ Q_DQT_DEF_ITEM_PLUS_UPGRADE(rkResult); }break;
	case DQT_DEF_ITEM_ENCHANT_SHIFT:			{ Q_DQT_DEF_ITEM_ENCHANT_SHIFT(rkResult); }break;
	case DQT_DEF_SUCCESS_RATE_CONTROL:			{ Q_DQT_DEF_SUCCESS_RATE_CONTROL(rkResult); }break;
	case DQT_DEFMONSTERBAG:						{ Q_DQT_DEFMONSTERBAG(rkResult); }break;	
	case DQT_DEF_COUNT_CONTROL:					{ Q_DQT_DEF_COUNT_CONTROL(rkResult); }break;
	case DQT_DEF_ITEM_BAG_ELEMENTS:				{ Q_DQT_DEF_ITEM_BAG_ELEMENTS(rkResult); }break;
	case DQT_DEF_MONSTER_BAG_ELEMENTS:			{ Q_DQT_DEF_MONSTER_BAG_ELEMENTS(rkResult); }break;
	case DQT_DEF_MONSTER_BAG_CONTROL:			{ Q_DQT_DEF_MONSTER_BAG_CONTROL(rkResult); }break;
	case DQT_DEF_OBJECT:						{ Q_DQT_DEF_OBJECT(rkResult);}break;
	case DQT_DEF_OBJECTABIL:					{ Q_DQT_DEF_OBJECTABIL(rkResult);}break;
	case DQT_DEF_ITEM_BY_LEVEL:					{ Q_DQT_DEF_ITEM_BY_LEVEL(rkResult);}break;
	case DQT_DEF_QUEST_REWARD:					{ Q_DQT_DEF_QUEST_REWARD(rkResult); }break;	
	case DQT_DEF_QUEST_RANDOM_EXP:				{ Q_DQT_DEF_QUEST_RANDOM_EXP(rkResult); }break;
	case DQT_DEF_QUEST_RANDOM_TACTICS_EXP:		{ Q_DQT_DEF_QUEST_RANDOM_TACTICS_EXP(rkResult); }break;
	case DQT_DEF_QUEST_RESET_SCHEDULE:			{ Q_DQT_DEF_QUEST_RESET_SCHEDULE(rkResult); }break;	
	case DQT_DEF_QUEST_MIGRATION:				{ Q_DQT_DEF_QUEST_MIGRATION(rkResult); }break;	
	case DQT_DEF_MISSION_CANDIDATE:				{Q_DQT_DEF_MISSION_CANDIDATE(rkResult);}break;
	case DQT_DEF_MISSION_ROOT:					{Q_DQT_DEF_MISSION_ROOT(rkResult);}break;	
	case DQT_DEF_DEFENCE_ADD_MONSTER:			{Q_DQT_DEF_DEFENCE_ADD_MONSTER(rkResult);}break;	
	case DQT_DEF_MISSION_DEFENCE_STAGE:			{Q_DEF_MISSION_DEFENCE_STAGE(rkResult);}break;	
	case DQT_DEF_MISSION_DEFENCE_WAVE:			{Q_DEF_MISSION_DEFENCE_WAVE(rkResult);}break;	
	case DQT_DEF_MISSION_DEFENCE7_MISSION:		{Q_DEF_MISSION_DEFENCE7_MISSION(rkResult);}break;	
	case DQT_DEF_MISSION_DEFENCE7_STAGE:		{Q_DEF_MISSION_DEFENCE7_STAGE(rkResult);}break;	
	case DQT_DEF_MISSION_DEFENCE7_WAVE:			{Q_DEF_MISSION_DEFENCE7_WAVE(rkResult);}break;	
	case DQT_DEF_MISSION_DEFENCE7_GUARDIAN:		{Q_DEF_MISSION_DEFENCE7_GUARDIAN(rkResult);}break;	
	case DQT_DEF_MISSION_BONUSMAP:				{Q_DEF_MISSION_BONUSMAP(rkResult);}break;			
	case DQT_DEF_ITEM_OPTION:					{Q_DQT_DEF_ITEM_OPTION(rkResult);}break;
	case DQT_DEF_ITEM_OPTION_ABIL:				{Q_DQT_DEF_ITEM_OPTION_ABIL(rkResult);}break;
	case DQT_DEF_ITEM_RARITY_UPGRADE:			{Q_DQT_DEF_ITEM_RARITY_UPGRADE(rkResult);}break;
	case DQT_DEF_PVP_GROUNDMODE:				{Q_DQT_DEF_PVP_GROUNDMODE(rkResult);	}break;
	case DQT_DEF_FILTER_UNICODE:				{Q_DQT_DEF_FILTER_UNICODE(rkResult);}break;
	case DQT_DEF_ITEM_SET:						{Q_DQT_DEF_ITEM_SET(rkResult);	}break;
	case DQT_DEF_SPEND_MONEY:					{Q_DQT_DEF_SPEND_MONEY(rkResult);}break;
	case DQT_DEF_GUILD_LEVEL:					{Q_DQT_DEF_GUILD_LEVEL(rkResult);}break;
	case DQT_DEF_GUILD_SKILL:					{Q_DQT_DEF_GUILD_SKILL(rkResult);}break;
	case DQT_DEF_EMPORIA:						{Q_DQT_DEF_EMPORIA(rkResult);}break;
	case DQT_DEF_ITEM_BAG_GROUP:				{Q_DQT_DEF_ITEM_BAG_GROUP(rkResult);}break;
	case DQT_DEF_PROPERTY:						{Q_DQT_DEF_PROPERTY(rkResult);}break;	
	case DQT_DEF_FIVE_ELEMENT_INFO:				{Q_DQT_DEF_FIVE_ELEMENT_INFO(rkResult);}break;	
	case DQT_DEF_ITEM_RARITY_UPGRADE_COST_RATE:	{Q_DQT_DEF_ITEM_RARITY_UPGRADE_COST_RATE(rkResult);}break;
	case DQT_DEF_TACTICS_LEVEL:					{Q_DQT_DEF_TACTICS_LEVEL(rkResult);}break;	
	case DQT_DEF_MONSTER_KILL_COUNT_REWARD:		{Q_DQT_DEF_MONSTER_KILL_COUNT_REWARD(rkResult);}break;
	case DQT_DEF_ACHIEVEMENTS:					{Q_DQT_DEF_ACHIEVEMENTS(rkResult);}break;	
	case DQT_DEF_MONSTERCARD:					{Q_DQT_DEF_MONSTERCARD(rkResult);}break;
	case DQT_DEF_MARRYTEXT:						{Q_DQT_DEF_MARRYTEXT(rkResult);}break;
	case DQT_DEF_HIDDENREWORDITEM:				{Q_DQT_DEF_HIDDENREWORDITEM(rkResult);}break;
	case DQT_DEF_HIDDENREWORDBAG:				{Q_DQT_DEF_HIDDENREWORDBAG(rkResult);}break;
	case DQT_DEF_EMOTION:						{Q_DQT_DEF_EMOTION(rkResult);}break;	
	case DQT_DEF_CONVERTITEM:					{Q_DQT_DEF_CONVERTITEM(rkResult);}break;
	case DQT_DEF_PET_HATCH:						{Q_DQT_DEF_PET_HATCH(rkResult);}break;	
	case DQT_DEF_LOAD_RECOMMENDATIONITEM:		{Q_DQT_DEF_LOAD_RECOMMENDATIONITEM(rkResult);}break;
	case DQT_DEF_RARE_MONSTER_SPEECH:			{Q_DQT_DEF_RARE_MONSTER_SPEECH(rkResult);}break;
	case DQT_LOAD_CARD_LOCAL:					{Q_DQT_LOAD_CARD_LOCAL(rkResult);}break;
	case DQT_LOAD_DEF_HOMETOWNTOMAPCOST:		{Q_DQT_LOAD_DEF_HOMETOWNTOMAPCOST(rkResult);} break;
	case DQT_LOAD_DEF_MYHOME_TEX:				{Q_DQT_LOAD_DEF_MYHOME_TEX(rkResult);} break;
	case DQT_LOAD_DEF_SKILLIDX_TO_SKILLNO:		{Q_DQT_LOAD_DEF_SKILLIDX_TO_SKILLNO(rkResult);}break;
	case DQT_DEF_LOAD_CARDABIL:					{Q_DQT_DEF_LOAD_CARDABIL(rkResult);}break;
	case DQT_LOAD_DEF_CARD_KEY_STRING:			{Q_DQT_LOAD_DEF_CARD_KEY_STRING(rkResult);}break;
	case DQT_DEF_GROUND_RARE_MONSTER:			{Q_DQT_DEF_GROUND_RARE_MONSTER(rkResult);}break;
	case DQT_DEF_TRANSTOWER:					{Q_DQT_DEF_TRANSTOWER(rkResult);}break;
	case DQT_DEF_PARTY_INFO:					{Q_DQT_DEF_PARTY_INFO(rkResult);}break;
	case DQT_DEF_MAP_ITEM_BAG:					{Q_DQT_DEF_MAP_ITEM_BAG(rkResult);}break;
	case DQT_DEF_OBJECT_BAG:					{Q_DQT_DEF_OBJECT_BAG(rkResult);}break;
	case DQT_DEF_OBJECT_BAG_ELEMENTS:			{Q_DQT_DEF_OBJECT_BAG_ELEMENTS(rkResult);}break;
	case DQT_DEF_MISSION_RESULT:				{Q_DQT_DEF_MISSION_RESULT(rkResult);}break;
	case DQT_DEF_MISSION_CLASS_REWARD:			{Q_DQT_DEF_MISSION_CLASS_REWARD(rkResult);}break;
	case DQT_DEF_MISSION_RANK_REWARD:			{Q_DQT_DEF_MISSION_RANK_REWARD(rkResult);}break;
	case DQT_LOAD_DEF_CHARCARDEFFECT:			{Q_DQT_LOAD_DEF_CHARCARDEFFECT(rkResult);}break;
	case DQT_LOAD_DEF_SIDEJOBRATE:				{Q_DQT_LOAD_DEF_SIDEJOBRATE(rkResult);}break;
	case DQT_LOAD_DEF_EVENTITEMSET:				{Q_DQT_LOAD_DEF_EVENTITEMSET(rkResult);}break;
	case DQT_LOAD_DEF_REDICEOPTIONCOST:			{Q_DQT_LOAD_DEF_REDICEOPTIONCOST(rkResult);}break;
	case DQT_LOAD_DEF_MYHOMESIDEJOBTIME:		{Q_DQT_LOAD_DEF_MYHOMESIDEJOBTIME(rkResult);}break;
	case DQT_LOAD_DEF_MYHOMEBUILDINGS:			{Q_DQT_LOAD_DEF_MYHOMEBUILDINGS(rkResult);}break;
	case DQT_LOAD_DEF_MONSTER_ENCHANT_GRADE:	{Q_DQT_LOAD_DEF_MONSTER_ENCHANT_GRADE(rkResult);}break;
	case DQT_LOAD_DEF_BASICOPTIONAMP:			{Q_DQT_LOAD_DEF_BASICOPTIONAMP(rkResult);}break;
	case DQT_LOAD_DEF_ALRAM_MISSION:			{Q_DQT_LOAD_DEF_ALRAM_MISSION(rkResult);}break;
	case DQT_LOAD_DEF_DEATHPENALTY:				{Q_DQT_LOAD_DEF_DEATHPENALTY(rkResult);}break;
	case DQT_LOAD_DEF_SKILLEXTENDITEM:			{Q_DQT_LOAD_DEF_SKILLEXTENDITEM(rkResult);}break;
	case DQT_LOAD_DEF_NPC_TALK_MAP_MOVE:		{Q_DQT_LOAD_DEF_NPC_TALK_MAP_MOVE(rkResult);}break;
	case DQT_LOAD_DEF_SPECIFIC_REWARD:			{Q_DQT_LOAD_DEF_SPECIFIC_REWARD(rkResult);}break;
	case DQT_LOAD_DEF_JOBSKILL_PROBABILITY:		{Q_DQT_LOAD_DEF_JOBSKILL_PROBABILITY(rkResult);}break;
	case DQT_LOAD_DEF_JOBSKILL_LOCATIONITEM:	{Q_LOAD_DEF_JOBSKILL_LOCATIONITEM(rkResult);}break;	
	case DQT_LOAD_DEF_JOBSKILL_SKILL:			{Q_LOAD_DEF_JOBSKILL_SKILL(rkResult);}break;		
	case DQT_LOAD_DEF_JOBSKILL_SKILLEXPERTNESS:	{Q_LOAD_DEF_JOBSKILL_SKILLEXPERTNESS(rkResult);}break;	
	case DQT_LOAD_DEF_JOBSKILL_TOOL:			{Q_LOAD_DEF_JOBSKILL_TOOL(rkResult);}break;
	case DQT_LOAD_DEF_JOBSKILL_ITEMUPGRADE:		{Q_DQT_LOAD_DEF_JOBSKILL_ITEMUPGRADE(rkResult);}break;
	case DQT_LOAD_DEF_JOBSKILL_MACHINE:			{Q_DQT_LOAD_DEF_JOBSKILL_MACHINE(rkResult);}break;
	case DQT_LOAD_DEF_JOBSKILL_MACHINESTATUS:	{Q_DQT_LOAD_DEF_JOBSKILL_MACHINESTATUS(rkResult);}break;
	case DQT_LOAD_DEF_JOBSKILL_SAVEIDX:			{Q_DQT_LOAD_DEF_JOBSKILL_SAVEIDX(rkResult);}break;	
	case DQT_LOAD_DEF_JOBSKILL_SHOP:			{Q_DQT_LOAD_DEF_JOBSKILL_SHOP(rkResult);}break;
	case DQT_LOAD_DEF_JOBSKILL_RECIPE:			{Q_DQT_LOAD_DEF_JOBSKILL_RECIPE(rkResult);}break;	
	case DQT_LOAD_DEF_SOCKET_ITEM:				{Q_LOAD_DEF_SOCKET_ITEM(rkResult);}break;
	case DQT_LOAD_DEF_JOBSKILL_ITEMSOULEXTRACT:			{Q_DQT_LOAD_DEF_JOBSKILL_ITEMSOULEXTRACT(rkResult);}break;
	case DQT_LOAD_DEF_JOBSKILL_ITEMSOULTRANSITION:		{Q_DQT_LOAD_DEF_JOBSKILL_ITEMSOULTRANSITION(rkResult);}break;	
	default:
		{
			return false;
		}
	}

	return true;
}