#include "StdAfx.h"
#include "lohengrin/dbtables.h"
#include "TableDataManager.h"
#include "Item.h"
#include "PgJobSkillSaveIdx.h"
#include "PgProbability.h"
#include "PgJobSkillWorkBench.h"
#include "ItemDefMgr.h"
#include "PgControlDefMgr.h"

namespace JobSkillWorkBenchUtil
{
	size_t const iStartSlotNo = 1;
	int const iBlessBaseValue = 10000;

	//
	bool IsWorkBenchItem(int const iItemNo, CONT_DEF_JOBSKILL_MACHINE const& rkContDefJSMachine)
	{
		return rkContDefJSMachine.end() != rkContDefJSMachine.find(iItemNo);
	}
	bool IsWorkBenchItem(int const iItemNo)
	{
		CONT_DEF_JOBSKILL_MACHINE const* pkDefJSMachine = NULL;
		g_kTblDataMgr.GetContDef(pkDefJSMachine);
		if( pkDefJSMachine )
		{
			return IsWorkBenchItem(iItemNo, *pkDefJSMachine);
		}
		return false;
	}

	bool GetWorkbenchInfo(int const iItemNo, CONT_DEF_JOBSKILL_MACHINE::mapped_type& rkOut)
	{
		CONT_DEF_JOBSKILL_MACHINE const* pkDefJSMachine = NULL;
		g_kTblDataMgr.GetContDef(pkDefJSMachine);
		if( pkDefJSMachine )
		{
			CONT_DEF_JOBSKILL_MACHINE::const_iterator iter = pkDefJSMachine->find(iItemNo);
			if( pkDefJSMachine->end() != iter )
			{
				rkOut = (*iter).second;
				return true;
			}
		}
		return false;
	}

	int GetWorkBenchSlotCount(int const iItemNo)
	{
		CONT_DEF_JOBSKILL_MACHINE::mapped_type kOut;
		if( GetWorkbenchInfo(iItemNo, kOut) )
		{
			return kOut.iSlotCount;
		}
		return 0;
	}

	//
	bool GetUpgradeInfo(int const iItemNo, CONT_DEF_JOBSKILL_ITEM_UPGRADE::mapped_type& rkOut)
	{
		CONT_DEF_JOBSKILL_ITEM_UPGRADE const* pkDefJSItemUpgrade = NULL;
		g_kTblDataMgr.GetContDef(pkDefJSItemUpgrade);
		if( pkDefJSItemUpgrade )
		{
			CONT_DEF_JOBSKILL_ITEM_UPGRADE::const_iterator find_iter = pkDefJSItemUpgrade->find(iItemNo);
			if( pkDefJSItemUpgrade->end() != find_iter )
			{
				rkOut = (*find_iter).second;
				return true;
			}
		}
		return false;
	}
	int GetRemainUpgradeCount(int const iItemNo)
	{
		CONT_DEF_JOBSKILL_ITEM_UPGRADE::mapped_type kDef;
		if( GetUpgradeInfo(iItemNo, kDef) )
		{
			return kDef.iUpgradeCount;
		}
		return 0;
	}
	bool IsCanUpgrade(int const iItemNo)
	{
		return 0 != GetRemainUpgradeCount(iItemNo);
	}
	int GetFailItem(int const iItemNo)
	{
		int const iMagicNumber = 1;
		int const iMagicNumber2 = 10;
		if( 0 != iItemNo % iMagicNumber2 )
		{
			return iItemNo - iMagicNumber;
		}
		return iItemNo;
	}
	int GetTurnTime(int const iItemNo)
	{
		CONT_DEF_JOBSKILL_ITEM_UPGRADE::mapped_type kDef;
		if( GetUpgradeInfo(iItemNo, kDef) )
		{
			return kDef.iUpgradeTime;
		}
		return 0;
	}
	int GetUseDuration(int const iItemNo)
	{
		CONT_DEF_JOBSKILL_ITEM_UPGRADE::mapped_type kDef;
		if( GetUpgradeInfo(iItemNo, kDef) )
		{
			return kDef.iMachine_UseDuration;
		}
		int const iDefault = 1;
		return iDefault;
	}

	//
	int GetMakeTroubleTime(int const iItemNo)
	{
		CONT_DEF_JOBSKILL_ITEM_UPGRADE::mapped_type kDef;
		if( GetUpgradeInfo(iItemNo, kDef) )
		{
			int const iRandRet = BM::Rand_Range(kDef.iErrorStateMaxProbability);
			if( iRandRet <= kDef.iErrorStateProbability )
			{
				return kDef.iErrorStateTimeAbsolute;
			}
		}
		return 0;
	}

	//
	size_t Add(CONT_JSWB_UPGRADE_ITEM& rkContStatus, size_t const iMaxSlotCount, int const iItemNo)
	{
		if( iMaxSlotCount > rkContStatus.size() )
		{
			for( size_t iNewSlot = iStartSlotNo; iMaxSlotCount > iNewSlot; ++iNewSlot )
			{
				if( rkContStatus.end() == rkContStatus.find(iNewSlot) )
				{
					rkContStatus.insert( std::make_pair(iNewSlot, CONT_JSWB_UPGRADE_ITEM::mapped_type(iItemNo, GetTurnTime(iItemNo))) );
					return iNewSlot;
				}
			}
		}
		return 0; // fail
	}
	size_t Add(CONT_JSWB_UPGRADE_ITEM& rkContStatus, size_t const iMaxSlotCount, size_t const iNewSlot, int const iItemNo)
	{
		if( iMaxSlotCount > rkContStatus.size() )
		{
			if( rkContStatus.end() == rkContStatus.find(iNewSlot) )
			{
				rkContStatus.insert( std::make_pair(iNewSlot, CONT_JSWB_UPGRADE_ITEM::mapped_type(iItemNo, GetTurnTime(iItemNo))) );
				return iNewSlot;
			}
		}
		return 0; // fail
	}
	bool Set(CONT_JSWB_UPGRADE_ITEM& rkContStatus, size_t const iSlotNo, SJobSkillWorkBenchSlotItemStatus const& rkStatus)
	{
		CONT_JSWB_UPGRADE_ITEM::iterator find_iter = rkContStatus.find( iSlotNo );
		if( rkContStatus.end() != find_iter )
		{
			(*find_iter).second = rkStatus;
			return true;
		}
		return false;
	}
	bool Get(CONT_JSWB_UPGRADE_ITEM const& rkContStatus, size_t const iSlotNo, SJobSkillWorkBenchSlotItemStatus &rkOut)
	{
		CONT_JSWB_UPGRADE_ITEM::const_iterator find_iter = rkContStatus.find( iSlotNo );
		if( rkContStatus.end() != find_iter )
		{
			rkOut = (*find_iter).second;
			return true;
		}
		return false;
	}
	void Del(CONT_JSWB_UPGRADE_ITEM& rkContStatus, size_t const iSlotNo)
	{
		rkContStatus.erase(iSlotNo);
	}

	bool IsSystemLog(EWorkBenchEventType const eType)
	{
		switch( eType )
		{
		case WBET_NONE:
		case WBET_BLESS_END:
		case WBET_FIND_NEXT_SLOT:
			{
				return true;
			}break;
		default:
			{
			}break;
		}
		return false;
	}
	bool IsExpertnessLog(EWorkBenchEventType const eType)
	{
		switch( eType )
		{
		case WBET_UPGRADE:
		case WBET_UPGRADE_FAIL:
		case WBET_UPGRADE_TROUBLE_FAIL:
			{
				return true;
			}break;
		default:
			{
			}break;
		}
		return false;
	}
	
	bool SetAlterInfoFromItemNo(int const iItemNo, PgJobSkillWorkBenchAlterInfo& rkOutAlter)
	{// 자기 작업대에 적용될 
		GET_DEF(CItemDefMgr, kItemDefMgr);
		CItemDef const* pkItemDef = kItemDefMgr.GetDef(iItemNo);
		if(!pkItemDef)
		{
			return false;
		}

		int  const iWorkTimeRate = pkItemDef->GetAbil(AT_JS_SINGLE_WORKBENCH_UPGRADE_FAST_RATE);
		int const iExpertnessRate = pkItemDef->GetAbil(AT_JS_SINGLE_WORKBENCH_ADD_EXPERTNESS_RATE);
		int const iDecDurRate = pkItemDef->GetAbil(AT_JS_SINGLE_WORKBENCH_DEC_DURATION_RATE);
		bool const bAutoRepair = 0 < pkItemDef->GetAbil(AT_JS_SINGLE_WORKBENCH_AUTO_REPAIR);
		bool const bCompleteItemSendMail = 0 < pkItemDef->GetAbil(AT_JS_SINGLE_WORKBENCH_AUTO_MAIL);

		rkOutAlter.SetWorkTimeRate(iWorkTimeRate);
		rkOutAlter.SetExertnessRate(iExpertnessRate);
		rkOutAlter.SetDecDurRate(iDecDurRate);
		rkOutAlter.SetAutoRepair(bAutoRepair);
		rkOutAlter.SetCompleteItemSendMail(bCompleteItemSendMail);

		return true;
	}

	bool IsWorkBenchPublicAlterItem(int const iItemNo)
	{// 모든 작업대에 적용될 
		GET_DEF(CItemDefMgr, kItemDefMgr);
		CItemDef const* pkItemDef = kItemDefMgr.GetDef(iItemNo);
		if(pkItemDef
			&& 0 < pkItemDef->GetAbil(AT_JS_MGR_WORKBENCH)
			)
		{
			return true;
		}
		return false;
	}

	//
	size_t const iMaxDBCount = 10;
};

//
tagJobSkillWorkBenchSlotItemStatus::tagJobSkillWorkBenchSlotItemStatus()
	: iItemNo(0), iTurnSec(0), iEleapsedSec(0)
{
}
tagJobSkillWorkBenchSlotItemStatus::tagJobSkillWorkBenchSlotItemStatus(tagJobSkillWorkBenchSlotItemStatus const& rhs)
	: iItemNo(rhs.iItemNo), iTurnSec(rhs.iTurnSec), iEleapsedSec(rhs.iEleapsedSec)
{
}
tagJobSkillWorkBenchSlotItemStatus::tagJobSkillWorkBenchSlotItemStatus(int const ItemNo, __int64 const TurnSec)
	: iItemNo(ItemNo), iTurnSec(TurnSec), iEleapsedSec(0)
{
}
tagJobSkillWorkBenchSlotItemStatus::tagJobSkillWorkBenchSlotItemStatus(int const ItemNo, __int64 const TurnSec, __int64 const EleapsedSec)
	: iItemNo(ItemNo), iTurnSec(TurnSec), iEleapsedSec(EleapsedSec)
{
}

//
PgJobSkillWorkBenchAlterInfo::PgJobSkillWorkBenchAlterInfo()
{
	Init();
}

PgJobSkillWorkBenchAlterInfo::PgJobSkillWorkBenchAlterInfo(int const iWorkTimeRate, int const iExpertnessRate, int const iDecDurRate, bool const bAutoRepair, bool const bCompleteItemSendMail)
{
	m_iWorkTimeRate			=iWorkTimeRate;			// 가공 속도 변동 상대값
	m_iExpertnessRate		=iExpertnessRate;		// 숙련도 상승 상대값
	m_iDecDurRate			=iDecDurRate;			// 내구도 감소량 상대값
	m_bAutoRepair			=bAutoRepair;			// 자동 수리
	m_bCompleteItemSendMail	=bCompleteItemSendMail;	// 아이템 메일로 쏘기
}

PgJobSkillWorkBenchAlterInfo::~PgJobSkillWorkBenchAlterInfo()
{
}

void PgJobSkillWorkBenchAlterInfo::Init()
{
	m_iWorkTimeRate			=0;		// 가공 속도 변동 상대값
	m_iExpertnessRate		=0;		// 숙련도 상승 상대값
	m_iDecDurRate			=0;		// 내구도 감소량 상대값
	m_bAutoRepair			=false;	// 자동 수리
	m_bCompleteItemSendMail	=false;	// 아이템 메일로 쏘기
}

//
PgJobSkillWorkBench::PgJobSkillWorkBench()
	: m_iItemNo(0), m_iDuration(0), m_kContSlotItem(), m_iCurUpgradeSlot(0), m_bHasTrouble(false), m_iBlessDurationSec(0), m_iTroubleTime(0), m_kBlessCharGuid(), m_iBlessSkillNo(0)
{
}
PgJobSkillWorkBench::~PgJobSkillWorkBench()
{
}

EWorkBenchStatus PgJobSkillWorkBench::GetStatus() const
{
	if( 0 == m_iDuration )
	{
		return WBS_STOP;
	}
	else if( m_kContSlotItem.empty() )
	{
		return WBS_NONE;
	}
	else if( m_bHasTrouble )
	{
		return WBS_WORKING_TROUBLE;
	}
	else if( IsHaveComplete() )
	{
		return WBS_WORKING_COMPLETE;
	}
	return WBS_WORKING;
}
EWorkBenchStatus PgJobSkillWorkBench::GetMajorStatus() const
{
	if( 0 == m_iDuration )
	{
		return WBS_STOP;
	}
	else if( m_kContSlotItem.empty() )
	{
		return WBS_NONE;
	}
	else if( m_bHasTrouble )
	{
		return WBS_WORKING_TROUBLE;
	}
	else if( IsHaveAllComplete() )
	{
		return WBS_WORKING_COMPLETE;
	}
	return WBS_WORKING;
}

bool PgJobSkillWorkBench::IsHaveComplete() const
{
	CONT_JSWB_UPGRADE_ITEM::const_iterator iter = m_kContSlotItem.begin();
	while( m_kContSlotItem.end() != iter )
	{
		CONT_JSWB_UPGRADE_ITEM::mapped_type const& rkStatus = (*iter).second;
		if( false == JobSkillWorkBenchUtil::IsCanUpgrade(rkStatus.iItemNo) )
		{
			return true;
		}
		++iter;
	}
	return false;
}
bool PgJobSkillWorkBench::IsHaveAllComplete() const
{
	CONT_JSWB_UPGRADE_ITEM::const_iterator iter = m_kContSlotItem.begin();
	while( m_kContSlotItem.end() != iter )
	{
		CONT_JSWB_UPGRADE_ITEM::mapped_type const& rkStatus = (*iter).second;
		if( false != JobSkillWorkBenchUtil::IsCanUpgrade(rkStatus.iItemNo) )
		{
			return false;
		}
		++iter;
	}
	return true;
}

EWorkBenchEventType PgJobSkillWorkBench::Update(__int64 const iEleapsedTime, int& iEndItemNo, int& iResultItemNo, PgJobSkillWorkBenchAlterInfo const& kPublicAlterInfo, size_t& stWorkedAtThisTimeSlotNo)
{
	if( 0 == m_iDuration )
	{
		return WBET_NONE;
	}

	if( 0 < m_iBlessDurationSec )
	{
		m_iBlessDurationSec -= iEleapsedTime;
		if( 0 >= m_iBlessDurationSec )
		{
			m_iBlessDurationSec = 0;
		}
	}

	EWorkBenchEventType const eRet = Work(iEleapsedTime, iEndItemNo, iResultItemNo, kPublicAlterInfo, stWorkedAtThisTimeSlotNo);
	NextSlot(); // 현재 아이템 업글이 완료되면 다음 슬롯 찾는다
	return eRet; // status is changed
}
void PgJobSkillWorkBench::UpdateCompleteItem(__int64 const iEleapsedTime, CONT_JSWB_DELETE_ITEM& rkOut)
{
	CONT_JSWB_UPGRADE_ITEM::iterator iter = m_kContSlotItem.begin();
	while( m_kContSlotItem.end() != iter )
	{
		CONT_JSWB_UPGRADE_ITEM::mapped_type& rkStatus = (*iter).second;
		if( rkStatus.IsHaveNoUpgrade() )
		{
			rkStatus.iEleapsedSec += iEleapsedTime;
			if( rkStatus.IsEndTurn() )
			{
				rkOut.push_back( (*iter).second.iItemNo );
				iter = m_kContSlotItem.erase(iter);
				continue;
			}
		}
		++iter;
	}
}
EWorkBenchEventType PgJobSkillWorkBench::Work(__int64 const iOrgEleapsedTime, int& iEndItemNo, int& iResultItemNo, PgJobSkillWorkBenchAlterInfo const& kPublicAlterInfo, size_t& stWorkedAtThisTimeSlotNo)
{
	stWorkedAtThisTimeSlotNo = m_iCurUpgradeSlot;
	EWorkBenchEventType eRet = WBET_NONE;
	if( false == m_kContSlotItem.empty()
	&&	JobSkillWorkBenchUtil::iStartSlotNo <= m_iCurUpgradeSlot )
	{
		SJobSkillWorkBenchSlotItemStatus kCurStatus;
		CONT_DEF_JOBSKILL_ITEM_UPGRADE::mapped_type kDefJSUpgradeInfo;
		if( JobSkillWorkBenchUtil::Get(m_kContSlotItem, m_iCurUpgradeSlot, kCurStatus)
		&&	JobSkillWorkBenchUtil::GetUpgradeInfo(kCurStatus.iItemNo, kDefJSUpgradeInfo)
		&&	0 < kDefJSUpgradeInfo.iUpgradeCount )
		{
			int const iAdjustWorkTimeRate = kPublicAlterInfo.GetWorkTimeRate()+m_kAlterInfo.GetWorkTimeRate();

			if( 0 < m_iBlessDurationSec )
			{
				__int64 const iEleapsedTime = iOrgEleapsedTime + (iOrgEleapsedTime * g_kEventView.VariableCont().iJobSkill_BlessRate / JobSkillWorkBenchUtil::iBlessBaseValue); //시간 가속화
				__int64 const iAdjustedWorkTime = iEleapsedTime + ( (iEleapsedTime*iAdjustWorkTimeRate)/ABILITY_RATE_VALUE); // (만분률만큼 시간이 더 빨리간다)
				kCurStatus.iEleapsedSec += iAdjustedWorkTime;
			}
			else
			{
				__int64 const iAdjustedWorkTime =  iOrgEleapsedTime + ( (iOrgEleapsedTime*iAdjustWorkTimeRate)/ABILITY_RATE_VALUE ); // (만분률만큼 시간이 더 빨리간다)
				kCurStatus.iEleapsedSec += iAdjustedWorkTime;
			}

			if( kCurStatus.IsEndTurn() ) // 1턴 종료
			{
				iEndItemNo = kCurStatus.iItemNo;
				if( false == m_bHasTrouble )
				{
					CONT_DEF_JOBSKILL_PROBABILITY_BAG const* pkDefJSProb = NULL;
					g_kTblDataMgr.GetContDef(pkDefJSProb);
					if( pkDefJSProb )
					{
						SProbability kRet = ProbabilityUtil::GetOne(*pkDefJSProb, kDefJSUpgradeInfo.iResultProbabilityNo, kDefJSUpgradeInfo.iResourceProbabilityUp);
						if( 0 < kRet.iResultNo
						&&	0 < kRet.iCount )
						{
							kCurStatus.iItemNo = kRet.iResultNo;
							eRet = WBET_UPGRADE;
						}
						else
						{
							kCurStatus.iItemNo = JobSkillWorkBenchUtil::GetFailItem(kCurStatus.iItemNo); // 실패
							eRet = WBET_UPGRADE_FAIL;
						}
					}
				}
				else
				{
					kCurStatus.iItemNo = JobSkillWorkBenchUtil::GetFailItem(kCurStatus.iItemNo); // 실패
					eRet = WBET_UPGRADE_TROUBLE_FAIL;
				}
				kCurStatus.iEleapsedSec = 0;
				kCurStatus.iTurnSec = JobSkillWorkBenchUtil::GetTurnTime(kCurStatus.iItemNo);

				iResultItemNo = kCurStatus.iItemNo;
				
				{// 내구도 감소
					int const iAdjustDecDurRate = kPublicAlterInfo.GetDecDurRate()+m_kAlterInfo.GetDecDurRate();
					int const iAdjustedDecDuration = kDefJSUpgradeInfo.iMachine_UseDuration - ( (kDefJSUpgradeInfo.iMachine_UseDuration*iAdjustDecDurRate)/ABILITY_RATE_VALUE);
					m_iDuration = std::max(m_iDuration - iAdjustedDecDuration, 0);
				}
				
				m_bHasTrouble = false; // 한턴 끝나면 초기화
				m_iTroubleTime = JobSkillWorkBenchUtil::GetMakeTroubleTime(kCurStatus.iItemNo);
			}
			else
			{
				if( 0 < m_iTroubleTime )
				{
					m_iTroubleTime -= iOrgEleapsedTime;
					if( 0 >= m_iTroubleTime )
					{
						eRet = WBET_TROUBLE;
						m_bHasTrouble = true;
						m_iTroubleTime = 0;
						if( m_kAlterInfo.IsAutoRepair() )
						{
							HasTrouble(false);
							eRet = WBET_AUTO_REPAIR;
						}
					}
				}
			}

			JobSkillWorkBenchUtil::Set(m_kContSlotItem, m_iCurUpgradeSlot, kCurStatus);
		}
		else
		{
			eRet = WBET_FIND_NEXT_SLOT; // 강제로 다음거 찾기
		}
	}
	if( m_kContSlotItem.empty()
	&&	0 < m_iCurUpgradeSlot )
	{
		eRet = WBET_FIND_NEXT_SLOT;
	}
	return eRet;
}
void PgJobSkillWorkBench::NextSlot()
{
	// 없다가 있는 경우
	// 있다가 없는 경우
	if( m_kContSlotItem.empty() )
	{
		m_iCurUpgradeSlot = 0;
		m_bHasTrouble = false;
		return ;
	}

	SJobSkillWorkBenchSlotItemStatus kCurStatus;
	if( JobSkillWorkBenchUtil::Get(m_kContSlotItem, m_iCurUpgradeSlot, kCurStatus) )
	{
		if( 0 != JobSkillWorkBenchUtil::GetRemainUpgradeCount(kCurStatus.iItemNo) )
		{
			return ;
		}
	}

	m_bHasTrouble = false;
	typedef std::list< size_t > CONT_SLOT_NO;
	CONT_SLOT_NO kContSlotList;
	// 업글 되는 슬롯들 확인
	{
		CONT_JSWB_UPGRADE_ITEM::const_iterator iter = m_kContSlotItem.begin();
		while( m_kContSlotItem.end() != iter )
		{
			SJobSkillWorkBenchSlotItemStatus kCurStatus;
			if( JobSkillWorkBenchUtil::Get(m_kContSlotItem, (*iter).first, kCurStatus) )
			{
				if( false == kCurStatus.IsHaveNoUpgrade() )
				{
					kContSlotList.push_back( (*iter).first );
				}
			}
			++iter;
		}
	}
	if( kContSlotList.empty() )
	{
		// 없으면 중단
		m_iCurUpgradeSlot = 0;
	}
	else if( 1 == kContSlotList.size() )
	{
		//  size 1이면 그놈
		m_iCurUpgradeSlot = kContSlotList.front();
	}
	else
	{
		struct Greater {
			Greater(size_t const Value)
				: iValue(Value) {
			}
			Greater(Greater const& rhs)
				: iValue(rhs.iValue) {
			}
			bool operator()(size_t const rhs) const {
				return iValue < rhs;
			}
			size_t const iValue;
		};

		//  현재 슬롯보다 큰 슬롯을 검색
		CONT_SLOT_NO::const_iterator find_iter = std::find_if( kContSlotList.begin(), kContSlotList.end(), Greater(m_iCurUpgradeSlot) );
		if( kContSlotList.end() != find_iter )
		{
			// 있으면 이놈
			m_iCurUpgradeSlot = (*find_iter);
		}
		else
		{
			//  없으면 begin
			m_iCurUpgradeSlot = kContSlotList.front();
		}
	}
}
int PgJobSkillWorkBench::Pop(size_t const iSlotNo)
{
	SJobSkillWorkBenchSlotItemStatus kStatus;
	if( JobSkillWorkBenchUtil::Get(m_kContSlotItem, iSlotNo, kStatus) )
	{
		JobSkillWorkBenchUtil::Del(m_kContSlotItem, iSlotNo);
		if( m_iCurUpgradeSlot == iSlotNo )
		{
			NextSlot(); // 다음 슬롯 호출
		}
		return kStatus.iItemNo;
	}
	return 0;
}
bool PgJobSkillWorkBench::Push(size_t const iSlotNo, int const iItemNo)
{
	/*if( 0 == iSlotNo )
	{
		size_t const iRetNewSlot = JobSkillWorkBenchUtil::Add(m_kContSlotItem, JobSkillWorkBenchUtil::GetWorkBenchSlotCount(m_iItemNo), iItemNo)
	}
	else*/
	size_t const iRetNewSlot = JobSkillWorkBenchUtil::Add(m_kContSlotItem, JobSkillWorkBenchUtil::GetWorkBenchSlotCount(m_iItemNo), iSlotNo, iItemNo);
	NextSlot();
	return 0 != iRetNewSlot;
}
void PgJobSkillWorkBench::WriteToPacket(BM::CPacket& rkPacket) const
{
	rkPacket.Push( m_iItemNo );
	rkPacket.Push( m_iDuration );

	PU::TWriteTable_AM(rkPacket, m_kContSlotItem);
	rkPacket.Push( m_iCurUpgradeSlot );
	rkPacket.Push( m_bHasTrouble );

	rkPacket.Push( m_iBlessDurationSec );
	rkPacket.Push( m_iTroubleTime );
	rkPacket.Push( m_kBlessCharGuid );
	rkPacket.Push( m_iBlessSkillNo );
}
void PgJobSkillWorkBench::ReadFromPacket(BM::CPacket& rkPacket)
{
	m_kContSlotItem.clear();

	rkPacket.Pop( m_iItemNo );
	rkPacket.Pop( m_iDuration );

	PU::TLoadTable_AM(rkPacket, m_kContSlotItem);
	rkPacket.Pop( m_iCurUpgradeSlot );
	rkPacket.Pop( m_bHasTrouble );

	rkPacket.Pop( m_iBlessDurationSec );
	rkPacket.Pop( m_iTroubleTime );
	rkPacket.Pop( m_kBlessCharGuid );
	rkPacket.Pop( m_iBlessSkillNo );
	
	JobSkillWorkBenchUtil::SetAlterInfoFromItemNo(m_iItemNo, m_kAlterInfo);
}

void PgJobSkillWorkBench::ReadFromDB(CEL::DB_DATA_ARRAY::const_iterator& iter)
{
	m_kContSlotItem.clear();

	BYTE cTemp = 0;
	(*iter).Pop( m_iItemNo );				++iter;
	(*iter).Pop( m_iDuration );				++iter;
	for( size_t iCur = 1; JobSkillWorkBenchUtil::iMaxDBCount >= iCur; ++iCur )
	{
		int iItemNo = 0;
		__int64 iEleapsedSec = 0;
		(*iter).Pop( iItemNo );				++iter;
		(*iter).Pop( iEleapsedSec );		++iter;

		if( 0 < iItemNo )
		{
			m_kContSlotItem.insert( std::make_pair(iCur, CONT_JSWB_UPGRADE_ITEM::mapped_type(iItemNo, JobSkillWorkBenchUtil::GetTurnTime(iItemNo), iEleapsedSec)) );
		}
	}
	(*iter).Pop( m_iCurUpgradeSlot );		++iter;
	(*iter).Pop( cTemp );		m_bHasTrouble = (0 != cTemp);	++iter;
	(*iter).Pop( m_iTroubleTime );			++iter;
	(*iter).Pop( m_iBlessDurationSec );		++iter;
	(*iter).Pop( m_kBlessCharGuid );		++iter;
	(*iter).Pop( m_iBlessSkillNo );			++iter;

	JobSkillWorkBenchUtil::SetAlterInfoFromItemNo(m_iItemNo, m_kAlterInfo);
}
void PgJobSkillWorkBench::WriteToDB(BM::GUID const& rkItemGuid, CEL::DB_QUERY& rkQuery) const
{
	rkQuery.PushStrParam( rkItemGuid );
	rkQuery.PushStrParam( m_iItemNo );
	rkQuery.PushStrParam( m_iDuration );
	for( size_t iCur = 1; JobSkillWorkBenchUtil::iMaxDBCount >= iCur; ++iCur )
	{
		CONT_JSWB_UPGRADE_ITEM::const_iterator find_iter = m_kContSlotItem.find( iCur );
		if( m_kContSlotItem.end() != find_iter )
		{
			rkQuery.PushStrParam( (*find_iter).second.iItemNo );
			rkQuery.PushStrParam( (*find_iter).second.iEleapsedSec );
		}
		else
		{
			rkQuery.PushStrParam( static_cast< int >(0) );
			rkQuery.PushStrParam( static_cast< __int64 >(0) );
		}
	}
	rkQuery.PushStrParam( m_iCurUpgradeSlot );
	rkQuery.PushStrParam( static_cast< BYTE >((m_bHasTrouble)?1:0) );
	rkQuery.PushStrParam( m_iTroubleTime );
	rkQuery.PushStrParam( m_iBlessDurationSec );
	rkQuery.PushStrParam( m_kBlessCharGuid );
	rkQuery.PushStrParam( m_iBlessSkillNo );
}

bool PgJobSkillWorkBench::GetCurrentUpgradeSlotItemStatus(SJobSkillWorkBenchSlotItemStatus& kOut) const
{
	return JobSkillWorkBenchUtil::Get(m_kContSlotItem, CurUpgradeSlot(), kOut);
}

bool PgJobSkillWorkBench::GetUpgradeSlotItemStatus(int const iSlot, SJobSkillWorkBenchSlotItemStatus& kOut) const
{
	return JobSkillWorkBenchUtil::Get(m_kContSlotItem, iSlot, kOut);
}

size_t PgJobSkillWorkBench::GetEmptySlotNo() const
{
	CONT_JSWB_UPGRADE_ITEM::const_iterator iter;
	int const iMaxSlotCnt = JobSkillWorkBenchUtil::GetWorkBenchSlotCount(m_iItemNo);
	if(iMaxSlotCnt == m_kContSlotItem.size())
	{
		return 0;
	}

	for(size_t i = JobSkillWorkBenchUtil::iStartSlotNo; i < iMaxSlotCnt+JobSkillWorkBenchUtil::iStartSlotNo; ++i)
	{
		iter = m_kContSlotItem.find(i);
		if(m_kContSlotItem.end() == iter)
		{
			return i;
		}
	}
	return 0;
}

size_t PgJobSkillWorkBench::GetEmptySlotCnt() const
{
	int const iMaxSlotCnt = JobSkillWorkBenchUtil::GetWorkBenchSlotCount(m_iItemNo);
	return iMaxSlotCnt - m_kContSlotItem.size();
}

size_t PgJobSkillWorkBench::GetNotEmptySlotCnt() const
{
	int const iMaxSlotCnt = JobSkillWorkBenchUtil::GetWorkBenchSlotCount(m_iItemNo);
	return m_kContSlotItem.size() - iMaxSlotCnt;
}

size_t PgJobSkillWorkBench::GetCompleteSlotCnt() const
{
	size_t stCnt = 0;
	CONT_JSWB_UPGRADE_ITEM::const_iterator iter = m_kContSlotItem.begin();
	while( m_kContSlotItem.end() != iter )
	{
		CONT_JSWB_UPGRADE_ITEM::mapped_type const& rkStatus = (*iter).second;
		if(!rkStatus.IsRemainUpgrade())
		{
			++stCnt;
		}
		++iter;
	}
	return stCnt;
}

void PgJobSkillWorkBench::GetCompleteSlotNum(PgJobSkillWorkBench::CONT_INT& kOut) const
{
	CONT_JSWB_UPGRADE_ITEM::const_iterator iter = m_kContSlotItem.begin();
	while( m_kContSlotItem.end() != iter )
	{
		CONT_JSWB_UPGRADE_ITEM::mapped_type const& rkStatus = (*iter).second;
		if( false == JobSkillWorkBenchUtil::IsCanUpgrade(rkStatus.iItemNo) )
		{
			kOut.push_back( static_cast<int>( (*iter).first) );
		}
		++iter;
	}
}

bool PgJobSkillWorkBench::GetItemUpgradeInfo(size_t const iSlotNo, SJobSkillItemUpgrade& kOut)
{
	if( false == m_kContSlotItem.empty()
		&&	JobSkillWorkBenchUtil::iStartSlotNo <= iSlotNo )
	{
		SJobSkillWorkBenchSlotItemStatus kStatus;
		CONT_DEF_JOBSKILL_ITEM_UPGRADE::mapped_type kDefJSUpgradeInfo;
		if( JobSkillWorkBenchUtil::Get(m_kContSlotItem, iSlotNo, kStatus) 
			&&	JobSkillWorkBenchUtil::GetUpgradeInfo(kStatus.iItemNo, kDefJSUpgradeInfo)
			)
		{
			kOut = kDefJSUpgradeInfo;
			return true;
		}
	}
	return false;
}

void PgJobSkillWorkBench::ItemNo(int const& iItemNo)
{
	JobSkillWorkBenchUtil::SetAlterInfoFromItemNo(iItemNo, m_kAlterInfo);
	m_iItemNo = iItemNo;
}

// GM 커맨드
void PgJobSkillWorkBench::MakeCompleteOnNextTick()
{// 강제로 작업 턴을 완료 시킴
	CONT_JSWB_UPGRADE_ITEM::iterator iter = m_kContSlotItem.find( m_iCurUpgradeSlot );
	if( m_kContSlotItem.end() != iter )
	{
		CONT_JSWB_UPGRADE_ITEM::mapped_type& rkStatus = (*iter).second;
		rkStatus.iEleapsedSec = rkStatus.iTurnSec;
	}
}

void PgJobSkillWorkBench::MakeTroubleOnNextTick()
{// 강제로 다음틱에 작업대를 고장나게함
	m_iTroubleTime = 1;
}

bool PgJobSkillWorkBench::SetCurUpgradeSlotRemainTime(__int64 iRemainSec)
{
	CONT_JSWB_UPGRADE_ITEM::iterator find_iter = m_kContSlotItem.find( CurUpgradeSlot() );
	if( m_kContSlotItem.end() != find_iter )
	{
		SJobSkillWorkBenchSlotItemStatus& kInfo = (*find_iter).second;
		
		__int64 const i64RemainTime =  kInfo.iTurnSec - kInfo.iEleapsedSec;
		__int64 const i64Gab = i64RemainTime - iRemainSec;
		kInfo.iEleapsedSec += i64Gab;
		return true;
	}
	return false;
}

bool PgJobSkillWorkBench::SetCompleteSlotRemainTime(size_t stSlot, __int64 iRemainSec)
{
	CONT_JSWB_UPGRADE_ITEM::iterator find_iter = m_kContSlotItem.find( stSlot );
	if( m_kContSlotItem.end() != find_iter )
	{
		SJobSkillWorkBenchSlotItemStatus& kInfo = (*find_iter).second;
		if(!kInfo.IsRemainUpgrade())
		{
			__int64 const i64RemainTime =  kInfo.iTurnSec - kInfo.iEleapsedSec;
			__int64 const i64Gab = i64RemainTime - iRemainSec;
			kInfo.iEleapsedSec += i64Gab;
			return true;
		}
	}
	return false;
}

bool PgJobSkillWorkBench::IsAbleAutoRepair() const
{
	return m_kAlterInfo.IsAutoRepair();
}
bool PgJobSkillWorkBench::IsCompleteItemSendMail() const
{
	return m_kAlterInfo.IsCompleteItemSendMail();
}
//void PgJobSkillWorkBench::LoadDB( const SJobSkillWorkBenchStatus& rkWorkBenchStatus )
//{	
//	m_kCurrentUpgradeItem.iItemNo = rkWorkBenchStatus.iItemNo;
//	m_kCurrentUpgradeItem.iGrade = rkWorkBenchStatus.iItemGrade;
//	SetWorkBenchStatus( static_cast<EWorkBenchStatus>(rkWorkBenchStatus.iWorkBenchStatus) );
//	m_iRemainUpgradeCount = rkWorkBenchStatus.iRemainUpgradeCount;
//	m_kTurnBeginTime = rkWorkBenchStatus.kTurnBeginTime;
//	m_kTurnEndTime = rkWorkBenchStatus.kTurnEndTime;
//	m_iWorkBenchDuration = rkWorkBenchStatus.iDuration;
//}
//
//void PgJobSkillWorkBench::SaveDB( const bool bIsImmidiate )
//{
//	if( !bIsImmidiate )
//	{// 일정시간 마다 저장( 가공시작한 것만 )
//		BM::PgPackedTime kCheckTime = BM::PgPackedTime::LocalTime();
//		CGameTime::AddTime( kCheckTime, CGameTime::MINUTE );
//
//		if(	m_kTurnBeginTime.IsNull()
//		||	m_kTurnEndTime.IsNull()
//		||	kCheckTime < m_kLastSaveTime )
//		{
//			return ;
//		}
//	}
//
//	//CEL::DB_QUERY kQuery( DT_PLAYER, DQT_UPDATE_JOBSKILL_WORKBENCH, L"[dbo].[UP_Update_WorkBench_Status]" );
//	//kQuery.InsertQueryTarget( m_kWorkBenchGuid );
//	//kQuery.QueryOwner( m_kOwnerGuid );
//	//kQuery.PushStrParam( m_kWorkBenchGuid );
//	//kQuery.PushStrParam( m_kOwnerGuid );
//	//kQuery.PushStrParam( static_cast<int>( GetWorkBenchType() ) );
//	//kQuery.PushStrParam( m_kCurrentUpgradeItem.iItemNo );
//	//kQuery.PushStrParam( m_kCurrentUpgradeItem.iGrade );
//	//kQuery.PushStrParam( static_cast<int>( GetWorkBenchStatus() ) );
//	//kQuery.PushStrParam( m_iRemainUpgradeCount );	
//	//kQuery.PushStrParam( BM::DBTIMESTAMP_EX(m_kTurnBeginTime) );
//	//kQuery.PushStrParam( BM::DBTIMESTAMP_EX(m_kTurnEndTime) );
//	//kQuery.PushStrParam( m_iWorkBenchDuration );
//	//g_kCoreCenter.PushQuery( kQuery );
//
//	m_kLastSaveTime = BM::PgPackedTime::LocalTime(); // 저장 시간 업데이트
//}