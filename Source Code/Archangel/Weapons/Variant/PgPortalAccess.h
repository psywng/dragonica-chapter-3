#ifndef WEAPON_VARIANT_BASIC_GROUND_PGPORTALACCESS_H
#define WEAPON_VARIANT_BASIC_GROUND_PGPORTALACCESS_H

#include "Item.h"

typedef enum
{
	E_MOVE_LOCK				= 0x00,
	E_MOVE_PERSONAL			= 0x01,
	E_MOVE_PARTYMASTER		= 0x02,
	E_MOVE_ANY_PARTYMEMMBER	= 0x04,
}EPartyMoveType;

typedef enum eGateWayQuestType
{
	GWQT_Ing	=	0x01,	// 진행중일 때(실패 포함 안함)
	GWQT_IngAny	=	0x02,	// 진행중일 때(실패 포함)
	GWQT_Ended	=	0x04,	// 퀘스트를 완료 했을 때
	GWQT_Test_Flag	= GWQT_Ing| GWQT_Ended,	// 설정 가능 플래그 조합값
}EGateWayQuestType;

class PgPlayer;

class PgMapMoveChecker
{
public:
	explicit PgMapMoveChecker( CONT_DEFMAP const &kContDetMap );
	~PgMapMoveChecker(){}

	EPartyMoveType GetMoveType( int const iGroundNo )const;

private:
	CONT_DEFMAP	const &m_kContDefMap;

private:
	PgMapMoveChecker();

};

#pragma pack(1)
class PgPortalAccess
{
public:
	typedef struct tagKey_Base
	{
		tagKey_Base()
			:	iGroup(0)
		{}

		int iGroup;
	}SKey_Base;

	typedef struct tagKey
		:	public tagKey_Base
	{
		tagKey()
			:	iItemNo(0)
			,	iItemCount(1)
			,	kInvType(IT_ETC)
			,	bConsume(true)
			,	bParty(false)
		{
		}

		bool IsEmpty()const
		{
			return !iItemNo || !iItemCount;
		}

		int			iItemNo;
		size_t		iItemCount;
		EInvType	kInvType;
		bool		bConsume;//소모여부
		bool		bParty;
	}SKey;

	typedef struct tagQuestKey
		:	public tagKey_Base
	{
		tagQuestKey()
			:	sQuestNo(0)
			,	byState(0)
			,	bParty(false)
		{
		}

		bool IsEmpty()const
		{
			return !sQuestNo;
		}

		short int	sQuestNo;
		BYTE		byState;
		bool		bParty;
	}SQKey;

	typedef struct tagTimeKey
	{
		typedef enum
		{
			TIME_IN,
			TIME_YET,
			TIME_OVER,
		}E_TIME_RET;

		tagTimeKey( WORD const _wMin_Open=0, WORD const _wMin_Close=0 )
			:	wMin_Open(_wMin_Open)
			,	wMin_Close(_wMin_Close)
		{}

		bool operator < ( tagTimeKey const &rhs )const
		{
			return wMin_Open < rhs.wMin_Open;
		}

		int Check( WORD const wMin )const
		{
			if ( wMin_Open > wMin )
			{
				return TIME_YET;
			}
			if ( wMin < wMin_Close )
			{
				return TIME_IN;
			}
			return TIME_OVER;
		}

		bool operator > ( tagTimeKey const &rhs ){return rhs < *this;}
		bool operator ==( tagTimeKey const &rhs ){return !(rhs < *this) && !(*this < rhs); }

		WORD wMin_Open;
		WORD wMin_Close;
	}STimeKey;

	typedef std::set<int>			ConKey_Base;
	typedef std::vector<SKey>		ConKey;
	typedef std::vector<SQKey>		ConQKey;
	typedef std::vector<STimeKey>	ContTimeKey;

public:
	PgPortalAccess(void);
	~PgPortalAccess(void);// 상속금지?

	bool Build( TiXmlElement const *pkElement, CONT_DEFMAP const &kContDerMap );

	BYTE GetMoveType()const{return m_byMoveType;}
	bool IsAccess( PgPlayer *pkPlayer, bool bIsPartyMaster, CONT_PLAYER_MODIFY_ORDER *pContOrder )const;

	void Get( SReqMapMove_MT &rkRMM )const;

	int GetName()const{return m_iName;}
	int GetErrorMessage()const{return m_iErrorMessage;}

private:
	SGroundKey		m_kTargetGroundKey;
	short			m_nTargetPortal;
	BYTE			m_byMoveType;

	int				m_iName;

	// Access
	int		m_iErrorMessage;//이값이 0이면 기본메세지전송
	int		m_iLevelLimit_Min;
	int		m_iLevelLimit_Max;
	bool	m_bLevelParty;

	ConKey		m_kConKey;
	ConQKey		m_kConQuestKey;
	ContTimeKey	m_kContTimekey;

	ConKey_Base	m_kConKeyGroup;
	ConKey_Base	m_kConQuestKeyGroup;
};
typedef std::vector<PgPortalAccess>		CONT_PORTAL_ACCESS;

#pragma pack()

#endif // WEAPON_VARIANT_BASIC_GROUND_PGPORTALACCESS_H