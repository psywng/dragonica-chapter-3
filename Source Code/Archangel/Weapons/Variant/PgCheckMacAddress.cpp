#include "stdafx.h"
#include "BM/LocalMgr.h"
#include "PgCheckMacAddress.h"

// 바른손 내부 테스트 서버
// SetMacAddress() 내의 해당 국가에서만 호출 되어야 함!
void PgCheckMacAddress::setInternal()
{
	m_kContMacAddr.insert(L"00-15-17-46-F4-7A");
	m_kContMacAddr.insert(L"00-15-17-A7-47-44");
	m_kContMacAddr.insert(L"00-15-17-AA-44-34");
	m_kContMacAddr.insert(L"00-15-17-76-FF-00");
	m_kContMacAddr.insert(L"00-30-48-7E-3E-B2");
	m_kContMacAddr.insert(L"00-15-17-4F-B0-EC");
	m_kContMacAddr.insert(L"00-30-48-7E-6F-F4");
	m_kContMacAddr.insert(L"00-30-48-C0-89-1C");
	m_kContMacAddr.insert(L"00-15-17-9F-5E-C8");
	m_kContMacAddr.insert(L"00-15-17-7E-CA-A4");
	m_kContMacAddr.insert(L"00-15-17-AA-49-84");
	m_kContMacAddr.insert(L"00-30-48-C0-89-26");
}

void PgCheckMacAddress::SetMacAddress( LOCAL_MGR::NATION_CODE const eNationCode )
{
	// !! 해당 브랜치의 국가만 추가하세요 !!
	switch( eNationCode )
	{
	case LOCAL_MGR::NC_KOREA:
		{
			//setInternal(); // 바른손 내부 테스트 서버
		}break;
	case LOCAL_MGR::NC_DEVELOP:
		{
			//setInternal(); // 바른손 내부 테스트 서버
		}break;
	default:
		{
		}break;
	}
}

bool PgCheckMacAddress::CheckMacAddress()
{
	if( m_kContMacAddr.empty() )
	{
		return true;	// 개발용

		/*
			머지 주의!!
			위에 주석처리하고 이거 풀어야 함
			return false;	// 실제 서비스 국가
		*/
	}

	PIP_ADAPTER_INFO pAdapterInfo = NULL;
	PIP_ADAPTER_INFO pInfo = NULL;
	DWORD dwSize = 0;

	if( ERROR_BUFFER_OVERFLOW == GetAdaptersInfo( pAdapterInfo, &dwSize ) )
	{	
		pAdapterInfo = (PIP_ADAPTER_INFO) malloc( dwSize );
		GetAdaptersInfo( pAdapterInfo, &dwSize );
	}

	if( NO_ERROR == GetAdaptersInfo( pAdapterInfo, &dwSize ) )
	{
		pInfo = pAdapterInfo;
		while( pInfo ) // 모든 Mac Address search
		{
			wchar_t kTemp[255] = {0,};
			wsprintf( kTemp, L"%02x-%02x-%02x-%02x-%02x-%02x",
				pInfo->Address[0],
				pInfo->Address[1],
				pInfo->Address[2],
				pInfo->Address[3],
				pInfo->Address[4],
				pInfo->Address[5] );

			std::wstring kMacAddress(kTemp);
			UPR(kMacAddress);			
			CONT_MACADDR::const_iterator mac_iter = m_kContMacAddr.find( kMacAddress );
			if( mac_iter != m_kContMacAddr.end() )
			{
				free( pAdapterInfo );
				return true;
			}

			pInfo = pInfo->Next;
		}
	}

	free( pAdapterInfo );
	return false;
}