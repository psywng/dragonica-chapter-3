#ifndef WEAPON_VARIANT_JOBSKILL_PGJOBSKILLWORKBENCHMGR_H
#define WEAPON_VARIANT_JOBSKILL_PGJOBSKILLWORKBENCHMGR_H

class PgJobSkillWorkBenchAlterInfo;
class PgJobSkillWorkBench;

typedef struct tagWorkBenchCompleteInfo
{
	tagWorkBenchCompleteInfo()
		: eLogType(WBET_NONE), kOwnerGuid(), kWorkBenchGuid(), iWorkBenchItemNo(0), iWorkItemNo(0), iResultItemNo(0), iDuration(0), iUseDuration(0), kBlessCharGuid(), iBlessSkillNo(0)
	{
	}
	tagWorkBenchCompleteInfo(EWorkBenchEventType const EventType, BM::GUID const& rkOwnerGuid, BM::GUID const& rkWorkBenchGuid, PgJobSkillWorkBench const& rkWorkBench, int const WorkItemNo, int const ResultItemNo, int const iOldDuration)
		: eLogType(EventType), kOwnerGuid(rkOwnerGuid), kWorkBenchGuid(rkWorkBenchGuid), iWorkBenchItemNo(rkWorkBench.ItemNo())
		, iWorkItemNo(WorkItemNo), iResultItemNo(ResultItemNo), iDuration(rkWorkBench.Duration()), iUseDuration(iOldDuration - rkWorkBench.Duration())
		, kBlessCharGuid(rkWorkBench.BlessCharGuid()), iBlessSkillNo(rkWorkBench.BlessSkillNo())
	{
	}

	EWorkBenchEventType eLogType;
	BM::GUID kOwnerGuid;
	BM::GUID kWorkBenchGuid;
	int iWorkBenchItemNo;
	int iWorkItemNo;
	int iResultItemNo;
	int iDuration;
	int iUseDuration;
	BM::GUID kBlessCharGuid;
	int iBlessSkillNo;

	DEFAULT_TBL_PACKET_FUNC();
} SWorkBenchCompleteInfo;
typedef std::list< SWorkBenchCompleteInfo > CONT_WORKBENCH_COMPLETE_INFO;
typedef std::list< std::pair< BM::GUID, int > > CONT_WORKBENCH_DELETE_ITEM;

class PgJobSkillWorkBenchMgr
{
public:
	typedef std::map< BM::GUID, PgJobSkillWorkBench > CONT_JS_WORK_BENCH; // ItemGuid, WorkBench 1:1
public:
	PgJobSkillWorkBenchMgr();
	PgJobSkillWorkBenchMgr(BM::GUID const& rkHomeGuid, BM::GUID const& rkOwnerGuid);
	~PgJobSkillWorkBenchMgr();

	int Pop(BM::GUID const& rkItemGuid, size_t const iSlotNo);
	EWorkBenchResult Push(BM::GUID const& rkItemGuid, size_t const iSlotNo, int const iItemNo);
	EWorkBenchResult Del(BM::GUID const& rkItemGuid, bool& bIsPublicAlterWorkBench_out);
	bool Add(BM::GUID const& rkItemGuid, int const iItemNo, int const iDuration, bool& bIsPublicAlterWorkBench_out);

	void WriteToPacket(BM::CPacket& rkPacket) const;
	void WriteToPacket(BM::GUID const& rkItemGuid, BM::CPacket& rkPacket) const;
	void ReadFromPacket(BM::CPacket& rkPacket);
	void ReadFromPacket(BM::GUID const& rkItemGuid, BM::CPacket& rkPacket);

	void ReadFromDB(BM::GUID const& rkItemGuid, CEL::DB_DATA_ARRAY::const_iterator& iter);
	bool WriteToDB(BM::GUID const& rkItemGuid, CEL::DB_QUERY& rkQuery) const;

	void Process(__int64 const iEleapsedTime, CONT_WORKBENCH_COMPLETE_INFO& rkOutCompleteInfo, CONT_WORKBENCH_DELETE_ITEM& rkContDeleteItem, size_t& stWorkedAtThisTimeSlotNo); // call 10second
	
	bool GetWorkBench(BM::GUID const& rkWorkBenchGuid, PgJobSkillWorkBench& kOut) const;
	void Clear();
	void SaveToDB(BM::GUID const& rkItemGuid);

	CONT_JS_WORK_BENCH const CopyAllWorkBenchInfo() const;
		
	bool RepairWorkBench(BM::GUID const& rkWorkBenchGuid);	// 고장 상태를 수리 함
	bool BlessWorkBench(BM::GUID const& rkWorkBenchGuid, BM::GUID const& rkCharGuid, int const iBlessSkillNo);	// 축복

	size_t GetEmptySlotCnt(int const iGatherType);
	size_t GetNotEmptySlotCnt(int const iGatherType);
	size_t GetCompleteSlotCnt(int const iGatherType);

	bool IsExistWorkBenchInThisStatus(int const iGatherType, EWorkBenchStatus const eStatus);
	bool IsExistBlessedWorkBench(int const iGatherType);
	bool IsExistWorkBench(int const iGatherType);
	size_t GetWorkBenchCnt() const;	// 설치된 작업대의 갯수

	bool IsBlessedWorkBench(BM::GUID const& rkWorkBenchGuid) const;
	__int64 GetBlessRemainTime(BM::GUID const& rkCharGuid) const;

	bool IsExistWorkBench() const;	//작업대가 1개라도 존재하는가
	
	bool SetPublicAlterInfo(int const iItemNo);
	bool ClearPublicAlterInfo(int const iItemNo);

	bool IsAbleAutoRepair(BM::GUID const& rkWorkBenchGuid) const;			// 자동수리가 가능한가
	bool IsCompleteItemSendMail(BM::GUID const& rkWorkBenchGuid) const;		// 아이템 
	// GM 커맨드
	void MakeCompleteAllWorkBenchOnNextTick();	// 강제로 모든 작업대를 다음틱에 완료 시킴
	void MakeTroubleAllWorkBenchOnNextTick();	// 강제로 모든 작업대를 다음틱에 고장나게함
	void SetBlessRemainTime_DelBlessGuid(BM::GUID const& rkCharGuid, __int64 const iSec); // 버프 주기 남은 시간을 조정함
	bool SetCurUpgradeSlotRemainTime(BM::GUID const& rkItemGuid, __int64 const iSec);
	bool SetCompleteSlotRemainTime(BM::GUID const& rkItemGuid, size_t stSlot, __int64 const iSec);

	bool CheckMajorWorkBenchStatus();

private:
	CONT_JS_WORK_BENCH m_kCont;
	CLASS_DECLARATION_S_NO_SET(BM::GUID, HomeGuid); // 마이홈 Guid
	CLASS_DECLARATION_S(BM::GUID, OwnerGuid); // 마이홈 소유자 Guid
	CLASS_DECLARATION_S(BM::GUID, OwnerMemberGuid); // 마이홈 소유자 MemberGuid
	CLASS_DECLARATION_S(ContGuidSet, ContBlessGuid);
	CLASS_DECLARATION_S(EWorkBenchStatus, OldMajorStatus);
	CLASS_DECLARATION_S(PgJobSkillWorkBenchAlterInfo, PublicAlterInfo);	//모든 작업대에 적용될 조정값(관리작업대)
	CLASS_DECLARATION(bool, m_bPublicAlterSeted, PublicAlterSeted);		//관리작업대가 설치되었는가

};

#endif // WEAPON_VARIANT_JOBSKILL_PGJOBSKILLWORKBENCHMGR_H