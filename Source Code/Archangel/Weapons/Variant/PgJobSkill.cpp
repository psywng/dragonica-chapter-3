#include "stdafx.h"
#include "PgJobSkill.h"

#include "PgPlayer.h"
#include "tabledatamanager.h"
#include "PgJobSkillExpertness.h"
#include "PgJobSkillTool.h"

__int64 const BASIC_LEARN_COST = 2;
float const ENCREASE_COST_RATE = 2.5f;

EJOBSKILL_LEARN_RET JobSkill_LearnUtil::IsEnableLearnJobSkill(PgPlayer *pPlayer, int const iJobSkillNo)
{
	if(!pPlayer)
	{
		return JSLR_ERR;
	}
	PgMySkill* pMySkill = pPlayer->GetMySkill();
	if(!pMySkill)
	{
		return JSLR_ERR;
	}	

	CONT_DEF_JOBSKILL_SKILL const* pkContDefJobSkill;
	g_kTblDataMgr.GetContDef(pkContDefJobSkill);
	if(0 == pkContDefJobSkill->size())
	{
		return JSLR_ERR;
	}
	CONT_DEF_JOBSKILL_SKILL::const_iterator iter_Skill = pkContDefJobSkill->find(iJobSkillNo);
	if(iter_Skill == pkContDefJobSkill->end() )
	{//직업스킬이 아니다
		return JSLR_ERR;
	}
	CONT_DEF_JOBSKILL_SKILL::mapped_type const &rkJobSkillInfo = (*iter_Skill).second;

	bool const bLearnedSkill = pMySkill->IsExist(iJobSkillNo);
	if( true == bLearnedSkill )
	{//이미 배운 스킬이다.
		return JSLR_ALREADY_BUY;
	}
	
	//if( JST_2ND_MAIN == rkJobSkillInfo.eJobSkill_Type)
	//{
	//	int const iHaveCount = GetHaveJobSkillTypeCount(pPlayer, rkJobSkillInfo.eJobSkill_Type);
	//	if( EJobSkillEnum::JSE_LEARN_2ND_MAX <= iHaveCount)
	//	{//최대 갯수 만큼 배웠다.(2차 직업 전용)
	//		return JSLR_FULL_COUNT;
	//	}
	//}

	if( 0 != rkJobSkillInfo.i01NeedParent_JobSkill_No)
	{//선행 스킬이 필요 할 경우(보조 스킬의 경우)
		PgJobSkillExpertness const& rkJobSkillExpertness = pPlayer->JobSkillExpertness();
		bool const bIsHave = rkJobSkillExpertness.IsHave( rkJobSkillInfo.i01NeedParent_JobSkill_No );
		if( !bIsHave )
		{//선행스킬 안배웟다.
			return JSLR_NEED_SKILL;
		}
		else
		{//배웠는데,
			int iExpertness = rkJobSkillExpertness.Get(rkJobSkillInfo.i01NeedParent_JobSkill_No);
			if( rkJobSkillInfo.i01NeedParent_JobSkill_Expertness > iExpertness)
			{//숙련도가 부족하다.
				return JSLR_NEED_EXPERTNESS;
			}
		}
	}
	else
	{//선행 스킬이 필요 없을 경우(주 스킬의 경우)
		int const iClass = pPlayer->GetAbil(AT_CLASS);
		if( UCLASS_THIEF >= iClass )
		{//1차전직 안했다.
			return JSLR_NOT_CLASS;
		}
	}

	__int64 const iNeedMoney = GetLearnCost(pPlayer, iJobSkillNo);
	__int64 const iHaveMoney = pPlayer->GetAbil64(AT_MONEY);
	if( iHaveMoney < iNeedMoney)
	{
		return JSLR_NEED_MONEY;
	}
	return JSLR_OK;
}

__int64 JobSkill_LearnUtil::GetLearnCost(PgPlayer *pPlayer, int const iJobSkillNo)
{
	if(!pPlayer)
	{
		return 0;
	}
	
	CONT_DEF_JOBSKILL_SKILL const* pkContDefJobSkill;
	g_kTblDataMgr.GetContDef(pkContDefJobSkill);
	if(0 == pkContDefJobSkill->size())
	{
		return false;
	}
	CONT_DEF_JOBSKILL_SKILL::const_iterator iter_Skill = pkContDefJobSkill->find(iJobSkillNo);
	if(iter_Skill == pkContDefJobSkill->end() )
	{
		return 0;
	}
	CONT_DEF_JOBSKILL_SKILL::mapped_type const &rkJobSkillInfo = (*iter_Skill).second;

	int const iHaveCount = GetHaveJobSkillTypeCount(pPlayer, rkJobSkillInfo.eJobSkill_Type);
	__int64 iPrice = 0;
	float iRate = pow(ENCREASE_COST_RATE, iHaveCount);	//소수점 버림
	iPrice = BASIC_LEARN_COST * iRate;
	iPrice = iPrice * 10000;	//골드단위니까 곱하기 1만 해주기
	return iPrice;
}


int JobSkill_LearnUtil::GetHaveJobSkillTypeCount(PgPlayer *pPlayer, EJobSkillType const eType)
{
	if(!pPlayer)
	{
		return 0;
	}
	PgMySkill* pMySkill = pPlayer->GetMySkill();
	if(!pMySkill)
	{
		return 0;
	}	
	int iCount = 0;
	CONT_DEF_JOBSKILL_SKILL const* pkContDefJobSkill;
	g_kTblDataMgr.GetContDef(pkContDefJobSkill);
	if(0 == pkContDefJobSkill->size())
	{
		return 0;
	}

	bool bMainSkill = IsMainJobSkill(eType);

	CONT_DEF_JOBSKILL_SKILL::const_iterator iter_Skill = pkContDefJobSkill->begin();
	for(iter_Skill; iter_Skill != pkContDefJobSkill->end(); ++iter_Skill)
	{
		CONT_DEF_JOBSKILL_SKILL::key_type const &rkJobSkillNo = (*iter_Skill).first;
		CONT_DEF_JOBSKILL_SKILL::mapped_type const &rkJobSkillInfo = (*iter_Skill).second;

		if( bMainSkill == IsMainJobSkill(rkJobSkillInfo.eJobSkill_Type) )
		{
			bool const bHaveSkill = pMySkill->IsExist(rkJobSkillNo);
			if(bHaveSkill)
			{
				++iCount;
			}
		}
	}
	return iCount;
}

int JobSkill_LearnUtil::GetHaveJobSkillTypeCount(PgPlayer *pPlayer, int const iJobSkillNo)//같은 타입스킬 몇개 가지고 있냐
{
	if(!pPlayer)
	{
		return 0;
	}
	PgMySkill* pMySkill = pPlayer->GetMySkill();
	if(!pMySkill)
	{
		return 0;
	}	
	CONT_DEF_JOBSKILL_SKILL const* pkContDefJobSkill;
	g_kTblDataMgr.GetContDef(pkContDefJobSkill);
	if(0 == pkContDefJobSkill->size())
	{
		return false;
	}
	CONT_DEF_JOBSKILL_SKILL::const_iterator iter_Skill = pkContDefJobSkill->find(iJobSkillNo);
	if( pkContDefJobSkill->end() == iter_Skill )
	{
		return false;
	}
	CONT_DEF_JOBSKILL_SKILL::mapped_type const &rkJobSkillInfo = (*iter_Skill).second;
	EJobSkillType const eOrgType = rkJobSkillInfo.eJobSkill_Type;

	int iCount = 0;

	bool bMainSkill = IsMainJobSkill(eOrgType);

	CONT_DEF_JOBSKILL_SKILL::const_iterator iter_Comp = pkContDefJobSkill->begin();
	for(iter_Comp; iter_Comp != pkContDefJobSkill->end(); ++iter_Comp)
	{
		CONT_DEF_JOBSKILL_SKILL::key_type const &rkJobSkillNo_Comp = (*iter_Comp).first;
		CONT_DEF_JOBSKILL_SKILL::mapped_type const &rkJobSkillInfo_Comp = (*iter_Comp).second;
		if( bMainSkill == IsMainJobSkill(rkJobSkillInfo.eJobSkill_Type) )
		{
			bool const bHaveSkill = pMySkill->IsExist(rkJobSkillNo_Comp);
			if(bHaveSkill)
			{
				++iCount;
			}
		}
	}
	return iCount;
}

int JobSkill_LearnUtil::GetHaveJobSkillTypeCount_Detail(PgPlayer *pPlayer, int const iJobSkillNo)//같은 채집타입을 몇개 가지고 있냐
{
	if(!pPlayer)
	{
		return 0;
	}
	PgMySkill* pMySkill = pPlayer->GetMySkill();
	if(!pMySkill)
	{
		return 0;
	}	
	CONT_DEF_JOBSKILL_SKILL const* pkContDefJobSkill;
	g_kTblDataMgr.GetContDef(pkContDefJobSkill);
	if(0 == pkContDefJobSkill->size())
	{
		return false;
	}
	CONT_DEF_JOBSKILL_SKILL::const_iterator iter_Skill = pkContDefJobSkill->find(iJobSkillNo);
	if( pkContDefJobSkill->end() == iter_Skill )
	{
		return false;
	}
	CONT_DEF_JOBSKILL_SKILL::mapped_type const &rkJobSkillInfo = (*iter_Skill).second;
	EJobSkillType const eOrgType = rkJobSkillInfo.eJobSkill_Type;

	int iCount = 0;

	CONT_DEF_JOBSKILL_SKILL::const_iterator iter_Comp = pkContDefJobSkill->begin();
	for(iter_Comp; iter_Comp != pkContDefJobSkill->end(); ++iter_Comp)
	{
		CONT_DEF_JOBSKILL_SKILL::key_type const &rkJobSkillNo_Comp = (*iter_Comp).first;
		CONT_DEF_JOBSKILL_SKILL::mapped_type const &rkJobSkillInfo_Comp = (*iter_Comp).second;
		if( eOrgType == rkJobSkillInfo_Comp.eJobSkill_Type)
		{
			bool const bHaveSkill = pMySkill->IsExist(rkJobSkillNo_Comp);
			if(bHaveSkill)
			{
				++iCount;
			}
		}
	}
	return iCount;
}

bool JobSkill_LearnUtil::IsEnableUseJobSkill(PgPlayer *pPlayer, int const iJobSkillNo)
{
	if(!pPlayer)
	{
		return false;
	}
	PgMySkill* pMySkill = pPlayer->GetMySkill();
	if(!pMySkill)
	{
		return false;
	}	

	CONT_DEF_JOBSKILL_SKILL const* pkContDefJobSkill;
	g_kTblDataMgr.GetContDef(pkContDefJobSkill);
	if(0 == pkContDefJobSkill->size())
	{
		return false;
	}
	CONT_DEF_JOBSKILL_SKILL::const_iterator iter_Skill = pkContDefJobSkill->find(iJobSkillNo);
	if(iter_Skill == pkContDefJobSkill->end() )
	{//직업스킬이 아니다
		return false;
	}
	CONT_DEF_JOBSKILL_SKILL::mapped_type const &rkJobSkillInfo = (*iter_Skill).second;

	bool const bLearnedSkill = pMySkill->IsExist(iJobSkillNo);
	if( false == bLearnedSkill )
	{//안배운 스킬이다.
		return false;
	}

	if( 0 != rkJobSkillInfo.i01NeedParent_JobSkill_No)
	{//선행 스킬이 필요 할 경우(보조 스킬의 경우)
		PgJobSkillExpertness const& rkJobSkillExpertness = pPlayer->JobSkillExpertness();
		bool const bIsHave = rkJobSkillExpertness.IsHave( rkJobSkillInfo.i01NeedParent_JobSkill_No );
		if( !bIsHave )
		{//선행스킬 안배웟다.
			return false;
		}
		else
		{//배웠는데,
			int iExpertness = rkJobSkillExpertness.Get(rkJobSkillInfo.i01NeedParent_JobSkill_No);
			if( rkJobSkillInfo.i01NeedParent_JobSkill_Expertness > iExpertness)
			{//숙련도가 부족하다.
				return false;
			}
		}
	}
	else
	{//선행 스킬이 필요 없을 경우(주 스킬의 경우)
		int const iClass = pPlayer->GetAbil(AT_CLASS);
		if( UCLASS_THIEF >= iClass )
		{//1차전직 안했다.
			return false;
		}
	}
	return true;
}

bool JobSkill_LearnUtil::IsMainJobSkill(EJobSkillType const eType) //주스킬이냐
{
	if( eType == JST_1ST_MAIN
	|| eType == JST_2ND_MAIN
	|| eType == JST_3RD_MAIN)
	{
		return true;
	}
	return false;
}

int JobSkill_Util::GetJobSkillPenaltyLevel(PgPlayer const *pPlayer, int const iJobSkillNo)//디버프 레벨이 몇이냐
{
	if(!pPlayer)
	{
		return 0;
	}
	PgJobSkillExpertness::VALUE_TYPE rkDestExpertness;
	PgJobSkillExpertness const &rkMyExpertness = pPlayer->JobSkillExpertness();
	if(!rkMyExpertness.Get(iJobSkillNo, rkDestExpertness))
	{
		return 0;
	}

	PgJobSkillExpertness::CONT_EXPERTNESS const& rkContJobSkill = rkMyExpertness.GetAllSkillExpertness();
	PgJobSkillExpertness::CONT_EXPERTNESS::const_iterator find_iter = rkContJobSkill.begin();
	std::list<PgJobSkillExpertness::VALUE_TYPE> kContPenaltyList;
	while( find_iter != rkContJobSkill.end() )
	{
		PgJobSkillExpertness::VALUE_TYPE const &rkValue = (*find_iter).second;
		kContPenaltyList.push_back(rkValue);
		++find_iter;
	}

	if( !kContPenaltyList.size() )
	{
		return 0;
	}
	kContPenaltyList.sort();

	int const iMyExpertness = rkDestExpertness / JSE_EXPERTNESS_DEVIDE;
	int iPenaltyLevel = 0;
	int iCurrentMax = 0;
	std::list<int>::const_iterator penalty_iter = kContPenaltyList.begin();
	while( penalty_iter != kContPenaltyList.end() )
	{
		int const iPenalty = (*penalty_iter) / JSE_EXPERTNESS_DEVIDE;
		if( iMyExpertness < iPenalty)
		{
			if(iCurrentMax < iPenalty )
			{
				++iPenaltyLevel;
				iCurrentMax = iPenalty;
			}
		}
		++penalty_iter;
	}
	return iPenaltyLevel;
}

bool JobSkill_Util::IsJobSkill_Tool(int const iItemNo)
{
	CONT_DEF_JOBSKILL_TOOL const *pkDefJobSkillTool = NULL;
	g_kTblDataMgr.GetContDef(pkDefJobSkillTool);
	CONT_DEF_JOBSKILL_TOOL::const_iterator find_iter = pkDefJobSkillTool->find(iItemNo);
	if( find_iter != pkDefJobSkillTool->end())
	{
		return true;
	}
	return false;
}

bool JobSkill_Util::IsUseableJobSkill_Tool(PgPlayer *pPlayer, int const iItemNo)
{	
	if(!pPlayer)
	{
		return false;
	}
	bool bResult = false;
	CONT_DEF_JOBSKILL_TOOL::mapped_type kJobTool;
	if( true == JobSkillToolUtil::GetToolInfo(iItemNo, kJobTool) )
	{//채집 도구의 경우
		if(pPlayer)
		{
			bResult = (JobSkill_LearnUtil::IsEnableUseJobSkill(pPlayer, kJobTool.i01Need_Skill_No));
		}
	}
	return bResult;
}
bool JobSkill_Util::IsUseableJobSkill_SaveIndex(PgPlayer *pPlayer, int const iItemNo)
{
	if(!pPlayer)
	{
		return false;
	}
	bool bResult = false;
	CONT_DEF_JOBSKILL_SAVEIDX const* pkDefJobSkillSaveIdx = NULL;
	g_kTblDataMgr.GetContDef(pkDefJobSkillSaveIdx);
	CONT_DEF_JOBSKILL_SAVEIDX::const_iterator iter = pkDefJobSkillSaveIdx->begin();
	while( iter != pkDefJobSkillSaveIdx->end() )
	{
		if( iter->second.iBookItemNo == iItemNo)
		{
			break;
		}
		++iter;
	}
	if( pkDefJobSkillSaveIdx->end() != iter )
	{//채집 도감의 경우
		CONT_DEF_JOBSKILL_SAVEIDX::mapped_type const &rkSaveIndex = (*iter).second;
		int const iNeedSkill = rkSaveIndex.iNeedSkillNo01;
		int const iNeedExpertness= rkSaveIndex.iNeedSkillExpertness01;
		if(pPlayer)
		{
			int const iSaveIdx = rkSaveIndex.iSaveIdx;
			if( false == pPlayer->JobSkillSaveIdx().Get(iSaveIdx) )
			{
				PgMySkill* pMySkill = pPlayer->GetMySkill();
				if(pMySkill)
				{//안배웠고, 숙련도 충족할때만
					bool bCantUse = pMySkill->IsExist(iNeedSkill);
					PgJobSkillExpertness const& rkJobSkillExpertness = pPlayer->JobSkillExpertness();
					int const iExpertness = rkJobSkillExpertness.Get(iNeedSkill);
					bResult = ( iExpertness >= iNeedExpertness ? true : false );
				}
			}
			else
			{
				bResult = false;
			}
		}
	}
	return bResult;
}

bool JobSkill_Util::IsJobSkill_SaveIndex(int const iItemNo)
{
	CONT_DEF_JOBSKILL_SAVEIDX const* pkDefJobSkillSaveIdx = NULL;
	g_kTblDataMgr.GetContDef(pkDefJobSkillSaveIdx);
	CONT_DEF_JOBSKILL_SAVEIDX::const_iterator iter = pkDefJobSkillSaveIdx->begin();
	while( iter != pkDefJobSkillSaveIdx->end() )
	{
		if( iter->second.iBookItemNo == iItemNo )
		{
			return true;
		}
		++iter;
	}
	return false;
}
bool JobSkill_Util::GetJobSkill_SaveIndex(int const iItemNo, CONT_DEF_JOBSKILL_SAVEIDX::mapped_type& rkSaveIdx)
{
	CONT_DEF_JOBSKILL_SAVEIDX const* pkDefJobSkillSaveIdx = NULL;
	g_kTblDataMgr.GetContDef(pkDefJobSkillSaveIdx);
	CONT_DEF_JOBSKILL_SAVEIDX::const_iterator iter = pkDefJobSkillSaveIdx->begin();
	while( iter != pkDefJobSkillSaveIdx->end() )
	{
		if( iter->second.iBookItemNo == iItemNo )
		{
			rkSaveIdx = (*iter).second;
			return true;
		}
		++iter;
	}
	return false;
}
bool JobSkill_Util::IsJobSkill_Item(int const iItemNo)
{
	CONT_DEF_JOBSKILL_ITEM_UPGRADE const* pkDefJobSkill_ItemUpgrade = NULL;
	g_kTblDataMgr.GetContDef(pkDefJobSkill_ItemUpgrade);
	CONT_DEF_JOBSKILL_ITEM_UPGRADE::const_iterator find_iter = pkDefJobSkill_ItemUpgrade->find(iItemNo);
	if( pkDefJobSkill_ItemUpgrade->end() != find_iter)
	{
		return true;
	}
	return false;
}
bool JobSkill_Util::GetJobSkill_Item(int const iItemNo, CONT_DEF_JOBSKILL_ITEM_UPGRADE::mapped_type &rkItemUpgrade)
{
	CONT_DEF_JOBSKILL_ITEM_UPGRADE const* pkDefJobSkill_ItemUpgrade = NULL;
	g_kTblDataMgr.GetContDef(pkDefJobSkill_ItemUpgrade);
	CONT_DEF_JOBSKILL_ITEM_UPGRADE::const_iterator find_iter = pkDefJobSkill_ItemUpgrade->find(iItemNo);
	if( pkDefJobSkill_ItemUpgrade->end() != find_iter)
	{
		rkItemUpgrade = (*find_iter).second;
		return true;
	}
	return false;
}

EJobSkillType JobSkill_Util::GetJobSkillType(int const iSkillNo)
{
	CONT_DEF_JOBSKILL_SKILL const* pkContDefJobSkill;
	g_kTblDataMgr.GetContDef(pkContDefJobSkill);
	
	CONT_DEF_JOBSKILL_SKILL::const_iterator find_iter = pkContDefJobSkill->find(iSkillNo);
	if(find_iter != pkContDefJobSkill->end() )
	{
		CONT_DEF_JOBSKILL_SKILL::mapped_type const &rkJobSkill = (*find_iter).second;

		return rkJobSkill.eJobSkill_Type;
	}

	return JST_NONE;

}

bool JobSkill_Util::IsJobSkill_Machine(int const iItemNo)
{
	CONT_DEF_JOBSKILL_MACHINE const * pkContDefJobSkillMachine;
	g_kTblDataMgr.GetContDef(pkContDefJobSkillMachine);
	CONT_DEF_JOBSKILL_MACHINE::const_iterator find_iter = pkContDefJobSkillMachine->find(iItemNo);

	if( find_iter != pkContDefJobSkillMachine->end())
	{
		return true;
	}
	return false;
}

bool JobSkill_Util::GetJobSkill_Machine(int const iItemNo, CONT_DEF_JOBSKILL_MACHINE::mapped_type & rkDataOut)
{
	CONT_DEF_JOBSKILL_MACHINE const * pkContDefJobSkillMachine;
	g_kTblDataMgr.GetContDef(pkContDefJobSkillMachine);
	CONT_DEF_JOBSKILL_MACHINE::const_iterator find_iter = pkContDefJobSkillMachine->find(iItemNo);

	if( find_iter != pkContDefJobSkillMachine->end() )
	{
		rkDataOut = (*find_iter).second;
		return true;
	}
	return false;
}

eJobSkillMachineType JobSkill_Util::GetJobSkillMachineType(int const iItemNo)
{
	CONT_DEF_JOBSKILL_MACHINE const * pkContDefJobSkillMachine;
	g_kTblDataMgr.GetContDef(pkContDefJobSkillMachine);
	CONT_DEF_JOBSKILL_MACHINE::const_iterator find_iter = pkContDefJobSkillMachine->find(iItemNo);

	if( find_iter != pkContDefJobSkillMachine->end() )
	{
		CONT_DEF_JOBSKILL_MACHINE::mapped_type const& rkDataOut = (*find_iter).second;
		return static_cast<eJobSkillMachineType>(rkDataOut.iGatherType);
	}
	return JSMT_ERR;
}

bool JobSkill_Util::IsUseableJobSkill_Machine(PgPlayer* pPlayer, int const iItemNo)
{
	bool bResult = false;
	CONT_DEF_JOBSKILL_MACHINE const *pkContDefJobSkillMachine;
	g_kTblDataMgr.GetContDef(pkContDefJobSkillMachine);
	CONT_DEF_JOBSKILL_MACHINE::const_iterator find_iter = pkContDefJobSkillMachine->find(iItemNo);
	if( find_iter != pkContDefJobSkillMachine->end() )
	{
		CONT_DEF_JOBSKILL_MACHINE::mapped_type const &rkMachine = (*find_iter).second;
		PgMySkill* pMySkill = pPlayer->GetMySkill();
		if(pMySkill)
		{
			bResult = pMySkill->IsExist(rkMachine.iNeedJobSkillNo01);
			if(bResult)
			{
				int const iCurExpertness = pPlayer->JobSkillExpertness().Get(rkMachine.iNeedJobSkillNo01);
				if( iCurExpertness >= rkMachine.iNeedJobSkillExpertness01)
				{
					bResult = true;
				}
			}
		}
	}
	return bResult;
}

eJobSkillMaterialType JobSkill_Util::GetJobSkillMaterialType(int const iItemNo)
{
	CONT_DEF_JOBSKILL_ITEM_UPGRADE const * pkContDefJobSkillItemUpgrade;
	g_kTblDataMgr.GetContDef(pkContDefJobSkillItemUpgrade);
	CONT_DEF_JOBSKILL_ITEM_UPGRADE::const_iterator find_iter = pkContDefJobSkillItemUpgrade->find(iItemNo);

	if( find_iter != pkContDefJobSkillItemUpgrade->end() )
	{
		CONT_DEF_JOBSKILL_ITEM_UPGRADE::mapped_type const& rkDataOut = (*find_iter).second;
		return static_cast<eJobSkillMaterialType>(rkDataOut.iResourceType);
	}
	return JSMRT_ERR;
}

//
SJobSkillSaveIdx const * const JobSkill_Third::GetJobSkillSaveIdx(int const iSaveIdx)
{
	CONT_DEF_JOBSKILL_SAVEIDX const* pkContDefSaveIdx = NULL;
	g_kTblDataMgr.GetContDef(pkContDefSaveIdx);
	if( !pkContDefSaveIdx )
	{
		return NULL;
	}

	CONT_DEF_JOBSKILL_SAVEIDX::const_iterator c_iter = pkContDefSaveIdx->find(iSaveIdx);
	if( c_iter == pkContDefSaveIdx->end() )
	{
		return NULL;
	}
	
	return &(c_iter->second);
}

int JobSkill_Third::GetResourceProbabilityUp(PgInventory * const pkInv, CONT_JS3_RESITEM_INFO const& kContResItemInfo)
{
	int iRate = 0;
	if( !pkInv )
	{
		return iRate;
	}

	PgBase_Item kItem;
	CONT_DEF_JOBSKILL_ITEM_UPGRADE::mapped_type kItemUpgrade;
	CONT_JS3_RESITEM_INFO::const_iterator c_iter = kContResItemInfo.begin();
	while(c_iter != kContResItemInfo.end())
	{
		if( JobSkill_Util::GetJobSkill_Item((*c_iter).first, kItemUpgrade) )
		{
			iRate += kItemUpgrade.iResourceProbabilityUp * (*c_iter).second;
		}

		++c_iter;
	}

	return iRate;
}


int JobSkill_Third::GetJobSkill3ProbabilityNo(int const iItemNo)
{
	CONT_DEF_JOBSKILL_RECIPE const* pkDefJobSkillRecipe = NULL;
	g_kTblDataMgr.GetContDef(pkDefJobSkillRecipe);
	if( pkDefJobSkillRecipe )
	{
		CONT_DEF_JOBSKILL_RECIPE::const_iterator iter = pkDefJobSkillRecipe->find(iItemNo);
		if( iter != pkDefJobSkillRecipe->end() )
		{
			return (*iter).second.kProbability.iNo;
		}
	}
	return 0;
}

int JobSkill_Third::GetJobSkill3NeedProductPoint(int const iItemNo)
{
	CONT_DEF_JOBSKILL_RECIPE const* pkDefJobSkillRecipe = NULL;
	g_kTblDataMgr.GetContDef(pkDefJobSkillRecipe);
	if( pkDefJobSkillRecipe )
	{
		CONT_DEF_JOBSKILL_RECIPE::const_iterator iter = pkDefJobSkillRecipe->find(iItemNo);
		if( iter != pkDefJobSkillRecipe->end() )
		{
			return (*iter).second.iNeedProductPoint;
		}
	}
	return 0;
}

int JobSkill_Third::GetJobSkill3ExpertnessGain(int const iRecipeItemNo)
{
	CONT_DEF_JOBSKILL_RECIPE const* pkDefJobSkillRecipe = NULL;
	g_kTblDataMgr.GetContDef(pkDefJobSkillRecipe);
	if( pkDefJobSkillRecipe )
	{
		CONT_DEF_JOBSKILL_RECIPE::const_iterator iter = pkDefJobSkillRecipe->find(iRecipeItemNo);
		if( iter != pkDefJobSkillRecipe->end() )
		{
			return (*iter).second.iExpertnessGain;
		}
	}
	return 0;
}

HRESULT JobSkill_Third::CheckNeedSkill(PgPlayer * pkPlayer, int const iSaveIdx)
{
	if( !pkPlayer )
	{
		return E_FAIL;
	}

	CONT_DEF_JOBSKILL_SAVEIDX const* pkContDefSaveIdx = NULL;
	g_kTblDataMgr.GetContDef(pkContDefSaveIdx);
	if( !pkContDefSaveIdx )
	{
		return E_FAIL;
	}

	CONT_DEF_JOBSKILL_RECIPE const* pkDefJobSkillRecipe = NULL;
	g_kTblDataMgr.GetContDef(pkDefJobSkillRecipe);
	if( !pkDefJobSkillRecipe )
	{
		return E_FAIL;
	}

	CONT_DEF_JOBSKILL_SAVEIDX::const_iterator save_it = pkContDefSaveIdx->find(iSaveIdx);
	if( save_it == pkContDefSaveIdx->end() )
	{
		return E_FAIL;
	}
	
	int const iNeedSkillNo = (*save_it).second.iNeedSkillNo01;

	if( false==pkPlayer->JobSkillSaveIdx().Get(iSaveIdx) )
	{
		return E_JS3_CREATEITEM_NOT_LEARN_NEEDSKILL;
	}

	if( false==JobSkill_LearnUtil::IsEnableUseJobSkill(pkPlayer, iNeedSkillNo) )
	{
		return E_JS3_CREATEITEM_NOT_LEARN_NEEDSKILL;
	}

	return S_OK;
}

HRESULT JobSkill_Third::CheckNeedProductPoint(PgPlayer * pkPlayer, int const iRecipeItemNo)
{
	if( !pkPlayer )
	{
		return E_FAIL;
	}

	PgJobSkillExpertness const &rkExpertness = pkPlayer->JobSkillExpertness();
	int const iMaxProductPoint = JobSkillExpertnessUtil::GetBiggestMaxExhaustion(rkExpertness.GetAllSkillExpertness(), JST_3RD_MAIN); // 3차 주 스킬만 생산게이지가 있다
	int const iNewProductPoint = rkExpertness.CurProductPoint()+GetJobSkill3NeedProductPoint(iRecipeItemNo);
	if(iNewProductPoint > iMaxProductPoint)
	{
		return E_JS3_CREATEITEM_NOT_ENOUGH_NEED_PRODUCTPOINT;
	}

	return S_OK;
}

bool JobSkill_Third::UseResItem(int const iDefResourceGroupNo, int const iDefGrade, int const iItemNo, int const iResourceGroupNo, int const iGrade)
{
	if(0==iDefGrade)
	{
		if(iDefResourceGroupNo==iItemNo)
		{
			return true;
		}
	}
	else
	{
		if( (iDefResourceGroupNo == iResourceGroupNo)
			&& (iDefGrade <= iGrade) )
		{
			return true;
		}
	}

	return false;
}

bool JobSkill_Third::GetContGroupItemNo(int const iGroupNo, int const iGrade, VEC_INT & kContItemNo)
{
	CONT_DEF_JOBSKILL_ITEM_UPGRADE const* pkDefJobSkill_ItemUpgrade = NULL;
	g_kTblDataMgr.GetContDef(pkDefJobSkill_ItemUpgrade);
	if(!pkDefJobSkill_ItemUpgrade)
	{
		return false;
	}

	CONT_DEF_JOBSKILL_ITEM_UPGRADE::const_iterator find_iter = pkDefJobSkill_ItemUpgrade->begin();
	while( pkDefJobSkill_ItemUpgrade->end() != find_iter)
	{
		if( (iGroupNo == (*find_iter).second.iResourceGroupNo)
		&& (iGrade <= (*find_iter).second.iGrade)
		&& (0 == (*find_iter).second.iUpgradeCount) )
		{
			kContItemNo.push_back((*find_iter).second.iItemNo);
		}
		++find_iter;
	}

	return false==kContItemNo.empty();
}

HRESULT JobSkill_Third::CheckNeedItems(PgInventory * const pkInv, int const iRecipeItemNo, CONT_JS3_RESITEM_INFO const& kContResItemInfo)
{
	if( !pkInv )
	{
		return E_FAIL;
	}
	CONT_DEF_JOBSKILL_RECIPE const* pkDefJobSkillRecipe = NULL;
	g_kTblDataMgr.GetContDef(pkDefJobSkillRecipe);
	if( !pkDefJobSkillRecipe )
	{
		return E_FAIL;
	}

	CONT_DEF_JOBSKILL_RECIPE::mapped_type kRecipe;
	if( false==GetJobSkill3_Recipe(iRecipeItemNo, kRecipe) )
	{
		return E_JS3_CREATEITEM_NOT_FOUND_ITEM;
	}

	typedef std::map<int,int> CONT_MAP;
	CONT_MAP kContHaveItem;
	CONT_JS3_RESITEM_INFO::const_iterator resitem_info = kContResItemInfo.begin();
	while(resitem_info != kContResItemInfo.end())
	{
		int const iItemNo = (*resitem_info).first;
		kContHaveItem.insert( std::make_pair(iItemNo, pkInv->GetTotalCount(iItemNo)) );
		++resitem_info;
	}

	CONT_DEF_JOBSKILL_ITEM_UPGRADE::mapped_type kItemUpgrade;
	resitem_info = kContResItemInfo.begin();
	while(resitem_info != kContResItemInfo.end())
	{
		int const iItemNo = (*resitem_info).first;
		int const iCount = (*resitem_info).second;

		JobSkill_Util::GetJobSkill_Item(iItemNo, kItemUpgrade);
		for(int i=0; i<MAX_JS3_RECIPE_RES; ++i)
		{
			if( kRecipe.kResource[i].iCount>0
			&&	UseResItem(kRecipe.kResource[i].iGroupNo, kRecipe.kResource[i].iGrade, iItemNo, kItemUpgrade.iResourceGroupNo, kItemUpgrade.iGrade) )
			{
				kContHaveItem[iItemNo] -= iCount;
				kRecipe.kResource[i].iCount -= iCount;
				break;
			}
		}

		if(kContHaveItem[iItemNo] < 0)
		{
			break;
		}

		++resitem_info;
	}
	
	int iCount = 0;
	for(int i=0; i<MAX_JS3_RECIPE_RES; ++i)
	{
		iCount += std::max<int>(kRecipe.kResource[i].iCount,0);
	}

	if(0 != iCount)
	{
		return E_JS3_CREATEITEM_NOT_ENOUGH_NEED_ITEM;
	}

	return S_OK;
}

bool JobSkill_Third::GetJobSkill3_Recipe(int const iItemNo, CONT_DEF_JOBSKILL_RECIPE::mapped_type & rkOutRecipe)
{
	CONT_DEF_JOBSKILL_RECIPE const* pkDefJobSkillRecipe = NULL;
	g_kTblDataMgr.GetContDef(pkDefJobSkillRecipe);
	if( pkDefJobSkillRecipe )
	{
		CONT_DEF_JOBSKILL_RECIPE::const_iterator iter = pkDefJobSkillRecipe->find(iItemNo);
		if( iter != pkDefJobSkillRecipe->end() )
		{
			rkOutRecipe = (*iter).second;
			return true;
		}
	}
	return false;
}

HRESULT JobSkill_Third::GetTotalProbability(int const iRecipeItemNo)
{
	int const iNo = GetJobSkill3ProbabilityNo(iRecipeItemNo);

	CONT_DEF_JOBSKILL_PROBABILITY_BAG const* pkJobskillProbabilityBag = NULL;
	g_kTblDataMgr.GetContDef( pkJobskillProbabilityBag );
	if( pkJobskillProbabilityBag )
	{
		CONT_DEF_JOBSKILL_PROBABILITY_BAG::const_iterator bag_it = pkJobskillProbabilityBag->find(iNo);
		if( bag_it != pkJobskillProbabilityBag->end() )
		{
			return (*bag_it).second.iTotalProbability;
		}
	}
	return 0;
}

HRESULT JobSkill_Third::GetResProbabilityItem(PgInventory *const pkInv, int const iItemNo, CONT_JS3_RESITEM_INFO const& kContResItemInfo, JobSkill_Third::CONT_JS3_RESULT_ITEM & rkContItem, bool & rbRemainResultItem)
{
	if( !pkInv )
	{
		return E_FAIL;
	}
	int iResourceProbabilityUp = GetResourceProbabilityUp(pkInv, kContResItemInfo);
	return GetResProbabilityItem(iItemNo, iResourceProbabilityUp, rkContItem, rbRemainResultItem);
}

HRESULT JobSkill_Third::GetResProbabilityItem(int const iItemNo, int const iResourceProbabilityUp, JobSkill_Third::CONT_JS3_RESULT_ITEM & rkContItem, bool & rbRemainResultItem)
{
	int iTempResourceProbabilityUp = iResourceProbabilityUp;
	int const iNo = GetJobSkill3ProbabilityNo(iItemNo);
	int const iTotalProbability = GetTotalProbability(iItemNo);

	CONT_DEF_JOBSKILL_PROBABILITY_BAG const* pkJobskillProbabilityBag = NULL;
	g_kTblDataMgr.GetContDef( pkJobskillProbabilityBag );
	if( pkJobskillProbabilityBag )
	{
		CONT_DEF_JOBSKILL_PROBABILITY_BAG::const_iterator bag_it = pkJobskillProbabilityBag->find(iNo);
		if( bag_it != pkJobskillProbabilityBag->end() )
		{
			CONT_DEF_JOBSKILL_PROBABILITY_BAG::mapped_type const& rkDefProbBag = (*bag_it).second;
			CONT_PROBABILITY::const_iterator c_it = rkDefProbBag.kContProbability.begin();
			while(c_it != rkDefProbBag.kContProbability.end())
			{
				rkContItem.push_back( std::make_pair((*c_it).iProbability,(*c_it)) );
				++c_it;
			}
		}
	}

	//재료 확률값 적용
	JobSkill_Third::CONT_JS3_RESULT_ITEM::value_type kValue;
	for(JobSkill_Third::CONT_JS3_RESULT_ITEM::iterator it=rkContItem.begin(); it!=rkContItem.end(); /*No ++it*/)
	{
		int iRate = (*it).first - iTempResourceProbabilityUp;
		iTempResourceProbabilityUp -= (*it).first;
		if(iRate > 0)
		{
			(*it).first = iRate;
			break;
		}
		else
		{
			kValue = (*it);
			it = rkContItem.erase(it);
		}
	}

	if(rkContItem.empty() && kValue.second.IsRight())
	{
		rkContItem.push_back(kValue);
	}

	//10000에 맞추기 : 넘는 값 제거
	int iTotalRate = 0;
	for(JobSkill_Third::CONT_JS3_RESULT_ITEM::iterator it=rkContItem.begin(); it!=rkContItem.end(); ++it)
	{
		iTotalRate += (*it).first;
		int const iOver = iTotalProbability-iTotalRate;
		if(iOver < 0)
		{
			(*it).first += iOver;
			
			//10000을 넘은 이후 목표아이템 제거
			if((*it).first != 0)
			{
				++it;
			}
			rbRemainResultItem = (it != rkContItem.end());
			rkContItem.erase(it,rkContItem.end());
			break;
		}
	}
	
	//10000에 맞추기 : 모자라는 값 채우기
	int const iIdx = rkContItem.size()-1;
	int const iMod = iTotalProbability-iTotalRate;
	if(iIdx>=0 && iMod>0)
	{
		rkContItem.at(iIdx).first += iMod;
	}

	return S_OK;
}