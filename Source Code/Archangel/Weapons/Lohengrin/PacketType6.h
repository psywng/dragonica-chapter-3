#ifndef WEAPON_LOHENGRIN_PACKET_PACKETTYPE6_H
#define WEAPON_LOHENGRIN_PACKET_PACKETTYPE6_H

unsigned short const PACKET_TYPE6_BASE = 60000;
unsigned short const PACKET_TYPE6_MAX = 65000;




// RESERVE : 60001 - 61999
//	Subject -> Ground 에 보내는 패킷
unsigned short const PT_UNIT_NOTIFY_BASE = PACKET_TYPE6_BASE;//유닛이 이벤트 넘겨주는것//패킷으로 쓰지마라.
PACKET_DEF(PT_UNIT_NFY_ON_DIE,		PT_UNIT_NOTIFY_BASE+1 )	
PACKET_DEF(PT_U_G_BROADCAST_AREA,	PT_UNIT_NOTIFY_BASE+2 )		// Area Broadcast packet
PACKET_DEF(PT_U_G_NFY_ADD_EFFECT,	PT_UNIT_NOTIFY_BASE+3 )		// Added Effect
PACKET_DEF(PT_U_G_NFY_DELETE_EFFECT,	PT_UNIT_NOTIFY_BASE+4 )		// Added Effect
PACKET_DEF(PT_P_G_SEND_PACKET,				PT_UNIT_NOTIFY_BASE+5 )	// CUnit::Send(..)
PACKET_DEF(PT_U_G_RUN_ACTION,					PT_UNIT_NOTIFY_BASE+6 )//Unit 에서 Ground로 보내는 엑션 패킷
PACKET_DEF(PT_U_G_ABIL_CHANGE,					PT_UNIT_NOTIFY_BASE+7 )		// 어빌 수정
PACKET_DEF(PT_U_G_ABIL64_CHANGE,				PT_UNIT_NOTIFY_BASE+8 )		
PACKET_DEF(PT_U_G_NFY_EFFECT_ON_TIMER,			PT_UNIT_NOTIFY_BASE+9 )
PACKET_DEF(PT_U_G_NFY_CHANGED_INVEN,			PT_UNIT_NOTIFY_BASE+10)
PACKET_DEF(PT_U_G_NFY_ABIL_CHANGE,				PT_UNIT_NOTIFY_BASE+11)
PACKET_DEF(PT_U_G_NFY_EXP_SHARE_PARTY_MEMBER,	PT_UNIT_NOTIFY_BASE+12)
PACKET_DEF(PT_U_G_NFY_ADD_EXP,					PT_UNIT_NOTIFY_BASE+13)
PACKET_DEF(PT_U_G_NFY_GOLD_SHARE_PARTY_MEMBER,	PT_UNIT_NOTIFY_BASE+14)
PACKET_DEF(PT_U_G_BROADCAST_GROUND,				PT_UNIT_NOTIFY_BASE+15)	// Broadcase For Ground
PACKET_DEF(PT_U_G_NFY_ADD_AGGRO_METER,			PT_UNIT_NOTIFY_BASE+16)
PACKET_DEF(PT_U_G_REQ_MAP_MOVE,					PT_UNIT_NOTIFY_BASE+17)
PACKET_DEF(PT_U_G_SEND_TO_USERS,				PT_UNIT_NOTIFY_BASE+18)
PACKET_DEF(PT_U_G_NFY_ADD_KILLCOUNTER,			PT_UNIT_NOTIFY_BASE+19)
PACKET_DEF(PT_U_G_NFY_UPDATE_QUEUE_EFFECT,		PT_UNIT_NOTIFY_BASE+20)
PACKET_DEF(PT_U_G_NFY_SYNCTYPE_CHANGE,			PT_UNIT_NOTIFY_BASE+21)
PACKET_DEF(PT_U_G_NFY_ADD_MISSION_SCORE,		PT_UNIT_NOTIFY_BASE+22)
PACKET_DEF(PT_U_G_NFY_QUESTREWARD_ADDSKILL,		PT_UNIT_NOTIFY_BASE+23)
PACKET_DEF(PT_U_G_NFY_MARRY_MONEY,				PT_UNIT_NOTIFY_BASE+24)
PACKET_DEF(PT_U_G_NFY_SEND_CALLER,				PT_UNIT_NOTIFY_BASE+25)
PACKET_DEF(PT_U_G_NFY_PT_M_T_USEITEM,			PT_UNIT_NOTIFY_BASE+26)
PACKET_DEF(PT_U_G_NFY_SHARE_HP_SET,				PT_UNIT_NOTIFY_BASE+27)
PACKET_DEF(PT_U_G_NFY_ATTACKED_MONSTER,			PT_UNIT_NOTIFY_BASE+28)

unsigned short const PT_EFFECT_NOTIFY_BASE = 61000;//이펙트가 유닛으로 넘겨줌.
PACKET_DEF(PT_E_U_ADD_EFFECT,		PT_EFFECT_NOTIFY_BASE+1 )
PACKET_DEF(PT_E_U_MODIFY_EFFECT,	PT_EFFECT_NOTIFY_BASE+2 )
PACKET_DEF(PT_E_U_DELETE_EFFECT,	PT_EFFECT_NOTIFY_BASE+3 )
PACKET_DEF(PT_E_U_ON_TIMER,			PT_EFFECT_NOTIFY_BASE+4 )

unsigned short const PT_MACHINECONTROL_BASE = 61000;
//_MCTRL_ 서버 매니지먼트 패킷
//_TER_ Terminal 터미널
//_MMS_ Middle Management Server
//_SMS_ Sub Menagement Server
//_GT_ Game Center
//PACKET_DEF(PT_MCTRL_X_X_PACKETDEF, PT_MACHINECONTROL_BASE+1);
typedef enum :short
{
	MCT_REFRESH_CFG		= 1,
	MCT_REFRESH_LIST	= 2,
	MCT_REFRESH_STATE	,
	MCT_SERVER_ON		,
	MCT_SERVER_OFF		,
	MCT_SERVER_SHUTDOWN ,
	MCT_RE_SYNC ,
	MCT_CHANGE_MAXUSER,
	MCT_SMC_SYNC,
	MCT_ERR_CMD,

	MCT_GET_LOG_FILE_INFO,
	MCT_MMC_ANS_LOG_FILE_INFO,
	MCT_SMC_ANS_LOG_FILE_INFO,

	MCT_REQ_GET_FILE,
	MCT_MMC_ANS_GET_FILE,
	MCT_SMC_ANS_GET_FILE,

	MCT_REFRESH_LOG,

	MCT_REQ_NOTICE,
}EMMC_CMD_TYPE;

PACKET_DEF(PT_REQ_TOOL_MON_CMD, PT_MACHINECONTROL_BASE+1);			//커멘드	EMMC_CMD_TYPE 사용
PACKET_DEF(PT_ANS_MON_TOOL_CMD, PT_MACHINECONTROL_BASE+2);			//커멘드	EMMC_CMD_TYPE 사용

PACKET_DEF(PT_ANS_MMC_TOOL_CMD_RESULT, PT_MACHINECONTROL_BASE+3);			//커멘드	EMMC_CMD_TYPE 사용

PACKET_DEF(PT_MMC_SMC_NFY_CMD, PT_MACHINECONTROL_BASE+9);
PACKET_DEF(PT_SMC_MMC_ANS_CMD, PT_MACHINECONTROL_BASE+10);
PACKET_DEF(PT_SMC_MMC_REFRESH_STATE, PT_MACHINECONTROL_BASE+11);
PACKET_DEF(PT_MMC_TOOL_NFY_INFO, PT_MACHINECONTROL_BASE+12);
PACKET_DEF(PT_SMC_MMC_REQ_CHECK_SMC_VERSION, PT_MACHINECONTROL_BASE + 13);	//SMC 버전 체크
PACKET_DEF(PT_SMC_MMC_ANS_CHECK_SMC_VERSION, PT_MACHINECONTROL_BASE + 14);

PACKET_DEF( PT_SMC_MMC_REQ_DATA_SYNC_INFO,			PT_MACHINECONTROL_BASE+15 )
PACKET_DEF( PT_MMC_SMC_ANS_DATA_SYNC_INFO,			PT_MACHINECONTROL_BASE+16 )//싱크 정보 관련

PACKET_DEF( PT_SMC_MMC_REQ_GET_FILE,			PT_MACHINECONTROL_BASE+17 )
PACKET_DEF( PT_MMC_SMC_ANS_GET_FILE,			PT_MACHINECONTROL_BASE+18 )//파일 보내는거.

PACKET_DEF( PT_SMC_MMC_ANS_DATA_SYNC_END,			PT_MACHINECONTROL_BASE+20 )//싱크 끝.

PACKET_DEF( PT_MMC_SMC_GET_LOG_FILE_INFO,		PT_MACHINECONTROL_BASE+21 )
PACKET_DEF( PT_SMC_MMC_ANS_LOG_FILE_INFO,		PT_MACHINECONTROL_BASE+22 )

PACKET_DEF(PT_MCTRL_MMS_SMS_REQ_REFRESH_HANDLE, PT_MACHINECONTROL_BASE+25);
PACKET_DEF(PT_MCTRL_MMS_SMS_ANS_REFRESH_HANDLE, PT_MACHINECONTROL_BASE+26);

PACKET_DEF(PT_MCTRL_MMC_A_NFY_SERVER_COMMAND, PT_MACHINECONTROL_BASE+22);	//서버 관리 커맨드 MMS->Server
PACKET_DEF(PT_MCTRL_A_MMC_ANS_SERVER_COMMAND, PT_MACHINECONTROL_BASE+23);	//서버 관리 응답   Server->MMS

PACKET_DEF(PT_MCTRL_MMC_SMC_NFY_PROCESSID,	PT_MACHINECONTROL_BASE+24);
PACKET_DEF(PT_MCTRL_MMC_A_NFY_NOTICE,	PT_MACHINECONTROL_BASE+25);


PACKET_DEF(PT_MMC_MCT_LOG_FILE_INFO,	PT_MACHINECONTROL_BASE+27);
PACKET_DEF(PT_SMC_MCT_LOG_FILE_INFO,	PT_MACHINECONTROL_BASE+28);
PACKET_DEF(PT_MMC_MCT_ANS_GET_FILE,	PT_MACHINECONTROL_BASE+29);
PACKET_DEF(PT_SMC_MCT_ANS_GET_FILE,	PT_MACHINECONTROL_BASE+30);

// MMC -> IMM -> CONSENT 
PACKET_DEF(PT_MMC_CONSENT_NFY_INFO, PT_MACHINECONTROL_BASE+12);


unsigned short const PT_GAMEMASTER_BASE = 62000;//GM 서버,

PACKET_DEF(PT_IMM_LOGIN_PATCH_VERSION_EDIT,	PT_GAMEMASTER_BASE+1);			// GAME PATCH VERSION EDIT 
PACKET_DEF(PT_A_SEND_NOTICE, PT_GAMEMASTER_BASE+2);							// 공지 사용 패킷


unsigned short const PT_LOGSERVER_BASE = 63000;//LOG 서버,
PACKET_DEF(PT_A_WRITE_SIMPLE_LOG,	PT_LOGSERVER_BASE+2);
PACKET_DEF(PT_A_HEARTBEAT_LOG,		PT_LOGSERVER_BASE+3);			// 허트비트
PACKET_DEF(PT_A_SEND_LOGMODE,		PT_LOGSERVER_BASE+4);			// 로그모드
PACKET_DEF(PT_A_GAME_LOG,			PT_LOGSERVER_BASE+5);


typedef enum
	:BYTE
{
	IMCT_STATE_NFY = 1,//세션 변동
//	IMCT_SESSION = 2,
}E_IMM_MCC_CMD_TYPE;

PACKET_DEF(PT_IMM_MCC_NFY,			PT_LOGSERVER_BASE+6 );


unsigned short const PT_LINKAGE_BASE = 64000;	// 해외 연동 패킷 타입
PACKET_DEF( PT_GF_AP_REQ_OVERLAP_ACCOUNT,		PT_LINKAGE_BASE+1 )	// 0xFA01
PACKET_DEF( PT_AP_GF_ANS_OVERLAP_ACCOUNT,		PT_LINKAGE_BASE+2 )	// 0xFA02
PACKET_DEF( PT_GF_AP_REQ_CREATE_ACCOUNT,		PT_LINKAGE_BASE+3 )	// 0xFA03
PACKET_DEF( PT_AP_GF_ANS_CREATE_ACCOUNT,		PT_LINKAGE_BASE+4 )	// 0xFA04
PACKET_DEF( PT_GF_AP_REQ_CHECK_ACCOUNT,			PT_LINKAGE_BASE+5 )	// 0xFA05
PACKET_DEF( PT_AP_GF_ANS_CHECK_ACCOUNT,			PT_LINKAGE_BASE+6 )	// 0xFA06
PACKET_DEF( PT_GF_AP_REQ_GET_GAMEPOINT,			PT_LINKAGE_BASE+7 )	// 0xFA07
PACKET_DEF( PT_AP_GF_ANS_GET_GAMEPOINT,			PT_LINKAGE_BASE+8 )	// 0xFA08
PACKET_DEF( PT_GF_AP_REQ_ADD_GAMEPOINT,			PT_LINKAGE_BASE+9 )	// 0xFA09
PACKET_DEF( PT_AP_GF_ANS_ADD_GAMEPOINT,			PT_LINKAGE_BASE+10 )// 0xFA0A
PACKET_DEF( PT_GF_AP_REQ_MODIFY_PASSWORD,		PT_LINKAGE_BASE+11 )// 0xFA0B
PACKET_DEF( PT_AP_GF_ANS_MODIFY_PASSWORD,		PT_LINKAGE_BASE+12 )// 0xFA0C
PACKET_DEF( PT_GF_AP_REQ_USE_COUPON,			PT_LINKAGE_BASE+13 )// 0xFA0D
PACKET_DEF( PT_AP_GF_ANS_USE_COUPON,			PT_LINKAGE_BASE+14 )// 0xFA0E
PACKET_DEF( PT_GF_AP_REQ_MODIFY_MOBILELOCK,		PT_LINKAGE_BASE+15 )// 0xFA0F
PACKET_DEF( PT_AP_GF_ANS_MODIFY_MOBILELOCK,		PT_LINKAGE_BASE+16 )// 0xFA10
PACKET_DEF( PT_GF_AP_REQ_SERVER_LOGIN,			PT_LINKAGE_BASE+17 )// 0xFA11
PACKET_DEF( PT_AP_GF_ANS_SERVER_LOGIN_SUCCESS,	PT_LINKAGE_BASE+18 )// 0xFA12
PACKET_DEF( PT_GF_AP_REQ_SET_TABLEDATA,			PT_LINKAGE_BASE+19 )// 0xFA13
PACKET_DEF( PT_AP_GF_ANS_SET_TABLEDATA,			PT_LINKAGE_BASE+20 )// 0xFA14

PACKET_DEF( PT_AP_IM_REQ_ORDER,					PT_LINKAGE_BASE+101 )
PACKET_DEF( PT_IM_AP_ANS_ORDER,					PT_LINKAGE_BASE+102 )

#endif // WEAPON_LOHENGRIN_PACKET_PACKETTYPE6_H