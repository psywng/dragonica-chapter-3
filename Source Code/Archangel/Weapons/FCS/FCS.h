#pragma once

#include "Common.h"

#ifdef _MD_
	#pragma comment (lib, "FCS_MD.lib" )
#endif

#ifdef _MDd_
	#pragma comment (lib, "FCS_MDd.lib" )
#endif

#ifdef _MDo_
	#pragma comment (lib, "FCS_MDo.lib" )
#endif

#ifdef _MT_
	#pragma comment (lib, "FCS_MT.lib" )
#endif

#ifdef _MTd_
	#pragma comment (lib, "FCS_MTd.lib" )
#endif

#ifdef _MTo_
	#pragma comment (lib, "FCS_MTo.lib" )
#endif
