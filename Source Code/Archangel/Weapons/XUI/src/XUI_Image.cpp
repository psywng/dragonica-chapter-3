#include "stdafx.h"
#include "XUI_Image.h"

using namespace XUI;


CXUI_Image::CXUI_Image(void)
{
}

CXUI_Image::~CXUI_Image(void)
{
}

void CXUI_Image::VInit()
{
#ifdef XUI_USE_GENERAL_OFFSCREEN
	if (NotUseOffscreen())
		UseOffscreen(false);
	else
		UseOffscreen(true);
#endif
	CXUI_Wnd::VInit();
}