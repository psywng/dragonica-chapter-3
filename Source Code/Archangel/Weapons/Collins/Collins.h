#pragma once

#include "Common.h"

#ifdef _MD_
	#pragma comment (lib, "Collins_MD.lib" )
#endif

#ifdef _MDd_
	#pragma comment (lib, "Collins_MDd.lib" )
#endif

#ifdef _MDo_
	#pragma comment (lib, "Collins_MDo.lib" )
#endif

#ifdef _MT_
	#pragma comment (lib, "Collins_MT.lib" )
#endif

#ifdef _MTd_
	#pragma comment (lib, "Collins_MTd.lib" )
#endif

#ifdef _MTo_
	#pragma comment (lib, "Collins_MTo.lib" )
#endif
