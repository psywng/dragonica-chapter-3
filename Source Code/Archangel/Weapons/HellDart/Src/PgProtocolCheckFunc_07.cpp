#include "stdafx.h"
#include "BM/Packet.h"
#include "Lohengrin/Packetstruct.h"
#include "Variant/Item.h"

// Pet

extern HRESULT CALLBACK Check_PT_C_M_REQ_ITEM_ACTION( BM::CPacket & );

HRESULT CALLBACK Check_PT_C_M_REQ_PET_ACTION(BM::CPacket &kPacket)
{
	return Check_PT_C_M_REQ_ITEM_ACTION( kPacket );
}

HRESULT CALLBACK Check_PT_C_M_REQ_PET_RENAME(BM::CPacket &kPacket)
{
	SItemPos kItemPos;
	if ( true == kPacket.Pop( kItemPos ) )
	{
		std::wstring wstrPetName;
		if ( true == kPacket.Pop( wstrPetName, PgItem_PetInfo::MAX_PET_NAMELEN ) )
		{
			size_t const iRemainSize = kPacket.RemainSize();
			if (	!iRemainSize
				||	sizeof(SItemPos) == iRemainSize )
			{
				return S_OK;
			}
		}
	}
	return E_FAIL;
}

HRESULT CALLBACK Check_PT_C_M_REQ_PET_COLOR_CHANGE(BM::CPacket &kPacket)
{
	SItemPos kItemPos;
	if ( true == kPacket.Pop( kItemPos ) )
	{
		char cColorIndex = 0;
		kPacket.Pop( cColorIndex );

		if ( 0 <= cColorIndex  )
		{
			size_t const iRemainSize = kPacket.RemainSize();
			if (	!iRemainSize
				||	sizeof(SItemPos) == iRemainSize )
			{
				return S_OK;
			}
		}
	}
	return E_FAIL;
}

HRESULT CALLBACK Check_PT_C_M_REQ_ITEM_CHANGE_TO_PET(BM::CPacket &kPacket)
{
	SItemPos kItemPos;
	if ( true == kPacket.Pop( kItemPos ) )
	{
		if ( true == kPacket.Pop( kItemPos ) )
		{
			DWORD dwClientTime;
			if ( true == kPacket.Pop( dwClientTime ) )
			{
				if ( sizeof(bool) == kPacket.RemainSize() )
				{
					return S_OK;
				}
			}
		}
	}
	return E_FAIL;
}
