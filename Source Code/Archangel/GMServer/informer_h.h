

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Sat Feb 03 12:52:24 2018
 */
/* Compiler settings for .\informer.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__


#ifndef __informer_h_h__
#define __informer_h_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

#ifndef __informer_INTERFACE_DEFINED__
#define __informer_INTERFACE_DEFINED__

/* interface informer */
/* [auto_handle][endpoint][unique][version][uuid] */ 

int KickPlayer( 
    /* [in] */ handle_t hBinding,
    /* [string][in] */ const wchar_t *szAccountID);

int ChangePassword( 
    /* [in] */ handle_t hBinding,
    /* [string][in] */ const wchar_t *szAccountID,
    /* [string][in] */ const wchar_t *szPassword);

int CreateAccount( 
    /* [in] */ handle_t hBinding,
    /* [string][in] */ const wchar_t *szAccountID,
    /* [string][in] */ const wchar_t *szPassword,
    /* [string][in] */ const wchar_t *szBirthday);

int FreezeAccount( 
    /* [in] */ handle_t hBinding,
    /* [string][in] */ const wchar_t *szAccountID,
    /* [in] */ int iNewState);

int AuxiliaryPassword( 
    /* [in] */ handle_t hBinding,
    /* [string][in] */ const wchar_t *szAccountID,
    /* [string][in] */ const wchar_t *szAuxiliaryPassword,
    /* [in] */ int iSetType);

int AddPoint( 
    /* [in] */ handle_t hBinding,
    /* [string][in] */ const wchar_t *szPayId,
    /* [string][in] */ const wchar_t *szAccountID,
    /* [in] */ const int iPoints);

int GiveItem( 
    /* [in] */ handle_t hBinding,
    /* [string][in] */ const wchar_t *szCharName,
    /* [in] */ int iItemID,
    /* [in] */ const int iCount);

int GetPoint( 
    /* [in] */ handle_t hBinding,
    /* [string][in] */ const wchar_t *szAccountID,
    /* [out] */ int *piReturnCode);

int ChangeBirthday( 
    /* [in] */ handle_t hBinding,
    /* [string][in] */ const wchar_t *szAccountID,
    /* [string][in] */ const wchar_t *szBirthday);



extern RPC_IF_HANDLE informer_v1_0_c_ifspec;
extern RPC_IF_HANDLE informer_v1_0_s_ifspec;
#endif /* __informer_INTERFACE_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


