#ifndef GM_GMSERVER_NETWORK_PGSENDWRAPPER_H
#define GM_GMSERVER_NETWORK_PGSENDWRAPPER_H

extern bool SetSendWrapper(const SERVER_IDENTITY &kRecvSI);

extern bool SendToServer(SERVER_IDENTITY const &kSI, BM::CPacket const &rkPacket);
extern bool SendToImmigration(BM::CPacket const &rkPacket);
extern bool SendToLog(BM::CPacket const &rkPacket);

#endif // GM_GMSERVER_NETWORK_PGSENDWRAPPER_H