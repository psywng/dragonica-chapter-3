#include "StdAfx.h"
#include "PgGMTask.h"
#include "PgGMUserMgr.h"
#include "PgMsgHolder.h"
#include "PgPetitionDataMgr.h"
#include "PgGmChatMgr.h"
#include "PgChnRpcModule.h"
#include "PgNcGmCommand.h"
#include "PgGMTask.h"
#include "PgSendWrapper.h"
#include "Variant/PgNoticeAction.h"
#include "Variant/PgEventView.h"

tagGMEventOrder::tagGMEventOrder(PgMsgQue *pkMsgQue)
{
	m_pkMsgQue = pkMsgQue;
	m_kMsgGuid = pkMsgQue->MsgGuid();
}

void tagGMEventOrder::Clear()
{
}

PgGMTask::PgGMTask(void)
{
	if(g_kLocal.IsServiceRegion(LOCAL_MGR::NC_CHINA))
	{
		m_pkLocalProcess = new PgChnRpcModule;	
	}
	else if(g_kLocal.IsServiceRegion(LOCAL_MGR::NC_KOREA))
	{
		m_pkLocalProcess = new PgNcGmCommand;
	}
}

PgGMTask::~PgGMTask(void)
{
	if (g_kLocal.IsServiceRegion(LOCAL_MGR::NC_CHINA))
	{
		delete m_pkLocalProcess;
	} 
	else if(g_kLocal.IsServiceRegion(LOCAL_MGR::NC_KOREA))
	{
		delete m_pkLocalProcess;
	}
}

//WebGmTool
HRESULT PgGMTask::SelectGmOrderData()
{
//	CEL::DB_QUERY kQuery(DT_GM, DQT_SELECT_GM_ORDER_DATA, L"EXEC [dbo].[UP_Select_Gm_Order_Data] ?, ?" );	
//	kQuery.PushParam(kElement.nRealm);	
//	kQuery.PushParam(kElement.nChannel);	
	CEL::DB_QUERY kQuery(DT_GM, DQT_SELECT_GM_ORDER_DATA, L"EXEC [dbo].[UP_Select_Gm_Order_Data]" );	
	g_kCoreCenter.PushQuery(kQuery, true);
	return S_OK;
}


HRESULT PgGMTask::ChangeOrderState(BM::GUID const &CmdGUID, int const State)
{
//	INFO_LOG(BM::LOG_LV6, _T("[%s] Change Oder Query State State[%d]"), __FUNCTIONW__, State);
//	CEL::DB_QUERY kQuery(DT_GM, DQT_CHANGE_ORDER_DATA_STATE, L"EXEC [dbo].[UP_Change_Order_State] ?, ?" );
//	kQuery.PushParam(CmdGUID);
//	kQuery.PushParam(State);

	CEL::DB_QUERY kQuery(DT_GM, DQT_CHANGE_ORDER_DATA_STATE, L"EXEC [dbo].[UP_Change_Order_State]" );
	kQuery.PushStrParam(CmdGUID);
	kQuery.PushStrParam(State);
	return g_kCoreCenter.PushQuery(kQuery, true);
}

HRESULT PgGMTask::FailedOrder(BM::GUID const &CmdGUID, int const iErrorCode)
{
	CEL::DB_QUERY kQuery(DT_GM, DQT_FAILED_ORDER, L"EXEC [dbo].[UP_Failed_Order]" );
	kQuery.PushStrParam(CmdGUID);
	kQuery.PushStrParam(iErrorCode);
	return g_kCoreCenter.PushQuery(kQuery, true);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void PgGMTask::RecvGMToolProcess(CEL::CSession_Base * const pkSession, BM::CPacket * const pkPacket)
{
	EGMCommandType eCommand;
	BM::GUID kReqGuid;
	pkPacket->Pop(eCommand);

	switch (eCommand)
	{
	case EGMC_MATCH_GMID:
		{
			g_kGMUserMgr.WaitLogin(pkPacket);
		}break;
	case EGMC_GET_SERVER_LIST:
		{
			BM::CPacket kPacket(PT_G_O_ANS_GMCOMMAND, eCommand);
			g_kProcessCfg.Locked_Write_ServerList(kPacket, 0);
		}break;
	case EGMC_GET_GMLIST:
		{
			pkPacket->Pop(kReqGuid);
			BM::CPacket kPacket(PT_G_O_ANS_GMCOMMAND);
			kPacket.Push(kReqGuid);
			kPacket.Push(eCommand);
			g_kGMUserMgr.WriteToPacketGmUser(kPacket);
			g_kGMUserMgr.SendToLogin(kReqGuid, kPacket);
		}break;
	case EGMC_DELETE_GM:
		{
			g_kGMUserMgr.DeleteLoginedGM(pkPacket);			
		}break;
	case EGMC_NOTICE:
		{
			unsigned short usRealm;
			unsigned short usChannel;
			std::wstring wstrContent;

			pkPacket->Pop(usRealm);
			pkPacket->Pop(usChannel);
			pkPacket->Pop(wstrContent);

			PgNoticeAction kNotice;
			kNotice.SetNoticeData(1, usRealm, usChannel, 0, wstrContent);
			kNotice.Send(CEL::ST_IMMIGRATION);
		}break;
	case EGMC_GET_PETITIONDATA:
		{
			unsigned short usRealm = 0;
			BM::GUID kGmGuid;
			pkPacket->Pop(kGmGuid);
			pkPacket->Pop(usRealm);
			g_kPetitionDataMgr.SendPetitionDataToUser(kGmGuid, usRealm);
		}break;
	case EGMC_MODIFY_PETITIONDATA:
		{
			BM::GUID kReqGuid;
			pkPacket->Pop(kReqGuid);
			SPetitionData kModifyData;
			kModifyData.ReadFromPacket(*pkPacket);
			g_kPetitionDataMgr.UpdatePetitionData(kReqGuid, kModifyData);
		}break;
	case EGMC_SENDMSG_TO_GM:
		{
			// GM툴로 보내주자.
			BM::CPacket kPacket(PT_G_O_ANS_GMCOMMAND, eCommand);
			kPacket.Push(*pkPacket);

			//로그 저장
			std::wstring wstrMsg;
			std::wstring wstrGmName;
			pkPacket->Pop(wstrGmName);
			pkPacket->Pop(wstrMsg);
			wstrGmName += L":" + wstrMsg;
			g_kGmChatMgr.WriteChatMsg(wstrGmName);
		}break;
	default:
		{
			INFO_LOG( BM::LOG_LV0, __FL__ << _T("Unknown GMToolCommand received CommandType[") << eCommand << _T("]") );
		}break;
	}
}

void PgGMTask::RecvGMLocaleProcess(BM::CPacket * const pkPacket)
{
	if(m_pkLocalProcess)
	{
		m_pkLocalProcess->LocaleGMCommandProcess(pkPacket);
	}
}

void PgGMTask::GetNowTime(BM::DBTIMESTAMP_EX &rkOut)
{
	SYSTEMTIME kNowTime;
	g_kEventView.GetLocalTime(&kNowTime);
	rkOut = kNowTime;
}

HRESULT PgGMWorker::VProcess(SGMEventOrder *pkMsg)
{
	bool bProcessRet = false;
	
	SGMEventOrder::CONT_WORKDATA_TARGET::const_iterator target_itor = pkMsg->m_kContTarget.begin();
	if(target_itor == pkMsg->m_kContTarget.end())
	{//이럴 수가 없기 때문.
		__asm int 3;
		return E_FAIL;
	}

	SGMEventOrder::CONT_WORKDATA_TARGET::value_type const &kStrAccountID = (*target_itor);

	BM::GUID const kMsgGuid = pkMsg->MsgGuid();
	
	switch(pkMsg->ePriMsgType)
	{
	case GOET_KICK_USER:
		{
			bProcessRet = m_pkLocalProcess->ReqKickUser(kMsgGuid, kStrAccountID.c_str());
		}break;
	case GOET_CHANGE_PW:
		{
			std::wstring kStrPW;
			pkMsg->Pop(kStrPW);	
			
			bProcessRet = m_pkLocalProcess->ReqChangePassWord(kMsgGuid, kStrAccountID.c_str(), kStrPW.c_str(), 0);
		}break;
	case GOET_CREATE_ACCOUNT:
		{	
			std::wstring kStrPW;
			std::wstring kStrBirth;
			pkMsg->Pop(kStrPW);	
			pkMsg->Pop(kStrBirth);	
			
			bProcessRet = m_pkLocalProcess->ReqCreateAccount(kMsgGuid, kStrAccountID.c_str(), kStrPW.c_str(), kStrBirth.c_str());
		}break;
	case GOET_FREEZE_ACCOUNT:
		{
			int iNewState = 0;
			pkMsg->Pop(iNewState);	
			
			bProcessRet = m_pkLocalProcess->ReqFreezeAccount(kMsgGuid, kStrAccountID.c_str(), iNewState, 0);
		}break;
	case GOET_ADD_POINT:
		{
			int iPoint = 0;
			pkMsg->Pop(iPoint);	
			
			bProcessRet = m_pkLocalProcess->ReqAddPoint(kMsgGuid, kStrAccountID.c_str(), iPoint);
		}break;
	case GOET_GET_POINT:
		{
			bProcessRet = m_pkLocalProcess->ReqGetPoint(kMsgGuid, kStrAccountID.c_str());
		}break;
	case GOET_CHANGE_BIRTH:
		{
			std::wstring kStrBirth;
			pkMsg->Pop(kStrBirth);

			bProcessRet = m_pkLocalProcess->ReqChangeBirthday(kMsgGuid, kStrAccountID.c_str(), kStrBirth.c_str());
		}break;
	}

	if(!bProcessRet)
	{//에러 발생시 오류 전달
		//로그 남길 필요 있겠음.
		return m_pkLocalProcess->VPostErrorMsg(kMsgGuid, kStrAccountID, 0, GE_SYSTEM_ERR);
	}

	return S_OK;
}