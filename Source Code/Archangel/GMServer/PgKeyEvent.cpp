#include "stdafx.h"
#include "PgGMTask.h"
#include "informer_h.h"

bool CALLBACK OnEscape( unsigned short const &rkInputKey)
{
	std::cout<< __FUNCTIONW__ << std::endl;
	std::cout << "Press [F11] key to quit" << std::endl;
	return true;
}

bool CALLBACK OnF1( unsigned short const &rkInputKey)
{
	std::cout<< "F1" << std::endl;

	wchar_t const * szAccountID = _T("dtest1");
	wchar_t const * szPassword = _T("1");
	wchar_t const * szBirthday = _T("2008 12:34:56");
	
	KickPlayer(0, szAccountID);

//	ChangePassword(0, szAccountID, szPassword);
//	CreateAccount(0, szAccountID, szPassword, szBirthday);
//	FreezeAccount(0, szAccountID, 1);
//	AddPoint(0, NULL, szAccountID, 1000);
//	GetPoint(0, szAccountID, &iTemp);
//	ChangeBirthday(0, szAccountID, szBirthday);

	return true;
}

bool CALLBACK OnF2( unsigned short const &rkInputKey)
{
	std::cout<< __FUNCTION__ << std::endl;

	wchar_t const * szAccountID = _T("dtest1");
	wchar_t const * szPassword = _T("1");
	wchar_t const * szBirthday = _T("2008 12:34:56");
	
	ChangePassword(0, szAccountID, szPassword);

	return true;
}

bool CALLBACK OnF3( unsigned short const &rkInputKey)
{
	std::cout<< __FUNCTION__ << std::endl;

	wchar_t const * szAccountID = _T("zzw400");
	wchar_t const * szPassword = _T("1234");
	wchar_t const * szBirthday = _T("2008 12:34:56");
	
	CreateAccount(0, szAccountID, szPassword, szBirthday);
	return true;
}

bool CALLBACK OnF4( unsigned short const &rkInputKey)
{
	std::cout<< __FUNCTION__ << std::endl;

	wchar_t const * szAccountID = _T("dtest1");
	wchar_t const * szPassword = _T("1");
	wchar_t const * szBirthday = _T("2008 12:34:56");
	
	FreezeAccount(0, szAccountID, 1);
	return true;
}

bool CALLBACK OnF5( unsigned short const &rkInputKey)
{
	std::cout<< __FUNCTION__ << std::endl;

	wchar_t const * szAccountID = _T("dtest1");
	wchar_t const * szPassword = _T("1");
	wchar_t const * szBirthday = _T("2008 12:34:56");
	
	AddPoint(0, NULL, szAccountID, 1000);
	return true;
}

bool CALLBACK OnF6( unsigned short const &rkInputKey)
{
	std::cout<< __FUNCTION__ << std::endl;

	wchar_t const * szAccountID = _T("dtest1");
	wchar_t const * szPassword = _T("1");
	wchar_t const * szBirthday = _T("2008 12:34:56");
	
	int iTemp;
	GetPoint(0, szAccountID, &iTemp);
	return true;
}


bool CALLBACK OnTerminateServer(const WORD& rkInputKey)
{
	INFO_LOG( BM::LOG_LV6, _T("===========================================================") );
	INFO_LOG( BM::LOG_LV6, _T("[GMServer] will be shutdown") );
	INFO_LOG( BM::LOG_LV6, _T("\tIt takes some times depens on system....WAITING...") );
	INFO_LOG( BM::LOG_LV6, _T("===========================================================") );
	g_kConsoleCommander.StopSignal(true);
	INFO_LOG( BM::LOG_LV6, _T("=== Shutdonw END ====") );
	return false;
}

bool CALLBACK OnEnd(WORD const& rkInputKey)
{
	// Ctrl + Shift + END
	std::cout << __FUNCTION__ << " / Key:" << rkInputKey << std::endl;
	SHORT sState = GetKeyState(VK_SHIFT);
	if (HIBYTE(sState) == 0)
	{
		return false;
	}
	sState = GetKeyState(VK_CONTROL);
	if (HIBYTE(sState) == 0)
	{
		return false;
	}
	std::cout << "SUCCESS " << __FUNCTION__ << " / Key:" << rkInputKey << std::endl;

	if ( IDOK == ::MessageBoxA(NULL,"[WARNING] GM Server will be CRASHED.. are you sure??","DRAGONICA_GM",MB_OKCANCEL) )
	{
		if ( IDOK == ::MessageBoxA(NULL,"[WARNING][WARNING] GM Server will be CRASHED.. are you sure??","DRAGONICA_GM",MB_OKCANCEL) )
		{
			INFO_LOG( BM::LOG_LV0, __FL__ << _T("Server crashed by INPUT") );
			::RaiseException(1,  EXCEPTION_NONCONTINUABLE_EXCEPTION, 0, NULL);
		}
	}
	return false;
}

bool RegistKeyEvent()
{
	g_kConsoleCommander.Regist( VK_ESCAPE,	OnEscape );
	g_kConsoleCommander.Regist( VK_F1,	OnF1 );
	g_kConsoleCommander.Regist( VK_F2,	OnF2 );
	g_kConsoleCommander.Regist( VK_F3,	OnF3 );
	g_kConsoleCommander.Regist( VK_F4,	OnF4 );
	g_kConsoleCommander.Regist( VK_F5,  OnF5 );
	g_kConsoleCommander.Regist( VK_F6,  OnF6 );
	g_kConsoleCommander.Regist( VK_F11, OnTerminateServer );
	g_kConsoleCommander.Regist( VK_END,	OnEnd );
	return true;
}
