#include "StdAfx.h"
#include "Variant/Global.h"
#include "PgServerSetMgr.h"
#include "PgRecvFromItem.h"
#include "Variant/PgPlayer.h"
//#include "Variant/PgClassDefMgr.h"
//#include "PgGMProcessMgr.h"
#include "Lohengrin/PacketStruct4Map.h"
#include "Variant/PgMctrl.h"
#include "Variant/PgEventview.h"
#include "Variant/PgMission.h"
#include "Variant/gm_const.h"
#include "Lohengrin/packetstruct.h"
#include "Lohengrin/PgRealmManager.h"
#include "Variant/PgControlDefMgr.h"
#include "Global.h"
#include "PgLChannelMgr.h"


extern SERVER_IDENTITY g_kItemSI;

PgServerSetMgr::PgServerSetMgr(void)
:	m_bShutDownServer(false)
{
}

PgServerSetMgr::~PgServerSetMgr(void)
{
	Clear();
}

void PgServerSetMgr::Clear()
{
	CONT_SERVER_BALANCE::iterator bal_itr = m_kContServerBalance.begin();
	for ( ; bal_itr!=m_kContServerBalance.end() ; ++bal_itr )
	{
		SAFE_DELETE(*bal_itr);
	}
	m_kContServerBalance.clear();
}

bool PgServerSetMgr::Locked_SendToGround(BM::GUID const &kCharacterGuid, BM::CPacket const &rkPacket, bool const bIsGndWrap)const
{
	BM::CAutoMutex kLock(m_kMutex);

	return SendToGround(kCharacterGuid, rkPacket, bIsGndWrap);
}

bool PgServerSetMgr::Locked_SendToGround(SGroundKey const &kKey, BM::CPacket const &rkPacket, bool const bIsGndWrap)const
{
	BM::CAutoMutex kLock(m_kMutex);

	return SendToGround(kKey, rkPacket, bIsGndWrap);
}

bool PgServerSetMgr::SendToGround(BM::GUID const &kCharacterGuid, BM::CPacket const &rkPacket, bool const bIsGndWrap)const
{
	SContentsUser kConUser;
	if(S_OK != GetPlayerInfo(kCharacterGuid, false, kConUser))
	{
		return false;
	}
	return SendToGround( kConUser.kGndKey, rkPacket, bIsGndWrap );
}

bool PgServerSetMgr::SendToGround(SGroundKey const &kKey, BM::CPacket const &rkPacket, bool const bIsGndWrap)const
{
	SERVER_IDENTITY kSI;
	switch ( kKey.GroundNo() )
	{
	case PvP_Lobby_GroundNo_AnterRoom:
	case PvP_Lobby_GroundNo_Exercise:
	case PvP_Lobby_GroundNo_Ranking:
	case PvP_Lobby_GroundNo_League:
		{
			return SendToPvPLobby( rkPacket, kKey.GroundNo() );
		}break;
	default:
		{
			if( S_OK == GetServerIdentity(kKey, kSI) )
			{	
				if(bIsGndWrap)
				{
					BM::CPacket kPacket(PT_A_GND_WRAPPED_PACKET, kKey );
					kPacket.Push(rkPacket);
					SendToServer(kSI, kPacket);
				}
				else
				{
					BM::CPacket kPacket(PT_A_A_WRAPPED_PACKET, kKey );
					kPacket.Push(rkPacket);
					SendToServer(kSI, kPacket);
				}
				return true;
			}
		}break;
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgServerSetMgr::SendToSwitch( BM::GUID const &kMemberGuid, BM::CPacket const &rkPacket)const
{
	CONT_CENTER_PLAYER_BY_KEY::const_iterator itor = m_kContPlayer_MemberKey.find( kMemberGuid );
	if( m_kContPlayer_MemberKey.end() != itor )
	{
		return SendToServer( (*itor).second->GetSwitchServer(), rkPacket );
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

int Cmp_PlayerCount(void const *pOrg, void const *pTgt)
{//작은거 리턴.
	TBL_SERVERLIST const *pkOrgTbl = (TBL_SERVERLIST const *)pOrg;
	TBL_SERVERLIST const *pkTgtTbl = (TBL_SERVERLIST const *)pTgt;

	if(pkOrgTbl->nPlayerCount < pkTgtTbl->nPlayerCount)
	{
		return -1;
	}

	if(pkOrgTbl->nPlayerCount > pkTgtTbl->nPlayerCount)
	{
		return 1;
	}
	return 0;
}

void PgServerSetMgr::Recv_PT_S_T_ANS_RESERVE_MEMBER(BM::CPacket * const pkPacket)const
{
	ESwitchReservePlayerResult eRet = SRPR_NONE;
	BM::GUID kSwitchKeyGuid;
	BM::GUID kMemberGuid;
	CEL::ADDR_INFO kSwitchAddr;

	pkPacket->Pop(eRet);
	pkPacket->Pop(kMemberGuid);
	pkPacket->Pop(kSwitchKeyGuid);
	pkPacket->Pop(kSwitchAddr);
	switch(eRet)
	{
	case SRPR_SUCEESS:
		{//Success
			SSwitchPlayerData kSPD;
			kSPD.ReadFromPacket(*pkPacket);

			INFO_LOG( BM::LOG_LV6, __FL__ << _T("<08> User Assign Success From Switch ID[") << kSPD.ID() << _T("], MemberKey[") << kSPD.guidMember.str().c_str() << _T("]") );

			SSwitchReserveResult kSRR = kSPD;
			kSRR.eRet = SRPR_SUCEESS;
			kSRR.addrSwitch	= kSwitchAddr;
			kSRR.guidSwitchKey = kSPD.guidOrderKey;

			INFO_LOG( BM::LOG_LV6, __FL__ << _T("<09> Send to LoginServer, the Switch Information") );
			
			BM::CPacket kPacket(PT_T_IM_NFY_RESERVED_SWITCH_INFO);
			kSRR.WriteToPacket(kPacket);
			g_kProcessCfg.Locked_SendToServerType(CEL::ST_CONTENTS, kPacket);
		}break;
	default:
		{	//실패 결과 전송
			CAUTION_LOG( BM::LOG_LV4, __FL__ << _T("<08-2> User Assign Fail From Switch") );
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Invalid CaseType"));
	//이런일이???
	//		SSwitchReserveResult;
	//		kSRR.eRet = eRet;
	//		BM::CPacket kPacket(PT_T_IM_NFY_RESERVED_SWITCH_INFO, kSRR);
	//		SendToImmigration(kPacket);
		}break;
	}
	return;
}

void PgServerSetMgr::ProcessRemoveUser_Common(CONT_CENTER_PLAYER_BY_KEY::mapped_type pCPData, bool bNotify)
{	
	INFO_LOG( BM::LOG_LV7, __FL__ << _T("MemberGuid[") << pCPData->GetMemberGUID().str().c_str() << _T("], Name[")
		<< pCPData->Name().c_str() << _T("], bNotify[") << bNotify << _T("]")
		);
	if (bNotify)
	{
		SERVER_IDENTITY kSI = g_kProcessCfg.ServerIdentity();
		kSI.nServerNo = 0; kSI.nServerType = 0;
		BM::CPacket kNfyPacket(PT_A_NFY_USER_DISCONNECT);
		kNfyPacket.Push(pCPData->GetMemberGUID());
		kNfyPacket.Push(pCPData->GetID());
		kSI.WriteToPacket(kNfyPacket);
		SendToContents(kNfyPacket);
	}

	ProcessRemoveUser_NotifyOtherMgr( pCPData, true );
}

void PgServerSetMgr::ProcessRemoveUser_NotifyOtherMgr( CONT_CENTER_PLAYER_BY_KEY::mapped_type const &pCPData, bool const bParty )const
{
	//모든 Contents 클래스에 유저가 나감을 알리자

	// PvP Lobby
	if ( g_kProcessCfg.IsPublicChannel() )
	{
		BM::CPacket kNfyPacket(PT_A_NFY_USER_DISCONNECT);
		kNfyPacket.Push(pCPData->GetMemberGUID());
		kNfyPacket.Push(pCPData->GetID());
		SendToPvPLobby( kNfyPacket, 0 );
	}

	// Party Mgr
	BM::CPacket kWPacket(PT_A_NFY_USER_DISCONNECT);
	SContentsUser kUserInfo;
	pCPData->CopyTo(kUserInfo);
	kUserInfo.kGndKey.Clear(); // 로그아웃이니까 현재 위치는 클리어.
	kUserInfo.WriteToPacket(kWPacket);

	if ( true == bParty )
	{
		::SendToGlobalPartyMgr(kWPacket);
		kWPacket.PosAdjust();
	}
	else
	{
		if ( g_kProcessCfg.IsPublicChannel() )
		{
			BM::CPacket kPartyPacket( PT_T_T_SYNC_USER_PARTY2, pCPData->GetID() );
			::SendToGlobalPartyMgr( kPartyPacket );
		}
	}
}

bool PgServerSetMgr::Locked_ProcessRemoveUser( BM::GUID const &kMemberGuid, bool bNotifyContents, bool bNotifyMap )
{
	BM::CAutoMutex kLock(m_kMutex, true);

	CONT_CENTER_PLAYER_BY_KEY::iterator user_itor = m_kContPlayer_MemberKey.find(kMemberGuid);
	if( m_kContPlayer_MemberKey.end() != user_itor)
	{
		CONT_CENTER_PLAYER_BY_KEY::mapped_type pCPData = (*user_itor).second;//pData 는 pool 데이터이므로 user_itor 삭제해도 됨.

		if ( bNotifyMap )
		{
			BM::CPacket kPacket( PT_A_NFY_USER_DISCONNECT, kMemberGuid );
			kPacket.Push( pCPData->GetID() );
			::SendToServerType(CEL::ST_MAP, kPacket);
		}

		ProcessRemoveUser_Common( pCPData, bNotifyContents );
		RemovePlayer(pCPData);
		g_kTotalObjMgr.ReleaseUnit( pCPData );
		return true;
	}

	if (bNotifyContents)
	{
		// Public Channel 로 변경하는 중일 수도 있다.
		// Contents 에는 일단 통보를 하자
		SERVER_IDENTITY kSI = g_kProcessCfg.ServerIdentity();
		kSI.nServerNo = 0; kSI.nServerType = 0;
		BM::CPacket kNfyPacket(PT_A_NFY_USER_DISCONNECT);
		kNfyPacket.Push(kMemberGuid);
		kNfyPacket.Push(BM::GUID::NullData());
		kSI.WriteToPacket(kNfyPacket);
		SendToContents(kNfyPacket);
	}

	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgServerSetMgr::Locked_ProcessRemoveUser(SERVER_IDENTITY const &kSI)
{
	BM::CAutoMutex kLock(m_kMutex, true);
	switch(kSI.nServerType)
	{
	case CEL::ST_LOG:
	case CEL::ST_MAP:
	case CEL::ST_ITEM:
	case CEL::ST_MACHINE_CONTROL:
	case CEL::ST_CENTER:
		{
		}break;
	case CEL::ST_SWITCH:
		{
			ProcessRemoveUser_BySwitch(kSI);
		}break;
	default:
		{
			VERIFY_INFO_LOG( false, BM::LOG_LV1, __FL__ << _T("Unknown SeverType[") << C2L(kSI) << _T("]") );
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Invalid CaseType"));
		}break;
	}
	return true;
}

size_t PgServerSetMgr::ProcessRemoveUser_ByGround(SGroundKey const &kGndKey)
{
	size_t iRet = 0;
	CONT_CENTER_PLAYER_BY_KEY::iterator user_itor = m_kContPlayer_MemberKey.begin();
	while( m_kContPlayer_MemberKey.end() != user_itor )
	{
		CONT_CENTER_PLAYER_BY_KEY::mapped_type pCPData = (*user_itor).second;//pData 는 pool 데이터이므로 user_itor 삭제해도 됨.

		SGroundKey const &kNowGndKey = pCPData->GroundKey();
		std::wstring const &wstrID = pCPData->Name();

		if(kNowGndKey == kGndKey)
		{
			// 여기서 직접 지우면 로직이 많이 꼬이고,
			// 복잡해진다. 스위치한테 끊으라고 통보해도 잘 처리되니까 스위치부터 시작하도록 하자.
//			ProcessRemoveUser_Common(pCPData);
//			RemovePlayer(pCPData);
//			g_kTotalObjMgr.ReleaseUnit( pCPData );			
//			user_itor = m_kContPlayer_MemberKey.begin();			
//			continue;

			BM::CPacket kDPacket( PT_A_S_NFY_USER_DISCONNECT, static_cast<BYTE>(CDC_CenterRemoveUserByGnd) );
			kDPacket.Push( user_itor->first );
			SendToServer( pCPData->GetSwitchServer(), kDPacket );
			++iRet;
		}
		++user_itor;
	}
	return iRet;
}

void PgServerSetMgr::ProcessRemoveUser_BySwitch(SERVER_IDENTITY const &kSI)
{
	CONT_CENTER_PLAYER_BY_KEY::iterator user_itor = m_kContPlayer_MemberKey.begin();
	while( m_kContPlayer_MemberKey.end() != user_itor)
	{
		CONT_CENTER_PLAYER_BY_KEY::mapped_type pCPData = (*user_itor).second;//pData 는 pool 데이터이므로 user_itor 삭제해도 됨.

		SERVER_IDENTITY const &kSwitchServer = pCPData->GetSwitchServer();
//		std::wstring const &wstrID = pCPData->szID;

		if(kSI == kSwitchServer)
		{
			ProcessRemoveUser_Common(pCPData);
			RemovePlayer(pCPData);
			g_kTotalObjMgr.ReleaseUnit( pCPData );

			user_itor = m_kContPlayer_MemberKey.begin();//RemovePlayer 내부에서 이터레이터 깨짐.
			continue;
		}
		++user_itor;
	}
	return;
}

std::wstring PgServerSetMgr::LoginedPlayerName(BM::GUID const &rkMemberGuid, BM::GUID const &rkCharacterGuid)
{
	CONT_CENTER_PLAYER_BY_KEY::iterator itor = m_kContPlayer_MemberKey.find(rkMemberGuid);
	if( m_kContPlayer_MemberKey.end() != itor)
	{
		CONT_CENTER_PLAYER_BY_KEY::mapped_type pData = (*itor).second;
		if(!pData)
		{ 
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pData is NULL"));
			return std::wstring();
			//return NULL;
		}
	
		PgPlayer *pkPlayer = pData;	
		if(pkPlayer == NULL || pkPlayer->GetID() != rkCharacterGuid)
		{ 
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return Null String!"));
			return std::wstring();
			//return NULL;
		}
		//return wstrName = rElement.szName;
		return pkPlayer->Name();
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Not Find Data!"));
	return std::wstring(); 
	//return NULL;
}

bool PgServerSetMgr::UpdatePlayerData( UNIT_PTR_ARRAY& rkUnitArray, SPortalWaiter *pkPortalWaiter, bool const bClearGroundKey )const
{
	CONT_CENTER_PLAYER_BY_KEY::const_iterator login_itr;
	UNIT_PTR_ARRAY::iterator unit_itr = rkUnitArray.begin();
	while ( unit_itr!=rkUnitArray.end() )
	{
		switch (unit_itr->pkUnit->UnitType())
		{
		case UT_MYHOME:
			{
			}break;
		case UT_PLAYER:
			{
				PgPlayer *pkMapUser = dynamic_cast<PgPlayer*>(unit_itr->pkUnit);

				login_itr = m_kContPlayer_MemberKey.find( pkMapUser->GetMemberGUID() );
				if ( login_itr != m_kContPlayer_MemberKey.end() && (login_itr->second != NULL) )
				{
					PgPlayer *pkPlayer = login_itr->second;
					pkMapUser->CopyTo( *pkPlayer, rkUnitArray.GetWriteType() );
					
					if ( pkPortalWaiter )
					{
						pkPortalWaiter->Add( pkMapUser->GetID(), pkMapUser->VolatileID() );
						if ( pkPortalWaiter->iOwnerLv < 1)
						{
							pkPortalWaiter->iOwnerLv = pkMapUser->GetAbil(AT_LEVEL);
						}
					}

					if ( true == bClearGroundKey )
					{
						// GroundKey를 지운다.
						pkPlayer->GroundKey( SGroundKey() );
					}
				}
				else
				{
					unit_itr = rkUnitArray.erase(unit_itr);
					continue;
				}
			}break;
		}
		++unit_itr;
	}

	return !(rkUnitArray.empty());
}

bool PgServerSetMgr::Locked_TargetMapMoveByGuid( bool const bGMCommand, BM::GUID const &kCharGuid, BM::GUID const &kTargetGuid, bool const bMemberGuid )const
{
	BM::CAutoMutex kLock( m_kMutex );
	return ReqTargetMapMoveByGuid( bGMCommand, kCharGuid, kTargetGuid, bMemberGuid );
}

bool PgServerSetMgr::ReqTargetMapMoveByGuid( bool const bGMCommand, BM::GUID const &kCharGuid, BM::GUID const &kTargetGuid, bool const bMemberGuid )const
{
	CONT_CENTER_PLAYER_BY_KEY::mapped_type pkTargetPlayer = NULL;

	if ( bMemberGuid )
	{
		CONT_CENTER_PLAYER_BY_KEY::const_iterator itr = m_kContPlayer_MemberKey.find( kTargetGuid );
		if ( itr != m_kContPlayer_MemberKey.end() )
		{
			pkTargetPlayer = itr->second;
		}
	}
	else
	{
		CONT_CENTER_PLAYER_BY_KEY::const_iterator itr = m_kContPlayer_CharKey.find( kTargetGuid );
		if ( itr != m_kContPlayer_CharKey.end() )
		{
			pkTargetPlayer = itr->second;
		}
	}
	
	if ( !pkTargetPlayer )
	{
		return false;
	}

	int const iGroundNo = pkTargetPlayer->GroundKey().GroundNo();
	if ( PvP_Lobby_GroundNo_Max < iGroundNo )
	{
		BM::CPacket kReqPacket( PT_T_M_REQ_MAP_MOVE_TARGET, pkTargetPlayer->GetID() );
		kReqPacket.Push( kCharGuid );
		kReqPacket.Push( bGMCommand );
		return SendToGround( pkTargetPlayer->GroundKey(), kReqPacket, true );
	}
	else
	{
		T_GNDATTR const kGndAttr = (iGroundNo ? GATTR_PVP : GATTR_DEFAULT);
		BM::CPacket kFailedPacket( PT_T_C_ANS_MAP_MOVE_TARGET_FAILED, bGMCommand );
		kFailedPacket.Push( kGndAttr );
		kFailedPacket.Push( true );
		kFailedPacket.Push( pkTargetPlayer->Name() );
		return this->SendToUser( kCharGuid, kFailedPacket, false );
	}
}

bool PgServerSetMgr::Locked_TargetMapMoveByName( bool const bGMCommand, BM::GUID const &kCharGuid, std::wstring const &wstrTargetName, bool const bID )const
{
	BM::CAutoMutex kLock( m_kMutex );
	return ReqTargetMapMoveByName( bGMCommand, kCharGuid, wstrTargetName, bID );
}

bool PgServerSetMgr::ReqTargetMapMoveByName( bool const bGMCommand, BM::GUID const &kCharGuid, std::wstring const &wstrTargetName, bool const bID )const
{
	CONT_CENTER_PLAYER_BY_KEY::mapped_type pkTargetPlayer = NULL;

	if ( bID )
	{
		CONT_CENTER_PLAYER_BY_ID::const_iterator itr = m_kContPlayer_MemberID.find( wstrTargetName );
		if ( itr != m_kContPlayer_MemberID.end() )
		{
			pkTargetPlayer = itr->second;
		}
	}
	else
	{
		CONT_CENTER_PLAYER_BY_ID::const_iterator itr = m_kContPlayer_CharName.find( wstrTargetName );
		if ( itr != m_kContPlayer_CharName.end() )
		{
			pkTargetPlayer = itr->second;
		}
	}

	if ( !pkTargetPlayer )
	{
		return false;
	}

	int const iGroundNo = pkTargetPlayer->GroundKey().GroundNo();
	if ( PvP_Lobby_GroundNo_Max < iGroundNo )
	{
		BM::CPacket kReqPacket( PT_T_M_REQ_MAP_MOVE_TARGET, pkTargetPlayer->GetID() );
		kReqPacket.Push( kCharGuid );
		kReqPacket.Push( bGMCommand );
		return SendToGround( pkTargetPlayer->GroundKey(), kReqPacket, true );
	}
	else
	{
		T_GNDATTR const kGndAttr = (iGroundNo ? GATTR_PVP : GATTR_DEFAULT);
		BM::CPacket kFailedPacket( PT_T_C_ANS_MAP_MOVE_TARGET_FAILED, bGMCommand );
		kFailedPacket.Push( kGndAttr );
		kFailedPacket.Push( true );
		kFailedPacket.Push( pkTargetPlayer->Name() );
		return this->SendToUser( kCharGuid, kFailedPacket, false );
	}
}

bool PgServerSetMgr::ReqMapMove( UNIT_PTR_ARRAY& rkUnitArray, SReqMapMove_MT &rkRMM, CONT_PET_MAPMOVE_DATA &kContPetMapMoveData, CONT_PLAYER_MODIFY_ORDER &kContModifyOrder )
{
	if ( rkUnitArray.empty() )
	{
		INFO_LOG( BM::LOG_LV0, __FL__ << _T("UnitArray is Empty") );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	{
		// MapMove Lock 상태 검사
		UNIT_PTR_ARRAY::const_iterator unit_itor = rkUnitArray.begin();
		while (unit_itor != rkUnitArray.end())
		{
			BM::GUID kLockKey;
			if (GetMapMoveLock(unit_itor->pkUnit->GetID(), kLockKey))
			{
				if (kLockKey != rkRMM.kMapMoveKey)
				{
					INFO_LOG( BM::LOG_LV5, __FL__ << _T("Cannot MapMove, because already MapMoveLocked Player[") << unit_itor->pkUnit->GetID().str().c_str()
						<< _T("], Name[") << unit_itor->pkUnit->Name().c_str() << _T("]")
						);
					INFO_LOG( BM::LOG_LV5, __FL__ << _T("\t\tReqKey[") << rkRMM.kMapMoveKey.str().c_str() << _T("], SavedMapMoveKey[") << kLockKey.str().c_str() << _T("]")
						);
					LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
					return false;
				}
			}
			++unit_itor;
		}
	}

	BYTE byMapMoveType = MMT_NONE;
	switch ( rkRMM.cType )
	{
	case MMET_BackToChannel:
		{
			if ( g_kProcessCfg.IsPublicChannel() )
			{
				byMapMoveType = MMT_PUBLIC_PARTY;
				break;
			}
		} // break을 사용하지 않는다.
	case MMET_Login:
	case MMET_PublicChannelFailed:
		{
			if ( SUCCEEDED( GetServerIdentity( rkRMM.kTargetKey, rkRMM.kTargetSI ) ) )
			{
				byMapMoveType = MMT_FIRST;
			}
		}break;
	case MMET_Mission:
		{
			rkRMM.cType = MMET_Normal;// *****안하면 무한Call
			SPortalWaiter kPortalWaiter( rkRMM, PORTAL_MISSION );
			if ( UpdatePlayerData( rkUnitArray, &kPortalWaiter, true ) )
			{
				kPortalWaiter.m_kContPetMoveData.swap( kContPetMapMoveData );
				kPortalWaiter.m_kModifyOrder.swap( kContModifyOrder );

				BM::CPacket kMissionPortalPacket(PT_A_T_REQ_INDUN_CREATE);
				kPortalWaiter.WriteToPacket( kMissionPortalPacket );
				return SendToPortalMgr( kMissionPortalPacket );
				// return !!!
			}
		}break;
	case MMET_SuperGround:
		{
			rkRMM.cType = MMET_Normal;// *****안하면 무한Call
			SPortalWaiter kPortalWaiter( rkRMM, PORTAL_SUPERGND );
			if ( UpdatePlayerData( rkUnitArray, &kPortalWaiter, true ) )
			{
				kPortalWaiter.m_kContPetMoveData.swap( kContPetMapMoveData );
				kPortalWaiter.m_kModifyOrder.swap( kContModifyOrder );

				BM::CPacket kPortalPacket(PT_A_T_REQ_INDUN_CREATE);
				kPortalWaiter.WriteToPacket( kPortalPacket );
				return SendToPortalMgr( kPortalPacket );
				// return !!!
			}
		}break;
	case MMET_GoToPublic_PartyOrder:
		{
			if ( UpdatePlayerData( rkUnitArray, NULL, true  ) )
			{
				SPortalWaiter::CONT_WAIT_LIST kWaiterList;
					
				UNIT_PTR_ARRAY::iterator unit_itr = rkUnitArray.begin();
				for ( ; unit_itr!=rkUnitArray.end() ; ++unit_itr )
				{
					PgPlayer *pkPlayer = dynamic_cast<PgPlayer*>(unit_itr->pkUnit);
					if ( pkPlayer )
					{
						kWaiterList.insert( std::make_pair(pkPlayer->GetID(), pkPlayer->VolatileID()) );
					}
				}

				if ( kWaiterList.size() )
				{
					BM::CPacket kPartyMgrPacket( PT_T_T_REQ_READY_JOIN_HARDCORE );
					PU::TWriteTable_AA( kPartyMgrPacket, kWaiterList );
					PU::TWriteTable_AM( kPartyMgrPacket, kContPetMapMoveData );
					kContModifyOrder.WriteToPacket( kPartyMgrPacket );
					
					::SendToGlobalPartyMgr( kPartyMgrPacket );
				}

				return true;
				// return !!!
			}
		}break;
	case MMET_PvP:
		{
			if ( g_kProcessCfg.IsPublicChannel() )
			{// 공용채널에서여기 걸리면 SelectCharacter 에서 호출한거다.
				BM::CPacket kContents( PT_T_T_REQ_JOIN_LOBBY );
				rkUnitArray.WriteToPacket( kContents, WT_DEFAULT );

				if ( !SendToPvPLobby( kContents, rkRMM.kTargetKey.GroundNo() ) )
				{
					VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("Error") );
				}
				return true;
				// return !!!
			}
			else
			{
				byMapMoveType = MMT_PUBLIC_PERSONAL;
			}
		}break; 
	case MMET_BackToPvP:
	case MMET_KickToPvP:
		{
			if ( g_kProcessCfg.IsPublicChannel() )
			{
				byMapMoveType = MMT_DEFAULT;
				// return !!!
			}
			else
			{// 일반채널에서는 오면 안된다.
				VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("Critical Error!!") );
			}
		}break;
	case MMET_GoToPublicGround:
	case MMET_GoTopublicGroundParty:
	case MMET_BATTLESQUARE:
		{
			if ( !g_kProcessCfg.IsPublicChannel() )
			{
				byMapMoveType = ( (MMET_GoTopublicGroundParty == rkRMM.cType) ? MMT_PUBLIC_PARTY : MMT_PUBLIC_PERSONAL);
				break;//break
			}

			// 현재가 공용채널이면 채널 이동은 아니다.
			rkRMM.cType = MMET_Normal;	
		} // break을 사용하지 않는다.(이거다음에는 default:)
	default:
		{
			if ( SUCCEEDED( GetServerIdentity( rkRMM.kTargetKey, rkRMM.kTargetSI ) ) )
			{
				byMapMoveType = MMT_DEFAULT;
			}
		}break;
	}

	switch ( byMapMoveType )
	{
	case MMT_DEFAULT:
		{
			BM::CPacket kMovePacket(PT_T_N_REQ_MAP_MOVE, rkRMM);
			kMovePacket.Push(g_kProcessCfg.ChannelNo());
			kMovePacket.Push( rkUnitArray.size() );

			// MapMove Lock 걸기
			UNIT_PTR_ARRAY::const_iterator unit_itr = rkUnitArray.begin();
			for ( ; unit_itr != rkUnitArray.end() ; ++unit_itr )
			{
				InsertMapMoveLock(rkRMM.kMapMoveKey, unit_itr->pkUnit->GetID());
				unit_itr->pkUnit->WriteToPacket( kMovePacket, WT_MAPMOVE_FIRST );
			}

			PU::TWriteTable_AM( kMovePacket, kContPetMapMoveData );
			kContModifyOrder.WriteToPacket( kMovePacket );

			SendToContents(kMovePacket);
		}break;
	case MMT_FIRST:
		{
			// 로그인이면 Contents로 한번 더 보내주면 낭비다.
			BM::CPacket kMovePacket(PT_T_M_REQ_MAP_MOVE, rkRMM );
			rkUnitArray.WriteToPacket( kMovePacket, WT_MAPMOVE_SERVER );

			PU::TWriteTable_AM( kMovePacket, kContPetMapMoveData );
			kContModifyOrder.WriteToPacket( kMovePacket );

			SendToGround( rkRMM.kTargetKey, kMovePacket, true );
		}break;
	case MMT_PUBLIC_PERSONAL:
		{
			// 채널 이동이면 여기서 지워야 하는거야 짬시키지 말것!
			BM::CPacket kClientPacket( PT_T_C_NFY_MAP_MOVING_CHANNEL_CHANGE );

			BM::CPacket kMovePacket(PT_T_N_REQ_MAP_MOVE, rkRMM);
			kMovePacket.Push(g_kProcessCfg.ChannelNo());

			size_t const iWrPos = kMovePacket.WrPos();
			kMovePacket.Push( rkUnitArray.size() );
			size_t iWriteSize = 0;

			UNIT_PTR_ARRAY::iterator unit_itr = rkUnitArray.begin();
			for ( ; unit_itr != rkUnitArray.end() ; ++unit_itr )
			{
				PgPlayer *pkPlayer = dynamic_cast<PgPlayer*>(unit_itr->pkUnit);
				if ( pkPlayer )
				{
					PgParty_Release::DoAction( pkPlayer, false );// 파티 제거
					pkPlayer->WriteToPacket( kMovePacket, WT_MAPMOVE_FIRST );
					++iWriteSize;

					CONT_CENTER_PLAYER_BY_KEY::iterator key_itr = m_kContPlayer_CharKey.find( pkPlayer->GetID() );
					if ( key_itr != m_kContPlayer_CharKey.end() ) 
					{
						CONT_CENTER_PLAYER_BY_KEY::mapped_type pkSavedPtr = key_itr->second;
						if ( pkSavedPtr )
						{
							// 클라이언트한테 맵이동중 채널이 변경된다고 알려준다.
							::SendToUser( pkSavedPtr->GetMemberGUID(), pkSavedPtr->GetSwitchServer(), kClientPacket );

							// 센터급 컨텐츠한테 통보
							ProcessRemoveUser_NotifyOtherMgr( pkSavedPtr, true );

							// 플레이어 삭제
							RemovePlayer(pkSavedPtr);
							g_kTotalObjMgr.ReleaseUnit( pkSavedPtr );
						}
						else
						{
							VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("pkPlayer is NULL ") << pkPlayer->GetID() );
							LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkPlayer is NULL"));
						}
					}
					else
					{
						INFO_LOG( BM::LOG_LV5, __FL__ << _T("Not Found Player<") << pkPlayer->Name() << _T("/") << pkPlayer->GetID() << _T(">") );
						LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Not Find Data!"));
					}
				}
			}

			if ( iWriteSize != rkUnitArray.size() )
			{
				kMovePacket.ModifyData( iWrPos, &iWriteSize, sizeof(iWriteSize) );
			}

			PU::TWriteTable_AM( kMovePacket, kContPetMapMoveData );
			kContModifyOrder.WriteToPacket( kMovePacket );

			SendToContents(kMovePacket);
		}break;
	case MMT_PUBLIC_PARTY:
		{
			BM::CPacket kMovePacket(PT_T_N_REQ_MAP_MOVE, rkRMM);
			kMovePacket.Push(g_kProcessCfg.ChannelNo());
			
			size_t const iWrPos = kMovePacket.WrPos();
			kMovePacket.Push( rkUnitArray.size() );
			size_t iWriteSize = 0;

			UNIT_PTR_ARRAY::iterator unit_itr = rkUnitArray.begin();
			for ( ; unit_itr != rkUnitArray.end() ; ++unit_itr )
			{
				PgPlayer *pkPlayer = dynamic_cast<PgPlayer*>(unit_itr->pkUnit);
				if ( pkPlayer )
				{
					PgParty_Release::DoAction( pkPlayer, true );// 파티 버프 제거
					pkPlayer->WriteToPacket( kMovePacket, WT_MAPMOVE_FIRST );
					++iWriteSize;

					CONT_CENTER_PLAYER_BY_KEY::iterator key_itr = m_kContPlayer_CharKey.find( pkPlayer->GetID() );
					if ( key_itr != m_kContPlayer_CharKey.end() ) 
					{
						CONT_CENTER_PLAYER_BY_KEY::mapped_type pkSavedPtr = key_itr->second;
						if ( pkSavedPtr )
						{
							// 센터급 컨텐츠한테 통보
							ProcessRemoveUser_NotifyOtherMgr( pkSavedPtr, false );

							// 플레이어 삭제
							RemovePlayer(pkSavedPtr);
							g_kTotalObjMgr.ReleaseUnit( pkSavedPtr );
						}
						else
						{
							VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("pkPlayer is NULL ") << pkPlayer->GetID() );
							LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkPlayer is NULL"));
						}
					}
					else
					{
						INFO_LOG( BM::LOG_LV5, __FL__ << _T("Not Found Player<") << pkPlayer->Name() << _T("/") << pkPlayer->GetID() << _T(">") );
						LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Not Find Data!"));
					}
				}
			}

			if ( iWriteSize != rkUnitArray.size() )
			{
				kMovePacket.ModifyData( iWrPos, &iWriteSize, sizeof(iWriteSize) );
			}

			PU::TWriteTable_AM( kMovePacket, kContPetMapMoveData );
			kContModifyOrder.WriteToPacket( kMovePacket );

			SendToContents(kMovePacket);
		}break;
	default:
		{
			// 인던생성을 해야 하는 거 일 수도 있다!
			// 인던으로 생성 할 수 있는지 찾아보자.
			SGroundKey kGndkey( rkRMM.kTargetKey.GroundNo(), BM::GUID::IsNull(rkRMM.kTargetKey.Guid()) ? BM::GUID::Create() : rkRMM.kTargetKey.Guid() );
			HRESULT const hRet = GroundLoadBalance( kGndkey, rkRMM.kTargetSI );
			if ( S_OK == hRet )
			{
				// 인던으로 만들자.
				rkRMM.kTargetKey.Guid( kGndkey.Guid() );

				if ( rkRMM.cType == MMET_Login )
				{
					rkRMM.nTargetPortal = 1;
				}

				SPortalWaiter kPortalWaiter(rkRMM, (rkRMM.cType==MMET_Login) ? PORTAL_NODELAY : PORTAL_DELAY );
				kPortalWaiter.m_kContPetMoveData.swap( kContPetMapMoveData );
				kPortalWaiter.m_kModifyOrder.swap( kContModifyOrder );

				if ( UpdatePlayerData( rkUnitArray, &kPortalWaiter, true ) )
				{
					BM::CPacket kPortalPacket( PT_A_T_REQ_INDUN_CREATE );
					kPortalWaiter.WriteToPacket( kPortalPacket );
					SendToPortalMgr( kPortalPacket );
					return true;
				}
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
				return false;
			}

			if ( g_kProcessCfg.IsPublicChannel() )
			{// Public Channel 이라면 원래채널로 돌려보내야 한다.
				rkRMM.cType = MMET_BackToChannel;
				rkRMM.kTargetKey.Clear();
				return ReqMapMove( rkUnitArray, rkRMM, kContPetMapMoveData, kContModifyOrder );
			}

			INFO_LOG( BM::LOG_LV1, __FL__ << _T("NotFound Target Ground[") << rkRMM.kTargetKey.GroundNo() << _T("-") << rkRMM.kTargetKey.Guid().str().c_str() << _T("] Next UserList") );

			// 다시 돌려보낸다.	
			// 무한 Recursive Call 을 막기 위해 한번더 검사해 주기
			if( S_OK == GetServerIdentity(rkRMM.kCasterKey, rkRMM.kTargetSI ) )
			{
				if ( rkRMM.SetBackHome(MMET_Failed) )
				{
					kContModifyOrder.clear();
					return ReqMapMove( rkUnitArray, rkRMM, kContPetMapMoveData, kContModifyOrder );
				}			
			}

			INFO_LOG( BM::LOG_LV5, __FL__ << _T("Cannot go back to Origin_Map because cannot find Ground[") << rkRMM.kTargetKey.GroundNo() << _T("]") );
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}break;
	}

	return true;
}

void PgServerSetMgr::Locked_Recv_PT_M_T_REQ_MAP_MOVE_COME( SReqMapMove_CM const &kRMMC, VEC_GUID const &kUserList, bool const bMemberGuid )
{
	BM::CAutoMutex kLock(m_kMutex,true);
	
	UNIT_PTR_ARRAY kUnitArray;
	CONT_CENTER_PLAYER_BY_KEY::iterator login_itr;
	VEC_GUID::const_iterator user_itr = kUserList.begin();
	for ( ; user_itr!=kUserList.end(); ++user_itr )
	{
		if ( bMemberGuid )
		{
			login_itr = m_kContPlayer_MemberKey.find( *user_itr );
			if ( login_itr == m_kContPlayer_MemberKey.end() )
			{
				BM::CPacket kFailedPacket( PT_T_M_ANS_MAP_MOVE_COME_FAILED, *user_itr );
				SendToGround( kRMMC.kGndKey, kFailedPacket, true );
				continue;
			}
		}
		else
		{
			login_itr = m_kContPlayer_CharKey.find( *user_itr );
			if ( login_itr == m_kContPlayer_CharKey.end() )
			{
				BM::CPacket kFailedPacket( PT_T_M_ANS_MAP_MOVE_COME_FAILED, *user_itr );
				SendToGround( kRMMC.kGndKey, kFailedPacket, true );
				continue;
			}
		}

		PgPlayer *pkUser = login_itr->second;

		// MapMove Lock 상태 검사
		BM::GUID kLockKey;
		if (GetMapMoveLock(pkUser->GetID(), kLockKey))
		{
			if (kLockKey != BM::GUID::NullData())
			{
				INFO_LOG(BM::LOG_LV5, __FL__ << _T("Cannot MapMove, because already MapMoveLocked Player=") << pkUser->GetID() << _T(", Nanme=")
					<< pkUser->Name() << _T(", SavedMapMoveKey=") << kLockKey.str().c_str());

				BM::CPacket kFailedPacket( PT_T_M_ANS_MAP_MOVE_COME_FAILED, *user_itr );
				SendToGround( kRMMC.kGndKey, kFailedPacket, true );
				continue;
			}
		}

		int const iGroundNo = pkUser->GroundKey().GroundNo();
		switch ( iGroundNo )
		{
		case PvP_Lobby_GroundNo_AnterRoom:
		case PvP_Lobby_GroundNo_Exercise:
		case PvP_Lobby_GroundNo_Ranking:
		case PvP_Lobby_GroundNo_League:
			{
				if ( MMET_Normal != kRMMC.cType )
				{
					break;
				}
			}// break 사용하지 않는다.
		case 0:
			{
				kUnitArray.Add( pkUser );
			}break;
		default:
			{
				BM::CPacket kMPacket(PT_A_M_NFY_REQ_MAP_MOVE);
				kRMMC.WriteToPacket(kMPacket);
				kMPacket.Push( pkUser->GetID() );
				SendToGround( pkUser->GroundKey(), kMPacket, true );
			}break;
		}

	}

	if ( !kUnitArray.empty() )
	{
		SReqMapMove_MT kRMM((EMapMoveEventType)kRMMC.cType);
		kRMM.kTargetKey = kRMM.kCasterKey = kRMMC.kGndKey;
		kRMM.pt3TargetPos = kRMMC.pt3TargetPos;

		CONT_PET_MAPMOVE_DATA kEmptyPet;
		CONT_PLAYER_MODIFY_ORDER kEmptyOrder;
		ReqMapMove( kUnitArray, kRMM, kEmptyPet, kEmptyOrder );
	}
}

bool PgServerSetMgr::Locked_Recv_PT_M_T_REQ_MAP_MOVE( BM::CPacket * const pkPacket )
{
	BM::CAutoMutex kLock(m_kMutex, true);
	return RecvMapMove(pkPacket, false);
}

bool PgServerSetMgr::Locked_Recv_PT_T_T_REQ_MAP_MOVE( BM::CPacket * const pkPacket )
{
	BM::CAutoMutex kLock(m_kMutex, true);

	SReqMapMove_MT kRMM;
	if( pkPacket->Pop(kRMM) )
	{
		SPortalWaiter::CONT_WAIT_LIST kWaitList;
		CONT_PET_MAPMOVE_DATA kPetMapMoveData;
		CONT_PLAYER_MODIFY_ORDER kContModifyOrder;
		PU::TLoadTable_AA( *pkPacket, kWaitList );
		PU::TLoadTable_AM( *pkPacket, kPetMapMoveData );
		kContModifyOrder.ReadFromPacket( *pkPacket );

		UNIT_PTR_ARRAY kUnitArray;

		SPortalWaiter::CONT_WAIT_LIST::const_iterator wait_itr = kWaitList.begin();
		for ( ; wait_itr!=kWaitList.end() ; ++wait_itr )
		{
			CONT_CENTER_PLAYER_BY_KEY::const_iterator login_itr = m_kContPlayer_CharKey.find( wait_itr->first );
			
			// 여기서 맵이 할당 안된 놈들만 이동하게 걸러주어야 한다.
			if ( login_itr != m_kContPlayer_CharKey.end() )
			{
				CONT_CENTER_PLAYER_BY_KEY::mapped_type const &pkElement = login_itr->second;
				if ( pkElement->VolatileID() == wait_itr->second )
				{
					if ( pkElement->GroundKey().IsEmpty() )
					{
						kUnitArray.Add( pkElement );
					}
				}
				else
				{
					CAUTION_LOG( BM::LOG_LV6, __FL__ << L" Character<" << pkElement->Name() << L"/" << pkElement->GetID() << L"> LogOut->OtherJoin->OldMapMove Process Excute!!" );
				}
			}
		}

		if ( kUnitArray.size() )
		{
			return ReqMapMove( kUnitArray, kRMM, kPetMapMoveData, kContModifyOrder );
		}
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgServerSetMgr::Locked_Recv_PT_T_T_REQ_RECENT_MAP_MOVE( BM::CPacket * const pkPacket )
{
	SPortalWaiter::CONT_WAIT_LIST kWaiterList;
	PU::TLoadTable_AA( *pkPacket, kWaiterList );
	if ( kWaiterList.size() )
	{
		BM::CAutoMutex kLock(m_kMutex, true);

		SPortalWaiter::CONT_WAIT_LIST::const_iterator wait_itr = kWaiterList.begin();
		for ( ; wait_itr != kWaiterList.end() ; ++wait_itr )
		{
			CONT_CENTER_PLAYER_BY_KEY::const_iterator login_itr = m_kContPlayer_CharKey.find( wait_itr->first );
			if ( login_itr != m_kContPlayer_CharKey.end() )
			{
				CONT_CENTER_PLAYER_BY_KEY::mapped_type const &pkPlayer = login_itr->second;
				if ( pkPlayer )
				{// 여기서 맵이 할당 안된 놈들만 이동하게 걸러주어야 한다.
					if ( pkPlayer->VolatileID() == wait_itr->second )
					{
						if ( pkPlayer->GroundKey().IsEmpty() )
						{
							SReqMapMove_MT kRMM( MMET_Failed );
							kRMM.pt3TargetPos = pkPlayer->GetRecentPos(GATTR_DEFAULT);
							pkPlayer->GetRecentGround( kRMM.kTargetKey, GATTR_DEFAULT );
							kRMM.kCasterKey = kRMM.kTargetKey;

							UNIT_PTR_ARRAY kUnitArray(pkPlayer);
							CONT_PET_MAPMOVE_DATA kEmptyPet;
							CONT_PLAYER_MODIFY_ORDER kEmptyOrder;
							ReqMapMove( kUnitArray, kRMM, kEmptyPet, kEmptyOrder );
						}
					}
					else
					{
						CAUTION_LOG( BM::LOG_LV6, __FL__ << L" Character<" << pkPlayer->Name() << L"/" << pkPlayer->GetID() << L"> LogOut->OtherJoin->OldMapMove Process Excute!!" );
					}
				}
			}
		}
		return true;
	}
	
	return false;
}

bool PgServerSetMgr::RecvMapMove( BM::CPacket * const pkPacket, bool bResMovePacket )
{
	SReqMapMove_MT kRMM;
	if( pkPacket->Pop(kRMM) )
	{
		UNIT_PTR_ARRAY kUnitArray;
		kUnitArray.ReadFromPacket(*pkPacket);

		CONT_PET_MAPMOVE_DATA kContPetMapMoveData;
		PU::TLoadTable_AM( *pkPacket, kContPetMapMoveData );

		CONT_PLAYER_MODIFY_ORDER kContModifyOrder;
		kContModifyOrder.ReadFromPacket( *pkPacket );

		if ( true == bResMovePacket )
		{
			UNIT_PTR_ARRAY::const_iterator itor_unit = kUnitArray.begin();
			while (itor_unit != kUnitArray.end())
			{
				RemoveMapMoveLock((*itor_unit).pkUnit->GetID());
				++itor_unit;
			}

			if ( UpdatePlayerData( kUnitArray ) )
			{
				SERVER_IDENTITY kSI;
				switch ( kRMM.cType )
				{
				case MMET_PvP:
				case MMET_BackToChannel:
					{
						VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("Cannot go to this line TargetGroundType[") << kRMM.cType << _T("]") );
					}break;
				case MMET_BackToPvP:
				case MMET_KickToPvP:
					{
						bool const bKick = ( MMET_KickToPvP == kRMM.cType );

						BM::CPacket kContents( PT_C_T_REQ_EXIT_ROOM, bKick );
						kUnitArray.WriteToPacket( kContents, WT_MAPMOVE_SERVER|WT_OP_NOHPMP );

						if ( !SendToPvPLobby( kContents, kRMM.kTargetKey.GroundNo() ) )
						{
							VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("Error") );
						}
					}break;
				default:
					{
						if( S_OK == GetServerIdentity( kRMM.kTargetKey, kSI ) )
						{//서버가 있어야지.
							BM::CPacket kPacket(PT_T_M_REQ_MAP_MOVE, kRMM );
							kUnitArray.WriteToPacket( kPacket, WT_MAPMOVE_SERVER );
							PU::TWriteTable_AM( kPacket, kContPetMapMoveData );
							kContModifyOrder.WriteToPacket( kPacket );
							return SendToGround( kRMM.kTargetKey, kPacket, true );
						}

						// 서버가 없으면 다시 돌려보내야 해
						// 여기 걸릴일이 있을까?
						if ( kRMM.SetBackHome(MMET_Failed) )
						{
							return ReqMapMove( kUnitArray, kRMM, kContPetMapMoveData, kContModifyOrder );
						}

						UNIT_PTR_ARRAY::iterator unit_itr = kUnitArray.begin();
						for ( ; unit_itr!=kUnitArray.end(); ++unit_itr )
						{
							INFO_LOG( BM::LOG_LV0, __FL__ << _T("[MapMove Failed] [MapNo:") << kRMM.kTargetKey.GroundNo() << _T("] ") 
								<< unit_itr->pkUnit->Name().c_str() << _T("-") << unit_itr->pkUnit->GetID().str().c_str() << _T("]") );

							PgPlayer *pkPlayer = dynamic_cast<PgPlayer*>(unit_itr->pkUnit);
							if ( pkPlayer )
							{
								BM::CPacket kDPacket( PT_A_S_NFY_USER_DISCONNECT, static_cast<BYTE>(CDC_CharMapErr) );
								kDPacket.Push( pkPlayer->GetMemberGUID() );
								SendToServer( pkPlayer->GetSwitchServer(), kDPacket );
							}
						}
					}break;
				}
			}
		}
		else
		{
			// Map으로 부터 요청 받았다 -> Contents로 보내줘야 한다.
			return ReqMapMove( kUnitArray, kRMM, kContPetMapMoveData, kContModifyOrder );
		}
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgServerSetMgr::Locked_Recv_PT_M_T_REQ_Kick_User( BM::CPacket * const pkPacket  )
{
	BM::CAutoMutex kLock(m_kMutex, true);

	BM::GUID kCmdGuid;
	BM::GUID kMemberGuid;

	pkPacket->Pop( kCmdGuid );
	pkPacket->Pop( kMemberGuid );

	CONT_CENTER_PLAYER_BY_KEY::iterator login_itr = m_kContPlayer_MemberKey.find( kMemberGuid );
	if ( login_itr != m_kContPlayer_MemberKey.end() )
	{
		CONT_CENTER_PLAYER_BY_KEY::mapped_type pCData = login_itr->second;
		if ( pCData )
		{
//			SChannelChangeData kChannelChangeData( g_kProcessCfg.RealmNo(), nChannelNo );
//			kChannelChangeData.kCharGuid = pCData->GetID();

			BM::CPacket kPacket( PT_T_N_ANS_KICK_USER);
			kPacket.Push( kCmdGuid );

			ProcessRemoveUser_Common(pCData);

			RemovePlayer(pCData);
			g_kTotalObjMgr.ReleaseUnit( pCData );

			SendToContents(kPacket);

			return true;
		}
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

void PgServerSetMgr::Locked_Recv_PT_M_T_REFRESH_LEVELUP(BM::CPacket* const pkPacket)
{
	BM::CAutoMutex kLock(m_kMutex, true);

	BM::GUID kCharGuid;
	int iLevel = 0;

	pkPacket->Pop(kCharGuid);
	pkPacket->Pop(iLevel);

	CONT_CENTER_PLAYER_BY_KEY::const_iterator iter = m_kContPlayer_CharKey.find(kCharGuid);
	if( m_kContPlayer_CharKey.end() != iter )
	{
		CONT_CENTER_PLAYER_BY_KEY::mapped_type const pkElement = (*iter).second;
		if( pkElement)
		{
			if( iLevel > 0  )
			{
				pkElement->SetAbil(AT_LEVEL, iLevel);
			}			
		}
	}
}

void PgServerSetMgr::Locked_Recv_PT_M_T_REFRESH_CLASS_CHANGE(BM::CPacket* const pkPacket)
{
	BM::CAutoMutex kLock(m_kMutex, true);

	BM::GUID kCharGuid;
	int iClass = 0;

	pkPacket->Pop(kCharGuid);
	pkPacket->Pop(iClass);

	CONT_CENTER_PLAYER_BY_KEY::const_iterator iter = m_kContPlayer_CharKey.find(kCharGuid);
	if( m_kContPlayer_CharKey.end() != iter )
	{
		CONT_CENTER_PLAYER_BY_KEY::mapped_type const pkElement = (*iter).second;
		if( pkElement)
		{
			if( iClass > 0  )
			{
				pkElement->SetAbil(AT_CLASS, iClass);
			}			
		}
	}
}

bool PgServerSetMgr::Locked_PT_T_T_REQ_EXIT_LOBBY( BM::CPacket* const pkPacket )
{
	size_t const iRDPos = pkPacket->RdPos();

	pkPacket->RdPos( iRDPos + sizeof(EUnitType) );
	
	BM::GUID kCharacterGuid;
	pkPacket->Pop( kCharacterGuid );

	pkPacket->RdPos( iRDPos );

	BM::CAutoMutex kLock(m_kMutex, true);

 	CONT_CENTER_PLAYER_BY_KEY::iterator login_itr = m_kContPlayer_CharKey.find( kCharacterGuid );
	if ( login_itr != m_kContPlayer_CharKey.end() )
	{
		PgPlayer *pkPlayer = login_itr->second;
		pkPlayer->ReadFromPacket( *pkPacket );

		SReqMapMove_MT kRMM( MMET_BackToChannel );
		kRMM.pt3TargetPos = pkPlayer->GetRecentPos(GATTR_DEFAULT);
		pkPlayer->GetRecentGround( kRMM.kTargetKey, GATTR_DEFAULT );

		UNIT_PTR_ARRAY kUnitArray(pkPlayer);
		CONT_PET_MAPMOVE_DATA kEmptyPet;
		CONT_PLAYER_MODIFY_ORDER kEmptyOrder;
 		return ReqMapMove( kUnitArray, kRMM, kEmptyPet, kEmptyOrder );
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

void PgServerSetMgr::Locked_PT_T_T_REQ_KICK_ROOM_USER( BM::GUID const &kCharGuid, SGroundKey const &kGndKey )const
{
	BM::CAutoMutex kLock( m_kMutex, false );

	CONT_CENTER_PLAYER_BY_KEY::const_iterator login_itr = m_kContPlayer_CharKey.find( kCharGuid );
	if ( login_itr != m_kContPlayer_CharKey.end() )
	{
		PgPlayer const *pkPlayer = login_itr->second;
		switch( pkPlayer->GroundKey().GroundNo() )
		{
		case PvP_Lobby_GroundNo_Exercise:
		case PvP_Lobby_GroundNo_Ranking:
		case PvP_Lobby_GroundNo_League:
			{
				// 로비에만 있는 거니까
				BM::CPacket kPacket( PT_ROOM_TO_LOBBY_USER, static_cast<size_t>(1) );
				kPacket.Push( kCharGuid );
				kPacket.Push( true );
				SendToPvPLobby( kPacket, pkPlayer->GroundKey().GroundNo() );
			}break;
		case PvP_Lobby_GroundNo_AnterRoom:
		case 0:
			{
				VERIFY_INFO_LOG( false, BM::LOG_LV5, __FL__ );
				BM::CPacket kPacket( PT_ROOM_TO_LOBBY_USER, static_cast<size_t>(1) );
				kPacket.Push( kCharGuid );
				kPacket.Push( true );
				SendToPvPLobby( kPacket, pkPlayer->GroundKey().GroundNo() );
				// 이건 안되는데.
			}break;
		default:
			{
				if ( kGndKey == pkPlayer->GroundKey() )
				{
					VERIFY_INFO_LOG( false, BM::LOG_LV5, __FL__ );
				}
				else
				{
					BM::CPacket kGndPacket( PT_T_M_REQ_KICK_ROOM_USER, kCharGuid );
					SendToGround( pkPlayer->GroundKey(), kGndPacket, true );
				}
			}break;
		}
	}
}

bool PgServerSetMgr::Locked_Recv_PT_T_T_ANS_MAP_MOVE_RESULT( BM::GUID const &kCharGuid, SAnsMapMove_MT& rkAMM )
{
	BM::CAutoMutex kLock(m_kMutex, true);
	return RecvAnsMapMove( kCharGuid, rkAMM, rkAMM.kGroundKey );
}

bool PgServerSetMgr::Locked_Recv_PT_M_T_ANS_MAP_MOVE_RESULT( BM::CPacket * const pkPacket )
{
	SAnsMapMove_MT kAMM;
	if ( !pkPacket->Pop(kAMM) )
	{
		return false;
	}

	UNIT_PTR_ARRAY kUnitArray;
	kUnitArray.ReadFromPacket( *pkPacket, false );

	BM::CAutoMutex kLock(m_kMutex, true);

	if( !IsAliveGround( kAMM.kGroundKey ) )
	{
		kAMM.eRet = MMR_FAILED_AGAIN;
	}

	switch( kAMM.eRet )
	{
	case MMR_NONE:		//	처음 로그인해서 Map이동에 성공한 경우이다.
	case MMR_SUCCESS:	//	Map이동에 성공했으니까 이동시키자
		{
			SGroundKey kRealGndKey = kAMM.kGroundKey;
			kRealGndKey.ReadFromPacket( *pkPacket );

			if ( RecvAnsMapMove( kUnitArray, kAMM, kRealGndKey ) )
			{
				static size_t const iSize = 1;

				UNIT_PTR_ARRAY::iterator unit_itr = kUnitArray.begin();
				for ( ; unit_itr!=kUnitArray.end(); ++unit_itr )
				{
					PgPlayer* pkUser = dynamic_cast<PgPlayer*>(unit_itr->pkUnit);

					// 유저에게 통보
					BM::CPacket kCPacket( PT_T_C_NFY_CHARACTER_MAP_MOVE );
					kCPacket.Push(kAMM.cType);
					kCPacket.Push(kRealGndKey);
					kCPacket.Push(kAMM.kAttr);
					kCPacket.Push(pkUser->GetID());
					kCPacket.Push(iSize);
					if ( MMR_NONE == kAMM.eRet )
					{
						pkUser->WriteToPacket( kCPacket, WT_MAPMOVE_FIRST );
					}
					else
					{
						pkUser->WriteToPacket( kCPacket, WT_MAPMOVE_CLIENT );
					}
					kCPacket.Push( *pkPacket );

					SendToUser( pkUser->GetMemberGUID(), kCPacket );
				}
			}
		}break;
	case MMR_FAILED:	// Map에서 이동이 실패했으니까 이전맵으로 다시 돌려보내자
		{
			SReqMapMove_MT kRMM((EMapMoveEventType)kAMM.cType);
			kRMM.nTargetPortal = 0;
			kRMM.pt3TargetPos = kAMM.pt3Pos;
			kRMM.kCasterKey = kRMM.kTargetKey = kAMM.kGroundKey;

			CONT_PET_MAPMOVE_DATA kEmptyPet;
			CONT_PLAYER_MODIFY_ORDER kEmptyOrder;
			return ReqMapMove( kUnitArray, kRMM, kEmptyPet, kEmptyOrder );
		}break;
	case MMR_FAILED_AGAIN:	
		{
			UNIT_PTR_ARRAY::iterator unit_itr = kUnitArray.begin();
			for ( ; unit_itr!=kUnitArray.end(); ++unit_itr )
			{
				INFO_LOG( BM::LOG_LV5, __FL__ << _T("This User<") << unit_itr->pkUnit->Name() << _T("> Missing Ground") );
				PgPlayer* pkUser = dynamic_cast<PgPlayer*>(unit_itr->pkUnit);
				if ( pkUser )
				{
					if ( g_kProcessCfg.IsPublicChannel() )
					{
						// 공용채널에서는 이전채널로 복귀 시켜주어야 한다.
						SReqMapMove_MT kRMM( MMET_BackToChannel );
						kRMM.pt3TargetPos = pkUser->GetRecentPos(GATTR_DEFAULT);
						pkUser->GetRecentGround( kRMM.kTargetKey, GATTR_DEFAULT );

						CONT_PET_MAPMOVE_DATA kEmptyPet;
						CONT_PLAYER_MODIFY_ORDER kEmptyOrder;
						return ReqMapMove( kUnitArray, kRMM, kEmptyPet, kEmptyOrder );
//						break;
					}
					else
					{
						// 일반 채널이면 접속을 끊어버려!!
						BM::CPacket kDPacket( PT_A_S_NFY_USER_DISCONNECT, static_cast<BYTE>(CDC_CharMapErr) );
						kDPacket.Push( pkUser->GetMemberGUID() );
						::SendToServer( pkUser->GetSwitchServer(), kDPacket );
					}
				}
				else
				{
					VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << unit_itr->pkUnit->Name() << _T("/") << unit_itr->pkUnit->GetID() << _T(" Is Not Player!!!") );
				}
			}	
		}break;
	}
	return true;
}

bool PgServerSetMgr::FindRandomMap( SGroundKey& kGndKey )const
{
// 	CONT_GROUND::const_iterator itrNo = m_kContAliveGround.begin();
// 	while(m_kContAliveGround.end() != itrNo)//아무맵이나 첫번째 맵을 찾아서
// 	{
// 		CONT_GROUND::mapped_type const& rkGnd = (*itrNo).second;
// 		if(	rkGnd.kAttr == GATTR_DEFAULT || rkGnd.kAttr == GATTR_VILLAGE )
// 		{
// 			kGndKey = rkGnd.kKey;
// 			return true;
// 		}
// 		++itrNo;
// 	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

// HRESULT PgServerSetMgr::Locked_UserOpenGround(BM::GUID const &rkMemberGuid, int const iGroundNo)
// {
// 	BM::CAutoMutex kLock(m_kMutex, true);
// 
// 	CONT_CENTER_PLAYER_BY_KEY::iterator login_itr = m_kContPlayer_MemberKey.find(rkMemberGuid);
// 	if ( login_itr == m_kContPlayer_MemberKey.end() )
// 	{
// 		return E_FAIL;
// 	}
// 
// 	return login_itr->second->kMapInfo.IsOpenGround(iGroundNo);
// }

bool PgServerSetMgr::RecvAnsMapMove( BM::GUID const &kCharGuid, SAnsMapMove_MT const &rkAMM, SGroundKey const &kRealGndKey )
{
	CONT_CENTER_PLAYER_BY_KEY::iterator login_itr=  m_kContPlayer_CharKey.find( kCharGuid );
	if ( login_itr != m_kContPlayer_CharKey.end() )
	{
		PgPlayer *pkPlayer = login_itr->second;
		if ( pkPlayer )
		{
			// 스위치로 통보
			BM::CPacket kSPacket( PT_T_S_NFY_CHARACTER_MAP_MOVE, rkAMM.kSI );
			kSPacket.Push( kRealGndKey );
			kSPacket.Push( pkPlayer->GetMemberGUID() );
			kSPacket.Push( pkPlayer->GetID() );
			SendToServer( pkPlayer->GetSwitchServer(), kSPacket );

			INFO_LOG( BM::LOG_LV0, __FL__ << _T("[") << pkPlayer->Name().c_str() << _T("]MapServer Changed Success[") << rkAMM.kSI.nServerNo << _T("]") );
			pkPlayer->GroundKey(rkAMM.kGroundKey);//그라운드키 저장. (서버프로세스 번호는 저장 안함)

//			if ( ! rkAMM.kGroundKey.IsEmpty() )
//			{
				// 리센트 정보만 센터서버의 메모리에 업데이트 해준다.
//				pkPlayer->UpdateRecentGround( rkAMM.kGroundKey, rkAMM.kAttr);// 필요가 있나??
//				pkPlayer->SetPos(  );
//				pkPlayer->UpdateRecentPos( rkAMM.kAttr );
//			}

			{
				// 각각의 Contents 클래스로 통보
				SContentsUser kContentsUserData;
				pkPlayer->CopyTo(kContentsUserData);

				BM::CPacket kXPacket(PT_T_N_NFY_USER_ENTER_GROUND);
				kXPacket.Push(rkAMM);
				kContentsUserData.WriteToPacket(kXPacket);
				kXPacket.Push(pkPlayer->GuildGuid());
				::SendToGlobalPartyMgr(kXPacket);						kXPacket.PosAdjust();
				::SendToFriendMgr(kXPacket);							kXPacket.PosAdjust();
				::SendToGuildMgr(kXPacket);								kXPacket.PosAdjust();
				::SendToCoupleMgr(kXPacket);							kXPacket.PosAdjust();
				::SendToRealmContents(PMET_EVENTQUEST, kXPacket);		kXPacket.PosAdjust();
				::SendToRealmContents(PMET_BATTLESQUARE, kXPacket);		kXPacket.PosAdjust();
				{
					BM::CPacket kNfyPacket(PT_T_N_NFY_USER_ENTER_GROUND, kContentsUserData.kCharGuid);
					kContentsUserData.WriteToPacket(kNfyPacket);
					::SendToRealmContents(PMET_JS_WORKBENCH, kNfyPacket);
				}
			}
			return true;
		}	
		VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("[") << kCharGuid.str().c_str() << _T("]Is NULL") );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkPlayer is NULL"));
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

//맵에서 받은 유저 정보를 기록해야됨.
bool PgServerSetMgr::RecvAnsMapMove( UNIT_PTR_ARRAY &rkUnitArray, SAnsMapMove_MT const &rkAMM, SGroundKey const &kRealGndKey )
{
	CONT_CENTER_PLAYER_BY_KEY::iterator login_itr;
	UNIT_PTR_ARRAY::iterator unit_itr = rkUnitArray.begin();
	while ( unit_itr!= rkUnitArray.end() )
	{
		if ( !RecvAnsMapMove( unit_itr->pkUnit->GetID(), rkAMM, kRealGndKey ) )
		{
			unit_itr = rkUnitArray.erase( unit_itr );
		}
		else
		{
			++unit_itr;
		}
	}

	return (!rkUnitArray.empty());
}

bool PgServerSetMgr::Locked_NfyReloadGameData()
{
	BM::CAutoMutex kLock(m_kMutex, true);

	BM::CPacket kPacket(PT_T_M_NFY_RELOAD_GAMEDATA);

	INFO_LOG(BM::LOG_LV7, _T("Send StoreKey(ReloadData) ::")<< g_kControlDefMgr.StoreValueKey());
	kPacket.Push(g_kControlDefMgr.StoreValueKey());//StoreKey. 나감.
	g_kTblDataMgr.DataToPacket(kPacket);
	
	SendToServerType(CEL::ST_MAP, kPacket);
	SendToServerType(CEL::ST_ITEM, kPacket);

	return true;
}

bool PgServerSetMgr::CheckValidBaseWear(int const iWearNo, const EKindCharacterBaseWear eType)
{
	CONT_DEFCHARACTER_BASEWEAR const* pContDefCharacterBaseWear = NULL;
	g_kTblDataMgr.GetContDef(pContDefCharacterBaseWear);

	CONT_DEFCHARACTER_BASEWEAR::const_iterator wearItor = pContDefCharacterBaseWear->find(iWearNo);
	if(pContDefCharacterBaseWear->end() != wearItor)
	{
		if(eType == (*wearItor).second.iWearType)
		{
			return true;
		}
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

void PgServerSetMgr::Locked_DisplayState()
{
	BM::CAutoMutex kLock(m_kMutex);
	U_STATE_LOG(BM::LOG_LV1, _T("------------ Logined User List -----------"));

	CONT_CENTER_PLAYER_BY_ID::const_iterator user_itor = m_kContPlayer_MemberID.begin();

	while(user_itor != m_kContPlayer_MemberID.end())
	{
		CONT_CENTER_PLAYER_BY_ID::mapped_type pElement = (*user_itor).second;
		if( pElement )
		{
			U_STATE_LOG(BM::LOG_LV6, L"LoginedUser ID["<<(*user_itor).first<<L"] "<<C2L(pElement->GroundKey()));
		}
		else
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV1, __FL__<<L"pElement is NULL");
		}
		++user_itor;
	}

	U_STATE_LOG(BM::LOG_LV1, _T("--------------------- Cut Line ---------------------"));
}

void PgServerSetMgr::Recv_PT_M_T_NFY_CREATE_GROUND( BM::CPacket &rkPacket )
{
	GroundArr kGndArr;
	rkPacket.Pop(kGndArr);

	GroundArr::const_iterator gnd_itr = kGndArr.begin();
	for( ; gnd_itr != kGndArr.end() ; ++gnd_itr )
	{
		SGround const &rkGnd = *gnd_itr;
		if ( SUCCEEDED( Locked_RegistGnd( rkGnd ) ) )
		{
			if ( BM::GUID::IsNotNull(rkGnd.kKey.Guid()) )
			{
				switch ( rkGnd.kAttr )
				{
				case GATTR_EMPORIA:
				case GATTR_EMPORIABATTLE:
				case GATTR_HARDCORE_DUNGEON:
					{
					}break;
				case GATTR_BOSS:
				case GATTR_HARDCORE_DUNGEON_BOSS:
					{
						BM::CPacket kPortalPacket( PT_M_T_ANS_CREATE_GROUND );
						rkGnd.kKey.WriteToPacket( kPortalPacket );
						kPortalPacket.Push(S_OK);
						::SendToPortalMgr( kPortalPacket );

						::SendToChannelContents( PMET_Boss, kPortalPacket );
					}break;
				default:
					{
						BM::CPacket kPortalPacket( PT_M_T_ANS_CREATE_GROUND, rkGnd.kKey );
						kPortalPacket.Push(S_OK);
						::SendToPortalMgr( kPortalPacket );
					}break;
				}
			}

			BM::CPacket kBSPacket(PT_A_A_REQ_BS_GAME_INFO);
			rkGnd.kKey.WriteToPacket( kBSPacket );
			::SendToRealmContents( PMET_BATTLESQUARE, kBSPacket );

			CONT_DEFGROUNDBUILDINGS const * pkContDef = NULL;
			g_kTblDataMgr.GetContDef(pkContDef);
			if(pkContDef)
			{
				CONT_DEFGROUNDBUILDINGS::const_iterator gnd_iter = pkContDef->find( rkGnd.kKey.GroundNo() );
				if ( gnd_iter != pkContDef->end() )
				{
					for(CONT_SET_BUILDINGS::const_iterator map_iter = (*gnd_iter).second.kCont.begin();map_iter != (*gnd_iter).second.kCont.end();++map_iter)
					{
						//if(0 < (*map_iter).iGrade) // 모든 마이홈 로딩
						{
							BM::CPacket kHomeTownPacket( PT_T_N_REQ_LOAD_MYHOME );
							rkGnd.kKey.WriteToPacket( kHomeTownPacket );
							kHomeTownPacket.Push( (*map_iter).iBuildingNo );
							::SendToContents(kHomeTownPacket);
						}
					}
				}
			}
		}
	}
}

HRESULT PgServerSetMgr::Locked_RegistGnd( SGround const &rkGnd )
{
	BM::CAutoMutex kLock( m_kMutex, true );

	CONT_GROUND::_Pairib ret = m_kContAliveGround.insert(std::make_pair(rkGnd.kKey, rkGnd));
	if( ret.second )
	{
		SServerBalance* pkBalance = GetServerBalance(rkGnd.kSI);
		if ( pkBalance )
		{
			pkBalance->IncGround(rkGnd.kAttr);
		}

		INFO_LOG( BM::LOG_LV6, L"[RegistGround] " << rkGnd.kKey.GroundNo() <<L" / " << rkGnd.kKey.Guid() );
		return S_OK;
	}

	CAUTION_LOG( BM::LOG_LV1, L"[RegistGround]Failed "<< rkGnd.kKey.GroundNo() <<L" / " << rkGnd.kKey.Guid() );
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << L"Return E_FAIL");
	return E_FAIL;
}

void PgServerSetMgr::UnregistGnd( SGround const &rkGnd )
{
	switch ( rkGnd.kAttr )
	{
	case GATTR_BOSS:
	case GATTR_HARDCORE_DUNGEON_BOSS:
	case GATTR_SUPER_GROUND:
		{
			BM::CPacket kNfyPacket( PT_M_T_NFY_DELETE_GROUND, rkGnd.kKey );
			SendToChannelContents( PMET_Boss, kNfyPacket );
		}break;
	case GATTR_MISSION:
	case GATTR_CHAOS_MISSION:
		{
			BM::CPacket kNfyPacket( PT_T_T_NFY_MISSION_DELETE, rkGnd.kKey );
			SendToMissionMgr( kNfyPacket );
		}break;
	case GATTR_EMPORIA:
	case GATTR_EMPORIABATTLE:
		{
			BM::CPacket kNfyPacket( PT_T_N_ANS_DELETE_PUBLICMAP, rkGnd.kKey );
			SendToRealmContents( PMET_EMPORIA, kNfyPacket );
		}break;
	case GATTR_HARDCORE_DUNGEON:
		{
			BM::CPacket kNfyPacket( PT_T_N_ANS_DELETE_PUBLICMAP, rkGnd.kKey );
			SendToRealmContents( PMET_HARDCORE_DUNGEON, kNfyPacket );
		}break;
	}

//	size_t const iRemoveUserCount = ProcessRemoveUser_ByGround( kGnd.kKey );

	SServerBalance* pkBalance = GetServerBalance(rkGnd.kSI);
	if ( pkBalance )
	{
		pkBalance->DecGround( rkGnd.kAttr );
	}

	// 로그를 남겨
// 	PgLogWrapper kLog(LOG_CONTENTS_MAP_CREATE);
// 	kLog.MemberKey( kGnd.kKey.Guid() );
// 	kLog.Push( static_cast<int>(0) );			// iValue1(삭제0)
// 	kLog.Push( static_cast<int>(kGnd.kAttr) );	// iValue2(GroundType)
// 	kLog.Send();

	INFO_LOG( BM::LOG_LV6, L"[UnRegistGround] " << rkGnd.kKey.GroundNo() <<L" / " << rkGnd.kKey.Guid() );
}

HRESULT PgServerSetMgr::Locked_UnregistGnd(const GND_KEYLIST &kGndKeyList)
{
	BM::CAutoMutex kLock( m_kMutex, true );
	GND_KEYLIST::const_iterator key_itr = kGndKeyList.begin();
	for ( ; key_itr != kGndKeyList.end() ; ++key_itr )
	{
		CONT_GROUND::iterator gnd_itr = m_kContAliveGround.find(*key_itr);
		if( gnd_itr != m_kContAliveGround.end() )
		{
			UnregistGnd( gnd_itr->second );
			m_kContAliveGround.erase(gnd_itr);
		}
	}

	return S_OK;
}

HRESULT PgServerSetMgr::Locked_ReqUnregistGnd(SERVER_IDENTITY const &kSI)
{
	if( kSI.nServerType != CEL::ST_MAP )
	{
		return S_OK;
	}

	BM::CAutoMutex kLock( m_kMutex, true );
	GND_KEYLIST kGndKeyList;
	CONT_GROUND::iterator gnd_itor = m_kContAliveGround.begin();
	while( gnd_itor != m_kContAliveGround.end() )
	{
		if((*gnd_itor).second.kSI == kSI)
		{
			kGndKeyList.push_back((*gnd_itor).first);
		}
		++gnd_itor;
	}
	BM::CPacket kTPacket(PT_T_N_REQ_DELETE_GROUND, g_kProcessCfg.ChannelNo());
	kTPacket.Push(kGndKeyList);
	SendToContents(kTPacket);

	return S_OK;

}

bool PgServerSetMgr::Locked_IsAliveGround(SGroundKey const &rkKey)const
{
	BM::CAutoMutex kLock( m_kMutex, false );
	return IsAliveGround( rkKey );
}

bool PgServerSetMgr::IsAliveGround( SGroundKey const &rkKey )const
{
	CONT_GROUND::const_iterator gnd_itr = m_kContAliveGround.find(rkKey);
	return m_kContAliveGround.end() != gnd_itr;
}

HRESULT PgServerSetMgr::GetServerIdentity(SGroundKey const &kGndKey, SERVER_IDENTITY &rkOutSI)const
{
	CONT_GROUND::const_iterator gnd_itor = m_kContAliveGround.find(kGndKey);

	if(gnd_itor != m_kContAliveGround.end())
	{
		rkOutSI = (*gnd_itor).second.kSI;
		return S_OK;	
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return E_FAIL"));
	return E_FAIL;
}

SGroundKey PgServerSetMgr::GetPlayerKey(BM::GUID const &rkMember)
{
	SGroundKey kGndKey;

	CONT_CENTER_PLAYER_BY_KEY::iterator itor = m_kContPlayer_MemberKey.find( rkMember );
	if( m_kContPlayer_MemberKey.end() != itor)
	{
		CONT_CENTER_PLAYER_BY_KEY::mapped_type pData = (*itor).second;
		kGndKey = pData->GroundKey();
	}

	return kGndKey;
}

SGroundKey PgServerSetMgr::GetPlayerKey(std::wstring const &kId)
{
	SGroundKey kGndKey;
	
	
	CONT_CENTER_PLAYER_BY_ID::iterator itor = m_kContPlayer_MemberID.find( kId );
	if( m_kContPlayer_MemberID.end() != itor)
	{
		CONT_CENTER_PLAYER_BY_ID::mapped_type pData = (*itor).second;
		kGndKey = pData->GroundKey();
	}

	return kGndKey;
}

SServerBalance* PgServerSetMgr::GetServerBalance(SERVER_IDENTITY const &rhs)const
{
	CONT_SERVER_BALANCE::const_iterator itr;
	for ( itr=m_kContServerBalance.begin(); itr!= m_kContServerBalance.end(); ++itr )
	{
		if ( *(*itr) == rhs )
		{
			return *itr;
		}
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return NULL"));
	return NULL;
}

void PgServerSetMgr::Locked_Build( CONT_SERVER_HASH const &kContServerHash, CONT_MAP_CONFIG const &kContMapCfg_Static, CONT_MAP_CONFIG const &kContMapCfg_Mission )
{
	BM::CAutoMutex kLock( m_kMutex, true );

	CONT_DEFMAP const *pkContDefMap = NULL;
	g_kTblDataMgr.GetContDef( pkContDefMap );

	short const nMyRealm = g_kProcessCfg.RealmNo();
	short const nMyChannel = g_kProcessCfg.ChannelNo();

	CONT_SERVER_HASH::const_iterator server_itr = kContServerHash.begin();
	for ( ; server_itr != kContServerHash.end() ; ++server_itr )
	{
		if (	server_itr->first.nRealm == nMyRealm
			&&	server_itr->first.nChannel == nMyChannel
			) 
		{
			SServerBalance* pkBalance = new_tr SServerBalance;
			if ( pkBalance )
			{
				pkBalance->Set(server_itr->first);
				m_kContServerBalance.push_back(pkBalance);
			}
			else
			{
				VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("Memory New Error!!!") );
			}	
		}
	}

	// 맵서버 Config 에서 ServerIdentity 로 만들어 놓는다.
	CONT_MAP_CONFIG::const_iterator map_cfg_itr = kContMapCfg_Static.begin();
	for ( ; map_cfg_itr != kContMapCfg_Static.end(); ++map_cfg_itr )
	{
		if (	map_cfg_itr->nRealmNo == nMyRealm 
			&&	map_cfg_itr->nChannelNo == nMyChannel)
		{
			CONT_DEFMAP::const_iterator defmap_itr = pkContDefMap->find( map_cfg_itr->iGroundNo );
			if ( defmap_itr != pkContDefMap->end() )
			{
				switch ( defmap_itr->second.iAttr )
				{
				case GATTR_INSTANCE:
				case GATTR_PVP:
				case GATTR_BOSS:
				case GATTR_EMPORIABATTLE:
				case GATTR_EMPORIA:
				case GATTR_MYHOME:
				case GATTR_HOMETOWN:
				case GATTR_HARDCORE_DUNGEON:
				case GATTR_HARDCORE_DUNGEON_BOSS:
					{
						SERVER_IDENTITY kSI;
						kSI.nRealm = map_cfg_itr->nRealmNo;
						kSI.nChannel = map_cfg_itr->nChannelNo;
						kSI.nServerNo = map_cfg_itr->nServerNo;
						kSI.nServerType = CEL::ST_MAP;//맵은 강제

						CONT_GROUND2SERVER_BALANCE::_Pairib ret = m_kContGround2ServerBalance.insert( std::make_pair(map_cfg_itr->iGroundNo, CONT_GROUND2SERVER_BALANCE::mapped_type()));

						SServerBalance* pkBalance = GetServerBalance(kSI);
						if ( pkBalance )
						{
							if( !(ret.first->second.insert(pkBalance).second) )//Set 에 넣은결과
							{
								VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("GroundNo[") << map_cfg_itr->iGroundNo << _T("]'s Server SameKey Used {Realm[") << kSI.nRealm << _T("] Channel[") << kSI.nChannel << _T("] ServerNo[") << kSI.nServerNo << _T("]") );
							}
						}
						else
						{
							INFO_LOG( BM::LOG_LV0, __FL__ << _T("Not Found Ground[") << map_cfg_itr->iGroundNo << _T("]'s Server{Realm[") << kSI.nRealm << _T("] Channel[") << kSI.nChannel << _T("] ServerNo[") << kSI.nServerNo << _T("]}") );
						}
					}break;
				case GATTR_MISSION:
				case GATTR_CHAOS_MISSION:
					{
						VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("MissionGround Regist at MissionNo!! plz.. GroundNo[") << map_cfg_itr->iGroundNo << _T("] {Realm[") << map_cfg_itr->nRealmNo << _T("] Channel[") << map_cfg_itr->nChannelNo << _T("] ServerNo[") << map_cfg_itr->nServerNo << _T("]}") );
					}break;
				case GATTR_DEFAULT:
				case GATTR_VILLAGE:
				case GATTR_HIDDEN_F:
				case GATTR_CHAOS_F:
				case GATTR_STATIC_DUNGEON:
				case GATTR_MARRY:
				case GATTR_BATTLESQUARE:
				case GATTR_SUPER_GROUND:
					{

					}break;
				default:
					{
						VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("Unknown Ground[") << map_cfg_itr->iGroundNo << _T("] Attr[") << defmap_itr->second.iAttr << _T("] {Realm[") << map_cfg_itr->nRealmNo << _T("] Channel[") << map_cfg_itr->nChannelNo << _T("] ServerNo[") << map_cfg_itr->nServerNo << _T("]}") );
					}break;
				}	
			}
			else
			{
				VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("GroundNo[") << map_cfg_itr->iGroundNo << _T("] Is Bad {Realm[") << map_cfg_itr->nRealmNo << _T("] Channel[") << map_cfg_itr->nChannelNo << _T("] ServerNo[") << map_cfg_itr->nServerNo << _T("]}") );
			}
		}
	}

	// 미션맵
	map_cfg_itr = kContMapCfg_Mission.begin();
	for ( ; map_cfg_itr != kContMapCfg_Mission.end(); ++map_cfg_itr )
	{
		if (	map_cfg_itr->nRealmNo == nMyRealm 
			&&	map_cfg_itr->nChannelNo == nMyChannel)
		{
			CONT_GROUND2SERVER_BALANCE::_Pairib ret = m_kContGround2ServerBalance.insert( std::make_pair(map_cfg_itr->iGroundNo, CONT_GROUND2SERVER_BALANCE::mapped_type()));

			SERVER_IDENTITY kSI;
			kSI.nRealm = map_cfg_itr->nRealmNo;
			kSI.nChannel = map_cfg_itr->nChannelNo;
			kSI.nServerNo = map_cfg_itr->nServerNo;
			kSI.nServerType = CEL::ST_MAP;//맵은 강제

			SServerBalance* pkBalance = GetServerBalance(kSI);
			if ( pkBalance )
			{
				if(!(*ret.first).second.insert(pkBalance).second)//Set 에 넣은결과
				{
					VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("MissionNo[") << map_cfg_itr->iGroundNo << _T("]'s Server SameKey Used {Realm[") << kSI.nRealm << _T("] Channel[") << kSI.nChannel << _T("] ServerNo[") << kSI.nServerNo << _T("]}") );
				}
			}
			else
			{
				INFO_LOG( BM::LOG_LV0, __FL__ << _T("Not Found MissionNo[") << map_cfg_itr->iGroundNo << _T("]'s Server{Realm[") << kSI.nRealm << _T("] Channel[") << kSI.nChannel << _T("] ServerNo[") << kSI.nServerNo << _T("]}") );
			}
		}
	}
}

void PgServerSetMgr::Locked_ShutDown(void)
{
	BM::CAutoMutex kLock(m_kMutex, true);
	m_bShutDownServer = true;

	CONT_SERVER_HASH kCont;
	g_kProcessCfg.Locked_GetServerInfo( CEL::ST_MAP, kCont );

	CONT_SERVER_HASH::const_iterator server_itr = kCont.begin();
	for ( ; server_itr != kCont.end() ; ++server_itr )
	{
		SERVER_IDENTITY const &kSI = server_itr->first;
		if(		g_kProcessCfg.ServerIdentity().nRealm == kSI.nRealm
			&&	g_kProcessCfg.ServerIdentity().nChannel == kSI.nChannel
			)
		{
			BM::CPacket kPacket(PT_MCTRL_MMC_A_NFY_SERVER_COMMAND);
			kPacket.Push(MCC_Shutdown_Service);
			kSI.WriteToPacket(kPacket);

			g_kProcessCfg.Locked_SendToServer( kSI, kPacket );
		}
	}
}

void PgServerSetMgr::Locked_CheckShutDown(void)
{
	BM::CAutoMutex kLock(m_kMutex, true);
	if ( true == m_bShutDownServer )
	{
		size_t const iRemainUserCount = m_kContPlayer_MemberKey.size();
		if ( iRemainUserCount > 0 )
		{
			INFO_LOG( BM::LOG_LV5, L"Server Terminate Wait Remain User Count : " << iRemainUserCount );
		}
		else
		{
			INFO_LOG( BM::LOG_LV6, _T("===========================================================") );
			INFO_LOG( BM::LOG_LV6, _T("[CenterServer] will be shutdown") );
			INFO_LOG( BM::LOG_LV6, _T("\tIt takes some times depens on system....WAITING...") );
			INFO_LOG( BM::LOG_LV6, _T("===========================================================") );
			g_kConsoleCommander.StopSignal(true);
			INFO_LOG( BM::LOG_LV6, _T("=== Shutdown END ====") );
		}
	}
}

HRESULT PgServerSetMgr::Locked_OnGreetingServer(SERVER_IDENTITY const &kRecvSI, CEL::CSession_Base *pkSession)
{
	BM::CAutoMutex kLock(m_kMutex, true);
	if ( kRecvSI.nServerType == CEL::ST_MAP )
	{
		SServerBalance* pkBalance = GetServerBalance(kRecvSI);
		if ( pkBalance )
		{
			pkBalance->Live(true);
		}
		else
		{
			VERIFY_INFO_LOG( false, BM::LOG_LV1, __FL__ << _T("Not Found Realm[") << kRecvSI.nRealm << _T("] Channel[") << kRecvSI.nChannel << _T("] ServerNo[") << kRecvSI.nServerNo << _T("]") );
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return E_FAIL"));
			return E_FAIL;
		}
	}
	return S_OK;
}

HRESULT PgServerSetMgr::GroundLoadBalance( SGroundKey const &kKey, SERVER_IDENTITY &kOutSI )const
{
	if ( IsAliveGround(kKey) )
	{
		// 이미 존재하는 그라운드다.
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return S_FALSE"));
		return S_FALSE;
	}

	CONT_GROUND2SERVER_BALANCE::const_iterator gnd_svr_itr = m_kContGround2ServerBalance.find(kKey.GroundNo());
	SServerBalance* pServerBalance = NULL;

	if( gnd_svr_itr != m_kContGround2ServerBalance.end())
	{
		CONT_GROUND2SERVER_BALANCE::mapped_type const &kElement = gnd_svr_itr->second;

		CONT_GROUND2SERVER_BALANCE::mapped_type::const_iterator candi_itor = kElement.begin();
		while(candi_itor != kElement.end())
		{
			if ( !pServerBalance || (pServerBalance->Point() > (*candi_itor)->Point()) )
			{
				pServerBalance = (*candi_itor);
			}
			++candi_itor;
		}

		if( pServerBalance )
		{
			pServerBalance->Get(kOutSI);
			return S_OK;
		}
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return E_FAIL"));
	return E_FAIL;
}

HRESULT PgServerSetMgr::Locked_GroundLoadBalance( SGroundKey const &kKey, SERVER_IDENTITY &kOutSI )const
{//어디에다 만들어야 하는지.
	BM::CAutoMutex kLock( m_kMutex, false );
	return GroundLoadBalance( kKey, kOutSI );
}

HRESULT PgServerSetMgr::Locked_GetPlayerInfo(BM::GUID const &rkGuid, bool const bIsMemberGuid, SContentsUser &rkOut) const
{
	BM::CAutoMutex kLock(m_kMutex);
	return GetPlayerInfo(rkGuid, bIsMemberGuid, rkOut);
}

HRESULT PgServerSetMgr::GetPlayerInfo(BM::GUID const &rkGuid, bool const bIsMemberGuid, SContentsUser &rkOut) const
{
	if( bIsMemberGuid )
	{
		CONT_CENTER_PLAYER_BY_KEY::const_iterator iter = m_kContPlayer_MemberKey.find(rkGuid);
		if( m_kContPlayer_MemberKey.end() != iter )
		{
			CONT_CENTER_PLAYER_BY_KEY::mapped_type const pkElement = (*iter).second;
			if( pkElement)
			{
				pkElement->CopyTo(rkOut);
				return S_OK;
			}
		}
	}
	else
	{
		CONT_CENTER_PLAYER_BY_KEY::const_iterator iter = m_kContPlayer_CharKey.find(rkGuid);
		if( m_kContPlayer_CharKey.end() != iter )
		{
			CONT_CENTER_PLAYER_BY_KEY::mapped_type const pkElement = (*iter).second;
			if( pkElement)
			{
				pkElement->CopyTo(rkOut);
				return S_OK;
			}
		}
	}

	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return E_FAIL"));
	return E_FAIL;
}

HRESULT PgServerSetMgr::Locked_GetPlayerInfo(std::wstring const &rkCharName, SContentsUser &rkOut) const
{
	BM::CAutoMutex kLock(m_kMutex);
	CONT_CENTER_PLAYER_BY_ID::const_iterator iter = m_kContPlayer_CharName.find( rkCharName );
	if( m_kContPlayer_CharName.end() != iter)
	{
		CONT_CENTER_PLAYER_BY_KEY::mapped_type const pkElement = (*iter).second;
		if( pkElement)
		{
			pkElement->CopyTo(rkOut);
			return S_OK;
		}
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return E_FAIL"));
	return E_FAIL;
}

/*
HRESULT PgServerSetMgr::Locked_GetPlayerInfo_OtherChannel(std::wstring const &rkCharName, SContentsUser &rkOut) const
{
	BM::CAutoMutex kLock(m_kMutex);
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return E_FAIL"));
	return E_FAIL;
}
*/

/*
void PgServerSetMgr::Locked_WriteToPacket_User(BM::CPacket& rkPacket, EServerSetMgrPacketOp const eOp)const
{
}
*/

size_t PgServerSetMgr::Locked_CurrentPlayerCount()const
{
	BM::CAutoMutex kLock(m_kMutex);
	int iHomeSize = 0;
	CONT_CENTER_PLAYER_BY_KEY::const_iterator itor_pl = m_kContPlayer_MemberKey.begin();
	while (m_kContPlayer_MemberKey.end() != itor_pl)
	{
		if (NULL != (*itor_pl).second )
		{
			EUnitType const eType = (*itor_pl).second->UnitType();
			if (eType == UT_MYHOME)
			{
				++iHomeSize;	
			}
		}
		++itor_pl;
	}
	return m_kContPlayer_MemberKey.size() - iHomeSize;
}

/*
void PgServerSetMgr::Locked_RecvRealmUserMgrPacket(CEL::CSession_Base *pkSession, BM::CPacket * const pkPacket)
{
}
*/

bool PgServerSetMgr::Locked_GetRealmUserByCharGuid(BM::GUID const &rkCharGuid, SRealmUserInfo& rkRealmInfo)const
{
	BM::CAutoMutex kLock(m_kMutex);
	return GetRealmUserByCharGuid(rkCharGuid, rkRealmInfo);
}

bool PgServerSetMgr::GetRealmUserByCharGuid(BM::GUID const &rkCharGuid, SRealmUserInfo& rkRealmInfo)const
{
	CONT_CENTER_PLAYER_BY_KEY::const_iterator itr = m_kContPlayer_CharKey.find( rkCharGuid );
	if ( itr != m_kContPlayer_CharKey.end() )
	{
		PgPlayer *pkPlayer = itr->second;
		rkRealmInfo.Set( *pkPlayer );
		return true;
	}

	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgServerSetMgr::Locked_GetRealmUserByMemGuid(BM::GUID const &rkMemberGuid, SRealmUserInfo& rkRealmInfo)const
{
	BM::CAutoMutex kLock(m_kMutex);
	return GetRealmUserByMemGuid( rkMemberGuid, rkRealmInfo);
}

bool PgServerSetMgr::GetRealmUserByMemGuid(BM::GUID const &rkMemberGuid, SRealmUserInfo& rkRealmInfo)const
{
	CONT_CENTER_PLAYER_BY_KEY::const_iterator itr = m_kContPlayer_MemberKey.find( rkMemberGuid );
	if ( itr != m_kContPlayer_MemberKey.end() )
	{
		PgPlayer *pkPlayer = itr->second;
		rkRealmInfo.Set( *pkPlayer );
		return true;
	}

	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgServerSetMgr::Locked_GetRealmUserByNickname(std::wstring const& wstrNickname, SRealmUserInfo& rkRealmInfo)const
{
	BM::CAutoMutex kLock(m_kMutex);

	CONT_CENTER_PLAYER_BY_ID::const_iterator itr = m_kContPlayer_CharName.find( wstrNickname );
	if ( itr != m_kContPlayer_CharName.end() )
	{
		PgPlayer *pkPlayer = itr->second;
		rkRealmInfo.Set( *pkPlayer );
		return true;
	}

	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

void PgServerSetMgr::Locked_Recv_PT_N_T_NFY_SELECT_CHARACTER( BM::CPacket * const pkPacket )
{
	BM::CAutoMutex kLock( m_kMutex, true );

	SReqMapMove_MT kRMM;
	pkPacket->Pop(kRMM);

	UNIT_PTR_ARRAY kUnitArray;
	EWRITETYPE const eWriteType = kUnitArray.ReadFromPacket( *pkPacket );

	CONT_PET_MAPMOVE_DATA kContPetMapMoveData;
	PU::TLoadTable_AM( *pkPacket, kContPetMapMoveData );

	CONT_PLAYER_MODIFY_ORDER kContModifyItemOrder;
	kContModifyItemOrder.ReadFromPacket( *pkPacket );

	UNIT_PTR_ARRAY::iterator unit_itr = kUnitArray.begin();
	for ( ; unit_itr!=kUnitArray.end() ; ++unit_itr )
	{
		PgPlayer *pkPlayer = dynamic_cast<PgPlayer*>(unit_itr->pkUnit);
		if ( pkPlayer )
		{
			CONT_PET_MAPMOVE_DATA::mapped_type const *pkPetMapMoveData = NULL;
			CONT_PET_MAPMOVE_DATA::const_iterator pet_itr = kContPetMapMoveData.find( pkPlayer->SelectedPetID() );
			if ( pet_itr != kContPetMapMoveData.end() )
			{
				pkPetMapMoveData = &(pet_itr->second);
			}

			if ( true == Recv_PT_N_T_NFY_SELECT_CHARACTER( pkPlayer, kRMM, eWriteType, pkPetMapMoveData, kContModifyItemOrder ) )
			{
				// 첫번째 놈한테만 kContModifyItemOrder를 보내고 나머지한테는 보내지 말아야 한다.
				kContModifyItemOrder.clear(); // 그러니까 이코드는 맞다
				unit_itr->bAutoRemove = false;// AutoRemove안되게해야한다.
			}
		}
		else
		{
			VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("Unit[") << unit_itr->pkUnit->Name().c_str() << _T("-") << unit_itr->pkUnit->GetID().str().c_str() << _T("] Not PgPlayer") );
		}
	}
}

bool PgServerSetMgr::InsertPlayer(PgPlayer *pkPlayer)
{
	CONT_CENTER_PLAYER_BY_KEY::_Pairib kGuidRet = m_kContPlayer_MemberKey.insert( std::make_pair(pkPlayer->GetMemberGUID(), pkPlayer) );
	if ( kGuidRet.second )
	{
		kGuidRet = m_kContPlayer_CharKey.insert( std::make_pair(pkPlayer->GetID(), pkPlayer) );
		if ( kGuidRet.second )
		{
			CONT_CENTER_PLAYER_BY_ID::_Pairib kStringRet = m_kContPlayer_MemberID.insert( std::make_pair(pkPlayer->MemberID(), pkPlayer) );
			if ( kStringRet.second )
			{
				kStringRet = m_kContPlayer_CharName.insert(std::make_pair(pkPlayer->Name(), pkPlayer));
				return kStringRet.second;
			}
		}
		RemovePlayer( pkPlayer );
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgServerSetMgr::RemovePlayer(PgPlayer *pkPlayer)
{
	size_t iCount = m_kContPlayer_MemberKey.erase(pkPlayer->GetMemberGUID());
	iCount += m_kContPlayer_CharKey.erase(pkPlayer->GetID());
	iCount += m_kContPlayer_MemberID.erase(pkPlayer->MemberID());
	iCount += m_kContPlayer_CharName.erase(pkPlayer->Name());

	RemoveMapMoveLock(pkPlayer->GetID());
	return iCount == 4;
}

bool PgServerSetMgr::Recv_PT_N_T_NFY_SELECT_CHARACTER(PgPlayer *pkPlayer, SReqMapMove_MT & rkRMM, EWRITETYPE const eWriteType, CONT_PET_MAPMOVE_DATA::mapped_type const * const pkPetMapMoveData, CONT_PLAYER_MODIFY_ORDER &kContModifyItemOrder )
{
	if (pkPlayer == NULL)
	{
		VERIFY_INFO_LOG( false, BM::LOG_LV5, __FL__ << _T("player is NULL") );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkPlaer is NULL"));
		return false;
	}

	if ( true == m_bShutDownServer )
	{
		CAUTION_LOG( BM::LOG_LV0, __FL__ << _T("ShutDownServer Error [") << pkPlayer->Name() << _T("-") << pkPlayer->GetID() << _T("]") );
		return false;
	}

	RemoveMapMoveLock(pkPlayer->GetID());
	
	if( !InsertPlayer(pkPlayer) )
	{
		//VERIFY_INFO_LOG( false, BM::LOG_LV0, _T("[%s] Insert Error [%s-%s]"), __FUNCTIONW__, pkNew->Name().c_str(), pkNew->GetID().str().c_str() );
		INFO_LOG( BM::LOG_LV0, __FL__ << _T("Insert Error [") << pkPlayer->Name().c_str() << _T("-") << pkPlayer->GetID().str().c_str() << _T("]") );
		CAUTION_LOG( BM::LOG_LV0, __FL__ << _T("Insert Error [") << pkPlayer->Name().c_str() << _T("-") << pkPlayer->GetID().str().c_str() << _T("]") );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("InsertPlayer is Failed!"));
		return false;//실패.
	}

	if ( pkPlayer->HaveParty() )
	{
		if ( g_kProcessCfg.IsPublicChannel() )
		{
			BM::CPacket kPartyPacket( PT_T_T_SYNC_USER_PARTY, pkPlayer->GetID() );
			kPartyPacket.Push( pkPlayer->GetChannel() );
			::SendToGlobalPartyMgr( kPartyPacket );
		}
// 		else
// 		{
// 			INFO_LOG( BM::LOG_LV5, _T("[Recv_SELECT_CHARACTER] Player<") << pkPlayer->Name() << _T("/") << pkPlayer->GetID() << _T("> Have PartyGuid<") << pkPlayer->PartyGuid() << _T(">") );
// 			pkPlayer->PartyGuid( BM::GUID::NullData() );
// 		}
	}

	UNIT_PTR_ARRAY kUnitArray;
	kUnitArray.Add( pkPlayer );
	kUnitArray.SetWriteType(eWriteType);

	CONT_PET_MAPMOVE_DATA kContPetMapMoveData;
	if ( pkPetMapMoveData )
	{
		kContPetMapMoveData.insert( std::make_pair( pkPlayer->SelectedPetID(), *pkPetMapMoveData ) );
	}

	if ( !ReqMapMove( kUnitArray, rkRMM, kContPetMapMoveData, kContModifyItemOrder ) )
	{
		BM::CPacket kNPacket( PT_T_N_NFY_SELECT_CHARACTER_FAILED, pkPlayer->GetID() );
		SendToContents( kNPacket );

		BM::CPacket kPacket( PT_T_C_ANS_SELECT_CHARACTER, E_SCR_LASTVILLAGE_ERROR );
		kPacket.Push( rkRMM.kTargetKey.GroundNo() );
		SendToUser( pkPlayer->GetMemberGUID(), kPacket );

		RemovePlayer(pkPlayer);
		return false;
	}
	return true;
}

bool PgServerSetMgr::Locked_Recv_PT_N_T_RES_MAP_MOVE( BM::CPacket * const pkPacket )
{
	BM::CAutoMutex kLock(m_kMutex, true);
	return RecvMapMove(pkPacket, true);
}

void PgServerSetMgr::Recv_PT_M_T_ANS_MAP_MOVE_TARGET_FAILED( BM::CPacket * const pkPacket )const
{
	BM::GUID kReqCharGuid;
	BM::GUID kTargetCharGuid;
//	bool bGMCommand = false;
//	T_GNDATTR kGndAttr = GATTR_DEFAULT;
	pkPacket->Pop( kReqCharGuid );
	pkPacket->Pop( kTargetCharGuid );
//	pkPacket->Pop( bGMCommand );
//	pkPacket->Pop( kGndAttr );

	BM::CPacket kAnsPacket( PT_T_C_ANS_MAP_MOVE_TARGET_FAILED, *pkPacket );

	SRealmUserInfo kRealmUserInfo;
	if ( true == Locked_GetRealmUserByCharGuid( kTargetCharGuid, kRealmUserInfo ) )
	{
		kAnsPacket.Push( true );
		kAnsPacket.Push( kRealmUserInfo.wName );
	}
	else
	{
		kAnsPacket.Push( false );
	}

	Locked_SendToUser( kReqCharGuid, kAnsPacket, false );
}

bool PgServerSetMgr::InsertMapMoveLock(BM::GUID const& rkMoveGuid, BM::GUID const& rkCharacterGuid)
{
	CONT_PLAYER_MAPMOVE_LOCK::_Pairib ibRet = m_kPlayerMapMoveLock.insert(std::make_pair(rkCharacterGuid, rkMoveGuid));
	return ibRet.second;
}

void PgServerSetMgr::RemoveMapMoveLock(BM::GUID const& rkCharacterGuid)
{
	m_kPlayerMapMoveLock.erase(rkCharacterGuid);
}

bool PgServerSetMgr::GetMapMoveLock(BM::GUID const& rkCharacterGuid, BM::GUID& rkOutLock)
{
	CONT_PLAYER_MAPMOVE_LOCK::const_iterator itor_lock = m_kPlayerMapMoveLock.find(rkCharacterGuid);
	if (itor_lock != m_kPlayerMapMoveLock.end())
	{
		rkOutLock = itor_lock->second;
		return true;
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgServerSetMgr::Locked_Recv_PT_I_M_REQ_HOME_CREATE(BM::CPacket * pkPacket)
{
	BM::CAutoMutex kLock(m_kMutex, true);

	SReqMapMove_MT kRMM;
	pkPacket->Pop(kRMM);
	PgMyHome kMyHome;
	kMyHome.ReadFromPacket(*pkPacket);

	PgMyHome * pkMyHome = dynamic_cast<PgMyHome*>(g_kTotalObjMgr.CreateUnit(UT_MYHOME, kMyHome.GetID() ) );
	if ( pkMyHome )
	{
		BM::CPacket kPacket;
		kMyHome.WriteToPacket(kPacket);
		pkMyHome->ReadFromPacket(kPacket);
		if(true == InsertPlayer(pkMyHome))
		{
			BM::CPacket kPacket(PT_I_M_REQ_HOME_CREATE);
			kMyHome.WriteToPacket(kPacket);
			return SendToGround(kRMM.kTargetKey,kPacket,true);
		}
		g_kTotalObjMgr.ReleaseUnit( dynamic_cast<CUnit*>(pkMyHome) );
	}

	return false;
}

bool PgServerSetMgr::Locked_Recv_PT_I_M_REQ_HOME_DELETE( BM::CPacket * const pkPacket  )
{
	BM::CAutoMutex kLock(m_kMutex, true);

	BM::GUID kMemberGuid;
	pkPacket->Pop( kMemberGuid );

	CONT_CENTER_PLAYER_BY_KEY::iterator login_itr = m_kContPlayer_MemberKey.find( kMemberGuid );
	if ( login_itr != m_kContPlayer_MemberKey.end() )
	{
		CONT_CENTER_PLAYER_BY_KEY::mapped_type pCData = login_itr->second;
		if ( pCData )
		{
			RemovePlayer(pCData);
			g_kTotalObjMgr.ReleaseUnit(pCData);
			return true;
		}
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgServerSetMgr::Locked_Recv_PT_M_M_UPDATE_PLAYERPLAYTIME( BM::CPacket * const pkPacket )
{
	SGroundKey kGroundKey;
	BM::GUID kCharGuid;
	int iAccConSec = 0;
	int iAccDicSec = 0;
	__int64 i64SelectSec = 0;

	pkPacket->Pop(kGroundKey);
	pkPacket->Pop(kCharGuid);
	pkPacket->Pop(iAccConSec);
	pkPacket->Pop(iAccDicSec);
	pkPacket->Pop(i64SelectSec);


	BM::CPacket kPacket(PT_M_M_UPDATE_PLAYERPLAYTIME);
	kPacket.Push(kGroundKey);
	kPacket.Push(kCharGuid);
	kPacket.Push(iAccConSec);
	kPacket.Push(iAccDicSec);
	kPacket.Push(i64SelectSec);
	SendToServerType(CEL::ST_MAP, kPacket);


	BM::CAutoMutex kLock(m_kMutex, true);

	CONT_CENTER_PLAYER_BY_KEY::iterator login_itr = m_kContPlayer_CharKey.find( kCharGuid );
	if ( login_itr != m_kContPlayer_CharKey.end() )
	{
		CONT_CENTER_PLAYER_BY_KEY::mapped_type pCData = login_itr->second;
		if ( pCData )
		{
			pCData->SetPlayTime(iAccConSec,iAccDicSec);
			pCData->SetSelectCharacterSec(i64SelectSec);
			return true;
		}
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}
