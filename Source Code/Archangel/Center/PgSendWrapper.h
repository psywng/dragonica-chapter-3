#ifndef CENTER_CENTERSERVER_NETWORK_PGSENDWRAPPER_H
#define CENTER_CENTERSERVER_NETWORK_PGSENDWRAPPER_H

extern bool SetSendWrapper(SERVER_IDENTITY const &kRecvSI);

extern bool SendToServerType(CEL::E_SESSION_TYPE const eServerType, BM::CPacket const &rkPacket);
extern bool SendToServer(SERVER_IDENTITY const &kSI, BM::CPacket const &rkPacket);
extern bool SendToUser( BM::GUID const &kMemberGuid, SERVER_IDENTITY const &kSwitchSI, BM::CPacket const &rkPacket );

extern bool SendToImmigration(BM::CPacket const &rkPacket);
extern bool SendToManagementServer(BM::CPacket const &rkPacket);
extern bool SendToLog(BM::CPacket const &rkPacket);
extern bool SendToChannelContents( EContentsMessageType const eType, BM::CPacket const &rkPacket, int const iSecondType = 0 );
extern bool SendToOtherChannelContents( short const nTargetChannel, EContentsMessageType const eType, BM::CPacket const &rkPacket, int const iSecondType = 0 );
extern bool SendToRealmContents( EContentsMessageType eType, BM::CPacket const &rkPacket );
extern bool SendToItem(SERVER_IDENTITY const &kSI, SGroundKey const &kGndKey, BM::CPacket const &rkPacket);

extern bool SendToCenter(BM::CPacket const &rkPacket);
extern bool SendToContents( BM::CPacket const &rkPacket );
extern bool SendToChannelGround( short const nTargetChannel, SGroundKey const &kTargetGndKey, BM::CPacket const &rkPacket, bool const bIsGndWrap = true );

extern bool SendToGlobalPartyMgr(BM::CPacket const &rkPacket);
extern bool SendToPvPLobby(BM::CPacket const &rkPacket, int const iLobbyID);
extern bool SendToMissionMgr(BM::CPacket const &rkPacket);
extern bool SendToPortalMgr(BM::CPacket const &rkPacket);
extern bool SendToFriendMgr(BM::CPacket const &rkPacket);
extern bool SendToGuildMgr(BM::CPacket const &rkPacket);
extern bool SendToChannelChatMgr(BM::CPacket const &rkPacket);
extern bool SendToRealmChatMgr(BM::CPacket const &rkPacket);
extern bool SendToCoupleMgr(BM::CPacket const &rkPacket);
extern bool SendToRankMgr(BM::CPacket const &rkPacket);
extern bool SendToHardCoreDungeonMgr( BM::CPacket const &rkPacket );

#endif // CENTER_CENTERSERVER_NETWORK_PGSENDWRAPPER_H