#include "stdafx.h"

#include "PgKeyEvent.h"
#include "Variant/PgDBCache.h"
#include "PgRecvFromServer.h"
#include "PgRecvFromItem.h"
//#include "PgGMProcessMgr.h"
#include "Variant/PgEventview.h"
#include "Lohengrin/PgRealmManager.h"
#include "PgLChannelMgr.h"
//#include "PgIBMgr.h"
#include "Variant/PgEventView.h"
#include "Contents/PgResultMgr.h"
#include "PgTimer.h"
#include "PgPvPLobbyMgr.h"
#include "PgPortalMgr.h"
#include "PgGlobalMarryMgr.h"

//	1 seconds
void CALLBACK Timer1s(DWORD dwUserData)
{
	//std::cout << __FUNCTION__ << std::endl;
	g_kTotalObjMgr.ReleaseFlush();

	g_kEventView.ProcessEvent();
	DWORD const dwCurTime = BM::GetTime32();
	g_kPortalMgr.OnTick( dwCurTime );
	g_kResultMgr.Tick(dwCurTime);
	g_kMarryMgr.Tick();
}

void CALLBACK TimerCheckShutDown(DWORD dwUserData)
{
	g_kServerSetMgr.Locked_CheckShutDown();
}

void CALLBACK Timer30s(DWORD dwUserData)
{
	static Loki::Mutex s_UserCounterLogMutex;
	static DWORD s_dwUserCounterLogTimer = BM::GetTime32();
	{
		// Timer Thread 가 혹시 두개 이상 될까봐 Thread safe...
		BM::CAutoMutex kLock(s_UserCounterLogMutex);
		if ( BM::TimeCheck( s_dwUserCounterLogTimer, 900000 ) )	// 15분마다 한번씩..
		{
			//서버 접속자 정보 로그
			SERVER_IDENTITY kSI = g_kProcessCfg.ServerIdentity();
			PgLogCont kLogCont(ELogMain_System_User_Count);
			PgLog kLog;
			kLog.Set(0, kSI.nServerType);
			kLog.Set(1, kSI.nServerNo);
			kLog.Set(2, static_cast<int>(g_kServerSetMgr.Locked_CurrentPlayerCount()));
			kLogCont.Add(kLog);
			kLogCont.Commit();

#ifdef _MEMORY_TRACKING
			S_STATE_LOG(BM::LOG_LV0, _T("---Memory Observe---"));
			g_kObjObserver.DisplayState(g_kLogWorker, LT_S_STATE);
			if (g_pkMemoryTrack)
			{
				g_pkMemoryTrack->DisplayState(g_kLogWorker, LT_S_STATE);
			}
#endif
		}
	}
	// 현재 Channel 접속자수를 보낸다. (Center/Login)
	if ( !g_kProcessCfg.IsPublicChannel() )
	{
		UpdateCurrentUserCount();
	}
}

//	1 minute
void CALLBACK Timer1m(DWORD dwUserData)
{
	//DWORD const dwCurTime = BM::GetTime32();

	//static DWORD dwEventCheckTime = 0;
	//if(BM::TimeCheck(dwEventCheckTime, 30000))
	//{
	//	CEL::DB_QUERY kQuery( DT_PLAYER, DQT_USER_EVENT, _T("EXEC [dbo].[UP_LoadEvent]"));
	//	g_kCoreCenter.PushQuery(kQuery);
	//}
}

void CALLBACK TimerPvP(DWORD dwUserData)
{
	static DWORD dwEventCheckTime = BM::GetTime32();
	if( BM::TimeCheck(dwEventCheckTime, 10000) )
	{
		g_kPvPLobbyMgr.OnTick();
	}
	else
	{
		g_kPvPLobbyMgr.OnTick_Event();
	}

	static DWORD dwLogCheckTime = BM::GetTime32();
	if ( BM::TimeCheck( dwLogCheckTime, 30000 ) )
	{
		g_kPvPLobbyMgr.OnTick_Log();
	}
}


void UpdateCurrentUserCount()
{
	BM::CPacket kUPacket(PT_T_A_NFY_USERCOUNT);

	int const iMax = static_cast<int>( g_kSwitchAssignMgr.MaxPlayerCount() );
//	int const iCurrent = static_cast<int>( g_kSwitchAssignMgr.Locked_CurrentPlayerCount() );
	int const iCurrent = static_cast<int>( g_kSwitchAssignMgr.NowPlayerCount() );// Switch Assign Mgr에서 보내야 한다.(공용채널의 유저수가 여기로 카운팅이 되어야함)

	g_kRealmMgr.UpdateUserCount(g_kProcessCfg.RealmNo(), g_kProcessCfg.ChannelNo(), iMax, iCurrent);
	kUPacket.Push(g_kProcessCfg.RealmNo());
	kUPacket.Push(g_kProcessCfg.ChannelNo());
	kUPacket.Push(iMax);
	kUPacket.Push(iCurrent);

	SERVER_IDENTITY kSI;
	kSI.nServerType = CEL::ST_LOGIN;
	BM::CPacket kLPacket(PT_A_A_WRAPPER, kSI);
	kLPacket.Push(kUPacket);
	::SendToImmigration(kLPacket);

	kSI.nServerType = CEL::ST_CONTENTS;
	BM::CPacket kTPacket(PT_A_A_WRAPPER, kSI);
	kTPacket.PosAdjust();
	kTPacket.Push(kUPacket);
	::SendToImmigration(kTPacket);
}
