#include "stdafx.h"
#include "BM/PgFilterString.h"
#include "Lohengrin/PacketStruct.h"
#include "Collins/Log.h"
#include "Variant/PgParty.h"
#include "Variant/PgPartyMgr.h"
#include "Variant/PgLogUtil.h"
#include "Variant/PgHardCoreDungeonParty.h"
#include "global.h"
#include "PgTask_Contents.h"
#include "PgGlobalPartyMgr.h"


namespace PgGlobalPartyMgrUtil
{
	bool IsCanWarpGround( SChnGroundKey const& rkGndKey )
	{
		CONT_DEFMAP const* pkDefMap = NULL;
		g_kTblDataMgr.GetContDef(pkDefMap);

		if( !pkDefMap )
		{
			return false;
		}

		CONT_DEFMAP::const_iterator find_iter = pkDefMap->find(rkGndKey.GroundNo());
		if( pkDefMap->end() == find_iter )
		{
			return false;
		}

		if ( g_kProcessCfg.ChannelNo() != rkGndKey.Channel() )
		{
			return false;
		}

		bool bCant = false;
		if( true == g_kLocal.IsServiceRegion( LOCAL_MGR::NC_TAIWAN ) )
		{
			bCant = (0 != ((*find_iter).second.iAttr&GATTR_FLAG_CANT_PARTYWARP));
		}
		else
		{
			bCant = (0 != ((*find_iter).second.iAttr&GATTR_FLAG_CANT_WARP));
		}

		if( bCant )
		{
			return false;
		}
		return true;
	}
};


int const iPartyNameDefStrNo = 10;//

PgGlobalPartyMgrImpl::PgGlobalPartyMgrImpl()
	: PgPartyMgr::T_MY_BASE_MGR_TYPE(), m_kCharToParty(), m_kContPartyNameSet()
{
}

PgGlobalPartyMgrImpl::~PgGlobalPartyMgrImpl()
{
}

bool PgGlobalPartyMgrImpl::GetCharToParty(BM::GUID const &rkCharGuid, BM::GUID& rkOutGuid) const
{
	ContCharToParty::const_iterator iter = m_kCharToParty.find(rkCharGuid);
	if(m_kCharToParty.end() != iter)
	{
		rkOutGuid = (*iter).second;
		return true;
	}

	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgGlobalPartyMgrImpl::AddCharToParty(BM::GUID const &rkCharGuid, BM::GUID const &rkPartyGuid)
{
	if( BM::GUID::IsNull(rkCharGuid) )
	{
		return false;
	}

	ContCharToParty::_Pairib kRet = m_kCharToParty.insert( std::make_pair(rkCharGuid, rkPartyGuid) );
	return kRet.second;
}

BM::GUID PgGlobalPartyMgrImpl::DelCharToParty(BM::GUID const &rkCharGuid)
{
	BM::GUID kPartyGuid;

	ContCharToParty::iterator iter = m_kCharToParty.find(rkCharGuid);
	if( m_kCharToParty.end() != iter )
	{
		kPartyGuid = iter->second;
		m_kCharToParty.erase(iter);
	}
	else
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return NULLGUID"));
	}
	
	return kPartyGuid;
}

bool PgGlobalPartyMgrImpl::FindCharToParty(BM::GUID const &rkCharGuid) const
{
	if( BM::GUID::IsNull(rkCharGuid) )
	{
		return false;
	}

	ContCharToParty::const_iterator iter = m_kCharToParty.find(rkCharGuid);
	return (m_kCharToParty.end() != iter);
}

PgGlobalParty * PgGlobalPartyMgrImpl::GetCharToParty(BM::GUID const &rkCharGuid )const
{
	BM::GUID kPartyGuid;
	if ( true == GetCharToParty( rkCharGuid, kPartyGuid ) )
	{
		return GetParty( kPartyGuid );
	}

	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return NULL"));
	return NULL;
}

HRESULT PgGlobalPartyMgrImpl::CheckJoinParty(SContentsUser const& rkMaster, SContentsUser const& rkUser)
{
	if( rkMaster.Empty() )//마스터 정보가 NULL
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
		return PRC_Fail;
	}

	if( rkUser.Empty() )//상대편 정보가 NULL
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
		return PRC_Fail;
	}

	{//PvP에 있는 유저라 파티를 가입시킬 수 없다.
		CONT_DEFMAP const *pkDefMap = NULL;
		g_kTblDataMgr.GetContDef(pkDefMap);
		if( !pkDefMap )
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
			return PRC_Fail;
		}
		
		CONT_DEFMAP::const_iterator iter = pkDefMap->find(rkUser.kGndKey.GroundNo());
		if( pkDefMap->end() == iter )
		{
			INFO_LOG(BM::LOG_LV5, __FL__ << _T("Cannot find GroundDef User[") << rkUser.Name() << _T("],GroundNo=") << rkUser.kGndKey.GroundNo());
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
			return PRC_Fail;
		}

		switch ( (*iter).second.iAttr )
		{
		case GATTR_PVP:
			{
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_PVP"));
				return PRC_Fail_PVP;
			}break;
		case GATTR_MISSION:
		case GATTR_CHAOS_MISSION:
			{
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_Area"));
				return PRC_Fail_Area;
			}break;
		case GATTR_EMPORIA:
			{
			}break;
		default:
			{
				if( 0 != ((*iter).second.iAttr & GATTR_FLAG_SUPER) )
				{
					return PRC_Fail_Area;
				}
			}break;
		}
	}

	if( rkMaster.kCharGuid == rkUser.kCharGuid )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_NotInviteMe"));
		return PRC_Fail_NotInviteMe;//나를 초대 하지 마라
	}

	BM::GUID kUserPartyGuid;
	BM::GUID kMasterPartyGuid;
	bool const bUserFindParty = GetCharToParty(rkUser.kCharGuid, kUserPartyGuid);//이미 파티에 가입 되었는가?
	bool const bMasterFindParty = GetCharToParty(rkMaster.kCharGuid, kMasterPartyGuid);//마스터 파티가 있나?
	
	PgGlobalParty* pkParty = GetParty(kMasterPartyGuid);//마스터 파티가 실존 하면

	if( bUserFindParty )//유저가 파티가 없고
	{
		if( kUserPartyGuid == kMasterPartyGuid )
		{
			if( pkParty )
			{
				bool const bIsMember = pkParty->IsMember(rkUser.kCharGuid);
				if( !bIsMember )
				{
					LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_AnswerWait"));
					return PRC_Fail_AnswerWait;
				}
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_MineMember"));
				return PRC_Fail_MineMember;//이미 같은 맴버
			}
		}
		else
		{
			PgGlobalParty* pkUserParty = GetParty(kUserPartyGuid);
			if( pkUserParty )
			{
				bool const bIsMember = pkUserParty->IsMember(rkUser.kCharGuid);
				if( !bIsMember )
				{
					LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_AnswerWait"));
					return PRC_Fail_AnswerWait;
				}				
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_Party"));
				return PRC_Fail_Party;//이미 다른 파티 가입
			}			
			else if( !bMasterFindParty )//마스터에 파티가 없으면
			{
				return PRC_Success_Create;//생성하면 된다
			}
		}
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
		return PRC_Fail;
	}
	else//유저가 파티가 있고
	{
		if( !bMasterFindParty )//마스터에 파티가 없으면
		{
			return PRC_Success_Create;//생성하면 된다
		}
		else//마스터 파티가 있으면
		{
			//다음 검증
		}
	}
	
	if( !pkParty )
	{
		DelCharToParty(rkMaster.kCharGuid);//파티 포인터가 없으면 파티 정보 삭제
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
		return PRC_Fail;
	}

	bool bIsMaster = pkParty->IsMaster(rkMaster.kCharGuid);
	if( !bIsMaster )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_NotMaster"));
		return PRC_Fail_NotMaster;//마스터가 아니면 초대 안돼
	}


	if( rkUser.sLevel < pkParty->Option().GetOptionLevel() )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_Level"));
		return PRC_Fail_Level;// 레벨이 맞지 않음(파티방)
	}


	return PRC_Success;//초대 가능
}

//
bool PgGlobalPartyMgrImpl::ProcessMsg(SEventMessage *pkMsg)
{
	PACKET_ID_TYPE wType = 0;
	pkMsg->Pop(wType);
	switch(wType)
	{
	case PT_M_T_ANS_SUMMONPARTYMEMBER:
		{
			BM::GUID	kOwnerGuid;
			SGroundKey	kCastGndKey;
			HRESULT		hRet = E_FAIL;

			pkMsg->Pop(kOwnerGuid);
			pkMsg->Pop(kCastGndKey);
			pkMsg->Pop(hRet);

			BM::CPacket kPacket(PT_T_M_ANS_SUMMONPARTYMEMBER);
			kPacket.Push(kOwnerGuid);
			kPacket.Push(kCastGndKey);
			kPacket.Push(hRet);
			
			g_kServerSetMgr.Locked_SendToGround(kCastGndKey,kPacket,true);
		}break;
	case PT_M_T_ANS_MOVETOSUMMONER:
		{
			BM::GUID	kOwnerGuid;
			SGroundKey	kCastGndKey;

			pkMsg->Pop(kOwnerGuid);
			pkMsg->Pop(kCastGndKey);

			BM::CPacket kPacket(PT_T_M_ANS_MOVETOSUMMONER);
			kPacket.Push(kOwnerGuid);
			kPacket.Push(kCastGndKey);

			g_kServerSetMgr.Locked_SendToGround(kCastGndKey,kPacket,true);
		}break;
	case PT_M_T_REQ_PARTY_BUFF:
		{
			BM::GUID	kOwnerGuid;
			SGroundKey	kCastGndKey;
			BM::GUID	kPartyGuid;

			pkMsg->Pop(kOwnerGuid);
			pkMsg->Pop(kCastGndKey);

			bool const bIsParty = GetCharToParty(kOwnerGuid, kPartyGuid);
			if( bIsParty )
			{
				PartyBuffRefresh(kOwnerGuid, kPartyGuid);
			}
		}break;
	case PT_M_T_REQ_MOVETOSUMMONER:
		{
			BM::GUID kOwnerGuid,
					 kMemberGuid;

			SGroundKey	kCastGndKey,
						kGndKey;
			SItemPos	kItemPos;

			pkMsg->Pop(kOwnerGuid);
			pkMsg->Pop(kMemberGuid);
			pkMsg->Pop(kCastGndKey);
			pkMsg->Pop(kGndKey);
			pkMsg->Pop(kItemPos);

			BM::CPacket kPacket(PT_T_M_REQ_MOVETOSUMMONER);
			kPacket.Push(kOwnerGuid);
			kPacket.Push(kMemberGuid);
			kPacket.Push(kCastGndKey);
			kPacket.Push(kItemPos);

			g_kServerSetMgr.Locked_SendToGround(kGndKey,kPacket,true);
		}break;
	case PT_M_T_REQ_SUMMONPARTYMEMBER:
		{
			BM::GUID kOwnerGuid,
					 kMemberGuid;

			SGroundKey kCasterGndKey;
			SItemPos kItemPos;
			POINT3 kCasterPos;

			pkMsg->Pop(kOwnerGuid);
			pkMsg->Pop(kMemberGuid);
			pkMsg->Pop(kCasterGndKey);
			pkMsg->Pop(kItemPos);

			SChnGroundKey kChnGndKey;
			if( GetMemberGroundKey( kMemberGuid, kChnGndKey ) )
			{
				if ( g_kProcessCfg.ChannelNo() == kChnGndKey.Channel() )
				{
					BM::CPacket kPacket(PT_T_M_REQ_SUMMONPARTYMEMBER);
					kPacket.Push(kOwnerGuid);
					kPacket.Push(kMemberGuid);
					kPacket.Push(kCasterGndKey);
					kPacket.Push(kItemPos);
					SendToGround( kChnGndKey, kPacket );
				}
				else
				{
					BM::CPacket kPacket(PT_M_T_ANS_SUMMONPARTYMEMBER);
					kPacket.Push(kOwnerGuid);
					kPacket.Push(E_CANNOT_MOVE_MAP);
					g_kServerSetMgr.Locked_SendToGround( kCasterGndKey, kPacket, true );
				}
			}
			else
			{
				BM::CPacket kPacket(PT_M_T_ANS_SUMMONPARTYMEMBER);
				kPacket.Push(kOwnerGuid);
				kPacket.Push(E_NOT_FOUND_MEMBER);
				g_kServerSetMgr.Locked_SendToGround( kCasterGndKey, kPacket, true );
			}
		}break;
	case PT_M_T_REQ_PARTYMEMBERPOS:
		{
			BM::GUID kOwnerGuid,
					 kMemberGuid;

			SGroundKey kCasterGndKey;
			SItemPos kItemPos;
			pkMsg->Pop(kOwnerGuid);
			pkMsg->Pop(kMemberGuid);
			pkMsg->Pop(kCasterGndKey);
			pkMsg->Pop(kItemPos);

			SChnGroundKey kChnGndKey;
			if( GetMemberGroundKey(kMemberGuid,kChnGndKey)
			&&	PgGlobalPartyMgrUtil::IsCanWarpGround(kChnGndKey) )
			{
				BM::CPacket kPacket(PT_T_M_REQ_PARTYMEMBERPOS);
				kPacket.Push(kOwnerGuid);
				kPacket.Push(kMemberGuid);
				kPacket.Push(kCasterGndKey);
				kPacket.Push(kItemPos);
				SendToGround( kChnGndKey, kPacket );
			}
			else
			{
				BM::CPacket kPacket(PT_T_M_ANS_PARTYMEMBERPOS);
				kPacket.Push(kOwnerGuid);
				kPacket.Push( static_cast< HRESULT >(E_CANNOT_STATE_MAPMOVE) );
				g_kServerSetMgr.Locked_SendToGround(kCasterGndKey,kPacket,true);
			}
		}break;
	case PT_C_N_REQ_CREATE_PARTY_2ND: //생성
		{
			SContentsUser kMaster;

			kMaster.ReadFromPacket(*pkMsg);

			HRESULT hCreateRet = ReqCreateParty(kMaster, *pkMsg);
			if( PRC_Success != hCreateRet )
			{
				BM::CPacket kPacket(PT_N_C_NFY_CREATE_PARTY);
				kPacket.Push(hCreateRet);
				g_kServerSetMgr.Locked_SendToUser(kMaster.kMemGuid, kPacket);
			}
		}break;
	case PT_C_N_REQ_PARTY_RENAME:
		{
			BM::GUID kCharGuid;
			pkMsg->Pop( kCharGuid );

			ReqRenameParty( kCharGuid, *pkMsg );
		}break;

	case PT_C_N_REQ_JOIN_PARTY_2ND: //초대
		{
			BM::GUID kPartyGuid;
			SContentsUser kMaster;
			SContentsUser kUser;

			SPartyOption rkOption;
			
			kMaster.ReadFromPacket(*pkMsg);			
			kUser.ReadFromPacket(*pkMsg);

			bool const bIsParty = GetCharToParty(kMaster.kCharGuid, kPartyGuid);
			if( bIsParty )
			{
				PgGlobalParty* pkParty = GetParty(kPartyGuid);
				if( pkParty )
				{
					rkOption = pkParty->Option();
				}
			}
			HRESULT const hReqJoinRet = ReqJoinParty(kMaster, kUser, rkOption);
			if( PRC_Success != hReqJoinRet )
			{
				BM::CPacket kRetPacket(PT_N_C_ANS_JOIN_PARTY_TO_MASTER);
				kRetPacket.Push(hReqJoinRet);
				g_kServerSetMgr.Locked_SendToUser(kMaster.kMemGuid, kRetPacket);//결과

				PgGlobalParty* pkParty = GetParty(kPartyGuid);
				if( pkParty )
				{
					bool const bIsMember = pkParty->IsMember(kUser.kCharGuid);
					bool const bIsDestroy = pkParty->IsDestroy();
					if( bIsDestroy
						&& bIsMember )
					{
						DestroyParty(kPartyGuid);
					}
				}
			}
		}break;
	case PT_C_N_REQ_JOIN_PARTYFIND_2ND: //초대
		{
			BM::GUID kPartyGuid;
			SContentsUser kMaster;
			SContentsUser kUser;

			SPartyOption rkOption;

			kUser.ReadFromPacket(*pkMsg);
			kMaster.ReadFromPacket(*pkMsg);			


			bool const bIsParty = GetCharToParty(kMaster.kCharGuid, kPartyGuid);
			if( bIsParty )
			{
				PgGlobalParty* pkParty = GetParty(kPartyGuid);
				if( pkParty )
				{
					rkOption = pkParty->Option();

					HRESULT const hReqJoinRet = ReqJoinPartyFind(kMaster, kUser, rkOption);
					if( PRC_Success != hReqJoinRet )
					{
						BM::CPacket kRetPacket(PT_N_C_ANS_JOIN_PARTY_TO_MASTER);
						kRetPacket.Push(hReqJoinRet);
						g_kServerSetMgr.Locked_SendToUser(kUser.kMemGuid, kRetPacket);//결과

						PgGlobalParty* pkParty = GetParty(kPartyGuid);
						if( pkParty )
						{
							bool const bIsMember = pkParty->IsMember(kUser.kCharGuid);
							bool const bIsDestroy = pkParty->IsDestroy();
							if( bIsDestroy
								&& bIsMember )
							{
								DestroyParty(kPartyGuid);
							}
						}
					}
				}
			}
		}break;
	case PT_C_N_ANS_JOIN_PARTY_2ND: //초대 응답
		{
			bool bAnsJoin = false;
			BM::GUID kPartyGuid;
			SContentsUser kMaster;
			SContentsUser kUser;

			kUser.ReadFromPacket(*pkMsg);

			if( kUser.Empty() )
			{
				break;
			}

			pkMsg->Pop(bAnsJoin);
			pkMsg->Pop(kPartyGuid);

			HRESULT const hRet = AnsJoinParty(kPartyGuid, kUser, bAnsJoin, false);

			if(	bAnsJoin//허가면서
			&&	PRC_Success != hRet )//성공이 아니면
			{
				BM::CPacket kAnsUserPacket(PT_N_C_ANS_JOIN_PARTY, kPartyGuid);
				kAnsUserPacket.Push(hRet);
				g_kServerSetMgr.Locked_SendToUser(kUser.kMemGuid, kAnsUserPacket);

				if( PRC_Fail_MemberCount == hRet )
				{
					if( kUser.kMemGuid != kMaster.kMemGuid )
					{
						g_kServerSetMgr.Locked_SendToUser(kMaster.kMemGuid, kAnsUserPacket);
					}
				}
			}
		}break;
	case PT_C_N_ANS_JOIN_PARTYFIND_2ND: //초대 응답
		{
			bool bAnsJoin = false;
			BM::GUID kPartyGuid;
			SContentsUser kMaster;
			SContentsUser kUser;

			kUser.ReadFromPacket(*pkMsg);

			pkMsg->Pop(bAnsJoin);
			pkMsg->Pop(kPartyGuid);

			if( kUser.Empty() )
			{
				break;
			}

			HRESULT const hRet = AnsJoinParty(kPartyGuid, kUser, bAnsJoin, false);

			if( hRet == PRC_Fail_Refuse )//거부
			{
				BM::CPacket kRetPacket(PT_N_C_ANS_JOIN_PARTY_TO_MASTER);
				kRetPacket.Push(hRet);
				kRetPacket.Push(kUser.kCharGuid);
				kRetPacket.Push(kUser.kName);
				g_kServerSetMgr.Locked_SendToUser(kUser.kCharGuid, kRetPacket, false);//마스터에게 거부 메시지
			}

			if(	bAnsJoin//허가면서
				&&	PRC_Success != hRet )//성공이 아니면
			{
				BM::CPacket kAnsUserPacket(PT_N_C_ANS_JOIN_PARTY, kPartyGuid);
				kAnsUserPacket.Push(hRet);
				g_kServerSetMgr.Locked_SendToUser(kUser.kMemGuid, kAnsUserPacket);
			}
		}break;
	case PT_A_NFY_USER_DISCONNECT: //탈퇴
	case PT_C_N_REQ_LEAVE_PARTY_2ND:
		{
			SContentsUser kUser;
			kUser.ReadFromPacket(*pkMsg);

			HRESULT const hLeaveRet = LeaveParty( kUser.kCharGuid );
		}break;
	case PT_C_N_REQ_PARTY_CHANGE_OPTION_2ND://파티 옵션 변경
		{
			SContentsUser kUser;
			kUser.ReadFromPacket(*pkMsg);
			ReqChangeOptionParty(kUser, *pkMsg);
		}break;
	case PT_T_N_NFY_USER_ENTER_GROUND://맵이동
		{
			SAnsMapMove_MT kAMM;
			SContentsUser kUser;

			pkMsg->Pop(kAMM);
			kUser.ReadFromPacket(*pkMsg);

			BM::GUID kPartyGuid;
			bool const bIsParty = GetCharToParty(kUser.kCharGuid, kPartyGuid);
			if( bIsParty )
			{
				MovedProcess( kPartyGuid, kUser, g_kProcessCfg.ChannelNo() );
			}
		}break;
	case PT_T_T_NFY_USER_ENTER_GROUND:
		{
			short nChannelNo = 0;
			SContentsUser kUser;
			pkMsg->Pop( nChannelNo );
			kUser.ReadFromPacket(*pkMsg);

			BM::GUID kPartyGuid;
			bool const bIsParty = GetCharToParty(kUser.kCharGuid, kPartyGuid);
			if( bIsParty )
			{
				MovedProcess( kPartyGuid, kUser, nChannelNo );
			}
		}break;
	case PT_A_U_SEND_TOPARTY_BYCHARGUID://파티 Guid로 패킷 전송
		{
			BM::GUID kCharGuid;
			BM::CPacket::PACKET_DATA kData;

			pkMsg->Pop(kCharGuid);
			pkMsg->Pop(kData);

			if ( kData.size() <= sizeof(BM::CPacket::DEF_PACKET_TYPE) )
			{
				VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("PT_A_U_SEND_TOPARTY_BYCHARGUID: Size Error!! Size=") << kData.size() );
			}
			else 
			{
				BM::CPacket kPacket;
				kPacket.Push(&kData.at(0), kData.size()*sizeof(BM::CPacket::PACKET_DATA::value_type));
				SendParty_Packet(kCharGuid, kPacket);
			}
		}break;
	case PT_M_N_NFY_PARTY_USER_PROPERTY_2ND://파티원 HP Update
		{
			BM::GUID kPartyGuid;
			SContentsUser kUser;
			BYTE cPropertyType = 0;
			unsigned short sVal = 0;

			kUser.ReadFromPacket(*pkMsg);

			bool const bIsParty = GetCharToParty(kUser.kCharGuid, kPartyGuid);
			if( bIsParty )
			{
				ChangeMemberAbility(kPartyGuid, kUser, *pkMsg);
			}
		}break;
	case PT_M_N_REQ_PARTY_COMMAND_2ND:
		{

			BYTE cCmdType = 0;
			SContentsUser kUser;
			kUser.ReadFromPacket(*pkMsg);
			pkMsg->Pop(cCmdType);

			BM::GUID kPartyGuid;
			switch(cCmdType)
			{
			case PC_Summon_Member:
				{
					if( GetCharToParty(kUser.kCharGuid, kPartyGuid) )
					{
						SummonMember(kPartyGuid, kUser);
					}
				}break;
			default:
				{
					LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Invalid CaseType"));
				}break;
			}
		}break;
	case PT_A_U_SEND_TOFRIEND_PARTY:
		{
			PartyFriendCheck(pkMsg);
		}break;
	case PT_A_U_SEND_TOFRIEND_PARTYINFO:
		{
			PartyFriendCheckInfo(pkMsg);
		}break;
		//>> For GM Command
	case PT_C_N_REQ_PARTY_RENAME_GM:
		{
			BM::GUID kMasterCharGuid;
			BYTE cKeyInfoType = 0;

			pkMsg->Pop(cKeyInfoType);

			BM::CPacket kOPacket(PT_C_N_REQ_PARTY_RENAME_2ND);

			size_t const iPreSize = kOPacket.Size();
			switch(cKeyInfoType)
			{
			case KIT_CharGuid:// CharGuid
				{
					BM::GUID kCharGuid;
					pkMsg->Pop(kCharGuid);

					::WritePlayerInfoToPacket_ByGuid(kCharGuid, false, kOPacket);
				}break;
			case KIT_CharName://CharName
				{
					std::wstring kCharName;
					pkMsg->Pop(kCharName);
					WritePlayerInfoToPacket_ByName(kCharName, kOPacket);
				}break;
			case KIT_MembGuid://MembGuid
				{
					BM::GUID kMembGuid;
					pkMsg->Pop(kMembGuid);
					::WritePlayerInfoToPacket_ByGuid(kMembGuid, true, kOPacket);
				}break;
			default:
				{
					VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("Wrong User Info Key Type=") <<cKeyInfoType);
					LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Invalid CaseType"));
				}break;
			}

			if( iPreSize != kOPacket.Size() )
			{
				kOPacket.Push(*(BM::CPacket*)pkMsg);
				SendToGlobalPartyMgr(kOPacket);
			}
		}break;
		//<< For GM Command
	case PT_C_N_REQ_JOIN_PARTY:// 유저로부터 파티 생성/가입 요청을 받음
		{
			BM::GUID kMasterCharGuid;
			BM::GUID kUserGuid;
			BYTE cJoinType = 0;

			pkMsg->Pop(kMasterCharGuid);// 가입을 주도하는 놈의 캐릭터GUID
			pkMsg->Pop(cJoinType);

			BM::CPacket kPPacket(PT_C_N_REQ_JOIN_PARTY_2ND);
			if(S_OK != ::WritePlayerInfoToPacket_ByGuid(kMasterCharGuid, false, kPPacket))
			{
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! WritePlayerInfoToPacket_ByGuid isn't S_OK"));
				break;
			}

			SContentsUser kUser;
			switch(cJoinType)
			{
			case PCT_REQJOIN_CHARGUID:
				{
					pkMsg->Pop(kUserGuid);// 가입을 당하는 놈의 캐릭터GUID

					::GetPlayerByGuid(kUserGuid, false, kUser);
				}break;
			case PCT_REQJOIN_CHARNAME:
				{
					std::wstring kCharName;
					pkMsg->Pop(kCharName);// 가입을 당하는 놈의 맴버GUID
					::GetPlayerByName(kCharName, kUser);
				}break;
			case PCT_REQJOIN_MEMBERGUID:
				{
					pkMsg->Pop(kUserGuid);// 가입을 당하는 놈의 캐릭터GUID

					::GetPlayerByGuid(kUserGuid, true, kUser);
				}break;
			default:
				{
					VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("invalid Party join type=") << cJoinType);
					LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Invalid CaseType"));
				}
			}

			kUser.WriteToPacket(kPPacket);
			SendToGlobalPartyMgr(kPPacket);
		}break;
	case PT_C_N_REQ_JOIN_PARTYFIND:// 유저가 파티 가입을 요청
		{
			BM::GUID kMasterCharGuid;
			BM::GUID kUserGuid;
			BYTE cJoinType = 0;

			pkMsg->Pop(kMasterCharGuid);// 가입 캐릭터GUID
			pkMsg->Pop(cJoinType);

			BM::CPacket kQPacket(PT_C_N_REQ_JOIN_PARTYFIND_2ND);
			if(S_OK != ::WritePlayerInfoToPacket_ByGuid(kMasterCharGuid, false, kQPacket))
			{
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! WritePlayerInfoToPacket_ByGuid isn't S_OK"));
				break;
			}

			SContentsUser kUser;
			switch(cJoinType)
			{
			case PCT_REQJOIN_CHARGUID:
				{
					pkMsg->Pop(kUserGuid);// 가입하는 파티의 마스터 캐릭터GUID

					::GetPlayerByGuid(kUserGuid, false, kUser);
				}break;
			case PCT_REQJOIN_CHARNAME:
				{
					std::wstring kCharName;
					pkMsg->Pop(kCharName);// 가입하는 파티의 맴버GUID

					::GetPlayerByName(kCharName, kUser);
				}break;
			case PCT_REQJOIN_MEMBERGUID:
				{
					pkMsg->Pop(kUserGuid);// 가입하는 파티의 캐릭터GUID

					::GetPlayerByGuid(kUserGuid, true, kUser);
				}break;
			default:
				{
					VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__ << _T("invalid Party join type=") << cJoinType);
					LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Invalid CaseType"));
				}
			}

			kUser.WriteToPacket(kQPacket);
			SendToGlobalPartyMgr(kQPacket);
		}break;
	case PT_C_N_REQ_PARTY_CHANGE_MASTER:
		{
			BM::GUID kMasterCharGuid;
			BM::GUID kNewMasterGuid;

			pkMsg->Pop(kMasterCharGuid);
			pkMsg->Pop(kNewMasterGuid);

			ChangeMaster( kMasterCharGuid, kNewMasterGuid);
		}break;
	case PT_C_M_REQ_PARTY_LIST:
		{
			BM::GUID kCharGuid;

			pkMsg->Pop(kCharGuid);

			BM::CPacket kCPacket(PT_M_C_ANS_PARTY_LIST);
			WritePartyListInfoToPacket(*pkMsg, kCPacket);
			g_kServerSetMgr.Locked_SendToUser(kCharGuid, kCPacket, false);
		}break;
	case PT_C_N_ANS_JOIN_PARTY:// 유저가 파티에 가입여부를 대답
	case PT_C_N_ANS_JOIN_PARTYFIND:
	case PT_C_N_REQ_LEAVE_PARTY:
	case PT_M_N_NFY_PARTY_USER_PROPERTY:
	case PT_C_N_REQ_CREATE_PARTY:
	case PT_M_N_REQ_PARTY_COMMAND:
	case PT_C_N_REQ_PARTY_CHANGE_OPTION:
		{
			BM::GUID kCharGuid;
			pkMsg->Pop(kCharGuid);
			{
				PACKET_ID_TYPE wNewType = 0;
				switch (wType)
				{
					case PT_C_N_ANS_JOIN_PARTY:
						{
							wNewType = PT_C_N_ANS_JOIN_PARTY_2ND;
						}break;
					case PT_C_N_ANS_JOIN_PARTYFIND:
						{
							wNewType = PT_C_N_ANS_JOIN_PARTYFIND_2ND;
						}break;
					case PT_C_N_REQ_LEAVE_PARTY:
						{
							wNewType = PT_C_N_REQ_LEAVE_PARTY_2ND;
						}break;
					case PT_M_N_NFY_PARTY_USER_PROPERTY:
						{
							wNewType = PT_M_N_NFY_PARTY_USER_PROPERTY_2ND;
						}break;
					case PT_C_N_REQ_CREATE_PARTY:
						{
							wNewType = PT_C_N_REQ_CREATE_PARTY_2ND;
						}break;
					case PT_C_N_REQ_PARTY_CHANGE_OPTION:
						{
							wNewType = PT_C_N_REQ_PARTY_CHANGE_OPTION_2ND;
						}break;
					case PT_M_N_REQ_PARTY_COMMAND:
						{
							wNewType = PT_M_N_REQ_PARTY_COMMAND_2ND;
						}break;
					default:
						{
							VERIFY_INFO_LOG(false, BM::LOG_LV5, __FUNCTIONW__ << _T("[PT_C_N_ANS_JOIN_PARTY] undefined packet type=") << wType);
							LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Invalid CaseType"));
						}break;
				}
				BM::CPacket kSPacket(wNewType);
				if( S_OK != ::WritePlayerInfoToPacket_ByGuid(kCharGuid, false, kSPacket) )
				{
					break;
				}

				kSPacket.Push(*(BM::CPacket*)pkMsg);
				SendToGlobalPartyMgr(kSPacket);
			}
		}break;
		// PT_C_N_REQ_KICKOUT_PARTY_USER handler 어느것이 진짜?
		// CASE 1:
	case PT_C_N_REQ_KICKOUT_PARTY_USER:
		{
			BM::GUID kMasterCharGuid;
			BYTE cCommandType = 0;

			pkMsg->Pop(kMasterCharGuid);
			pkMsg->Pop(cCommandType);

			BM::CPacket kTPacket(PT_C_N_REQ_KICKOUT_PARTY_USER_2ND);
			kTPacket.Push(cCommandType);
			bool bRet = (S_OK == ::WritePlayerInfoToPacket_ByGuid(kMasterCharGuid, false, kTPacket));

			size_t const iPreSize = kTPacket.Size();
			if( PCT_KICK_CHARGUID == cCommandType )
			{
				BM::GUID kCharGuid;
				pkMsg->Pop(kCharGuid);
				bRet = (S_OK == ::WritePlayerInfoToPacket_ByGuid(kCharGuid, false, kTPacket));
			}
			else if( PCT_KICK_CHARNAME == cCommandType )
			{
				std::wstring kCharName;
				pkMsg->Pop(kCharName);
				bRet = (S_OK == WritePlayerInfoToPacket_ByName(kCharName, kTPacket));
			}

			if( iPreSize != kTPacket.Size() )
			{
				kTPacket.Push(*(BM::CPacket*)pkMsg);
				SendToGlobalPartyMgr(kTPacket);
			}
		}break;
	case PT_C_N_REQ_KICKOUT_PARTY_USER_2ND://강퇴
		{
			BYTE bCommandType = 0;
			BM::GUID kPartyGuid;
			SContentsUser kMaster;
			SContentsUser kUser;

			pkMsg->Pop(bCommandType);
			kMaster.ReadFromPacket(*pkMsg);
			kUser.ReadFromPacket(*pkMsg);

			bool const bIsParty = GetCharToParty(kMaster.kCharGuid, kPartyGuid);
			if( bIsParty )
			{
				HRESULT const hRet = KickUser(kPartyGuid, kMaster, kUser.kCharGuid, kUser.kGndKey.GroundNo(), kUser.kMemGuid, kUser.Name());
				if( PRC_Fail_KickUserNoArea == hRet )
				{
					BM::CPacket kRetPacket(PT_N_C_ANS_JOIN_PARTY_TO_MASTER);
					kRetPacket.Push(hRet);
					g_kServerSetMgr.Locked_SendToUser(kMaster.kMemGuid, kRetPacket);//결과					
				}
			}
		}break;
	case PT_C_M_REQ_CHAT_INPUTNOW://일반 채팅 입력도중 외에 전부
		{
			BM::GUID kCharGuid;
			BYTE cChatMode = 0;
			bool bPopup = false;

			pkMsg->Pop(kCharGuid);
			pkMsg->Pop(cChatMode);
			pkMsg->Pop(bPopup);

			{//패킷스콥
				BM::CPacket kUPacket(PT_C_M_REQ_CHAT_INPUTNOW_2ND);
				kUPacket.Push(cChatMode);
				::WritePlayerInfoToPacket_ByGuid(kCharGuid, false, kUPacket);
				kUPacket.Push(bPopup);
				SendToChannelChatMgr(kUPacket);
			}
		}break;
	case PT_M_T_REQ_REGIST_HARDCORE_VOTE:
		{
			int iMode = 0;
			SGroundKey kDungeonGndKey;
			BM::GUID kCharGuid;
			pkMsg->Pop( iMode );
			kDungeonGndKey.ReadFromPacket( *pkMsg );
			pkMsg->Pop( kCharGuid );

			PgGlobalParty *pkParty = GetCharToParty( kCharGuid );
			if ( pkParty )
			{
				class PgFunction_HCDExtFunction
					:	public PgPartyContents_HardCoreDungeon::PgExtFunction
				{
				public:
					PgFunction_HCDExtFunction(){}
					virtual ~PgFunction_HCDExtFunction(){}

					virtual void Release( BYTE const byState, PgPartyContents_HardCoreDungeon::CONT_MEMBER_STATE const &kMemberState )
					{
						switch( byState )
						{
						case E_HCT_VOTE:
							{
								PgPartyContents_HardCoreDungeon::CONT_MEMBER_STATE::const_iterator member_itr = kMemberState.begin();
								for ( ; member_itr != kMemberState.end() ; ++member_itr )
								{
									if ( E_HCT_V_OK >= member_itr->second.byState )
									{
										// 취소 요청 통보
										BM::CPacket kMPacket( PT_T_M_ANS_RET_HARDCORE_VOTE_CANCEL, member_itr->first );
										g_kServerSetMgr.Locked_SendToGround( member_itr->first, kMPacket, true );
									}
								}
							}break;
						case H_HCT_READY:
							{
								SPortalWaiter::CONT_WAIT_LIST kWaiterList;

								PgPartyContents_HardCoreDungeon::CONT_MEMBER_STATE::const_iterator member_itr = kMemberState.begin();
								for ( ; member_itr != kMemberState.end() ; ++member_itr )
								{
									if ( E_HCT_V_OK == member_itr->second.byState )
									{
										kWaiterList.insert( std::make_pair( member_itr->first, member_itr->second.kVolatileID ) );
									}
								}

								if ( kWaiterList.size() )
								{
									BM::CPacket kFailedPacket( PT_T_T_REQ_RECENT_MAP_MOVE );
									PU::TWriteTable_AA( kFailedPacket, kWaiterList );
									::SendToCenter( kFailedPacket );
								}
							}break;
						default:
							{
							}break;
						}
					}
				};

				PgPartyContents_HardCoreDungeon *pkPartyContents = new PgPartyContents_HardCoreDungeon;
				if ( true == pkParty->AttachPartyContents( pkPartyContents ) )
				{
					pkPartyContents->SetExtFunction<PgFunction_HCDExtFunction>();
					pkPartyContents->SetMode( iMode );
					pkPartyContents->SetDungeonGndKey( kDungeonGndKey );	

					VEC_GUID kMemberGuidList;
					kMemberGuidList.reserve( pkParty->MemberCount() );
					pkParty->GetMemberCharGuidList( kMemberGuidList );

					SChnGroundKey kChnGndKey;

					VEC_GUID::const_iterator member_itr = kMemberGuidList.begin();
					for ( ; member_itr != kMemberGuidList.end() ; ++member_itr )
					{
						if ( true == pkParty->GetMemberChnGndInfo( *member_itr, kChnGndKey ) )
						{
							BM::CPacket kMPacket( PT_T_M_ANS_REGIST_HARDCORE_VOTE, *member_itr );
							kMPacket.Push( static_cast<int>(0) );// No Error
							pkPartyContents->WriteToPacket( kMPacket );
							SendToGround( kChnGndKey, kMPacket );
						}
					}
				}
				else
				{
					BM::CPacket kMPacket( PT_T_M_ANS_REGIST_HARDCORE_VOTE, kCharGuid );
					kMPacket.Push( static_cast<int>(402015) );// 파티의 하드코어 던젼 입장을 처리중에 있습니다.
					g_kServerSetMgr.Locked_SendToGround( kCharGuid, kMPacket, true );
				}
			}
		}break;
	case PT_M_T_REQ_RET_HARDCORE_VOTE:
		{
			BM::GUID kCharGuid;
			BYTE byState = 0;
			pkMsg->Pop( kCharGuid );
			pkMsg->Pop( byState );

			PgGlobalParty *pkParty = GetCharToParty( kCharGuid );
			if ( pkParty )
			{
				PgPartyContents_HardCoreDungeon *pkPartyContents = dynamic_cast<PgPartyContents_HardCoreDungeon*>(pkParty->GetPartyContents());
				if ( pkPartyContents )
				{
					if ( E_HCT_VOTE == pkPartyContents->GetState() )
					{
						if ( true == pkParty->IsMaster( kCharGuid ) )
						{
							if ( E_HCT_V_OK == byState )
							{
								if ( true == pkPartyContents->IsAllSetState() )
								{
									VEC_GUID kMemberGuidList;
									kMemberGuidList.reserve( pkParty->MemberCount() );
									pkParty->GetMemberCharGuidList( kMemberGuidList );

									VEC_GUID::const_iterator member_itr = kMemberGuidList.begin();
									for ( ; member_itr != kMemberGuidList.end() ; ++member_itr )
									{
										BYTE const byMemberState = pkPartyContents->GetMemberState(*member_itr);
										bool const bIsJoin = ( E_HCT_V_OK >= byMemberState );

										if ( true == bIsJoin )
										{
											BM::CPacket kReqJoinPacket( PT_T_M_REQ_JOIN_HARDCORE, *member_itr );
											g_kServerSetMgr.Locked_SendToGround( *member_itr, kReqJoinPacket, true );
										}

										if ( !bIsJoin )
										{
											if ( E_HCT_V_HARDCORE_DUNGEON != byMemberState )
											{
												LeaveParty( *member_itr );
											}
										}
									}

									pkPartyContents->SetWaitMove();
								}
							}
							else
							{
								pkPartyContents = NULL;// NULL 했다!
								pkParty->DetachPartyContents();
							}
						}
						else
						{
							if ( S_OK == pkPartyContents->SetMemberState( kCharGuid, byState ) )
							{
								VEC_GUID kMemberGuidList;
								kMemberGuidList.reserve( pkParty->MemberCount() );
								pkParty->GetMemberCharGuidList( kMemberGuidList );

								VEC_GUID::const_iterator member_itr = kMemberGuidList.begin();
								for ( ; member_itr != kMemberGuidList.end() ; ++member_itr )
								{
									if ( E_HCT_V_OK >= pkPartyContents->GetMemberState(*member_itr) )
									{
										BM::CPacket kMPacket( PT_T_M_ANS_REGIST_HARDCORE_VOTE, *member_itr );
										kMPacket.Push( static_cast<int>(0) );// No Error
										pkPartyContents->WriteToPacket( kMPacket );
										g_kServerSetMgr.Locked_SendToGround( *member_itr, kMPacket, true );
									}
								}
							}
						}
					}
				}
			}
		}break;
	case PT_M_T_ANS_JOIN_HARDCORE_FAILED:
		{
			BM::GUID kCharGuid;
			pkMsg->Pop( kCharGuid );

			PgGlobalParty *pkParty = GetCharToParty( kCharGuid );
			if ( pkParty )
			{
				PgPartyContents_HardCoreDungeon *pkPartyContents = dynamic_cast<PgPartyContents_HardCoreDungeon*>(pkParty->GetPartyContents());
				if ( pkPartyContents )
				{
					if ( H_HCT_READY == pkPartyContents->GetState() )
					{
						if ( true == pkParty->IsMaster( kCharGuid ) )
						{
							pkPartyContents = NULL;//Null했다.
							pkParty->DetachPartyContents();
						}
						else
						{
							LeaveParty( kCharGuid );
						}

						if ( SUCCEEDED(pkPartyContents->SetMove()) )
						{
							SReqMapMove_MT kRMM(MMET_GoTopublicGroundParty);
							kRMM.kTargetKey = pkPartyContents->GetDungeonGndKey();

							BM::CPacket kMovePacket( PT_T_T_REQ_MAP_MOVE, kRMM );
							pkPartyContents->WriteToPacket_JoinDungeonOrder( kMovePacket );
							::SendToCenter( kMovePacket );

							pkPartyContents = NULL;// Set NULL pkPartyContents
							pkParty->DetachPartyContents();
						}
					}
				}
			}
		}break;
	case PT_T_T_REQ_READY_JOIN_HARDCORE:
		{
			SPortalWaiter::CONT_WAIT_LIST kWaiterList;
			PU::TLoadTable_AA( *pkMsg, kWaiterList );

			if ( kWaiterList.size() )
			{
				SPortalWaiter::CONT_WAIT_LIST::const_iterator wait_itr = kWaiterList.begin();
				PgGlobalParty *pkParty = GetCharToParty( wait_itr->first );
				if ( pkParty )
				{
					PgPartyContents_HardCoreDungeon *pkPartyContents = dynamic_cast<PgPartyContents_HardCoreDungeon*>(pkParty->GetPartyContents());
					if ( pkPartyContents )
					{
						if ( H_HCT_READY == pkPartyContents->GetState() )
						{
							for ( ; wait_itr != kWaiterList.end() ; ++wait_itr )
							{
								pkParty->MovedGnd( wait_itr->first, SChnGroundKey() );// 초기화 해놔야 한다.
								pkPartyContents->SetMemberMoveReady( wait_itr->first, wait_itr->second );								
							}

							pkPartyContents->ReadFromPacket_MapMoveOrder( *pkMsg );

							if ( SUCCEEDED(pkPartyContents->SetMove()) )
							{
								SReqMapMove_MT kRMM(MMET_GoTopublicGroundParty);
								kRMM.kTargetKey = pkPartyContents->GetDungeonGndKey();

								BM::CPacket kMovePacket( PT_T_T_REQ_MAP_MOVE, kRMM );
								pkPartyContents->WriteToPacket_JoinDungeonOrder( kMovePacket );
								::SendToCenter( kMovePacket );

								pkPartyContents = NULL;// Set NULL pkPartyContents
								pkParty->DetachPartyContents();
							}
							break;//break!!!!!!!
						}
					}
				}

				BM::CPacket kFailedPacket( PT_T_T_REQ_RECENT_MAP_MOVE );
				PU::TWriteTable_AA( kFailedPacket, kWaiterList );
				::SendToCenter( kFailedPacket );
			}
		}break;
	case PT_N_T_NFY_COMMUNITY_STATE_HOMEADDR_PARTY:
		{
			BM::GUID kCharGuid;
			pkMsg->Pop( kCharGuid );
			SHOMEADDR kHomeAddr;
			pkMsg->Pop( kHomeAddr );

			PgGlobalParty* pkParty = GetCharToParty(kCharGuid);
			if( pkParty )
			{
				if( true == pkParty->SetFriendHomeAddr(kCharGuid, kHomeAddr) )
				{
					VEC_GUID kContMember;
					pkParty->GetMemberCharGuidList(kContMember);
					VEC_GUID::const_iterator member_iter = kContMember.begin();
					while( kContMember.end() != member_iter)
					{
						SContentsUser kUser;
						if( S_OK == ::GetPlayerByGuid( (*member_iter), false, kUser) )
						{
							BM::CPacket kPacket(PT_T_C_NFY_PARTY_MEMBER_MYHOME);
							kPacket.Push(kCharGuid);
							kPacket.Push(kHomeAddr);
							SendToUser(pkParty, kUser.kCharGuid, kPacket);
						}
						++member_iter;
					}
				}
			}
		}break;
	default:
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV3, __FL__ << _T("invalid packet type=") << wType );
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Invalid CaseType"));
			return false;
		}
	}
	return true;
}

bool PgGlobalPartyMgrImpl::PartyFriendCheckInfo(BM::CPacket* pkPacket)
{
	BM::GUID kCharGuid;
	bool bSet = false;
	pkPacket->Pop(kCharGuid);
	pkPacket->Pop(bSet);

	PgGlobalParty* pkParty = GetCharToParty(kCharGuid);
	if( pkParty )
	{
		SContentsUser kUser;
		if( S_OK == ::GetPlayerByGuid(kCharGuid, false, kUser) )
		{
			BM::CPacket kIPacket(PT_A_U_SEND_TOFRIEND_PARTY);
			kIPacket.Push(kUser.sChannel);
			kIPacket.Push(pkParty->PartyGuid());
			kIPacket.Push(kCharGuid);
			kIPacket.Push(true);
			pkParty->WriteToMemberInfoList(kIPacket);
			SendToFriendMgr(kIPacket);
			return true;
		}
	}	
	return false;
}

bool PgGlobalPartyMgrImpl::PartyFriendCheck(BM::CPacket* pkPacket)
{
	BM::GUID kCharGuid;
	BM::GUID kPartyGuid;
	VEC_UserInfo Vec_PartyUserInfo;
	bool bSet = false;

	pkPacket->Pop(kCharGuid);
	pkPacket->Pop(bSet);
	pkPacket->Pop(kPartyGuid);
	PU::TLoadArray_M(*pkPacket, Vec_PartyUserInfo);

	PgGlobalParty* pkParty = GetParty(kPartyGuid);
	if( !pkParty )
	{
		INFO_LOG(BM::LOG_LV5, __FL__ << _T("Cannot get Party=") << kPartyGuid);
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}


	VEC_UserInfo::iterator iter = Vec_PartyUserInfo.begin();
	while( Vec_PartyUserInfo.end() != iter )
	{
		pkParty->PartyFriendCheck(*iter);
		++iter;
	}

	SyncToLocalPartyMgr(pkParty);//Gnd Local 동기화
	return true;
}

void PgGlobalPartyMgrImpl::SetPartyOptionAreaNo(int const iGroundNo, SPartyOption& rkOption)
{
	if( 0 == rkOption.GetOptionArea_NameNo() )
	{
		CONT_DEF_PARTY_INFO const* pkDefPartyInfo = NULL;
		g_kTblDataMgr.GetContDef(pkDefPartyInfo);

		if( pkDefPartyInfo )
		{
			for(int i=POC_Area1; i<POC_Max; ++i)
			{
				CONT_DEF_PARTY_INFO::const_iterator iter = pkDefPartyInfo->find(i);
				if( pkDefPartyInfo->end() != iter )
				{
					CONT_DEF_PARTY_INFO::mapped_type const kContinentList = iter->second;
					CONT_DEF_PARTY_INFO::mapped_type::const_iterator iter_ContinentList = kContinentList.begin();
					while( kContinentList.end() != iter_ContinentList )
					{
						if( iGroundNo == iter_ContinentList->iGroundNo )
						{
							rkOption.SetOptionArea_NameNo(iter_ContinentList->iArea_NameNo);
							return;
						}							

						++iter_ContinentList;
					}
				}
			}
		}
	}
}

HRESULT PgGlobalPartyMgrImpl::ReqJoinParty(SContentsUser const& rkMaster, SContentsUser const& rkUser, SPartyOption& rkOption)
{
	BM::GUID kPartyGuid;

	HRESULT const hCheckRet = CheckJoinParty(rkMaster, rkUser);//파티 가입조건이?
	if( PRC_Success_Create != hCheckRet
	&&	PRC_Success != hCheckRet )//생성, 성공 아니면
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return ") << hCheckRet );
		return hCheckRet;//실패
	}
	
	if( PRC_Success_Create == hCheckRet )//파장이 파티가 없으면 생성
	{
		SetPartyOptionAreaNo(rkMaster.kGndKey.GroundNo(), rkOption);

		std::wstring kNewPartyName;	
		HRESULT const hCreateRet = CreateParty(kNewPartyName, kPartyGuid, rkOption);
		if( PRC_Success != hCreateRet )
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return ") << hCheckRet );
			return hCheckRet;//생성 실패
		}

		HRESULT const hMasterJoinRet = AnsJoinParty(kPartyGuid, rkMaster, true, true);//마스터를 강제로 가입
		if( PRC_Success != hMasterJoinRet )
		{
			Delete(kPartyGuid);
			DelCharToParty(rkMaster.kCharGuid);//실패면 마스터 파티 삭제
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
			return PRC_Fail;
		}

		if( rkUser.sLevel < rkOption.GetOptionLevel() )
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_Level"));
			return PRC_Fail_Level;// 레벨이 맞지 않음(파티방)
		}

		// Log
		PgLogCont kLogCont(ELogMain_Contents_Party, ELogSub_Party);				
		kLogCont.MemberKey(rkMaster.kMemGuid);
		kLogCont.UID(rkMaster.iUID);
		kLogCont.CharacterKey(rkMaster.kCharGuid);
		kLogCont.ID(rkMaster.kAccountID);
		kLogCont.Name(rkMaster.Name());
		kLogCont.ChannelNo(static_cast<short>(rkMaster.sChannel));
		kLogCont.Class(static_cast<short>(rkMaster.iClass));
		kLogCont.Level(static_cast<short>(rkMaster.sLevel));
		kLogCont.GroundNo(static_cast<int>(rkMaster.kGndKey.GroundNo()));

		PgLog kLog(ELOrderMain_Party, ELOrderSub_Create);
		kLog.Set(0, kNewPartyName);
		std::wstring strPartyInfo = PgLogUtil::GetPartyOptionPublic(rkOption.GetOptionPublicTitle());
		strPartyInfo += _T(" ");
		strPartyInfo += PgLogUtil::GetPartyOptionItemString(rkOption.GetOptionItem());
		kLog.Set(1, strPartyInfo);
		kLog.Set(0, static_cast<int>(rkOption.GetOptionPublicTitle()));
		kLog.Set(1, static_cast<int>(rkOption.GetOptionItem()));
		kLog.Set(2, static_cast<int>(rkOption.GetOptionLevel()));
		kLog.Set(3, static_cast<int>(rkMaster.cGender) );
		kLog.Set(2, kPartyGuid.str());

		kLogCont.Add( kLog );
		kLogCont.Commit();
	}

	bool const bFindGuid = GetCharToParty(rkMaster.kCharGuid, kPartyGuid);//마스터 파티 Guid
	if( !bFindGuid )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
		return PRC_Fail;
	}

	PgGlobalParty* pkParty = GetParty(kPartyGuid);//마스터 파티
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
		return PRC_Fail;
	}

	bool bIsMaster = pkParty->IsMaster(rkMaster.kCharGuid);
	if( !bIsMaster )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_NotMaster"));
		return PRC_Fail_NotMaster;
	}

	HRESULT const hAddWiatRet = pkParty->AddWait(rkUser.kCharGuid);
	if( PRC_Success != hAddWiatRet )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return E_FAIL From PartyAddWait"));
		return hAddWiatRet;//실패
	}

	AddCharToParty(rkUser.kCharGuid, kPartyGuid);//파티열 등록

	BM::CPacket kUserPacket(PT_N_C_REQ_JOIN_PARTY);//초대 메시지
	kUserPacket.Push(rkMaster.Name());
	kUserPacket.Push(kPartyGuid);
	kUserPacket.Push(rkMaster.kCharGuid);
	kUserPacket.Push(pkParty->PartyName());
	kUserPacket.Push(rkMaster.sLevel);
	kUserPacket.Push(rkMaster.iClass);
	g_kServerSetMgr.Locked_SendToUser(rkUser.kMemGuid, kUserPacket);

	return PRC_Success;//성공
}

HRESULT PgGlobalPartyMgrImpl::ReqJoinPartyFind(SContentsUser const& rkMaster, SContentsUser const& rkUser, SPartyOption const& rkOption)
{
	BM::GUID kPartyGuid;

	HRESULT const hCheckRet = CheckJoinParty(rkMaster, rkUser);//파티 가입조건이?
	if( PRC_Success_Create != hCheckRet
		&&	PRC_Success != hCheckRet )//생성, 성공 아니면
	{
		if( PRC_Fail_MineMember == hCheckRet )
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PMS_Fail_DupReqJoin"));
			return PMS_Fail_DupReqJoin; // 이미 다른 파티에 가입 신청한 상태다
		}
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return ") << hCheckRet);
		return hCheckRet;//실패
	}

	bool const bFindGuid = GetCharToParty(rkMaster.kCharGuid, kPartyGuid);//마스터 파티 Guid
	if( !bFindGuid )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
		return PRC_Fail;
	}

	PgGlobalParty* pkParty = GetParty(kPartyGuid);//마스터 파티
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
		return PRC_Fail;
	}

	bool bIsMaster = pkParty->IsMaster(rkMaster.kCharGuid);
	if( !bIsMaster )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_NotMaster"));
		return PRC_Fail_NotMaster;
	}

	HRESULT const hAddWiatRet = pkParty->AddWait(rkUser.kCharGuid);
	if( PRC_Success != hAddWiatRet )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return ") << hAddWiatRet);
		return hAddWiatRet;//실패
	}

	AddCharToParty(rkUser.kCharGuid, kPartyGuid);//파티열 등록

	BM::CPacket kUserPacket(PT_N_C_REQ_JOIN_PARTYFIND);//초대 메시지
	kUserPacket.Push(rkUser.Name());
	kUserPacket.Push(kPartyGuid);
	kUserPacket.Push(rkUser.kCharGuid);
	kUserPacket.Push(pkParty->PartyName());
	kUserPacket.Push(rkUser.sLevel);
	kUserPacket.Push(rkUser.iClass);
	g_kServerSetMgr.Locked_SendToUser(rkMaster.kMemGuid, kUserPacket);

	BM::CPacket kRetPacket(PT_N_C_ANS_JOIN_PARTY_TO_MASTER);
	kRetPacket.Push( PRC_Success_PartyInvite );
	g_kServerSetMgr.Locked_SendToUser(rkUser.kMemGuid, kRetPacket);//결과

	return PRC_Success;//성공
}

HRESULT PgGlobalPartyMgrImpl::AnsJoinParty(BM::GUID const &rkPartyGuid, SContentsUser const& rkNewPartyUserInfo, bool const bWantJoin, bool const bMaster)
{
	BM::GUID const &rkCharGuid = rkNewPartyUserInfo.kCharGuid;

	PgGlobalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		DelCharToParty(rkCharGuid);
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_None"));
		return PRC_None;
	}

	BM::GUID kUserPartyGuid;
	bool const bFindParty = GetCharToParty(rkNewPartyUserInfo.kCharGuid, kUserPartyGuid);
	if( bWantJoin
	&&	!bFindParty )//생자로 가입 요청 하는 사람
	{
		HRESULT const hAddWaitRet = pkParty->AddWait(rkNewPartyUserInfo.kCharGuid);
		if( PRC_Success != hAddWaitRet )
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return ") << hAddWaitRet);
			return hAddWaitRet;
		}
	}
	else if( bFindParty )
	{
		if( rkPartyGuid != kUserPartyGuid )
		{
			PgGlobalParty* pkParty = GetParty(kUserPartyGuid);
			if( pkParty )
			{
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_Party"));
				return PRC_Fail_Party;
			}
			else
			{
				DelCharToParty(rkCharGuid);
			}
		}
	}

	if( pkParty->MemberCount() >= PV_MAX_MEMBER_CNT )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_MemberCount"));
		return PRC_Fail_MemberCount;//인원수 제한으로 등록 불가
	}


	//승낙 / 거부
	//성공/ 실패
	HRESULT hAnsJoinRet = PRC_Success;
	if( bWantJoin//가입을 원하고
	&&	pkParty )//파티가 있으면
	{
		SPartyUserInfo kPartyUserInfo(rkNewPartyUserInfo);
		kPartyUserInfo.kChnGndKey.Channel( g_kProcessCfg.ChannelNo() );
		hAnsJoinRet = pkParty->Add( kPartyUserInfo, bMaster);//가입
		if( PRC_Success == hAnsJoinRet )
		{
			///////////////////////////////////
			PartyBuffMember(pkParty, rkCharGuid, true);

			SContentsUser kUser;
			if( S_OK == ::GetPlayerByGuid(rkCharGuid, false, kUser) )
			{
				BM::CPacket kJPacket(PT_A_U_SEND_TOFRIEND_PARTY);
				kJPacket.Push(kUser.sChannel);
				kJPacket.Push(rkPartyGuid);
				kJPacket.Push(rkCharGuid);
				kJPacket.Push(true);
				pkParty->WriteToMemberInfoList(kJPacket);
				SendToFriendMgr(kJPacket);
			}
			///////////////////////////////////
			if( !bMaster )//마스터가 아니면
			{
				BM::CPacket kNfyPacket(PT_N_C_NFY_JOIN_PARTY, rkPartyGuid);
				kPartyUserInfo.WriteToPacket(kNfyPacket);
				SendToPartyMember(pkParty, kNfyPacket, rkCharGuid);//새로운 맴버 가입
			}

			BM::CPacket kAnsUserPacket(PT_N_C_ANS_JOIN_PARTY, rkPartyGuid);
			kAnsUserPacket.Push(hAnsJoinRet);
			pkParty->WriteToPacket(kAnsUserPacket);
			g_kServerSetMgr.Locked_SendToUser(rkCharGuid, kAnsUserPacket, false);//가입 환영

			AddCharToParty(rkCharGuid, rkPartyGuid);//다시 추가

			SyncToLocalPartyMgr(pkParty);//Gnd Local 동기화

			SContentsUser kLogUser;
			std::wstring kID = std::wstring();
			if( S_OK == ::GetPlayerByGuid(rkCharGuid, false, kLogUser) )
			{
				kID = kLogUser.kAccountID;
			}
			
			LogPartyAction( pkParty, rkNewPartyUserInfo, ELOrderSub_Join );
		}
	}
	else
	{
		DelCharToParty(rkCharGuid);

		if( pkParty )
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_Refuse"));
			hAnsJoinRet = PRC_Fail_Refuse;//거부

			BM::CPacket kRetPacket(PT_N_C_ANS_JOIN_PARTY_TO_MASTER);
			kRetPacket.Push(hAnsJoinRet);
			kRetPacket.Push(rkNewPartyUserInfo.kCharGuid);
			kRetPacket.Push(rkNewPartyUserInfo.kName);
			g_kServerSetMgr.Locked_SendToUser(pkParty->MasterCharGuid(), kRetPacket, false);//마스터에게 거부 메시지

			pkParty->DelWait(rkCharGuid);
		}
		else
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_None"));
			hAnsJoinRet = PRC_None;//파티가 증발
		}
	}

	return hAnsJoinRet;
}

void PgGlobalPartyMgrImpl::PartyWaitUserSend(PgGlobalParty* pkParty, SContentsUser const &rkUser)
{
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Not Find Party"));
		return;
	}

	if( rkUser.Empty() )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Not Find SContentsUser Info"));
		return;
	}

	BM::GUID rkPartyGuid = pkParty->PartyGuid();
	VEC_GUID kRecvGuidVec;
	BM::CPacket kNfyPacket(PT_N_C_ANS_JOIN_PARTY_TO_MASTER);
	kNfyPacket.Push(PRC_Fail_Refuse);
	kNfyPacket.Push(rkUser.kCharGuid);
	kNfyPacket.Push(rkUser.Name());

	VEC_GUID kWaiterVec;
	pkParty->GetWaitCharGuidList(kWaiterVec);
	VEC_GUID::iterator wait_iter = kWaiterVec.begin();
	while(kWaiterVec.end() != wait_iter)
	{
		BM::GUID const &rkCharGuid = (*wait_iter);
		DelCharToParty(rkCharGuid);
		pkParty->Del(rkCharGuid);
		kRecvGuidVec.push_back(rkCharGuid);//통보 목록 추가
		++wait_iter;
	}

	BM::CPacket kGndNfyPacket( PT_N_M_NFY_DELETE_PARTY, rkPartyGuid );
	SendToLocalPartyMgr(pkParty, kGndNfyPacket);//그라운드 파티 삭제

	VEC_GUID kVec;
	pkParty->GetMemberCharGuidList(kVec);
	VEC_GUID::iterator iter = kVec.begin();
	while(kVec.end() != iter)
	{
		BM::GUID const &rkCharGuid = (*iter);
		DelCharToParty(rkCharGuid);
		pkParty->Del(rkCharGuid);
		kRecvGuidVec.push_back(rkCharGuid);//통보 목록 추가

		++iter;
	}

	g_kServerSetMgr.Locked_SendToUser(kRecvGuidVec, kNfyPacket, false);//사용자 통보	
}

void PgGlobalPartyMgrImpl::PartyWaitUserSend_Refuse(PgGlobalParty* pkParty, SContentsUser const &rkUser)
{
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Not Find Party"));
		return;
	}

	if( rkUser.Empty() )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Not Find SContentsUser Info"));
		return;
	}

	BM::GUID rkPartyGuid = pkParty->PartyGuid();
	VEC_GUID kRecvGuidVec;
	BM::CPacket kNfyPacket(PT_N_C_ANS_JOIN_PARTY_TO_MASTER);
	kNfyPacket.Push(PRC_Fail_Refuse);
	kNfyPacket.Push(rkUser.kCharGuid);
	kNfyPacket.Push(rkUser.Name());

	VEC_GUID kWaiterVec;
	pkParty->GetWaitCharGuidList(kWaiterVec);
	VEC_GUID::iterator wait_iter = kWaiterVec.begin();
	while(kWaiterVec.end() != wait_iter)
	{
		BM::GUID const &rkCharGuid = (*wait_iter);
		DelCharToParty(rkCharGuid);
		pkParty->Del(rkCharGuid);
		kRecvGuidVec.push_back(rkCharGuid);//통보 목록 추가
		++wait_iter;
	}

	g_kServerSetMgr.Locked_SendToUser(kRecvGuidVec, kNfyPacket, false);//사용자 통보	
}

void PgGlobalPartyMgrImpl::DestroyParty( PgGlobalParty * pkParty )
{
	BM::GUID const kPartyGuid = pkParty->PartyGuid();

	VEC_GUID kRecvGuidVec;
	BM::CPacket kNfyPacket(PT_N_C_NFY_PARTY_DESTROY, kPartyGuid );

	pkParty->GetWaitCharGuidList(kRecvGuidVec);
	VEC_GUID::iterator wait_iter = kRecvGuidVec.begin();
	while(kRecvGuidVec.end() != wait_iter)
	{
		BM::GUID const &rkCharGuid = (*wait_iter);
		DelCharToParty(rkCharGuid);
		pkParty->Del(rkCharGuid);
		++wait_iter;
	}

	BM::CPacket kGndNfyPacket( PT_N_M_NFY_DELETE_PARTY, kPartyGuid );
	SendToLocalPartyMgr(pkParty, kGndNfyPacket);//그라운드 파티 삭제

	
	VEC_GUID kVec;
	pkParty->GetMemberCharGuidList(kVec);
	VEC_GUID::iterator itr = kVec.begin();
	for( ; kVec.end() != itr ; ++itr )
	{
		BM::GUID const &rkCharGuid = (*itr);
		DelCharToParty(rkCharGuid);

		pkParty->Del(rkCharGuid);
		kRecvGuidVec.push_back(rkCharGuid);//통보 목록 추가

		// Log
		SContentsUser kLogUser;
		if( S_OK == ::GetPlayerByGuid(rkCharGuid, false, kLogUser) )
		{
			DeleteLog(kLogUser, kPartyGuid);
		}
	}

	if ( kRecvGuidVec.size() )
	{
		g_kServerSetMgr.Locked_SendToUser(kRecvGuidVec, kNfyPacket, false);//사용자 통보
	}
	
	Delete(kPartyGuid);//파티 삭제
}

void PgGlobalPartyMgrImpl::DestroyParty(BM::GUID const &rkPartyGuid)
{
	PgGlobalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Not Find Party"));
		return;
	}

	DestroyParty( pkParty );
}

HRESULT PgGlobalPartyMgrImpl::LeaveParty( BM::GUID const &kCharGuid )
{
	//assert(BM::GUID::NullData() != kUserInfo.kCharGuid);

	BM::GUID const kPartyGuid = DelCharToParty( kCharGuid );
	PgGlobalParty* pkParty = GetParty(kPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
		return PRC_Fail;
	}

	SPartyUserInfo kLeaveMember;
	pkParty->GetMemberInfo(kCharGuid, kLeaveMember);

	////////////////////////////////////////////////
	PartyBuffMember(pkParty, kCharGuid, false);
	// 파티장 주의에 있는 사람으로 파티장 권한이 주어진다
	HRESULT hDelRet;
	BM::GUID kNewMasterGuid;
	bool hMasterRet = pkParty->SetPartyMaster(kCharGuid, kNewMasterGuid);
	if( hMasterRet )
	{
		hDelRet = pkParty->Del(kCharGuid, kNewMasterGuid);
	}
	else
	{
		hDelRet = pkParty->Del(kCharGuid);
	}

	int iFriendCount = 0;
	iFriendCount = pkParty->GetFriendCount(kCharGuid);
	if( iFriendCount != 0 )
	{
			BM::CPacket kKPacket(PT_A_U_SEND_TOFRIEND_PARTY);
			kKPacket.Push(g_kProcessCfg.ChannelNo());
			kKPacket.Push(kPartyGuid);
			kKPacket.Push(kCharGuid);
			kKPacket.Push(false);
			pkParty->WriteToMemberInfoList(kKPacket);
			SendToFriendMgr(kKPacket);
	}
	////////////////////////////////////////////////
	if( PRC_Success_Waiter == hDelRet)
	{
		SContentsUser kContentsUser;
		::GetPlayerByGuid( kCharGuid, false, kContentsUser );

		AnsJoinParty(pkParty->PartyGuid(), kContentsUser, false, false);//가입을 거부 했다.
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_Refuse"));
		return PRC_Fail_Refuse;
	}

	bool bChangeMaster = (PRC_Success_Master == hDelRet);//마스터가 변경

	SPartyUserInfo kNewMaster;
	pkParty->GetMasterInfo(kNewMaster);//마스터 정보

	BM::CPacket kMemberPacket(PT_N_C_NFY_LEAVE_PARTY_USER);//삭제됨 멤버들에게 보냄
	kMemberPacket.Push(kCharGuid);
	kMemberPacket.Push(bChangeMaster);

	BM::CPacket kMapPacket(PT_N_M_NFY_LEAVE_PARTY_USER);//삭제됨 로컬파티매니저
	kMapPacket.Push(pkParty->PartyGuid());
	kMapPacket.Push(kCharGuid);
	kMapPacket.Push(bChangeMaster);

	if( bChangeMaster )
	{
		kMemberPacket.Push(kNewMaster.kName);
		kMemberPacket.Push(kNewMaster.kCharGuid);

		kMapPacket.Push(kNewMaster.kCharGuid);
	}

	SendToLocalPartyMgr(pkParty, kMapPacket);//Gnd Local

	SendToGround( kLeaveMember.kChnGndKey,kMapPacket );

	SendToPartyMember(pkParty, kMemberPacket);//남은 멤버들에게
	SendToUser( kLeaveMember.kChnGndKey.Channel(), kLeaveMember.kMemberGuid, kMemberPacket );

	if( PRC_Success_Destroy == hDelRet )
	{
		bool const bRet = Delete(pkParty->PartyGuid());
		if( bRet )
		{
			return hDelRet;
		}
	}

	return PRC_Success;
}

HRESULT PgGlobalPartyMgrImpl::KickUser(BM::GUID const &rkPartyGuid, SContentsUser const &rkUser, BM::GUID const &rkKickCharGuid, int const &KickUserGroundNo, BM::GUID const &rkKickMemberGuid, std::wstring const& rkKickName)
{
	//assert(rkPartyGuid != BM::GUID::NullData());
	if( rkUser.Empty() )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
		return PRC_Fail;
	}

	BM::GUID kPartyGuid;
	if( !GetCharToParty(rkKickCharGuid, kPartyGuid) )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
		return PRC_Fail;
	}

	if( rkPartyGuid != kPartyGuid )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
		return PRC_Fail;
	}

	PgGlobalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
		return PRC_Fail;
	}

	bool bIsMasterOrder = pkParty->IsMaster(rkUser.kCharGuid);
	if( !bIsMasterOrder )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_NotMaster"));
		return PRC_Fail_NotMaster;
	}

	bool bItsMe = rkUser.kCharGuid == rkKickCharGuid;
	if( bItsMe )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_Me"));
		return PRC_Fail_Me;
	}

	SPartyUserInfo kLeaveMember;
	if( !pkParty->GetMemberInfo(rkKickCharGuid, kLeaveMember) )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;//킥 할 사람이 없다
	}

	{
		CONT_DEFMAP const *pkDefMap = NULL;
		g_kTblDataMgr.GetContDef(pkDefMap);
		if( !pkDefMap )
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
			return PRC_Fail;
		}
		
		CONT_DEFMAP::const_iterator iter = pkDefMap->find(KickUserGroundNo);
		if( pkDefMap->end() == iter )
		{
			INFO_LOG( BM::LOG_LV0, __FL__ << _T("Cannot find GroundDef User[") << rkUser.Name().c_str() << _T("],GroundNo[") << KickUserGroundNo << _T("]") );
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
			return PRC_Fail;
		}

		bool bLocalValue = false;
		static LOCAL_MGR::NATION_CODE const eNation = static_cast<LOCAL_MGR::NATION_CODE>(g_kLocal.ServiceRegion());
		switch (eNation)
		{
		case LOCAL_MGR::NC_SINGAPORE:
		case LOCAL_MGR::NC_THAILAND:
		case LOCAL_MGR::NC_INDONESIA:
		case LOCAL_MGR::NC_VIETNAM:
		case LOCAL_MGR::NC_PHILIPPINES:
			{
				if( ((*iter).second.iAttr == GATTR_CHAOS_MISSION) )
				{
					bLocalValue = true;
				}
			}break;
		default:
			{
				if( ((*iter).second.iAttr == GATTR_MISSION) || ((*iter).second.iAttr == GATTR_CHAOS_MISSION) )
				{
					bLocalValue = true;
				}
			}break;
		}
		if( 0 != ((*iter).second.iAttr & GATTR_FLAG_SUPER) )
		{
			return PRC_Fail_KickUserNoArea;
		}

		if( true == bLocalValue )
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_KickUserNoArea"));
			return PRC_Fail_KickUserNoArea;
		}
	}

	DelCharToParty(rkKickCharGuid);
	////////////////////////////////////////////////
	PartyBuffMember(pkParty, rkKickCharGuid, false);

	pkParty->Del(rkKickCharGuid);

	int iFriendCount = 0;
	iFriendCount = pkParty->GetFriendCount(rkKickCharGuid);
	if( iFriendCount != 0 )
	{
		SContentsUser kUser;
		if( S_OK == ::GetPlayerByGuid(rkKickCharGuid, false, kUser) )
		{
			BM::CPacket kLPacket(PT_A_U_SEND_TOFRIEND_PARTY);
			kLPacket.Push(kUser.sChannel);
			kLPacket.Push(rkPartyGuid);
			kLPacket.Push(rkKickCharGuid);
			kLPacket.Push(false);
			pkParty->WriteToMemberInfoList(kLPacket);
			SendToFriendMgr(kLPacket);
		}
	}
	////////////////////////////////////////////////

	BM::CPacket kMemberPacket(PT_N_C_NFY_KICKOUT_PARTY_USER);
	kMemberPacket.Push(rkKickCharGuid);
	SendToPartyMember(pkParty, kMemberPacket);// 멤버들에게 통보
	g_kServerSetMgr.Locked_SendToUser(rkKickCharGuid, kMemberPacket, false);//킥 당한 사람에게도

	BM::CPacket kMapPacket(PT_N_M_NFY_LEAVE_PARTY_USER);
	kMapPacket.Push(pkParty->PartyGuid());
	kMapPacket.Push(rkKickCharGuid);
	kMapPacket.Push(false);//마스터 변경없다
	SendToLocalPartyMgr(pkParty, kMapPacket);//로컬파티에게 보냄
	SendToGround( kLeaveMember.kChnGndKey, kMapPacket );//킥 당한 유저의 Gnd LocalMng에도 전송

	// Log
	PgLogCont kLogCont(ELogMain_Contents_Party, ELogSub_Party);				
	kLogCont.MemberKey(rkKickMemberGuid);
	kLogCont.CharacterKey(rkKickCharGuid);

	SContentsUser kKickUser;
	std::wstring kKickID = std::wstring();
	short iChannel = 0;
	short iClass = 0;
	short iLevel = 0;
	int iGround = 0;
	int iUID = 0;
	int iGender = 0;
	if( S_OK == ::GetPlayerByGuid(rkKickCharGuid, false, kKickUser) )
	{
		kKickID = static_cast<std::wstring>(kKickUser.kAccountID);
		iChannel = static_cast<short>(kKickUser.sChannel);
		iClass = static_cast<short>(kKickUser.iClass);
		iLevel = static_cast<short>(kKickUser.sLevel);
		iGround = static_cast<int>(kKickUser.kGndKey.GroundNo());
		iUID = kKickUser.iUID;
		iGender = static_cast<int>(kKickUser.cGender);
	}
	kLogCont.UID(iUID);
	kLogCont.ID(kKickID);
	kLogCont.Name(rkKickName);
	kLogCont.ChannelNo(static_cast<short>(iChannel));
	kLogCont.Class(static_cast<short>(iClass));
	kLogCont.Level(static_cast<short>(iLevel));
	kLogCont.GroundNo(static_cast<int>(iGround));

	PgLog kLog(ELOrderMain_Party, ELOrderSub_Exile);
	kLog.Set(0, pkParty->PartyName());	// Message1
	kLog.Set(1, rkUser.Name());	// Message2
	kLog.Set(3, iGender );
	kLog.Set(2, pkParty->PartyGuid().str());	//guidValue
	kLog.Set(3, rkUser.kCharGuid.str() );// guidValue2

	kLogCont.Add(kLog);
	kLogCont.Commit();

	return true;
}

HRESULT PgGlobalPartyMgrImpl::ChangeMaster( BM::GUID const &kMasterCharGuid, BM::GUID const &kTargetCharGuid)
{
	PgGlobalParty* pkParty = GetCharToParty( kMasterCharGuid );

	try
	{
		if( !pkParty )
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_ChangeMaster"));
			throw PRC_Fail_ChangeMaster;
		}

		bool bRet = pkParty->IsMaster(kMasterCharGuid);
		if( !bRet )
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_NotMaster"));
			throw PRC_Fail_NotMaster;
		}

		HRESULT const hRet = pkParty->IsChangeMaster();
		if ( PRC_Success != hRet )
		{
			throw hRet;
		}

		bRet = pkParty->ChangeMaster(kTargetCharGuid);
		if( !bRet )
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_ChangeMaster"));
			throw PRC_Fail_ChangeMaster;
		}
	}
	catch( HRESULT eErrorCode )
	{
		BM::CPacket kErrPacket( PT_N_C_ANS_PARTY_CHANGE_MASTER, eErrorCode );
		SendToUser( pkParty, kMasterCharGuid, kErrPacket );
		return eErrorCode;
	}
	
	BM::GUID const &rkNewPartyMasterGuid = pkParty->MasterCharGuid();

	BM::CPacket kPacket(PT_N_C_ANS_PARTY_CHANGE_MASTER);
	kPacket.Push(static_cast<HRESULT>(PRC_Success));
	kPacket.Push(rkNewPartyMasterGuid);
	SendToPartyMember(pkParty, kPacket);

	SPartyUserInfo kUserInfo;
	pkParty->GetMasterInfo(kUserInfo);
	SendToGround( kUserInfo.kChnGndKey, kPacket );

	BM::CPacket kLocalGndPacket(PT_N_M_NFY_PARTY_CHANGE_MASTER);
	kLocalGndPacket.Push(pkParty->PartyGuid());
	kLocalGndPacket.Push(rkNewPartyMasterGuid);
	SendToLocalPartyMgr(pkParty, kLocalGndPacket);

	///////////////////////////////////
	PartyBuffMember(pkParty, rkNewPartyMasterGuid, true);
	///////////////////////////////////

	return PRC_Success;
}

bool PgGlobalPartyMgrImpl::MovedProcess(BM::GUID const &rkPartyGuid, SContentsUser const &rkUser, short const nChannelNo )
{
	//assert( !rkUser.Empty() );

	PgGlobalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	PartyWaitUserSend_Refuse(pkParty, rkUser);

	bool bIsWaiter = pkParty->IsWait(rkUser.kCharGuid);
	if( bIsWaiter )//파티 초대 대상자가 맵 이동
	{
		AnsJoinParty(rkPartyGuid, rkUser, false, false);//거부 한것으로 처리
	}
	else// 도착한 그라운드로 세팅
	{
		SPartyUserInfo kPartyUserInfo;
		if( pkParty->GetMemberInfo(rkUser.kCharGuid, kPartyUserInfo) )
		{
			SChnGroundKey const kChnGndKey( rkUser.kGndKey, nChannelNo );
			if( pkParty->MovedGnd(rkUser.kCharGuid, kChnGndKey) )
			{
				BM::CPacket kMemberPacket(PT_N_C_NFY_PARTY_USER_MAP_MOVE);
				kMemberPacket.Push(rkUser.kCharGuid);
				kMemberPacket.Push(rkUser.kGndKey);
				SendToPartyMember(pkParty, kMemberPacket);// 파티원들에게 통보

				BM::CPacket kOldMapPacket(PT_N_M_NFY_PARTY_USER_MAP_MOVE);
				kOldMapPacket.Push(pkParty->PartyGuid());
				kOldMapPacket.Push(rkUser.kGndKey);
				kOldMapPacket.Push(rkUser.kCharGuid);
				SendToGround( kPartyUserInfo.kChnGndKey, kOldMapPacket );//이전맵으로 통보

				SyncToLocalPartyMgr(pkParty);//맵이동 대상인 새로운 맵의 로컬파티 매니저들에게 통보

				///////////////////////////////////
				PartyBuffMember(pkParty, rkUser.kCharGuid, true);
				///////////////////////////////////
			}
		}
	}
	return true;
}

void PgGlobalPartyMgrImpl::SendToPartyMember( PgGlobalParty const *pkParty, BM::CPacket const& rkSndPacket, BM::GUID const &rkIgnore)
{
	if( !pkParty ) 
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Party is NULL"));
		return;
	}

	VEC_GUID kVec;
	pkParty->GetMemberGuidList( kVec, true, g_kProcessCfg.ChannelNo(), rkIgnore );
	if( kVec.size() )
	{
		g_kServerSetMgr.Locked_SendToUser( kVec, rkSndPacket, true );
	}

	pkParty->GetMemberGuidList( kVec, true, CProcessConfig::GetPublicChannel(), rkIgnore );
	VEC_GUID::const_iterator guid_itr = kVec.begin();
	for ( ; guid_itr != kVec.end() ; ++guid_itr )
	{
		g_kSwitchAssignMgr.SendToUser( *guid_itr, rkSndPacket );
	}
}

void PgGlobalPartyMgrImpl::SyncToLocalPartyMgr(const PgGlobalParty* pkParty)
{
	if( !pkParty ) 
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Party is NULL"));
		return;
	}


	BM::CPacket kPacket(PT_N_M_NFY_SYNC_PARTY_MEMBER, pkParty->PartyGuid());

	pkParty->WriteToPacket(kPacket);

	SendToLocalPartyMgr(pkParty, kPacket);
}

void PgGlobalPartyMgrImpl::SendToLocalPartyMgr( PgGlobalParty const *pkParty, BM::CPacket const& rkPacket )
{
	if( !pkParty ) 
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Party is NULL"));
		return;
	}

	VEC_GUID kVec;
	pkParty->GetMemberCharGuidList(kVec);
	if( !kVec.size() ) 
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! MemberCharGuidList Size is 0"));
		return;
	}

	CheckOverlap kCheck;
	VEC_GUID::iterator iter = kVec.begin();
	while(kVec.end() != iter)
	{
		SPartyUserInfo kPartyMember;
		if( pkParty->GetMemberInfo(*iter, kPartyMember) )
		{
			CheckOverlap::_Pairib bRet = kCheck.insert( kPartyMember.kChnGndKey );
			if( bRet.second )
			{
				SendToGround( kPartyMember.kChnGndKey, rkPacket);
			}
		}
		++iter;
	}
}

bool PgGlobalPartyMgrImpl::ChangeMemberAbility(BM::GUID const &rkPartyGuid, SContentsUser const &rkUser, BM::CPacket &rkPacket)
{
	//assert(BM::GUID::NullData() != rkPartyGuid);
	//assert( !rkUser.Empty() );

	PgGlobalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	BYTE cChangeAbilType = 0;
	rkPacket.Pop( cChangeAbilType );

	BYTE const cChangedAbil = pkParty->ChangeAbility(rkUser.kCharGuid, cChangeAbilType, rkPacket);
	if( 0 != cChangedAbil )
	{
		BM::CPacket kPacket(PT_M_C_NFY_CHANGE_MEMBER_ABIL, rkUser.kCharGuid);
		if( pkParty->WriteToChangedAbil(rkUser.kCharGuid, cChangedAbil, kPacket) )
		{
			SendToPartyMember(pkParty, kPacket);
		}
	}

	return true;
}

bool PgGlobalPartyMgrImpl::SendParty_Packet(BM::GUID const &rkCharGuid, BM::CPacket const &rkPacket)
{
	BM::GUID kPartyGuid;
	if( !GetCharToParty(rkCharGuid, kPartyGuid) )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	PgGlobalParty* pkParty = GetParty(kPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	SendToPartyMember(pkParty, rkPacket);
	return true;
}

HRESULT PgGlobalPartyMgrImpl::ReqCreateParty(SContentsUser const& rkMaster, BM::CPacket& rkPacket)
{
	if( rkMaster.Empty() )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
		return PRC_Fail;
	}

	BM::GUID kPartyGuid;
	bool const bMasterParty = GetCharToParty(rkMaster.kCharGuid, kPartyGuid);
	if( bMasterParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_Party"));
		return PRC_Fail_Party;//이미 파티가 있는 마스터
	}

	std::wstring	kNewPartyName;
	SPartyOption	kNewOption;

	rkPacket.Pop(kNewPartyName);
	kNewOption.ReadFromPacket(rkPacket);


	if( kNewPartyName.empty() )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
		return PRC_Fail;
	}

	if( PV_MAX_NAME_LENGTH < kNewPartyName.size() )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_Max"));
		return PRC_Fail_Max;
	}

	bool const bFiltered = g_kFilterString.Filter(kNewPartyName, false, FST_ALL);
	bool const bUniCodeFiltered = (false == g_kUnicodeFilter.IsCorrect(UFFC_PARTY_NAME, kNewPartyName));
	if( bFiltered
	||	bUniCodeFiltered )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_BadName"));
		return PRC_Fail_BadName;
	}

	SetPartyOptionAreaNo(rkMaster.kGndKey.GroundNo(), kNewOption);

	HRESULT const hCreateRet = CreateParty(kNewPartyName, kPartyGuid, kNewOption);
	if( PRC_Success != hCreateRet )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return ") << hCreateRet);
		return hCreateRet;//생성 실패
	}

	HRESULT const hMasterJoinRet = AnsJoinParty(kPartyGuid, rkMaster, true, true);//마스터를 강제로 가입
	if( PRC_Success != hMasterJoinRet )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
		Delete(kPartyGuid);
		DelCharToParty(rkMaster.kCharGuid);//실패면 마스터 파티 삭제
		return PRC_Fail;
	}

	////////////////////////////////////////////////
	EPartyReturnCode eRet = PRC_Fail_NoMaster;
	bool const bIsParty = GetCharToParty(rkMaster.kCharGuid, kPartyGuid);
	if( bIsParty )
	{
		PgGlobalParty* pkParty = GetParty(kPartyGuid);
		if( pkParty )
		{
			bool const bIsMaster = pkParty->IsMaster(rkMaster.kCharGuid);
			if( bIsMaster )
			{
				eRet = PRC_Success;
				pkParty->Option(kNewOption);

				SyncToLocalPartyMgr(pkParty);

				SendToPartyMember(pkParty, rkPacket);

				// Log
				PgLogCont kLogCont(ELogMain_Contents_Party, ELogSub_Party);				
				kLogCont.MemberKey(rkMaster.kMemGuid);
				kLogCont.UID(rkMaster.iUID);
				kLogCont.CharacterKey(rkMaster.kCharGuid);
				kLogCont.ID(rkMaster.kAccountID);
				kLogCont.Name(rkMaster.Name());
				kLogCont.ChannelNo(static_cast<short>(rkMaster.sChannel));
				kLogCont.Class(static_cast<short>(rkMaster.iClass));
				kLogCont.Level(static_cast<short>(rkMaster.sLevel));
				kLogCont.GroundNo(static_cast<int>(rkMaster.kGndKey.GroundNo()));

				PgLog kLog(ELOrderMain_Party, ELOrderSub_Create);
				kLog.Set(0, static_cast<std::wstring>(pkParty->PartyName()));
				std::wstring strPartyInfo = PgLogUtil::GetPartyOptionPublic(pkParty->Option().GetOptionPublicTitle());
				strPartyInfo += _T(" ");
				strPartyInfo += PgLogUtil::GetPartyOptionItemString(pkParty->Option().GetOptionItem());
				kLog.Set(1, strPartyInfo);
				kLog.Set(0, static_cast<int>(pkParty->Option().GetOptionPublicTitle()));
				kLog.Set(1, static_cast<int>(pkParty->Option().GetOptionItem()));
				kLog.Set(2, static_cast<int>(pkParty->Option().GetOptionLevel()));
				kLog.Set(3, static_cast<int>(rkMaster.cGender) );
				kLog.Set(2, pkParty->PartyGuid().str());

				kLogCont.Add(kLog);
				kLogCont.Commit();

				BM::CPacket kPacket(PT_N_C_ANS_PARTY_CHANGE_MASTER);
				kPacket.Push(PRC_Success);
				kPacket.Push(rkMaster.kCharGuid);
				g_kServerSetMgr.Locked_SendToGround(rkMaster.kGndKey, kPacket, true);
			}
		}
	}

	if( PRC_Success != eRet )//실패시 마스터만
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
		return PRC_Fail;
	}
	////////////////////////////////////////////////

	return PRC_Success;
}

HRESULT PgGlobalPartyMgrImpl::ReqRenameParty( BM::GUID const &rkCharGuid, BM::CPacket& rkPacket )
{
	PgGlobalParty* pkParty = GetCharToParty( rkCharGuid );
	if( pkParty )
	{
		return ReqRenameParty( pkParty, rkCharGuid, rkPacket );
	}

	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_NotMaster"));
	return PRC_Fail_NotMaster;
}

HRESULT PgGlobalPartyMgrImpl::ReqChangeOptionParty( SContentsUser const &kMaster, BM::CPacket &rkPacket )
{
	PgGlobalParty* pkParty = GetCharToParty( kMaster.kCharGuid );
	if( pkParty )
	{
		if ( PRC_Success == ReqRenameParty( pkParty, kMaster.kCharGuid, rkPacket ) )
		{
			SPartyOption kNewOption;
			kNewOption.ReadFromPacket( rkPacket );
			pkParty->Option(kNewOption);

			BM::CPacket kPacket(PT_N_C_ANS_PARTY_CHANGE_OPTION);
			kPacket.Push((BYTE)PRC_Success);
			kNewOption.WriteToPacket(kPacket);

			SyncToLocalPartyMgr(pkParty);						
			SendToPartyMember(pkParty, kPacket);

			// Log
			PgLogCont kLogCont(ELogMain_Contents_Party, ELogSub_Party);				
			kLogCont.MemberKey(kMaster.kMemGuid);
			kLogCont.UID(kMaster.iUID);
			kLogCont.CharacterKey(kMaster.kCharGuid);
			kLogCont.ID(kMaster.kAccountID);
			kLogCont.Name(kMaster.Name());
			kLogCont.ChannelNo(kMaster.sChannel);
			kLogCont.Class(static_cast<short>(kMaster.iClass));
			kLogCont.Level(static_cast<short>(kMaster.sLevel));
			kLogCont.GroundNo(static_cast<int>(kMaster.kGndKey.GroundNo()));

			PgLog kLog(ELOrderMain_Party, ELOrderSub_Modify);
			kLog.Set(0, static_cast<std::wstring>(pkParty->PartyName()));
			std::wstring strPartyInfo = PgLogUtil::GetPartyOptionPublic(pkParty->Option().GetOptionPublicTitle());
			strPartyInfo += _T(" ");
			strPartyInfo += PgLogUtil::GetPartyOptionItemString(pkParty->Option().GetOptionItem());
			kLog.Set(1, strPartyInfo);
			kLog.Set(0, static_cast<int>(pkParty->Option().GetOptionPublicTitle()));
			kLog.Set(1, static_cast<int>(pkParty->Option().GetOptionItem()));
			kLog.Set(2, static_cast<int>(pkParty->Option().GetOptionLevel()));
			kLog.Set(3, static_cast<int>(kMaster.cGender) );
			kLog.Set(2, pkParty->PartyGuid().str());
			kLogCont.Add(kLog);
			kLogCont.Commit();
			return PRC_Success;
		}
	}

	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_NotMaster"));
	return PRC_Fail_NotMaster;
}

HRESULT PgGlobalPartyMgrImpl::ReqRenameParty( PgGlobalParty * pkParty, BM::GUID const &rkCharGuid, BM::CPacket& rkPacket )
{
	try
	{
		bool const bMaster = pkParty->IsMaster( rkCharGuid );
		if( !bMaster )
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_NotMaster"));
			throw PRC_Fail_NotMaster;
		}

		HRESULT const hRet = pkParty->IsChangeOption();
		if ( PRC_Success != hRet )
		{
			throw hRet;
		}

		bool bClear = false;
		std::wstring kNewPartyName;
		rkPacket.Pop(kNewPartyName);
		rkPacket.Pop(bClear);

		if( kNewPartyName.empty()
			&&	!bClear )
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail"));
			throw PRC_Fail;
		}

		if( PV_MAX_NAME_LENGTH < kNewPartyName.size()
			&&	!bClear )
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_Max"));
			throw PRC_Fail_Max;
		}

		bool const bFiltered = g_kFilterString.Filter(kNewPartyName, false, FST_ALL);
		bool const bUniCodeFiltered = (false == g_kUnicodeFilter.IsCorrect(UFFC_PARTY_NAME, kNewPartyName));
		if( bFiltered
			||	bUniCodeFiltered )
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_BadName"));
			throw PRC_Fail_BadName;
		}

		PartyNameSet::iterator name_iter = m_kContPartyNameSet.find(kNewPartyName);
		if( m_kContPartyNameSet.end() != name_iter
			&&	!bClear )
		{
			if( pkParty->PartyName() != kNewPartyName )
			{
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_Dup_Name"));
				throw PRC_Fail_Dup_Name;
			}
		}

		if( !bClear )
		{
			m_kContPartyNameSet.erase(pkParty->PartyName());//기존 이름 해제
			pkParty->PartyName(kNewPartyName);
			m_kContPartyNameSet.insert(kNewPartyName);
		}
		else
		{
			//GM 초기화이면 해제 해 주지 않는다.(기존의 이름이 욕설일 수도 있으니깐)
			pkParty->PartyName(std::wstring());//Clear Name
		}
	}
	catch ( HRESULT eErrorCode )
	{
		BM::CPacket kErrPacket( PT_N_C_ANS_PARTY_RENAME, eErrorCode );
		SendToUser( pkParty, rkCharGuid, kErrPacket );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! ReqRenameParty isn't Success"));
		return eErrorCode;
	}

	BM::CPacket kLocalPacket(PT_N_M_NFY_PARTY_RENAME);
	kLocalPacket.Push(pkParty->PartyGuid());
	kLocalPacket.Push(pkParty->PartyName());
	pkParty->Option().WriteToPacket(kLocalPacket);
	SendToLocalPartyMgr(pkParty, kLocalPacket);
	return PRC_Success;
}

bool PgGlobalPartyMgrImpl::Delete(BM::GUID const &rkPartyGuid)
{
	PgGlobalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	PartyNameSet::iterator iter = m_kContPartyNameSet.find(pkParty->PartyName());

	bool const bDelRet = PgPartyMgr< PgGlobalParty >::Delete(rkPartyGuid);
	if( bDelRet )
	{
		if( m_kContPartyNameSet.end() != iter )
		{
			m_kContPartyNameSet.erase(iter);
		}
	}
	return bDelRet;
}

void PgGlobalPartyMgrImpl::DeleteLog(SContentsUser const &rkCaster, BM::GUID const &rkPartyGuid)
{
	PgGlobalParty* pkParty = GetParty(rkPartyGuid);
	if( pkParty )
	{
		// Log
		PgLogCont kLogCont(ELogMain_Contents_Party, ELogSub_Party);				
		kLogCont.MemberKey(rkCaster.kMemGuid);
		kLogCont.UID(rkCaster.iUID);
		kLogCont.CharacterKey(rkCaster.kCharGuid);
		kLogCont.ID(rkCaster.kAccountID);
		kLogCont.Name(rkCaster.Name());
		kLogCont.ChannelNo(static_cast<short>(rkCaster.sChannel));
		kLogCont.Class(static_cast<short>(rkCaster.iClass));
		kLogCont.Level(static_cast<short>(rkCaster.sLevel));
		kLogCont.GroundNo(static_cast<int>(rkCaster.kGndKey.GroundNo()));

		PgLog kLog(ELOrderMain_Party, ELOrderSub_Delete);
		kLog.Set(0, static_cast<std::wstring>(pkParty->PartyName()));
		std::wstring strPartyInfo = PgLogUtil::GetPartyOptionPublic(pkParty->Option().GetOptionPublicTitle());
		strPartyInfo += _T(" ");
		strPartyInfo += PgLogUtil::GetPartyOptionItemString(pkParty->Option().GetOptionItem());
		kLog.Set(1, strPartyInfo);
		kLog.Set(0, static_cast<int>(pkParty->Option().GetOptionPublicTitle()));
		kLog.Set(1, static_cast<int>(pkParty->Option().GetOptionItem()));
		kLog.Set(2, static_cast<int>(pkParty->Option().GetOptionLevel()));
		kLog.Set(3, static_cast<int>(rkCaster.cGender) );
		kLog.Set(2, pkParty->PartyGuid().str());

		kLogCont.Add(kLog);
		kLogCont.Commit();
	}
}

HRESULT PgGlobalPartyMgrImpl::CreateParty(std::wstring const& rkPartyName, BM::GUID& rkOutGuid, SPartyOption const& kNewOption)
{
	if( !rkPartyName.empty() )//파티명이 없으면(ReqJoin으로 생성된 파티)
	{
		PartyNameSet::iterator name_iter = m_kContPartyNameSet.find(rkPartyName);
		if( m_kContPartyNameSet.end() != name_iter )//같은 이름이 있는가
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return PRC_Fail_Dup_Name"));
			return PRC_Fail_Dup_Name;//이름 중복
		}
	}

	HRESULT const hRet = PgPartyMgr< PgGlobalParty >::CreateParty(rkPartyName, rkOutGuid, kNewOption);
	if( PRC_Success != hRet )
	{
		return hRet;
	}

	if( !rkPartyName.empty() )
	{
		PartyNameSet::_Pairib kNameRet = m_kContPartyNameSet.insert(rkPartyName);
		//assert(kNameRet.second);
	}
	return hRet;
}

bool PgGlobalPartyMgrImpl::SummonMember(BM::GUID const &rkPartyGuid, SContentsUser const &rkCaster)
{
	if( rkCaster.Empty() )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	PgGlobalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	VEC_GUID kCharGuids;
	pkParty->GetMemberCharGuidList(kCharGuids, rkCaster.kCharGuid);

	BM::CPacket kPacket(PT_N_M_ANS_PARTY_COMMAND);
	kPacket.Push(rkCaster.kCharGuid);
	kPacket.Push((BYTE)PC_Summon_Member);
	kPacket.Push(kCharGuids);
	g_kServerSetMgr.Locked_SendToGround(rkCaster.kGndKey, kPacket);
	return true;
}

void PgGlobalPartyMgrImpl::PartyBuffMember(PgGlobalParty* pkParty, BM::GUID const &rkCharGuid, bool bBuff)
{
	if( !pkParty )
	{
		INFO_LOG(BM::LOG_LV0, __FL__ << _T("Cannot find Party Info Null Char=") << rkCharGuid);
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Party is NULL"));
		return;
	}

	VEC_GUID kCharGuids;
	pkParty->GetMemberCharGuidList(kCharGuids);	

	SPartyUserInfo kPartyMaster;
	if( pkParty->GetMemberInfo(pkParty->MasterCharGuid(), kPartyMaster) )
	{
		VEC_GUID::iterator iter = kCharGuids.begin();
		while( kCharGuids.end() != iter )
		{
			SPartyUserInfo kPartyMember;

			if( pkParty->GetMemberInfo((*iter), kPartyMember) )
			{		
				BM::CPacket kPacket(PT_N_M_REQ_PARTY_BUFF);
				kPacket.Push(bBuff);
				kPacket.Push(kPartyMaster.GroundNo());
				kPacket.Push(pkParty->PartyGuid());
				kPacket.Push(rkCharGuid);
				kPacket.Push(kPartyMember.kCharGuid);
				pkParty->WriteToMemberInfoList(kPacket);

				SendToGround( kPartyMember.kChnGndKey, kPacket );
			}
			++iter;
		}
	}
}

bool PgGlobalPartyMgrImpl::GetMemberGroundKey( BM::GUID const & rkCharGuid, SChnGroundKey & rkChnGndKey )const
{
	BM::GUID kPartyGuid;
	if(!GetCharToParty(rkCharGuid,kPartyGuid))
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	PgGlobalParty* pkParty = GetParty(kPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	SPartyUserInfo kPartyUserInfo;
	if(!pkParty->GetMemberInfo(rkCharGuid,kPartyUserInfo))
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	rkChnGndKey = kPartyUserInfo.kChnGndKey;
	return true;
}

bool PgGlobalPartyMgrImpl::LogPartyAction( PgParty* const pkParty,  SContentsUser const& kUserInfo, EOrderSubType const kType )const
{
	PgLogCont kLogCont(ELogMain_Contents_Party, ELogSub_Party);				
	kLogCont.MemberKey(kUserInfo.kMemGuid);
	kLogCont.UID(kUserInfo.iUID);
	kLogCont.CharacterKey(kUserInfo.kCharGuid);
	kLogCont.ID(kUserInfo.kAccountID);
	kLogCont.Name(kUserInfo.kName);
	kLogCont.ChannelNo( kUserInfo.sChannel);
	kLogCont.Class(static_cast<short>(kUserInfo.iClass));
	kLogCont.Level(static_cast<short>(kUserInfo.sLevel));
	kLogCont.GroundNo(static_cast<int>(kUserInfo.kGndKey.GroundNo()));

	PgLog kLog(ELOrderMain_Party, kType);
	switch ( kType )
	{
	case ELOrderSub_Join:
		{
			std::wstring strPartyInfo = PgLogUtil::GetPartyOptionPublic(pkParty->Option().GetOptionPublicTitle());
			strPartyInfo += _T(" ");
			strPartyInfo += PgLogUtil::GetPartyOptionItemString(pkParty->Option().GetOptionItem()
				);
			kLog.Set(3, static_cast<int>(kUserInfo.cGender) );
			kLog.Set(1, strPartyInfo);
			kLog.Set(0, static_cast<std::wstring>(pkParty->PartyName()));
			kLog.Set(2, pkParty->PartyGuid().str());
		}break;
	case ELOrderSub_Break:
		{
			kLog.Set(3, static_cast<int>(kUserInfo.cGender) );
			kLog.Set(0, static_cast<std::wstring>(pkParty->PartyName()));
			kLog.Set(2, pkParty->PartyGuid().str());
			
		}break;
	default:
		{
			CAUTION_LOG( BM::LOG_LV1, __FL__ << _T("Unknown Type=") << static_cast<int>(kType) );
			return false;
		}break;
	}

	kLogCont.Add(kLog);
	kLogCont.Commit();
	return true;
}

typedef struct tagSFunctionFindPartyListForEach : public PgGlobalPartyMgrImpl::SFunctionForEach
{
	tagSFunctionFindPartyListForEach(BM::CPacket &rkPacket, BM::CPacket& rkSendPacket)
		: m_rkPacket(rkPacket), m_rkSendPacket(rkSendPacket)
	{
	}

	bool GetPartyMasterGroundAttribute(PgGlobalParty* pkParty, int &rkAtrribute)
	{
		if( pkParty )
		{
			SPartyUserInfo kMaster;
			if( true == pkParty->GetMasterInfo(kMaster) )
			{
				CONT_DEFMAP const* pkDefMap = NULL;
				g_kTblDataMgr.GetContDef(pkDefMap);

				if( pkDefMap )
				{
					CONT_DEFMAP::const_iterator find_iter = pkDefMap->find(kMaster.GroundNo());
					if( pkDefMap->end() != find_iter )
					{
						rkAtrribute = (*find_iter).second.iAttr;
						return true;
					}
				}
			}
		}
		return false;
	}

	bool PartySearchCheck(PgGlobalParty* pkParty, int iPartyAttribute, int iPartyContinent, int iPartyArea_NameNo)
	{
		if( pkParty )
		{
			int iGroundAttrubute = 0;
			if( false == GetPartyMasterGroundAttribute(pkParty, iGroundAttrubute) )
			{
				return false;
			}

			if( (GATTR_FLAG_CANT_WARP | GATTR_FLAG_NOPARTY) & iGroundAttrubute )
			{
				return false;
			}

			if( (iPartyAttribute == pkParty->Option().GetOptionAttribute()) || (0 == iPartyAttribute) )
			{
				if( iPartyContinent == pkParty->Option().GetOptionContinent() )
				{
					if( (iPartyArea_NameNo == pkParty->Option().GetOptionArea_NameNo()) || (0 == iPartyAttribute) )
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	virtual void operator() (PgGlobalPartyMgrImpl::PartyHash const& rkContParty)
	{
		int iPartyAttribute = 0;
		int iPartyContinent = 0;
		int iPartyArea_NameNo = 0;

		m_rkPacket.Pop(iPartyAttribute);
		m_rkPacket.Pop(iPartyContinent);
		m_rkPacket.Pop(iPartyArea_NameNo);

		size_t const iRetWrPos = m_rkSendPacket.WrPos();
		m_rkSendPacket.Push( static_cast< BYTE >(PRC_Fail_NoMaster) ); // 없는걸 기본으로
		size_t const iCountWrPos = m_rkSendPacket.WrPos();
		int iCount = 0;
		m_rkSendPacket.Push( iCount );

		PgGlobalPartyMgrImpl::PartyHash::const_iterator iter = rkContParty.begin();
		while( rkContParty.end() != iter )
		{
			PgGlobalPartyMgrImpl::PartyHash::mapped_type const pkParty = iter->second;
			if( pkParty )
			{
				if( pkParty->Option().GetOptionPublicTitle() == POT_Public
					&&	pkParty->MaxMemberCount() > pkParty->MemberCount() && PartySearchCheck(pkParty, iPartyAttribute, iPartyContinent, iPartyArea_NameNo) )
				{
					m_rkSendPacket.Push( pkParty->PartyGuid() );
					m_rkSendPacket.Push( pkParty->PartyName() );
					m_rkSendPacket.Push( static_cast< BYTE >(pkParty->MemberCount()) );
					m_rkSendPacket.Push( static_cast< BYTE >(pkParty->MaxMemberCount()) );
					m_rkSendPacket.Push( pkParty->MasterCharGuid() );
					m_rkSendPacket.Push( pkParty->Option().iPartyOption );
					m_rkSendPacket.Push( pkParty->Option().iPartyLevel );
					m_rkSendPacket.Push( pkParty->Option().PartySubName() );
					m_rkSendPacket.Push( pkParty->Option().iPartyAttribute );
					m_rkSendPacket.Push( pkParty->Option().iPartyContinent );
					m_rkSendPacket.Push( pkParty->Option().iPartyArea_NameNo );

					VEC_GUID kVec;
					pkParty->GetMemberCharGuidList(kVec);
					int iMemberSize = static_cast<int>(PV_MAX_MEMBER_CNT-1);
					int iMemberCount = 0;

					SPartyUserInfo kPartyMember;
					VEC_GUID::iterator Member_iter = kVec.begin();
					while(kVec.end() != Member_iter)
					{
						kPartyMember.Clear();

						if( pkParty->GetMemberInfo(*Member_iter, kPartyMember) )
						{
							kPartyMember.WriteToPacket(m_rkSendPacket);
							++iMemberCount;
						}
						++Member_iter;
					}
					for(int i=iMemberCount; i<iMemberSize; ++i)
					{
						kPartyMember.Clear();
						kPartyMember.WriteToPacket(m_rkSendPacket);
					}

					++iCount;
					if( PV_MAX_PARTY_LIST_CNT <= iCount )
					{
						break;
					}
				}
			}

			++iter;
		}

		if( 0 < iCount )
		{
			BYTE const ucRet = PRC_Success;
			m_rkSendPacket.ModifyData(iRetWrPos, &ucRet, sizeof(ucRet));
			m_rkSendPacket.ModifyData(iCountWrPos, &iCount, sizeof(iCount));
		}
	}
private:
	BM::CPacket& m_rkPacket;
	BM::CPacket& m_rkSendPacket;
} SFunctionFindPartyListForEach;

void PgGlobalPartyMgrImpl::WritePartyListInfoToPacket(BM::CPacket &rkPacket, BM::CPacket& rkSendPacket)
{
	ForEach( SFunctionFindPartyListForEach(rkPacket, rkSendPacket) );
}

bool PgGlobalPartyMgrImpl::GetPartyMemberList( VEC_GUID &rkOutCharGuidList, BM::GUID const &kGuid, bool const bPartyGuid, bool const bIgnoreCharGuid )const
{
	BM::GUID kPartyGuid = kGuid;
	if ( !bPartyGuid )
	{
		if ( !GetCharToParty( kGuid, kPartyGuid ) )
		{
			return false;
		}
	}

	PgGlobalPartyMgrImpl *pkGlobalPartyMgr = const_cast<PgGlobalPartyMgrImpl*>(this);
	PgGlobalParty *pkParty = pkGlobalPartyMgr->GetParty( kPartyGuid );
	if ( pkParty )
	{
		pkParty->GetMemberCharGuidList( rkOutCharGuidList, (true == bIgnoreCharGuid ? kGuid : BM::GUID::NullData()) );
		return true;
	}
	return false;
}

bool PgGlobalPartyMgrImpl::PartyBuffRefresh(BM::GUID const &kGuid, BM::GUID const &rkPartyGuid)
{
	SContentsUser kUser;
	if( S_OK == ::GetPlayerByGuid(kGuid, false, kUser) )
	{
		PgGlobalParty* pkParty = GetParty(rkPartyGuid);
		if( !pkParty )
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}

		PartyBuffMember(pkParty, kUser.kCharGuid, true);
	}
	return true;
}

bool PgGlobalPartyMgrImpl::SendToGround( SChnGroundKey const &kChnGndkey, BM::CPacket const &kPacket )const
{
	if ( kChnGndkey.IsEmpty() )
	{
		return false;
	}

	return ::SendToChannelGround( kChnGndkey.Channel(), static_cast<SGroundKey const>(kChnGndkey), kPacket );
}

bool PgGlobalPartyMgrImpl::SendToUser( PgParty *pkParty, BM::GUID const &rkGuid, BM::CPacket const &kPacket )const
{
	SPartyUserInfo kUserInfo;
	if ( pkParty && pkParty->GetMemberInfo( rkGuid, kUserInfo ) )
	{
		return SendToUser( kUserInfo.kChnGndKey.Channel(), kUserInfo.kMemberGuid, kPacket );
	}
	return false;
}

bool PgGlobalPartyMgrImpl::SendToUser( short const nChannel, BM::GUID const &kMemberGuid, BM::CPacket const &kPacket )const
{
	if ( nChannel )
	{
		return g_kServerSetMgr.Locked_SendToUser( kMemberGuid, kPacket, true );//나간사람에게
	}

	return g_kSwitchAssignMgr.SendToUser( kMemberGuid, kPacket );
}
//
//
PgGlobalPartyMgr::PgGlobalPartyMgr()
{
}
PgGlobalPartyMgr::~PgGlobalPartyMgr()
{
}
bool PgGlobalPartyMgr::ProcessMsg(SEventMessage *pkMsg)
{
	BM::CAutoMutex kLock(m_kMutex_Wrapper_, true);
	return Instance()->ProcessMsg(pkMsg);
}
bool PgGlobalPartyMgr::GetPartyMemberList( VEC_GUID &rkOutCharGuidList, BM::GUID const &kGuid, bool const bPartyGuid, bool const bIgnoreCharGuid)const
{
	BM::CAutoMutex kLock(m_kMutex_Wrapper_);
	return Instance()->GetPartyMemberList(rkOutCharGuidList, kGuid, bPartyGuid, bIgnoreCharGuid);
}

void PgGlobalPartyMgr::WritePartyListInfoToPacket(BM::CPacket &rkPacket, BM::CPacket& rkSendPacket)
{
	BM::CAutoMutex kLock(m_kMutex_Wrapper_);
	Instance()->WritePartyListInfoToPacket(rkPacket, rkSendPacket);
}