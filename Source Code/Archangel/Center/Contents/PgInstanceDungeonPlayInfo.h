#ifndef CENTER_CENTERSERVER_CONTENTS_PGINSTANCEDUNGEONPLAYINFO_H
#define CENTER_CENTERSERVER_CONTENTS_PGINSTANCEDUNGEONPLAYINFO_H

#include "Lohengrin/PacketStruct4Map.h"
#include "Variant/PgMonster.h"

typedef struct tagIndunPlayerInfo
{
	static size_t const ms_unSelectBoxNum = 0xFFFFFFFF;

	tagIndunPlayerInfo( BM::GUID const &kMemGuid=BM::GUID::NullData(), __int64 const iExp=0, TCHAR const* szName=NULL )
		:	kMemberGuid(kMemGuid)
		,	iNowExp(iExp)
		,	uiSelectBoxNum(ms_unSelectBoxNum)
	{
		if ( szName )
		{
			wstrName = szName;
		}
	}

	bool operator==( const tagIndunPlayerInfo& rkPair )
	{
		return kMemberGuid == rkPair.kMemberGuid;
	}

	tagIndunPlayerInfo( const tagIndunPlayerInfo& rkPair )
	{
		kMemberGuid = rkPair.kMemberGuid;
		iNowExp = rkPair.iNowExp;
		uiSelectBoxNum = rkPair.uiSelectBoxNum;
		wstrName = rkPair.wstrName;
		kResultItemList = rkPair.kResultItemList;
//		kBonus = rkPair.kBonus;
	}

	tagIndunPlayerInfo &operator=( const tagIndunPlayerInfo& rkPair )
	{
		kMemberGuid = rkPair.kMemberGuid;
		iNowExp = rkPair.iNowExp;
		uiSelectBoxNum = rkPair.uiSelectBoxNum;
		wstrName = rkPair.wstrName;
		kResultItemList = rkPair.kResultItemList;
//		kBonus = rkPair.kBonus;
		return *this;
	}

	BM::GUID			kMemberGuid;
	__int64				iNowExp;
	size_t				uiSelectBoxNum;
	std::wstring		wstrName;
	CONT_ITEM_CREATE_ORDER kResultItemList;
//	SConResultBonus		kBonus;

} SINDUNPLAYINFO;

class PgInstanceDungeonPlayInfo
{
public:
	static size_t const ms_iRESULTSELECT_MAXCOUNT = 4;
	static size_t const ms_iRESULTITEM_MAXCOUNT = 4;
	static DWORD const	ms_dwRESULTSELECT_WAIT_TIME	= 28000;//19000;

	PgInstanceDungeonPlayInfo();
	virtual ~PgInstanceDungeonPlayInfo();

	void Init( SGroundKey const rkKey );
	SGroundKey const& GetKey()const{	return m_kKey;	}
	void Clear();

	bool Join(const SNfyJoinIndun& kJoinInfo );
	void Leave( BM::GUID const &kCharGuid );
	DWORD GetTime()	{	return m_dwTime;	}

	bool SetSelectBox( size_t iSelect, BM::GUID const &kCharGuid );
	bool ResultStart( VEC_RESULTINDUN const& rkResultList );
	void ResultEnd();
	void BroadCast( BM::CPacket const& rkPacket );
	bool SetResultItem( const SNfyResultItemList& rkResultItem );

	bool IsStart()const{return m_bResultStart;}
//	bool SetResultBonus( SConResultBonus& rkBonus );

public:
	typedef std::map< BM::GUID, SINDUNPLAYINFO>		CONT_PLAYER;	// first key : Character GUID, second key : Info
	typedef std::vector< BM::GUID >					CONT_SELECT;

protected:
	Loki::Mutex			m_kMutex;

	DWORD				m_dwTime;
	SGroundKey			m_kKey;
	CONT_PLAYER			m_kPlayerInfo;
	bool				m_bRecvResultItem;
	bool				m_bResultStart;		// 결과 시작이 되었느냐?

	CONT_SELECT			m_kSelectBox;
	int					m_iMaxItemCount;	// 최대 아이템 갯수
	
};

#endif // CENTER_CENTERSERVER_CONTENTS_PGINSTANCEDUNGEONPLAYINFO_H