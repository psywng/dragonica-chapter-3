#ifndef CENTER_CENTERSERVER_CONTENTS_PGGLOBALPARTYMGR_H
#define CENTER_CENTERSERVER_CONTENTS_PGGLOBALPARTYMGR_H

#include "Lohengrin/ProcessConfig.h"

class PgGlobalPartyMgr_Base
{
public:
	PgGlobalPartyMgr_Base(){}
	virtual ~PgGlobalPartyMgr_Base(){}

	virtual bool ProcessMsg(SEventMessage *pkMsg) = 0;
	virtual bool GetPartyMemberList( VEC_GUID &rkOutCharGuidList, BM::GUID const &kGuid, bool const bPartyGuid=false, bool const bIgnoreCharGuid=false )const = 0;
};

class PgGlobalPartyMgrImpl
	: public PgPartyMgr< PgGlobalParty >
{
	typedef std::set< std::wstring > PartyNameSet;
	typedef std::set< SChnGroundKey > CheckOverlap;
	typedef std::map< BM::GUID, BM::GUID > ContCharToParty;
	typedef std::vector< SPartyUserInfo > VEC_UserInfo;
public:

	PgGlobalPartyMgrImpl();
	virtual ~PgGlobalPartyMgrImpl();

	bool ProcessMsg(SEventMessage *pkMsg);
	bool GetPartyMemberList( VEC_GUID &rkOutCharGuidList, BM::GUID const &kGuid, bool const bPartyGuid=false, bool const bIgnoreCharGuid=false )const;
	void WritePartyListInfoToPacket(BM::CPacket &rkPacket, BM::CPacket& rkSendPacket);

protected:
	bool PartyBuffRefresh(BM::GUID const &kGuid, BM::GUID const &rkPartyGuid);
	HRESULT CheckJoinParty(SContentsUser const& rkMaster, SContentsUser const& rkUser);

	void SyncToLocalPartyMgr(const PgGlobalParty* pkParty);
	void SendToLocalPartyMgr(const PgGlobalParty* pkParty, BM::CPacket const& rkPacket);

	// send packet interface.
	void SendToPartyMember( PgGlobalParty const * pkParty, BM::CPacket const& rkSndPacket, BM::GUID const &rkIgnore = BM::GUID::NullData());// ��Ƽ����鿡�� ����

	HRESULT ReqCreateParty(SContentsUser const& rkMaster, BM::CPacket& rkPacket);
	HRESULT ReqChangeOptionParty( SContentsUser const &kMaster, BM::CPacket &rkPacket );
	HRESULT ReqRenameParty( BM::GUID const &rkCharGuid, BM::CPacket& rkPacket);
	HRESULT ReqRenameParty( PgGlobalParty * pkParty, BM::GUID const &rkCharGuid, BM::CPacket& rkPacket );
	HRESULT ReqJoinParty(SContentsUser const& rkMaster, SContentsUser const& rkUser, SPartyOption& rkOption);
	HRESULT ReqJoinPartyFind(SContentsUser const& rkMaster, SContentsUser const& rkUser, SPartyOption const& rkOption);
	HRESULT AnsJoinParty(BM::GUID const &rkPartyGuid, SContentsUser const& rkNewPartyUserInfo, bool const bWantJoin, bool const bMaster);
	HRESULT LeaveParty( BM::GUID const &kCharGuid );
	HRESULT KickUser(BM::GUID const &rkPartyGuid, SContentsUser const &rkUser, BM::GUID const &rkKickCharGuid, int const &KickUserGroundNo, BM::GUID const &rkKickMemberGuid, std::wstring const& rkKickName);
	HRESULT ChangeMaster( BM::GUID const &kMasterCharGuid, BM::GUID const &kTargetCharGuid);

	bool MovedProcess( BM::GUID const &rkPartyGuid, SContentsUser const &rkUser, short const nChannelNo );
	void DestroyParty(BM::GUID const &rkPartyGuid);
	void PartyWaitUserSend(PgGlobalParty* pkParty, SContentsUser const &rkUser);
	void PartyWaitUserSend_Refuse(PgGlobalParty* pkParty, SContentsUser const &rkUser);

	bool MasterCancelJoining(SContentsUser const &rkMaster);
	bool ChangeMemberAbility(BM::GUID const &rkPartyGuid, SContentsUser const &rkUser, BM::CPacket &rkPacket);
	bool SummonMember(BM::GUID const &rkPartyGuid, SContentsUser const &rkCaster);

	bool SendParty_Packet(BM::GUID const &rkCharGuid, BM::CPacket const &rkPacket);

	bool GetCharToParty(BM::GUID const &rkCharGuid, BM::GUID& rkOutGuid) const;
	bool AddCharToParty(BM::GUID const &rkCharGuid, BM::GUID const &rkPartyGuid);
	BM::GUID DelCharToParty(BM::GUID const &rkCharGuid);
	bool FindCharToParty(BM::GUID const &rkCharGuid) const;

	PgGlobalParty * GetCharToParty(BM::GUID const &rkCharGuid )const;

	bool PartyFriendCheck(BM::CPacket* pkPacket);
	bool PartyFriendCheckInfo(BM::CPacket* pkPacket);
	void PartyBuffMember(PgGlobalParty* pkParty, BM::GUID const &rkCharGuid, bool bBuff);
	bool GetMemberGroundKey(BM::GUID const & rkCharGuid,SChnGroundKey & kGndKey)const;
	void DeleteLog(SContentsUser const &rkCaster, BM::GUID const &rkPartyGuid);
	bool LogPartyAction( PgParty* const pkParty, SContentsUser const& kUserInfo, EOrderSubType const kType )const;
	void SetPartyOptionAreaNo(int const iGroundNo, SPartyOption& rkOption);

protected:
	void DestroyParty( PgGlobalParty * pkParty );

	virtual bool Delete(BM::GUID const &rkPartyGuid);
	virtual HRESULT CreateParty(std::wstring const& rkPartyName, BM::GUID& rkOutGuid, SPartyOption const& rkOption);

	bool SendToGround( SChnGroundKey const &kChnGndkey, BM::CPacket const &kPacket )const;
	bool SendToUser( PgParty *pkParty, BM::GUID const &rkGuid, BM::CPacket const &kPacket )const;
	bool SendToUser( short const nChannel, BM::GUID const &kMemberGuid, BM::CPacket const &kPacket )const;

protected:
	ContCharToParty m_kCharToParty;//Character Guid -> Party Guid
	PartyNameSet m_kContPartyNameSet;//Party Name Set
};

class PgGlobalPartyMgr
	: public PgGlobalPartyMgr_Base
	, public TWrapper< PgGlobalPartyMgrImpl >
{
public:
	PgGlobalPartyMgr();
	virtual ~PgGlobalPartyMgr();

	virtual bool ProcessMsg(SEventMessage *pkMsg);
	virtual bool GetPartyMemberList( VEC_GUID &rkOutCharGuidList, BM::GUID const &kGuid, bool const bPartyGuid=false, bool const bIgnoreCharGuid=false )const;
	virtual void WritePartyListInfoToPacket(BM::CPacket &rkPacket, BM::CPacket& rkSendPacket);
};

//
class PgGlobalPartyMgr_Public
{
public:
	typedef std::map< BM::GUID, short >		CONT_PARTY_CHANNEL;

public:
	PgGlobalPartyMgr_Public();
	virtual ~PgGlobalPartyMgr_Public();
	
	HRESULT Add( BM::GUID const &kCharGuid, short const nChannelNo );
	short Del( BM::GUID const &kCharGuid );
	short GetChannel( BM::GUID const &kCharGuid )const;

private:
	CONT_PARTY_CHANNEL		m_kContPartyChannel;
};

class PgGlobalPartyMgr_Public_Wrapper
	:	public PgGlobalPartyMgr_Base
	,	public	TWrapper<PgGlobalPartyMgr_Public>
{
public:
	PgGlobalPartyMgr_Public_Wrapper();
	virtual ~PgGlobalPartyMgr_Public_Wrapper();

	virtual bool ProcessMsg(SEventMessage *pkMsg);
	virtual bool GetPartyMemberList( VEC_GUID &rkOutCharGuidList, BM::GUID const &kGuid, bool const bPartyGuid=false, bool const bIgnoreCharGuid=false )const;

	HRESULT Add( BM::GUID const &kCharGuid, short const nChannelNo );
	short Del( BM::GUID const &kCharGuid );
	short GetChannel( BM::GUID const &kCharGuid )const;
};

template <class T> struct CreateStatic_GlobalPartyMgr;

template < > struct CreateStatic_GlobalPartyMgr< PgGlobalPartyMgr_Base >
{
	static PgGlobalPartyMgr_Base* Create()
	{
		if ( true == g_kProcessCfg.IsPublicChannel() )
		{
			return new PgGlobalPartyMgr_Public_Wrapper;
		}
		return new PgGlobalPartyMgr;
	}

	static void Destroy(PgGlobalPartyMgr_Base* p)
	{
		delete p;
	}
};

#define g_kPartyMgr Loki::SingletonHolder< PgGlobalPartyMgr_Base, CreateStatic_GlobalPartyMgr >::Instance()

#endif // CENTER_CENTERSERVER_CONTENTS_PGGLOBALPARTYMGR_H