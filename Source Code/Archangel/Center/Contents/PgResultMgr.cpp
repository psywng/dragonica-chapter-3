#include "stdafx.h"
#include "PgResultMgr.h"

PgResultMgr::PgResultMgr()
{
	m_kPool.Init( 20, 20 );
}

PgResultMgr::~PgResultMgr()
{
	Clear();
}

void PgResultMgr::Clear()
{
	BM::CAutoMutex kLock(m_kMutex);
	ConResult::iterator result_itr = m_kResult.begin();
	for ( ; result_itr!=m_kResult.end() ; ++result_itr )
	{
		m_kPool.Delete( result_itr->second );
	}
	m_kResult.clear();
}

void PgResultMgr::ProcessMsg(SEventMessage *pkMsg)
{
	PACKET_ID_TYPE usType = 0;
	pkMsg->Pop( usType );

	switch ( usType )
	{
	case PT_M_T_ANS_CREATE_GROUND:
		{
			SGroundKey kGndKey;
			if ( pkMsg->Pop( kGndKey ) )
			{
				BM::CAutoMutex kLock(m_kMutex);
				PgInstanceDungeonPlayInfo *pkInfo = m_kPool.New();
				if ( pkInfo )
				{
					pkInfo->Init( kGndKey );

					ConResult::_Pairib ibRet = m_kResult.insert( std::make_pair( kGndKey, pkInfo ) );
					if( !ibRet.second )
					{
						m_kPool.Delete(pkInfo);
					}
				}
			}	
		}break;
	case PT_M_T_NFY_DELETE_GROUND:
		{
			SGroundKey kGndKey;
			pkMsg->Pop( kGndKey );
			EraseResult( kGndKey );
		}break;
	}
}

bool PgResultMgr::EraseResult( SGroundKey const &rkKey )
{
	BM::CAutoMutex kLock(m_kMutex);
	ConResult::iterator itr = m_kResult.find( rkKey );
	if ( itr != m_kResult.end() )
	{
		PgInstanceDungeonPlayInfo *pkInfo = itr->second;
		if ( !pkInfo->IsStart() )
		{		
			pkInfo->Clear();
			m_kResult.erase( itr );
			m_kPool.Delete( pkInfo );
		}
		return true;
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgResultMgr::AddPlayer( SGroundKey const &rkKey, VEC_JOININDUN const& rkPlayerList )
{
	PgInstanceDungeonPlayInfo* pkPlayInfo = GetInfo( rkKey );
	if ( !pkPlayInfo )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	VEC_JOININDUN::const_iterator join_itr;
	for( join_itr=rkPlayerList.begin(); join_itr!=rkPlayerList.end(); ++join_itr )
	{
		pkPlayInfo->Join( *join_itr );
	}
	return true;
}

bool PgResultMgr::DeletePlayer( SGroundKey const &rkKey, BM::GUID const &rkCharGuid )
{
	PgInstanceDungeonPlayInfo* pkPlayInfo = GetInfo( rkKey );
	if ( !pkPlayInfo )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	pkPlayInfo->Leave( rkCharGuid );
	return true;
}

bool PgResultMgr::Start( SGroundKey const &rkKey, VEC_RESULTINDUN const& rkResultList )
{
	BM::CAutoMutex kLcok( m_kMutex );
	ConResult::iterator itr = m_kResult.find( rkKey );
	if ( itr != m_kResult.end() )
	{
		PgInstanceDungeonPlayInfo* pkPlayInfo = itr->second;

		if ( pkPlayInfo->ResultStart( rkResultList ) )
		{
			m_kTick.push( pkPlayInfo );
			return true;
		}
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgResultMgr::SetItem( SGroundKey const &rkKey, const SNfyResultItemList& rkReusltItem )
{
	PgInstanceDungeonPlayInfo* pkPlayInfo = GetInfo( rkKey );
	if ( !pkPlayInfo )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	return pkPlayInfo->SetResultItem( rkReusltItem );
}

// bool PgResultMgr::SetBonus( SGroundKey const &rkKey, SConResultBonus& rkBonus )
// {
// 	PgInstanceDungeonPlayInfo* pkPlayInfo = GetInfo( rkKey );
// 	if ( !pkPlayInfo )
// 	{
// 		return false;
// 	}
// 
// 	return pkPlayInfo->SetResultBonus( rkBonus );
// }

bool PgResultMgr::SelectBox( SGroundKey const &rkKey, BM::GUID const &rkCharGuid, int const iSelect )
{
	PgInstanceDungeonPlayInfo* pkPlayInfo = GetInfo( rkKey );
	if ( !pkPlayInfo )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	return pkPlayInfo->SetSelectBox( (size_t)iSelect, rkCharGuid );
}

void PgResultMgr::Tick( DWORD const dwCurTime )
{
	BM::CAutoMutex kLock( m_kMutex );
	while ( !m_kTick.empty() )
	{
		PgInstanceDungeonPlayInfo *pkInfo = m_kTick.front();
		if ( pkInfo->GetTime() > dwCurTime )
		{
			break;
		}
		pkInfo->ResultEnd();
		m_kTick.pop();
		EraseResult( pkInfo->GetKey() );
	}
}

void PgResultMgr::BroadCast( SGroundKey const &rkKey, BM::CPacket const& rkPacket )
{
	PgInstanceDungeonPlayInfo* pkPlayInfo = GetInfo( rkKey );
	if ( !pkPlayInfo )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! Not Find PlayInfo"));
		return;
	}

	pkPlayInfo->BroadCast( rkPacket );
}
