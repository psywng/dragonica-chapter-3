#include "stdafx.h"
#include "Variant/Item.h"
#include "PgInstanceDungeonPlayInfo.h"
#include "PgActionT2M.h"

PgInstanceDungeonPlayInfo::PgInstanceDungeonPlayInfo()
:	m_iMaxItemCount(0)
,	m_bRecvResultItem(false)
,	m_bResultStart(false)
{
	m_kSelectBox.reserve( ms_iRESULTSELECT_MAXCOUNT );

	for( size_t i=0; i!=ms_iRESULTSELECT_MAXCOUNT; ++i )
	{
		m_kSelectBox.push_back( BM::GUID::NullData() );
	}
}

PgInstanceDungeonPlayInfo::~PgInstanceDungeonPlayInfo()
{}

void PgInstanceDungeonPlayInfo::Init( SGroundKey const rkKey )
{
	Clear();
	m_kKey = rkKey;
}

void PgInstanceDungeonPlayInfo::Clear()
{
	BM::CAutoMutex kLock( m_kMutex );
	m_dwTime = 0;
	m_bRecvResultItem = false;
	m_bResultStart = false;
	m_kKey.Clear();
	m_kPlayerInfo.clear();
	m_iMaxItemCount = 0;

	CONT_SELECT::iterator select_itr;
	for ( select_itr=m_kSelectBox.begin(); select_itr!=m_kSelectBox.end(); ++select_itr )
	{
		(*select_itr) = BM::GUID::NullData();
	}
}

bool PgInstanceDungeonPlayInfo::ResultStart( VEC_RESULTINDUN const& rkResultList )
{
	BM::CAutoMutex kLock( m_kMutex );

	if ( m_bResultStart )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}
	
	//CURR_USER_LOG(BM::LOG_LV7, _T("[INSTANCEDUNGEON::RESULT_START] GroundKey[%d-%s]"), m_kKey.GroundNo(), m_kKey.Guid().str().c_str() );
	BM::CPacket kCPacket( PT_N_C_NFY_BOSS_RESULT, m_bRecvResultItem );
	if ( true == m_bRecvResultItem )
	{
		kCPacket.Push( m_kPlayerInfo.size() );

		VEC_RESULTINDUN::const_iterator rst_itr;
		CONT_ITEM_CREATE_ORDER::iterator item_itr;
		CONT_PLAYER::iterator user_itr;
		for ( rst_itr=rkResultList.begin(); rst_itr!=rkResultList.end(); ++rst_itr )
		{
			user_itr=m_kPlayerInfo.find( rst_itr->kCharGuid );
			if ( user_itr==m_kPlayerInfo.end() )
			{
				continue;
			}

			SINDUNPLAYINFO& rkInfo = user_itr->second;
			rkInfo.iNowExp = rst_itr->iNowExp - rkInfo.iNowExp; // iNowExp 값이 여러번 사망시 음수가 될 수 있음
			int iAddExp = std::max(static_cast< int >(rkInfo.iNowExp), 0); // 최소 0 (표시 항상 양수가 되도록, 음수일 경우 0으로 표시)
			int iTemp = 0;
			kCPacket.Push( rkInfo.kMemberGuid );					// GUID
			kCPacket.Push( user_itr->first );						// Character GUID
			kCPacket.Push( iTemp );									// 예비
			kCPacket.Push( iTemp );									// 예비
			kCPacket.Push( iTemp+1 );								// 보스
			kCPacket.Push( iAddExp );								// 경험치
			kCPacket.Push( iTemp );									// 보너스 경험치
			kCPacket.Push( rst_itr->iLevel );						// 레벨
			kCPacket.Push( rkInfo.wstrName );						// 이름
			int iItemCount = m_iMaxItemCount;
			kCPacket.Push( m_iMaxItemCount );						// 결과 아이템 갯수

			PU::TWriteArray_M(kCPacket, rkInfo.kResultItemList);
	//		kCPacket.Push( rkInfo.kResultItemList );


	// 		for( item_itr=rkInfo.kResultItemList.begin(); item_itr!=rkInfo.kResultItemList.end(); ++item_itr )
	// 		{
	// 			kCPacket.Push( (int)(item_itr->ItemNo()) );		// 아이템 번호(***나중에 DWORD로???***)
	// 			--iItemCount;
	// 		}
	// 
	// 		while( iItemCount-- )	{	kCPacket.Push( iTemp );	}	// 빈 아이템
		}

		m_dwTime = BM::GetTime32() + ms_dwRESULTSELECT_WAIT_TIME;
	}

	BroadCast( kCPacket );	
	m_bResultStart = true;
	return m_bRecvResultItem;
}

bool PgInstanceDungeonPlayInfo::SetResultItem( const SNfyResultItemList& rkResultItem )
{
	BM::CAutoMutex kLock( m_kMutex );
	m_iMaxItemCount = rkResultItem.iMaxItemCount;

	const VEC_RESULTITEM& rkContList = rkResultItem.kResultItemList;
	
	VEC_RESULTITEM::const_iterator item_itr = rkContList.begin();
	while(item_itr!=rkContList.end() )
	{
		CONT_PLAYER::iterator user_itr = m_kPlayerInfo.find( item_itr->kCharGuid );
		
		if(user_itr == m_kPlayerInfo.end())
		{
			INFO_LOG( BM::LOG_LV0, __FL__ << _T("This User No Dungeon User, Character GUID=") << item_itr->kCharGuid << _T(" / Dungeon=") << C2L(m_kKey) );
			++item_itr;
			continue;
		}

		user_itr->second.kResultItemList = item_itr->kItemList;
		++item_itr;
	}

	m_bRecvResultItem = true;
	return true;
}

bool PgInstanceDungeonPlayInfo::SetSelectBox( size_t iSelect, BM::GUID const &kCharGuid )
{
	BM::CAutoMutex kLock( m_kMutex );
	if ( iSelect >= ms_iRESULTSELECT_MAXCOUNT )
	{
		INFO_LOG( BM::LOG_LV0, __FL__ << _T("This Select BoxNo[") << iSelect << _T("] is Bad, Character GUID[") << kCharGuid << _T("] Dungeon=") << C2L(m_kKey) );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	CONT_PLAYER::iterator user_itr = m_kPlayerInfo.find( kCharGuid );
	if ( user_itr==m_kPlayerInfo.end() )
	{
		INFO_LOG( BM::LOG_LV0, __FL__ << _T("This User No Dungeon User, Character GUID[") << kCharGuid << _T("], Dungeon=") << C2L(m_kKey));
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}
	else
	{
		if ( user_itr->second.uiSelectBoxNum != SINDUNPLAYINFO::ms_unSelectBoxNum )
		{
			// 이미 선택한 놈이 또 선택하려고 하면 어쩌냐
			INFO_LOG( BM::LOG_LV0, __FL__ << _T("This User Select New BoxNo(") << iSelect << _T("), Character GUID[") << kCharGuid << _T("] / Dungeon=") << C2L(m_kKey));
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}
	}

	if ( m_kSelectBox[iSelect] != BM::GUID::NullData() )
	{
		// 이미 선택되어 있는 박스이다.
		INFO_LOG( BM::LOG_LV0, __FL__ << _T("This Box[") << iSelect << _T("] Selected->Character GUID[") << m_kSelectBox[iSelect] << _T("] / Dungeon[") << C2L(m_kKey) << _T("]")  );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	//INFO_LOG( BM::LOG_LV0, _T("[%s] Select Box[%d] / Dungeon[%d-%s]"), __FUNCTIONW__, iSelect, m_kKey.GroundNo(), m_kKey.Guid().str().c_str() );
	user_itr->second.uiSelectBoxNum = iSelect;
	m_kSelectBox[iSelect] = kCharGuid;
	BM::CPacket kCPacket(PT_M_C_NFY_SELECTED_BOX, (int)iSelect );
	kCPacket.Push( kCharGuid );
	BroadCast( kCPacket );
	return true;
}

void PgInstanceDungeonPlayInfo::ResultEnd()
{
	BM::CAutoMutex kLock( m_kMutex );
	if ( !m_bResultStart )
	{
		INFO_LOG( BM::LOG_LV0, __FL__ << _T("Not Result Start!! Ground[") << C2L(m_kKey) << _T("]") );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Warning!! ResultStart is NULL"));
		return;
	}

	CONT_PLAYER::iterator user_itr;
	size_t iCash = 0;
	for ( user_itr=m_kPlayerInfo.begin(); user_itr!=m_kPlayerInfo.end(); ++user_itr )
	{
		// 아이템을 준다
		CONT_ITEM_CREATE_ORDER const& rkResultItemList = user_itr->second.kResultItemList;
		if( !rkResultItemList.empty() )
		{
			// 결과 아이템 생성 요청(맵서버로)
			PgActionT2M_CreateItem kAction(CIE_Mission, rkResultItemList);
			kAction.DoAction(user_itr->first, m_kKey);
			//CURR_USER_LOG(BM::LOG_LV7, _T("[INSTANCEDUNGEON::CREATE_RESULT_ITEM] ItemNo[%d] UserCHARGUID[%s] GroundKey[%d-%s]"), item_itr->ItemNo(), user_itr->first.str().c_str(), m_kKey.GroundNo(), m_kKey.Guid().str().c_str() );
		}

		if ( user_itr->second.uiSelectBoxNum != SINDUNPLAYINFO::ms_unSelectBoxNum )
		{
			continue;
		}

		for( size_t i=iCash; i!=ms_iRESULTSELECT_MAXCOUNT; ++i )
		{
			if ( m_kSelectBox[i] == BM::GUID::NullData() )
			{
				user_itr->second.uiSelectBoxNum = i;
				m_kSelectBox[i] = user_itr->first;
				BM::CPacket kCPacket(PT_M_C_NFY_SELECTED_BOX, (int)i );
				kCPacket.Push( user_itr->first );
				BroadCast( kCPacket );
				iCash = i;
				break;
			}
		}
	}

	BM::CPacket kPacket( PT_M_C_NFY_OPEN_BOX );
	BroadCast( kPacket );
	m_bResultStart = false;
	//CURR_USER_LOG(BM::LOG_LV7, _T("[INSTANCEDUNGEON::RESULT_END] GroundKey[%d-%s]"), m_kKey.GroundNo(), m_kKey.Guid().str().c_str() );
}

void PgInstanceDungeonPlayInfo::BroadCast( BM::CPacket const& rkPacket )
{
	BM::CAutoMutex kLock( m_kMutex );
	CONT_PLAYER::iterator user_itr;
	for ( user_itr=m_kPlayerInfo.begin(); user_itr!=m_kPlayerInfo.end(); ++user_itr )
	{
		g_kServerSetMgr.Locked_SendToUser( user_itr->second.kMemberGuid, rkPacket );
	}
}


bool PgInstanceDungeonPlayInfo::Join( const SNfyJoinIndun& kJoinInfo )
{
	BM::CAutoMutex kLock( m_kMutex );
	std::wstring kCharName;
	if( S_OK == GetCharacterNameByGuid(kJoinInfo.kCharGuid, false, kCharName) )
	{
		m_kPlayerInfo.insert( std::make_pair( kJoinInfo.kCharGuid, SINDUNPLAYINFO(kJoinInfo.kMemberGuid, kJoinInfo.iNowExp, kCharName.c_str()) ) );
		CURR_USER_LOG(BM::LOG_LV7, L"[INSTANCEDUNGEON::JOINPLAYER] CHARGUID["<<kJoinInfo.kCharGuid<<L"], "<<C2L(m_kKey));
		return true;
	}

	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

void PgInstanceDungeonPlayInfo::Leave( BM::GUID const &kCharGuid )
{
	BM::CAutoMutex kLock( m_kMutex );
	CURR_USER_LOG(BM::LOG_LV7, L"[INSTANCEDUNGEON::LEAVEPLAYER] CHARGUID["<<kCharGuid<<L"], "<<C2L(m_kKey));
	m_kPlayerInfo.erase( kCharGuid );
}