#ifndef CENTER_CENTERSERVER_CONTENTS_PGRESULTMGR_H
#define CENTER_CENTERSERVER_CONTENTS_PGRESULTMGR_H

#include "PgInstanceDungeonPlayInfo.h"

class PgResultMgr
{
public:
	typedef BM::TObjectPool< PgInstanceDungeonPlayInfo >			ConResultPool;
	typedef std::map< SGroundKey, PgInstanceDungeonPlayInfo* >		ConResult;
	typedef std::queue<PgInstanceDungeonPlayInfo*>					ConIndunResultTickQueue;

	PgResultMgr();
	virtual ~PgResultMgr();

	void Clear();

	void ProcessMsg(SEventMessage *pkMsg);

	bool EraseResult( SGroundKey const &rkKey );

	bool AddPlayer( SGroundKey const &rkKey, VEC_JOININDUN const& rkPlayerList );
	bool DeletePlayer( SGroundKey const &rkKey, BM::GUID const &rkCharGuid );

	bool Start( SGroundKey const &rkKey, VEC_RESULTINDUN const& rkResultList );
	bool SetItem( SGroundKey const &rkKey, const SNfyResultItemList& rkReusltItem );
//	bool SetBonus( SGroundKey const &rkKey, SConResultBonus& rkBonus );
	bool SelectBox( SGroundKey const &rkKey, BM::GUID const &rkCharGuid, int const iSelect );

	void Tick( DWORD const dwCurTime );

	void BroadCast( SGroundKey const &rkKey, BM::CPacket const& rkPacket );

protected:
	PgInstanceDungeonPlayInfo* GetInfo( SGroundKey const &rkKey );

private:
	Loki::Mutex					m_kMutex;

	ConResultPool				m_kPool;
	ConResult					m_kResult;
	ConIndunResultTickQueue		m_kTick;
};

inline PgInstanceDungeonPlayInfo* PgResultMgr::GetInfo( SGroundKey const &rkKey )
{
	BM::CAutoMutex kLock(m_kMutex);
	ConResult::iterator itr = m_kResult.find( rkKey );
	if ( itr == m_kResult.end() )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return NULL"));
		return NULL;
	}
	return itr->second;
}

#define g_kResultMgr SINGLETON_STATIC(PgResultMgr)

#endif // CENTER_CENTERSERVER_CONTENTS_PGRESULTMGR_H