//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SuperGrid.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_SUPERGTYPE                  129
#define IDD_DIALOG_SUPERGRID            130
#define IDB_HARDCORE                    131
#define IDD_DIALOG1                     131
#define IDB_FOLDERS                     131
#define IDD_INSERT                      131
#define IDB_SUBITEMS                    150
#define IDD_DIALOG2                     151
#define IDD_ACCOUNT                     151
#define IDC_EDIT1                       1000
#define IDC_EDPASSWORD                  1001
#define IDC_MONTHCALENDAR1              1003
#define IDC_BUTTON_INSERT               1003
#define IDC_LIST1                       1007
#define IDC_STATIC_FRAME                1008
#define IDC_EDID                        1013
#define IDC_LOGBOX						1014
#define ID_TOOLS_VIEWINADIALOG          32773
#define ID_TOOLS_DELETE                 32774
#define ID_TOOLS_EXPAND                 32775
#define ID_TOOLS_COLLAPSE               32776
#define ID_TOOLS_EXPANDALL              32777
#define ID_TOOLS_SEARCH                 32779
#define ID_TOOLS_SORT                   32780
#define ID_TOOLS_UPDATESUBITEM          32781
#define ID_TOOLS_DRAGDROP               32782
#define ID_TOOLS_DELETEALL              32783
#define ID_TOOLS_DYNAMICUPDATEITEM      32784
#define ID_TOOLS_SETIMAGE               32785
#define ID_CONTROL_STARTALLCHANNEL      32786
#define IDM_STARTALLCHANNEL             32787
#define ID_CONTROL_STARTSELECTEDCHANNEL 32788
#define ID_CONTROL_SEPERATOR            32789
#define ID_CONTROL_STOPALLCHANNEL       32790
#define ID_CONTROL_STOPSELECTEDCHANNEL  32791
#define IDM_STARTCHECKEDCHANNEL         32792
#define IDM_STOPALLCHANNEL              32793
#define IDM_STOPCHECKEDCHANNEL          32794

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        152
#define _APS_NEXT_COMMAND_VALUE         32795
#define _APS_NEXT_CONTROL_VALUE         1015
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
