#ifndef MACHINE_MMC_PGKEYEVENT_H
#define MACHINE_MMC_PGKEYEVENT_H

extern bool RegistKeyEvent();
extern bool CALLBACK OnTerminateServer(WORD const& rkInputKey);

#endif // MACHINE_MMC_PGKEYEVENT_H