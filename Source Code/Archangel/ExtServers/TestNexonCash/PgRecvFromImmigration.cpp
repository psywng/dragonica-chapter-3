#include "stdafx.h"
#include "PgRecvFromImmigration.h"
#include "Variant/PgMctrl.h"
#include "Lohengrin/PgRealmManager.h"
#include "Collins/Log.h"

#include "BM/Vstring.h"

extern void CALLBACK OnAcceptFromOtherServer( CEL::CSession_Base *pkSession );
extern void CALLBACK OnDisConnectToOtherServer(CEL::CSession_Base *pkSession);
extern void CALLBACK OnRecvFromOtherServer(CEL::CSession_Base *pkSession, BM::CPacket * const pkPacket);

typedef enum eJAPAN_CASH_HEADER
	:	BYTE
{
	XXX_Item = 0,
	XXX_Coupon = 1,

	// 아이템 구매 패킷
	Remain_Cash = 0,
	Purchase = 1,
	Purchase_Ok = 2,
		Purchase_Confirm = 3,
		Purchase_Cancel = 4,
		No_Remain = 5,
	DB_Error = 6,
	Gift = 7,
	Gift_Limit_Over = 8,
		Gift_Not_Ready = 9,
		Gift_Under_Age = 10,

	// 쿠폰 패킷
	Check_Coupon = 0,
	Use_Coupon = 1,
	Check_Cafe_Coupon = 2,
	Use_Cafe_Coupon = 3,

	kValidCoupon = 0,
	kUsedCoupon = 1,
	kExpiredCoupon = 2,
	kReUsableCoupon = 3, 
	kInvalidCoupon = 4,
	kInvalidDomain = 5,
	kInvalidServer = 6, 
	kError = 7,
}E_JAPAN_CASH_HEADER;

HRESULT NP_Push(BM::CPacket &kPacket, BYTE byValue)
{
	kPacket.Push(byValue);
	return S_OK;
}

HRESULT NP_Push(BM::CPacket &kPacket, short nValue)
{
	char* pkTemp = (char*)&nValue;
	short kX  = MAKEWORD(pkTemp[1], pkTemp[0]);

	kPacket.Push(kX);

	return S_OK;
}

HRESULT NP_Push(BM::CPacket &kPacket, int iValue)
{
	int iTemp = 0;
	char* pkTemp = (char*)&iTemp;

	char* pkinBuf = (char*)&iValue;
	
	pkTemp[3] = pkinBuf[0];
	pkTemp[2] = pkinBuf[1];
	pkTemp[1] = pkinBuf[2];
	pkTemp[0] = pkinBuf[3];

	kPacket.Push(iTemp);
	return S_OK;
}

HRESULT NP_Push(BM::CPacket &kPacket, __int64 iValue)
{
	__int64 iTemp = 0;
	char* pkTemp = (char*)&iTemp;

	char* pkinBuf = (char*)&iValue;
	
	pkTemp[7] = pkinBuf[0];
	pkTemp[6] = pkinBuf[1];
	pkTemp[5] = pkinBuf[2];
	pkTemp[4] = pkinBuf[3];
	pkTemp[3] = pkinBuf[4];
	pkTemp[2] = pkinBuf[5];
	pkTemp[1] = pkinBuf[6];
	pkTemp[0] = pkinBuf[7];

	kPacket.Push(iTemp);
	return S_OK;
}

HRESULT NP_Push(BM::CPacket &kPacket, std::string &strValue)
{
	short n_len = strValue.length();
	NP_Push(kPacket, n_len);
	if(n_len)
	{
		kPacket.Push(&strValue.at(0), n_len);
	}
	return S_OK;
}

HRESULT NP_Push(BM::CPacket &kPacket, std::wstring &strValue)
{
	return NP_Push(kPacket, (std::string)MB(strValue));
}


HRESULT NP_Pop(BM::CPacket &kPacket, BYTE &byValue)
{
	kPacket.Pop(byValue);
	return S_OK;
}

HRESULT NP_Pop(BM::CPacket &kPacket, short &nValue)
{
	short kTemp = 0; 
	kPacket.Pop(kTemp);

	char* pkTemp = (char*)&kTemp;
	char* pkValue = (char*)&nValue;

	pkValue[0] = pkTemp[1];
	pkValue[1] = pkTemp[0];

	return S_OK;
}

HRESULT NP_Pop(BM::CPacket &kPacket, int &iValue)
{
	int iTemp = 0;
	kPacket.Pop(iTemp);

	char* pkTemp = (char*)&iTemp;
	char* pkinBuf = (char*)&iValue;
	
	pkinBuf[0] = pkTemp[3];
	pkinBuf[1] = pkTemp[2];
	pkinBuf[2] = pkTemp[1];
	pkinBuf[3] = pkTemp[0];

	
	return S_OK;
}

HRESULT NP_Pop(BM::CPacket &kPacket, __int64 &iValue)
{
	__int64 iTemp = 0;
	char* pkTemp = (char*)&iTemp;

	kPacket.Pop(iTemp);

	char* pkinBuf = (char*)&iValue;
	
	pkinBuf[7] = pkTemp[0];
	pkinBuf[6] = pkTemp[1];
	pkinBuf[5] = pkTemp[2];
	pkinBuf[4] = pkTemp[3];
	pkinBuf[3] = pkTemp[4];
	pkinBuf[2] = pkTemp[5];
	pkinBuf[1] = pkTemp[6];
	pkinBuf[0] = pkTemp[7];

	
	return S_OK;
}

HRESULT NP_Pop(BM::CPacket &kPacket, std::string &strValue)
{
	short n_len;
	NP_Pop(kPacket, n_len);

	if(n_len)
	{
		char *pkBuf = new char[n_len+1];
		kPacket.PopMemory(pkBuf, n_len);
		pkBuf[n_len] = 0;

		strValue = pkBuf;
	}
	return S_OK;
}

HRESULT NP_Pop(BM::CPacket &kPacket, std::wstring &strValue)
{
	std::string kStr;
	HRESULT const hRet = NP_Pop(kPacket, kStr);

	strValue = UNI(kStr);
	return hRet;
}



void CALLBACK OnConnectFromGame(CEL::CSession_Base *pkSession)
{
	INFO_LOG( BM::LOG_LV6, __FL__ << _T(" Accept From Game [") << pkSession->Addr().ToString().c_str() << _T("]") );
}

void CALLBACK OnDisConnectFromGame(CEL::CSession_Base *pkSession)
{
	INFO_LOG( BM::LOG_LV6, __FL__ << _T(" DisConnect From Game [") << pkSession->Addr().ToString().c_str() << _T("]") );
}

int g_iTempMoney = 123456;
void CALLBACK OnRecvFromGame(CEL::CSession_Base *pkSession, BM::CPacket * const pkPacket)
{
	BYTE byPri;
	BYTE bySub;
	int dwReqKey;
	
	std::string strMemberID;
	std::string strCharName;

	NP_Pop(*pkPacket, byPri);
	NP_Pop(*pkPacket, bySub);
	NP_Pop(*pkPacket, dwReqKey);

	BM::CPacket kAnsPacket;

	NP_Push(kAnsPacket, byPri);
	NP_Push(kAnsPacket, bySub);
	NP_Push(kAnsPacket, dwReqKey);

	if( XXX_Item == byPri 
	&&	Remain_Cash == bySub)
	{
		INFO_LOG( BM::LOG_LV0, _T("캐쉬 검색요청 ReqKey:")<<dwReqKey);

		NP_Push(kAnsPacket, (int)g_iTempMoney);
		pkSession->VSend(kAnsPacket);
	}


	if( XXX_Item == byPri
	&&	(Purchase == bySub ||	Gift == bySub))
	{//구입
		if(Purchase == bySub )
		{
			INFO_LOG( BM::LOG_LV1, _T("구입요청 ReqKey:")<<dwReqKey);
		}
		
		if(Gift == bySub)
		{
			INFO_LOG( BM::LOG_LV2, _T("선물요청 ReqKey:")<<dwReqKey);
		}

		int item_id= 0, price= 0;
		short product_type = 0;
		__int64 orderid = 0;
		BYTE domaintype = 0;
		short server_type;

		NP_Pop(*pkPacket, product_type);
		NP_Pop(*pkPacket, orderid);
		NP_Pop(*pkPacket, item_id);
		NP_Pop(*pkPacket, price);
		NP_Pop(*pkPacket, strMemberID);
		NP_Pop(*pkPacket, strCharName);//캐릭명
		NP_Pop(*pkPacket, (BYTE)domaintype);
		NP_Pop(*pkPacket, server_type);//서버타입

		if(Gift == bySub)
		{
			std::string strTargetID;
			std::string strSSN;
			NP_Pop(*pkPacket, strTargetID);
			NP_Pop(*pkPacket, strSSN);
		}
		
		//답문
		{
			BM::CPacket kDGPacket;
			BYTE byA, byRet, byDBID;
			int  iRemainCash, iTransactionID;
			
//			switch(rand()%3)
			switch(2)
			{
			case 0:
				{//잔고부족
					NP_Push(kDGPacket, (BYTE)XXX_Item);
					NP_Push(kDGPacket, (BYTE)No_Remain);
					NP_Push(kDGPacket, (int)dwReqKey);
				}break;
			case 1:
				{//디비오류
					NP_Push(kDGPacket, (BYTE)XXX_Item);
					NP_Push(kDGPacket, (BYTE)DB_Error);
					NP_Push(kDGPacket, (int)dwReqKey);
				}break;
			case 2:
				{//
					g_iTempMoney - price;

					NP_Push(kDGPacket, (BYTE)XXX_Item);
					NP_Push(kDGPacket, (BYTE)Purchase_Ok);
					NP_Push(kDGPacket, (int)dwReqKey);
					
					//아래 두개를 서버로 쏴주면 지급 완료.
					NP_Push(kDGPacket, (BYTE)(rand()%255));//DBID
					NP_Push(kDGPacket, (int)(rand()));//트랜ID

//					[XXX_Item] [Purchase_Confirm] {Request_Id} [DB_Id] {Transaction_Id} 이 포맷으로 게임서버가 응답을 주면됨
				}break;
			}
			pkSession->VSend(kDGPacket);
		}
	}

	if( XXX_Item == byPri
	&&	(Purchase_Confirm == bySub ||	Purchase_Cancel == bySub))
	{
		if(Purchase_Confirm == bySub)
		{
			INFO_LOG( BM::LOG_LV3, _T("커밋트랜 ReqKey:")<<dwReqKey);
		}
		
		if(Purchase_Cancel == bySub)
		{
			INFO_LOG( BM::LOG_LV3, _T("롤백트랜 ReqKey:")<<dwReqKey);
		}
		BM::CPacket kDGPacket;
		NP_Push(kDGPacket, byPri);
		NP_Push(kDGPacket, bySub);
		NP_Push(kDGPacket, dwReqKey);
		
		pkSession->VSend(kDGPacket);
	}
}

void CALLBACK OnConnectToJapanCash(CEL::CSession_Base *pkSession)
{	//!  세션락
	bool const bIsSucc = pkSession->IsAlive();
	if(bIsSucc)
	{	
		INFO_LOG( BM::LOG_LV6, __FL__ << _T(" Connect Success [") << pkSession->Addr().ToString().c_str() << _T("]") );

		extern void CheckNexonPacket(CEL::CSession_Base *pkSession);
		CheckNexonPacket(pkSession);
		BM::CPacket kPacket;
///*
		kPacket.Push(0x00000000);//4

		kPacket.Push((char)0x00);//4
		kPacket.Push((char)0x0D);
		kPacket.Push((char)0x00);
		kPacket.Push((char)0x08);

		kPacket.Push((char)0x73);//4
		kPacket.Push((char)0x65);
		kPacket.Push((char)0x6A);
		kPacket.Push((char)0x6F);
		
		kPacket.Push((char)0x6E);//4
		kPacket.Push((char)0x5F);
		kPacket.Push((char)0x6E);
		kPacket.Push((char)0x78);
//*/


/*55001000000000000D000873656A6F6E5F6E78 

		kPacket.Push(0x00000000);
		kPacket.Push((char)0x00);
		kPacket.Push((char)0x0D);
		kPacket.Push((char)0x00);
		kPacket.Push((char)0x01);

		kPacket.Push((char)0x03);
		kPacket.Push((char)0x2E);
*/
//		(55 00 10 0000 0000 000D 0008 7365 6A6F 6E5F 6E78 ) ;//Send(55000A00000000000D0001032E)
//		pkSession->VSend(kPacket, false);
	}
	else
	{
		INFO_LOG( BM::LOG_LV1, __FL__ << _T(" Failed to connect to Immigration") );
//		g_kProcessCfg.Locked_ConnectImmigration();
	}
}

void CheckNexonPacket(CEL::CSession_Base *pkSession)
{

	int GameCode = 0x01008206;
//例 - 
//	[ ] : 1-byte, 
//	| |: 2-byte, 
//	{ } : 4-byte, 
//	< >:8-byte
//	( ): String, 

//GAME: 게임 서버, DAEMON : Cash Daemon 서버
//(패킷 형식은 Header, Length를 제외한 부분)

/*
GAME->DAEMON : [XXX_Item] [Remain_Cash] {Request_Id} (Nexon_id)
성공: DAEMON->GAME : [XXX_Item] [Remain_Cash] {Request_Id} {잔고}
실패: DAEMON->GAME : [XXX_Item] [DB_Error] {Request_Id}

XXX_Item : 패킷 타입 (XXX 게임 아이템)
Remain_Cash : 잔고 확인 패킷 ID
DB_Error : DB에러를 알리기 위한ID
Request_Id : 게임 서버에서는 트랜잭션마다 다른 Local Id를 생성하여, 패킷 구별용으로 사용할 필요가 있다. Daemon프로세스틑 수령한 request_id를 다시 return하여, 그 명령에 대한 응답인지를 게임서버가 알 수 있도록 해줌
Nexon_id : 넥슨 아이디
*/

//	(4~14 까지 돈 없음)
	//Send_ReqJapanCash
	{
		int Request_Id = 0;

		BM::CPacket kGDPacket;
		
		NP_Push(kGDPacket, (BYTE)XXX_Item);
		NP_Push(kGDPacket, (BYTE)Remain_Cash);
		NP_Push(kGDPacket, (int)Request_Id);
		NP_Push(kGDPacket, (std::string)"BARUNSON4");
		
		pkSession->VSend(kGDPacket);
	}
	{
		int Request_Id = 0;
		BM::CPacket kDGPacket;
		BYTE byA, byB;
		int iRemainCash;
		kDGPacket.Pop(byA);
		kDGPacket.Pop(byB);
		kDGPacket.Pop(Request_Id);
		switch(byB)
		{
		case Remain_Cash:
			{//캐쉬값
				kDGPacket.Pop(iRemainCash);
			}break;
		case DB_Error:
			{//오류
			}break;
		}
	}



{//구입.
	
	int Request_Id = 0, item_id, price;
	short product_type;
	__int64 orderid;
	BYTE domaintype;

	BM::CPacket kGDPacket;
	NP_Push(kGDPacket, (BYTE)XXX_Item);
	NP_Push(kGDPacket, (BYTE)Purchase);
	NP_Push(kGDPacket, (int)Request_Id);
	NP_Push(kGDPacket, (short)product_type);
	NP_Push(kGDPacket, (__int64)orderid);
	NP_Push(kGDPacket, (int)item_id);
	NP_Push(kGDPacket, (int)price);
	NP_Push(kGDPacket, (std::string)"Nexon_ID");
	NP_Push(kGDPacket, (std::string)"Dragonica");
	NP_Push(kGDPacket, (BYTE)domaintype);
	NP_Push(kGDPacket, (short)1);//서버타입
//	pkSession->VSend(kGDPacket);

/*
4) 넥슨 포인트 사용(아이템 구입)패킷
GAME->DAEMON :
[XXX_Item][Purchase]{request_id}|product_type|<orderid>{item_id}{price}(Nexon_id)(gameid)[domaintype]|servertype|

XXX_Item : 패킷 타입(XXX 게임 아이템)
Purchase : 구입 패킷 ID
Request_Id : 패킷 구별용 key
Product_Type : 아이템 종류(Appendix a.를 참고)
Order_Id : 주문번호. 이는 게임 서버에서 아이템주문마다 오리지널 SN를 별도DB에서 관리할 수 있음(Optional)
Item_Id : 게임 서버에서 사용하는 아이템 코드
Price : 아이템 가격
Nexon_id : 넥슨ID
GameId : 게임ID
DomainType : 게임 서버 번호
ServerType : 접속 경로 – 외부 협력회사 등 (Optional). 특별히 없을 경우에는 「1」을 발송
*/
//	[ ] : 1-byte, 
//	| |: 2-byte, 
//	{ } : 4-byte, 
//	< >:8-byte
//	( ): String, 

	BM::CPacket kDGPacket;
	BYTE byA, byRet, byDBID;
	int  iRemainCash, iTransactionID;
	kDGPacket.Pop(byA);
	kDGPacket.Pop(byRet);
	kDGPacket.Pop(Request_Id);
	switch(byRet)
	{
	case No_Remain://잔고부족
	case DB_Error://DB오류 발생
		{
			BM::CPacket kGDPacket2;
			kGDPacket2.Push(XXX_Item);
			kGDPacket2.Push(Purchase_Cancel);
			kGDPacket2.Push((int)Request_Id);
			kGDPacket2.Push(byDBID);
			kGDPacket2.Push(iTransactionID);
		}break;
	case Purchase_Ok:
		{
			kDGPacket.Pop(byDBID);
			kDGPacket.Pop(iTransactionID);


			//아이템 지급을 해주고.
			//commit 트랜
			BM::CPacket kGDPacket2;
			kGDPacket2.Push(XXX_Item);
			kGDPacket2.Push(Purchase_Confirm);
			kGDPacket2.Push((int)Request_Id);
			kGDPacket2.Push(byDBID);
			kGDPacket2.Push(iTransactionID);

			
//			[XXX_Item] [Purchase_Confirm] {Request_Id} [DB_Id] {Transaction_Id}
		}break;
	}

	//트랜잭션 후 보낸대로 받는다.
	BM::CPacket kDGPacket2;

	BYTE byRetHeader, byPurchaseRet; 
	int iRequestID;
	kDGPacket2.Pop(byRetHeader);
	kDGPacket2.Pop(byPurchaseRet);
	kDGPacket2.Pop(iRequestID);
}
/*
b) 포인트 잔고가 부족할 경우
DAEMON->GAME : [XXX_Item] [No_Remain] {Request_Id} 
c) DB에러 발생
DAEMON->GAME : [XXX_Item] [DB_Error] {Request_Id}

a) DB처리 성공했을 경우
DAEMON->GAME : [XXX_Item][Purchase_Ok]{Request_Id}[DB_Id]{Transaction_Id}

게임 서버에서는 Purchase_Ok를 받은 후, 유저에게 실제로 아이템을 지급하도록 처리함. 문제가 없는 경우는 받은 [DB_Id]와 {Transaction_Id}의 값을Confirm해야 함. 일정 시간내에 Confirm을 받지 못한 경우, Daemon에서 Rollback함
GAME->DAEMON :
 [XXX_Item] [Purchase_Confirm] {Request_Id} [DB_Id] {Transaction_Id}

문제 있을 경우는Purchase_Cancel를 발송하면 Daemon에서Rollback함
GAME->DAEMON :
 [XXX_Item] [Purchase_Cancel] {Request_Id} [DB_Id] {Transaction_Id}

Daemon에서는 Purchase_Confirm 혹은 Purchase_Cancel를 받고나서 같은 형식으로 최종확인 Packet을 발송함
DAEMON->GAME : [XXX_Item] [Purchase_Confirm] {Request_Id} 혹은
DAEMON->GAME : [XXX_Item] [Purchase_Cancel] {Request_Id}

b) 포인트 잔고가 부족할 경우
DAEMON->GAME : [XXX_Item] [No_Remain] {Request_Id} 

c) DB에러 발생
DAEMON->GAME : [XXX_Item] [DB_Error] {Request_Id}

b), c)의 경우는 게임 서버에서 Confirm 필요 없음


*/










/*
{
	int Request_Id = 0, item_id, price;
	short product_type;
	__int64 orderid;
	BYTE domaintype;

	BM::CPacket kGDPacket;
	kGDPacket.Push(XXX_Item);
	kGDPacketush((short)product_type);
	kGDPacket.Push((__int64)orderid);
	kGDPacket.Push((int)item_id);
	kGDPacket.Push((int)price);
	kGDPacket.Push("Nexon_ID");
	kGDPacket.Push("GameID");
	kGDPacket.Push((BYTE)domaintype);
	kGDPacket.Push((short)1);//서버타입
	kGDPacket.Push("TargetID");
	kGDPacket.Push("TargetSSN");



	BM::CPacket kDGPacket;
	BYTE byA, byRet, byDBID;
	int  iAmount, iTransactionID;
	kDGPacket.Pop(byA);
	kDGPacket.Pop(byRet);.Push(Gift);
	kGDPacket.Push((int)Request_Id);
	kGDPacket.P
	kDGPacket.Pop(Request_Id);
	
	
	switch(byRet)
	{
	case Gift_Under_Age:
		{
		}break;
	case Gift_Limit_Over:
		{
			kDGPacket.Pop(iAmount);
		}break;
	}

//	[ ] : 1-byte, 
//	| |: 2-byte, 
//	{ } : 4-byte, 
//	< >:8-byte
//	( ): String, 

	//트랜잭션 후 보낸대로 받는다.
//	BM::CPacket kDGPacket2;

//	BYTE byRetHeader, byPurchaseRet; 
//	int iRequestID;
//	kDGPacket2.Pop(byRetHeader);
//	kDGPacket2.Pop(byPurchaseRet);
//	kDGPacket2.Pop(iRequestID);
}
*/
/*
5) 프레젠트
Purchase는 같은 형태이지만, 받을 사람의 넥슨 아이디와 선물하는 유저의 주민등록번호(한국의 경우)가 필요함. 다음 패킷을 받고나서 일반적인 아이템 구입 패킷과 같이 Purchase_Ok, Purchase_Cancel 등을 return해서 confirm을 수령
GAME->DAEMON :
[XXX_Item][Gift]{request_id}|product_type|<orderid>{item_id}{price}
(Nexon_id)(gameid)[domaintype]|servertype|(선물을 받을 유저 gameid)(선물할 유저의 SSN)

선물 받을 유저  gameid :아이템 받을 유저ID
선물할 유저SSN : 주민등록번호 앞자리 7개Ex) “9909091”
//*/
/*
선물의 경우는 Purchase에는 없는 두개의 return값이 존재함
DAEMON->GAME
[XXX_Item][Gift_Under_Age]{request_id}
[XXX_Item][Gift_Limit_Over]{request_id}{amount}
선물 가능한 한도액을 넘은 경우, 이를 return함. 뒤에서부터 amount 4 byte는 현재는 의미없는 값을 return하고 있음(예비용).

** 쿠폰 사용 패킷
Game서버에서 30자리의 Coupon번호를 발송하면 Daemon프로세스에서 쿠폰 시스템과 연계되고, 포인트차지 처리를 하여 Game서버에 Item Type을 다시 보냄

Coupon번호는 모두 알파벳으로 대문자, 소문자 구별이 없음. 내부적으로는 모두 대문자로 처리함.

Method는 Check와 Use 두가지가 있음. 쿠폰시스템은 모든 게임에서 사용되기 때문에, 받은 Item Type이 올바른지 게임 서버에서 먼저 확인해야 함

Process:
1) Check 리퀘스트
2) Check 결과값과 item type을 수령
3) Check 결과값이 에러일 경우 메시지를 표시하여 프로세스 종료
3) item type이 틀렸을 경우 ‘다른 게임용 쿠폰입니다.’ 라는 에러 메시지를 유저에게 표시하여 처리 종료
4) item type이 올바를 경우 Use리퀘스트 하여, 실제 아이템 지급
*/
/*
a) 쿠폰 상태 확인 
game -> daemon
[XXX_Coupon] [Check_Coupon] {request_id} (encrypt_coupon_num) (Nexon_id) (game_id) [domain_type] |server_type|

daemon -> game
[XXX_Coupon] [Check_Coupon] {request_id} [return_value] {item_type} 

item_type : FXCouponMap 테이블의 CouponSN
encrypt_coupon_num : 30자리 쿠폰..


Return_value:
kValidCoupon = 0
kUsedCoupon = 1
kExpiredCoupon = 2
kReUsableCoupon = 3
kInvalidCoupon = 4
kInvalidDomain = 5    // domain type가 틀렸을 경우
kInvalidServer = 6    // server type가 틀렸을 경우
kError = 7    // DB error등

kError인 경우는 item_type = 0으로 return됨

b) 쿠폰 사용
game -> daemon
[XXX_Coupon] [Use_Coupon] {request_id} (encrypt_coupon_num) (Nexon_id) (game_id) [domain_type] |server_type|

daemon -> game
[XXX_Coupon] [Use_Coupon] {request_id} [return_value] {item_type}
Return_value:
성공 : kValidCoupon = 0
재사용시 : kReUsableCoupon = 3
사용할 수 없는 쿠폰 : kUsedCoupon = 1 혹은 kExpiredCoupon = 2 혹은 kInvalidCoupon = 4 혹은 kInvalidDomain = 5 혹은 kInvalidServer = 6
에러 : kError = 7


c) 쿠폰 상태 확인(PC방 가능)
game -> daemon
[XXX_Coupon] [Check_Cafe_Coupon] {request_id} (encrypt_coupon_num) (Nexon_id) (game_id) [domain_type] |server_type|[is_cafe]

daemon -> game
[XXX_Coupon] [Check_Cafe_Coupon] {request_id} [return_value] {item_type} 

item_type : FXCouponMap 테이블 CouponSN
encrypt_coupon_num : 30자리 쿠폰...
is_cafe : PC방 가능 여부 0 = 일반 / 1 = PC방

Return_value:
kValidCoupon = 0
kUsedCoupon = 1
kExpiredCoupon = 2
kReUsableCoupon = 3
kInvalidCoupon = 4
kInvalidDomain = 5    // domain type가 틀렸을 경우
kInvalidServer = 6    // server type가 틀렸을 경우
kError = 7    // DB error 등
kInvalidCafe = 51 // PC방 이외의 장소에서 쿠폰을 사용한 경우

kError일 경우는 item_type = 0으로 return됨

d) 쿠폰 사용(PC방 가능)
game -> daemon
[XXX_Coupon] [Use_Cafe_Coupon] {request_id} (encrypt_coupon_num) (Nexon_id) (game_id) [domain_type] |server_type|[is_cafe]

daemon -> game
[XXX_Coupon] [Use_Cafe_Coupon] {request_id} [return_value] {item_type}
Return_value:
성공 : kValidCoupon = 0
재사용되었을 경우 : kReUsableCoupon = 3
사용할 수 없는 쿠폰 : kUsedCoupon = 1  혹은  kExpiredCoupon = 2 혹은  kInvalidCoupon = 4 혹은 kInvalidDomain = 5 혹은 kInvalidServer = 6
에러 : kError = 7


*/
}








void CALLBACK OnDisConnectToJapanCash(CEL::CSession_Base *pkSession)
{//!  세션락
	INFO_LOG( BM::LOG_LV6, __FL__ << _T(" Close Session Success") );
	g_kProcessCfg.Locked_OnDisconnectServer(pkSession);
}

void CALLBACK OnRecvFromJapanCash(CEL::CSession_Base *pkSession, BM::CPacket * const pkPacket)
{	// 세션키로 해당 채널을 찾아 메시지를  multiplexing 함.
	BYTE byTop;
	BYTE byCase;
	int iRequestID;
	NP_Pop(*pkPacket, byTop);
	NP_Pop(*pkPacket, byCase);
	NP_Pop(*pkPacket, iRequestID);

#if _DEBUG
	INFO_LOG( BM::LOG_LV0, __FL__ << _T(" Recved Start[") << usType << _T("]") );
#endif

	if(byTop == XXX_Item)
	{
//		BM::CPacket kDGPacket;
		int iRemainCash;
		switch(byCase)
		{
		case Remain_Cash:
			{//캐쉬값
				NP_Pop(*pkPacket, iRemainCash);
			}break;
		case DB_Error:
			{//디비 오류
				int iTransactionID;
/*				BYTE byDBID;
				BM::CPacket kGDPacket2;
				kGDPacket2.Push(XXX_Item);
				kGDPacket2.Push(Purchase_Cancel);
				kGDPacket2.Push(iRequestID);
				kGDPacket2.Push(byDBID);
				kGDPacket2.Push(iTransactionID);
*/			}break;
		case Purchase_Ok:
			{
				BYTE byDBID;
				int iTransactionID;
				NP_Pop(*pkPacket, byDBID);
				NP_Pop(*pkPacket, iTransactionID);

				//아이템 지급을 해주고.
				//commit 트랜
				BM::CPacket kGDPacket2;
				kGDPacket2.Push(XXX_Item);
				kGDPacket2.Push(Purchase_Confirm);
				kGDPacket2.Push((int)iRequestID);
				kGDPacket2.Push(byDBID);
				kGDPacket2.Push(iTransactionID);
				//			[XXX_Item] [Purchase_Confirm] {Request_Id} [DB_Id] {Transaction_Id}
			}break;
		}
	}
	

	

//	PgChannel * pChann = g_kRealmMgr.Get(pkSession->SessionKey());
//	if(pChann) 
//	{
//		pChann->OnRecvPacket(pkSession, pkPacket); 
//		return;
//	}
	//INFO_LOG(BM::LOG_LV0, _T("[%s]-[%d] Can't Find Channel"), __FUNCTIONW__, __LINE__);
}

/*
	//트랜잭션 후 보낸대로 받는다.
	BM::CPacket kDGPacket2;

	BYTE byRetHeader, byPurchaseRet; 
	int iRequestID;
	kDGPacket2.Pop(byRetHeader);
	kDGPacket2.Pop(byPurchaseRet);
	kDGPacket2.Pop(iRequestID);

	*/
