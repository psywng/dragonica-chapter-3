#include "stdafx.h"
#include "Variant/PgParty.h"
#include "Variant/PgPartyMgr.h"
#include "Variant/PgJobSkillTool.h"
#include "PgLocalPartyMgr.h"
#include "PgGroundTrigger.h"
#include "PgGround.h"
#include "PgMissionGround.h"
#include "PgPartyItemRule.h"
#include "PgRequest.h"
#include "PgReqMapMove.h"
#include "PgMissionMan.h"
#include "PgAction.h"
#include "Lohengrin/VariableContainer.h"
#include "PgSuperGround.h"
#include "Variant/PgJobSkill.h"

PgGroundTrigger::PgGroundTrigger(void)
{
}

PgGroundTrigger::~PgGroundTrigger(void)
{
}

PgGroundTrigger::PgGroundTrigger( PgGroundTrigger const &rhs )
:	m_kID(rhs.m_kID)
,	m_ptMin(rhs.m_ptMin)
,	m_ptMax(rhs.m_ptMax)
,	m_kEnable(rhs.m_kEnable)
{
}

PgGroundTrigger& PgGroundTrigger::operator=( PgGroundTrigger const &rhs )
{
	m_kID = rhs.m_kID;
	m_ptMin = rhs.m_ptMin;
	m_ptMax = rhs.m_ptMax;
	m_kEnable = rhs.m_kEnable;
	return *this;
}

bool PgGroundTrigger::InitTriggerPhysX(NiAVObject const* pkObj, float const fBuffer)
{
	if( !pkObj )
	{
		return false;
	}

	NiBound const &kBound = pkObj->GetWorldBound();
	m_ptMin = m_ptMax = POINT3( kBound.GetCenter().x, kBound.GetCenter().y ,kBound.GetCenter().z );

	// 낮은 트리거, 높은 트리거 모두 OK
	float const fRadius = kBound.GetRadius() + fBuffer;	// 서버에서는 전체범위를 약간 크게 잡아야 한다.
	m_ptMin -= fRadius;
	m_ptMax += fRadius;
	return true;
}

bool PgGroundTrigger::Init( GTRIGGER_ID const &kID, NiNode *pkTriggerRoot, float const fBuffer )
{
	if ( !pkTriggerRoot )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	if ( kID.empty() )
	{
		INFO_LOG( BM::LOG_LV0, __FL__<<L"ID is None");
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false") );
		return false;
	}

	m_kID = kID;
	NiAVObject *pkObj = pkTriggerRoot->GetObjectByName( m_kID.c_str() );
	if( !InitTriggerPhysX(pkObj, fBuffer) )
	{
		//INFO_LOG( BM::LOG_LV0, __FL__<<L"Not Found Object["<<UNI(m_kID)<<L"]" );	//상위에서 로그 남김
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}
	return true;
}

bool PgGroundTrigger::IsInPos( POINT3 const &pt3Pos )const
{
	return (pt3Pos <= m_ptMax) && (pt3Pos >= m_ptMin);
}

void PgGroundTrigger::WriteToPacket(BM::CPacket& rkPacket) const
{
	rkPacket.Push( m_kID );
	rkPacket.Push( m_kEnable );
}


//============================================================================
// PgGTrigger_Portal
//----------------------------------------------------------------------------
PgGTrigger_Normal::PgGTrigger_Normal()
	: PgGroundTrigger()
{
}

PgGTrigger_Normal::PgGTrigger_Normal(PgGTrigger_Normal const& rhs)
	: PgGroundTrigger(rhs)
{
}

PgGTrigger_Normal::~PgGTrigger_Normal()
{
}

bool PgGTrigger_Normal::Build(GTRIGGER_ID const& kID, NiNode* pkTriggerRoot, TiXmlElement const* pkElement)
{
	if( !Init( kID, pkTriggerRoot ) )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}
	return true;
}

bool PgGTrigger_Normal::Event(CUnit* pkUnit, PgGround* const pkGround, BM::CPacket* const pkPacket)
{
	if( !pkUnit
	||	!pkGround
	||	!pkPacket )
	{
		return false;
	}

	if( !Enable() )
	{
		return false;
	}

	if( !IsInPos(pkUnit->GetPos()) )
	{
		return false;
	}

	return true;
}

//============================================================================
// PgGTrigger_Portal
//----------------------------------------------------------------------------
PgGTrigger_Portal::PgGTrigger_Portal(void)
	: PgGroundTrigger(), m_kContPortal()
{
}

PgGTrigger_Portal::PgGTrigger_Portal(PgGTrigger_Portal const& rhs)
	: PgGroundTrigger(rhs), m_kContPortal(rhs.m_kContPortal)
{
}

PgGTrigger_Portal::~PgGTrigger_Portal(void)
{
}

bool PgGTrigger_Portal::Build( GTRIGGER_ID const &kID, NiNode *pkTriggerRoot, TiXmlElement const *pkElement )
{
	CONT_DEFMAP const * pkContDefMap = NULL;
	g_kTblDataMgr.GetContDef(pkContDefMap);

	if ( !pkContDefMap )
	{
		VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("CONT_DEFMAP is NULL") );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	if ( !Init( kID, pkTriggerRoot ) )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	{
		CONT_PORTAL_ACCESS::value_type kPortal;
		if ( kPortal.Build( pkElement, *pkContDefMap ) )
		{
			m_kContPortal.push_back( kPortal );
		}
	}

	pkElement = pkElement->FirstChildElement();
	while ( pkElement )
	{
		if ( !::strcmp(pkElement->Value(), "ITEM") )
		{
			if ( m_kContPortal.empty() )
			{
				m_kContPortal.resize(1);
			}

			CONT_PORTAL_ACCESS::value_type kPortal;
			if ( kPortal.Build( pkElement, *pkContDefMap ) )
			{
				m_kContPortal.push_back( kPortal );
			}
		}
		pkElement = pkElement->NextSiblingElement();
	}

	if( m_kContPortal.empty() )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	}
	
	return !m_kContPortal.empty();
}

bool PgGTrigger_Portal::Event( CUnit *pkUnit, PgGround * const pkGround, BM::CPacket * const pkPacket )
{
	if( !Enable() )
	{
		return false;
	}

	if ( !IsInPos( pkUnit->GetPos() ) )
	{
		// 포탈을 벗어났습니다.
		pkUnit->SendWarnMessage( 18995 );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}
	
	size_t iTargetIndex = 0;
	if ( pkPacket->Pop( iTargetIndex ) )
	{
		if ( m_kContPortal.size() > iTargetIndex )
		{
			PgPlayer *pkPlayer = dynamic_cast<PgPlayer*>(pkUnit);
			if ( pkPlayer )
			{
				bool bSuccess = false;
				BYTE const kMoveType = m_kContPortal.at(iTargetIndex).GetMoveType();

				SReqMapMove_MT kRMM(MMET_None);
				PgReqMapMove kMapMove( pkGround, kRMM, &(m_kContPortal.at(iTargetIndex)) );
				if ( kMapMove.Add( pkPlayer ) )
				{
					if ( kMoveType & E_MOVE_PARTYMASTER )
					{// 파티 마스터만 이동을 요청할 수 있다.
						if ( pkPlayer->HaveParty() )
						{
							bSuccess = pkGround->AddPartyMember( pkPlayer, kMapMove );
						}
					}
					else if( kMoveType & E_MOVE_ANY_PARTYMEMMBER )
					{// 파티원 누구나 이동 요청 가능
						if ( pkPlayer->HaveParty() )
						{
							bSuccess = pkGround->AddAnyPartyMember( pkPlayer, kMapMove );
							if( false == bSuccess )
							{
								// 파티원중 한명이라도 안되면 모두 이동 불가
								LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
								return false;
							}
						}
					}

					if ( !bSuccess )
					{
						bSuccess = kMoveType & E_MOVE_PERSONAL;
					}

					if ( bSuccess )
					{
						return kMapMove.DoAction();
					}
				}
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
				return false;
			}
		}
	}

	// 잘못된 포탈입니다.
	pkUnit->SendWarnMessage( 18994 );
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

//============================================================================
// PgGTrigger_PartyPortal
//----------------------------------------------------------------------------
PgGTrigger_PartyPortal::PgGTrigger_PartyPortal(void)
	: PgGroundTrigger(), m_kContPortal()
{
}

PgGTrigger_PartyPortal::PgGTrigger_PartyPortal(PgGTrigger_PartyPortal const& rhs)
	: PgGroundTrigger(rhs), m_kContPortal(rhs.m_kContPortal)
{
}

PgGTrigger_PartyPortal::~PgGTrigger_PartyPortal(void)
{
}

bool PgGTrigger_PartyPortal::Build( GTRIGGER_ID const &kID, NiNode *pkTriggerRoot, TiXmlElement const *pkElement )
{
	CONT_DEFMAP const * pkContDefMap = NULL;
	g_kTblDataMgr.GetContDef(pkContDefMap);

	if ( !pkContDefMap )
	{
		VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("CONT_DEFMAP is NULL") );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	if ( !Init( kID, pkTriggerRoot ) )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	{
		CONT_PORTAL_ACCESS::value_type kPortal;
		if ( kPortal.Build( pkElement, *pkContDefMap ) )
		{
			m_kContPortal.push_back( kPortal );
		}
	}

	pkElement = pkElement->FirstChildElement();
	while ( pkElement )
	{
		if ( !::strcmp(pkElement->Value(), "ITEM") )
		{
			if ( m_kContPortal.empty() )
			{
				m_kContPortal.resize(1);
			}

			CONT_PORTAL_ACCESS::value_type kPortal;
			if ( kPortal.Build( pkElement, *pkContDefMap ) )
			{
				m_kContPortal.push_back( kPortal );
			}
		}
		pkElement = pkElement->NextSiblingElement();
	}

	if( m_kContPortal.empty() )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	}
	
	return !m_kContPortal.empty();
}

bool PgGTrigger_PartyPortal::Event( CUnit *pkUnit, PgGround * const pkGround, BM::CPacket * const pkPacket )
{
	if( !Enable() )
	{
		return false;
	}

	if ( !IsInPos( pkUnit->GetPos() ) )
	{
		// 포탈을 벗어났습니다.
		pkUnit->SendWarnMessage( 18995 );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}
	
	size_t iTargetIndex = 0;
	if ( pkPacket->Pop( iTargetIndex ) )
	{
		if ( m_kContPortal.size() > iTargetIndex )
		{
			PgPlayer *pkPlayer = dynamic_cast<PgPlayer*>(pkUnit);
			if ( pkPlayer )
			{
				bool bSuccess = false;
				BYTE const kMoveType = m_kContPortal.at(iTargetIndex).GetMoveType();

				SReqMapMove_MT kRMM(MMET_None);
				PgReqMapMove kMapMove( pkGround, kRMM, &(m_kContPortal.at(iTargetIndex)) );
				if ( kMapMove.Add( pkPlayer ) )
				{					
					if ( pkPlayer->HaveParty() )
					{
						// 파티원 누구나 이동 요청 가능
						bSuccess = pkGround->AddAnyPartyMember( pkPlayer, kMapMove );
						if( false == bSuccess )
						{
							// 파티원중 한명이라도 안되면 모두 이동 불가
							LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
							return false;
						}
					}
					else
					{
						if ( !bSuccess )
						{
							bSuccess = kMoveType & E_MOVE_PERSONAL;
						}
					}

					if ( bSuccess )
					{
						return kMapMove.DoAction();
					}
				}
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
				return false;
			}
		}
	}

	// 잘못된 포탈입니다.
	pkUnit->SendWarnMessage( 18994 );
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

//============================================================================
// PgGTrigger_PartyMemberPortal
//----------------------------------------------------------------------------
PgGTrigger_PartyMemberPortal::PgGTrigger_PartyMemberPortal(void)
	: PgGroundTrigger(), m_kContPortal()
{
}

PgGTrigger_PartyMemberPortal::PgGTrigger_PartyMemberPortal(PgGTrigger_PartyMemberPortal const& rhs)
	: PgGroundTrigger(rhs), m_kContPortal(rhs.m_kContPortal)
{
}

PgGTrigger_PartyMemberPortal::~PgGTrigger_PartyMemberPortal(void)
{
}

bool PgGTrigger_PartyMemberPortal::Build( GTRIGGER_ID const &kID, NiNode *pkTriggerRoot, TiXmlElement const *pkElement )
{
	CONT_DEFMAP const * pkContDefMap = NULL;
	g_kTblDataMgr.GetContDef(pkContDefMap);

	if ( !pkContDefMap )
	{
		VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("CONT_DEFMAP is NULL") );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	if ( !Init( kID, pkTriggerRoot ) )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	{
		CONT_PORTAL_ACCESS::value_type kPortal;
		if ( kPortal.Build( pkElement, *pkContDefMap ) )
		{
			m_kContPortal.push_back( kPortal );
		}
	}

	pkElement = pkElement->FirstChildElement();
	while ( pkElement )
	{
		if ( !::strcmp(pkElement->Value(), "ITEM") )
		{
			if ( m_kContPortal.empty() )
			{
				m_kContPortal.resize(1);
			}

			CONT_PORTAL_ACCESS::value_type kPortal;
			if ( kPortal.Build( pkElement, *pkContDefMap ) )
			{
				m_kContPortal.push_back( kPortal );
			}
		}
		pkElement = pkElement->NextSiblingElement();
	}

	if( m_kContPortal.empty() )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	}
	
	return !m_kContPortal.empty();
}

bool PgGTrigger_PartyMemberPortal::Event( CUnit *pkUnit, PgGround * const pkGround, BM::CPacket * const pkPacket )
{
	if( !Enable() )
	{
		return false;
	}

	if ( !IsInPos( pkUnit->GetPos() ) )
	{
		// 포탈을 벗어났습니다.
		pkUnit->SendWarnMessage( 18995 );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}
	
	size_t iTargetIndex = 0;
	if ( pkPacket->Pop( iTargetIndex ) )
	{
		if ( m_kContPortal.size() > iTargetIndex )
		{
			PgPlayer *pkPlayer = dynamic_cast<PgPlayer*>(pkUnit);
			if ( pkPlayer )
			{
				bool bSuccess = false;
				BYTE const kMoveType = m_kContPortal.at(iTargetIndex).GetMoveType();

				SReqMapMove_MT kRMM(MMET_None);
				PgReqMapMove kMapMove( pkGround, kRMM, &(m_kContPortal.at(iTargetIndex)) );
				if ( kMapMove.Add( pkPlayer ) )
				{
					if ( pkPlayer->HaveParty() )
					{
						if( pkGround )
						{
							// 파티장만 이동가능
							BM::GUID kMasterGuid;
							if ( pkGround->GetPartyMasterGuid( pkPlayer->PartyGuid(), kMasterGuid ) )
							{
								if ( kMasterGuid != pkPlayer->GetID() )
								{
									pkPlayer->SendWarnMessage(80027);
									LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
									return false;
								}
							}
						}

						bSuccess = pkGround->AddPartyAllMember( pkPlayer, kMapMove );
						if( false == bSuccess )
						{
							pkPlayer->SendWarnMessage(400906);							
							LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
							return false;
						}
					}
					else
					{
						if ( !bSuccess )
						{
							bSuccess = kMoveType & E_MOVE_PERSONAL;
						}
					}

					if ( bSuccess )
					{
						return kMapMove.DoAction();
					}
				}
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
				return false;
			}
		}
	}

	// 잘못된 포탈입니다.
	pkUnit->SendWarnMessage( 18994 );
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

//============================================================================
// PgGTrigger_Mission
//----------------------------------------------------------------------------
PgGTrigger_Mission::PgGTrigger_Mission(void)
:	PgGroundTrigger(), m_iMissionKey(0), m_iMissionNo(0)
{}

PgGTrigger_Mission::PgGTrigger_Mission(PgGTrigger_Mission const& rhs)
	: PgGroundTrigger(rhs), m_iMissionKey(rhs.m_iMissionKey), m_iMissionNo(rhs.m_iMissionNo)
{
}

PgGTrigger_Mission::~PgGTrigger_Mission(void)
{}

bool PgGTrigger_Mission::Build( GTRIGGER_ID const &kID, NiNode *pkTriggerRoot, TiXmlElement const *pkElement )
{
	CONT_DEF_MISSION_ROOT const *pkMissionRoot = NULL;
	g_kTblDataMgr.GetContDef( pkMissionRoot );
	if ( !pkMissionRoot )
	{
		VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__<<L"CONT_DEF_MISSION_ROOT is NULL" );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	if ( !Init( kID, pkTriggerRoot ) )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	TiXmlAttribute const *pkAttribute = pkElement->FirstAttribute();

	char const *pcAttrName = NULL;
	char const *pcAttrValue = NULL;
	while ( pkAttribute )
	{
		pcAttrName = pkAttribute->Name();
		pcAttrValue = pkAttribute->Value();

		if( !::strcmp(pcAttrName, "PARAM") )
		{
			m_iMissionNo = ::atoi( pcAttrValue );
			CONT_DEF_MISSION_ROOT::const_iterator itr = pkMissionRoot->find( m_iMissionNo );
			if ( itr != pkMissionRoot->end() )
			{
				m_iMissionKey = itr->second.iKey;
				return true;
			}

			VERIFY_INFO_LOG( false, BM::LOG_LV4, __FL__<<L"Not Found MissionNo["<<m_iMissionNo<<L"]" );
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}
		pkAttribute = pkAttribute->Next();
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgGTrigger_Mission::Event( CUnit *pkUnit, PgGround * const pkGround, BM::CPacket * const pkPacket )
{
	if( !Enable() )
	{
		return false;
	}

	if ( !IsInPos( pkUnit->GetPos() ) )
	{
		// 포탈을 벗어났습니다.
		pkUnit->SendWarnMessage( 18995 );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	return MissionEvent(pkUnit, pkGround, pkPacket);
}

bool PgGTrigger_Mission::MissionEvent( CUnit *pkUnit, PgGround * const pkGround, BM::CPacket * const pkPacket )
{
	PgPlayer* pkPlayer = dynamic_cast<PgPlayer*>(pkUnit);//맵서버가 받아서 셋팅한다
	if(pkPlayer)
	{
// 		if ( NULL != pkPlayer->GetEffect(PgThrowUpPenalty::ms_iEffectNo) )
// 		{
// 			// 패널티 상태에서는 미션에 입장 할 수 없습니다.
// 			pkPlayer->SendWarnMessage(80016);
// 			return false;
// 		}

		if( !pkGround )
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV4, __FL__<<L"pkGround is NULL");
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}

		BM::GUID kMasterGuid;
		//if ( g_kLocalPartyMgr.GetPartyMasterGuid( pkPlayer->PartyGuid(), kMasterGuid ) )
		if ( pkGround->GetPartyMasterGuid( pkPlayer->PartyGuid(), kMasterGuid ) )
		{
			if ( kMasterGuid != pkPlayer->GetID() )
			{
				if ( pkGround->CheckUnit(kMasterGuid) )
				{// 파티 마스터랑 같은 그라운드에 있으면 미션에 들어 갈 수 없다.
					pkPlayer->SendWarnMessage(80027);
					LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
					return false;
				}
			}
		}

		if ( INT_MAX == pkPlayer->GetAbil( AT_MISSION_THROWUP_PENALTY ) )
		{
			// 패널티가 있으면 체크를 해야 한다.
			PgRequest_CheckPenalty kCheckPenalty( pkGround->GroundKey(), static_cast<WORD>(AT_MISSION_THROWUP_PENALTY), pkPacket );
			kCheckPenalty.DoAction( pkPlayer );
			return false;
		}

		int iLevel = 0;
		if ( !pkPacket->Pop( iLevel ) )
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}

		int iType = 0;
		int iEventMissionKey = m_iMissionKey;

		switch( GetType() )
		{
		case GTRIGGER_TYPE_MISSION_NPC:
			{
				iType = MT_EVENT_MISSION;
			}break;
		case GTRIGGER_TYPE_MISSION_EVENT_NPC:
			{
				iType = MT_EVENT_MISSION_NPC;
			}break;
		case GTRIGGER_TYPE_MISSION_EASY:
			{
				iType = MT_MISSION_EASY;
			}break;
		default:
			{
				iType = MT_MISSION;
			}break;
		}
		//int iType = static_cast<int>( (GTRIGGER_TYPE_MISSION_NPC == GetType()) ? MT_EVENT_MISSION : MT_MISSION );
		// 히든 맵은 아직 열리지 않아야 된다.
		//if( MT_MISSION != static_cast<EMissionTypeKind>(iType) )
		{
			if( MT_EVENT_MISSION_NPC == static_cast<EMissionTypeKind>(iType) )
			{
				/*int iEventMissionNo = 0;
				if ( !pkPacket->Pop( iEventMissionNo ) )
				{
					LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
					return false;
				}
				if( 0 > (m_iMissionKey + iEventMissionNo) )
				{
					LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
					return false;
				}
				iEventMissionKey = m_iMissionKey + iEventMissionNo;*/
			}
			else if( MT_EVENT_MISSION == static_cast<EMissionTypeKind>(iType) )
			{
				int const iMissionEventAllClear = 0x000F;
				if( iMissionEventAllClear == (iMissionEventAllClear & pkPlayer->GetAbil(AT_MISSION_EVENT)) )
				{
					// 미션 이벤트 맵을 모두 "SSS" 등급으로 클리어 했다. 히든 맵이 등장한다.
					iType = static_cast<int>(MT_EVENT_HIDDEN);
				}
			}
		}
		/////////////////////////////////////

		if ( 0 == iLevel )
		{
			PgRequest_MissionInfo kReq( iEventMissionKey, pkGround->GroundKey(), iType );
			if ( kReq.DoAction( pkPlayer ) )
			{
				return true;
			}
			pkPlayer->SendWarnMessage(400225);
		}
		else
		{
			PgPlayer_MissionData const *pkMissionData = pkPlayer->GetMissionData( static_cast<unsigned int>(iEventMissionKey) );
			if ( pkMissionData )
			{
				PgMissionInfo const *pkMissionInfo = NULL;
				if( g_kMissionMan.GetMissionKey( iEventMissionKey, pkMissionInfo) )
				{
					// Client에서는 레벨이 1부터 오고 서버에서는 레벨은 0부터이다.
					SMissionKey kMissionKey( iEventMissionKey, --iLevel );

					SMissionOptionMissionOpen const *pkMissionOpen = NULL;
					if ( pkMissionInfo->GetMissionOpen( kMissionKey.iLevel, pkMissionOpen) )
					{
						if ( !pkMissionData->IsPlayingLevel(pkMissionOpen->m_kLimit_PreLevelValue) )
						{
							pkPlayer->SendWarnMessage(400225);
							LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
							return false;
						}

						if( false == pkMissionData->IsClearLevel(iLevel) )
						{
							// Clear가 되지 않은 경우만 Quest 체크한다.
							PgMyQuest const *pkMyQuest = pkPlayer->GetMyQuest();
							if(!pkMyQuest)
							{
								pkPlayer->SendWarnMessage(400225);
								LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
								return false;
							}

							if( DEFENCE7_MISSION_LEVEL == (iLevel+1) )
							{
								VEC_INT	VecClearQuestValue;
								VecClearQuestValue.clear();
								pkMissionOpen->GetClearQuestIDVec(VecClearQuestValue);
								bool const bClearRet = pkMyQuest->IsEndedQuestVec(VecClearQuestValue);
								if( (false == bClearRet) && ( VecClearQuestValue.size() != 0) )
								{
									pkPlayer->SendWarnMessage(8016);
									LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
									return false;
								}
							}

							VEC_INT	VecIngQuestValue;
							VecIngQuestValue.clear();
							pkMissionOpen->GetIngQuestIDVec(VecIngQuestValue);
							bool const bIngRet = pkMyQuest->IsIngQuestVec(VecIngQuestValue);
							bool const bEndRet = pkMyQuest->IsEndedQuestVec(VecIngQuestValue);

							if( false == (bIngRet || bEndRet) && ( VecIngQuestValue.size() != 0))
							{
								int const iErrorTTW = pkMissionInfo->GetErrorText();
								pkPlayer->SendWarnMessage(iErrorTTW);
								LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
								return false;
							}

							if( pkUnit )
							{
								VEC_INT	VecEffectValue;
								VecEffectValue.clear();
								pkMissionOpen->GetEffectNo(VecEffectValue);

								PgAction_MissionEffectCheck kCheckAction( VecEffectValue, pkGround, pkGround->GroundKey(), true );
								if( false == kCheckAction.DoAction( pkUnit, NULL ) )
								{
									pkPlayer->SendWarnMessage(400645);
									LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
									return false;
								}
							}
						}
						else
						{
							VEC_GUID kGuidVec;
							pkGround->GetPartyMemberGround( pkPlayer->PartyGuid(), pkGround->GroundKey(), kGuidVec );

							PgPlayer *pkPartyMember = NULL;
							VEC_GUID::iterator user_itr = kGuidVec.begin();
							for ( ; user_itr!=kGuidVec.end(); ++user_itr )
							{
								pkPartyMember = dynamic_cast<PgPlayer*>(pkGround->GetUnit( *user_itr ));
								if( pkPartyMember )
								{
									if ( pkPartyMember->GetID() != pkPlayer->GetID() )
									{
										if ( INT_MAX == pkPartyMember->GetAbil( AT_MISSION_THROWUP_PENALTY ) )
										{
											PgRequest_CheckPenalty kCheckPenalty( pkGround->GroundKey(), static_cast<WORD>(AT_MISSION_THROWUP_PENALTY), NULL );
											kCheckPenalty.DoAction( pkPartyMember );
										}
									}
								}
							}
						}

						if( GTRIGGER_TYPE_MISSION_NPC == GetType() )
						{
							// Event Map Quest Check(Party All Member)
							// Quest All Check 모두 설정된 퀘스트가 진행/완료가 되어야 된다.
							VEC_INT	VecIngQuestValue;
							VecIngQuestValue.clear();
							pkMissionOpen->GetIngQuestIDVec(VecIngQuestValue);
							PgAction_MissionEventQuestCheck kCheckQuestAction( VecIngQuestValue, pkGround, pkGround->GroundKey() );
							if( false == kCheckQuestAction.DoAction( pkUnit, NULL ) )
							{
								pkPlayer->SendWarnMessage(790551);
								LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
								return false;
							}

							if( MT_EVENT_HIDDEN == static_cast<EMissionTypeKind>(iType) )
							{
								PgAction_MissionEventHiddenRankCheck kCheckHiddenRankAction(pkGround, pkGround->GroundKey());
								if( false == kCheckHiddenRankAction.DoAction( pkUnit, NULL ) )
								{
									pkPlayer->SendWarnMessage(790552);
									LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
									return false;
								}

								
								// 이벤트 맵에서는 레벨 6을 파티장이 들어 갈 경우, 파티원 모두 5레벨을 클리어 해야만 된다.
								// 그렇지 않은 경우는 5레벨만 입장이 가능하다.
								PgAction_MissionEventHiddenLevelClearCheck kCheckHiddenLevelClearCheck(iEventMissionKey, iLevel, pkGround, pkGround->GroundKey());
								if( false == kCheckHiddenLevelClearCheck.DoAction( pkUnit, NULL ))
								{
									pkPlayer->SendWarnMessage(790553);
									LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
									return false;
								}
							}
						}

						// 파티가 있는 경우 모든 파티원이 같은 맵에 존재해야 들어 갈수가 있다.
						PgAction_MissionPartyMemberCheck kPartyMemverCheck( pkGround );
						if( false == kPartyMemverCheck.DoAction( pkPlayer, NULL ) )
						{
							pkPlayer->SendWarnMessage(400906);
							LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
							return false;
						}

						PgRequest_MissionJoin kReq( kMissionKey, pkGround->GroundKey(), iType, pkPacket );
						if ( kReq.DoAction( pkPlayer ) )
						{
							return true;
						}
					}
					else
					{
						if( MISSION_LEVEL_MAX == (iLevel+1) )
						{
							pkPlayer->SendWarnMessage(401070);

							LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
							return false;
						}
						VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__<<L"pkMissionOpen is NULL Key["<<kMissionKey.iKey<<L"] Level["<<kMissionKey.iLevel<<L"]" );
					}
				}
				pkPlayer->SendWarnMessage(400225);
			}
		}
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

//============================================================================
// PgGTrigger_Mission_Npc
//----------------------------------------------------------------------------
PgGTrigger_Mission_Npc::PgGTrigger_Mission_Npc(void)
	: PgGTrigger_Mission()
{
}

PgGTrigger_Mission_Npc::PgGTrigger_Mission_Npc(PgGTrigger_Mission_Npc const& rhs)
	: PgGTrigger_Mission(rhs)
{
}

PgGTrigger_Mission_Npc::~PgGTrigger_Mission_Npc(void)
{
}

//============================================================================
// PgGTrigger_Mission_Event_Npc
//----------------------------------------------------------------------------
PgGTrigger_Mission_Event_Npc::PgGTrigger_Mission_Event_Npc(void)
	: PgGTrigger_Mission()
{
}

PgGTrigger_Mission_Event_Npc::PgGTrigger_Mission_Event_Npc(PgGTrigger_Mission_Event_Npc const& rhs)
	: PgGTrigger_Mission(rhs)
{
}

PgGTrigger_Mission_Event_Npc::~PgGTrigger_Mission_Event_Npc(void)
{
}

//============================================================================
// PgGTrigger_Mission_Easy
//----------------------------------------------------------------------------
PgGTrigger_Mission_Easy::PgGTrigger_Mission_Easy(void)
	: PgGTrigger_Mission()
{
}

PgGTrigger_Mission_Easy::PgGTrigger_Mission_Easy(PgGTrigger_Mission_Easy const& rhs)
	: PgGTrigger_Mission(rhs)
{
}

PgGTrigger_Mission_Easy::~PgGTrigger_Mission_Easy(void)
{
}

//============================================================================
// PgGTrigger_Mission_Easy
//----------------------------------------------------------------------------
PgGTrigger_Hidden_Portal::PgGTrigger_Hidden_Portal(void)
	: PgGroundTrigger(), m_iParam(0)
{
}

PgGTrigger_Hidden_Portal::PgGTrigger_Hidden_Portal(PgGTrigger_Hidden_Portal const& rhs)
	: PgGroundTrigger(rhs), m_iParam(rhs.m_iParam)
{
}

PgGTrigger_Hidden_Portal::~PgGTrigger_Hidden_Portal(void)
{
}

bool PgGTrigger_Hidden_Portal::Build( GTRIGGER_ID const &kID, NiNode *pkTriggerRoot, TiXmlElement const *pkElement )
{
	if ( !Init( kID, pkTriggerRoot ) )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	TiXmlAttribute const *pkAttribute = pkElement->FirstAttribute();

	char const *pcAttrName = NULL;
	char const *pcAttrValue = NULL;
	while ( pkAttribute )
	{
		pcAttrName = pkAttribute->Name();
		pcAttrValue = pkAttribute->Value();

		if( !::strcmp(pcAttrName, "PARAM") )
		{
			m_iParam = ::atoi( pcAttrValue );
		}
		pkAttribute = pkAttribute->Next();
	}

	return m_iParam > 0;
}

bool PgGTrigger_Hidden_Portal::Event( CUnit *pkUnit, PgGround * const pkGround, BM::CPacket * const pkPacket )
{
	if( !Enable() )
	{
		return false;
	}

	if ( !IsInPos( pkUnit->GetPos() ) )
	{
		// 포탈을 벗어났습니다.
		pkUnit->SendWarnMessage( 18995 );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	// 제한 입장 시작 체크
	// Error Msg : 400987
	SYSTEMTIME kNow;
	::GetLocalTime( &kNow );

	int iOpenMinValue = 0;
	if( S_OK != g_kVariableContainer.Get(EVar_Kind_Hidden, EVar_Hidden_Open_Min, iOpenMinValue) )
	{
		VERIFY_INFO_LOG( false, BM::LOG_LV1, __FL__ << _T("Can't Find EVar_Kind_Hidden 'EVar_Hidden_Open_Min' Value") );
	}

	if( (0 > iOpenMinValue) || (59 < iOpenMinValue) )
	{
		iOpenMinValue = 10;
	}

	PgPlayer *pkPlayer = dynamic_cast<PgPlayer*>(pkUnit);
	if ( pkPlayer )
	{
		if( (0 == (kNow.wHour%2)) && (iOpenMinValue >= kNow.wMinute) )
		{
			// 짝수 시간 Open

			const CONT_DEFMAP* pkContDefMap = NULL;
			g_kTblDataMgr.GetContDef(pkContDefMap);

			if(!pkContDefMap)
			{
				INFO_LOG(BM::LOG_LV0, __FL__ << L"Cannot find ContDefMap");
				return false;
			}

			GET_DEF(CItemDefMgr, kItemDefMgr);
			CItemDef const* pItemDef = kItemDefMgr.GetDef(m_iParam);
			if(!pItemDef)
			{
				INFO_LOG(BM::LOG_LV0, __FL__ << L"Cannot find HiddenPortal ItemNo");
				LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
				return false;
			}

			int const iMapNo = pItemDef->GetAbil(AT_MAP_NUM);

			CONT_DEFMAP::const_iterator itor = pkContDefMap->find(iMapNo);
			if (itor == pkContDefMap->end())
			{
				INFO_LOG(BM::LOG_LV0, __FL__ << L"Cannot find DefMap MapNo[" << iMapNo << "]" );
				return false;
			}

			const TBL_DEF_MAP& rkDefMap = itor->second;

			if( false == pkPlayer->GetHiddenOpen()->IsComplete(rkDefMap.sHiddenIndex) )
			{
				BM::CPacket kPacket( PT_M_C_REQ_HIDDEN_MOVE_CHECK );
				kPacket.Push( pkPlayer->GetID() );
				kPacket.Push( m_iParam );
				pkPlayer->Send(kPacket);

				return true;
			}
			else
			{
				// 하루에 한번만 입장 가능
				pkPlayer->SendWarnMessage( 400990 );
			}

			return false;
		}
		else
		{
			pkPlayer->SendWarnMessage( 400987 );
		}
	}
	//



	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}
//============================================================================
// PgGTrigger_InMission
//----------------------------------------------------------------------------
PgGTrigger_InMission::PgGTrigger_InMission(void)
	: PgGroundTrigger(), m_iType(0)
{
}

PgGTrigger_InMission::PgGTrigger_InMission(PgGTrigger_InMission const& rhs)
	: PgGroundTrigger(rhs), m_iType(rhs.m_iType)
{
}

PgGTrigger_InMission::~PgGTrigger_InMission(void)
{}

bool PgGTrigger_InMission::Build( GTRIGGER_ID const &kID, NiNode *pkTriggerRoot, TiXmlElement const *pkElement )
{
	if ( !Init( kID, pkTriggerRoot ) )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	TiXmlAttribute const *pkAttribute = pkElement->FirstAttribute();

	char const *pcAttrName = NULL;
	char const *pcAttrValue = NULL;
	while ( pkAttribute )
	{
		pcAttrName = pkAttribute->Name();
		pcAttrValue = pkAttribute->Value();

		if( !::strcmp(pcAttrName, "PARAM") )
		{
			m_iType = ::atoi( pcAttrValue );
		}
		pkAttribute = pkAttribute->Next();
	}

	return m_iType > 0;
}

bool PgGTrigger_InMission::Event( CUnit *pkUnit, PgGround * const pkGround, BM::CPacket * const /*pkPacket*/ )
{
	if( !Enable() )
	{
		return false;
	}

	if ( !IsInPos( pkUnit->GetPos() ) )
	{
		// 포탈을 벗어났습니다.
		pkUnit->SendWarnMessage( 18995 );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	PgMissionGround *pkMissionGnd = dynamic_cast<PgMissionGround*>(pkGround);
	if ( pkMissionGnd )
	{
		size_t const iNowStage = pkMissionGnd->GetStage();
		return pkMissionGnd->SwapStage_Before( iNowStage + 1, pkUnit );
	}	
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

//============================================================================
// PgGTrigger_PortalEmporia
//----------------------------------------------------------------------------
PgGTrigger_PortalEmporia::PgGTrigger_PortalEmporia(void)
	: PgGroundTrigger(), m_kEmporiaKey()
{
}

PgGTrigger_PortalEmporia::PgGTrigger_PortalEmporia( PgGTrigger_PortalEmporia const &rhs )
:	PgGroundTrigger(rhs)
,	m_kEmporiaKey(rhs.m_kEmporiaKey)
{
}

PgGTrigger_PortalEmporia& PgGTrigger_PortalEmporia::operator=( PgGTrigger_PortalEmporia const &rhs )
{
	PgGroundTrigger::operator = ( rhs );
	m_kEmporiaKey = rhs.m_kEmporiaKey;
	return *this;
}

PgGTrigger_PortalEmporia::~PgGTrigger_PortalEmporia(void)
{
}

bool PgGTrigger_PortalEmporia::Build( GTRIGGER_ID const &kID, NiNode *pkTriggerRoot, TiXmlElement const *pkElement )
{
	CONT_DEF_EMPORIA const * pkContDefEmporia = NULL;
	g_kTblDataMgr.GetContDef(pkContDefEmporia);

	if ( !pkContDefEmporia )
	{
		VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__ << _T("CONT_DEF_EMPORIA is NULL") );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	if ( !Init( kID, pkTriggerRoot ) )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	TiXmlAttribute const *pkAttribute = pkElement->FirstAttribute();

	char const *pcAttrName = NULL;
	char const *pcAttrValue = NULL;
	while ( pkAttribute )
	{
		pcAttrName = pkAttribute->Name();
		pcAttrValue = pkAttribute->Value();

		if( !::strcmp(pcAttrName, "GUID") )
		{
			m_kEmporiaKey.kID.Set( std::string(pcAttrValue) );
		}
		else if ( !::strcmp(pcAttrName, "PARAM") )
		{
			m_kEmporiaKey.byGrade = static_cast<BYTE>(::atoi(pcAttrValue));
		}
		pkAttribute = pkAttribute->Next();
	}

	if ( pkContDefEmporia->find( m_kEmporiaKey.kID ) == pkContDefEmporia->end() )
	{
		VERIFY_INFO_LOG( false, BM::LOG_LV4, __FL__ << _T("Not Found EmporiaID<") << m_kEmporiaKey.kID << _T("> TriggerID <") << kID << _T(">") );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	return true;
}

bool PgGTrigger_PortalEmporia::Event( CUnit *pkUnit, PgGround * const pkGround, BM::CPacket * const pkPacket )
{
	if( !Enable() )
	{
		return false;
	}

	if ( !IsInPos( pkUnit->GetPos() ) )
	{
		// 포탈을 벗어났습니다.
		pkUnit->SendWarnMessage( 18995 );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	PgPlayer *pkPlayer = dynamic_cast<PgPlayer*>(pkUnit);
	if ( pkPlayer )
	{
		if ( pkPlayer->HaveParty() )
		{
			// 파티가 있으면 진입할 수 없습니다.
			INFO_LOG( BM::LOG_LV5, _T("[ReqJoinEmporia] FAILED ") << pkPlayer->Name() << _T("<") << pkPlayer->GetID() << _T("> Had Party") );
			pkPlayer->SendWarnMessage( 19013 );
		}
		else
		{
			SReqMapMove_MT kRMM(MMET_None);
			kRMM.nTargetPortal = 1;
			kRMM.kCasterKey = pkGround->GroundKey();
			kRMM.kCasterSI = g_kProcessCfg.ServerIdentity();

			BM::CPacket kCheckPacket( PT_M_N_REQ_MAP_MOVE_CHECK, pkPlayer->GetID() );
			kCheckPacket.Push(kRMM);
			kCheckPacket.Push(pkPlayer->GuildGuid());
			kCheckPacket.Push(false);
			kCheckPacket.Push(m_kEmporiaKey);
			
			::SendToRealmContents( PMET_EMPORIA, kCheckPacket );
			return true;
		}
	}

	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

//============================================================================
// PgGTrigger_TeleMove
//----------------------------------------------------------------------------
PgGTrigger_TeleMove::PgGTrigger_TeleMove(void)
	: PgGroundTrigger(), m_kTargetPos(), m_kTargetID()
{
}

PgGTrigger_TeleMove::PgGTrigger_TeleMove(PgGTrigger_TeleMove const& rhs)
	: PgGroundTrigger(rhs), m_kTargetPos(rhs.m_kTargetPos), m_kTargetID(rhs.m_kTargetID)
{
}

PgGTrigger_TeleMove::~PgGTrigger_TeleMove(void)
{}

bool PgGTrigger_TeleMove::Build( GTRIGGER_ID const &kID, NiNode *pkTriggerRoot, TiXmlElement const *pkElement )
{
	if ( !Init( kID, pkTriggerRoot ) )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	TiXmlAttribute const *pkAttribute = pkElement->FirstAttribute();

	char const *pcAttrName = NULL;
	char const *pcAttrValue = NULL;
	while ( pkAttribute )
	{
		pcAttrName = pkAttribute->Name();
		pcAttrValue = pkAttribute->Value();

		if( !::strcmp(pcAttrName, "PARAM_STRING") )
		{
			m_kTargetID = pcAttrValue;
			break;//나머지 정보는 필요없음
		}
		pkAttribute = pkAttribute->Next();
	}

	return !m_kTargetID.empty();
}

bool PgGTrigger_TeleMove::Event( CUnit *pkUnit, PgGround * const pkGround, BM::CPacket * const pkPacket )
{
	if(!pkPacket)
	{
		return false;
	}

	if( !Enable() )
	{
		return false;
	}

	int iType = 0;
	pkPacket->Pop(iType);
	std::string kTriggerID;
	pkPacket->Pop(kTriggerID);
	POINT3 kNowPos;
	pkPacket->Pop(kNowPos);

	switch( iType )
	{
	case TELE_PARTY_MOVE_OTHER:
		{
			// 위치 검사 없음
		}break;
	case TELE_PARTY_MOVE:
		{
			if( TELE_PARTY_MOVE == iType
			&&	BM::GUID::IsNotNull(pkUnit->GetPartyGuid()) )
			{
				VEC_GUID kContGuid;
				pkGround->GetPartyMember(pkUnit->GetPartyGuid(), kContGuid);
				VEC_GUID::const_iterator iter = kContGuid.begin();
				while( kContGuid.end() != iter )
				{
					if( pkUnit->GetID() != (*iter) )
					{
						PgPlayer* pkOtherPlayer = dynamic_cast< PgPlayer* >(pkGround->GetUnit((*iter)));
						if( pkOtherPlayer
						&&	!pkOtherPlayer->IsAlive() )
						{
							int const iMessageNo = 400649;
							BM::CPacket kPacket( PT_M_C_NFY_WARN_MESSAGE, iMessageNo );
							kPacket.Push( static_cast< BYTE >(EL_Warning) );
							pkGround->Broadcast(kPacket);

							BM::CPacket kFailedPacket(PT_M_C_ANS_FAILED_HYPER_MOVE);
							kFailedPacket.Push( iType );
							pkUnit->Send(kFailedPacket);
							return false;
						}
					}
					++iter;
				}
			}
		} // no break;
	case TELE_MOVE:
	case TELE_JUMP:
	default:
		{
			if ( !IsInPos( pkUnit->GetPos() ) )
			{
				if ( !IsInPos( kNowPos ) )	//안전장치. 클라에서 보내준 데이터.
				{
					// 포탈을 벗어났습니다.
					//pkUnit->SendWarnMessage( 18995 );
					LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
					return false;
				}
			}
		}break;
	}
	if( POINT3::NullData() == m_kTargetPos )
	{
		m_kTargetPos = pkGround->GetNodePosition(m_kTargetID.c_str());
	}

	PgPlayer* pkPlayer = dynamic_cast<PgPlayer*>(pkUnit);
	if(pkPlayer)	//점프 시작
	{
		pkPlayer->SetHyperMoveInfo(static_cast<HYPERMOVE_TYPE>(iType), BM::GetTime32(), m_kTargetPos);

		if( TELE_PARTY_MOVE == iType
		&&	BM::GUID::IsNotNull(pkPlayer->GetPartyGuid()) )
		{
			VEC_GUID kContGuid;
			pkGround->GetPartyMember(pkPlayer->GetPartyGuid(), kContGuid);
			VEC_GUID::const_iterator iter = kContGuid.begin();
			while( kContGuid.end() != iter )
			{
				if( pkPlayer->GetID() != (*iter) )
				{
					PgPlayer* pkOtherPlayer = dynamic_cast< PgPlayer* >(pkGround->GetUnit((*iter)));
					if( pkOtherPlayer )
					{
						BM::CPacket kPacket(PT_M_C_NFY_PARTY_TELE_PORT);
						kPacket.Push( kTriggerID );
						kPacket.Push( static_cast< int >(TELE_PARTY_MOVE_OTHER) );
						pkOtherPlayer->Send(kPacket);
					}
				}
				++iter;
			}
		}
	
		return true;
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

//============================================================================
// PgGTrigger_BattleArea
//----------------------------------------------------------------------------
PgGTrigger_BattleArea::PgGTrigger_BattleArea(void)
	:	PgGTrigger_Normal(), m_iParam(0)
{}

PgGTrigger_BattleArea::~PgGTrigger_BattleArea(void)
{
}

bool PgGTrigger_BattleArea::InitTriggerPhysX(NiAVObject const* pkObj, float const fBuffer)
{
	if( !pkObj )
	{
		return false;
	}

	//NiPoint3 ptPos = pkObj->GetWorldTranslate();
	NiBound const& kBound = pkObj->GetWorldBound();
	m_ptMin = m_ptMax = POINT3( kBound.GetCenter().x, kBound.GetCenter().y ,kBound.GetCenter().z );

	// 엠포리아 방어전 용도
	float const fRadius = kBound.GetRadius();
	float const fAddValue = ::sqrt( ( fRadius * fRadius ) / 3.0f + 1.0f ) + fBuffer; // 1.0은 나누기로 손실된 값의 보정용
	m_ptMin -= fAddValue;
	m_ptMax += fAddValue;
	return true;
}

PgGTrigger_BattleArea::PgGTrigger_BattleArea( PgGTrigger_BattleArea const &rhs )
:	PgGTrigger_Normal(rhs)
,	m_iParam(rhs.m_iParam)
{
}

PgGTrigger_BattleArea& PgGTrigger_BattleArea::operator=( PgGTrigger_BattleArea const &rhs )
{
	PgGTrigger_Normal::operator = ( rhs );
	m_iParam = rhs.m_iParam;
	return *this;
}

bool PgGTrigger_BattleArea::Build( GTRIGGER_ID const &kID, NiNode *pkTriggerRoot, TiXmlElement const *pkElement )
{
	if ( !Init( kID, pkTriggerRoot, 0.0f ) )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	TiXmlAttribute const *pkAttribute = pkElement->FirstAttribute();

	char const *pcAttrName = NULL;
	char const *pcAttrValue = NULL;
	while ( pkAttribute )
	{
		pcAttrName = pkAttribute->Name();
		pcAttrValue = pkAttribute->Value();

		if( !::strcmp(pcAttrName, "PARAM") )
		{
			m_iParam = ::atoi( pcAttrValue );
		}
		pkAttribute = pkAttribute->Next();
	}

	return 0 != m_iParam;
}

//============================================================================
// PgGTrigger_TransTower
//----------------------------------------------------------------------------
PgGTrigger_TransTower::PgGTrigger_TransTower(void)
:	m_i64SaveMoney(0i64)
{
}

PgGTrigger_TransTower::PgGTrigger_TransTower(PgGTrigger_TransTower const& rhs)
:	PgGTrigger_Normal(rhs)
,	m_kTowerID(rhs.m_kTowerID)
,	m_i64SaveMoney(rhs.m_i64SaveMoney)
{
}

PgGTrigger_TransTower::~PgGTrigger_TransTower(void)
{
}

bool PgGTrigger_TransTower::Build( GTRIGGER_ID const &kID, NiNode *pkTriggerRoot, TiXmlElement const *pkElement )
{
	if( !Init( kID, pkTriggerRoot ) )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	m_i64SaveMoney = 0i64;

	TiXmlAttribute const *pkAttribute = pkElement->FirstAttribute();
	char const *pcAttrName = NULL;
	char const *pcAttrValue = NULL;
	while ( pkAttribute )
	{
		pcAttrName = pkAttribute->Name();
		pcAttrValue = pkAttribute->Value();

		if( !::strcmp(pcAttrName, "GUID") )
		{
			m_kTowerID.Set( std::string(pcAttrValue) );
		}
		else if ( !::strcmp(pcAttrName, "PARAM") )
		{
			m_i64SaveMoney = ::_atoi64( pcAttrValue );
		}

		pkAttribute = pkAttribute->Next();
	}

	CONT_DEF_TRANSTOWER const *pkDefTransTower = NULL;
	g_kTblDataMgr.GetContDef( pkDefTransTower );

	CONT_DEF_TRANSTOWER::const_iterator itr = pkDefTransTower->find( m_kTowerID );
	if ( itr != pkDefTransTower->end() )
	{
		return true;
	}

	VERIFY_INFO_LOG( false, BM::LOG_LV4, __FL__ << L"Not Found TransTower ID<" << m_kTowerID << L"> in DEF_TRANSTOWER");
	LIVE_CHECK_LOG( BM::LOG_LV4, L"Return false");
	return false;
}

bool PgGTrigger_TransTower::Event( CUnit *pkUnit, PgGround * const pkGround, BM::CPacket * const pkPacket )
{
	if( !Enable() )
	{
		return false;
	}

	if ( !IsInPos( pkUnit->GetPos() ) )
	{
		// 포탈을 벗어났습니다.
//		pkUnit->SendWarnMessage( 18995 );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	return Event( pkUnit, pkGround, pkPacket, m_kTowerID, m_i64SaveMoney );
}

bool PgGTrigger_TransTower::Event( CUnit *pkUnit, PgGround * const pkGround, BM::CPacket * const pkPacket, BM::GUID const &kTowerID, __int64 const i64SaveMoney )
{
	PgPlayer * pkPlayer = dynamic_cast<PgPlayer*>(pkUnit);
	if ( !pkPlayer )
	{
		LIVE_CHECK_LOG( BM::LOG_LV0, __FL__ << L"Dynamic case Error" );
		return false;
	}

	int iType = 0;
	pkPacket->Pop( iType );

	try
	{
		__int64 const i64HaveMoney = pkUnit->GetAbil64( AT_MONEY );

		switch ( iType )
		{
		case E_TRANSTOWER_SAVEPOS:
			{
				// 돈이 충분히 있는지 검사
				if ( i64HaveMoney < i64SaveMoney )
				{
					throw 700036;// 소지금이 부족합니다.
				}

				CONT_PLAYER_MODIFY_ORDER kOrder;

				if ( i64SaveMoney )
				{
					SPMOD_Add_Money kDelMoneyData(-i64SaveMoney);//필요머니 빼기.
					SPMO kIMO(IMET_ADD_MONEY, pkUnit->GetID(), kDelMoneyData);
					kOrder.push_back(kIMO);
				}
				
				{
					SRecentInfo kRecentInfo( pkGround->GetGroundNo(), pkUnit->GetPos() );
					SPMO kIMO( IMET_TRANSTOWER_SAVE_RECENT, pkUnit->GetID(), kRecentInfo );
					kOrder.push_back(kIMO);
				}

				PgAction_ReqModifyItem kItemModifyAction( CIE_TransTower_Save_Recent, pkGround->GroundKey(), kOrder );
				kItemModifyAction.DoAction( pkUnit, NULL );
			}break;
		case E_TRANSTOWER_MAPMOVE:
			{
				TBL_DEF_TRANSTOWER_TARGET_KEY kTargetKey;
				bool bDisCountItem = false;
				if (	!pkPacket->Pop( kTargetKey )
					||	!pkPacket->Pop( bDisCountItem )
					)
				{
					throw 0;
				} 

				CONT_DEF_TRANSTOWER const *pkDefTransTower = NULL;
				g_kTblDataMgr.GetContDef( pkDefTransTower );

				CONT_DEF_TRANSTOWER::const_iterator def_itr = pkDefTransTower->find( kTowerID );
				if ( def_itr == pkDefTransTower->end() )
				{
					VERIFY_INFO_LOG( false, BM::LOG_LV4, __FL__ << L"Not Found TransTower ID<" << kTowerID << L"> in DEF_TRANSTOWER");
					throw 0;
				}

				TBL_DEF_TRANSTOWER_TARGET kTarget( kTargetKey );
				CONT_DEF_TRANSTOWER_TARGET::const_iterator target_itr = def_itr->second.find( kTarget );
				if ( target_itr == def_itr->second.end() )
				{
					throw 0;
				}

				if ( FAILED(pkPlayer->IsOpenWorldMap( kTarget.iGroundNo )) )
				{
					// 이펙트가 존재하는지 찾아본다.
					if ( !pkPlayer->IsItemEffect( EFFECTNO_TRANSTOWER_FREE ) )
					{
						CONT_DEFMAP const *pkDefMap = NULL;
						g_kTblDataMgr.GetContDef( pkDefMap );

						int iContinent = 0;
						CONT_DEFMAP::const_iterator map_itr = pkDefMap->find( kTarget.iGroundNo );
						if ( map_itr != pkDefMap->end() )
						{
							iContinent = static_cast<int>(map_itr->second.sContinent);
						}

						if (	!iContinent 
							||	!pkPlayer->IsItemEffect( EFFECTNO_TRANSTOWER_FREE + iContinent ) )
						{
							// 이동한 맵이 아님
							throw 16;
						}
					}
				}

				kTarget = *target_itr;

				if ( true == bDisCountItem )
				{
					SItemPos kDisCountItemPos;
					pkPacket->Pop( kDisCountItemPos );

					PgBase_Item kDisCountItem;
					if ( SUCCEEDED(pkPlayer->GetInven()->GetItem( kDisCountItemPos, kDisCountItem )) )
					{
						if ( !kDisCountItem.IsUseTimeOut() )
						{
							GET_DEF(CItemDefMgr, kItemDefMgr);
							CItemDef const *pkItemDef = kItemDefMgr.GetDef(kDisCountItem.ItemNo());
							if ( pkItemDef )
							{
								kTarget.i64Price -= std::max( SRateControl::GetValueRate( kTarget.i64Price , static_cast<__int64>(pkItemDef->GetAbil( AT_USE_ITEM_CUSTOM_VALUE_1 )) ), 0i64 ); 
							}
						}
					}
				}

				if ( i64HaveMoney < kTarget.i64Price )
				{
					// 돈이 부족
					throw 700036;
				}

				// 맵이동을 해야 한다.
				SReqMapMove_MT kRMM(MMET_None);
				kRMM.kTargetKey.GroundNo(kTarget.iGroundNo);
				kRMM.nTargetPortal = kTarget.nTargetSpawn;

				PgReqMapMove kMapMove( pkGround, kRMM, NULL );
				if( !kMapMove.Add( pkPlayer ) )
				{
					throw 0;
				}

				if ( kTarget.i64Price )
				{
					SPMOD_Add_Money kDelMoneyData( -kTarget.i64Price );//필요머니 빼기.
					SPMO kIMO(IMET_ADD_MONEY, pkPlayer->GetID(), kDelMoneyData);
					kMapMove.AddModifyOrder( kIMO );
				}

				if ( !kMapMove.DoAction() )
				{
					throw 0;
				}
			}break;
/*
		case E_TRANSTOWER_OPENMAP:
			{
				TBL_DEF_TRANSTOWER_TARGET_KEY kTargetKey;
				if ( !pkPacket->Pop( kTargetKey ) )
				{
					throw 0;
				}

				SItemPos kItemPos;
				if ( !pkPacket->Pop( kItemPos ) )
				{
					throw 0;
				}

				CONT_DEF_TRANSTOWER const *pkDefTransTower = NULL;
				g_kTblDataMgr.GetContDef( pkDefTransTower );

				CONT_DEF_TRANSTOWER::const_iterator def_itr = pkDefTransTower->find( kTowerID );
				if ( def_itr == pkDefTransTower->end() )
				{
					VERIFY_INFO_LOG( false, BM::LOG_LV4, __FL__ << L"Not Found TransTower ID<" << kTowerID << L"> in DEF_TRANSTOWER");
					throw 0;
				}

				TBL_DEF_TRANSTOWER_TARGET kTarget( kTargetKey );
				CONT_DEF_TRANSTOWER_TARGET::const_iterator target_itr = def_itr->second.find( kTarget );
				if ( target_itr == def_itr->second.end() )
				{
					throw 0;
				}

				if ( SUCCEEDED(pkPlayer->IsOpenWorldMap( kTarget.iGroundNo )) )
				{
					// 이동한 맵임
					throw 0;
				}

				PgBase_Item kItem;
				if ( FAILED(pkPlayer->GetInven()->GetItem( kItemPos, kItem )) )
				{
					throw 0;
				}

				GET_DEF(CItemDefMgr, kItemDefMgr);
				CItemDef const* pItemDef = kItemDefMgr.GetDef(kItem.ItemNo());
				if(!pItemDef)
				{
					VERIFY_INFO_LOG( false, BM::LOG_LV4, __FL__ << L"Not Found ItemDef ItemNo<" << kItem.ItemNo() << L">" );
					throw 0;
				}

				int const iErrorMsg = PgAction_ReqUseItem::CheckUseTime( *pItemDef );
				if ( iErrorMsg )
				{
					throw iErrorMsg;
				}

				int const iCustomType = pItemDef->GetAbil(AT_USE_ITEM_CUSTOM_TYPE);
				if ( UICT_TRANSTOWER_OPENMAP != iCustomType )
				{
					throw 0;
				}

				CONT_PLAYER_MODIFY_ORDER kOrder;

				{
					SPMOD_Modify_Count kDelData( kItem, kItemPos, -1 );//1씩 감소.
					SPMO kIMO(IMET_MODIFY_COUNT|IMC_DEC_DUR_BY_USE, pkPlayer->GetID(), kDelData);
					kOrder.push_back(kIMO);
				}

				{
					SRecentInfo kRecentInfo( kTarget.iGroundNo, pkPlayer->GetPos() );
					SPMO kIMO( IMET_TRANSTOWER_SAVE_RECENT, pkPlayer->GetID(), kRecentInfo );
					kOrder.push_back(kIMO);
				}

				PgAction_ReqModifyItem kItemModifyAction( CIE_TransTower_Open_Map, pkGround->GroundKey(), kOrder );
				kItemModifyAction.DoAction( pkUnit, NULL );
			}break;
*/
		}

	}
	catch( int iErrorMsg )
	{
		if ( iErrorMsg )
		{
			pkUnit->SendWarnMessage( iErrorMsg );
		}
		return false;
	}

	return true;
}




///////////////////////////////////
PgGTrigger_SuperGround::PgGTrigger_SuperGround(void)
	: PgGroundTrigger(), m_iSuperGroundNo(0), m_iSpawnNo(0)
{
}
PgGTrigger_SuperGround::PgGTrigger_SuperGround(PgGTrigger_SuperGround const& rhs)
	: PgGroundTrigger(rhs), m_iSuperGroundNo(rhs.m_iSuperGroundNo), m_iSpawnNo(rhs.m_iSpawnNo)
{
}
PgGTrigger_SuperGround::~PgGTrigger_SuperGround(void)
{
}
bool PgGTrigger_SuperGround::Build( GTRIGGER_ID const &kID, NiNode *pkTriggerRoot, TiXmlElement const *pkElement )
{
	if ( !Init( kID, pkTriggerRoot ) )
	{
		return false;
	}

	TiXmlAttribute const *pkAttribute = pkElement->FirstAttribute();
	char const *pcAttrName = NULL;
	char const *pcAttrValue = NULL;
	while ( pkAttribute )
	{
		pcAttrName = pkAttribute->Name();
		pcAttrValue = pkAttribute->Value();

		if( !::strcmp(pcAttrName, "PARAM") )
		{
			m_iSuperGroundNo = PgStringUtil::SafeAtoi(pcAttrValue);
		}
		else if( !::strcmp(pcAttrName, "PARAM2") )
		{
			m_iSpawnNo = PgStringUtil::SafeAtoi(pcAttrValue);
		}
		pkAttribute = pkAttribute->Next();
	}

	if( 0 == m_iSpawnNo )
	{
		m_iSpawnNo = 1;
	}
	if( 0 == m_iSuperGroundNo )
	{
		VERIFY_INFO_LOG( false, BM::LOG_LV4, __FL__<<L"SuperGroundNo is 0, Trigger["<<kID<<L"]" );
		return false;
	}
	return true;
}
bool PgGTrigger_SuperGround::Event( CUnit *pkUnit, PgGround * const pkGround, BM::CPacket * const pkPacket )
{
	if( !Enable() )
	{
		return false;
	}

	if ( !IsInPos( pkUnit->GetPos() ) )
	{
		// 포탈을 벗어났습니다.
		pkUnit->SendWarnMessage( 18995 );
		return false;
	}

	PgPlayer* pkPlayer = dynamic_cast<PgPlayer*>(pkUnit);//맵서버가 받아서 셋팅한다
	if(pkPlayer)
	{
		if( !pkGround )
		{
			return false;
		}

		// 파티가 있는 경우 모든 파티원이 같은 맵에 존재해야 들어 갈수가 있다.
		PgAction_MissionPartyMemberCheck kPartyMemberCheck( pkGround );
		if( false == kPartyMemberCheck.DoAction( pkPlayer, NULL ) )
		{
			pkPlayer->SendWarnMessage(400906);
			return false;
		}

		int iSuperGroundMode = 0;
		if( !pkPacket->Pop( iSuperGroundMode ) )
		{
			return false;
		}

		if( SuperGroundUtil::ReqEnterSuperGround(pkUnit, pkGround->GroundKey(), m_iSuperGroundNo, iSuperGroundMode) )
		{
			return true;
		}
		else
		{
			pkPlayer->SendWarnMessage( 6, EL_Warning );
		}
	}
	return false;
}

//////////////////////////////////////////
PgGTrigger_InSuperGround::PgGTrigger_InSuperGround(void)
	: PgGroundTrigger(), m_iSpawnNo(0)
{
}

PgGTrigger_InSuperGround::PgGTrigger_InSuperGround(PgGTrigger_InSuperGround const& rhs)
	: PgGroundTrigger(rhs), m_iSpawnNo(rhs.m_iSpawnNo)
{
}

PgGTrigger_InSuperGround::~PgGTrigger_InSuperGround(void)
{}

bool PgGTrigger_InSuperGround::Build( GTRIGGER_ID const &kID, NiNode *pkTriggerRoot, TiXmlElement const *pkElement )
{
	if( !Init( kID, pkTriggerRoot ) )
	{
		return false;
	}
	TiXmlAttribute const *pkAttribute = pkElement->FirstAttribute();
	char const *pcAttrName = NULL;
	char const *pcAttrValue = NULL;
	while ( pkAttribute )
	{
		pcAttrName = pkAttribute->Name();
		pcAttrValue = pkAttribute->Value();

		if( !::strcmp(pcAttrName, "PARAM2") )
		{
			m_iSpawnNo = PgStringUtil::SafeAtoi(pcAttrValue);
		}
		pkAttribute = pkAttribute->Next();
	}
	if( 0 == m_iSpawnNo )
	{
		m_iSpawnNo = 1;
	}
	return true;
}

bool PgGTrigger_InSuperGround::Event( CUnit *pkUnit, PgGround * const pkGround, BM::CPacket * const /*pkPacket*/ )
{
	if( !Enable() )
	{
		return false;
	}

	if ( !IsInPos( pkUnit->GetPos() ) )
	{
		// 포탈을 벗어났습니다.
		pkUnit->SendWarnMessage( 18995 );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	if( BM::GUID::IsNotNull(pkUnit->GetPartyGuid()) )
	{
		VEC_GUID kContGuid;
		pkGround->GetPartyMember(pkUnit->GetPartyGuid(), kContGuid);
		VEC_GUID::const_iterator iter = kContGuid.begin();
		while( kContGuid.end() != iter )
		{
			if( pkUnit->GetID() != (*iter) )
			{
				PgPlayer* pkOtherPlayer = dynamic_cast< PgPlayer* >(pkGround->GetUnit((*iter)));
				if( pkOtherPlayer
				&&	!pkOtherPlayer->IsAlive() )
				{
					int const iMessageNo = 400649;
					BM::CPacket kPacket( PT_M_C_NFY_WARN_MESSAGE, iMessageNo );
					kPacket.Push( static_cast< BYTE >(EL_Warning) );
					pkGround->Broadcast(kPacket);
					return false;
				}
			}
			++iter;
		}
	}

	PgSuperGround* pkSuperGround = dynamic_cast< PgSuperGround* >(pkGround);
	if( pkSuperGround )
	{
		return pkSuperGround->NextFloor(pkSuperGround->NowFloor()+1, m_iSpawnNo);
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

//============================================================================
// PgGTrigger_JobSkill
//----------------------------------------------------------------------------
PgGTrigger_JobSkill::PgGTrigger_JobSkill(void)
	: PgGroundTrigger(), iJobGrade(0)
{
}

PgGTrigger_JobSkill::PgGTrigger_JobSkill(PgGTrigger_JobSkill const& rhs)
: PgGroundTrigger(rhs), iJobGrade(rhs.iJobGrade)
{
}

PgGTrigger_JobSkill::~PgGTrigger_JobSkill(void)
{
}

bool PgGTrigger_JobSkill::Build( GTRIGGER_ID const &kID, NiNode *pkTriggerRoot, TiXmlElement const *pkElement )
{
	if ( !Init( kID, pkTriggerRoot ) )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	SetID(kID);

	TiXmlAttribute const *pkAttribute = pkElement->FirstAttribute();

	char const *pcAttrName = NULL;
	char const *pcAttrValue = NULL;
	while ( pkAttribute )
	{
		pcAttrName = pkAttribute->Name();
		pcAttrValue = pkAttribute->Value();

		if( !::strcmp(pcAttrName, "PARAM") )
		{
			iJobGrade = ::atoi( pcAttrValue );
		}
		pkAttribute = pkAttribute->Next();
	}

	return true;
}

bool PgGTrigger_JobSkill::Event( CUnit *pkUnit, PgGround * const pkGround, BM::CPacket * const pkPacket )
{
	BM::CPacket kPacket;	

	GTRIGGER_ID kName = GetID();

	if( !Enable() )
	{
		goto __FAILED;
	}

	if( !pkUnit )
	{
		goto __FAILED;
	}

	if ( !IsInPos( pkUnit->GetPos() ) )
	{
		// 포탈을 벗어났습니다.
		/*pkUnit->SendWarnMessage( 18995 );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		goto __FAILED;*/
	}

	if( !pkGround )
	{
		goto __FAILED;
	}

	PgPlayer *pkPlayer = dynamic_cast<PgPlayer*>(pkUnit);
	if( !pkPlayer )
	{
		goto __FAILED;
	}
	int iSkillNo = 0;
	pkPacket->Pop( iSkillNo );

	int iErrMgs = 0;
	// 유저 등록	
	CONT_DEF_JOBSKILL_SKILL const* pkDefJobSkill = NULL;
	g_kTblDataMgr.GetContDef(pkDefJobSkill);
	if( !pkDefJobSkill )
	{
		goto __FAILED;
	}

	EGatherType eGatherType = GT_None;
	if( !(pkGround->JobSkillLocationMgr().GetGatherType(m_kID, eGatherType)) )
	{//3. 기술 지역 오류
		pkUnit->SendWarnMessage(25014);
		goto __FAILED;
	}

	CONT_DEF_JOBSKILL_SKILL const* pkJSSkill = NULL;
	CONT_DEF_JOBSKILL_SKILLEXPERTNESS const* pkJSExpertness = NULL;
	g_kTblDataMgr.GetContDef(pkJSSkill);
	g_kTblDataMgr.GetContDef(pkJSExpertness);

	int const iMainSkillNo = pkGround->JobSkillLocationMgr().GetSkillNo(iJobGrade);
	int const iSubSkillNo = JobSkillUtil::GetJobSkillNo(eGatherType, JST_1ST_SUB, *pkDefJobSkill);
	//1. 도구장착 오류
	int const iUseSkillNo = JobSkillToolUtil::GetUseSkill(pkPlayer, eGatherType, iMainSkillNo, iSubSkillNo);
	if( 0 == iUseSkillNo)
	{//오류메세지는 JobSkillToolUtil::GetUseSkill()에서 처리한다.
		goto __FAILED;
	}

	int const iUseSkillExpertness = pkPlayer->JobSkillExpertness().Get(iUseSkillNo);
	int iNeedExhaustion = JobSkillExpertnessUtil::GetUseExhaustion(iUseSkillNo, iUseSkillExpertness, *pkJSSkill, *pkJSExpertness);
	int const iMaxExhaustion = JobSkillExpertnessUtil::GetBiggestMaxExhaustion(pkPlayer->JobSkillExpertness().GetAllSkillExpertness(), *pkJSSkill, *pkJSExpertness, JST_1ST_MAIN);
	int const iCurExhaustion = pkPlayer->JobSkillExpertness().CurExhaustion();
	if( iMaxExhaustion == iCurExhaustion)
	{//2. 피로도 오류
		pkUnit->SendWarnMessage(25013);
		goto __FAILED;
	}
	if( iMaxExhaustion <= (iCurExhaustion + iNeedExhaustion) )
	{
		iNeedExhaustion = iMaxExhaustion - iCurExhaustion;
	}

	int iSkillGatherType = JobSkillUtil::GetGatherType(iSkillNo, *pkDefJobSkill);
	if( iSkillGatherType != static_cast<int>(eGatherType) )
	{//4. 적정 지역 오류
		pkGround->JobSkillLocationMgr().DelUser(pkUnit, GetID(), pkUnit->GetID());	//현재 적용중인 채집 스킬을 스킬을 종료하고
		int iCorrectSkillNo = pkGround->JobSkillLocationMgr().GetSkillNo(iJobGrade);
		BM::CPacket kPacket(PT_M_C_NFY_JOBSKILL_ERROR, JSEC_WRONG_SKILL_LOCATION);
		kPacket.Push( iSkillNo );
		kPacket.Push( iCorrectSkillNo );
		pkUnit->Send(kPacket);
		goto __FAILED;
	}	

	if( !pkGround->JobSkillLocationMgr().IsEnable(m_kID) )
	{//5. 비활성화 오류
		pkUnit->SendWarnMessage(25011);
		goto __FAILED;
	}

	if( !pkGround->JobSkillLocationMgr().CheckSkillExpertness(pkPlayer, iJobGrade, iErrMgs) )
	{//에러 메세지 처리 세분화 필요, 패킷 날려서 확인 해야 함.
	 //6. 도감 학습 오류
		if( iErrMgs )
		{
			pkUnit->SendWarnMessage(iErrMgs);
		}
		goto __FAILED;
	}
	
	if( (0 < iUseSkillNo) && (!kName.empty()) )
	{
		DWORD dwOutTurnTime = 0;
		if( pkGround->JobSkillLocationMgr().SetUser(kName, pkPlayer, iUseSkillNo, dwOutTurnTime, iNeedExhaustion) )
		{
			pkGround->JobSkillLocationMgr().SendLocationAction(kPacket, true);
			kPacket.Push( iUseSkillNo );	// 사용할 스킬 번호
			kPacket.Push( GetID() );		// 사용되는 트리거의 아이디(바라 봐야할 위치 세팅을 하기위해)
			kPacket.Push( dwOutTurnTime );

			pkUnit->Send(kPacket);

			pkUnit->SetAbil(AT_CANNOT_EQUIP, 1);
			return true;
		}
		else
		{//트리거에 유저 등록 실패
			VERIFY_INFO_LOG( false, BM::LOG_LV1, __FL__ << _T("JobSkill TriggerID User Add Fail[") << pkPlayer->GetID().str().c_str() << _T("]") );
		}
	}
	else
	{
		// 스킬 설정이 되어 있지 않음
		VERIFY_INFO_LOG( false, BM::LOG_LV1, __FL__ << _T("JobSkill TriggerID Location SkillNo is not Data[") << iJobGrade << _T("]") );
	}

__FAILED:
	{
		if( !pkGround )
		{
			return false;
		}

		if( !pkUnit )
		{
			return false;
		}
		
		pkGround->JobSkillLocationMgr().SendLocationAction(kPacket, false);
		pkUnit->Send(kPacket);

		return false;
	}
}

//============================================================================
// PgGTrigger_Double_Up
//----------------------------------------------------------------------------
PgGTrigger_Double_Up::PgGTrigger_Double_Up(void)
	: PgGroundTrigger()
{
}

PgGTrigger_Double_Up::PgGTrigger_Double_Up(PgGTrigger_Double_Up const& rhs)
	: PgGroundTrigger(rhs)
{
}

PgGTrigger_Double_Up::~PgGTrigger_Double_Up(void)
{
}

bool PgGTrigger_Double_Up::Build( GTRIGGER_ID const &kID, NiNode *pkTriggerRoot, TiXmlElement const *pkElement )
{
	if ( !Init( kID, pkTriggerRoot ) )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	return true;
}

bool PgGTrigger_Double_Up::Event( CUnit *pkUnit, PgGround * const pkGround, BM::CPacket * const pkPacket )
{
	if( !Enable() )
	{
		return false;
	}	
	return true;
}