#include "stdafx.h"
#include "BM/PgTask.h"
#include "PgPartyItemRule.h"
#include "Variant/PgParty.h"
#include "Variant/PgPartyMgr.h"
#include "PgLocalPartyMgr.h"

PgLocalPartyMgrImpl::PgLocalPartyMgrImpl()
	: PgPartyMgr::T_MY_BASE_MGR_TYPE(), m_kItemRulePool(100, 100), m_kPartyItemRule()
{
}

PgLocalPartyMgrImpl::~PgLocalPartyMgrImpl()
{
	ContPartyItemRule::iterator itr = m_kPartyItemRule.begin();
	for ( ; itr!=m_kPartyItemRule.end() ; ++itr )
	{
		m_kItemRulePool.Delete( itr->second );
	}
	m_kPartyItemRule.clear();
}

void PgLocalPartyMgrImpl::Clear()
{
	T_MY_BASE_MGR_TYPE::Clear();
	ContPartyItemRule::iterator itr = m_kPartyItemRule.begin();
	for ( ; itr!=m_kPartyItemRule.end() ; ++itr )
	{
		m_kItemRulePool.Delete( itr->second );
	}
	m_kPartyItemRule.clear();
}

bool PgLocalPartyMgrImpl::WriteToPacketPartyName(BM::GUID const& rkPartyGuid, BM::CPacket& rkPacket)
{
	PgLocalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	return WriteToPacketPartyName(pkParty, rkPacket);
}

typedef struct tagFunctionFindGroundPartyForEach : public PgLocalPartyMgrImpl::SFunctionForEach
{
	tagFunctionFindGroundPartyForEach(BM::CPacket& rkPacket)
		: m_rkPacket(rkPacket)
	{
	}

	virtual void operator() (PgLocalPartyMgrImpl::PartyHash const& rkContParty)
	{
		size_t const iRetWrPos = m_rkPacket.WrPos();
		m_rkPacket.Push( static_cast< BYTE >(PRC_Fail_NoMaster) ); // 없는걸 기본으로
		size_t const iCountWrPos = m_rkPacket.WrPos();
		int iCount = 0;
		m_rkPacket.Push( iCount );

		PgLocalPartyMgrImpl::PartyHash::const_iterator iter = rkContParty.begin();
		while( rkContParty.end() != iter )
		{
			PgLocalPartyMgrImpl::PartyHash::mapped_type const pkParty = iter->second;
			if( pkParty )
			{
				if( pkParty->Option().GetOptionPublicTitle() == POT_Public
				&&	pkParty->MaxMemberCount() > pkParty->MemberCount() )
				{
					m_rkPacket.Push( pkParty->PartyGuid() );
					m_rkPacket.Push( pkParty->PartyName() );
					m_rkPacket.Push( static_cast< BYTE >(pkParty->MemberCount()) );
					m_rkPacket.Push( static_cast< BYTE >(pkParty->MaxMemberCount()) );
					m_rkPacket.Push( pkParty->MasterCharGuid() );
					m_rkPacket.Push( pkParty->Option().iPartyOption );
					m_rkPacket.Push( pkParty->Option().iPartyLevel );
					m_rkPacket.Push( pkParty->Option().PartySubName() );
					m_rkPacket.Push( pkParty->Option().iPartyAttribute );
					m_rkPacket.Push( pkParty->Option().iPartyContinent );
					m_rkPacket.Push( pkParty->Option().iPartyArea_NameNo );

					++iCount;
					if( PV_MAX_LIST_CNT <= iCount )
					{
						break;
					}
				}
			}

			++iter;
		}

		if( 0 < iCount )
		{
			BYTE const ucRet = PRC_Success;
			m_rkPacket.ModifyData(iRetWrPos, &ucRet, sizeof(ucRet));
			m_rkPacket.ModifyData(iCountWrPos, &iCount, sizeof(iCount));
		}
	}
private:
	BM::CPacket& m_rkPacket;
} SFunctionFindGroundPartyForEach;

void PgLocalPartyMgrImpl::WriteToPacketPartyList(BM::CPacket& rkPacket)
{
	ForEach( SFunctionFindGroundPartyForEach(rkPacket) );
}

bool PgLocalPartyMgrImpl::SyncFromContents(BM::GUID const& rkPartyGuid, BM::CPacket& rkPacket)
{
	PgLocalParty* pkParty = GetParty(rkPartyGuid);

	if( !pkParty )
	{
		SPartyOption kPartyOption;
		kPartyOption.Clear();
		HRESULT const hResult = NewParty(rkPartyGuid, pkParty, kPartyOption);//널 파티 생성
		if( PRC_Success != hResult )
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false; // Update 없음
		}
	}

	size_t const iPrevMemberCount = pkParty->MemberCount();
	
	pkParty->ReadFromPacket(rkPacket);//Sync

	size_t const iMemberCount = pkParty->MemberCount();
	if( !iMemberCount )//맴버가 없으면
	{
		Delete(rkPartyGuid);//파티 삭제
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false; // Update 없음
	}

	SyncPartyItemRule(pkParty);//Item Rule Sync

	if( iPrevMemberCount == iMemberCount )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false; // Update 없음
	}

	return true; // Update 있음
}

bool PgLocalPartyMgrImpl::Delete(BM::GUID const& rkPartyGuid)
{
	//Local에서만 삭제된다.
	PgLocalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;//이서버에 없다
	}

	DelItemRule(rkPartyGuid);//룰 삭제
	PgPartyMgr::Delete(rkPartyGuid);
	return true;
}

bool PgLocalPartyMgrImpl::Leave(SGroundKey const& rkGndKey, BM::CPacket& rkPacket)
{
	BM::GUID kPartyGuid;
	BM::GUID kLeaverGuid;
	bool bChangeMaster = false;
	BM::GUID kNewMasterGuid;

	rkPacket.Pop(kPartyGuid);
	rkPacket.Pop(kLeaverGuid);
	rkPacket.Pop(bChangeMaster);
	if( bChangeMaster )
	{
		rkPacket.Pop(kNewMasterGuid);
	}

	PgLocalParty* pkParty = GetParty(kPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	PgPartyItemRule* pkItemRule = NULL;
	bool const bFindRule = GetItemRule(kPartyGuid, pkItemRule);
	if( bFindRule )
	{
		pkItemRule->Del(rkGndKey, kLeaverGuid);
	}
	assert(bFindRule);

	SPartyUserInfo kLeaverInfo;//삭제 유저
	bool const bFindUser = pkParty->GetMemberInfo(kLeaverGuid, kLeaverInfo);
	if( !bFindUser )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	//
	HRESULT const hResult = pkParty->Del(kLeaverGuid, kNewMasterGuid);

	//
	if( PRC_Success_Destroy == hResult ) // 파티가 삭제되면 모든 파티원들의 Guid를 초기화 한다
	{
		Delete(kPartyGuid);
	}
	return true;
}

bool PgLocalPartyMgrImpl::ChangeMaster(BM::GUID const& rkPartyGuid, BM::GUID const& rkNewMasterGuid)
{
	assert(GUID_NULL != rkPartyGuid);

	PgLocalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	bool const bChanged = pkParty->ChangeMaster(rkNewMasterGuid);
	if( !bChanged )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	return true;
}

bool PgLocalPartyMgrImpl::Rename(BM::GUID const& rkPartyGuid, std::wstring const& rkNewPartyName,  SPartyOption const& rkNewPartyOption)
{
	PgLocalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	if( rkNewPartyName == pkParty->PartyName() )
	{
		if( rkPartyGuid != pkParty->PartyGuid()  )
			return true;
	}

	pkParty->PartyName(rkNewPartyName);

	pkParty->Option(rkNewPartyOption);
	return true;
}

bool PgLocalPartyMgrImpl::WriteToPacketPartyName(PgLocalParty* pkParty, BM::CPacket& rkPacket)
{
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	rkPacket.Clear();

	rkPacket.Push((BM::CPacket::DEF_PACKET_TYPE)PT_M_C_ANS_PARTY_NAME);
	rkPacket.Push(pkParty->PartyGuid());
	rkPacket.Push(pkParty->PartyName());
	rkPacket.Push((BYTE)pkParty->MemberCount());
	rkPacket.Push((BYTE)pkParty->MaxMemberCount());
	rkPacket.Push(pkParty->MasterCharGuid());
	pkParty->Option().WriteToPacket(rkPacket);
	return true;
}

bool PgLocalPartyMgrImpl::MapMoved(BM::GUID const& rkPartyGuid, SGroundKey const& rkCurGndKey, SGroundKey const& rkTrgGndKey, VEC_GUID const& kVec)
{
	PgLocalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	PgPartyItemRule* pkItemRule = NULL;
	bool const bFindItemRule = GetItemRule(rkPartyGuid, pkItemRule);

	SChnGroundKey const kTrgChnGndKey( rkTrgGndKey, g_kProcessCfg.ChannelNo() );

	VEC_GUID::const_iterator guid_iter = kVec.begin();
	while(kVec.end() != guid_iter)
	{
		SPartyUserInfo* pkMember = NULL;
		VEC_GUID::value_type const& rkCharGuid = (*guid_iter);
		bool const bFindMember = pkParty->Get(rkCharGuid, pkMember);
		if( bFindMember )
		{
			SGroundKey const kOldGndKey = static_cast<SGroundKey>(pkMember->kChnGndKey);
			bool const bMovedGnd = pkParty->MovedGnd( rkCharGuid, kTrgChnGndKey );
			if( bMovedGnd
			&&	bFindItemRule )
			{
				pkItemRule->MapMove(kOldGndKey, rkTrgGndKey, rkCharGuid);
			}
		}
		++guid_iter;
	}

	bool const bDeleteParty = IsRemainMemberInServer( pkParty, rkCurGndKey );//관리하는 파티유저가 없으면 파티정보 삭제
	if( bDeleteParty )
	{
		Delete(rkPartyGuid);//
	}
	return true;
}

bool PgLocalPartyMgrImpl::IsRemainMemberInServer( PgLocalParty const *pkParty, SGroundKey const& rkGndKey )const
{
	SChnGroundKey const kChnGndKey( rkGndKey, g_kProcessCfg.ChannelNo() );

	VEC_GUID kVec;
	pkParty->GetMemberCharGuidList(kVec);

	VEC_GUID::iterator member_iter = kVec.begin();
	while(kVec.end() != member_iter)
	{
		SPartyUserInfo kMemberInfo;
		bool const bFindMember = pkParty->GetMemberInfo((*member_iter), kMemberInfo);
		if( bFindMember )//맴버를 찾았으면
		{
			if( kChnGndKey == kMemberInfo.kChnGndKey )//이 유저의 Gnd를 서버가 관리하면
			{
				return false;//삭제 하지 마라
			}
		}
		++member_iter;
	}

	return true;//삭제 해라
}

bool PgLocalPartyMgrImpl::UpdateProperty(BM::GUID const& rkPartyGuid, BM::CPacket& rkPacket)
{
	PgLocalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	BM::GUID kCharGuid;
	BYTE cPropertyType = 0;

	rkPacket.Pop(kCharGuid);
	rkPacket.Pop(cPropertyType);
	
	BYTE const cChangeAbil = pkParty->ChangeAbility(kCharGuid, cPropertyType, rkPacket);
	if( 0 != cChangeAbil )
	{
		BM::CPacket kPacket(PT_M_N_NFY_PARTY_USER_PROPERTY);
		kPacket.Push( kCharGuid );
		if( pkParty->WriteToChangedAbil(kCharGuid, cChangeAbil, kPacket) )
		{
			// 공용맵에서는  어쩌냐.
			SendToGlobalPartyMgr(kPacket);
		}
	}

	return true;
}

bool PgLocalPartyMgrImpl::IsMaster(BM::GUID const& rkPartyGuid, BM::GUID const& rkCharGuid)
{
	if( rkPartyGuid == BM::GUID::NullData() )
	{
		return false;
	}

	PgLocalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		return false;
	}
	return pkParty->IsMaster(rkCharGuid);
}

bool PgLocalPartyMgrImpl::GetPartyMasterGuid(BM::GUID const& rkPartyGuid, BM::GUID& rkOutGuid)const
{
	if( rkPartyGuid == BM::GUID::NullData() )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	PgLocalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	rkOutGuid = pkParty->MasterCharGuid();
	return true;
}


bool PgLocalPartyMgrImpl::ProcessMsg(unsigned short const usType, SGroundKey const& rkGndKey, BM::CPacket* pkMsg)
{
	if( !pkMsg )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	switch( usType )
	{
	case PT_A_T_UPDATE_PARTY_PROPERTY:
		{
			BM::GUID kPartyGuid;
			pkMsg->Pop(kPartyGuid);

			UpdateProperty(kPartyGuid, *pkMsg);
		}break;
	case PT_N_M_NFY_PARTY_USER_MAP_MOVE://맵 이동 하면
		{
			BM::GUID kPartyGuid;
			BM::GUID kCharGuid;
			SGroundKey kTrgGndKey;

			pkMsg->Pop(kPartyGuid);
			pkMsg->Pop(kTrgGndKey);
			pkMsg->Pop(kCharGuid);

			VEC_GUID kVec;
			kVec.push_back(kCharGuid);
			MapMoved(kPartyGuid, rkGndKey, kTrgGndKey, kVec);
		}break;
	case PT_N_M_NFY_LEAVE_PARTY_USER:
		{
			Leave(rkGndKey, *pkMsg);
		}break;
	case PT_N_M_NFY_PARTY_CHANGE_MASTER:
		{
			BM::GUID kPartyGuid;
			BM::GUID kNewMasterGuid;

			pkMsg->Pop(kPartyGuid);
			pkMsg->Pop(kNewMasterGuid);

			ChangeMaster(kPartyGuid, kNewMasterGuid);
		}break;
	case PT_N_M_NFY_DELETE_PARTY:
		{
			BM::GUID kPartyGuid;

			pkMsg->Pop(kPartyGuid);

			Delete(kPartyGuid);
		}break;
	case PT_N_M_NFY_PARTY_RENAME:
		{
			BM::GUID kPartyGuid;
			std::wstring kNewPartName;
			SPartyOption kNewOption;

			pkMsg->Pop(kPartyGuid);
			pkMsg->Pop(kNewPartName);
			kNewOption.ReadFromPacket(*pkMsg);

			Rename(kPartyGuid, kNewPartName, kNewOption);
		}break;
	default:
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV3, __FL__<<L"invalid packet type ["<<usType<<L"]");
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}
	}
	return true;
}

bool PgLocalPartyMgrImpl::GetPartyMemberGround(BM::GUID const& rkPartyGuid, SGroundKey const& rkGndKey, VEC_GUID& rkOutVec, BM::GUID const& rkIgnore)const
{
	PgLocalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	SChnGroundKey kChnGndKey( rkGndKey, g_kProcessCfg.ChannelNo() );
	return pkParty->GetLocalMemberList( kChnGndKey, rkOutVec, rkIgnore);
}

size_t PgLocalPartyMgrImpl::GetMemberCount(BM::GUID const& rkPartyGuid)const
{
	PgLocalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return 1"));
		return 1;//파티가 없으면 파티원은 1명(나자신)
	}
	return pkParty->MemberCount();
}

bool PgLocalPartyMgrImpl::GetTotalLevel(BM::GUID const& rkPartyGuid, SGroundKey const& rkGndKey, unsigned int& iOutTotalLevel)
{
	PgLocalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;//파티가 없으니 총 레벨은 -_-;
	}

	SChnGroundKey kChnGndKey( rkGndKey, g_kProcessCfg.ChannelNo() );
	return pkParty->GetGndTotalLevel(kChnGndKey, iOutTotalLevel);
}

bool PgLocalPartyMgrImpl::GetPartyOption(BM::GUID const& rkPartyGuid, SPartyOption& rkOut)
{
	PgLocalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;//실패
	}

	rkOut = pkParty->Option();
	return true;//성공
}

bool PgLocalPartyMgrImpl::GetPartyShareItem_NextOwner(BM::GUID const& rkPartyGuid, SGroundKey const& rkGndKey, BM::GUID& rkNextOwner)
{
	PgLocalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;//실패
	}

	PgPartyItemRule* pkItemRule = NULL;
	bool bFindRule = GetItemRule(rkPartyGuid, pkItemRule);
	if( !bFindRule )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	bool const bFindNext = pkItemRule->GetNext(rkGndKey, rkNextOwner);
	return bFindNext;
}

bool PgLocalPartyMgrImpl::AddItemRule(BM::GUID const& rkPartyGuid, PgPartyItemRule*& pkOut)//파티 아이템 습득 룰을 만든다
{
	PgPartyItemRule* pkNewRule = m_kItemRulePool.New();
	if( !pkNewRule )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	ContPartyItemRule::_Pairib kRet = m_kPartyItemRule.insert( std::make_pair(rkPartyGuid, pkNewRule) );
	if( !kRet.second )
	{
		m_kItemRulePool.Delete(pkNewRule);
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}
	pkOut = pkNewRule;
	return true;
}

bool PgLocalPartyMgrImpl::GetItemRule(BM::GUID const& rkPartyGuid, PgPartyItemRule*& pkOut)
{
	ContPartyItemRule::iterator rule_iter = m_kPartyItemRule.find(rkPartyGuid);
	if( m_kPartyItemRule.end() != rule_iter)
	{
		pkOut = (*rule_iter).second;
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return true;
	}
	return false;
}

bool PgLocalPartyMgrImpl::DelItemRule(BM::GUID const& rkPartyGuid)
{
	PgPartyItemRule* pkItemRule = NULL;
	bool const bFindRule = GetItemRule(rkPartyGuid, pkItemRule);
	if( bFindRule )
	{
		pkItemRule->Clear();

		m_kPartyItemRule.erase(rkPartyGuid);
		m_kItemRulePool.Delete(pkItemRule);
		return true;
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgLocalPartyMgrImpl::SyncPartyItemRule(PgLocalParty* pkParty)
{
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	BM::GUID const& rkPartyGuid = pkParty->PartyGuid();

	//Item룰 생성
	PgPartyItemRule* pkItemRule = NULL;
	bool const bFindRuleRet = GetItemRule(rkPartyGuid, pkItemRule);
	if( !bFindRuleRet )
	{
		bool const bNewRuleRet = AddItemRule(rkPartyGuid, pkItemRule);
		if( !bNewRuleRet )
		{
			INFO_LOG(BM::LOG_LV0, __FL__<<L"Can't make party item rule");	
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}
	}

	VEC_GUID kVec;
	pkParty->GetMemberCharGuidList(kVec);

	ContPartyMemberGndKey kSet;
	VEC_GUID::const_iterator guid_iter = kVec.begin();
	while(kVec.end() != guid_iter)
	{
		VEC_GUID::value_type const& rkCharGuid = (*guid_iter);
		SPartyUserInfo* pkMember = NULL;
		bool const bFindMember = pkParty->Get(rkCharGuid, pkMember);
		if( bFindMember )
		{
			if ( g_kProcessCfg.ChannelNo() == pkMember->kChnGndKey.Channel() )
			{
				SGroundKey const kGndKey = static_cast<SGroundKey>(pkMember->kChnGndKey);
				pkItemRule->Add( kGndKey, pkMember->kCharGuid);//무조건 추가

				SPartyMemberGndKey kMemberGndKey( kGndKey, pkMember->kCharGuid );
				ContPartyMemberGndKey::_Pairib kRet = kSet.insert( kMemberGndKey );
				if( !kRet.second )
				{
					//
				}
			}
		}
		++guid_iter;
	}
	//이후에 없는것 삭제

	pkItemRule->Sync(kSet);

	return true;
}

bool PgLocalPartyMgrImpl::GetPartyMember(BM::GUID const& rkPartyGuid, VEC_GUID& rkOut)const
{
	PgLocalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}

	pkParty->GetMemberCharGuidList(rkOut);
	return true;
}

int PgLocalPartyMgrImpl::GetPartyMemberFriend(BM::GUID const& rkPartyGuid, BM::GUID const& rkCharGuid)
{
	PgLocalParty* pkParty = GetParty(rkPartyGuid);
	if( !pkParty )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return 0"));
		return 0;
	}
	return pkParty->GetLocalMemberFriend(rkCharGuid);
}


//
//
PgLocalPartyMgr::PgLocalPartyMgr()
{
}
PgLocalPartyMgr::~PgLocalPartyMgr()
{
}

void PgLocalPartyMgr::Clear()
{
	BM::CAutoMutex kLock(m_kMutex_Wrapper_, true);
	Instance()->Clear();
}
bool PgLocalPartyMgr::GetPartyMemberGround(BM::GUID const& rkPartyGuid, SGroundKey const& rkGndKey, VEC_GUID& rkOutVec, BM::GUID const& rkIgnore)const
{
	BM::CAutoMutex kLock(m_kMutex_Wrapper_);
	return Instance()->GetPartyMemberGround(rkPartyGuid, rkGndKey, rkOutVec, rkIgnore);
}
size_t PgLocalPartyMgr::GetMemberCount(BM::GUID const& rkPartyGuid)const
{
	BM::CAutoMutex kLock(m_kMutex_Wrapper_);
	return Instance()->GetMemberCount(rkPartyGuid);
}
bool PgLocalPartyMgr::GetTotalLevel(BM::GUID const& rkPartyGuid, SGroundKey const& rkGndKey, unsigned int& iOutTotalLevel)
{
	BM::CAutoMutex kLock(m_kMutex_Wrapper_);
	return Instance()->GetTotalLevel(rkPartyGuid, rkGndKey, iOutTotalLevel);
}
bool PgLocalPartyMgr::GetPartyMember(BM::GUID const& rkPartyGuid, VEC_GUID& rkOut)const
{
	BM::CAutoMutex kLock(m_kMutex_Wrapper_);
	return Instance()->GetPartyMember(rkPartyGuid, rkOut);
}
bool PgLocalPartyMgr::IsMaster(BM::GUID const& rkPartyGuid, BM::GUID const& rkCharGuid)
{
	BM::CAutoMutex kLock(m_kMutex_Wrapper_);
	return Instance()->IsMaster(rkPartyGuid, rkCharGuid);
}
bool PgLocalPartyMgr::GetPartyMasterGuid(BM::GUID const& rkPartyGuid, BM::GUID& rkOutGuid)const
{
	BM::CAutoMutex kLock(m_kMutex_Wrapper_);
	return Instance()->GetPartyMasterGuid(rkPartyGuid, rkOutGuid);
}
bool PgLocalPartyMgr::GetPartyOption(BM::GUID const& rkPartyGuid, SPartyOption& rkOut)
{
	BM::CAutoMutex kLock(m_kMutex_Wrapper_);
	return Instance()->GetPartyOption(rkPartyGuid, rkOut);
}
bool PgLocalPartyMgr::GetPartyShareItem_NextOwner(BM::GUID const& rkPartyGuid, SGroundKey const& rkGndKey, BM::GUID& rkNextOwner)
{
	BM::CAutoMutex kLock(m_kMutex_Wrapper_);
	return Instance()->GetPartyShareItem_NextOwner(rkPartyGuid, rkGndKey, rkNextOwner);
}
bool PgLocalPartyMgr::WriteToPacketPartyName(BM::GUID const& rkPartyGuid, BM::CPacket& rkPacket)
{
	BM::CAutoMutex kLock(m_kMutex_Wrapper_);
	return Instance()->WriteToPacketPartyName(rkPartyGuid, rkPacket);
}
void PgLocalPartyMgr::WriteToPacketPartyList(BM::CPacket& rkPacket)
{
	BM::CAutoMutex kLock(m_kMutex_Wrapper_);
	Instance()->WriteToPacketPartyList(rkPacket);
}
bool PgLocalPartyMgr::ProcessMsg(unsigned short const usType, SGroundKey const& rkGndKey, BM::CPacket* pkMsg)
{
	BM::CAutoMutex kLock(m_kMutex_Wrapper_, true);
	return Instance()->ProcessMsg(usType, rkGndKey, pkMsg);
}
bool PgLocalPartyMgr::SyncFromContents(BM::GUID const& rkPartyGuid, BM::CPacket& rkPacket)
{
	BM::CAutoMutex kLock(m_kMutex_Wrapper_, true);
	return Instance()->SyncFromContents(rkPartyGuid, rkPacket);
}
int PgLocalPartyMgr::GetPartyMemberFriend(BM::GUID const& rkPartyGuid, BM::GUID const& rkCharGuid)
{
	BM::CAutoMutex kLock(m_kMutex_Wrapper_);
	return Instance()->GetPartyMemberFriend(rkPartyGuid, rkCharGuid);
}