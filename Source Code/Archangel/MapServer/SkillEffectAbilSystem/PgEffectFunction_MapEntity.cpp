#include "stdafx.h"
#include "PgEffectAbilTable.h"
#include "PgEffectFunction_MapEntity.h"
#include "Variant/Global.h"
#include "Global.h"
#include "Variant/PgPartyMgr.h"
#include "PgLocalPartyMgr.h"
#include "PgGround.h"
#include "Variant/PgParty.h"
#include "Variant/PgPartyMgr.h"
#include "PgPartyItemRule.h"
#include "PgLocalPartyMgr.h"

///////////////////////////////////////////////////////////
//  PgDontJumpFunction
///////////////////////////////////////////////////////////
//void PgDontJumpFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
//{
////	int const iEffectNo = pkEffect->GetAbil(AT_EFFECTNUM1);
////
////	PgGround* pkGround = NULL;
////	pkArg->Get(ACTARG_GROUND, pkGround);
////
////	if(!pkGround)
////	{
////		INFO_LOG(BM::LOG_LV6, _T("[%s][%d] Cannot find Ground"), __FUNCTIONW__, pkEffect->GetEffectNo());
////		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkGround is NULL"));
////		return;
////	}
////
////	int const iMaxTarget = pkEffect->GetAbil(AT_MAX_TARGETNUM);
////
////	UNIT_PTR_ARRAY kUnitArray;
////	PgEntity* pkEntity = dynamic_cast<PgEntity*>(pkUnit);
////	if(pkEntity)
////	{
////		pkGround->FindEnemy(pkEntity, kUnitArray, iMaxTarget, UT_PLAYER );
////	}
////		
////	UNIT_PTR_ARRAY::const_iterator target_itor = kUnitArray.begin();
////	while(kUnitArray.end() != target_itor)
////	{			
////		//Effect의 Target List에 없으면
////		if(!pkEffect->IsTarget((*target_itor).pkUnit->GetID()))
////		{
////			CUnit* pkTarget = (*target_itor).pkUnit;
////			if(pkTarget)
////			{
////				EffectQueueData kData(EQT_ADD_EFFECT, iEffectNo, 0, pkArg, pkUnit, EFFECT_TYPE_GROUND);
////				pkTarget->AddEffectQueue(kData);
////
////				pkEffect->AddTarget(pkTarget->GetID());
//////					INFO_LOG(BM::LOG_LV6, _T("[%s][%d] AddTarget %s "), __FUNCTIONW__, pkEffect->GetEffectNo(), pkTarget->Name().c_str());
////			}
////		}
////
////		++target_itor;
////	}
//}

void PgDontJumpFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	PgGround* pkGround = NULL;
	pkArg->Get(ACTARG_GROUND, pkGround);

	if(!pkGround)
	{
		INFO_LOG(BM::LOG_LV6, __FL__<<L"Cannot find Ground, EffectNo="<<pkEffect->GetEffectNo());
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkGround is NULL"));
		return ;
	}

	int const iAuraEffectNo = pkEffect->GetAbil(AT_EFFECTNUM1);
	VEC_GUID& rkTargetList = pkEffect->GetTargetList();

	VEC_GUID::const_iterator target_itor = rkTargetList.begin();
	while(rkTargetList.end() != target_itor)
	{		
		CUnit* pkTarget = pkGround->GetUnit((*target_itor));
		if(pkTarget)
		{
			//pkTarget->DeleteEffect(iAuraEffectNo);
			EffectQueueData kData(EQT_DELETE_EFFECT, iAuraEffectNo);
			pkTarget->AddEffectQueue(kData);
		}

		++target_itor;
	}

	rkTargetList.clear();
}
//int PgDontJumpFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
//{
//	int const iEffectNo = pkEffect->GetAbil(AT_EFFECTNUM1);
//
//	PgGround* pkGround = NULL;
//	pkArg->Get(ACTARG_GROUND, pkGround);
//
//	if(!pkGround)
//	{
//		INFO_LOG(BM::LOG_LV6, __FL__<<L"Cannot find Ground, EffectNo="<<pkEffect->GetEffectNo());
//		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkGround is NULL"));
//		return ECT_MUSTDELETE;
//	}
//
//	int const iMaxTarget = pkEffect->GetAbil(AT_MAX_TARGETNUM);
//
//	UNIT_PTR_ARRAY kUnitArray;
//	PgEntity* pkEntity = dynamic_cast<PgEntity*>(pkUnit);
//	if(pkEntity)
//	{
//		pkGround->FindEnemy(pkEntity, kUnitArray, iMaxTarget, UT_PLAYER );
//	}
//		
//	UNIT_PTR_ARRAY::const_iterator target_itor = kUnitArray.begin();
//	while(kUnitArray.end() != target_itor)
//	{			
//		//Effect의 Target List에 없으면
//		if(!pkEffect->IsTarget((*target_itor).pkUnit->GetID()))
//		{
//			CUnit* pkTarget = (*target_itor).pkUnit;
//			if(pkTarget)
//			{
//				EffectQueueData kData(EQT_ADD_EFFECT, iEffectNo, 0, pkArg, pkUnit, EFFECT_TYPE_GROUND);
//				pkTarget->AddEffectQueue(kData);
//
//				pkEffect->AddTarget(pkTarget->GetID());
////					INFO_LOG(BM::LOG_LV6, _T("[%s][%d] AddTarget %s "), __FUNCTIONW__, pkEffect->GetEffectNo(), pkTarget->Name().c_str());
//			}
//		}
//
//		++target_itor;
//	}
//	return ECT_DOTICK;
//}

///////////////////////////////////////////////////////////
//  PgParelPoisonAura - Effect Function 파렐경 맵 엔티티가 사용하는 이펙트
///////////////////////////////////////////////////////////
void PgParelPoisonAura::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());
}
void PgParelPoisonAura::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	PgGround* pkGround = NULL;
	pkArg->Get(ACTARG_GROUND, pkGround);

	if(!pkGround)
	{
		INFO_LOG(BM::LOG_LV6, __FL__<<L"Cannot find Ground, EffectNo="<<pkEffect->GetEffectNo());
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkGround is NULL"));
		return ;
	}

	int const iAuraEffectNo = pkEffect->GetAbil(AT_EFFECTNUM1);
	VEC_GUID& rkTargetList = pkEffect->GetTargetList();

	VEC_GUID::const_iterator target_itor = rkTargetList.begin();
	while(rkTargetList.end() != target_itor)
	{		
		CUnit* pkTarget = pkGround->GetUnit((*target_itor));
		if(pkTarget)
		{
			//pkTarget->DeleteEffect(iAuraEffectNo);
			EffectQueueData kData(EQT_DELETE_EFFECT, iAuraEffectNo);
			pkTarget->AddEffectQueue(kData);
		}

		++target_itor;
	}

	rkTargetList.clear();
}

int PgParelPoisonAura::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	int const iEffectNo = pkEffect->GetAbil(AT_EFFECTNUM1);

	PgGround* pkGround = NULL;
	pkArg->Get(ACTARG_GROUND, pkGround);

	if(!pkGround)
	{
		INFO_LOG(BM::LOG_LV6, __FL__<<L"Cannot find Ground, EffectNo="<<pkEffect->GetEffectNo());
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkGround is NULL"));
		return ECT_MUSTDELETE;
	}

	int iMaxTarget = pkEffect->GetAbil(AT_MAX_TARGETNUM);

	// 이 이펙트를 쓰는 경우는 타겟팅이 0이면 20명까지 타겟팅 하도록 해준다.
	if(0 == iMaxTarget)
	{
		iMaxTarget = 20;
	}

	UNIT_PTR_ARRAY kUnitArray;
	PgEntity* pkEntity = dynamic_cast<PgEntity*>(pkUnit);
	if(pkEntity)
	{
		pkGround->FindEnemy(pkEntity, kUnitArray, iMaxTarget, UT_PLAYER );
	}
		
	UNIT_PTR_ARRAY::const_iterator target_itor = kUnitArray.begin();
	while(kUnitArray.end() != target_itor)
	{
		bool bAddEffect = false;

		CUnit* pkTarget = (*target_itor).pkUnit;
		if(pkTarget)
		{
			// 죽은 경우라면 이펙트 리스트에서 삭제 시켜 준다.
			if(pkTarget->IsState(US_DEAD))
			{
				pkEffect->DeleteTarget(pkTarget->GetID());
				++target_itor;
				continue;
			}

			//Effect의 Target List에 없으면
			if(!pkEffect->IsTarget(pkTarget->GetID()))
			{
				bAddEffect = true;
			}
			//Effect의 Target List에는 있으나
			else
			{			
				// 실제 이펙트는 걸려 있지 않은 경우(이펙트가 풀린 경우)
				CEffect* pkEffect2 = pkTarget->GetEffect(iEffectNo);
				if(NULL == pkEffect2)
				{
					bAddEffect = true;					
				}				
			}

			if(bAddEffect)
			{
				EffectQueueData kData(EQT_ADD_EFFECT, iEffectNo, 0, pkArg, pkUnit->GetID(), EFFECT_TYPE_GROUND);
				pkTarget->AddEffectQueue(kData);
				pkEffect->AddTarget(pkTarget->GetID());
			}
		}

		++target_itor;
	}

	//이펙트에 걸려있는 타겟들
	VEC_GUID& rkTargtList = pkEffect->GetTargetList();

	if(!rkTargtList.empty())
	{
		VEC_GUID::iterator target_guid_itor = rkTargtList.begin();
		while(target_guid_itor != rkTargtList.end())
		{
			bool bFind = false;
			//새로 잡은 타겟 리스트에 이펙트에 걸려있는 유저가 있는지 검색
			for(UNIT_PTR_ARRAY::const_iterator unit_itor = kUnitArray.begin(); unit_itor != kUnitArray.end(); ++unit_itor)
			{
				if((*unit_itor).pkUnit)
				{
					if((*unit_itor).pkUnit->GetID() == (*target_guid_itor))
					{
						bFind = true;
						break;
					}
				}
			}

			//범위를 벗어났으면 제거
			if(!bFind)
			{
				CUnit* pkTarget = pkGround->GetUnit(*target_guid_itor);
				if(pkTarget)
				{
					//pkTarget->DeleteEffect(iEffectNo);
					EffectQueueData kData(EQT_DELETE_EFFECT, iEffectNo);
					pkTarget->AddEffectQueue(kData);
				}
				else
				{
					INFO_LOG(BM::LOG_LV6, __FL__<<L"Effect["<<pkEffect->GetEffectNo()<<L"] Cannot Find Unit["<<(*target_guid_itor)<<L"] ");
				}
				target_guid_itor = rkTargtList.erase(target_guid_itor);
			}
			else
			{
				++target_guid_itor;
			}
		}
	}

	return ECT_DOTICK;
}


///////////////////////////////////////////////////////////
//  PgParelPoison - 파렐경 맵에 있는 독
///////////////////////////////////////////////////////////
void PgParelPoison::EffectBegin(CUnit* pkUnit, CEffect*pkEffect, SActArg const *pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());
}

void PgParelPoison::EffectEnd(CUnit* pkUnit, CEffect*pkEffect, SActArg const *pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());
}

int PgParelPoison::EffectTick(CUnit* pkUnit, CEffect*pkEffect, SActArg const *pkArg, DWORD const dwElapsed)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	//이펙트 중화 어빌값이 같을 경우 효과를 받지 않게 해준다.
	if(pkEffect->GetEffectNo() == pkUnit->GetAbil(AT_EXCEPT_EFFECT_TICK))
	{
		return ECT_DOTICK;
	}

	if(pkUnit->IsDead())
	{
		return ECT_DOTICK;
	}

	int const iAddHP = pkEffect->GetAbil(AT_HP);
	int const iNowHP = pkUnit->GetAbil(AT_HP);
	int iNewHP = __max(0, iNowHP + iAddHP);
	int const iDelta = iNewHP - iNowHP;

	BM::CPacket kPacket(PT_M_C_NFY_ABILCHANGED);
	kPacket.Push(pkUnit->GetID());
	kPacket.Push((short)AT_HP);
	kPacket.Push(iNewHP);
	kPacket.Push(pkEffect->GetCaster());
	kPacket.Push(pkEffect->GetEffectNo());
	kPacket.Push(iDelta);
	pkUnit->Send(kPacket, E_SENDTYPE_BROADALL);

	OnSetAbil(pkUnit, AT_HP, iNewHP);

	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgChaosMapAuraEffect - Effect Function 카오스 맵 버프 이펙트(카오스 맵에 있는 유저에게 디버프를 걸어준다.
///////////////////////////////////////////////////////////
void PgChaosMapAuraEffect::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());
}
void PgChaosMapAuraEffect::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	PgGround* pkGround = NULL;
	pkArg->Get(ACTARG_GROUND, pkGround);

	if(!pkGround)
	{
		INFO_LOG(BM::LOG_LV6, __FL__<<L"Cannot find Ground, EffectNo="<<pkEffect->GetEffectNo());
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkGround is NULL"));
		return ;
	}

	int const iAuraEffectNo = pkEffect->GetAbil(AT_EFFECTNUM1);
	VEC_GUID& rkTargetList = pkEffect->GetTargetList();

	VEC_GUID::const_iterator target_itor = rkTargetList.begin();
	while(rkTargetList.end() != target_itor)
	{		
		CUnit* pkTarget = pkGround->GetUnit((*target_itor));
		if(pkTarget)
		{
			//pkTarget->DeleteEffect(iAuraEffectNo);
			EffectQueueData kData(EQT_DELETE_EFFECT, iAuraEffectNo);
			pkTarget->AddEffectQueue(kData);
		}

		++target_itor;
	}

	rkTargetList.clear();
}

int PgChaosMapAuraEffect::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	int const iEffectNo = pkEffect->GetAbil(AT_EFFECTNUM1);

	PgGround* pkGround = NULL;
	pkArg->Get(ACTARG_GROUND, pkGround);

	if(!pkGround)
	{
		INFO_LOG(BM::LOG_LV6, __FL__<<L"Cannot find Ground, EffectNo="<<pkEffect->GetEffectNo());
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkGround is NULL"));
		return ECT_MUSTDELETE;
	}

	int const iDetectRange = pkEffect->GetAbil(AT_DETECT_RANGE);
	int const iMaxTarget = pkEffect->GetAbil(AT_MAX_TARGETNUM);

	UNIT_PTR_ARRAY kUnitArray;
	PgEntity* pkEntity = dynamic_cast<PgEntity*>(pkUnit);
	if(pkEntity)
	{
		pkGround->FindEnemy(pkEntity, kUnitArray, iMaxTarget, UT_PLAYER );
	}
		
	UNIT_PTR_ARRAY::const_iterator target_itor = kUnitArray.begin();
	while(kUnitArray.end() != target_itor)
	{			
		//Effect의 Target List에 없으면
		if(!pkEffect->IsTarget((*target_itor).pkUnit->GetID()))
		{
			CUnit* pkTarget = (*target_itor).pkUnit;
			if(pkTarget)
			{
				// 타겟이 해당 이펙트를 가지고 있고 리스트엔 없을 경우 리스트에만 추가
				if(CEffect* pkUnitEffect = pkTarget->GetEffect(iEffectNo))
				{
					pkEffect->AddTarget(pkTarget->GetID());
				}
				else
				{
					///타겟이 해당 이펙트도 가지고 있지 않고 리스트에도 없을 경우는 이펙트 + 리스트 추가
					EffectQueueData kData(EQT_ADD_EFFECT, iEffectNo, 0, pkArg, pkUnit->GetID(), EFFECT_TYPE_GROUND);
					pkTarget->AddEffectQueue(kData);

					pkEffect->AddTarget(pkTarget->GetID());
	//					INFO_LOG(BM::LOG_LV6, _T("[%s][%d] AddTarget %s "), __FUNCTIONW__, pkEffect->GetEffectNo(), pkTarget->Name().c_str());
				}

/* Pet은 안걸리게 막는다. 걸려야 한다면 여기를 풀것
				if(pkTarget->IsUnitType(UT_PLAYER))
				{ 
					PgPet* pkPet = pkGround->GetPet(dynamic_cast<PgPlayer*>(pkTarget));
					if(pkPet)
					{//장착 펫이 있으면
						//펫 한테도 디버프 이펙트를 걸어주어야 한다.
						//타겟이 해당 이펙트를 가지고 있고 리스트엔 없을 경우 리스트에만 추가
						if(CEffect* pkUnitEffect = pkPet->GetEffect(iEffectNo))
						{
							pkEffect->AddTarget(pkPet->GetID());
						}
						else
						{
							//타겟이 해당 이펙트도 가지고 있지 않고 리스트에도 없을 경우는 이펙트 + 리스트 추가
							EffectQueueData kData(EQT_ADD_EFFECT, iEffectNo, 0, pkArg, pkUnit->GetID(), EFFECT_TYPE_GROUND);
							pkPet->AddEffectQueue(kData);

							pkEffect->AddTarget(pkPet->GetID());
						}
					}
				}
*/
			}
		}	
		//Effect의 Target List에 있는 경우
		else
		{
			CUnit* pkTarget = (*target_itor).pkUnit;
			if(pkTarget)
			{
				CEffect* pkUnitEffect = pkTarget->GetEffect(iEffectNo);
				//디버프가 걸려있지 않은 경우
				if(!pkUnitEffect)
				{
					EffectQueueData kData(EQT_ADD_EFFECT, iEffectNo, 0, pkArg, pkUnit->GetID(), EFFECT_TYPE_GROUND);
					pkTarget->AddEffectQueue(kData);
				}
				else
				{
					//디버프가 걸려있는 상태에서 해제 버프를 발동 시킨 경우
					if(pkUnitEffect->GetEffectNo() == pkTarget->GetAbil(AT_EXCEPT_CHAOS_EFFECT_TICK))
					{
						//능력치 저하가 일어난 경우 풀어줘야한다.
						if(1 == pkUnitEffect->GetValue())
						{
							//pkTarget->DeleteEffect(iEffectNo);
							EffectQueueData kData(EQT_DELETE_EFFECT, iEffectNo);
							pkTarget->AddEffectQueue(kData);
							EffectQueueData kData2(EQT_ADD_EFFECT, iEffectNo, 0, pkArg, pkUnit->GetID(), EFFECT_TYPE_GROUND);
							pkTarget->AddEffectQueue(kData2);
						}
					}
				}

/* Pet은 안걸리게 막는다. 걸려야 한다면 여기를 풀것
				if(pkTarget->IsUnitType(UT_PLAYER))
				{
					//장착 펫이 있으면 
					PgPet* pkPet = pkGround->GetPet(dynamic_cast<PgPlayer*>(pkTarget));
					if(pkPet)
					{
						CEffect* pkUnitEffect = pkPet->GetEffect(iEffectNo);
						//디버프가 걸려있지 않은 경우
						if(!pkUnitEffect)
						{
							EffectQueueData kData(EQT_ADD_EFFECT, iEffectNo, 0, pkArg, pkUnit->GetID(), EFFECT_TYPE_GROUND);
							pkPet->AddEffectQueue(kData);
						}
						else
						{
							//디버프가 걸려있는 상태에서 해제 버프를 발동 시킨 경우
							if(pkUnitEffect->GetEffectNo() == pkPet->GetAbil(AT_EXCEPT_CHAOS_EFFECT_TICK))
							{
								//능력치 저하가 일어난 경우 풀어줘야한다.
								if(1 == pkUnitEffect->GetValue())
								{
									EffectQueueData kData(EQT_DELETE_EFFECT, iEffectNo);
									pkPet->AddEffectQueue(kData);
									EffectQueueData kData2(EQT_ADD_EFFECT, iEffectNo, 0, pkArg, pkUnit->GetID(), EFFECT_TYPE_GROUND);
									pkPet->AddEffectQueue(kData2);
								}
							}
						}
					}
				}
*/
			}
		}

		++target_itor;
	}

	//이펙트에 걸려있는 타겟들
	VEC_GUID& rkTargtList = pkEffect->GetTargetList();

	if(!rkTargtList.empty())
	{
		VEC_GUID::iterator target_guid_itor = rkTargtList.begin();
		while(target_guid_itor != rkTargtList.end())
		{
			bool bFind = false;
			//새로 잡은 타겟 리스트에 이펙트에 걸려있는 유저가 있는지 검색
			for(UNIT_PTR_ARRAY::const_iterator unit_itor = kUnitArray.begin(); unit_itor != kUnitArray.end(); ++unit_itor)
			{
				if((*unit_itor).pkUnit)
				{
					if((*unit_itor).pkUnit->GetID() == (*target_guid_itor))
					{
						//이미 카오스 디버프가 걸렸는 유닛일 경우에
						CUnit* pkTarget = (*unit_itor).pkUnit;
						if(pkTarget)
						{
							//카오스 맵에서 거는 이펙트 번호를 막는 어빌이 사라질 경우							
							if(iEffectNo != pkTarget->GetAbil(AT_EXCEPT_CHAOS_EFFECT_TICK))
							{
								if(CEffect* pkUnitEffect = pkTarget->GetEffect(iEffectNo))
								{
									if(0 == pkUnitEffect->GetValue()) // 막아주는 어빌이 사라지고 이펙트의 값이 0 인경우는 버프 효과를 받다가 방지효과가 사라진경우
									{
										EffectQueueData kData(EQT_DELETE_EFFECT, iEffectNo);
										pkTarget->AddEffectQueue(kData);
										EffectQueueData kData2(EQT_ADD_EFFECT, iEffectNo, 0, pkArg, pkUnit->GetID(), EFFECT_TYPE_GROUND);
										pkTarget->AddEffectQueue(kData2);
									}
								}
							}

/* Pet은 안걸리게 막는다. 걸려야 한다면 여기를 풀것
							if(pkTarget->IsUnitType(UT_PLAYER))
							{
								//장착 펫이 있으면 
								PgPet* pkPet = pkGround->GetPet(dynamic_cast<PgPlayer*>(pkTarget));
								if(pkPet)
								{
									//카오스 맵에서 거는 이펙트 번호를 막는 어빌이 사라질 경우							
									if(iEffectNo != pkPet->GetAbil(AT_EXCEPT_CHAOS_EFFECT_TICK))
									{
										if(CEffect* pkUnitEffect = pkPet->GetEffect(iEffectNo))
										{
											if(0 == pkUnitEffect->GetValue()) // 막아주는 어빌이 사라지고 이펙트의 값이 0 인경우는 버프 효과를 받다가 방지효과가 사라진경우
											{
												EffectQueueData kData(EQT_DELETE_EFFECT, iEffectNo);
												pkPet->AddEffectQueue(kData);
												EffectQueueData kData2(EQT_ADD_EFFECT, iEffectNo, 0, pkArg, pkUnit->GetID(), EFFECT_TYPE_GROUND);
												pkPet->AddEffectQueue(kData2);
											}
										}
									}
								}
							}
*/
						}
						bFind = true;
						break;
					}
				}
			}

			//범위를 벗어났으면 제거
			if(!bFind)
			{
				CUnit* pkTarget = pkGround->GetUnit(*target_guid_itor);
				if(pkTarget)
				{
					//pkTarget->DeleteEffect(iEffectNo);
					EffectQueueData kData(EQT_DELETE_EFFECT, iEffectNo);
					pkTarget->AddEffectQueue(kData);
										
				}
				else
				{
					INFO_LOG(BM::LOG_LV6, L"Effect["<<pkEffect->GetEffectNo()<<L"] Cannot Find Unit["<<(*target_guid_itor)<<L"] ");
					LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkTarget is NULL"));
				}
				target_guid_itor = rkTargtList.erase(target_guid_itor);
			}
			else
			{
				++target_guid_itor;
			}
		}
	}

	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgChaosMapEffect - 카오스맵 능력치 저하 디버프
///////////////////////////////////////////////////////////
void PgChaosMapEffect::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const *pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());
	// 카오스 버프를 막아주는 버프가 없을 경우 효과를 걸어준다.
	if(pkEffect->GetEffectNo() != pkUnit->GetAbil(AT_EXCEPT_CHAOS_EFFECT_TICK))
	{
		//능력치 저하가 일어났다는 표시
		pkEffect->SetValue(1);
		PgDefaultEffectFunction::EffectBegin(pkUnit, pkEffect, pkArg);
	}
}

void PgChaosMapEffect::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const *pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	//능력치 저하가 일어 났으므로 능력치 저하 해제
	if(1 == pkEffect->GetValue())
	{
		PgDefaultEffectFunction::EffectEnd(pkUnit, pkEffect, pkArg);
	}
}

int PgChaosMapEffect::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const *pkArg, DWORD const dwElapsed)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());	

	return ECT_DOTICK;
}