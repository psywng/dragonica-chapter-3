#include "stdafx.h"
#include "PgEffectAbilTable.h"
#include "PgEffectFunction_Thief.h"
#include "Variant/Global.h"
#include "Global.h"
#include "Variant/PgPartyMgr.h"
#include "PgLocalPartyMgr.h"
#include "PgGround.h"
#include "Variant/PgParty.h"
#include "Variant/PgPartyMgr.h"
#include "PgPartyItemRule.h"
#include "PgLocalPartyMgr.h"
#include "Variant/PgActionResult.h"

///////////////////////////////////////////////////////////
//  PgStripWeaponEffectFunction - 스트립 웨폰
///////////////////////////////////////////////////////////
void PgStripWeaponEffectFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());
	OnAddAbil(pkUnit, AT_PHY_ATTACK_MAX, pkEffect->GetAbil(AT_R_PHY_ATTACK));
	OnAddAbil(pkUnit, AT_PHY_ATTACK_MIN, pkEffect->GetAbil(AT_R_PHY_ATTACK));
}

void PgStripWeaponEffectFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());
	OnAddAbil(pkUnit, AT_PHY_ATTACK_MAX, -pkEffect->GetAbil(AT_R_PHY_ATTACK));
	OnAddAbil(pkUnit, AT_PHY_ATTACK_MIN, -pkEffect->GetAbil(AT_R_PHY_ATTACK));
}

int PgStripWeaponEffectFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	INFO_LOG(BM::LOG_LV6, __FL__<<L"Don't call me Tick EffectNo["<<pkEffect->GetEffectNo()<<L"] ");
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgBurrowEffectFunction - 버로우
///////////////////////////////////////////////////////////
void PgBurrowEffectFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_UNIT_HIDDEN,		1);
	OnAddAbil(pkUnit, AT_LOCK_HIDDEN_ATTACK,1);	
	int iAbil = pkUnit->GetAbil(AT_ENABLE_AUTOHEAL);
	//기존의 오토힐 상태를 저장해둔다.
	OnSetAbil(pkUnit, AT_ENABLE_AUTOHEAL_BACKUP, iAbil);
	//Hp 자동 회복이 되는 상태이면 제거
	if(iAbil & AUTOHEAL_HP)
	{
		OnSetAbil(pkUnit, AT_ENABLE_AUTOHEAL, iAbil-AUTOHEAL_HP);
		iAbil -= AUTOHEAL_HP;
	}

	//Mp 자동 회복이 되는 상태이면 제거
	if(iAbil & AUTOHEAL_MP)
	{
		OnSetAbil(pkUnit, AT_ENABLE_AUTOHEAL, iAbil-AUTOHEAL_MP);
	}

	OnAddAbil(pkUnit, AT_R_MOVESPEED,		-ABILITY_RATE_VALUE);
	OnAddAbil(pkUnit, AT_CANNOT_USEITEM,	1);
}

void PgBurrowEffectFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_UNIT_HIDDEN,		-1);
	OnAddAbil(pkUnit, AT_LOCK_HIDDEN_ATTACK,-1);	
	OnAddAbil(pkUnit, AT_R_MOVESPEED,		ABILITY_RATE_VALUE);
	OnAddAbil(pkUnit, AT_CANNOT_USEITEM,	-1);
	
	OnSetAbil(pkUnit, AT_ENABLE_AUTOHEAL, pkUnit->GetAbil(AT_ENABLE_AUTOHEAL_BACKUP));
	OnSetAbil(pkUnit, AT_ENABLE_AUTOHEAL_BACKUP, 0);
}

int PgBurrowEffectFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	PgGround* pkGround = NULL;
	pkArg->Get(ACTARG_GROUND, pkGround);

	if(!pkGround)
	{
		//INFO_LOG(BM::LOG_LV6, _T("[%s][%d] Cannot find Ground"), __FUNCTIONW__, pkEffect->GetEffectNo());
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkGround is NULL"));
		return ECT_MUSTDELETE;
	}

	int const iNeedMp = pkEffect->GetAbil(AT_MP);
	int const iNowMp = pkUnit->GetAbil(AT_MP);

	if(pkUnit->GetAbil(AT_UNIT_HIDDEN) == 0)
	{
		return ECT_MUSTDELETE;
	}

	if(-iNeedMp > iNowMp)
	{
		//INFO_LOG(BM::LOG_LV9, _T("[%s][%d] NeedMP : %d / MP : %d"), __FUNCTIONW__, pkEffect->GetEffectNo(), iNeedMp, iNowMp);
		return ECT_MUSTDELETE;
	}

	OnSetAbil(pkUnit, AT_MP, iNowMp + iNeedMp);

	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgExitEffectFunction - 비상탈출
///////////////////////////////////////////////////////////
void PgExitEffectFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_HP_RECOVERY, pkEffect->GetAbil(AT_HP_RECOVERY));
	OnAddAbil(pkUnit, AT_CANNOT_DAMAGE, 1);
	OnAddAbil(pkUnit, AT_CANNOT_ATTACK, 1);
}

void PgExitEffectFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_HP_RECOVERY, -pkEffect->GetAbil(AT_HP_RECOVERY));
	OnAddAbil(pkUnit, AT_CANNOT_DAMAGE, -1);
	OnAddAbil(pkUnit, AT_CANNOT_ATTACK, -1);
}

int PgExitEffectFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	INFO_LOG(BM::LOG_LV0, __FL__<<L"Don't call me Tick EffectNo["<<pkEffect->GetEffectNo()<<L"] ");
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgShadowWalkEffectFunction - 쉐도우 워크
///////////////////////////////////////////////////////////
void PgShadowWalkEffectFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_MOVESPEED, pkEffect->GetAbil(AT_R_MOVESPEED));
	OnAddAbil(pkUnit, AT_UNIT_HIDDEN, 1);
	OnSetAbil(pkUnit, AT_ENABLE_CHECK_ATTACK, pkEffect->GetAbil(AT_ENABLE_CHECK_ATTACK));
}

void PgShadowWalkEffectFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_MOVESPEED, -pkEffect->GetAbil(AT_R_MOVESPEED));
	OnAddAbil(pkUnit, AT_UNIT_HIDDEN, -1);
	OnAddAbil_Attack(pkUnit, AT_ENABLE_CHECK_ATTACK, -pkEffect->GetAbil(AT_ENABLE_CHECK_ATTACK));
}

int PgShadowWalkEffectFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	int const iNeedMp = pkEffect->GetAbil(AT_MP);
	int const iNowMp = pkUnit->GetAbil(AT_MP);

	if( 0 == pkUnit->GetAbil(AT_UNIT_HIDDEN) )
	{
		return ECT_MUSTDELETE;
	}

	if( 0 < pkUnit->GetAbil(AT_ENABLE_CHECK_ATTACK) )
	{
		if(0 < pkUnit->GetAbil(AT_CHECK_ATTACK_COUNT))
		{
			return ECT_MUSTDELETE;
		}
	}

	if(-iNeedMp > iNowMp)
	{
		//INFO_LOG(BM::LOG_LV9, _T("[%s][%d] NeedMP : %d / MP : %d"), __FUNCTIONW__, pkEffect->GetEffectNo(), iNeedMp, iNowMp);
		return ECT_MUSTDELETE;
	}

	OnSetAbil(pkUnit, AT_MP, iNowMp + iNeedMp);
	
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgChangeNinjaEffectFunction - 닌자변신
///////////////////////////////////////////////////////////
int PgChangeNinjaEffectFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	int const iNeedMp = pkEffect->GetAbil(AT_MP);
	int const iNowMp = pkUnit->GetAbil(AT_MP);

	if(-iNeedMp > iNowMp)
	{
		//INFO_LOG(BM::LOG_LV9, _T("[%s][%d] NeedMP : %d / MP : %d"), __FUNCTIONW__, pkEffect->GetEffectNo(), iNeedMp, iNowMp);
		return ECT_MUSTDELETE;
	}

	OnSetAbil(pkUnit, AT_MP, iNowMp + iNeedMp);

	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgProtectEdgeEffectFunction - 프로텍트엣지
///////////////////////////////////////////////////////////
int PgProtectEdgeEffectFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	void* pkVoid = NULL;
	pkArg->Get(ACTARG_GROUND, pkVoid);
	PgGround* pkGround = (PgGround*)pkVoid;
	if( !pkGround || !pkUnit || !pkEffect )
	{
		return ECT_DOTICK;
	}
	int iRange = pkEffect->GetAbil(AT_ATTACK_RANGE);
	if( !iRange )
	{
		iRange = 70;
	}
	UNIT_PTR_ARRAY kTargetArray;
	pkGround->GetUnitTargetList(pkUnit, kTargetArray, ESTARGET_ENEMY, iRange, static_cast<int>(AI_Z_LIMIT));

	if( !kTargetArray.empty() )
	{
		int const iMaxTarget = pkEffect->GetAbil(AT_MAX_TARGETNUM);
		int iPercent = pkEffect->GetAbil(AT_PHY_DMG_PER);
		// + 인 경우는 스킬의 최종 데미지를 감소 시키는 경우
		if( 0 == iPercent )
		{
			iPercent = static_cast<int>(ABILITY_RATE_VALUE_FLOAT);
		}

		if(0 < pkUnit->GetAbil(AT_PHY_DMG_PER))
		{
			iPercent = static_cast<int>(iPercent * static_cast<float>(pkUnit->GetAbil(AT_PHY_DMG_PER)) / ABILITY_RATE_VALUE_FLOAT);
		}
		// - 인 경우는 스킬의 최종 데미지를 1로 만들어 버리는 경우
		else if(0 > pkUnit->GetAbil(AT_PHY_DMG_PER))
		{
			iPercent += pkUnit->GetAbil(AT_PHY_DMG_PER);
		}
		iPercent += ( iPercent * pkUnit->GetAbil(AT_PHY_DMG_PER2) ) / ABILITY_RATE_VALUE;

		int iCount = 0;

		int const iHitRate = pkUnit->GetAbil(AT_C_HITRATE);

		GET_DEF( CSkillDefMgr, kSkillDefMgr);
		CSkillDef const* pkSkillDef = kSkillDefMgr.GetDef(pkEffect->GetEffectNo());

		UNIT_PTR_ARRAY::iterator unit_itor = kTargetArray.begin();
		while(kTargetArray.end() != unit_itor)
		{
			CUnit* pkTarget = (*unit_itor).pkUnit;
			if(pkUnit->IsTarget(pkTarget, true, ESTARGET_ENEMY, pkSkillDef))
			{
				int iRandValue = BM::Rand_Index(ABILITY_RATE_VALUE) % ABILITY_RATE_VALUE;
				int iMin = pkUnit->GetAbil(AT_C_PHY_ATTACK_MIN);
				int iMax = pkUnit->GetAbil(AT_C_PHY_ATTACK_MAX);
				int iDamage = iMin + (iRandValue % __max(1, iMax-iMin));

				if( iDamage > 0 )
				{
					// 레벨차이에 의한 확률 적용
					int iRandValue = BM::Rand_Index(ABILITY_RATE_VALUE) % ABILITY_RATE_VALUE;
					int iDecHitRate = CalcDecHitRate(pkUnit, pkTarget, iHitRate);
					if (iDecHitRate > iRandValue)
					{
						pkUnit->UseRandomSeedType(false);
						iDamage = CS_CheckDamage(pkUnit, pkTarget, iDamage, true, NULL, iPercent, false);
						pkUnit->UseRandomSeedType(true);
						
						int iDmgEffect = 0;
						if( IsDamageAction(pkTarget, pkSkillDef) ) // 이펙트에서 주는 것은 새로 데미지 액션을 검사해야 함
						{// 데미지 액션을 해야 하더라도,
							if(pkTarget->UnitType() != UT_PLAYER)
							{// Player가 아닌 녀석들만 데미지 액션을 하게 설정한다
								iDmgEffect = pkEffect->GetAbil(AT_EFFECTNUM1);
								SetDamageDelay(pkSkillDef, pkUnit, pkTarget);
							}
						}
						DoTickDamage(pkUnit, pkTarget, iDamage, pkEffect->GetEffectNo(), iDmgEffect, pkArg);
					}
				}
			}

			++unit_itor;
			++iCount;
			if(iMaxTarget <= iCount)
			{
				break;
			}		
		}
	}

	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgHPMPRestoreEffectFunction - 
///////////////////////////////////////////////////////////
void PgHPMPRestoreEffectFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	if(!pkUnit 
		|| !pkEffect
		)
	{
		return;
	}
	CEffectDef const* pkDef = pkEffect->GetEffectDef();
	if(pkDef
		&& 0 == pkDef->GetInterval()
		)
	{
		int const iAddMPRate = pkEffect->GetAbil(AT_ADD_MP_FROM_MAX_MP_RATE);
		if(0 < iAddMPRate)
		{
			int const iMaxMP = pkUnit->GetAbil(AT_C_MAX_MP);
			int const iCurMP = pkUnit->GetAbil(AT_MP);
			if(iMaxMP > iCurMP)
			{
				SkillFuncUtil::OnAddMPFromMaxMP(pkUnit, pkEffect, iAddMPRate);
			}
		}

		int const iAddHPRate = pkEffect->GetAbil(AT_ADD_HP_FROM_MAX_HP_RATE);
		if(0 < iAddHPRate)
		{
			int const iMaxHP = pkUnit->GetAbil(AT_C_MAX_HP);
			int const iCurHP = pkUnit->GetAbil(AT_HP);
			if(iMaxHP > iCurHP)
			{
				SkillFuncUtil::OnAddHPFromMaxHP(pkUnit, pkEffect,iAddHPRate);
			}
		}
	}
	int const iSpecipicIdle = pkEffect->GetAbil(AT_SKILL_SPECIFIC_IDLE);
	if(0 < iSpecipicIdle)
	{
		OnAddAbil(pkUnit, AT_SKILL_SPECIFIC_IDLE, iSpecipicIdle);
	}
}

int PgHPMPRestoreEffectFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	PgGround* pkGround = NULL;
	pkArg->Get(ACTARG_GROUND, pkGround);

	if(!pkGround)
	{
		INFO_LOG(BM::LOG_LV6, __FL__<<L"Cannot find Ground, EffectNo="<<pkEffect->GetEffectNo());
		return ECT_DOTICK;
	}

	if(pkUnit->IsDead())
	{
		return ECT_MUSTDELETE;
	}

	int const iAddMPRate = pkEffect->GetAbil(AT_ADD_MP_FROM_MAX_MP_RATE);
	if(0 < iAddMPRate)
	{
		int const iMaxMP = pkUnit->GetAbil(AT_C_MAX_MP);
		int const iCurMP = pkUnit->GetAbil(AT_MP);
		if(iMaxMP > iCurMP)
		{
			SkillFuncUtil::OnAddMPFromMaxMP(pkUnit, pkEffect, iAddMPRate);
		}
	}

	int const iAddHPRate = pkEffect->GetAbil(AT_ADD_HP_FROM_MAX_HP_RATE);
	if(0 < iAddHPRate)
	{
		int const iMaxHP = pkUnit->GetAbil(AT_C_MAX_HP);
		int const iCurHP = pkUnit->GetAbil(AT_HP);
		if(iMaxHP > iCurHP)
		{
			SkillFuncUtil::OnAddHPFromMaxHP(pkUnit, pkEffect,iAddHPRate);
		}
	}

	return ECT_DOTICK;
}

void PgHPMPRestoreEffectFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	if(!pkUnit 
		|| !pkEffect
		)
	{
		return;
	}

	int const iSpecipicIdle = pkEffect->GetAbil(AT_SKILL_SPECIFIC_IDLE);
	if(0 < iSpecipicIdle)
	{
		OnAddAbil(pkUnit, AT_SKILL_SPECIFIC_IDLE, -iSpecipicIdle);
	}
}