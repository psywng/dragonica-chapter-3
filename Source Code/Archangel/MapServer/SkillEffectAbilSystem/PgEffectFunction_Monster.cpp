#include "stdafx.h"
#include "PgEffectAbilTable.h"
#include "PgEffectFunction.h"
#include "Variant/Global.h"
#include "Global.h"
#include "Variant/PgPartyMgr.h"
#include "PgLocalPartyMgr.h"
#include "PgGround.h"
#include "PgEffectFunction_Monster.h"

///////////////////////////////////////////////////////////
//  PgVenomFunction
///////////////////////////////////////////////////////////
int PgVenomFunction::EffectTick(CUnit* pkUnit, CEffect *pkEffect, SActArg const *pkArg, DWORD const dwElapsed)
{
	int const iHP = pkUnit->GetAbil(AT_HP);
	int const iDec = GetTick_AT_HP_Value(pkEffect, pkEffect->GetActArg(), pkUnit);
	int const iNew = iHP + iDec;
	int const iDelta = iNew - iHP;

	BM::CPacket kPacket(PT_M_C_NFY_ABILCHANGED);
	kPacket.Push(pkUnit->GetID());
	kPacket.Push((short)AT_HP);
	kPacket.Push(iNew);
	kPacket.Push(pkEffect->GetCaster());
	kPacket.Push(pkEffect->GetEffectNo());
	kPacket.Push(iDelta);
	pkUnit->Send(kPacket, E_SENDTYPE_BROADALL);
	if (0 >= iNew)
	{
		pkUnit->SetTarget(pkEffect->GetCaster());
		pkUnit->SetAbil(AT_HP, 0, false, false);
		return ECT_MUSTDELETE;
	}

	pkUnit->SetAbil(AT_HP, iNew, false, false);
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgIncreaseSightFunction
///////////////////////////////////////////////////////////
void PgIncreaseSightFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	PgUnitEffectMgr& rkMgr = pkUnit->GetEffectMgr();
	//시야 증가
	int iAdd = pkEffect->GetAbil(AT_DETECT_RANGE);
	rkMgr.AddAbil(AT_DETECT_RANGE, iAdd);
	pkUnit->NftChangedAbil(AT_DETECT_RANGE, E_SENDTYPE_NONE);

	iAdd = pkEffect->GetAbil(AT_CHASE_RANGE);
	rkMgr.AddAbil(AT_CHASE_RANGE, iAdd);
	pkUnit->NftChangedAbil(AT_CHASE_RANGE, E_SENDTYPE_NONE);

}

void PgIncreaseSightFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	PgUnitEffectMgr& rkMgr = pkUnit->GetEffectMgr();
	//시야 감소
	int iAdd = pkEffect->GetAbil(AT_DETECT_RANGE);
	rkMgr.AddAbil(AT_DETECT_RANGE, -iAdd);
	pkUnit->NftChangedAbil(AT_DETECT_RANGE, E_SENDTYPE_NONE);

	iAdd = pkEffect->GetAbil(AT_CHASE_RANGE);
	rkMgr.AddAbil(AT_CHASE_RANGE, -iAdd);
	pkUnit->NftChangedAbil(AT_CHASE_RANGE, E_SENDTYPE_NONE);
}
int PgIncreaseSightFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgBlockingFunction
///////////////////////////////////////////////////////////
void PgBlockingFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	PgUnitEffectMgr& rkMgr = pkUnit->GetEffectMgr();

	int iAdd = pkEffect->GetAbil(AT_DODGE_RATE);
	rkMgr.AddAbil(AT_DODGE_RATE, iAdd);
	pkUnit->NftChangedAbil(AT_DODGE_RATE, E_SENDTYPE_BROADALL|E_SENDTYPE_EFFECTABIL);
}

void PgBlockingFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	PgUnitEffectMgr& rkMgr = pkUnit->GetEffectMgr();

	int iAdd = pkEffect->GetAbil(AT_DODGE_RATE);
	rkMgr.AddAbil(AT_DODGE_RATE, -iAdd);
	pkUnit->NftChangedAbil(AT_DODGE_RATE, E_SENDTYPE_BROADALL|E_SENDTYPE_EFFECTABIL);
}
int PgBlockingFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgNetBinderFunction
///////////////////////////////////////////////////////////
void PgNetBinderFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	PgUnitEffectMgr& rkMgr = pkUnit->GetEffectMgr();

	int iAdd = pkEffect->GetAbil(AT_R_MOVESPEED);
	rkMgr.AddAbil(AT_R_MOVESPEED, iAdd);
	pkUnit->NftChangedAbil(AT_R_MOVESPEED, E_SENDTYPE_BROADALL|E_SENDTYPE_EFFECTABIL);
}

void PgNetBinderFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	PgUnitEffectMgr& rkMgr = pkUnit->GetEffectMgr();

	int iAdd = pkEffect->GetAbil(AT_R_MOVESPEED);
	rkMgr.AddAbil(AT_R_MOVESPEED, -iAdd);
	pkUnit->NftChangedAbil(AT_R_MOVESPEED, E_SENDTYPE_BROADALL|E_SENDTYPE_EFFECTABIL);
}
int PgNetBinderFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgLavalonBreathFunction
///////////////////////////////////////////////////////////
int PgLavalonBreathFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	void* pkVoid = NULL;
	pkArg->Get(ACTARG_GROUND, pkVoid);
	PgGround* pkGround = (PgGround*)pkVoid;
	if (!pkGround)
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkGround is NULL"));
		return -1;
	}

	int const iDetectRange = 120;
	float fMetaForStart = 0.5f;
	float fMetaForEnd = 0.5f;
	int const iEffectNo = pkEffect->GetEffectNo();

	if ( 6000902 == iEffectNo )		//왼쪽
	{
		fMetaForStart = 0.45f;
		fMetaForEnd = 0.17f;
	}
	else if ( 6000901 == iEffectNo )	//오른쪽
	{
		fMetaForStart = 0.55f;
		fMetaForEnd = 0.83f;
	}

	POINT3 kTopLeft = pkGround->GetNodePosition("pt_blaze_01");			// Top Left
	POINT3 kTopRight = pkGround->GetNodePosition("pt_blaze_02");		// Top Right
	POINT3 kBottomLeft = pkGround->GetNodePosition("pt_blaze_03");		// Bottom Left
	POINT3 kBottomRight = pkGround->GetNodePosition("pt_blaze_04");		// Bottm Right

	POINT3 kUpperBase = kTopRight-kTopLeft;
	POINT3 kRightBase = kTopRight-kBottomRight;
	POINT3 kBottomBase = kBottomRight-kBottomLeft;
	POINT3 kLeftBase = kTopLeft-kBottomLeft;

	POINT3 kStartPos = kUpperBase*fMetaForStart;
	kStartPos+=(kTopLeft);
	kStartPos.z = 0;

	POINT3 kEndPos = kBottomBase*fMetaForEnd;
	kEndPos+=(kBottomLeft);
	kEndPos.z = 0;

	UNIT_PTR_ARRAY kTargetArray;

	pkGround->GetUnitInWidthFromLine(kStartPos, kEndPos, iDetectRange, UT_PLAYER, kTargetArray);
	PgActionResultVector kActionResult;

	bool bRet = CS_GetSkillResultDefault(iEffectNo, pkUnit, kTargetArray, &kActionResult);	//iEffectNo 위험하다 스킬넘버랑 이펙트 넘버랑 다르면 큰일
	
	UNIT_PTR_ARRAY::iterator unit_itor = kTargetArray.begin();

	while(kTargetArray.end() != unit_itor)
	{
		CUnit* pkTarget = (*unit_itor).pkUnit;
		if (pkTarget)
		{
			PgActionResult* pkResult = kActionResult.GetResult(pkTarget->GetID());
			if (pkResult && !pkResult->IsEmpty())
			{
				::DoFinalDamage(pkUnit, pkTarget, pkResult->GetValue(), iEffectNo, pkArg, ::GetTimeStampFromActArg(*pkArg, __FUNCTIONW__));
			}
		}
		++unit_itor;
	}

	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgAutoTickEffectFunction
///////////////////////////////////////////////////////////
void PgAutoTickEffectFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
}

void PgAutoTickEffectFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
}
int PgAutoTickEffectFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	void* pkVoid = NULL;
	pkArg->Get(ACTARG_GROUND, pkVoid);
	PgGround* pkGround = (PgGround*)pkVoid;
	if (!pkGround)
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkGround is NULL"));
		return ECT_MUSTDELETE;
	}
	GET_DEF(CSkillDefMgr, kSkillDefMgr);
	int const iMaxTarget = pkEffect->GetAbil(AT_MAX_TARGETNUM);

	for( int i = 0; i < 10; ++i)
	{
		int iAddSkillNo = pkEffect->GetAbil(AT_MON_SKILL_01+i);
		if (0<iAddSkillNo)
		{
			POINT3 kPos = pkUnit->GetPos();
			UNIT_PTR_ARRAY kTargetArray;
			int const iRange = pkEffect->GetAbil(AT_DETECT_RANGE);
			//EUnitType eType = pkUnit->UnitType();
			pkGround->GetUnitInRange(kPos, iRange, UT_PLAYER, kTargetArray, static_cast<int>(AI_Z_LIMIT));
			UNIT_PTR_ARRAY::iterator unit_itor = kTargetArray.begin();
			int iIndex = 0;
			while(kTargetArray.end() != unit_itor && iIndex < iMaxTarget)
			{
				CSkillDef const* pkSkill = kSkillDefMgr.GetDef(iAddSkillNo);	
				if (pkSkill && pkUnit->IsTarget((*unit_itor).pkUnit, false, ESTARGET_ENEMY))
				{
					int iPhyPercent = 0;
					int iDamage = 0;
					if (pkSkill->IsSkillAtt(SAT_PHYSICS))
					{
						iPhyPercent = pkSkill->GetAbil(AT_PHY_DMG_PER);
						iDamage += pkUnit->GetAbil(AT_C_PHY_ATTACK)*iPhyPercent/ABILITY_RATE_VALUE;
					}

					int iMagicPercent = 0;
					if (pkSkill->IsSkillAtt(SAT_MAGIC))
					{
						iMagicPercent = pkSkill->GetAbil(AT_MAGIC_DMG_PER);
						iDamage += pkUnit->GetAbil(AT_C_MAGIC_ATTACK)*iMagicPercent/ABILITY_RATE_VALUE;
					}

					//if (iDamage)
					{
						::DoTickDamage(pkUnit, (*unit_itor).pkUnit, iDamage, pkEffect->GetEffectNo(), pkSkill->GetEffectNo(), pkArg);
					}
				}
				++iIndex;
				++unit_itor;
			}
		}

	}
	return ECT_DOTICK;
}


///////////////////////////////////////////////////////////
//  PgDependAbilTickEffectFunction
///////////////////////////////////////////////////////////
PgDependAbilTickEffectFunction::PgDependAbilTickEffectFunction()
	: PgDefaultEffectFunction(), m_sDependAbilType(AT_ADDED_GAUGE_VALUE), m_iTargetValue(0)
{
}

void PgDependAbilTickEffectFunction::Build(PgAbilTypeTable const* pkAbilTable, CEffectDef const* pkDef)
{
	PgDefaultEffectFunction::Build(pkAbilTable, pkDef);


	// 빌드 이후 몇개는 지운다
	CONT_ABIL_TYPE_VAULE_TABLE::iterator loop_iter = m_kTable.begin();
	while( m_kTable.end() != loop_iter )
	{
		CONT_ABIL_TYPE_VAULE& rkList = (*(*loop_iter).second);
		CONT_ABIL_TYPE_VAULE::iterator sub_iter = rkList.begin();
		while( rkList.end() != sub_iter )
		{
			WORD const wType = (*sub_iter).m_wType;
			if( AT_MON_REMOVE_EFFECT_FORCESKILL == wType
			||	m_sDependAbilType == wType )
			{
				sub_iter = rkList.erase(sub_iter);
			}
			else
			{
				++sub_iter;
			}
		}
		++loop_iter;
	}
	//m_kTable.erase(AT_MON_REMOVE_EFFECT_FORCESKILL); // 해당 어빌은 Plus/Minus 하지 않음
	//m_kTable.erase(m_sDependAbilType); // 이펙트 삭제시 의존되는 어빌 번호는 빼주지 않는다. (이미 0값)
}

void PgDependAbilTickEffectFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	PgDefaultEffectFunction::EffectBegin(pkUnit, pkEffect, pkArg);

	pkUnit->SetAbil(m_sDependAbilType, pkEffect->GetAbil(m_sDependAbilType));
}

void PgDependAbilTickEffectFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	PgDefaultEffectFunction::EffectEnd(pkUnit, pkEffect, pkArg);
}

int PgDependAbilTickEffectFunction::EffectTick(CUnit* pkUnit, CEffect *pkEffect, SActArg const *pkArg, DWORD const dwElapsed)
{
	void* pkVoid = NULL;
	pkArg->Get(ACTARG_GROUND, pkVoid);
	PgGround* pkGround = static_cast< PgGround* >(pkVoid);
	if (!pkGround)
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("pkGround is NULL"));
		return ECT_MUSTDELETE;
	}

	if( m_iTargetValue >= pkUnit->GetAbil(m_sDependAbilType) ) // 의존된 어빌이 값이 다 떨어지면 자신을 지우자
	{
		return ECT_MUSTDELETE;
	}
	return ECT_DOTICK;
}

void PgMetaMorphosisEffectFunction::EffectBegin(CUnit *pkUnit, CEffect *pkEffect, const SActArg *pkArg)
{
	int const iClass = pkEffect->GetAbil(AT_MON_TRANSFORM_CLASS);
	if(0<iClass)
	{
		int const iCount = pkUnit->GetAbil(AT_MON_TRANSFORM_COUNT);
		for(int i = 0; i<iCount; ++i)
		{
			if(iClass == pkUnit->GetAbil(AT_MON_TRANSFORM_CLASS_01 + i) )	//기존에 있는 이펙트일 경우
			{
				INFO_LOG(BM::LOG_LV3, __FL__<<"Aleady MetaMorphosed Class! ClassNo : "<<iClass<<" EffectID : "<<pkEffect->GetEffectNo());
				return;	//절대 이런일 생기면 안됨
			}
		}
		pkUnit->SetAbil(AT_MON_TRANSFORM_CLASS_01 + iCount, iClass );//변신클래스 저장
		pkUnit->AddAbil(AT_MON_TRANSFORM_COUNT, 1);	//변신 횟수 증가
	}

	PgDefaultEffectFunction::EffectBegin(pkUnit, pkEffect, pkArg);
}

void PgMetaMorphosisEffectFunction::EffectEnd(CUnit *pkUnit, CEffect *pkEffect, const SActArg *pkArg)
{
	int const iClass = pkEffect->GetAbil(AT_MON_TRANSFORM_CLASS);
	if(0<iClass)
	{
		bool bFound = false;
		int const iCount = pkUnit->GetAbil(AT_MON_TRANSFORM_COUNT);
		for(int i = 0; i<iCount; ++i)	//
		{
			if(iClass == pkUnit->GetAbil(AT_MON_TRANSFORM_CLASS_01 + i) )	//몇번째 변신이 해제되었는가
			{
				pkUnit->SetAbil(AT_MON_TRANSFORM_CLASS_01 + i, 0);
				bFound = true;
			}
			else if(bFound)
			{
				pkUnit->SetAbil(AT_MON_TRANSFORM_CLASS_01 + i - 1, pkUnit->GetAbil(AT_MON_TRANSFORM_CLASS_01 + i));	//하나씩 앞으로 당겨 주자. 변신이 10번 이상이면 어떻하나?
				pkUnit->SetAbil(AT_MON_TRANSFORM_CLASS_01 + i, 0);
			}
		}

		if(bFound)	//변신이 진짜로 풀렸다
		{
			pkUnit->AddAbil(AT_MON_TRANSFORM_COUNT, -1);//변신 횟수 감소
		}
	}

	int const iForceFireSkillNo = pkEffect->GetAbil(AT_MON_REMOVE_EFFECT_FORCESKILL);
	if( 0 < iForceFireSkillNo
		&&	pkUnit->IsInUnitType(UT_MONSTER) )
	{
		pkUnit->GetSkill()->ForceReserve(iForceFireSkillNo);
		pkUnit->GetAI()->SetEvent(pkUnit->GetID(), EAI_EVENT_FORCE_SKILL);
	}

	PgDefaultEffectFunction::EffectEnd(pkUnit, pkEffect, pkArg);
}

///////////////////////////////////////////////////////////
//  PgHardDungeonEffectFunction
///////////////////////////////////////////////////////////
void PgHardDungeonEffectFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	PgDefaultEffectFunction::EffectBegin(pkUnit, pkEffect, pkArg);

	//기본적은 능력치를 다 올려 준 후에 HP/MP를 Max치에 맞추어 준다.
	OnAddAbil(pkUnit, AT_HP, pkUnit->GetAbil(AT_C_MAX_HP));
	OnAddAbil(pkUnit, AT_MP, pkUnit->GetAbil(AT_C_MAX_MP));
}

