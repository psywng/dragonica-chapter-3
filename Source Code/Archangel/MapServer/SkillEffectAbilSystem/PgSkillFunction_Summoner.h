#ifndef PGSKILLFUNTION_SUMMONER_H
#define PGSKILLFUNTION_SUMMONER_H

#include "PgSkillFunction.h"

//SkillNo 150001701 ��ĭ���
class PgSummonFunction :	public PgISkillFunction
{
public:
	PgSummonFunction() {}
	virtual ~PgSummonFunction() {}

public:
	virtual int SkillFire( CUnit *pUnit, int const iSkillNo, SActArg const*  pArg, UNIT_PTR_ARRAY* pUnitArray, PgActionResultVector* pResult);
};

#endif