#include "stdafx.h"
#include "PgSkillFunction.h"
#include "PgSkillFunction_Pet.h"
#include "Variant/PgActionResult.h"
#include "Variant/PgTotalObjectMgr.h"
#include "Variant/Global.h"
#include "Variant/PgPartyMgr.h"
#include "PgLocalPartyMgr.h"
#include "Global.h"
#include "PgGround.h"
#include "PgEffectFunction.h"

///////////////////////////////////////////////////////////
//  PgGoldRushSkillFunction
///////////////////////////////////////////////////////////
int PgGoldRushSkillFunction::SkillFire(CUnit* pkUnit, int const iSkillNo, SActArg const* pArg, UNIT_PTR_ARRAY* pkUnitArray, PgActionResultVector* pkResult)
{
	PgGround *pkGround = GetGroundPtr( pArg );
	if( !pkGround )
	{
		return -1;
	}

	GET_DEF(CSkillDefMgr, kSkillDefMgr);
	CSkillDef const* pkSkill = kSkillDefMgr.GetDef(iSkillNo);	

	int const iEffectPercent = pkSkill->GetAbil(AT_PERCENTAGE);

	int const iHitRate = ABILITY_RATE_VALUE; //��Ʈ Ȯ���� 100%

	VEC_GUID kVecGuid;
	kVecGuid.push_back( pkUnit->Caller() );

	PgLogCont kLogCont(ELogMain_Contents_Pet, ELogSub_Pet );
	kLogCont.MemberKey( pkGround->GroundKey().Guid() );// GroundGuid
	kLogCont.CharacterKey( pkUnit->GetID() );
	kLogCont.GroundNo( pkGround->GetGroundNo() );// GroundNo
	kLogCont.Name( pkUnit->Name() );
	kLogCont.ChannelNo( g_kProcessCfg.ChannelNo() );

	CheckTagetVaild(pkUnit, pkUnitArray, pkSkill, 1000); //Target�� ��ȿ�� Ÿ������ �˻��Ѵ�.

	int const iGoldDropMin = pkSkill->GetAbil(AT_GOLD_RUSH_DROPMONEY_VALUE_MIN);
	int const iGoldDropMax = pkSkill->GetAbil(AT_GOLD_RUSH_DROPMONEY_VALUE_MAX);
	int const iRandomValue = abs(iGoldDropMax - iGoldDropMin);

	UNIT_PTR_ARRAY::iterator unit_itor = pkUnitArray->begin();
	while(pkUnitArray->end() != unit_itor)
	{
		CUnit* pkTarget = (*unit_itor).pkUnit;
		if(pkTarget)
		{
			pkUnit->OnTargetDamaged(pkTarget->GetID());

			PgActionResult* pkAResult = pkResult->GetResult(pkTarget->GetID(), true);
			if(pkAResult && !pkAResult->GetInvalid() && (false == (*unit_itor).bReference) && (false == (*unit_itor).bRestore))
			{
				if(!pkAResult->GetMissed())
				{
					int iResult = 0;
					if(0 != iRandomValue)
					{
						iResult = BM::Rand_Index(iRandomValue);
					}

					__int64 i64Money = iGoldDropMin + iResult;
					if ( 0i64 < i64Money )
					{
						//Ÿ�� ��ġ�� �� �ѷ��ֱ�~
						pkGround->InsertItemBox( pkTarget->GetPos(), kVecGuid, NULL, PgBase_Item::NullData(), i64Money, kLogCont );
					}
				}
			}
		}

		++unit_itor;
	}
	
	kLogCont.Commit();
	return 1;
}