#include "stdafx.h"
#include "PgSkillFunction_Summoner.h"
#include "PgGround.h"

///////////////////////////////////////////////////////////
//  PgSummonFunction
///////////////////////////////////////////////////////////
int PgSummonFunction::SkillFire( CUnit *pUnit, int const iSkillNo, SActArg const*  pArg, UNIT_PTR_ARRAY* pUnitArray, PgActionResultVector* pkResult)
{
	PgGround* pGround = NULL;
	pArg->Get(ACTARG_GROUND, pGround);

	if( !pGround )
	{
		CAUTION_LOG( BM::LOG_LV5, __FL__ << _T("Cannot find Ground SkillNo = ") << iSkillNo);
		return -1;
	}

	GET_DEF(CSkillDefMgr, kSkillDefMgr);
	CSkillDef const* pSkill = kSkillDefMgr.GetDef(iSkillNo);
	if ( !pSkill )
	{
		CAUTION_LOG( BM::LOG_LV5, __FL__ << _T("CSkillDef is NULL SkillNo = ") << iSkillNo);
		return -1;
	}

	CreateSummoned_ kCreateInfo;
	kCreateInfo.kClassKey.iClass = pSkill->GetAbil(AT_CLASS);
	kCreateInfo.kClassKey.nLv = pSkill->GetAbil(AT_LEVEL);
	kCreateInfo.bUniqueClass = false;
	kCreateInfo.ptPos = pUnit->GetPos();

	{// �ٴڿ� ���� ��Ų��
		NxRaycastHit kHit;
		NxRay kRay( NxVec3(kCreateInfo.ptPos.x, kCreateInfo.ptPos.y, kCreateInfo.ptPos.z+20.0f ), NxVec3(0, 0, -1.0f) );
		NxShape *pHitShape = pGround->PhysXScene()->GetPhysXScene()->raycastClosestShape(kRay, NX_STATIC_SHAPES, kHit, -1, 200.0f, NX_RAYCAST_SHAPE|NX_RAYCAST_IMPACT);
		if(pHitShape)
		{
			kCreateInfo.ptPos.z = kHit.worldImpact.z;
		}
	}

	CUnit* pCaller = pUnit;

	kCreateInfo.kGuid.Generate();			// GUID ����

	CUnit* pSummoned = pGround->CreateSummoned(pCaller, &kCreateInfo, L"Summoned");

	return 1;
}