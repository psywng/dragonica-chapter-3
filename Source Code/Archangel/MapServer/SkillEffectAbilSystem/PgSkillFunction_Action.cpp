#include "stdafx.h"
#include "PgSkillFunction.h"
#include "PgSkillFunction_Action.h"
#include "Variant/PgActionResult.h"
#include "Variant/PgTotalObjectMgr.h"
#include "Variant/Global.h"
#include "Global.h"
#include "PgEffectFunction.h"

///////////////////////////////////////////////////////////
//  PgResurrection01SkillFunction
///////////////////////////////////////////////////////////
int PgResurrection01SkillFunction::SkillFire(CUnit* pkUnit, int const iSkillNo, int const iStatus, SActArg const* pArg)
{
	int iAlive = pkUnit->GetAbil(AT_REVIVED_BY_OTHER);
	if(0 < iAlive)
	{
		pkUnit->SetAbil(AT_REVIVED_BY_OTHER, 0);
		int const iHP = pkUnit->GetAbil(AT_HP_RESERVED);
		int const iMP = pkUnit->GetAbil(AT_MP_RESERVED);
		PgPlayer* pkPlayer = dynamic_cast<PgPlayer*>(pkUnit);
		if (pkPlayer != NULL)
		{
			pkPlayer->Alive(EALIVE_ITEM, E_SENDTYPE_BROADALL, iHP, iMP);

			BM::CPacket kNfyPacket(PT_U_G_RUN_ACTION);
			kNfyPacket.Push( static_cast< short >(GAN_SumitLog) );
			kNfyPacket.Push( static_cast< int >(LOG_USER_ALIVE) );
			pkPlayer->VNotify(&kNfyPacket);
		}
		pkUnit->SetAbil(AT_HP_RESERVED, 0);
		pkUnit->SetAbil(AT_MP_RESERVED, 0);
	}	

	return 0; //EActionR_Success
}

///////////////////////////////////////////////////////////
//  PgRunSkillFunction
///////////////////////////////////////////////////////////
int PgRunSkillFunction::SkillFire(CUnit* pkUnit, int const iSkillNo, int const iStatus, SActArg const* pArg)
{
	if(0 < pkUnit->GetAbil(AT_UNLOCK_HIDDEN_MOVE))
	{
		//움직이면 Hidden 상태 해제
		pkUnit->SetAbil(AT_UNIT_HIDDEN, 0);
	}

	return 0; //EActionR_Success
}