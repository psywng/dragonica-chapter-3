#include "stdafx.h"
#include "PgEffectAbilTable.h"
#include "PgEffectFunction_StatusEffect.h"
#include "Variant/Global.h"
#include "Global.h"
#include "Variant/PgPartyMgr.h"
#include "PgLocalPartyMgr.h"
#include "PgGround.h"
#include "Variant/PgParty.h"
#include "Variant/PgPartyMgr.h"
#include "PgPartyItemRule.h"
#include "PgLocalPartyMgr.h"

///////////////////////////////////////////////////////////
//  PgStunStatusFunction - 기절 (상태이상)
///////////////////////////////////////////////////////////
void PgStunStatusFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_MOVESPEED,	pkEffect->GetAbil(AT_R_MOVESPEED));
	OnAddAbil(pkUnit, AT_FROZEN,		pkEffect->GetAbil(AT_FROZEN));
	OnAddAbil(pkUnit, AT_CANNOT_ATTACK,	pkEffect->GetAbil(AT_CANNOT_ATTACK));
	OnAddAbil(pkUnit, AT_CANNOT_EQUIP,	pkEffect->GetAbil(AT_CANNOT_EQUIP));
	OnAddAbil(pkUnit, AT_CANNOT_USEITEM,pkEffect->GetAbil(AT_CANNOT_USEITEM));
	OnAddAbil(pkUnit, AT_CANNOT_CASTSKILL,pkEffect->GetAbil(AT_CANNOT_CASTSKILL));

	// Damage 받아도 깨어 날 수 없다.
	OnSetAbil(pkUnit, AT_FROZEN_DMG_WAKE, 0);
}

void PgStunStatusFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_MOVESPEED,		-pkEffect->GetAbil(AT_R_MOVESPEED));
	OnAddAbil(pkUnit, AT_FROZEN,			-pkEffect->GetAbil(AT_FROZEN));
	OnAddAbil(pkUnit, AT_CANNOT_ATTACK,		-pkEffect->GetAbil(AT_CANNOT_ATTACK));
	OnAddAbil(pkUnit, AT_CANNOT_EQUIP,		-pkEffect->GetAbil(AT_CANNOT_EQUIP));
	OnAddAbil(pkUnit, AT_CANNOT_USEITEM,	-pkEffect->GetAbil(AT_CANNOT_USEITEM));
	OnAddAbil(pkUnit, AT_CANNOT_CASTSKILL,	-pkEffect->GetAbil(AT_CANNOT_CASTSKILL));

	// Damage 받아도 깨어 날 수 없다.
	OnSetAbil(pkUnit, AT_FROZEN_DMG_WAKE, 1);
}

int PgStunStatusFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	INFO_LOG(BM::LOG_LV0, __FL__<<L"Don't call me Tick EffectNo["<<pkEffect->GetEffectNo()<<L"] ");
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgBleedStatusFunction - 출혈 (상태이상)
///////////////////////////////////////////////////////////
void PgBleedStatusFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_MAX_HP, pkEffect->GetAbil(AT_R_MAX_HP));

	int const iMaxHP = pkUnit->GetAbil(AT_C_MAX_HP);
	int const iNowHP = pkUnit->GetAbil(AT_HP);
	int iNewHP = __min(iNowHP, iMaxHP);

	OnSetAbil(pkUnit, AT_HP, iNewHP);
}

void PgBleedStatusFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_MAX_HP, -pkEffect->GetAbil(AT_R_MAX_HP));
}

int PgBleedStatusFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	int const iAddHP = GetTick_AT_HP_Value(pkEffect, pkEffect->GetActArg());
	int const iNowHP = pkUnit->GetAbil(AT_HP);
	int iNewHP = __max(0, iNowHP + iAddHP);
	int const iDelta = iNewHP - iNowHP;
	BM::CPacket kPacket(PT_M_C_NFY_ABILCHANGED);
	kPacket.Push(pkUnit->GetID());
	kPacket.Push((short)AT_HP);
	kPacket.Push(iNewHP);
	kPacket.Push(pkEffect->GetCaster());
	kPacket.Push(pkEffect->GetEffectNo());
	kPacket.Push(iDelta);
	pkUnit->Send(kPacket, E_SENDTYPE_BROADALL);

	OnSetAbil(pkUnit, AT_HP, iNewHP);

	if(0 == iNewHP)
	{
		pkUnit->SetTarget(pkEffect->GetCaster());
		return ECT_MUSTDELETE;
	}

	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgCurseStatusFunction - 저주 (상태이상)
///////////////////////////////////////////////////////////
void PgCurseStatusFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_STR, pkEffect->GetAbil(AT_R_STR));
	OnAddAbil(pkUnit, AT_R_INT, pkEffect->GetAbil(AT_R_INT));
	OnAddAbil(pkUnit, AT_R_CON, pkEffect->GetAbil(AT_R_CON));
	OnAddAbil(pkUnit, AT_R_DEX, pkEffect->GetAbil(AT_R_DEX));
	OnAddAbil(pkUnit, AT_R_MOVESPEED,		pkEffect->GetAbil(AT_R_MOVESPEED));
	OnAddAbil(pkUnit, AT_R_PHY_DEFENCE,		pkEffect->GetAbil(AT_R_PHY_DEFENCE));
	OnAddAbil(pkUnit, AT_R_MAGIC_DEFENCE,	pkEffect->GetAbil(AT_R_MAGIC_DEFENCE));

	int iAdd = pkEffect->GetAbil(AT_R_PHY_ATTACK_MAX);
	OnAddAbil(pkUnit, AT_R_PHY_ATTACK_MAX, iAdd);
	OnAddAbil(pkUnit, AT_R_PHY_ATTACK_MIN, iAdd);
	iAdd = pkEffect->GetAbil(AT_R_MAGIC_ATTACK_MAX);
	OnAddAbil(pkUnit, AT_R_MAGIC_ATTACK_MAX, iAdd);
	OnAddAbil(pkUnit, AT_R_MAGIC_ATTACK_MIN, iAdd);
}	

void PgCurseStatusFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_STR, -pkEffect->GetAbil(AT_R_STR));
	OnAddAbil(pkUnit, AT_R_INT, -pkEffect->GetAbil(AT_R_INT));
	OnAddAbil(pkUnit, AT_R_CON, -pkEffect->GetAbil(AT_R_CON));
	OnAddAbil(pkUnit, AT_R_DEX, -pkEffect->GetAbil(AT_R_DEX));
	OnAddAbil(pkUnit, AT_R_MOVESPEED,		-pkEffect->GetAbil(AT_R_MOVESPEED));
	OnAddAbil(pkUnit, AT_R_PHY_DEFENCE,		-pkEffect->GetAbil(AT_R_PHY_DEFENCE));
	OnAddAbil(pkUnit, AT_R_MAGIC_DEFENCE,	-pkEffect->GetAbil(AT_R_MAGIC_DEFENCE));

	int iAdd = pkEffect->GetAbil(AT_R_PHY_ATTACK_MAX);
	OnAddAbil(pkUnit, AT_R_PHY_ATTACK_MAX, -iAdd);
	OnAddAbil(pkUnit, AT_R_PHY_ATTACK_MIN, -iAdd);
	iAdd = pkEffect->GetAbil(AT_R_MAGIC_ATTACK_MAX);
	OnAddAbil(pkUnit, AT_R_MAGIC_ATTACK_MAX, -iAdd);
	OnAddAbil(pkUnit, AT_R_MAGIC_ATTACK_MIN, -iAdd);
}

int PgCurseStatusFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	INFO_LOG(BM::LOG_LV0, __FL__<<L"Don't call me Tick EffectNo["<<pkEffect->GetEffectNo()<<L"] ");
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgColdStatusFunction - 결빙 (상태이상)
///////////////////////////////////////////////////////////
void PgColdStatusFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_PHY_DEFENCE,		pkEffect->GetAbil(AT_R_PHY_DEFENCE));
	OnAddAbil(pkUnit, AT_R_MAGIC_DEFENCE,	pkEffect->GetAbil(AT_R_MAGIC_DEFENCE));
	OnAddAbil(pkUnit, AT_R_MOVESPEED,		pkEffect->GetAbil(AT_R_MOVESPEED));
	OnAddAbil(pkUnit, AT_R_ATTACK_SPEED,	pkEffect->GetAbil(AT_R_ATTACK_SPEED));
}

void PgColdStatusFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_PHY_DEFENCE,		-pkEffect->GetAbil(AT_R_PHY_DEFENCE));
	OnAddAbil(pkUnit, AT_R_MAGIC_DEFENCE,	-pkEffect->GetAbil(AT_R_MAGIC_DEFENCE));
	OnAddAbil(pkUnit, AT_R_MOVESPEED,		-pkEffect->GetAbil(AT_R_MOVESPEED));
	OnAddAbil(pkUnit, AT_R_ATTACK_SPEED,	-pkEffect->GetAbil(AT_R_ATTACK_SPEED));
}

int PgColdStatusFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	INFO_LOG(BM::LOG_LV0, __FL__<<L"Don't call me Tick EffectNo["<<pkEffect->GetEffectNo()<<L"] ");
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgSilenceStatusFunction - 침묵 (상태이상)
///////////////////////////////////////////////////////////
void PgSilenceStatusFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_MOVESPEED,		pkEffect->GetAbil(AT_R_MOVESPEED));
	OnAddAbil(pkUnit, AT_R_ATTACK_SPEED,	pkEffect->GetAbil(AT_R_ATTACK_SPEED));
	OnAddAbil(pkUnit, AT_CANNOT_CASTSKILL,	pkEffect->GetAbil(AT_CANNOT_CASTSKILL));
	OnAddAbil(pkUnit, AT_CANNOT_ATTACK,	pkEffect->GetAbil(AT_CANNOT_ATTACK));
}

void PgSilenceStatusFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_MOVESPEED,		-pkEffect->GetAbil(AT_R_MOVESPEED));
	OnAddAbil(pkUnit, AT_R_ATTACK_SPEED,	-pkEffect->GetAbil(AT_R_ATTACK_SPEED));
	OnAddAbil(pkUnit, AT_CANNOT_CASTSKILL,	-pkEffect->GetAbil(AT_CANNOT_CASTSKILL));
	OnAddAbil(pkUnit, AT_CANNOT_ATTACK,		-pkEffect->GetAbil(AT_CANNOT_ATTACK));
}

int PgSilenceStatusFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	INFO_LOG(BM::LOG_LV0, __FL__<<L"Don't call me Tick EffectNo["<<pkEffect->GetEffectNo()<<L"] ");
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgPoisonStatusFunction - 중독 (상태이상)
///////////////////////////////////////////////////////////
void PgPoisonStatusFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());
}

void PgPoisonStatusFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());
}

int PgPoisonStatusFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	int const iAddHP = GetTick_AT_HP_Value(pkEffect, pkEffect->GetActArg());
	int const iNowHP = pkUnit->GetAbil(AT_HP);
	int iNewHP = __max(1, iNowHP + iAddHP);
	int const iDelta = iNewHP - iNowHP;

	BM::CPacket kPacket(PT_M_C_NFY_ABILCHANGED);
	kPacket.Push(pkUnit->GetID());
	kPacket.Push((short)AT_HP);
	kPacket.Push(iNewHP);
	kPacket.Push(pkEffect->GetCaster());
	kPacket.Push(pkEffect->GetEffectNo());
	kPacket.Push(iDelta);
	pkUnit->Send(kPacket, E_SENDTYPE_BROADALL);

	OnSetAbil(pkUnit, AT_HP, iNewHP);

	// HP가 1이면 중단
	if(1 >= iNewHP)
	{
		return ECT_MUSTDELETE;
	}

	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgSleepStatusFunction - 수면 (상태이상)
///////////////////////////////////////////////////////////
void PgSleepStatusFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_MOVESPEED,		pkEffect->GetAbil(AT_R_MOVESPEED));
	OnAddAbil(pkUnit, AT_CANNOT_EQUIP,		pkEffect->GetAbil(AT_CANNOT_EQUIP));
	OnAddAbil(pkUnit, AT_CANNOT_USEITEM,	pkEffect->GetAbil(AT_CANNOT_USEITEM));
	OnAddAbil(pkUnit, AT_CANNOT_CASTSKILL,	pkEffect->GetAbil(AT_CANNOT_CASTSKILL));
	OnAddAbil(pkUnit, AT_R_ATTACK_SPEED,	pkEffect->GetAbil(AT_R_ATTACK_SPEED));
	OnAddAbil(pkUnit, AT_CANNOT_ATTACK,		pkEffect->GetAbil(AT_CANNOT_ATTACK));

	int iAbil = pkUnit->GetAbil(AT_ENABLE_AUTOHEAL);
	//기존의 오토힐 상태를 저장해둔다.
	OnSetAbil(pkUnit, AT_ENABLE_AUTOHEAL_BACKUP, iAbil);
	//Hp 자동 회복이 되는 상태이면 제거
	if(iAbil & AUTOHEAL_HP)
	{
		OnSetAbil(pkUnit, AT_ENABLE_AUTOHEAL, iAbil-AUTOHEAL_HP);
		iAbil -= AUTOHEAL_HP;
	}

	//Mp 자동 회복이 되는 상태이면 제거
	if(iAbil & AUTOHEAL_MP)
	{
		OnSetAbil(pkUnit, AT_ENABLE_AUTOHEAL, iAbil-AUTOHEAL_MP);
	}

	OnSetAbil(pkUnit, AT_HP_BACKUP, pkUnit->GetAbil(AT_HP));
}

void PgSleepStatusFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_MOVESPEED,		-pkEffect->GetAbil(AT_R_MOVESPEED));
	OnAddAbil(pkUnit, AT_CANNOT_EQUIP,		-pkEffect->GetAbil(AT_CANNOT_EQUIP));
	OnAddAbil(pkUnit, AT_CANNOT_USEITEM,	-pkEffect->GetAbil(AT_CANNOT_USEITEM));
	OnAddAbil(pkUnit, AT_CANNOT_CASTSKILL,	-pkEffect->GetAbil(AT_CANNOT_CASTSKILL));
	OnAddAbil(pkUnit, AT_R_ATTACK_SPEED,	-pkEffect->GetAbil(AT_R_ATTACK_SPEED));
	OnAddAbil(pkUnit, AT_CANNOT_ATTACK,		-pkEffect->GetAbil(AT_CANNOT_ATTACK));

	OnSetAbil(pkUnit, AT_ENABLE_AUTOHEAL, pkUnit->GetAbil(AT_ENABLE_AUTOHEAL_BACKUP));
	OnSetAbil(pkUnit, AT_ENABLE_AUTOHEAL_BACKUP, 0);
	OnSetAbil(pkUnit, AT_HP_BACKUP, 0);
}

int PgSleepStatusFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	//데미지를 입었다면 수면상태가 해제 된다.
	if(pkUnit->GetAbil(AT_HP_BACKUP) > pkUnit->GetAbil(AT_HP))
	{
		return ECT_MUSTDELETE;
	}
	//INFO_LOG(BM::LOG_LV0, _T("[%s] Don't call me Tick EffectNo[%d] "), __FUNCTIONW__, pkEffect->GetEffectNo());
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgFleshtoStoneStatusFunction - 석화 (상태이상)
///////////////////////////////////////////////////////////
void PgFleshtoStoneStatusFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_PHY_DEFENCE,		pkEffect->GetAbil(AT_R_PHY_DEFENCE));
	OnAddAbil(pkUnit, AT_R_MAGIC_DEFENCE,	pkEffect->GetAbil(AT_R_MAGIC_DEFENCE));
}

void PgFleshtoStoneStatusFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_PHY_DEFENCE,		-pkEffect->GetAbil(AT_R_PHY_DEFENCE));
	OnAddAbil(pkUnit, AT_R_MAGIC_DEFENCE,	-pkEffect->GetAbil(AT_R_MAGIC_DEFENCE));
}

int PgFleshtoStoneStatusFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	INFO_LOG(BM::LOG_LV0, __FL__<<L"Don't call me Tick EffectNo["<<pkEffect->GetEffectNo()<<L"] ");
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgBurnStatusFunction - 화상 (상태이상)
///////////////////////////////////////////////////////////
void PgBurnStatusFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_MAX_MP, pkEffect->GetAbil(AT_R_MAX_MP));

	int const iMaxMP = pkUnit->GetAbil(AT_C_MAX_MP);
	int const iNowMP = pkUnit->GetAbil(AT_MP);
	int iNewMP = __min(iNowMP, iMaxMP);

	OnSetAbil(pkUnit, AT_MP, iNewMP);
}

void PgBurnStatusFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_MAX_MP, -pkEffect->GetAbil(AT_R_MAX_MP));
}

int PgBurnStatusFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	//MP -
	{
		int const iAddMP = pkEffect->GetAbil(AT_MP);
		int const iNowMP = pkUnit->GetAbil(AT_MP);
		int iNewMP = __max(0, iNowMP + iAddMP);
		int const iDelta = iNewMP - iNowMP;
		BM::CPacket kPacket(PT_M_C_NFY_ABILCHANGED);
		kPacket.Push(pkUnit->GetID());
		kPacket.Push((short)AT_MP);
		kPacket.Push(iNewMP);
		kPacket.Push(pkEffect->GetCaster());
		kPacket.Push(pkEffect->GetEffectNo());
		kPacket.Push(iDelta);
		pkUnit->Send(kPacket, E_SENDTYPE_BROADALL);

		OnSetAbil(pkUnit, AT_MP, iNewMP);
	}

	//HP -
	{
		int const iAddHP = GetTick_AT_HP_Value(pkEffect, pkEffect->GetActArg());
		int const iNowHP = pkUnit->GetAbil(AT_HP);
		int iNewHP = __max(0, iNowHP + iAddHP);
		int const iDelta = iNewHP - iNowHP;
		BM::CPacket kPacket2(PT_M_C_NFY_ABILCHANGED);
		kPacket2.Push(pkUnit->GetID());
		kPacket2.Push((short)AT_HP);
		kPacket2.Push(iNewHP);
		kPacket2.Push(pkEffect->GetCaster());
		kPacket2.Push(pkEffect->GetEffectNo());
		kPacket2.Push(iDelta);
		pkUnit->Send(kPacket2, E_SENDTYPE_BROADALL);

		OnSetAbil(pkUnit, AT_HP, iNewHP);
		if(0 == iNewHP)
		{
			pkUnit->SetTarget(pkEffect->GetCaster());
			return ECT_MUSTDELETE;
		}
	}

	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgBlindStatusFunction - 실명 (상태이상)
///////////////////////////////////////////////////////////
void PgBlindStatusFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_HITRATE,		pkEffect->GetAbil(AT_R_HITRATE));
	OnAddAbil(pkUnit, AT_R_DODGE_RATE,	pkEffect->GetAbil(AT_R_DODGE_RATE));
}

void PgBlindStatusFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//INFO_LOG(BM::LOG_LV9, _T("[%s][%d]"), __FUNCTIONW__, pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_HITRATE,		-pkEffect->GetAbil(AT_R_HITRATE));
	OnAddAbil(pkUnit, AT_R_DODGE_RATE,	-pkEffect->GetAbil(AT_R_DODGE_RATE));
}

int PgBlindStatusFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	INFO_LOG(BM::LOG_LV0, __FL__<<L"Don't call me Tick EffectNo["<<pkEffect->GetEffectNo()<<L"] ");
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgSlowStatusFunction - 슬로우 (상태이상)
///////////////////////////////////////////////////////////
void PgSlowStatusFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const *pkArg)
{
	INFO_LOG(BM::LOG_LV9, __FL__<<L"EffectNo="<<pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_MOVESPEED,		pkEffect->GetAbil(AT_R_MOVESPEED));
	OnAddAbil(pkUnit, AT_R_ATTACK_SPEED,	pkEffect->GetAbil(AT_R_ATTACK_SPEED));
}

void PgSlowStatusFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const *pkArg)
{
	INFO_LOG(BM::LOG_LV9, __FL__<<L"EffectNo="<<pkEffect->GetEffectNo());

	OnAddAbil(pkUnit, AT_R_MOVESPEED,		-pkEffect->GetAbil(AT_R_MOVESPEED));
	OnAddAbil(pkUnit, AT_R_ATTACK_SPEED,	-pkEffect->GetAbil(AT_R_ATTACK_SPEED));
}

int PgSlowStatusFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const *pkArg, DWORD const dwElapsed)
{
	INFO_LOG(BM::LOG_LV6, __FL__<<L"Don't call me Tick EffectNo["<<pkEffect->GetEffectNo()<<L"] ");
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgMissionPenaltyFunction
///////////////////////////////////////////////////////////
void PgMissionPenaltyFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	int iPenaltyHP = pkEffect->GetAbil(AT_PENALTY_HP_RATE);
	if (0!=iPenaltyHP)
	{
		int iHP = pkUnit->GetAbil(AT_HP);
		iHP+=(int)((float)(iHP*iPenaltyHP)*0.0001f);
		pkUnit->SetAbil(AT_HP, iHP, true, false);
	}

	int iPenaltyMP = pkEffect->GetAbil(AT_PENALTY_MP_RATE);
	if (0!=iPenaltyMP)
	{
		int iMP = pkUnit->GetAbil(AT_MP);
		iMP+=(int)((float)(iMP*iPenaltyMP)*0.0001f);
		pkUnit->SetAbil(AT_MP, iMP, true, false);
	}

	int iPenaltyEXP = pkEffect->GetAbil(AT_ADD_EXPERIENCE_RATE);
	if (0!=iPenaltyEXP)
	{
		pkUnit->AddAbil(AT_ADD_EXPERIENCE_RATE, iPenaltyEXP);
	}
}

void PgMissionPenaltyFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	int iPenaltyEXP = pkEffect->GetAbil(AT_ADD_EXPERIENCE_RATE);
	if (0!=iPenaltyEXP)
	{
		pkUnit->AddAbil(AT_ADD_EXPERIENCE_RATE, -iPenaltyEXP);
	}
}
int PgMissionPenaltyFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgItemEffectFunction
///////////////////////////////////////////////////////////
void PgItemEffectFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{ 
	int const iAbilArray[6] = { AT_R_MAX_HP, AT_R_MAX_MP, AT_STR, AT_INT, AT_CON, AT_DEX };
	WORD wType[6] = {0,};
	PgUnitEffectMgr& rkMgr = pkUnit->GetEffectMgr();

	int iCount = 0;
	for(int i = 0; i < 6; ++i)
	{
		int const iAdd = pkEffect->GetAbil(iAbilArray[i]);
		if (0!=iAdd)
		{
			rkMgr.AddAbil(iAbilArray[i], iAdd);
			pkUnit->NftChangedAbil(iAbilArray[i]);
			wType[iCount] = (WORD)iAbilArray[i];
			++iCount;
		}
	}

	if (0 < iCount)
	{
		pkUnit->SendAbiles(wType, iCount, E_SENDTYPE_BROADALL|E_SENDTYPE_EFFECTABIL);
	}
}

void PgItemEffectFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	int const iAbilArray[6] = { AT_R_MAX_HP, AT_R_MAX_MP, AT_STR, AT_INT, AT_CON, AT_DEX };
	WORD wType[6] = {0,};
	PgUnitEffectMgr& rkMgr = pkUnit->GetEffectMgr();

	int iCount = 0;
	for(int i = 0; i < 6; ++i)
	{
		int const iAdd = pkEffect->GetAbil(iAbilArray[i]);
		if (0!=iAdd)
		{
			rkMgr.AddAbil(iAbilArray[i], -iAdd);
			pkUnit->NftChangedAbil(iAbilArray[i]);
			wType[iCount] = (WORD)iAbilArray[i];
			++iCount;
		}
	}

	if (0 < iCount)
	{
		pkUnit->SendAbiles(wType, iCount, E_SENDTYPE_BROADALL|E_SENDTYPE_EFFECTABIL);
	}
}
int PgItemEffectFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgCannotDamageFunction
///////////////////////////////////////////////////////////
void PgCannotDamageFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
//	int iValue = pkEffect->GetAbil(AT_CANNOT_DAMAGE);
	OnAddAbil( pkUnit, AT_CANNOT_DAMAGE, 1, E_SENDTYPE_NONE );
}

void PgCannotDamageFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
//	int iValue = pkEffect->GetAbil(AT_CANNOT_DAMAGE);
	OnAddAbil( pkUnit, AT_CANNOT_DAMAGE, -1, E_SENDTYPE_NONE );
}
int PgCannotDamageFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgSafetyCapFunction
///////////////////////////////////////////////////////////
void PgSafetyCapFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//공격불가 / 피격불가
	OnAddAbil(pkUnit, AT_CANNOT_DAMAGE, 1);
	OnAddAbil(pkUnit, AT_CANNOT_ATTACK, 1);
}

void PgSafetyCapFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	OnAddAbil(pkUnit, AT_CANNOT_DAMAGE, -1);
	OnAddAbil(pkUnit, AT_CANNOT_ATTACK, -1);
}
int PgSafetyCapFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgDefenceCorrectionFunction
///////////////////////////////////////////////////////////
void PgDefenceCorrectionFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	//HP
	PgUnitEffectMgr& rkMgr = pkUnit->GetEffectMgr();
	int const iAdd = pkEffect->GetAbil(AT_R_MAX_HP);
	rkMgr.AddAbil(AT_R_MAX_HP, iAdd);
	pkUnit->NftChangedAbil(AT_R_MAX_HP, E_SENDTYPE_BROADALL|E_SENDTYPE_EFFECTABIL);

	int const iLevel = pkUnit->GetAbil(AT_LEVEL);
	int const iHandicap = (int)((5.0f - ((float)iLevel)/50.0f)*1000.0f);
	pkEffect->SetActArg( AT_HANDYCAP, iHandicap );

	//Defence
	if ( 0!=iHandicap )
	{
		rkMgr.AddAbil(AT_R_PHY_DEFENCE, iHandicap);
		rkMgr.AddAbil(AT_R_MAGIC_DEFENCE, iHandicap);
		pkUnit->NftChangedAbil(AT_R_PHY_DEFENCE, E_SENDTYPE_BROADALL|E_SENDTYPE_EFFECTABIL);
		pkUnit->NftChangedAbil(AT_R_MAGIC_DEFENCE, E_SENDTYPE_BROADALL|E_SENDTYPE_EFFECTABIL);
	}
}

void PgDefenceCorrectionFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	PgUnitEffectMgr& rkMgr = pkUnit->GetEffectMgr();
	int const iAdd = pkEffect->GetAbil(AT_R_MAX_HP) * -1;
	rkMgr.AddAbil(AT_R_MAX_HP, iAdd);
	pkUnit->NftChangedAbil(AT_R_MAX_HP, E_SENDTYPE_BROADALL|E_SENDTYPE_EFFECTABIL);

	//Defence
	int iHandicap = 0;
	pkEffect->GetActArg(AT_HANDYCAP, iHandicap);
	if ( 0!=iHandicap )
	{
		rkMgr.AddAbil(AT_R_PHY_DEFENCE, -iHandicap);
		rkMgr.AddAbil(AT_R_MAGIC_DEFENCE, -iHandicap);
		pkUnit->NftChangedAbil(AT_R_PHY_DEFENCE, E_SENDTYPE_BROADALL|E_SENDTYPE_EFFECTABIL);
		pkUnit->NftChangedAbil(AT_R_MAGIC_DEFENCE, E_SENDTYPE_BROADALL|E_SENDTYPE_EFFECTABIL);
	}
}
int PgDefenceCorrectionFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	return ECT_DOTICK;
}

///////////////////////////////////////////////////////////
//  PgLovePowerFunction
///////////////////////////////////////////////////////////
void PgLovePowerFunction::EffectBegin(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	PgUnitEffectMgr& rkMgr = pkUnit->GetEffectMgr();

	int iAdd = pkEffect->GetAbil(AT_R_STR);
	rkMgr.AddAbil(AT_R_STR, iAdd);
	pkUnit->NftChangedAbil(AT_R_STR, E_SENDTYPE_BROADALL|E_SENDTYPE_EFFECTABIL);
}

void PgLovePowerFunction::EffectEnd(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg)
{
	PgUnitEffectMgr& rkMgr = pkUnit->GetEffectMgr();

	int iAdd = pkEffect->GetAbil(AT_R_STR);
	rkMgr.AddAbil(AT_R_STR, -iAdd);
	pkUnit->NftChangedAbil(AT_R_STR, E_SENDTYPE_BROADALL|E_SENDTYPE_EFFECTABIL);
}
int PgLovePowerFunction::EffectTick(CUnit* pkUnit, CEffect* pkEffect, SActArg const* pkArg, DWORD const dwElapsed)
{
	return ECT_DOTICK;
}
