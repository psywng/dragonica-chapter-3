#ifndef MAP_MAPSERVER_MAP_PGGROUNDMGR_H
#define MAP_MAPSERVER_MAP_PGGROUNDMGR_H

#include <map>

#include "BM/GUID.h"
#include "BM/ObjectPool.h"
#include "BM/Observer.h"
#include "variant/PgObjectMgr2.h"
#include "Variant/PgMission.h"
#include "PgSmallArea.h"
//#include "PgWayPoint.h"
#include "BM/PgTask.h"
#include "PgTask_Mapserver.h"
#include "PgGroundTrigger.h"
#include "PublicMap/PgWarGround.h"
#include "PublicMap/PgEmporiaGround.h"
#include "PgMissionGround.h"
#include "PgIndunHome.h"
#include "PublicMap/PgBSGround.h"
#include "PublicMap/PgHardCoreDungeon.h"
#include "PgSuperGround.h"

int const TickInfo_LoopCountMax  = 999;

class PgAlramMissionMgr_Warpper;

class PgGroundRscMgr
{
public:
	typedef BM::TObjectPool< PgGroundResource >	GND_RESOURCE_POOL;
	typedef std::map< int, PgGroundResource* > GND_RESOURCE_CONT;	// first : GroundNo
	
	typedef std::map< int, CONT_GTRIGGER* >				CONT_GTRIGGER_MGR;	// first : GroundNo		
	typedef std::map<int, PgAlramMissionMgr_Warpper* >	CONT_ALRAM_MISSION_MGR;// first : alram_type

public:
	PgGroundRscMgr();
	virtual ~PgGroundRscMgr();

public:
	static bool GetMapBuildData(	SERVER_IDENTITY const &kSI,
									CONT_MAP_CONFIG const &rkStaticData,
									CONT_MAP_CONFIG const &rkMissionData,
									CONT_DEFMAP const *pkDefMap,
									CONT_DEF_MISSION_ROOT const *pkMissionRoot,
									CONT_DEF_MISSION_CANDIDATE const *pkMissionCandi,
									CONT_MISSION_BONUSMAP const *pkMissionBonusMap,
									CONT_DEF_SUPER_GROUND_GROUP const* pkSuperGroundGrp,
									CONT_MAP_BUILD_DATA &rOutAddData,
									bool const bOnlyMyServer=true);

	HRESULT Reserve( CONT_MAP_BUILD_DATA &rHash );
	void Clear();

	HRESULT Locked_AddGroundResource(int const iGroundNo, T_GNDATTR const kAttr, PgAlramMissionMgr_Warpper const * pkAlramMissionMgr );
	HRESULT Locked_GetGroundResource(int const iGroundNo, PgGroundResource const *&rkOut )const;	// 그라운드 리소스를 반환한다.

protected:
	HRESULT ParseXml( PgGroundResource *pkGndResource, char const *pcXmlPath );
	bool ParseWorldXml( PgGroundResource *pkGndResource, TiXmlElement const *pkElement );

	bool	GetDefaultGsaPath(TiXmlNode const *pkNode,std::string &kPath);
	CONT_ALRAM_MISSION_MGR::mapped_type GetAlramMissionMgr( int const iType )const;

	GND_RESOURCE_POOL	m_kGndResourcePool;
	GND_RESOURCE_CONT	m_kGndResourceCon;//한번등록하면 지우지 않아야. 주소도 변하지 않아야.
	CONT_ALRAM_MISSION_MGR	m_kContAlramMissionMgr;

	mutable BM::ACE_RW_Thread_Mutex_Ext m_kRscLock;//m_kGndLock에 앞서 잡혀라.
};

class PgGroundManager
{
	typedef BM::TObjectPool< PgGround >				GND_POOL;
	typedef BM::TObjectPool< PgIndun >				GND_POOL_INDUN;
	typedef BM::TObjectPool< PgWarGround >			GND_POOL_WAR;
	typedef BM::TObjectPool< PgMissionGround >		GND_POOL_MISSION;
	typedef BM::TObjectPool< PgEmporiaGround >		GND_POOL_EMPORIA;
	typedef BM::TObjectPool< PgIndunHome >			GND_POOL_HOME;
	typedef BM::TObjectPool< PgBSGround >			GND_POOL_BS;
	typedef BM::TObjectPool< PgHardCoreDungeon >	GND_POOL_HARDCORE;
	typedef BM::TObjectPool< PgSuperGround >		GND_POOL_SUPERGND;

	typedef std::map<SGroundKey, PgGround*> GND_CONT;//전체 컨테이너
	
	typedef struct tagGroundAddDesc
	{
		tagGroundAddDesc()
			:	iBalance(0)
			,	iWeight(0)
			,	iControlID(0)
			,	iOwnerLv(0)
			,	kAttr(GATTR_DEFAULT)
		{}

		tagGroundAddDesc( SGroundMakeOrder const &kMakeOrder )
			:	kKey( kMakeOrder.kKey )
			,	iBalance( kMakeOrder.byBalance )
			,	iWeight( kMakeOrder.byWeigth )
			,	iControlID(0)
			,	iOwnerLv(kMakeOrder.iOwnerLv)
			,	kAttr(GATTR_DEFAULT)
		{}
		
		T_GNDATTR	kAttr;
		int			iBalance;
		int			iWeight;	// Map의 가중치
		int			iControlID;	// Monster ControlID
		int			iOwnerLv;
		SGroundKey	kKey;
	}SGroundAddDesc;

	typedef std::map<int, SMapPlayTime> GND_PLAYTIME;	// <GroundNo, PlayTimeInfo>

public:
	PgGroundManager();
	virtual ~PgGroundManager();

	void Init(bool bIsPublicMap);

	bool GroundEventNfy(SGroundKey const& rkGroundKey, BM::GUID const &rkCharacterGuid, BM::CPacket * const pkPacket);
	void Clear();

//	void SendAllBalancingGnd();
	bool Reserve(CONT_MAP_BUILD_DATA &rHash);
	HRESULT OrderCreate( SGroundMakeOrder const &kOrder, BM::CPacket * const pkPacket );
	bool OrderCreateMission( PgMission *pkMission );
	bool RestartMission(BM::CPacket * const pkPacket);

	void RecvGndWrapped(BM::CPacket* const pkPacket );
	void Release(const T_GNDATTR kReleaseGndAttr=GATTR_ALL);
	typedef struct tagSTickInfo 
	{
		short int sLoopCount;
		SGroundKey kNextGroundKey;
		tagSTickInfo()
		{
			Init();
		}
		void Init()
		{
			sLoopCount = 0;
			kNextGroundKey.Clear();
		}
	}STickInfo;

	typedef std::vector<STickInfo> TICK_INFO_VEC;

	void _TestReqCreateMissionGround(int const iCount) const;	// Mission 생성 테스트 함수 (외부에서 사용 금지) 오직 테스트용

public:
	void OnTimer(ETickInterval eInterval);
	void DisplayState();
	int Locked_GetConnectionMapUserCount() const;

public:
	bool ProcessMsg(SEventMessage *pkMsg);

	//void SendToGroundUser(SGroundKey const& rkTrgGndKey, const BM::GUID rkGuid, BM::CPacket const& rkPacket, DWORD const dwSendType = E_SENDTYPE_SELF) const;
	//void SendToGroundUser(SGroundKey const& rkTrgGndKey, VEC_GUID const& rkGuidVec, BM::CPacket const& rkPacket, DWORD const dwSendType = E_SENDTYPE_SELF) const;

	void ProcessRemoveUser(SERVER_IDENTITY const &kSI);
	void ProcessRemoveUser(BM::GUID const &kCharGuid);

	// 
	void SendAllGround()const;

protected:
	// 
	PgGround *GetGround(SGroundKey const &rkKey);//! 맵을 반환한다. --> Public으로 이동 금지!
	PgGround *AddGround(SGroundAddDesc const &rkAddDesc, GroundArr& rkOutGndArr, BM::CPacket * const pkPacket=NULL );

	PgGround* Create( SGroundAddDesc const&rkAddDesc, BM::CPacket * const pkPacket );
	PgGround* CreatePool( T_GNDATTR const kGndAttr );
	PgGround* PickupTickGround(ETickInterval eInterval);	// public 이동 금지(Lock을 안잡고있으니, 절대 옮기지 마세요)

	void OnTimer5s();
	void OnTimer30s();
	void AddPlayTime(SGroundKey const& rkGndKey, SMapPlayTime const& rkPlayTime);
	void WritePlayTimeLog(int const iElapsedTimeSec) const;
	bool IsHaveGround(SGroundKey const &rkKey) const;//내 서버에서 관리하는 그라운드 인가?

private:
	bool Delete(GND_KEYLIST& rkGndKeyList);
	void DeletePool( PgGround*& pkGnd );

	void SendNfyCreateGround( GroundArr const &kSendGndArr )const;

protected:
	//mutable Loki::Mutex m_kGndLock;// 생성요청이 들어왔을때 락을 잡아야지
	mutable BM::ACE_RW_Thread_Mutex_Ext m_kGndLock;
	
	GND_POOL			m_kGndPool;
	GND_POOL_INDUN		m_kGndPoolIndun;
	GND_POOL_WAR		m_kGndPoolWar;
	GND_POOL_MISSION	m_kGndPoolMission;
	GND_POOL_EMPORIA	m_kGndPoolEmporia;
	GND_POOL_HOME		m_kGndPoolHome;
	GND_POOL_BS			m_kGndPoolBS;
	GND_POOL_HARDCORE	m_kGndPoolHardCore;
	GND_POOL_SUPERGND	m_kGndPoolSuperGround;
	
	GND_CONT			m_kStaticMapCon;
	GND_PLAYTIME		m_kPlayTimeInfo;
	TICK_INFO_VEC		m_kTickInfo;
	Loki::Mutex			m_akTickInfoMutex[ETICK_INTERVAL_MAX];

public:
	PgGroundRscMgr		m_kGndRscMgr;
};

#include "PgGroundMgr.inl"

#define g_kGndMgr SINGLETON_STATIC(PgGroundManager)


//
class PgMissionDefMgr : public PgMissionContMgr
{
public:
	PgMissionDefMgr() {};
	virtual ~PgMissionDefMgr() {};

	size_t GetTotalMissionCount(int const iMissionKey) const
	{
		ConPack::const_iterator pack_iter = m_kConPack.find(iMissionKey);
		if(m_kConPack.end() != pack_iter)
		{
			const ConPack::mapped_type& rkMissionPack = (*pack_iter).second;
			return rkMissionPack.TotalMissionCount();
		}
		return 0;
	}
};
#define g_kMissionContMgr SINGLETON_STATIC(PgMissionDefMgr)//Mission Level Def Container

#endif // MAP_MAPSERVER_MAP_PGGROUNDMGR_H