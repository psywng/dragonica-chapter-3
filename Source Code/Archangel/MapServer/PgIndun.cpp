#include "stdafx.h"
#include "Variant/constant.h"
#include "Global.h"
#include "Variant/PgPartyMgr.h"
#include "PgLocalPartyMgr.h"
#include "PgIndun.h"
#include "Variant/PgParty.h"
#include "Variant/PgPartyMgr.h"
#include "variant/PgEventview.h"
#include "BM/PgTask.h"
#include "PgPartyItemRule.h"
#include "PgLocalPartyMgr.h"
#include "PgStoneMgr.h"
#include "PgAction.h"

PgIndun::PgIndun()
:	m_dwStartTime(0)
,	m_eState(INDUN_STATE_NONE)
,	m_dwAutoStateRemainTime(0)
,	m_eOpening(E_OPENING_NONE)

// 특정 옵션
,	m_bUseGroundEffect(true)
,	m_bUseItem(true)
,	m_iResultMoveMapNum(0)
{
}

PgIndun::~PgIndun()
{

}

EOpeningState PgIndun::Init( int const iMonsterControlID, bool const bMonsterGen )
{
	// 특정 옵션
	m_bUseGroundEffect = true;
	m_bUseItem = true;
	m_eOpening = PgGround::Init( iMonsterControlID, bMonsterGen );
	m_eState = INDUN_STATE_NONE;
	SetState( INDUN_STATE_OPEN );
	return m_eOpening;
}

void PgIndun::Clear()
{
	if ( !m_kOwnerGndInfo.kOwnerGndKey.IsEmpty() )
	{
		BM::CPacket kNfyPacket( PT_M_M_NFY_HARDCORE_BOSS_ENDTIME );
		GroundKey().WriteToPacket( kNfyPacket );
		kNfyPacket.Push( 0i64 );
		::SendToGround( m_kOwnerGndInfo.kOwnerGndKey, kNfyPacket );
	}

	PgGround::Clear();
	StartTime( 0 );
	m_dwAutoStateRemainTime = 0;
	m_eState = INDUN_STATE_NONE;
	m_eOpening = E_OPENING_NONE;

	m_bUseGroundEffect = true;
	m_bUseItem = true;
	m_kOwnerGndInfo = SOwnerGroundInfo();
}

bool PgIndun::Clone( PgGround* pkGround )
{
	return this->Clone( (dynamic_cast<PgIndun*>(pkGround)) );
}

bool PgIndun::Clone( PgIndun* pkIndun )
{
	if ( pkIndun )
	{
		StartTime( pkIndun->StartTime() );
		m_dwAutoStateRemainTime = pkIndun->m_dwAutoStateRemainTime;
		m_eState = pkIndun->m_eState;
		m_eOpening = pkIndun->m_eOpening;
		m_bUseGroundEffect = pkIndun->m_bUseGroundEffect;
		m_bUseItem = pkIndun->m_bUseItem;
		m_kOwnerGndInfo = pkIndun->m_kOwnerGndInfo;
		return PgGround::Clone( dynamic_cast<PgGround*>(pkIndun) );
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

bool PgIndun::IsDeleteTime()const
{
	BM::CAutoMutex kLock(m_kRscMutex);
	switch ( GetState() )
	{
	case INDUN_STATE_OPEN:
		{

		}break;
	case INDUN_STATE_WAIT:
		{
			if ( m_kWaitUserList.empty() )
			{
				return ( 0 == PgObjectMgr::GetUnitCount(UT_PLAYER) );
			}
		}break;
	default:
		{
			return ( 0 == PgObjectMgr::GetUnitCount(UT_PLAYER) );
		}break;
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

void PgIndun::UpdateAutoNextState(DWORD const dwNow, DWORD const dwElapsedTime)
{
	if ( 0 == m_dwAutoStateRemainTime )
	{
		return;
	}

	if ( dwElapsedTime >= m_dwAutoStateRemainTime )
	{
		EIndunState eState = (EIndunState)(m_eState << 1);
		switch( eState )
		{
		case INDUN_STATE_WAIT:
			{
				eState = INDUN_STATE_CLOSE;
			}break;
		case INDUN_STATE_READY:
			{
				for( ConWaitUser::const_iterator itr=m_kWaitUserList.begin(); itr!=m_kWaitUserList.end(); ++itr )
				{
					INFO_LOG(BM::LOG_LV5, __FL__<<L"[USER MAPLOADING]Wait OverTime["<<(ms_dwMaxWaitUserWaitngTime/1000)<<L" sec] : User["<<itr->kCharGuid<<L"]");
				}
			}break;
		}
		SetState( eState, true );
	}
	else
	{
		m_dwAutoStateRemainTime -= dwElapsedTime;
	}
}

bool PgIndun::RecvMapMove( UNIT_PTR_ARRAY &rkUnitArray, SReqMapMove_MT& rkRMM, CONT_PET_MAPMOVE_DATA &kContPetMapMoveData, CONT_PLAYER_MODIFY_ORDER const &kModifyOrder )
{
	if ( !PgGround::RecvMapMove( rkUnitArray, rkRMM, kContPetMapMoveData, kModifyOrder ) )
	{
		return false;
	}

	if ( INDUN_STATE_OPEN == m_eState )
	{
		VEC_GUID kWaitList;
		VEC_JOININDUN kJoinIndunList;
		UNIT_PTR_ARRAY::iterator unit_itr;
		for ( unit_itr=rkUnitArray.begin(); unit_itr!=rkUnitArray.end(); ++unit_itr )
		{
			PgPlayer *pkUser = dynamic_cast<PgPlayer*>(unit_itr->pkUnit);
			if ( pkUser )
			{
				kWaitList.push_back(pkUser->GetID());
				kJoinIndunList.push_back( SNfyJoinIndun(pkUser->GetID(),pkUser->GetMemberGUID(),pkUser->GetAbil64(AT_EXPERIENCE)) );
			}
		}

		// Contents Server에게 통보
		switch( this->GetAttr() )
		{
		case GATTR_PVP:
			{
				VERIFY_INFO_LOG( false, BM::LOG_LV0, __FL__<<L"State Error!!");
				kWaitList.clear();
			}break;
		case GATTR_HARDCORE_DUNGEON_BOSS:
			{
				BM::CPacket kMPacket( PT_M_M_REQ_READY_HARDCORE_BOSS );
				GroundKey().WriteToPacket( kMPacket );
				::SendToGround( rkRMM.kCasterKey, kMPacket );

				m_kOwnerGndInfo.kOwnerGndKey = rkRMM.kCasterKey;
			}// break을 사용하지 않고 다음에는 반드시 case GATTR_BOSS:
		case GATTR_BOSS:
		case GATTR_SUPER_GROUND_BOSS:
			{
				BM::CPacket kNPacket( PT_M_N_NFY_JOIN_INDUN_PLAYER, GroundKey() );
				kNPacket.Push( kJoinIndunList );
				::SendToResultMgr(kNPacket);
			}break;
		case GATTR_CHAOS_MISSION:
		case GATTR_MISSION:
			{
			}break;
		}

		this->InitWaitUser( kWaitList );
	}

	return true;
}

bool PgIndun::ReleaseUnit( CUnit *pkUnit, bool bRecursiveCall, bool const bSendArea )
{
	BM::CAutoMutex Lock(m_kRscMutex);

	if ( UT_PLAYER == pkUnit->UnitType() )
	{
		if ( INDUN_STATE_WAIT == GetState() ) 
		{
			ReleaseWaitUser( dynamic_cast<PgPlayer*>(pkUnit) );
		}
		if( GetAttr() == GATTR_BOSS
		||	GetAttr() == GATTR_SUPER_GROUND_BOSS )
		{
			BM::CPacket kNPacket( PT_M_N_NFY_LEAVE_INDUN_PLAYER, GroundKey() );
			kNPacket.Push( pkUnit->GetID() );
			SendToResultMgr(kNPacket);
		}
	}

	bool bResult = PgGround::ReleaseUnit( pkUnit, bRecursiveCall, bSendArea );

	if(pkUnit->IsInUnitType(UT_BOSSMONSTER))
	{
		SendMonsterCount();
	}

	return bResult;
}

void PgIndun::MapLoadComplete()
{
	BM::CAutoMutex Lock( m_kRscMutex );

	CUnit* pkUser = NULL;
	CONT_OBJECT_MGR_UNIT::iterator unit_itr;
	PgObjectMgr::GetFirstUnit(UT_PLAYER, unit_itr);
	while(( pkUser = PgObjectMgr::GetNextUnit(UT_PLAYER, unit_itr)) != NULL)
	{		
		this->SendMapLoadComplete( dynamic_cast<PgPlayer*>(pkUser) );
	}
}

void PgIndun::SendMapLoadComplete( PgPlayer *pkUser )
{
	BM::CAutoMutex Lock( m_kRscMutex );

	switch( m_eState )
	{
	case INDUN_STATE_WAIT:
		{
			ReleaseWaitUser( pkUser );
		}break;
	case INDUN_STATE_READY:
	case INDUN_STATE_PLAY:
		{
			PgGround::SendMapLoadComplete( pkUser );
		}break;
	default:
		{
			// xxxxxx 다시 돌려 보내야 한다.
		}
	}
}

/*
void PgIndun::SendMapLoadState( const BM::GUID kCharGuid, unsigned char const ucPer )
{
	SNfyMapLoad kMapLoad( kCharGuid, ucPer );
	BM::CPacket kBroadCast( PT_C_M_NFY_MAPLOAD_STATE, kMapLoad );
	Broadcast( kBroadCast, kCharGuid );
}
*/

void PgIndun::AllPlayerRegen(DWORD const dwSendFlag)
{
	PgPlayer *pkPlayer = NULL;
	CONT_OBJECT_MGR_UNIT::iterator kItor;
	PgObjectMgr::GetFirstUnit(UT_PLAYER, kItor);
	while( (pkPlayer = dynamic_cast<PgPlayer*>(PgObjectMgr::GetNextUnit(UT_PLAYER, kItor))) != NULL)
	{
		if ( pkPlayer )
		{
			pkPlayer->Alive(EALIVE_PVP,dwSendFlag);
		}
	}
}

void PgIndun::SetState( EIndunState const eState, bool bAutoChange, bool bChangeOnlyState )
{
	BM::CAutoMutex kLock(m_kRscMutex);
	if ( eState == m_eState)
	{
		return;
	}

	m_dwAutoStateRemainTime = 0;
	m_eState = eState;

	if( bChangeOnlyState )
	{
		return;
	}

	switch( m_eState )
	{
	case INDUN_STATE_OPEN:
		{
			SetAutoNextState(md_dwMaxOpenWaitingTime);//중요
		}break;
	case INDUN_STATE_WAIT:
		{
			SetAutoNextState(ms_dwMaxWaitUserWaitngTime);

			switch( GetAttr() )
			{
			case GATTR_BOSS:
			case GATTR_SUPER_GROUND_BOSS:
			case GATTR_HARDCORE_DUNGEON_BOSS:
				{
					CUnit* pkUser = NULL;
					CONT_OBJECT_MGR_UNIT::iterator kItor;
					PgObjectMgr::GetFirstUnit(UT_PLAYER, kItor);
					while ((pkUser = PgObjectMgr::GetNextUnit(UT_PLAYER, kItor)) != NULL)
					{
						// GroundDeleteEffect
						PgGround *pkGround = dynamic_cast<PgGround*>(this);
						if( pkGround )
						{
							PgAction_GroundDeleteEffect kAction(pkGround);
							kAction.DoAction(pkUser, NULL);
						}
					}
				}break;
			default:
				{
				}break;
			}
		}break;
	case INDUN_STATE_READY:
		{
			StartTime( BM::GetTime32() );

			GroundWeight( std::max<int>( static_cast<int>(PgObjectMgr::GetUnitCount(UT_PLAYER)) - 1, 0 ) );
			ActivateMonsterGenGroup( -1, false, false, m_iGroundWeight );
			ActivateMonsterGenGroup( m_iGroundWeight+1, false, false, 0 );
			PgGround::OnActivateEventMonsterGroup();

			MapLoadComplete();

			if ( !OpeningMovie() )
			{
				SetState( INDUN_STATE_PLAY );
			}
		}break;
	case INDUN_STATE_PLAY:
		{
			StartTime( BM::GetTime32() );

			switch( GetAttr() )
			{
			case GATTR_HARDCORE_DUNGEON_BOSS:
				{
					__int64 const i64NowTime = g_kEventView.GetLocalSecTime( CGameTime::DEFAULT );
					__int64 const i64RemainTime = m_kOwnerGndInfo.i64EndTime - i64NowTime;
					if ( 0i64 < i64RemainTime )
					{
						__int64 i64PlayingTime = 30i64 * CGameTime::MINUTE;
						if ( i64RemainTime < i64PlayingTime )
						{
							i64PlayingTime = i64RemainTime;
						}

						DWORD const dwEndRemainTime = static_cast<DWORD>(i64PlayingTime / CGameTime::MILLISECOND);
						__int64 const i64EndTime = i64NowTime + i64PlayingTime;

						BM::CPacket kNfyPacket( PT_M_M_NFY_HARDCORE_BOSS_ENDTIME );
						GroundKey().WriteToPacket( kNfyPacket );
						kNfyPacket.Push( i64EndTime );
						::SendToGround( m_kOwnerGndInfo.kOwnerGndKey, kNfyPacket );

						BM::CPacket kCPacket( PT_M_C_NFY_HARDCORE_BOSS_ENDTIME_IN, i64EndTime );
						Broadcast( kCPacket );

						SetAutoNextState( dwEndRemainTime );
					}
					else
					{
						// 종료되어야 한다.
						return SetState( INDUN_STATE_RESULT_WAIT );
					}
				} // braek 사용안함 다음에 반드시 case GATTR_BOSS:
			case GATTR_BOSS:
			case GATTR_SUPER_GROUND_BOSS:
				{
					CUnit* pkUser = NULL;
					CONT_OBJECT_MGR_UNIT::iterator kItor;
					PgObjectMgr::GetFirstUnit(UT_PLAYER, kItor);
					while ((pkUser = PgObjectMgr::GetNextUnit(UT_PLAYER, kItor)) != NULL)
					{
						// GroundEffect 주기
						PgGround *pkGround = dynamic_cast<PgGround*>(this);
						if( pkGround )
						{
							PgAction_GroundEffect kAction(this);
							kAction.DoAction(pkUser, NULL);
						}
					}
				}break;
			default:
				{
				}break;
			}

			SendMonsterCount();
		}break;
	case INDUN_STATE_RESULT_WAIT:
		{
			SEffectCreateInfo kCreate;
			kCreate.eType = EFFECT_TYPE_PENALTY;
			kCreate.iEffectNum = EFFECTNO_CANNOT_DAMAGE;
			kCreate.eOption = SEffectCreateInfo::ECreateOption_CallbyServer;

			CUnit* pkUser = NULL;
			CONT_OBJECT_MGR_UNIT::iterator kItor;
			PgObjectMgr::GetFirstUnit(UT_PLAYER, kItor);
			while ((pkUser = PgObjectMgr::GetNextUnit(UT_PLAYER, kItor)) != NULL)
			{
				if( false == pkUser->IsDead() )
				{
					pkUser->SetAbil( AT_HP, pkUser->GetAbil(AT_C_MAX_HP), true );
					pkUser->SetAbil( AT_MP, pkUser->GetAbil(AT_C_MAX_MP), true );
					pkUser->AddEffect( kCreate);//무적 이펙트를 걸어줌
				}
			}

			SetAutoNextState(GetResultWaitTime());
		}break;
	case INDUN_STATE_RESULT:
		{
			StartTime( BM::GetTime32() );
			switch( GetAttr() )
			{
			case GATTR_HARDCORE_DUNGEON_BOSS:
				{
					if ( !m_kOwnerGndInfo.kOwnerGndKey.IsEmpty() )
					{
						BM::CPacket kNfyPacket( PT_M_M_NFY_HARDCORE_BOSS_ENDTIME );
						GroundKey().WriteToPacket( kNfyPacket );
						kNfyPacket.Push( 0i64 );
						::SendToGround( m_kOwnerGndInfo.kOwnerGndKey, kNfyPacket );

						m_kOwnerGndInfo = SOwnerGroundInfo();// 초기화
					}
				} // braek 사용안함 다음에 반드시 case GATTR_BOSS:
			case GATTR_BOSS:
			case GATTR_SUPER_GROUND_BOSS:
				{
					size_t iPlayUserCount = 0;
					VEC_RESULTINDUN kResultList;
					CUnit* pkUser = NULL;
					CONT_OBJECT_MGR_UNIT::iterator kItor;
					PgObjectMgr::GetFirstUnit(UT_PLAYER, kItor);
					while ((pkUser = PgObjectMgr::GetNextUnit(UT_PLAYER, kItor)) != NULL)
					{
						if( false == pkUser->IsDead() )
						{
							kResultList.push_back( SNfyResultIndun(pkUser->GetID(), pkUser->GetAbil64(AT_EXPERIENCE), pkUser->GetAbil(AT_LEVEL)) );
						}

						// GroundDeleteEffect
						PgGround *pkGround = dynamic_cast<PgGround*>(this);
						if( pkGround )
						{
							PgAction_GroundDeleteEffect kAction(pkGround);
							kAction.DoAction(pkUser, NULL);
						}
						
						// 통계
						++iPlayUserCount;
					}

					BM::CPacket kNPacket( PT_M_N_NFY_RESULT_INDUN, GroundKey() );
					kNPacket.Push( kResultList );
					SendToResultMgr(kNPacket);

					BM::CPacket kWebPacket(PT_A_N_NFY_GROUND_PLAYER_COUNT_INFO);
					kWebPacket.Push( GetGroundNo() );
					kWebPacket.Push( iPlayUserCount );
					::SendToRealmContents(PMET_WEB_HELPER, kWebPacket);
				}
			default:
				{
					RemoveAllMonster();
				}break;
			}

		}break;
	}
}

void PgIndun::OnTick1s()
{
	BM::CAutoMutex Lock( m_kRscMutex );
	DWORD const dwNow = BM::GetTime32();
	DWORD dwElapsed = 0;
	bool bTimeUpdate = true;
	if( INDUN_STATE_PLAY == m_eState )
	{
		bTimeUpdate = false;
	}
	CheckTickAvailable(ETICK_INTERVAL_1S, dwNow, dwElapsed, bTimeUpdate);

	switch( m_eState )
	{
	case INDUN_STATE_WAIT:
		{
			if ( GATTR_HARDCORE_DUNGEON_BOSS == this->GetAttr() )
			{
				if ( 0i64 == m_kOwnerGndInfo.i64EndTime )
				{
					break;
				}
			}

			if ( IsAllUserSameStep(m_eOpening) )
			{
				if ( E_OPENING_READY == m_eOpening )
				{	
					m_eOpening = E_OPENING_PLAY;

					// Opening할 놈들을 찾아야 한다.
					UNIT_PTR_ARRAY kUnitArray;				
					if ( !GetHaveAbilUnitArray( kUnitArray, AT_MANUAL_OPENING, UT_MONSTER ) )
					{
						LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("GetHaveAbilUnitArray Failed!"));
						INFO_LOG(BM::LOG_LV4, __FL__<<L"Not Found Opening Unit / GroundNo["<<GetGroundNo()<<L"]" );
						m_eOpening = E_OPENING_NONE;
						SetState( INDUN_STATE_READY );
					}
					else
					{
 						BM::CPacket kOpeningAddUnitPacket(PT_M_C_ADD_UNIT);
 						kUnitArray.WriteToPacket(kOpeningAddUnitPacket,WT_SIMPLE);
 
 						CUnit* pkUser = NULL;
 						CONT_OBJECT_MGR_UNIT::iterator unit_itr;
						PgObjectMgr::GetFirstUnit(UT_PLAYER, unit_itr);
 						while(( pkUser = PgObjectMgr::GetNextUnit(UT_PLAYER, unit_itr)) != NULL)
 						{		
							pkUser->Send(kOpeningAddUnitPacket,E_SENDTYPE_SELF|E_SENDTYPE_SEND_BYFORCE);
							ReleaseWaitUser( dynamic_cast<PgPlayer*>(pkUser) );
						}

//						SetUnitAbil(kUnitArray, AT_HPMP_BROADCAST, 1);
					}

					ClearManualOpeningMonster(kUnitArray);
				}
				else
				{
					SetState( INDUN_STATE_READY );
				}
			}
		}break;
	case INDUN_STATE_READY:
		{
			if ( !OpeningMovie() )
			{
				SetState( INDUN_STATE_PLAY );
			}
		}break;
	case INDUN_STATE_PLAY:
		{
			PgGround::OnTick1s();
		}break;
	case INDUN_STATE_RESULT_WAIT:
		{

		}break;
	case INDUN_STATE_RESULT:
		{
			PgPlayer* pkPlayer = NULL;
			CONT_OBJECT_MGR_UNIT::iterator kItor;
			PgObjectMgr::GetFirstUnit(UT_PLAYER, kItor);
			while ((pkPlayer = dynamic_cast<PgPlayer*> (PgObjectMgr::GetNextUnit(UT_PLAYER, kItor))) != NULL)
			{
				if (pkPlayer->IsDead())
				{
					if ( DifftimeGetTime( pkPlayer->DeathTime(), dwNow ) >= this->GetDeathDelayTime() )
					{
						BM::CPacket kNfyPacket(PT_C_M_NFY_RECENT_MAP_MOVE);
						pkPlayer->VNotify(&kNfyPacket);
					}
				}
			}
		}break;
	case INDUN_STATE_CLOSE:
		{
			if( 0 != GetResultMoveMapNum())
			{
				SReqMapMove_MT kRMM(MMET_None);
				kRMM.kTargetKey.GroundNo(GetResultMoveMapNum());
				kRMM.nTargetPortal = 1;
				PgReqMapMove kMapMove( this, kRMM, NULL );

				bool bRun = false;
				PgPlayer* pkPlayer = NULL;
				CONT_OBJECT_MGR_UNIT::iterator kItor;
				PgObjectMgr::GetFirstUnit(UT_PLAYER, kItor);
				while ((pkPlayer = dynamic_cast<PgPlayer*> (PgObjectMgr::GetNextUnit(UT_PLAYER, kItor))) != NULL)
				{
					if ( kMapMove.Add( pkPlayer ) )
					{
						bRun = true;
					}
				}

				if( bRun )
				{
					kMapMove.DoAction();
				}
			}
			else
			{
				// 한명씩 쫒아 낸다.
				PgPlayer* pkPlayer = NULL;
				CONT_OBJECT_MGR_UNIT::iterator kItor;
				PgObjectMgr::GetFirstUnit(UT_PLAYER, kItor);
				while ((pkPlayer = dynamic_cast<PgPlayer*> (PgObjectMgr::GetNextUnit(UT_PLAYER, kItor))) != NULL)
				{
					if ( this->RecvRecentMapMove(pkPlayer) )
					{
						break;
					}
				}
			}
		}break;
	}

	UpdateAutoNextState( dwNow, dwElapsed );
}

void PgIndun::OnTick5s()
{
	BM::CAutoMutex Lock(m_kRscMutex);
	DWORD const dwNow = BM::GetTime32();
	DWORD dwkElapsed = 0;
	CheckTickAvailable(ETICK_INTERVAL_5S, dwNow, dwkElapsed, true);

	if( !(m_eState & (INDUN_STATE_RESULT|INDUN_STATE_RESULT_WAIT)) && BM::TimeCheck(m_kLastGenCheckTime, 6000))
	{
		RareMonsterGenerate();
		MonsterGenerate(m_kContGenPoint_Monster);
		ObjectUnitGenerate(m_kContGenPoint_Object);
	}

	DWORD const dwServerElapsedTime = g_kEventView.GetServerElapsedTime();

	PgPlayer* pkPlayer = NULL;
	CONT_OBJECT_MGR_UNIT::iterator kItor;
	PgObjectMgr::GetFirstUnit(UT_PLAYER, kItor);
	while ((pkPlayer = dynamic_cast<PgPlayer*> (PgObjectMgr::GetNextUnit(UT_PLAYER, kItor))) != NULL)
	{
		pkPlayer->Update(dwNow);

		if ( !pkPlayer->IsMapLoading() )
		{
			OnTick_AlramMission( pkPlayer, dwServerElapsedTime );
		}
	}
}

HRESULT PgIndun::SetUnitDropItem(CUnit *pkOwner, CUnit *pkDroper, PgLogCont &kLogCont )
{
	if ( INDUN_STATE_PLAY != m_eState )
	{// 그냥 S_OK로 리턴~
		return S_OK;
	}

	//Item & Gold
	switch( pkDroper->GetAbil(AT_GRADE) )
	{
	case EMGRADE_BOSS:
		{
			SNfyResultItemList kResultItemList( pkDroper->GetAbil(AT_MAX_DROP_ITEM_COUNT) );

			CONT_DEF_MAP_ITEM_BAG const *pkContMapItemBag = NULL;
			g_kTblDataMgr.GetContDef(pkContMapItemBag);

			CONT_DEF_MAP_ITEM_BAG::const_iterator map_item_bag_Itr = pkContMapItemBag->find( GetMapItemBagGroundNo() );
			
			CUnit *pkUser = NULL;
			CONT_OBJECT_MGR_UNIT::iterator unit_itr;
			PgObjectMgr::GetFirstUnit(UT_PLAYER, unit_itr);
			while((pkUser = PgObjectMgr::GetNextUnit(UT_PLAYER, unit_itr)) != NULL)
			{
				SNfyResultItem kUserItem( pkUser->GetID() );

				if( map_item_bag_Itr != pkContMapItemBag->end() )
				{
					PgPlayer *pkPlayer = dynamic_cast<PgPlayer*>(pkUser);
					if( pkPlayer && true == pkPlayer->IsTakeUpItem() )
					{
						PgAction_DropItemBox kDropItem( this, map_item_bag_Itr->second );
						if ( true == kDropItem.DoAction( pkDroper, pkUser ) )
						{
							CONT_MONSTER_DROP_ITEM::const_iterator bag_itr = kDropItem.m_kContDropItem.begin();
							for( ; bag_itr != kDropItem.m_kContDropItem.end() ; ++bag_itr )
							{
								if( bag_itr->ItemNo() )
								{
									kUserItem.kItemList.push_back( *bag_itr );
								}
							}
						}
					}
				}

				kResultItemList.push_back( kUserItem );
			}
			
			SendResultItem( kResultItemList );
			SetState(INDUN_STATE_RESULT_WAIT);
		}break;
	case EMGRADE_ELITE:
		{
			int const iMoveMapNum = pkDroper->GetAbil(AT_MON_RESULT_MOVE_MAP_NUM);
			if( 0 != iMoveMapNum )
			{
				SetResultMoveMapNum(iMoveMapNum);
				SetState(INDUN_STATE_RESULT, false, true);
				int iMoveMapTime = pkDroper->GetAbil(AT_MON_RESULT_MOVE_MAP_TIME);
				if( iMoveMapTime <= 0 )// 시간이 없으면 안된다, 디폴트 1초로.
				{
					iMoveMapTime = 1000;
				}
				SetAutoNextState(iMoveMapTime);
			}

			HRESULT kRet = PgGround::SetUnitDropItem(pkOwner, pkDroper, kLogCont );// 아이템 드랍은 해야지.
			int const iKill = pkDroper->GetAbil(AT_MON_RESULT_KILL_ALL);
			if((GetAttr()==GATTR_BOSS && iMoveMapNum) || iKill)
			{
				return S_FALSE;//가고일 같은 경우 미션맵은 아니지만 몹들을 다 죽여줘야 한다
			}
			return kRet;

		}break;
	default:
		{
			return PgGround::SetUnitDropItem(pkOwner, pkDroper, kLogCont );
		}break;
	}
	return S_OK;
}

void PgIndun::SendResultItem( SNfyResultItemList& rkResultItem )
{
	BM::CPacket kNPacket( PT_M_N_NFY_RESULTITEM_INDUN, GroundKey() );
	rkResultItem.WriteFromPacket( kNPacket );
	SendToResultMgr( kNPacket );
}


void PgIndun::ClearManualOpeningMonster(UNIT_PTR_ARRAY& rkUnitArray)
{
	UNIT_PTR_ARRAY::iterator unit_itr;
	for( unit_itr=rkUnitArray.begin(); unit_itr!=rkUnitArray.end(); ++unit_itr )
	{
		if ( unit_itr->pkUnit )
		{
			unit_itr->pkUnit->ActionID(0);
		}
	}
}

void PgIndun::SetUnitAbil(UNIT_PTR_ARRAY &rkUnitArray, WORD const wType, int const iValue)
{
	UNIT_PTR_ARRAY::iterator unit_itr;
	for( unit_itr=rkUnitArray.begin(); unit_itr!=rkUnitArray.end(); ++unit_itr )
	{
		if ( unit_itr->pkUnit )
		{
			unit_itr->pkUnit->SetAbil(wType, iValue);
		}
	}
}

DWORD PgIndun::GetResultWaitTime()
{	
	int const iTime = GetMapAbil(AT_RESULT_WAIT_TIME); 
	return 0<iTime ? iTime : 5000;
}