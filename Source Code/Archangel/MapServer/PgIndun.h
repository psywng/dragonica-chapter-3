#ifndef MAP_MAPSERVER_MAP_GROUND_PGINDUN_H
#define MAP_MAPSERVER_MAP_GROUND_PGINDUN_H

#include "Lohengrin/PacketStruct4Map.h"
#include "PgGround.h"

typedef struct tagMapWaitUser
{
	tagMapWaitUser( const BM::GUID kCharacterGuid, const EOpeningState _kStep=E_OPENING_NONE,unsigned char per=0 )
		:	kCharGuid( kCharacterGuid )
		,	ucPer( per )
		,	kStep( _kStep )
	{}

	bool operator<(const tagMapWaitUser& rhs)const
	{
		return kCharGuid < rhs.kCharGuid;
	}

	bool operator<(BM::GUID const & rkCharGuid)const
	{
		return kCharGuid < rkCharGuid;
	}

	bool operator==( const tagMapWaitUser& kPair ) const
	{
		return kCharGuid == kPair.kCharGuid;
	}

	bool operator==( BM::GUID const & rkCharGuid )const
	{
		return kCharGuid == rkCharGuid;
	}

	const BM::GUID kCharGuid;
	unsigned char ucPer;
	EOpeningState kStep;
} SWaitUser;

typedef struct tagOwnerGroundInfo
{
	tagOwnerGroundInfo()
	{}

	SGroundKey	kOwnerGndKey;
	__int64		i64EndTime;
}SOwnerGroundInfo;

class PgIndun 
	: public PgGround
{
public:

	static DWORD const md_dwMaxOpenWaitingTime		= 1000*60*10;	//10분
	static DWORD const ms_dwMaxWaitUserWaitngTime	= 1000*60*3;	//맵로딩 기다려 주는 시간 : 최대3분까지만 기다려 준다)
	typedef std::set<SWaitUser>						ConWaitUser;

public:
	PgIndun();
	virtual ~PgIndun();

	virtual EOpeningState Init( int const iMonsterControlID=0, bool const bMonsterGen=true );
	virtual void Clear();
	bool Clone( PgIndun* pkIndun );
	virtual bool Clone( PgGround* pkGround );

	// Ground Manager의 OnTimer에서 호출(리턴값이 true이면 그라운드가 삭제된다.)
	virtual bool IsDeleteTime()const;

	virtual void OnTick1s();
	virtual void OnTick5s();

	virtual EGroundKind GetKind()const{	return GKIND_INDUN;	}
	virtual bool ReleaseUnit( CUnit *pkUnit, bool bRecursiveCall=false, bool const bSendArea=true );

	virtual EIndunState GetState()const{return m_eState;}
	virtual int GetGroundNo()const { return GroundKey().GroundNo();}; //현재 그라운드 번호

public:
	virtual void SetState( EIndunState const eState, bool bAutoChange=false, bool bChangeOnlyState=false);

protected:
	virtual HRESULT SetUnitDropItem(CUnit *pkOwner, CUnit *pkDroper, PgLogCont &kLogCont );
	void AllPlayerRegen(DWORD const dwSendFlag=E_SENDTYPE_BROADCAST_GROUND);

protected:
	virtual bool VUpdate( CUnit* pkUnit, WORD const wType, BM::CPacket* pkNfy );
	virtual bool RecvGndWrapped( unsigned short usType, BM::CPacket* const pkPacket );
	virtual bool RecvGndWrapped_ItemPacket(unsigned short usType, BM::CPacket* const pkPacket );

	// User Loading에 관한
	virtual void InitWaitUser( VEC_GUID& rkCharGuidList );
	virtual bool AddWaitUser( BM::GUID const & rkCharGuid );
	virtual HRESULT	ReleaseWaitUser( PgPlayer *pkUser );
	virtual bool IsAllUserSameStep(EOpeningState const eState=E_OPENING_NONE)const;

	virtual bool RecvMapMove( UNIT_PTR_ARRAY &rkUnitArray, SReqMapMove_MT& rkRMM, CONT_PET_MAPMOVE_DATA &kContPetMapMoveData, CONT_PLAYER_MODIFY_ORDER const &kModifyOrder );
	virtual void SendMapLoadComplete( PgPlayer *pkUser );
	void MapLoadComplete();

	void SetAutoNextState( DWORD const dwAutoNextStateTime ){m_dwAutoStateRemainTime = dwAutoNextStateTime;}
	void UpdateAutoNextState(DWORD const dwNow, DWORD const dwElapsedTime);
	
	// Result Item 전송
	void SendResultItem( SNfyResultItemList& rkResultItem );

	// Death Delay Time
	virtual DWORD GetDeathDelayTime()const{	return 5000;	}
	virtual DWORD GetResultWaitTime();

	virtual bool IsMonsterTickOK(){return m_eState==INDUN_STATE_PLAY;}
	static void SetUnitAbil(UNIT_PTR_ARRAY &rkUnitArray, WORD const wType, int const iValue);

	virtual bool IsAlramMission(void)const{return ((NULL != m_pkAlramMissionMgr) && (INDUN_STATE_PLAY == m_eState));}

	// Opening 연출
private:
	// 이 함수는 특정 예외 상황에서 호출되는 함수이다.(잘모르면 함부로 건들지 말자)
	void ClearManualOpeningMonster(UNIT_PTR_ARRAY& rkUnitArray);

	CLASS_DECLARATION( DWORD, m_dwStartTime, StartTime );	// 실제 인던의 상태가 Play가 되기 시작한 시간

protected:
	ConWaitUser			m_kWaitUserList;
	EIndunState			m_eState;

	DWORD				m_dwAutoStateRemainTime;
	EOpeningState		m_eOpening;

	SOwnerGroundInfo	m_kOwnerGndInfo;

// 특정 옵션
public:

	virtual bool IsGroundEffect()const{return m_bUseGroundEffect;}
	virtual bool IsUseItem()const{return m_bUseItem;}

	virtual void SetResultMoveMapNum(int iNum) { m_iResultMoveMapNum = iNum; }
	virtual int	 GetResultMoveMapNum() { return m_iResultMoveMapNum; }

protected:
	bool			m_bUseGroundEffect;
	bool			m_bUseItem;

	int				m_iResultMoveMapNum;
};

#include "PgIndun.inl"

#endif // MAP_MAPSERVER_MAP_GROUND_PGINDUN_H