#ifndef MAP_MAPSERVER_NETWORK_PGSENDWRAPPER_H
#define MAP_MAPSERVER_NETWORK_PGSENDWRAPPER_H

#include "Lohengrin/packetstruct2.h"

extern HRESULT SetSendWrapper(SERVER_IDENTITY const &kRecvSI);

extern bool SendToServer(SERVER_IDENTITY const &kSI, BM::CPacket const &rkPacket);
extern bool SendToLog(BM::CPacket const &rkPacket);
extern bool SendToCenter(BM::CPacket const &rkPacket);
extern bool SendToItem(SGroundKey const &kGndKey, BM::CPacket const &rkPacket);
extern bool SendToRealmContents( EContentsMessageType eType, BM::CPacket const &rkPacket );
extern bool SendToGround(SGroundKey const &kGndKey, BM::CPacket const &rkPacket);
extern bool SendToClient( BM::GUID const &kMemberGuid, BM::CPacket const &kPacket);
extern bool SendToContents( BM::CPacket const &rkPacket );
extern bool SendToChannelContents( EContentsMessageType eType, BM::CPacket const &rkPacket, int const iSecondType = 0 );

extern bool SendToGuildMgr( BM::CPacket const &rkPacket );
extern bool SendToCoupleMgr(BM::CPacket const &rkPacket );
extern bool SendToChannelChatMgr(BM::CPacket const &rkPacket);
extern bool SendToRealmChatMgr(BM::CPacket const &rkPacket);
extern bool SendToGlobalPartyMgr( BM::CPacket const &rkPacket );
extern bool SendToMissionMgr( BM::CPacket const &rkPacket );
extern bool SendToResultMgr( BM::CPacket const &rkPacket );
extern bool SendToFriendMgr( BM::CPacket const &rkPacket );
extern bool SendToCouponEventView_Map(BM::CPacket const rkPacket);
extern bool SendToRealmContents(BM::CPacket const &rkPacket);
extern bool SendToRankMgr(BM::CPacket const &rkPacket);
extern bool SendToOXQuizEvent(BM::CPacket const &rkPacket);
extern bool SendToLuckyStarEvent(BM::CPacket const &rkPacket);
extern bool SendToPvPLobby(BM::CPacket const &kPacket, int const iLobbyID);
extern bool SendToPvPRoom( int const iLobbyID, int const iRoomIndex, BM::CPacket const &rkPacket );
extern bool SendToHardCoreDungeonMgr( BM::CPacket const &rkPacket );
extern bool SendToMyhomeMgr(BM::CPacket const &rkPacket);

#endif // MAP_MAPSERVER_NETWORK_PGSENDWRAPPER_H