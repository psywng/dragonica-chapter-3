#pragma once

#include "PgPvPType_DeathMatch.h"
#include "PgPvPModeStruct.h"

struct SHoldRet
{
	SHoldRet()
		:	dwHoldTime(0)
	{}

	BM::GUID	kCharGuid;
	DWORD		dwHoldTime;
};

class PgPvPHillStatus
{
public:
	PgPvPHillStatus( SPvPHillNode const &rkNode );
	~PgPvPHillStatus();

	HRESULT OnHill( CUnit *pkUnit, SHoldRet &kOldRet );
	HRESULT OutHill( CUnit *pkUnit, SHoldRet &kOldRet );

	void Hold( bool bHold );
	bool Hold()const{return m_bHold;}

	bool Refresh();
	bool GetHoldRet( DWORD const dwCurTime, SHoldRet &kRet );
	int	GetHoldTeam()const{return m_iHoldTeam;}

protected:
	SPvPHillNode		m_kContNode;
	CONT_PVPHILL_ITEM	m_kContHoldUnitInfo;
	DWORD				m_dwLastHoldTime;
	bool				m_bHold;
	int					m_iHoldTeam;
};


class PgPvPMode_KingOfHill
	:	public PgPvPType_DeathMatch
{
public:
	typedef std::map<size_t,PgPvPHillStatus>	CONT_PVPHILL_STATUS;

public:
	PgPvPMode_KingOfHill();
	virtual ~PgPvPMode_KingOfHill();

	virtual HRESULT Exit( CUnit *pkPlayer );

	virtual EPVPTYPE Type()const{return PVP_TYPE_KTH;}
	virtual HRESULT Init( CEL::CPacket &kPacket, PgWarGround* pkGnd );

	virtual HRESULT Update();

	// Event
	virtual HRESULT SetEvent_Kill( CUnit *pkUnit, CUnit *pkKiller );
	virtual HRESULT SetEvent_HillUp( CUnit *pkUnit, size_t const iHillNo );
	virtual HRESULT SetEvent_HillOut( CUnit *pkUnit );

protected:
	bool UpdateHoldResult( SHoldRet const &kRet );

	CONT_PVPHILL_STATUS				m_kContHill;
	CONT_PVPHILL_STATUS::iterator	m_itrNowHill;
	DWORD							m_dwNextChangeTime;
};