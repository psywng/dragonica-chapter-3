#ifndef MAP_MAPSERVER_MAP_GROUND_PUBLIC_PGWARGROUND_H
#define MAP_MAPSERVER_MAP_GROUND_PUBLIC_PGWARGROUND_H

#include "PgIndun.h"
#include "PgWarMode.h"

class PgWarGround
	:	public PgIndun
{
public:
	PgWarGround();
	virtual ~PgWarGround();

	virtual EOpeningState Init( int const iMonsterControlID=0, bool const bMonsterGen=true );
	virtual void Clear();

	bool Clone( PgWarGround* pkIndun );
	virtual bool Clone( PgGround* pkGround );

	// Ground Manager의 OnTimer에서 호출(리턴값이 true이면 그라운드가 삭제된다.)
	virtual bool IsDeleteTime()const;

	virtual EGroundKind GetKind()const{	return GKIND_WAR;	}
	virtual bool ReleaseUnit( CUnit *pkUnit, bool bRecursiveCall=false, bool const bSendArea=true );

	virtual void OnTick1s();

protected:
//NetWork
	virtual void VUpdate( BM::CSubject< BM::CPacket* > * const pChangedSubject, BM::CPacket* iNotifyCause );
	virtual bool VUpdate( CUnit* pkUnit, WORD const wType, BM::CPacket* pkNfy );
	virtual bool RecvGndWrapped( unsigned short usType, BM::CPacket* const pkPacket );
	virtual void RecvUnitDie(CUnit *pkUnit);
	virtual bool RecvGndWrapped_ItemPacket(unsigned short usType, BM::CPacket* const pkPacket );

	virtual void RecvMapMoveComeFailed( BM::GUID const &kCharGuid );
	virtual bool AdjustArea( CUnit *pkUnit, bool const bIsSendAreaData, bool const bIsCheckPos );

protected:
	virtual bool SaveUnit( CUnit *pkUnit, SReqSwitchReserveMember const *pRSRM = NULL );

	virtual bool IsAccess( PgPlayer *pkPlayer );
	virtual HRESULT SetUnitDropItem( CUnit *pkOwner, CUnit *pkDroper, PgLogCont &kLogCont );

	virtual bool IsDeathPenalty()const{return false;}

public:
	virtual bool IsDecEquipDuration()const{return false;}

//	인던 상속 함수
	virtual void SetState( EIndunState const eState, bool bAutoChange=false, bool bChangeOnlyState=false);
	virtual void SendMapLoadComplete( PgPlayer *pkUser );

//	그라운드 상속 함수
	void CallAlramReward( PgPlayer * pkPlayer );

protected:
	virtual void GMCommand_RecvGamePoint( PgPlayer *pkPlayer, int const iPoint );

// 전용함수
public:
	void Cancel();
	HRESULT Ready( BM::CPacket& kPacket );

protected:
	bool IsModeType( EPVPTYPE const kChkType )const;// War Ground가 PvP모드냐 다른거냐(공성전등)

	void RecvPvPReward( BM::CPacket& kPacket );

protected:
	PgWarMode			*m_pkMode;
};

inline bool PgWarGround::IsModeType( EPVPTYPE const kChkType )const
{
	return m_pkMode && (m_pkMode->Type() & kChkType);
}

#endif // MAP_MAPSERVER_MAP_GROUND_PUBLIC_PGWARGROUND_H