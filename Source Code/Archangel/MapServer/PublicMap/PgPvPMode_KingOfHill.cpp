#include "stdafx.h"
#include "PgWarGround.h"
#include "PgPvPMode_KingOfHill.h"

PgPvPHillStatus::PgPvPHillStatus( SPvPHillNode const &rkNode )
:	m_kContNode(rkNode)
,	m_dwLastHoldTime(0)
,	m_bHold(false)
,	m_iHoldTeam(TEAM_NONE)
{
}

PgPvPHillStatus::~PgPvPHillStatus()
{

}

bool PgPvPHillStatus::Refresh()
{
	int const iHoldTeam = m_iHoldTeam;
	m_iHoldTeam = TEAM_NONE;

	if ( !m_kContHoldUnitInfo.empty() )
	{
		CONT_PVPHILL_ITEM::const_iterator hill_itr = m_kContHoldUnitInfo.begin();
		m_iHoldTeam = hill_itr->iTeam;
		while ( ++hill_itr != m_kContHoldUnitInfo.end() )
		{
			if ( m_iHoldTeam != hill_itr->iTeam )
			{
				m_iHoldTeam = TEAM_NONE;
			}
		}
	}

	return m_iHoldTeam != iHoldTeam;
}

void PgPvPHillStatus::Hold( bool bHold )
{
	if ( m_bHold != bHold )
	{
		m_bHold = bHold;
		if ( m_bHold )
		{
			m_dwLastHoldTime = ::timeGetTime();
		}
	}
}

bool PgPvPHillStatus::GetHoldRet( DWORD const dwCurTime, SHoldRet &kRet )
{
	if ( m_kContHoldUnitInfo.empty() || (m_iHoldTeam == TEAM_NONE) )
	{
		return false;
	}

	kRet.kCharGuid = m_kContHoldUnitInfo.begin()->kCharGuid;
	kRet.dwHoldTime = DifftimeGetTime( m_dwLastHoldTime, dwCurTime );
	m_dwLastHoldTime = dwCurTime;
	return true;
}

HRESULT PgPvPHillStatus::OnHill( CUnit *pkUnit, SHoldRet &kOldRet )
{
	if ( !m_kContNode.InPos(pkUnit->GetPos() ) )
	{
		return E_FAIL;
	}

	CONT_PVPHILL_ITEM::iterator hill_itr = std::find( m_kContHoldUnitInfo.begin(), m_kContHoldUnitInfo.end(), pkUnit );
	if ( hill_itr == m_kContHoldUnitInfo.end() )
	{
		m_kContHoldUnitInfo.push_back( SPvPHillItem(pkUnit) );
		if ( Refresh() && m_bHold )
		{
			DWORD const dwCurTime = ::timeGetTime();
			if ( m_kContHoldUnitInfo.size() > 1 )
			{
				kOldRet.kCharGuid = m_kContHoldUnitInfo.begin()->kCharGuid;
				kOldRet.dwHoldTime = DifftimeGetTime( m_dwLastHoldTime, dwCurTime );
			}
			m_dwLastHoldTime = dwCurTime;
			return S_OK;
		}
	}
	return S_FALSE;
}

HRESULT PgPvPHillStatus::OutHill( CUnit *pkUnit, SHoldRet &kOldRet )
{
	CONT_PVPHILL_ITEM::iterator hill_itr = std::find( m_kContHoldUnitInfo.begin(), m_kContHoldUnitInfo.end(), pkUnit );
	if ( hill_itr != m_kContHoldUnitInfo.end() )
	{
		DWORD const dwCurTime = ::timeGetTime();
		if ( m_bHold && (m_iHoldTeam != TEAM_NONE) )
		{
			if ( hill_itr == m_kContHoldUnitInfo.begin() )
			{// 이전에 점령하고 있던 놈이라면
				kOldRet.kCharGuid = hill_itr->kCharGuid;
				kOldRet.dwHoldTime = DifftimeGetTime( m_dwLastHoldTime, dwCurTime );
				m_dwLastHoldTime = dwCurTime;
			}
		}

		m_kContHoldUnitInfo.erase( hill_itr );
		if ( Refresh() )
		{
			m_dwLastHoldTime = dwCurTime;
			return S_OK;
		}
		return S_FALSE;
	}
	return E_FAIL;
}

// 
PgPvPMode_KingOfHill::PgPvPMode_KingOfHill()
:	m_itrNowHill(m_kContHill.begin())
,	m_dwNextChangeTime(0)
{

}

PgPvPMode_KingOfHill::~PgPvPMode_KingOfHill()
{

}

HRESULT PgPvPMode_KingOfHill::Exit( CUnit *pkPlayer )
{
	if ( pkPlayer )
	{
		CONT_PVP_GAME_USER::iterator user_itr = m_kContPlay.find(pkPlayer->GetID());
		if ( user_itr != m_kContPlay.end() )
		{
			if ( user_itr->second.iHillNo )
			{
				SetEvent_HillOut( pkPlayer );
			}
			m_kContPlay.erase(user_itr);
			RefreshUserCount();
			return S_OK;
		}
		m_kContReady.erase( pkPlayer->GetID() );
	}
	return E_FAIL;
}

HRESULT PgPvPMode_KingOfHill::Init( CEL::CPacket &kPacket, PgWarGround* pkGnd )
{
	if ( SUCCEEDED( PgPvPType_DeathMatch::Init(kPacket, pkGnd ) ) )
	{
		m_kContHill.clear();
		CONT_PVPHILL const &kContPvPHill = pkGnd->GetContPvPHill();
		if ( kContPvPHill.size() >= 2 )
		{
			CONT_PVPHILL::const_iterator hill_itr;
			for ( hill_itr=kContPvPHill.begin(); hill_itr!=kContPvPHill.end(); ++hill_itr )
			{
				m_kContHill.insert( std::make_pair( hill_itr->iNo, CONT_PVPHILL_STATUS::mapped_type(*hill_itr)) );
				m_dwNextChangeTime = ::timeGetTime();
			}
			m_itrNowHill = m_kContHill.begin();
			return S_OK;
		}
	}

	VERIFY_INFO_LOG( false, BM::LOG_LV4, _T("[%s] Error"), __FUNCTIONW__ );
	return E_FAIL;
}

HRESULT PgPvPMode_KingOfHill::Update()
{
	if ( SUCCEEDED( PgPvPType_DeathMatch::Update() ) )
	{
		DWORD const dwCurTime = ::timeGetTime();
		SHoldRet kRet;
		if ( m_itrNowHill->second.GetHoldRet( dwCurTime, kRet ) )
		{
			UpdateHoldResult(kRet);
		}		

		if ( m_dwNextChangeTime < dwCurTime )
		{
			m_itrNowHill->second.Hold(false);
			if ( ++m_itrNowHill == m_kContHill.end() )
			{
				m_itrNowHill = m_kContHill.begin();
			}
			m_itrNowHill->second.Hold(true);
			CEL::CPacket kBroadCastPacket( PT_PM_C_NFY_HILL_CHANGE, m_itrNowHill->first );
			kBroadCastPacket.Push(m_itrNowHill->second.GetHoldTeam());
			VNotify( &kBroadCastPacket );

			m_dwNextChangeTime = dwCurTime + (DWORD)BM::Rand_Range( 15000, 45000 );
		}
		return S_OK;
	}
	return E_FAIL;
}

HRESULT PgPvPMode_KingOfHill::SetEvent_Kill( CUnit *pkUnit, CUnit *pkKiller )
{
	static WORD usPoint = 50;
	if ( pkUnit )
	{
		pkUnit->SetState(US_DEADREADY);
		pkUnit->DeathTime(::timeGetTime());

		CONT_PVP_GAME_USER::iterator itr = m_kContPlay.find(pkUnit->GetID());
		if ( itr != m_kContPlay.end() )
		{
			itr->second.kResult.SetDie();
			SetEventTeam_Die( itr->second.kTeamSlot.GetTeam(), 0 );

			if ( itr->second.iHillNo )
			{
				SetEvent_HillOut( pkUnit );
			}
		}

		if ( pkKiller )
		{
			CONT_PVP_GAME_USER::iterator itr = m_kContPlay.find(pkKiller->GetID());
			if ( itr != m_kContPlay.end() )
			{
				itr->second.kResult.SetKill(usPoint);
				SetEventTeam_Kill( itr->second.kTeamSlot.GetTeam(), usPoint );
			}

			CEL::CPacket kPacket(PT_M_C_NFY_GAME_EVENT_KILL);
			kPacket.Push(pkUnit->GetID());
			kPacket.Push(pkKiller->GetID());
			kPacket.Push(usPoint);
			VNotify(&kPacket);
			return S_OK;
		}
	}
	return E_FAIL;
}

bool PgPvPMode_KingOfHill::UpdateHoldResult( SHoldRet const &kRet )
{
	CONT_PVP_GAME_USER::iterator oldret_itr = m_kContPlay.find( kRet.kCharGuid );
	if ( oldret_itr != m_kContPlay.end() )
	{
		WORD usAddPoint = (WORD)(kRet.dwHoldTime/200);//1초당 5점
		oldret_itr->second.kResult.usPoint += usAddPoint;
		
		SPvPTeamResult *pkRet = GetTeamResult( oldret_itr->second.kTeamSlot.GetTeam() );
		if ( pkRet )
		{
			pkRet->usPoint += usAddPoint;
		}

		CEL::CPacket kSyncPacket( PT_PM_C_NFY_ADD_POINT_USER, kRet.kCharGuid );
		kSyncPacket.Push( usAddPoint );
		VNotify( &kSyncPacket );
		return true;
	}
	return false;
}

HRESULT PgPvPMode_KingOfHill::SetEvent_HillUp( CUnit *pkUnit, size_t const iHillNo )
{
	if ( pkUnit )
	{
		CONT_PVPHILL_STATUS::iterator hill_itr = m_kContHill.find( iHillNo );
		if ( hill_itr != m_kContHill.end() )
		{
			CONT_PVP_GAME_USER::iterator itr = m_kContPlay.find(pkUnit->GetID());
			if ( itr != m_kContPlay.end() )
			{
				SHoldRet kOldRet;
				switch( hill_itr->second.OnHill( pkUnit, kOldRet ) )
				{
				case S_OK:
					{
						CEL::CPacket kPacket( PT_PM_C_NFY_HILL_TEAM, m_itrNowHill->second.GetHoldTeam() );
						VNotify( &kPacket );

						UpdateHoldResult( kOldRet );
					} // No Break;
				case S_FALSE:
					{
						if ( itr->second.iHillNo )
						{// HillNo가 있으면 이건 머냐...
							if ( itr->second.iHillNo != iHillNo )
							{
								SetEvent_HillOut( pkUnit );
							}
						}
						itr->second.iHillNo = iHillNo;
					}break;
				case E_FAIL:
					{
						pkUnit->Send( CEL::CPacket(PT_PM_C_ANS_HILL_UP_ERROR, iHillNo), E_SENDTYPE_SELF );
						return E_ACCESSDENIED;
					}break;
				}
			}
			return S_OK;
		}
	}
	return E_FAIL;
}

HRESULT PgPvPMode_KingOfHill::SetEvent_HillOut( CUnit *pkUnit )
{
	if ( pkUnit )
	{
		CONT_PVP_GAME_USER::iterator itr = m_kContPlay.find(pkUnit->GetID());
		if ( itr != m_kContPlay.end() )
		{
			CONT_PVPHILL_STATUS::iterator hill_itr = m_kContHill.find( itr->second.iHillNo );
			if ( hill_itr != m_kContHill.end() )
			{
				SHoldRet kOldRet;
				switch( hill_itr->second.OutHill( pkUnit, kOldRet ) )
				{
				case S_OK:
					{
						CEL::CPacket kPacket( PT_PM_C_NFY_HILL_TEAM, m_itrNowHill->second.GetHoldTeam() );
						VNotify( &kPacket );

						UpdateHoldResult( kOldRet );
					} // No Break;
				case S_FALSE:
					{
						itr->second.iHillNo = 0;
						if ( kOldRet.kCharGuid == pkUnit->GetID() )
						{

						}
					}break;
				case E_FAIL:
					{
						return E_FAIL;
					}break;
				}
			}
			return S_OK;
		}
	}
	return E_FAIL;
}