#ifndef MAP_MAPSERVER_FRAMEWORK_PGKEYEVENT_H
#define MAP_MAPSERVER_FRAMEWORK_PGKEYEVENT_H

extern bool RegistKeyEvent();
extern bool CALLBACK OnTerminateServer(WORD const& rkInputKey);

#endif // MAP_MAPSERVER_FRAMEWORK_PGKEYEVENT_H