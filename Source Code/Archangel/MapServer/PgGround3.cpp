#include "stdafx.h"
#include "Variant/AlramMissionMgr.h"
#include "PgAction.h"
#include "PgGround.h"

void PgGround::OnTick_AlramMission( PgPlayer * pkPlayer, DWORD const dwCurTime )
{
	using namespace ALRAM_MISSION;
	if ( true == this->IsAlramMission() )
	{
		PgAlramMission &rkAlramMission = pkPlayer->GetAlramMission();
		switch ( rkAlramMission.OnTick( dwCurTime ) )
		{
		case RET_SUCCESS:
			{
				BM::CPacket kEventPacket( PT_M_C_NFY_ALRAMMISSION_SUCCESS );
				pkPlayer->Send( kEventPacket, E_SENDTYPE_SELF|E_SENDTYPE_MUSTSEND );

				this->CallAlramReward( pkPlayer );
			}break;
		case RET_NEED_NEWACTION:
			{
				if ( SUCCEEDED(m_pkAlramMissionMgr->GetNewAction(pkPlayer->GetAbil(AT_CLASS), rkAlramMission)) )
				{
					// 통보 해주어야 한다.
					BM::CPacket kNewAlraMPacket( PT_M_C_NFY_ALRAMMISSION_BEGIN, rkAlramMission.GetID() );
					kNewAlraMPacket.Push( rkAlramMission.GetBeginTime() );
					pkPlayer->Send( kNewAlraMPacket, E_SENDTYPE_SELF|E_SENDTYPE_SEND_BYFORCE );
				}
			}break;
		case RET_TIMEOVER:
			{
				BM::CPacket kFaildPacket( PT_M_C_NFY_ALRAMMISSION_END );
				pkPlayer->Send( kFaildPacket, E_SENDTYPE_SELF|E_SENDTYPE_MUSTSEND );
			}break;
		default:
			{
			}break;
		}
	}
}

void PgGround::CallAlramReward( PgPlayer * pkPlayer )
{
	BM::CAutoMutex kLock(m_kRscMutex);

	PgAlramMission &rkAlramMission = pkPlayer->GetAlramMission();

	TBL_DEF_ALRAM_MISSION const * const pkDef = rkAlramMission.GetDef();
	if ( pkDef )
	{
		int const iLevel = pkPlayer->GetAbil(AT_LEVEL);

		if ( pkDef->iExp )
		{
			PgAction_AddExp kAddPlayerExpAction( GroundKey(), static_cast<__int64>(pkDef->iExp), AEC_AlramMission, this, 0 );
			kAddPlayerExpAction.DoAction( pkPlayer, NULL );
		}

		if ( pkDef->iEffect )
		{
			SActArg kArg;
			kArg.Set(ACTARG_GROUND, this);
			pkPlayer->AddEffect( pkDef->iEffect, 0, &kArg, NULL );
		}

		CONT_ITEM_CREATE_ORDER kItemCreateOrder;
		for ( int i = 0; i<MAX_ALRAM_MISSION_ITEMBAG; ++i )
		{
			if ( pkDef->iItemBag[i] )
			{
				PgItemBag kItemBag;
				GET_DEF(CItemBagMgr, kItemBagMgr);
				if ( S_OK == kItemBagMgr.GetItemBag( pkDef->iItemBag[i], static_cast<short>(iLevel), kItemBag ) )
				{
					int iItemNo = 0;
					int iCount = 0;
					if ( S_OK == kItemBag.PopItem(iLevel, iItemNo, iCount) )
					{
						PgBase_Item kItem;
						if( SUCCEEDED(::CreateSItem(iItemNo, iCount, GIOT_NONE, kItem)) )
						{
							kItemCreateOrder.push_back(kItem);
						}
					}
				}
			}
		}

		if( !kItemCreateOrder.empty() )
		{
			PgAction_CreateItem kCreateAction(CIE_AlramMission, GroundKey(), kItemCreateOrder);
			kCreateAction.DoAction( pkPlayer, NULL );							
		}
	}

	if ( rkAlramMission.GetNextID() )
	{
		// 새로운 Action으로 전이해야 한다.
		if ( SUCCEEDED(m_pkAlramMissionMgr->GetNextAction( rkAlramMission )) )
		{
			BM::CPacket kNewAlraMPacket( PT_M_C_NFY_ALRAMMISSION_BEGIN, rkAlramMission.GetID() );
			kNewAlraMPacket.Push( rkAlramMission.GetBeginTime() );
			pkPlayer->Send( kNewAlraMPacket, E_SENDTYPE_SELF|E_SENDTYPE_SEND_BYFORCE );
		}
		else
		{
			CAUTION_LOG( BM::LOG_LV1, __FL__ << L"Next Action Failed!!! NaxtActionID<" << rkAlramMission.GetNextID() << L"> CurrentID<" << rkAlramMission.GetID() << L"> CharGuid<" << pkPlayer->GetID() << L">" );
		}
	}
}

CUnit* PgGround::CreateSummoned(CUnit* pCaller, CreateSummoned_* pCreateInfo, LPCTSTR lpszName)
{
	if(NULL==pCaller)	//Caller가 없는 소환수는 절대 없다!
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__<<L"pCaller NULL Data.");
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
        return NULL;
	}

	if(NULL==pCreateInfo)
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__<<L"SCreateSummoned_ NULL Data. Caller is "<<pCaller->Name());
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
        return NULL;
	}	

	if(UT_PLAYER!=pCaller->UnitType())	//Caller가 플레이어가 아니면 절대 안된다
	{
		VERIFY_INFO_LOG(false, BM::LOG_LV0, __FL__<<L"pCaller is not UT_PLAYER. "<<lpszName);
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
        return NULL;
	}

	PgSummoned *pSummoned = dynamic_cast<PgSummoned*>(g_kTotalObjMgr.CreateUnit(UT_SUMMONED, pCreateInfo->kGuid));
	if(NULL==pSummoned)
	{
		INFO_LOG(BM::LOG_LV0, __FL__<<L"Cannot CreateSummoned Guid["<<pCreateInfo->kGuid<<L"] Caller Is "<<pCaller->GetID());
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return NULL"));
		return NULL;
	}

	SummonedInfo_ kInfo(pCreateInfo->kGuid, pCreateInfo->kClassKey);
	kInfo.bSyncUnit = true;
	kInfo.bEternalLife = false;	//엔티티의 AUTO_HEAL때문에 있는 변수임

	Direction eFrontDir = DIR_DOWN;

	kInfo.kCaller = pCaller->GetID();
	kInfo.SetAbil(AT_TEAM, pCaller->GetAbil(AT_TEAM));
	kInfo.SetAbil(AT_OWNER_TYPE, pCaller->GetAbil(AT_OWNER_TYPE));
	kInfo.SetAbil(AT_CALLER_TYPE, pCaller->UnitType());
	kInfo.SetAbil(AT_CALLER_LEVEL, pCaller->GetAbil(AT_LEVEL));
	kInfo.SetAbil(AT_HITRATE, pCaller->GetAbil(AT_C_HITRATE));
	kInfo.SetAbil(AT_PHY_ATTACK_MIN, pCaller->GetAbil(AT_PHY_ATTACK_MIN));
	kInfo.SetAbil(AT_PHY_ATTACK_MAX, pCaller->GetAbil(AT_PHY_ATTACK_MAX));
	kInfo.SetAbil(AT_MAGIC_ATTACK_MIN, pCaller->GetAbil(AT_MAGIC_ATTACK_MIN));
	kInfo.SetAbil(AT_MAGIC_ATTACK_MAX, pCaller->GetAbil(AT_MAGIC_ATTACK_MAX));
	kInfo.SetAbil(AT_C_PHY_ATTACK_MIN, pCaller->GetAbil(AT_C_PHY_ATTACK_MIN));
	kInfo.SetAbil(AT_C_PHY_ATTACK_MAX, pCaller->GetAbil(AT_C_PHY_ATTACK_MAX));
	kInfo.SetAbil(AT_C_MAGIC_ATTACK_MIN, pCaller->GetAbil(AT_C_MAGIC_ATTACK_MIN));
	kInfo.SetAbil(AT_C_MAGIC_ATTACK_MAX, pCaller->GetAbil(AT_C_MAGIC_ATTACK_MAX));
	kInfo.SetAbil(AT_C_ABS_ADDED_DMG_PHY, pCaller->GetAbil(AT_C_ABS_ADDED_DMG_PHY) );
	kInfo.SetAbil(AT_C_ABS_ADDED_DMG_MAGIC, pCaller->GetAbil(AT_C_ABS_ADDED_DMG_MAGIC) );		
	kInfo.SetAbil(AT_HITRATE, pCaller->GetAbil(AT_HITRATE));
	kInfo.SetAbil(AT_PHY_DMG_PER, pCaller->GetAbil(AT_PHY_DMG_PER));
	kInfo.SetAbil(AT_MAGIC_DMG_PER, pCaller->GetAbil(AT_MAGIC_DMG_PER));

	{// 크리티컬 관계된 어빌들
		kInfo.SetAbil(AT_C_CRITICAL_RATE, pCaller->GetAbil(AT_C_CRITICAL_RATE));
		kInfo.SetAbil(AT_CRITICAL_ONEHIT, pCaller->GetAbil(AT_CRITICAL_ONEHIT));
		kInfo.SetAbil(AT_C_CRITICAL_POWER, pCaller->GetAbil(AT_C_CRITICAL_POWER));
	}

	if ( !SUCCEEDED( pSummoned->Create( &kInfo ) ))
	{
		g_kTotalObjMgr.ReleaseUnit( dynamic_cast<CUnit*>(pSummoned) );
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Summoned->Create fail. Return NULL"));
		return NULL;
	}
	
	if ( lpszName )
	{
		pSummoned->Name( lpszName );
	}
	pSummoned->SetPos( pCreateInfo->ptPos );
	pSummoned->SetState(US_IDLE);
	pSummoned->LastAreaIndex( PgSmallArea::NONE_AREA_INDEX );
	pSummoned->FrontDirection( eFrontDir );
	pSummoned->LifeTime(pCreateInfo->iLifeTime);

	if(false==pCaller->AddSummonUnit( pSummoned->GetID(), pSummoned->GetClassKey().iClass, false))
	{
		g_kTotalObjMgr.ReleaseUnit( dynamic_cast<CUnit*>(pSummoned) );
		return NULL;
	}

	AddUnit(pSummoned, true);

	return pSummoned;
}