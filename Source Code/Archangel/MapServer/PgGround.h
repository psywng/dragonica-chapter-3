#ifndef MAP_MAPSERVER_MAP_GROUND_PGGROUND_H
#define MAP_MAPSERVER_MAP_GROUND_PGGROUND_H

#include "BM/Observer.h"
#include "Lohengrin/GMCommand.h"
#include "Variant/Global.h"
#include "variant/gm_const.h"
#include "Variant/PgAggroMeter.h"
#include "Variant/PgAggroMeterMgr.h"
#include "Variant/PgComboCounter.h"
#include "Variant/PgComboCounterMgr.h"
#include "Variant/PgEventQuestBase.h"
#include "Variant/AlramMissionChecker.h"
#include "Collins/Log.h"
#include "constant.h"
#include "AI/PgAIManager.h"
#include "PgSmallArea.h"
#include "PgGroundResource.h"
#include "PgReqMapMove.h"
#include "PgGenPointMgr.h"
#include "PgLocalPartyMgr.h"
#include "PgLocalVendorMgr.h"
#include "PgUserQuestSessionMgr.h"
#include "PgMarryMgr.h"
#include "PgWorldEnvironmentStatus.h"
#include "Variant/PgHiddenRewordItemMgr.h"
#include "PgEventItemSet.h"
#include "PgDefencePartyMgr.h"
#include "PgJobSkillMgr.h"
#include "Variant/PgJobSkillLocationItem.h"


typedef enum eFAKE_REMOVE_TYPE
{
	FRT_NONE = 0,
	FRT_ONLY_HIDE,
	FRT_HIDE_AND_NO_MSG,
	FRT_HIDE_AND_NO_MSG_BUT_MAPMOVE,
}EFAKE_REMOVE_TYPE;

namespace PgGroundUtil
{
	inline bool IsVillageGround(T_GNDATTR const& rAttr);
	bool IsCanTalkableRange(CUnit* pkPC, BM::GUID const& rkNpcGuid, CUnit* pkNPC, int const iCurGround, ENpcMenuType const eMenuType, TCHAR const* szFucn, size_t const iLine);
	bool IsCanTalkableRange(CUnit* pkPC, POINT3 const& rkPos, BM::GUID const& rkNpcGuid, CUnit* pkNPC, int const iCurGround, ENpcMenuType const eMenuType, TCHAR const* szFunc, size_t const iLine);

	bool IsNeedNpcTestChangeItemEvent(SItemPos const& rkPos);
	bool IsCallSafe(CUnit * pkUnit,SItemPos const & kSourcePos,SItemPos const & kTargetPos);
	bool IsCanTalkableRange(CUnit* pkPC, BM::GUID const& rkNpcGuid, CUnit* pkNPC, int const iCurGround, ENpcMenuType const eMenuType, TCHAR const* szFunc, size_t const iLine);
	bool IsMyHomeHasFuctionItem(CUnit * pkPlayerUnit,CUnit * pkNpcUnit,EUseItemCustomType const kType);

	enum ESpecialUnitStatusType
	{
		SUST_None					= 0,
		SUST_CashShop				= 1,
		SUST_OpenMarket				= 2,
		SUST_PvPModeSelectting		= 3,
		SUST_HardCoreDungeonVote	= 4,
		SUST_Vendor					= 5,
	};
	typedef std::map< BM::GUID, ESpecialUnitStatusType > ContUnitSpecialStatus;

	
	class PgUnitSepcailStatusHandler // 상태값은 양립 못한다. (양립 하려면 컨테이너로 second를 적당히 수정 하시오)
	{
	protected:
		PgUnitSepcailStatusHandler();
		virtual ~PgUnitSepcailStatusHandler();

		ESpecialUnitStatusType EnterSpecStatus(BM::GUID const& rkGuid, ESpecialUnitStatusType const eEnterType, bool const bChange = false );
		bool ChangeSpecStatus( BM::GUID const &rkGuid, ESpecialUnitStatusType const eCheckType, ESpecialUnitStatusType const eChangeType );
		bool LeaveSpecStatus(BM::GUID const& rkGuid, ESpecialUnitStatusType const eLeaveType);
		ESpecialUnitStatusType GetSpecStatus(BM::GUID const& rkGuid) const;
		void ClearSpecStatus(BM::GUID const& rkGuid);

	private:
		mutable Loki::Mutex m_kUnitSpecialStatusMutex;
		ContUnitSpecialStatus m_kContSpecStatus;
	};

	void DeleteInvenCoupleItem(CUnit* pkUnit, WORD const wItemHaveAbilType, CItemDefMgr const& rkItemDefMgr, SGroundKey const& rkGndKey);

	bool IsMyHomeHasFuctionItem(CUnit * pkPlayerUnit,CUnit * pkNpcUnit,EUseItemCustomType);
	bool IsMyHomeHasSideJob(CUnit * pkPlayerUnit,CUnit * pkNpcUnit,eMyHomeSideJob const kSideJob);
	bool IsSaveHPMP(EGndAttr const eGndAttr);

	template<typename T_CONT>
	void InitGenPointChild(T_CONT& rkCont)
	{
		T_CONT::iterator itor_cont = rkCont.begin();
		while (rkCont.end() != itor_cont)
		{
			T_CONT::mapped_type& rkGenInfo = (*itor_cont).second;
			rkGenInfo.RemoveAll();

			++itor_cont;
		}
	}	
};

class PgPlayer;
class PgActionResultVector;
class PgLocalPartyMgr;
class PgLocalMarketMgr;

//#define MAX_SEND_BROADCASTING_DISTANCE 800
//#define MAX_SEND_BC_AREA_DISTANCE	MAX_SEND_BROADCASTING_DISTANCE * 1.5

int const ON_DIE_SKILL_HP = 1;
int const MONSTER_AI_TICK_INTER = 300;	//몬스터 AI틱 간격
int const MONSTER_AI_TICK_INTER_ERROR = 30;	//몬스터 틱의 허용오차. 기준의 1/10

#define AI_MONSTER_MAX_MOVETIME 10000	// Monster maximum move time
#define AI_MONSTER_MIN_MOVETIME	1500	// Monster minimum move time

enum eIndunState
{
	INDUN_STATE_NONE		= 0x00,
	INDUN_STATE_OPEN		= 0x01,	// 인던이 오픈되었다.
	INDUN_STATE_WAIT		= 0x02,	// 인던이 유저를 기다리는 중이다.
	INDUN_STATE_READY		= 0x04,	// 인던이 준비가 되었다.
	INDUN_STATE_PLAY		= 0x08,	// 인던의 유저들이 놀고 있는 중이다.
	INDUN_STATE_RESULT_WAIT	= 0x10,	// 인던의 플레이 결과를 보여주어야 한다.
	INDUN_STATE_RESULT		= 0x20,	//
	INDUN_STATE_CLOSE		= 0x40,	// 인던이 닫혀야 한다.
};
typedef DWORD	EIndunState;

// 오프닝여부
typedef enum eOpeningState
{
	E_OPENING_NONE			= S_OK,
	E_OPENING_READY			= S_FALSE,//인던 Ready상태에서 오프닝을 해야함
	E_OPENING_PLAY			= E_OPENING_READY+1,
	E_OPENING_ERROR			= E_FAIL,
}EOpeningState;

typedef enum
{
	E_SPAWN_DEFAULT			= 0,
	E_SPAWN_RED				= TEAM_RED,
	E_SPAWN_BLUE			= TEAM_BLUE,
	E_SPAWN_WIN				= 3,
	E_SPAWN_LOSE			= 4,
	E_SPAWN_BATTLEAREA_ATK	= 5,
	E_SPAWN_BATTLEAREA_DEF	= 6,
	E_SPAWN_DEFENCE7_RED	= 7,
	E_SPAWN_DEFENCE7_BLUE	= 8,
}ESpawnType;

#pragma pack(1)

/*
class CCheckSmallArea
	: public TVisitor< PgSmallArea* >
{
public:
	CCheckSmallArea(POINT3 const &rkPos)
		: m_kPoint(rkPos)
	{
	}

	virtual bool VIsCorrect(PgSmallArea* &pArea)
	{
		return pArea->IsAdjacentArea(m_kPoint);
	}
	POINT3 const m_kPoint;
};
*/

typedef struct tagSMonsterGen
{
	tagSMonsterGen()
	{
		iMonNo = 0;
		i64GenTime = 0;
	}

	tagSMonsterGen(int const _iMonNo,__int64 const i64CurTime)
	{
		iMonNo = _iMonNo;
		i64GenTime = i64CurTime;
	}

	tagSMonsterGen(tagSMonsterGen const& rkInfo)
	{
		iMonNo = rkInfo.iMonNo;
		i64GenTime = rkInfo.i64GenTime;
	}

	int		iMonNo;
	__int64	i64GenTime;
} SMonsterGen;

#pragma pack()

typedef std::map<int,SMonsterGen>	CONT_MONSTER_GEN;
typedef std::set<int>				CONT_GENERATED_MONSTER;

class PgGroundManager;
class PgUserQuestSessionMgr;
//Unit은 SmallArea 를 전혀 모른다. 단지 그라운드가 관리하기 쉽게 하려고 하는것이다. Objserver로도 등록 말것
class PgGround
	:	public BM::CObserver< BM::CPacket* >
	,	public PgGroundResource
	,	public PgAggroMeterMgr
	,	public PgComboCounterMgr
	,	protected PgObjectMgr2
	,	protected PgGroundUtil::PgUnitSepcailStatusHandler
{
	// 상수들
public:
	typedef std::vector<CUnit const*>	ConUnit;

	//typedef std::map< POINT3, PgSmallArea* > ContArea;
	typedef std::vector<PgSmallArea*> ContArea;

	typedef std::map< BM::GUID, SPartyUserInfo > ContPartyMember;
	typedef std::vector<SPartyUserInfo> VEC_UserInfo;
	typedef std::map< int, int > ContClassLevel;

public :
	static AntiHackCheckActionCount ms_kAntiHackCheckActionCount;
	static AntiHackCheckVariable ms_kAntiHackCheckVariable;
	static void SetAntiHackVariable();

	static float		ms_fAutoHealMultiplier_NotInViliage;

	static int			ms_iEnableEntityCritialAttack;	// Entity 생성시 Player의 Critial 확률및 공격력 적용
	static int			ms_iEnableEntityHitRate;		// Entity 생성시 Player의 명중률 적용
	static int			ms_iEnableEntityAttackExtInfo;	// Entity 생성시 Player의 추가 공격정보 적용

	static int			ms_iHiddenRewordItemNo;
	static T_GNDATTR	ms_kOffPet_GroundAttr;			// 펫이 나오지 않는 그라운드 속성

	static float		ms_KnightAwakeReviserByKill;	//몬스터를 죽였을때 차는 각성치 보정값
	static float		ms_WarriorAwakeReviserByKill;
	static float		ms_MageAwakeReviserByKill;
	static float		ms_BattleMageAwakeReviserByKill;
	static float		ms_HunterAwakeReviserByKill;
	static float		ms_RangerAwakeReviserByKill;
	static float		ms_ClownAwakeReviserByKill;
	static float		ms_AssassinAwakeReviserByKill;

	static int			ms_JobSkillPenalty[14];
	static int			ms_JobSkillBaseExpDiff[4]; // 1~4

public:
	PgGround();
	virtual ~PgGround();

public: //AI 예외 public 
	void AI_CallHelp(CUnit* pkUnit, float fCallRange);
	bool AI_SkillFire( CUnit* pkCaster, SActArg* pkActArg, const ESkillStatus eUnitState = ESS_FIRE);
	bool AI_SkillCasting( CUnit* pkCaster, SActArg* pkActArg, BM::CPacket& rkTailPacket);
	bool AI_GetSkillTargetList(CUnit* pkCaster, int const iSkillNo, UNIT_PTR_ARRAY& rkTargetList, bool bDamageFire, SActArg* pkActArg);
	bool FindEnemy(CUnit* pkFrom, int const iMaxTarget = 5);
	bool CanAttackTarget(CUnit* pkAttacker);
	void SendNfyAIAction(CUnit* pkUnit, EUnitState eState = US_IDLE, PgActionResultVector* pkActionResultVec=NULL, BM::CPacket* pkTailPacket = NULL, int const iSkillNo = 0);
	bool IsTargetInRange(CUnit* pkUnit, int const iZLimit = AI_Z_LIMIT, bool const bCheckFromGround = false);
	int GetReserveAISkill(CUnit* pkUnit);
	POINT3 GetUnitPos( BM::GUID const & rkGuid, bool bCheckImpact = false, float const fRayRange = 300.0f );
	void SendNftChaseEnemy(CUnit* pkUnit);
	float GetAdditionalAttackProb(CUnit *pkUnit);	
	bool GetAroundVector(CUnit *pkUnit, BM::GUID const &rkTargetGuid, float const fRange, bool bNear, POINT3 &rkOut);
	POINT3BY GetPathNormal(BM::GUID const & rkGuid);
	bool GetTargetPos(CUnit* pkUnit, POINT3& rkTargetPos);

protected:

	void ProcessTimeOutedItem(PgPlayer * const pkPlayer);
	void ProcessTimeOutedMonsterCard(PgPlayer * const pkPlayer);
	void ProcessUnbindItem(PgPlayer * const pkPlayer);
	void ProcessCheckMacroUse(PgPlayer * const pkPlayer);
	void ProcessRecommendPointRefresh(PgPlayer * const pkPlayer);

//////////////////////////

	void OnReqItemAction( PgPlayer *pkPlayer, BM::CPacket * pkNfy, PgPet * pkPet=NULL);

	void CheckPetAliveAchievement(PgPlayer * pkPlayer);

    virtual int GetMaxSummonUnitCount(CUnit * pkUnit)const;
	virtual void SendMonsterCount();

public:
	virtual EOpeningState Init( int const iMonsterControlID=0, bool const bMonsterGen=true );
	virtual void Clear();
	virtual bool Clone( PgGround* pkGround );

	// Ground Manager의 OnTimer에서 호출(리턴값이 true이면 그라운드가 삭제된다.)
	virtual bool IsDeleteTime()const{return false;}
	virtual void OnTick100ms();
	virtual void OnTick1s();
	virtual void OnTick5s();
	virtual void OnTick30s();
	inline bool CheckTickAvailable(ETickInterval const eInterval, DWORD const dwNow, DWORD& rdwElapsedTime, bool bUpdateLastTime = false);

	virtual EGroundKind GetKind()const{	return GKIND_DEFAULT;	}
	virtual EIndunState GetState()const{return INDUN_STATE_NONE;}
	virtual int GetGroundNo()const { return GroundKey().GroundNo();}; //현재 그라운드 번호
	virtual int GetGroundItemRarityNo()const { return GIOT_FIELD; }
	virtual void CheckEnchantControl(PgBase_Item& rkItem)const {};

	virtual int GetMapItemBagGroundNo() const { return GetGroundNo(); };

	virtual void GetGenGroupKey(SGenGroupKey& rkkGenGrpKey)const;
protected:
	DWORD GetTimeStamp();

	//	Zone
	HRESULT BuildZone();
#ifdef ZONE_OPTIMIZE_20090624
	HRESULT MakeZone(NiNode *pkRootNode);
#endif
	void ReleaseZone();

	bool RemoveGenGroupMonster(int const iGenGroupNo, bool const bKill);	//! gen group 몬스터를 제거한다. 
	bool RemoveGenGroupObject(int const iGenGroupNo);						//! 젠 그룹의 오브젝트를 제거한다. 

	HRESULT GetArea(POINT3 const &rkPos, POINT3 &rkOut, PgSmallArea* &pRetArea, bool const bIsMoveToSpawn = true, bool bCheckPos = true);
	PgSmallArea* GetArea( int const iAreaIndex )const;

	void ProcessMonsterGenNfy(int const iGenGroupNo,E_MONSTER_GEN_MODE const kMode);
	float GetAutoHealMultiplier()const;
	void OnActivateEventMonsterGroup();
	bool CheckPathNormal(CUnit* pkUnit, CSkillDef const* pkSkillDef, SActionInfo const& rkAction, float const fErrorDelta = 10.0f);//클라에서 보내준 패스노말과 공격방향 검사. 오차한계 10도
//	+-------------------------------------------------------+
//	|	Unit Control Method									|
//	+-------------------------------------------------------+
public:
	void OnRecvMonsterGenNfy(int const iGenGroupNo,E_MONSTER_GEN_MODE const kMode);
	size_t GetUnitCount( const eUnitType eType )const;
	bool UnitEventNfy(BM::GUID const &rkGuid, BM::CPacket* pkPacket);

	CUnit* GetUnit( BM::GUID const &rkGuid )const;
	PgPlayer* GetUser( BM::GUID const &rkCharGuid ) const;
	PgPet* GetPet( PgPlayer * pkPlayer )const;
	PgPet* GetPet( BM::GUID const &kPetID )const;

	bool CheckUnit( BM::GUID const &rkGuid )const;//Unit이 있는가?
	BM::GUID const & GetFollowingHead(CUnit* pktail, bool const bGotoEnd, short int sCallDepth = 0);

	int GetUnitAbil(BM::GUID const & rkGuid, WORD const wType);

	virtual HRESULT InsertMonster(TBL_DEF_MAP_REGEN_POINT const &rkGenInfo, int const iMonNo, BM::GUID &rkOutGuid, CUnit* pkCaller=NULL, bool bDropAllItem = false, int iEnchantGradeNo = 0);

	void DeletePet( BM::GUID const &kPetID );
	PgPet* CreatePet( PgPlayer * pkCaller, BM::GUID const &kPetID, CONT_PET_MAPMOVE_DATA::mapped_type &kPetData );
	CUnit* CreateEntity(CUnit* pkCaller, SCreateEntity* pkCreateInfo, LPCTSTR lpszName, bool bSyncUnit = true);
	int GetEntity( BM::GUID const &kCaller, int const iClass, UNIT_PTR_ARRAY& rkUnitArray);
	CUnit* CreateGuardianEntity(CUnit* pkCaller, SCreateEntity* pkCreateInfo, LPCTSTR lpszName, bool bSyncUnit = true);	
	CUnit* CreateSummoned(CUnit* pCaller, CreateSummoned_* pCreateInfo, LPCTSTR lpszName);

	virtual bool ReleaseUnit( CUnit *pkUnit, bool bRecursiveCall=false, bool const bSendArea=true );
	void OnPT_T_C_NFY_MARRY(EMarryState const eCommandType, BM::CPacket *const pkPacket);

protected:
	bool FakeRemoveUnit( CUnit * pkCaller, int const iHideMode );
	bool FakeAddUnit(CUnit * pkCaller);
	
	void SetUnitAbil( BM::GUID const & kCharGuid, WORD const Type, int const iValue, bool const blsSend = false, bool const bBroadcast = false );
	
	//void AddNPC(char const* pcName, char const* pcActor, char const* pcScript, char const* pcLocation, BM::GUID const &rkGuid, int iID);
	
	POINT3BY GetMoveDirection(BM::GUID const & rkGuid);

	void AutoHealAndCheckDieSkill(CUnit * pkUnit, DWORD const dwkElapsed, float const fAutoHealMultiplier = 1.0f);

public:
	void LogOut( SERVER_IDENTITY const &kSI );
	virtual bool LogOut(BM::GUID const &rkCharGuid);

protected:
	CUnit* GetUnitControl(CUnit* pkUnit, CUnit* pkCalledUnit, unsigned short usPacketType, BM::CPacket& rkPacket);

	virtual bool SaveUnit( CUnit *pkUnit, SReqSwitchReserveMember const *pRSRM = NULL );
	bool AddUnit( CUnit *pkUnit, bool const bIsSendAreaData = true );	//상속금지

	virtual void PreReleaseUnit( CUnit const *pkUnit );
	void ReleaseUnit( UNIT_PTR_ARRAY& rkUnitArray, bool bRecursiveCall=false );
	void ReleaseAllUnit();
	bool RemoveAllMonster(bool const bKill=false);
	bool RemoveAllObject();

	CUnit* GetTarget( CUnit *pkUnit )const;
	void GetTargetList(CUnit* pkUnit, UNIT_PTR_ARRAY& rkUnitArray, int const iInNum);
	void GetSummonUnitArray( CUnit *pkUnit, UNIT_PTR_ARRAY& rkUnitArray );
	
	PgMonster* GetFirstMonster(int const iMonID);
	PgBoss* GetBossMonster(int const iMonID);
//	void GetFirstUnit( EUnitType const eUnitType, CONT_OBJECT_MGR_UNIT::iterator& rkItor );
//	CUnit* GetNextUnit( EUnitType const eUnitType, CONT_OBJECT_MGR_UNIT::iterator& rkItor );
	
	CUnit* GetPlayer(char const *pcCharacterName);
	CUnit* GetPlayer( std::wstring const &wstrName );

	int GetTargetInArea(CUnit* pkCaster, CSkillDef const *pkSkillDef, EUnitType eUnitType, UNIT_PTR_ARRAY& rkTargetList, bool bDamageFire);

	POINT3 GetUnitPos( CUnit* pkUnit, bool bCheckImpact = false, float const fRayRange = 300.0f );
	
	void SetUserLife( PgPlayer *pkUser, int const iHP, int const iMP, DWORD const dwFlag=E_SENDTYPE_SELF );
	
	HRESULT InsertObjectUnit( TBL_DEF_MAP_REGEN_POINT const &rkGenInfo, SObjUnitBaseInfo const &kInfo, BM::GUID &rkOutGuid, CUnit* pkCaller=NULL );

	void UpdateAbilGuild(PgPlayer const *pkPC, const WORD eAbil);
	void UpdatePartyUnitAbil(PgPlayer const *pkPC, BYTE const cAbil);//파티 맴버의 컨텐츠로 HP갱신
	void SendNfyMapLoaded( PgPlayer* pkUser );// 상속금지
	virtual void WriteToPacket_AddMapLoadComplete( BM::CPacket &rkPacket )const{}

	bool IsInTriggerUnit( GTRIGGER_ID const& rkTriggerID, CUnit const* pkUnit);
	bool SetTriggerEnable(GTRIGGER_ID const& rkTriggerID, bool const bNewEnable);
	bool RecvTriggerAction( CUnit *pkUnit, BM::CPacket * const pkPacket );
	bool RecvTriggerActionScript(std::string const& rkID);

public:
	void InsertItemBox(POINT3 const &kptPos, VEC_GUID const &kOwners, CUnit* pkCaller, PgBase_Item const &kItem, __int64 const iMoney, PgLogCont &kLogCont );//아이템을 여러개쥔건 떨어지지 않는다.

	//	+-------------------------------------------------------+
	//	|	Map Move / Spawn Method								|
	//	+-------------------------------------------------------+
	bool SendToSpawnLoc(CUnit *pkUnit, int iPotalNo, bool bRandomize = true, int iSpawnType=E_SPAWN_DEFAULT);// 스폰지역으로 유저를 보냄
	bool SendToSpawnLoc(BM::GUID const &rkCharGuid, int iPotalNo, bool bRandomize = true, int iSpawnType=E_SPAWN_DEFAULT);	// 스폰지역으로 유저를 보냄

	HRESULT FindTriggerLoc( char const* szName, POINT3 &rkTriggerLoc_out )const;
	HRESULT FindSpawnLoc( int iPotalNo, POINT3 &rkSpawnLoc_out, bool bRandomize = true, int iSpawnType=E_SPAWN_DEFAULT) const;
	bool SendToPosLoc( CUnit *pkUnit, POINT3 const &pt3Pos, BYTE const byType=MMET_Normal );
	bool MissionItemOrderCheck(PgPlayer *pkPlayer, PgReqMapMove& kMapMove, int const iMissionNo, int iCount);
	bool MissionItemOrderCheck(PgPlayer *pkPlayer, SPMO &kIMO, int const iMissionNo, int iCount);
	bool MissionChaosItemOrderCheck(PgPlayer *pkPlayer, PgReqMapMove& kMapMove, int const iMissionNo, int iCount);
	bool MissionChaosItemOrderCheck(PgPlayer *pkPlayer, SPMO &kIMO, int const iMissionNo, int iCount);
	
	void ReqMapMoveCome( SReqMapMove_CM const &kRMMC, VEC_GUID const &kUserList );
	bool AddPartyMember( PgPlayer *pkMaster, PgReqMapMove &kMapMoveAction );
	void GetStatisticsPlayTime(__int64 i64Now, SMapPlayTime& rkOut);
	void SetEventQuest(PgEventQuest const & rkNewEventQuest);
	bool AddAnyPartyMember( PgPlayer *pkMember, PgReqMapMove &kMapMoveAction );
	bool AddPartyAllMember( PgPlayer *pkMaster, PgReqMapMove &kMapMoveAction );

protected:
	virtual bool RecvMapMove( UNIT_PTR_ARRAY &rkUnitArray, SReqMapMove_MT& rkRMM, CONT_PET_MAPMOVE_DATA &kContPetMapMoveData, CONT_PLAYER_MODIFY_ORDER const &kModifyOrder );
	bool RecvMapMoveCome( SReqMapMove_CM const &kRMMC, BM::GUID const &kComeCharGuid );
	virtual bool RecvMapMoveTarget( BM::GUID const &kTargetCharGuid, BM::GUID const &kReqCharGuid, bool const bGMCommand );
	virtual void RecvMapMoveComeFailed( BM::GUID const &kCharGuid );

	virtual bool IsAccess( PgPlayer * ){return true;}
	void ProcessMapMoveResult( UNIT_PTR_ARRAY& rkUnitArray, SAnsMapMove_MT const &rkAMM );
	virtual bool RecvRecentMapMove( PgPlayer *pkUser );	// 리센트맵 위치로 돌려 보내라!
	virtual bool AdjustArea( CUnit *pkUnit, bool const bIsSendAreaData, bool const bIsCheckPos );
	void ChangeArea( CUnit *pkUnit, PgSmallArea *pkNewArea, PgSmallArea *pkLastArea, bool const bSendAreaData );
	virtual void SendMapLoadComplete( PgPlayer *pkUser );

public:

	void DisplayAreaState();
	//NiPick* GetPhysxPick(){return m_pkPhysxPick;}
	//int GetWayPointGroupCount();
	//int GetWayPointIndexCount(int const iGroup);

//	+-------------------------------------------------------+
//	|	AI Method											|
//	+-------------------------------------------------------+
public:
	bool FindEnemy( CUnit* pkFrom, UNIT_PTR_ARRAY& rkUnitArray, int const iMaxTarget = 5, int const iUnitType=UT_PLAYER|UT_MONSTER, bool const bFindFromGround = false, bool const bCallTimeCheck = true);	//땅으로 레이 쏴서 확인
	void GetUnitInRange( POINT3 const& ptPos, int iRange, EUnitType eType, UNIT_PTR_ARRAY& rkUnitArray, int iZLimit = 0, bool const bFindFromGround = false);
	void GetUnitInCube( POINT3 const &ptCubeMin, POINT3 const &ptCubeMax, POINT3 const &ptPos, EUnitType eType, UNIT_PTR_ARRAY& rkUnitArray );
	CUnit* GetUnitByClassNo( int const iClassNo, EUnitType eType );
	int GetUnitByClassNo( int const iClassNo, EUnitType eType, UNIT_PTR_ARRAY& rkUnitArray );
    CUnit* GetUnitByType( EUnitType const eType );
	bool GetVisionDirection(CUnit* pkUnit, NxVec3& rkDirectionOut, bool bUseTargetPos = true);
	void GetUnitInWidthFromLine(POINT3 const& ptStart, POINT3 const& ptEnd, int iWidth, EUnitType eType, UNIT_PTR_ARRAY& rkUnitArray);
	void GetUnitIn2DRectangle(POINT3 const& kP1, POINT3 const& kP2, POINT3 const& kP3, POINT3 const& kP4, EUnitType eType, UNIT_PTR_ARRAY& rkUnitArray); // Only use x,y
	void DequeTargetToUNIT_PTR_ARRAY(CUnit::DequeTarget const &rkDeque, UNIT_PTR_ARRAY& rkArr);
	NxShape* RayCast(NxRay& rkWorldRay, NxRaycastHit& rkHit, NxReal maxDist = 200.0f, NxU32 hintFlags = NX_STATIC_SHAPES) const;

	//GetTargetList와 함수명 겹치지 않도록
	//pkUnit 에 따라서 타겟을 잡아 리스트에 넣어준다.
	void GetUnitTargetList(CUnit* pkUnit, UNIT_PTR_ARRAY& rkUnitArray, int const iTargetType, int iRange = 0, int const iZLimit = AI_Z_LIMIT);

	EOpeningState ActivateMonsterGenGroup( int const iGenGroup=0, bool const bReset=true, bool const bOnce=false, int const iCopy=0 );

protected:
	void FindUnit_WhenJoinedTeam(CUnit* pkUnit, UNIT_PTR_ARRAY& rkUnitArray, int const iTargetType, int const iRange, int const iZLimit, int const iTeam, bool const bIfNoneRangeThenAddUnit = false );
	void FindUnit_WhenNoneJoinedTeam(CUnit* pkUnit, UNIT_PTR_ARRAY& rkUnitArray, int const iTargetType, int const iRange, int const iZLimit);
	void RemoveNoneHiddenUnit(CUnit* pkUnit, UNIT_PTR_ARRAY& rkUnitArray, int const iRange, int const iZLimit);
	void RemoveAliveUnit(CUnit* pkUnit, UNIT_PTR_ARRAY& rkUnitArray, int const iRange, int const iZLimit);

	bool FindNearestUnt(const EUnitType eUT, POINT3 const& rkPos, float const fRange, int const iFindType, bool const bOnlyAlive, float* pfDist,
		BM::GUID& pkFindUnit, NxVec3& kNormalVector);
	CUnit* FindRandomUnit(POINT3 const &rkPos,float const fRange,const EUnitType eUnitType);
	
	HRESULT CheckMonsterDie(PgMonster* pkUnit);
	HRESULT CheckObjectUnitDie(PgObjectUnit* pkUnit);
	virtual HRESULT SetUnitDropItem(CUnit *pkOwner, CUnit *pkDroper, PgLogCont &kLogCont );

	bool InitRareMonsterGen();
	bool RareMonsterGenerate();
	bool GetRareMonster(int const iMonBagNo,int & iMonNo) const;
	bool RemoveRareMonster(int const iMonNo,PgMonster * pkMonster);
	bool GetRandomMonsterGenPoint(int const iGenGroup,PgGenPoint & kGenPoint);	// 몬스터 젠 그룹중 랜덤으로 젠포인트 하나를 가져 온다.

	
	EOpeningState MonsterGenerate( ContGenPoint_Monster& kContGenPoint, int const iCopy=0, CUnit* pkCaller=NULL);

	EOpeningState ActivateObjectUnitGenGroup( int const iGenGroup, bool const bReset, bool const bOnce);

	bool ObjectUnitGenerateGroundCheck(PgGenPoint_Object& kGenPoint, BM::GUID& kGuid);
	
	EOpeningState ObjectUnitGenerate(ContGenPoint_Object & kContGenPoint, bool const bReset = false);

	void ObjectUnitGenerate( bool const bReset=false, int const iGenGroup=0 );

	int TickAI( CUnit* pkUnit, DWORD dwkElapsed );
	virtual HRESULT PlayerTick(DWORD const dwElapsed);
	virtual HRESULT PetTick(DWORD const dwElapsed);

	HRESULT MonsterTick(DWORD const dwElapsed,VEC_UNIT& rkDelUnitVec);
	HRESULT ObjectUnitTick( DWORD const dwElapsed, UNIT_PTR_ARRAY& rkDelUnitArray );
	virtual bool IsMonsterTickOK(){	return PgObjectMgr::GetUnitCount(UT_PLAYER) > 0;}
	int MonsterTickAI( CUnit* pkUnit, DWORD dwkElapsed );

	bool IsAttackable(EAbilType eType, CUnit* pkCaster, CUnit *pkTarget,int& iSkillNo);
	//bool SkillFire(int const iSkillNo, CUnit* pkCaster, CUnit* pkTarget, PgActionResultVector* pkResult, SActArg* pkAct, bool bSendPacket = false);

	//void PlayerQuestTick(CUnit* pkUnit);

private:
	EOpeningState MonsterGenerate(PgGenPoint& rkGenPoint, int& iMonNo, BM::GUID& kMonGuid, CUnit* pkCaller = NULL);
	EOpeningState ObjectUnitGenerate(PgGenPoint_Object& rkGenPoint, PgGenPoint_Object::OwnGroupCont const& kCont);
	EOpeningState ObjectUnitGenerate(PgGenPoint_Object& rkGenPoint);

//	+-------------------------------------------------------+
//	|	Network Method										|
//	+-------------------------------------------------------+
public:
	virtual void VUpdate( BM::CSubject< BM::CPacket* > * const pChangedSubject, BM::CPacket* iNotifyCause );
//	void SendNoticeToAllUser( std::wstring wstrContent );

	//bool SendToUser_ByGuid( BM::GUID& rkGuid, BM::CPacket &rkPacket, DWORD const dwSendType = E_SENDTYPE_SELF );
	//bool SendToUser_ByGuidVec( VEC_GUID const &rkVec, BM::CPacket const &rkPacket, BM::GUID const &rkPartyGuid, DWORD const dwSendType = E_SENDTYPE_SELF);//GroundMng이외에서 호출 하지 마세요
	bool SendToUser_ByGuidVec( VEC_GUID const &rkVec, BM::CPacket const &rkPacket, DWORD const dwSendType = E_SENDTYPE_SELF);

	void RecvGndWrapped( BM::CPacket* const pkPacket );
	void RecvChatInputState(CUnit *pkUnit, BM::CPacket *pkPacket);
	void OrderSyncGameTime(BM::CPacket* const pkSendPacket);

	void Broadcast( BM::CPacket const& rkPacket, BM::GUID const &kCallerCharGuid, DWORD const dwSendFlag=E_SENDTYPE_NONE);
	void Broadcast(BM::CPacket const& rkPacket, CUnit const *pkCaller=NULL, DWORD const dwSendFlag=E_SENDTYPE_NONE);

protected:
	void Broadcast( BM::CPacket const& rkPacket, VEC_GUID& rkVecUnit, DWORD const dwSendFlag=E_SENDTYPE_NONE);

	virtual bool VUpdate( CUnit* pkUnit, WORD const wType, BM::CPacket* pkNfy );
	virtual bool RecvGndWrapped( unsigned short usType, BM::CPacket* const pkPacket );
	virtual bool RecvGndWrapped_ItemPacket(unsigned short usType, BM::CPacket* const pkPacket );
	virtual bool Recv_PT_C_M_REQ_PET_ACTION( PgPlayer *pkOwner, BM::CPacket * pkNfy );
	void SendContentsToDirect( CUnit* pkUnit, WORD const wType, BM::CPacket* pkNfy );			// 그냥 콘텐츠 서버로 바로 보내는 패킷
	
	void SendToArea(BM::CPacket *pkPacket, int const iAreaIndex, BM::GUID const &rkIgnoreGuid, BYTE const bySyncTypeCheck, DWORD const dwSendFlag );
	void SendToArea(BM::CPacket *pkPacket, PgSmallArea const &rkSmallArea, BM::GUID const &rkIgnoreGuid, BYTE const bySyncTypeCheck, DWORD const dwSendFlag );
	void SendAreaData( CUnit *pkUnit, PgSmallArea const* pkTo, PgSmallArea const* pkFrom,  BYTE const bySyncType=SYNC_TYPE_DEFAULT );
	void SendAddUnitAreaData( PgNetModule<> const &kNetModule, PgSmallArea const* pkTo, PgSmallArea const* pkFrom, BM::GUID const &kIgnoreCharGuid );
	void EffectTick(CUnit* pkUnit, DWORD dwElapsed);

	void ProcNotifyMsg( CUnit* pkUnit, WORD const wType, BM::CPacket* pkNfy);

	bool GMCommand( PgPlayer *pkPlayer, EGMCmdType const iCmdType, BM::CPacket * const pkNfy );
	bool GMCommandToContents( PgPlayer *pkPlayer, EGMCmdType const iCmdType, BM::CPacket * const pkNfy );
	virtual void GMCommand_RecvGamePoint( PgPlayer *pkPlayer, int const iPoint ){}

	void RecvChat( PgPlayer *pkUnit, BM::CPacket *pkPacket );

//	+-------------------------------------------------------+
//	|	 파티 / 퀘스트 관련									|
//	+-------------------------------------------------------+
public:
	bool GetPartyMember(BM::GUID const & rkPartyGuid, VEC_GUID& rkOut)const;
	bool GetPartyMasterGuid(BM::GUID const & rkPartyGuid, BM::GUID& rkOutGuid)const;
	bool GetPartyMemberGround(BM::GUID const & rkPartyGuid, SGroundKey const& rkGndKey, VEC_GUID& rkOutVec, BM::GUID const & rkIgnore = BM::GUID::NullData())const;
	size_t GetPartyMemberCount(BM::GUID const & rkPartyGuid)const;
	//void SendToGroundPartyPacket(SGroundKey const& rkGndKey, BM::GUID const & rkPartyGuid, VEC_GUID const& rkRecvGuidVec, BM::CPacket const& rkPacket, DWORD const dwSendType = E_SENDTYPE_SELF) const;
	bool GetPartyOption(BM::GUID const & rkPartyGuid, SPartyOption& rkOut);
	bool GetPartyShareItem_NextOwner(BM::GUID const & rkPartyGuid, SGroundKey const& rkGndKey, BM::GUID& rkNextOwner);
	int GetPartyMemberFriend(BM::GUID const & rkPartyGuid, BM::GUID const & rkCharGuid);
protected:
	bool SetPartyGuid( CUnit* pkUnit, BM::GUID const& rkNewGuid, EPartySystemType const kCause );
	void SetPartyGuid(BM::GUID const& rkCharGuid, BM::GUID const& rkNewGuid, EPartySystemType const kCause );
	void SetPartyGuid(VEC_GUID const& rkVecGuid, BM::GUID const& rkNewGuid, EPartySystemType const kCause );
    virtual bool CheckApplyUnitOwner(CUnit* pkOwner, BM::GUID& rkOutApplyOwner);
    virtual int GetGiveLevel(CUnit * pkMonster, CUnit * pkOwner);
	virtual void GiveExp(CUnit* pkMonster, CUnit* pkOwner);
	void CheckMonsterDieQuest(CUnit* pkCaster, CUnit* pkMonster);
	bool CheckUnitOwner(CUnit* pkUnit, BM::GUID& rkOut, BM::GUID * pkOutApplyOwner=NULL);
	void PartyBuffAction(int& iMasterGroundNo, BM::GUID& rkPartyGuid, BM::GUID& kCharGuid, CUnit* pkUnit, bool bBuff, BM::CPacket* pkPacket);

	void SendShowQuestDialog(CUnit* pkUnit, BM::GUID const &rkNpcGUID, EQuestShowDialogType const eQuestShowDialogType, int const iQuestID, int const iDialogID);
	void GiveAwakeValue(CUnit* pkMurderee, CUnit* pkMurderer);
//	+-------------------------------------------------------+
//	|	 NetWork Method										|
//	+-------------------------------------------------------+
	void Recv_PT_C_M_REQ_JOIN_LOBBY( CUnit *pkUnit, BM::CPacket &rkPacket );
	void Recv_PT_T_M_ANS_REGIST_HARDCORE_VOTE( BM::CPacket &rkPacket );
	void Recv_PT_C_M_REQ_RET_HARDCORE_VOTE( CUnit *pkUnit, BM::CPacket &rkPacket );
	void Recv_PT_T_M_ANS_RET_HARDCORE_VOTE_CANCEL( BM::CPacket &rkPacket );
	void Recv_PT_T_M_REQ_JOIN_HARDCORE( BM::CPacket &rkPacket );

//	+-------------------------------------------------------+
//	|	 피로도 관련										|
//	+-------------------------------------------------------+

//	+-------------------------------------------------------+
//	|	 AI 관련										    |
//	+-------------------------------------------------------+
    void StartAI() { m_bRunAI = true; }
    void StopAI() { m_bRunAI = false; }
    bool IsRunAI() const { return m_bRunAI; }

public:
	void SyncPlayerPlayTime(BM::GUID const kCharGuid, int const iAccConSec, int const iAccDisSec, __int64 const i64SelectSec);

//	+-------------------------------------------------------+
//	|	상속받은 클래스가 필요에 따라 추가하는 함수			|
//	+-------------------------------------------------------+
public:
//	PgPvPGround Method

public://영역 및 AI Protected 로 할것.
	//void SendNftMonsterGoal(CUnit* pkUnit,CUnit* pkTarget,POINT3& kPos);
	//void ProcessMonsterAttack(CUnit* pkMonster, CUnit* pkTarget, NxVec3& kNormal, int iSkill);
	//bool FindWayPos(CUnit* pkUnit);
	//bool FindBeforeWayPoint(int const& iGroup, int const& iIndex,POINT3& pt3NextPos);
	//bool FindNearWayPoint(POINT3 const& pt3Pos,int& iGroup , int& iIndex);
	//bool FindNextWayPoint(int const& iGroup, int const& iIndex,POINT3& pt3NextPos);
	//PgWayPoint2* GetWayPoint(int iGroup, int iIndex);

	bool GroundWeight(int const iWeight);
	void TunningLevel(int const iLevel);
	bool GetHaveAbilUnitArray(UNIT_PTR_ARRAY &rkUnitArray, WORD const kAbilType, EUnitType const eUnitType );

// 컨텐츠
protected:
	bool ReqJoinHardCoreDungeon( PgPlayer * pkUser, SGroundKey const *pkDungeonGndKey );
	
	void AddUnitArray( UNIT_PTR_ARRAY &kUnitArray, EUnitType const kUnitType );

	void ReqProjectileAction(CUnit* pkSendUnit, BM::CPacket * const pkPacket);
	void RecvReqAction2(CUnit* pkUnit, SActionInfo& rkAction, BM::CPacket * const pkPacket);
	EActionResultCode CheckAction2(CUnit* pkUnit, const SActionInfo& rkAction);
	bool FireSkill(CUnit* pkCaster, UNIT_PTR_ARRAY& rkTargetArray, const SActionInfo &rkAction, PgActionResultVector* pkActionResultVec);
	bool FireToggleSkill(CUnit* pkCaster, UNIT_PTR_ARRAY& rkTargetArray, const SActionInfo &rkAction, PgActionResultVector* pkActionResultVec);
	void FireSkillFailed(CUnit* pkCaster, UNIT_PTR_ARRAY& rkTargetArray, const SActionInfo &rkAction, PgActionResultVector* pkActionResultVec);	
	void RecvReqTrigger(CUnit* pkUnit, int const iType, BM::CPacket *pkNfy);
	void RecvReqRegQuickSlot(CUnit* pkUnit, size_t const slot_idx, SQuickInvInfo const& kQuickInvInfo);
	void RecvReqRegQuickSlotViewPage(CUnit* pkUnit, char const cViewPage);
	void RecvReqCastBegin(CUnit* pkUnit, int const iSkillNo, DWORD const dwCurrentTime);
	void ReqMonsterTrigger(CUnit* pkUnit, BM::GUID const & kObjectGuid, int iActionType);

	void CheckTargetList(UNIT_PTR_ARRAY& rkTargetArray); // 대신 맞는 타겟이 있는지 검사하여 리스트를 갱신한다.
	bool CheckHackingAction(CUnit* pkUnit, SActionInfo& rkAction); // 액션이 해킹된 것인지 체크 한다.
	void GetClinetProjectileInfo(CUnit* const pkCaster, SActionInfo const & rkAction, CSkillDef const* pkSkillDef, SClientProjectile & kInfo) const;
	void CheckEffectUser(CUnit* pkUnit);
	void HiddenMapCheck(CUnit* pkUnit);
	void HiddenMapTimeLimit(CUnit* pkUnit);
	void GetHiddenMapTime(DWORD &m_dwTotalTimeLimit);

	//>>Quest
	void SendAllQuestInfo(PgPlayer* pkPlayer);
	void ReqIntroQuest(CUnit *pkUnit, int const iQuestID, BM::GUID const& rkObjectGuid);
	void ReqRemoteCompleteQuest(CUnit *pkUnit, int const iQuestID);
	//<<Quest
	void ReqNpcTrigger(CUnit* pkUnit, BM::GUID const& kObjectGuid, int iActionType);

	//>>WorldEvent
	virtual void DoWorldEventAction(int const iGroundNo, SWorldEventAction const& rkAction, PgWorldEvent const& rkWorldEvent);
	virtual void DoWorldEventCondition(int const iGroundNo, SWorldEventCondition const& rkCondition, PgWorldEvent const& rkWorldEvent, CUnit const* pkCaster = NULL);
	virtual void NfyWorldEventToGroundUser(PgWorldEventState const& rkState);
	//<<WorldEvent
	//>>
	virtual void NfyWEClientOjbectToGroundUser(PgWEClientObjectState const& rkState);
	//<<

	virtual void DynamicGndAttr(EDynamicGroundAttr const eNewDGAttr);
	void CheckTickMarry();
	void CheckTickHidden();
	void CheckTickCouple();

	virtual void RecvUnitDie(CUnit *pkUnit);
	virtual bool PushBSInvenItem(CUnit* pkUnit, PgBase_Item const& rkItem)		{ return false; }

	virtual bool PushMissionDefenceInvenItem(CUnit* pkUnit, PgBase_Item const& rkItem)		{ return false; }

	virtual void Defence7ItemUse(CUnit* pkUnit, BM::CPacket * const pkPacket);
    int GetGuardianTunningNo(int const iGuardian)const;
	void SetGuardianAbil(CUnit* pkUnit, SMISSION_DEFENCE7_GUARDIAN const& kValue)const;
	void SetGuardianAbilUpdate(CUnit* pkUnit, int const iMonAbilNo)const;
//---------------------------------------------------------------------
// Alram Mission
	virtual void OnTick_AlramMission( PgPlayer *pkPlayer, DWORD const dwCurTime );
	virtual bool IsAlramMission(void)const{return ( NULL != m_pkAlramMissionMgr );}

public:
	virtual void CallAlramReward( PgPlayer * pkPlayer );
// Alram Mission
//======================================================================
//---------------------------------------------------------------------
// SimpleMarket 노점
public:
	bool RecvReqVendorCreate(CUnit* pkUnit, BM::GUID const& rkVendorGuid, std::wstring const& VendorTitle);
	bool RecvReqVendorRename(CUnit* pkUnit, BM::GUID const& rkVendorGuid, std::wstring const& VendorTitle);
	bool RecvReqVendorDelete(CUnit* pkUnit, BM::GUID const& rkVendorGuid);
	bool RecvReqReadToPacketVendorName(CUnit* pkUnit, BM::GUID const& rkVendorGuid, BM::CPacket &rkPacket);
	void RecvVendorClose(CUnit* pkUnit);
//======================================================================
	CLASS_DECLARATION_S(DWORD, LastGenCheckTime);
	CLASS_DECLARATION_S_NO_SET(BM::GUID, GroundMute);
	//CLASS_DECLARATION_S(DWORD, LastTickTime);
#ifdef DEF_ESTIMATE_TICK_DELAY
	CLASS_DECLARATION_S(DWORD, Tick100msDelayAverage);
#endif
	CLASS_DECLARATION_S_NO_SET(EDynamicGroundAttr, DynamicGndAttr);
	CLASS_DECLARATION_S_NO_SET(PgEventQuest, EventQuest);
	CLASS_DECLARATION_S(SMapPlayTime, PlayTime);
	CLASS_DECLARATION_S(__int64, LastPlayTimeCalculateTime);	// CGameTime::GetLocalTimeSec()
	typedef std::vector< DWORD > LASTTICKTIME_VEC;
	LASTTICKTIME_VEC m_kContLastTickTime;
	CLASS_DECLARATION_S_NO_SET(PgWorldEnvironmentStatus, WorldEnvironmentStatus);
	CLASS_DECLARATION_S(DWORD, LastAddExpInVillageTickTime);
	CLASS_DECLARATION_S(DWORD, DefendModeTickTime);
	CLASS_DECLARATION_S(bool, HiddenMapOut);

public:
	int GetMapLevel()const{return m_iMapLevel;}
	void SetMapLevel( int const iMapLevel );

	HRESULT SetEvent( TBL_EVENT const &kTblEvent );

	virtual bool IsBSItem(int const iItemNo) const				{ return false; }
	void UpdateWorldEnvironmentStatus(SWorldEnvironmentStatus const& rkNewEnvStatus);

	void SetRealmQuestStatus(SRealmQuestInfo const& rkRealmQuestInfo);

    virtual void SetTeam(BM::GUID const& kGuid, int const iTeam);
    virtual int GetTeam(BM::GUID const& kGuid)const;
	virtual bool IsDefenceItemList(int const iItemNo) const		{ return false; }
	bool GetDefenceModeMatchParty(BM::GUID const& kMyPartyGuid, BM::GUID& rkPartyGuid);
	int DefenceIsJoinParty(BM::GUID const& kPartyGuid);
	bool DefenceAddWaitParty(BM::GUID const& kPartyGuid, int eType);
	bool DefenceModifyWaitParty(BM::GUID const& kPartyGuid, int eType);
	bool DefenceDelWaitParty(BM::GUID const& kPartyGuid);
	void SendDefencePartyAllList(CUnit* pkUnit);

	PgJobSkillLocationMgr& JobSkillLocationMgr()		{ return m_kJobSkillLocationMgr; }
	void JobSkillLocationItemInit();
	void CheckTickJobSkillLocationItem();
    bool IsEventScriptPlayer()const { return !m_kContEventScriptPlayer.empty(); }

protected:
	
	int					m_iMapLevel;
	ContArea			m_kAreaCont;	//Small Area
	PgSmallArea*		m_pkBigArea;
	int					m_iGroundWeight;
	int					m_iTunningLevel;
    SET_GUID            m_kContEventScriptPlayer; //실행중인 EventScript UserGuid

	int m_iMonsterControlID;	//이름 짓기 정말 힘들다..
	DWORD m_dwTimeStamp;

	ContGenPoint_Monster	m_kContGenPoint_Monster;//몬스터 리젠포인트
	ContGenPoint_Object		m_kContGenPoint_Object;// ObjectUnit 리젠 포인트

	PgAIManager m_kAIMng;	// 일단 Ground에 소속되도록 해 놓았지만, Global로 빼 놓을 만도 하다.(Lock문제만 해결 된다면 가능)

	CONT_MONSTER_GEN		m_kContRareMonsterGen;// 희귀 몬스터 젠 테이블
	CONT_GENERATED_MONSTER	m_kContGeneratedMonster;//이미 젠된 몬스터 테이블 같은 희귀 몬스터가 다시 젠 될수 없다.

	// 그라운드의 소유자(현재는 엠포리아만 사용한다. 나중에 MyHome에서도 사용 할 수 있다.
	SGroundOwnerInfo		m_kGroundOwnerInfo;

	CONT_REALM_QUEST_INFO	m_kRealmQuestInfo;

public:
	bool PickUpGroundBox(CUnit* pkUnit, BM::GUID const& rkGuid, CUnit* pkLooter = NULL);
	void AddEffectToAllPlayer(int const iEffectNo);
	void HiddenMapRewordItem(CUnit* pkUnit);

public:
	// 특정옵션
	virtual bool IsGroundEffect()const{return true;}// 그라운으 이펙트 사용 여부
	virtual bool IsUseItem()const{return true;}// 소비 아이템을 사용 여부
	virtual bool IsDecEquipDuration()const{return true;}// 죽었을때 아이템 내구도 까기
	virtual bool IsDeathPenalty()const{return true;}// 죽었을때 패널티 주기

protected:
	static void SendUnitPosChange( CUnit *pkUnit, BYTE const eType, DWORD const dwSendFlag );

	virtual bool IsMacroCheckGround() const;
	void SetChangeGroundOwnerInfo( SGroundOwnerInfo const &kInfo, bool const bBroadCast=true );

	// Observer Mode Packet
	HRESULT SetOBMode_Start( PgPlayer *pkCasterPlayer, CUnit *pkTarget );
	HRESULT SetOBMode_Start( PgNetModule<> const &kNetModule, BM::GUID const &kCharID, CUnit *pkTarget, bool const bOnlyModule );
	HRESULT SetOBMode_End( BM::GUID const &kCharID );

	bool ProcessObserverPacket( BM::GUID const &kCharGuid, PgNetModule<> const &kNetModule, PgPlayer *pkPlayer, BM::CPacket::DEF_PACKET_TYPE const kType, BM::CPacket &rkPacket );
	int GetMapAbil(WORD const wType);	// public 함수 아님..(Lock 안했음)

	mutable Loki::Mutex m_kRscMutex;

	DWORD m_dwOnTick1s_Check2s;	//2초마다 유저의 스피드해킹 카운트를 초기화 시켜 줄려고.
	typedef std::map<int, int> CONT_MAPABIL;
	CONT_MAPABIL	m_kContMapAbil;


protected:
	PgLocalPartyMgr		m_kLocalPartyMgr;
	PgUserQuestSessionMgr m_kQuestTalkSession;
	PgLocalVendorMgr	m_kLocalVendorMgr;

	//
	PgMarryMgr				m_kMarryMgr;
	PgHiddenRewordItemMgr	m_kHiddenRewordItemMgr;
	PgEventItemSetMgr		m_kEventItemSetMgr;
	PgDefencePartyMgr		m_kDefencePartyMgr;
	PgJobSkillLocationMgr	m_kJobSkillLocationMgr;

private:

	bool Recv_PT_I_M_MYHOME_MOVE_TO_HOMETOWN(BM::CPacket* const pkPacket );
    
    bool m_bRunAI;

	CUnit* IsUnitInRangeImp(CUnit *pUnit, POINT3 const& ptFind, int iRange, EUnitType eType, int iZLimit, bool const bFindFromGround);
	CUnit* GetReservedUnitByEffect(CUnit *pUnit);
};

#include "PgGround.inl"

float const COUPLE_PASSIVE_HPMP_INCREASE_EFFECT_ABLE_DIST = 600.f;	//꿩먹고 알먹고 이펙트 가능 거리

#endif // MAP_MAPSERVER_MAP_GROUND_PGGROUND_H