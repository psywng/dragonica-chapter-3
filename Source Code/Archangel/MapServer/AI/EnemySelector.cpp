#include <stdafx.h>
#include "..\global.h"
#include "EnemySelector.h"

bool PgSkillAreaChooser::InArea(CUnit * pkUnit, ESkillArea eType, POINT3 const& rkCasterPos, NxVec3 const& rkVisDir, POINT3 const& rkTargetPos,
								int iParam1, int iParam2, int const iMinRange)
{
	bool bResult = false;

	if( NULL==pkUnit )
	{
		return 	bResult;
	}

	switch(eType)
	{
	case ESArea_Sphere:
		{
			int iAddRange = 0;
			if( pkUnit->GetAbil(AT_GROWTH_SKILL_RANGE) )
			{
				iAddRange = pkUnit->GetAbil(AT_UNIT_SIZE_XY);
			}
			bResult = InAreaSphere(rkCasterPos, rkVisDir, rkTargetPos, iParam1+iAddRange, iParam2);
		}break;
	case ESArea_Cube:
		{
			bResult = InAreaCube(rkCasterPos, rkVisDir, rkTargetPos, iParam1, iParam2);
		}break;
	case ESArea_Cone:
		{
			bResult = InAreaCone(rkCasterPos, rkVisDir, rkTargetPos, iParam1, iParam2);
		}break;
	case ESArea_Front_Sphere:
		{
			bResult = InAreaFrontSphere(rkCasterPos, rkVisDir, rkTargetPos, iParam1, iParam2);
		}break;
	default:
		{
			VERIFY_INFO_LOG(false, BM::LOG_LV5, __FL__<<L"Not defined SkillAreaType ["<<eType<<L"]");
			bResult = false;
		}break;
	}
	if (bResult && 0<iMinRange && iMinRange > POINT3::Distance(rkCasterPos, rkTargetPos))	//�ȼ��É����� ����, ���É�� �ּҰŸ� ������ �Ÿ�üũ
	{
		bResult = false;	
	}
	if( !bResult )
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	}

	return bResult;
}

bool PgSkillAreaChooser::InAreaSphere(POINT3 const& rkCasterPos, NxVec3 const& rkVisDir, POINT3 const& rkTargetPos, int iRange, int iNothing)
{
	float fDistanceQ = GetDistanceQ(rkCasterPos, rkTargetPos);
	if (fDistanceQ > (float)iRange * iRange)
	{
		LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
		return false;
	}
	return true;
}

bool PgSkillAreaChooser::InAreaCube(POINT3 const& rkCasterPos, NxVec3 const& rkVisDir, POINT3 const& rkTargetPos, int iLength, int iWidth)
{
	//INFO_LOG(BM::LOG_LV5, _T("[%s] Not implemented"), __FUNCTIONW__);
	//INFO_LOG(BM::LOG_LV8, _T("[%s] Target Pos : %.4f, %.4f, %.4f"), __FUNCTIONW__, rkTargetPos.x, rkTargetPos.y, rkTargetPos.z);
	//return InAreaSphere(rkCasterPos, rkVisDir, rkTargetPos, iLength, iWidth);
	NxVec3 kTargetVec(rkTargetPos.x-rkCasterPos.x, rkTargetPos.y-rkCasterPos.y, rkTargetPos.z-rkCasterPos.z);
	if(false==kTargetVec.isZero())	//���� ��ǥ�� ����
	{
		float fDist = rkVisDir.dot(kTargetVec);
		if (fDist <= 0 || fDist > (float)iLength)
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}

		NxVec3 kVisDirNoZ(rkVisDir.x, rkVisDir.y, 0);
		kTargetVec.z = 0;
		NxVec3 kDirVec = kVisDirNoZ * fDist;
		if (kDirVec.distance(kTargetVec) > (float)iWidth)
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}
	}

	return __max(iWidth, AI_Z_LIMIT) > abs(rkCasterPos.z - rkTargetPos.z);
}

bool PgSkillAreaChooser::InAreaCone(POINT3 const& rkCasterPos, NxVec3 const& rkVisDir, POINT3 const& rkTargetPos, int iLength, int iDegree)
{
	//INFO_LOG(BM::LOG_LV5, _T("[%s] Not implemented"), __FUNCTIONW__);
	return InAreaSphere(rkCasterPos, rkVisDir, rkTargetPos, iLength, iDegree);
}

bool PgSkillAreaChooser::InAreaFrontSphere(POINT3 const& rkCasterPos, NxVec3 const& rkVisDir, POINT3 const& rkTargetPos, int iLength, int iLength2)
{
	POINT3 kCasterPos(rkVisDir.x, rkVisDir.y, rkVisDir.z);
	kCasterPos*=static_cast<float>(iLength);
	return InAreaSphere(rkCasterPos+kCasterPos, rkVisDir, rkTargetPos, iLength2, 0);
}


/*
TE_CasterCircle(POINT3 const& rkPos, int iRange)
	:m_ptPos(rkPos), m_iRange(iRange)
{
}

bool TE_CasterCircle::VInArea(POINT3 const& rkPos)
{
	float fDistQ = DistanceQ(rkPos, m_ptPos);
	if (fDistQ > m_iRange*m_iRange)
	{
		return false;
	}
	return false;
}

TE_CasterCube(POINT3 const& rkPos, int iRange)
	:m_ptPos(rkPos), m_iRange(iRange)
{
}

bool TE_CasterCube::VInArea(POINT3 const& rkPos)
{
	INFO_LOG(BM::LOG_LV5, _T("[%s] Not implemented SkillArea type"), __FUNCTIONW__);
	float fDistQ = DistanceQ(rkPos, m_ptPos);
	if (fDistQ > m_iRange*m_iRange)
	{
		return false;
	}
	return false;
}

TE_CasterCone(POINT3 const& rkPos, int iRange)
	:m_ptPos(rkPos), m_iRange(iRange)
{
}

bool TE_CasterCone::VInArea(POINT3 const& rkPos)
{
	INFO_LOG(BM::LOG_LV5, _T("[%s] Not implemented SkillArea type"), __FUNCTIONW__);
	float fDistQ = DistanceQ(rkPos, m_ptPos);
	if (fDistQ > m_iRange*m_iRange)
	{
		return false;
	}
	return false;
}
*/