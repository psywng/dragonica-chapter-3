#ifndef MAP_MAPSERVER_ACTION_AI_ENEMYSELECTOR_H
#define MAP_MAPSERVER_ACTION_AI_ENEMYSELECTOR_H

class PgSkillAreaChooser
{
public:
	PgSkillAreaChooser() {};
	~PgSkillAreaChooser() {};

	static bool InArea(CUnit * pkUnit, ESkillArea eType, POINT3 const& rkCasterPos, NxVec3 const& rkVisDir, POINT3 const& rkTargetPos, int iParam1, int iParam2, int const iMinRange = 0);
protected:
	static bool InAreaSphere(POINT3 const& rkCasterPos, NxVec3 const& rkVisDir, POINT3 const& rkTargetPos, int iRange, int iNothing);
	static bool InAreaCube(POINT3 const& rkCasterPos, NxVec3 const& rkVisDir, POINT3 const& rkTargetPos, int iLength, int iWidth);
	static bool InAreaCone(POINT3 const& rkCasterPos, NxVec3 const& rkVisDir, POINT3 const& rkTargetPos, int iLength, int iDegree);
	static bool InAreaFrontSphere(POINT3 const& rkCasterPos, NxVec3 const& rkVisDir, POINT3 const& rkTargetPos, int iLength, int iLength2);
};

/*
template< typename T >
class TESelector
{
public:
	TESelector()
	{}
	virtual ~TESelector(){}
public:
	virtual bool VInArea(T& rkT) = 0;
};

class TE_CasterCircle
	: public TESelector< POINT3 >
{
public:
	TE_CasterCircle(POINT3 const& rkPos, int iRange );

	virtual bool VInArea(POINT3 const& rkPos);

private:
	POINT3 const m_ptPos;
	int const m_iRange;
};

class TE_CasterCube
	: public TESelector< POINT3 >
{
public:
	TE_CasterCube(POINT3 const& rkPos, int iRange );

	virtual bool VInArea(POINT3 const& rkPos);

private:
	POINT3 const m_ptPos;
	int const m_iRange;
};

class TE_CasterCone
	: public TESelector< POINT3 >
{
public:
	TE_CasterCone(POINT3 const& rkPos, int iRange );

	virtual bool VInArea(POINT3 const& rkPos);

private:
	POINT3 const m_ptPos;
	int const m_iRange;
};
*/

#endif // MAP_MAPSERVER_ACTION_AI_ENEMYSELECTOR_H