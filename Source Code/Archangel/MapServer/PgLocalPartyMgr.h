#ifndef MAP_MAPSERVER_OBJECT_PARTY_PGLOCALPARTYMGR_H
#define MAP_MAPSERVER_OBJECT_PARTY_PGLOCALPARTYMGR_H

#include "Variant/PgPartyMgr.h"
class PgPartyItemRule;

//
class PgLocalPartyMgrImpl
	:	public PgPartyMgr< PgLocalParty >
{
	typedef std::set< SGroundKey > ContGndKeySet;
public:
	typedef std::map< SGroundKey, int > ContCheckOverlap;
	typedef BM::TObjectPool< PgPartyItemRule > PoolPartyItemRule;
	typedef std::map< BM::GUID, PgPartyItemRule* > ContPartyItemRule;

	PgLocalPartyMgrImpl();
	virtual ~PgLocalPartyMgrImpl();

	void Clear();

	bool GetPartyMemberGround(BM::GUID const& rkPartyGuid, SGroundKey const& rkGndKey, VEC_GUID& rkOutVec, BM::GUID const& rkIgnore = BM::GUID::NullData())const;
	size_t GetMemberCount(BM::GUID const& rkPartyGuid)const;
	bool GetTotalLevel(BM::GUID const& rkPartyGuid, SGroundKey const& rkGndKey, unsigned int& iOutTotalLevel);
	bool GetPartyMember(BM::GUID const& rkPartyGuid, VEC_GUID& rkOut)const;
	
	bool IsMaster(BM::GUID const& rkPartyGuid, BM::GUID const& rkCharGuid);
	bool GetPartyMasterGuid(BM::GUID const& rkPartyGuid, BM::GUID& rkOutGuid)const;

	bool GetPartyOption(BM::GUID const& rkPartyGuid, SPartyOption& rkOut);
	bool GetPartyShareItem_NextOwner(BM::GUID const& rkPartyGuid, SGroundKey const& rkGndKey, BM::GUID& rkNextOwner);

	bool WriteToPacketPartyName(BM::GUID const& rkPartyGuid, BM::CPacket& rkPacket);
	void WriteToPacketPartyList(BM::CPacket& rkPacket);
	
	bool ProcessMsg(unsigned short const usType, SGroundKey const& rkGndKey, BM::CPacket* pkMsg);
	bool SyncFromContents(BM::GUID const& rkPartyGuid, BM::CPacket& rkPacket);

	int GetPartyMemberFriend(BM::GUID const& rkPartyGuid, BM::GUID const& rkCharGuid);
protected:
	virtual bool Delete(BM::GUID const& rkPartyGuid);

	bool Leave(SGroundKey const& rkGndKey, BM::CPacket& rkPacket);
	bool ChangeMaster(BM::GUID const& rkPartyGuid, BM::GUID const& rkNewMasterGuid);
	bool MapMoved(BM::GUID const& rkPartyGuid, SGroundKey const& rkCurGndKey, SGroundKey const& rkTrgGndKey, VEC_GUID const& kVec);
	bool Rename(BM::GUID const& rkPartyGuid, std::wstring const& rkNewPartyName, SPartyOption const& rkNewPartyOption );

	bool AddItemRule(BM::GUID const& rkPartyGuid, PgPartyItemRule*& pkOut);//������ ���� �꿡 ���ؼ�
	bool GetItemRule(BM::GUID const& rkPartyGuid, PgPartyItemRule*& pkOut);
	bool DelItemRule(BM::GUID const& rkPartyGuid);
	bool SyncPartyItemRule(PgLocalParty* pkParty);

	bool WriteToPacketPartyName(PgLocalParty* pkParty, BM::CPacket& rkPacket);

protected:
	bool UpdateProperty(BM::GUID const& rkPartyGuid, BM::CPacket& rkPacket);
	bool IsRemainMemberInServer( PgLocalParty const *pkParty, SGroundKey const& rkGndKey )const;

	PoolPartyItemRule m_kItemRulePool;
	ContPartyItemRule m_kPartyItemRule;
};

//
class PgLocalPartyMgr
	: TWrapper< PgLocalPartyMgrImpl >
{
public:
	PgLocalPartyMgr();
	virtual ~PgLocalPartyMgr();

	void Clear();
	bool GetPartyMemberGround(BM::GUID const& rkPartyGuid, SGroundKey const& rkGndKey, VEC_GUID& rkOutVec, BM::GUID const& rkIgnore = BM::GUID::NullData())const;
	size_t GetMemberCount(BM::GUID const& rkPartyGuid)const;
	bool GetTotalLevel(BM::GUID const& rkPartyGuid, SGroundKey const& rkGndKey, unsigned int& iOutTotalLevel);
	bool GetPartyMember(BM::GUID const& rkPartyGuid, VEC_GUID& rkOut)const;
	bool IsMaster(BM::GUID const& rkPartyGuid, BM::GUID const& rkCharGuid);
	bool GetPartyMasterGuid(BM::GUID const& rkPartyGuid, BM::GUID& rkOutGuid)const;
	bool GetPartyOption(BM::GUID const& rkPartyGuid, SPartyOption& rkOut);
	bool GetPartyShareItem_NextOwner(BM::GUID const& rkPartyGuid, SGroundKey const& rkGndKey, BM::GUID& rkNextOwner);
	bool WriteToPacketPartyName(BM::GUID const& rkPartyGuid, BM::CPacket& rkPacket);
	void WriteToPacketPartyList(BM::CPacket& rkPacket);
	bool ProcessMsg(unsigned short const usType, SGroundKey const& rkGndKey, BM::CPacket* pkMsg);
	bool SyncFromContents(BM::GUID const& rkPartyGuid, BM::CPacket& rkPacket);
	int GetPartyMemberFriend(BM::GUID const& rkPartyGuid, BM::GUID const& rkCharGuid);
};

#endif // MAP_MAPSERVER_OBJECT_PARTY_PGLOCALPARTYMGR_H