#include "stdafx.h"
#include "PgRequest.h"

bool PgRequest::Send()const
{
	return SendToMissionMgr( m_kPacket );
}

PgRequest_MissionInfo::PgRequest_MissionInfo(int const iMissionKey, SGroundKey const& _rkGndkey, int rkType)
:	PgRequest( PT_M_N_REQ_MISSION_INFO)
,	m_iMissionKey(iMissionKey)
,	m_iType(rkType)
{
	m_kPacket.Push(_rkGndkey);
	m_kPacket.Push(m_iMissionKey);
	m_kPacket.Push(m_iType);
}

bool PgRequest_MissionInfo::DoAction(PgPlayer *pkPlayer)
{
	PgPlayer_MissionData const *pkMissionData = pkPlayer->GetMissionData( (unsigned int)m_iMissionKey );
	if ( pkMissionData )
	{
		pkMissionData->WriteToPacket( m_kPacket );
		m_kPacket.Push(pkPlayer->GetID());
//		m_kPacket.Push(pkPlayer->GetMemberGUID());
		m_kPacket.Push(pkPlayer->GetAbil(AT_LEVEL));
		return Send();
	}
	LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
	return false;
}

PgRequest_MissionJoin::PgRequest_MissionJoin(SMissionKey const& _rkMissionKey, SGroundKey const& _rkGndkey, int const rkType, BM::CPacket * const pkPacket)
:	PgRequest(PT_M_N_REQ_ENTER_MISSION)	
,	m_iType(rkType)
,   m_pkAddonPacket(*pkPacket)
{
	m_kPacket.Push(_rkMissionKey);
	m_kPacket.Push(_rkGndkey);
	m_kPacket.Push(m_iType);
}

bool PgRequest_MissionJoin::DoAction(PgPlayer *pkPlayer)
{
	m_kPacket.Push(pkPlayer->GetID());	// Owner
	m_kPacket.Push(pkPlayer->GetAbil(AT_LEVEL));	// Contents Server가 레벨을 검사하여야 한다.
	m_kPacket.Push(m_pkAddonPacket);
	return Send();
}

PgRequest_MissionReStart::PgRequest_MissionReStart(BM::GUID const & _kMissionID, SMissionKey const& _kReMissionKey, BM::CPacket * const pkPacket)
:	PgRequest(PT_C_M_REQ_MISSION_RESTART)
,   m_pkAddonPacket(*pkPacket)
{
	m_kPacket.Push(_kMissionID);
	m_kPacket.Push(_kReMissionKey);
}

bool PgRequest_MissionReStart::DoAction(PgPlayer *pkPlayer)
{
	m_kPacket.Push(pkPlayer->GetAbil(AT_LEVEL));
	m_kPacket.Push(m_pkAddonPacket);
	return Send();
}

PgRequest_Notice::PgRequest_Notice( E_NOTICE_TYPE kType )
:	PgRequest(PT_M_N_NFY_NOTICE_PACKET)
,	m_kType(kType)
{
}

bool PgRequest_Notice::DoAction( BM::CPacket &kPacket )
{
	if ( m_kGuidList.empty() )
	{
		if ( m_kType == NOTICE_ALL )
		{
			m_kGuidList.push_back( BM::GUID::NullData() );
		}
		else
		{
			LIVE_CHECK_LOG(BM::LOG_LV1, __FL__ << _T("Return false"));
			return false;
		}
	}
	return Send( kPacket );
}

void PgRequest_Notice::Add( BM::GUID const &kGuid )
{
	m_kGuidList.push_back( kGuid );
}

bool PgRequest_Notice::Send( BM::CPacket &kPacket )
{
	m_kPacket.Push( m_kType );
	m_kPacket.Push( m_kGuidList );
	m_kPacket.Push( kPacket );
	return SendToContents( m_kPacket );
}

PgRequest_CheckPenalty::PgRequest_CheckPenalty( SGroundKey const &kGndKey, WORD const wType, BM::CPacket * const pkPacket )
:	PgRequest(PT_M_N_REQ_CHECK_PENALTY)
,	m_kType(wType)
,	m_pkPacket(pkPacket)
{
	m_kPacket.Push( g_kProcessCfg.ChannelNo() );
	m_kPacket.Push( kGndKey );
}

bool PgRequest_CheckPenalty::DoAction( PgPlayer *pkPlayer )
{
	m_kPacket.Push( pkPlayer->GetID() );
	m_kPacket.Push( m_kType );

	if ( m_pkPacket )
	{
		size_t const iRDPos = m_pkPacket->RdPos();
		m_pkPacket->PosAdjust();
		m_kPacket.Push( *m_pkPacket );
		m_pkPacket->RdPos( iRDPos );
	}

	return Send();
}

bool PgRequest_CheckPenalty::Send()const
{
	return SendToContents( m_kPacket );
}
