#ifndef SWITCH_SWITCHSERVER_PGKEYEVENT_H
#define SWITCH_SWITCHSERVER_PGKEYEVENT_H

extern bool RegistKeyEvent();
extern bool CALLBACK OnTerminateServer(WORD const& rkInputKey);

#endif // SWITCH_SWITCHSERVER_PGKEYEVENT_H