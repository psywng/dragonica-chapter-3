#ifndef LOGIN_LOGINSERVER_PGSENDWRAPPER_H
#define LOGIN_LOGINSERVER_PGSENDWRAPPER_H

extern bool SetSendWrapper(SERVER_IDENTITY const &kRecvSI);

extern bool SendToServer(SERVER_IDENTITY const &kSI, BM::CPacket const &rkPacket);
extern bool SendToImmigration(BM::CPacket const &rkPacket);
extern bool SendToLog(BM::CPacket const &rkPacket);
extern bool SendToGM(BM::CPacket const &rkPacket);

#endif // LOGIN_LOGINSERVER_PGSENDWRAPPER_H