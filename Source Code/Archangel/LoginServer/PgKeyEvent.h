#ifndef LOGIN_LOGINSERVER_PGKEYEVENT_H
#define LOGIN_LOGINSERVER_PGKEYEVENT_H

extern bool RegistKeyEvent();
extern bool CALLBACK OnTerminateServer(WORD const& rkInputKey);

#endif // LOGIN_LOGINSERVER_PGKEYEVENT_H