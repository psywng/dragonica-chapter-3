USE [DR2_Def]
GO
/****** Object:  Table [dbo].[TB_DefClassAbil_Skill]    Script Date: 06/30/2010 11:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_DefClassAbil_Skill](
	[AbilNo] [int] NOT NULL,
	[Memo] [nvarchar](100) COLLATE Korean_Wansung_CI_AS NULL,
	[Type01] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Type01]  DEFAULT ((0)),
	[Value01] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Value01]  DEFAULT ((0)),
	[Type02] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Type02]  DEFAULT ((0)),
	[Value02] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Value02]  DEFAULT ((0)),
	[Type03] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Type03]  DEFAULT ((0)),
	[Value03] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Value03]  DEFAULT ((0)),
	[Type04] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Type04]  DEFAULT ((0)),
	[Value04] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Value04]  DEFAULT ((0)),
	[Type05] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Type05]  DEFAULT ((0)),
	[Value05] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Value05]  DEFAULT ((0)),
	[Type06] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Type06]  DEFAULT ((0)),
	[Value06] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Value06]  DEFAULT ((0)),
	[Type07] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Type07]  DEFAULT ((0)),
	[Value07] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Value07]  DEFAULT ((0)),
	[Type08] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Type08]  DEFAULT ((0)),
	[Value08] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Value08]  DEFAULT ((0)),
	[Type09] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Type09]  DEFAULT ((0)),
	[Value09] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Value09]  DEFAULT ((0)),
	[Type10] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Type10]  DEFAULT ((0)),
	[Value10] [int] NOT NULL CONSTRAINT [DF_TB_DefClassAbil_Skill_Value10]  DEFAULT ((0)),
 CONSTRAINT [PK_TB_DefClassAbil_Skill] PRIMARY KEY CLUSTERED 
(
	[AbilNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ability table of TB_DefClass_Skill' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TB_DefClassAbil_Skill'
