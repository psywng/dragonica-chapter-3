/*
	Eric Kim, 2009-11-12
	최초작성
*/
USE [DR2_Def]
GO
/****** Object:  Table [dbo].[TB_DefMapAbil]    Script Date: 11/12/2009 16:42:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_DefMapAbil](
	[AbilNo] [int] NOT NULL,
	[Memo] [nvarchar](100) NULL,
	[Type01] [int] NOT NULL CONSTRAINT [DF_TB_DefMapAbil_Type01]  DEFAULT ((0)),
	[Value01] [int] NOT NULL CONSTRAINT [DF_TB_DefMapAbil_Value01]  DEFAULT ((0)),
	[Type02] [int] NOT NULL CONSTRAINT [DF_Table_1_Type011]  DEFAULT ((0)),
	[Value02] [int] NOT NULL CONSTRAINT [DF_Table_1_Value011]  DEFAULT ((0)),
	[Type03] [int] NOT NULL CONSTRAINT [DF_Table_1_Type011_1]  DEFAULT ((0)),
	[Value03] [int] NOT NULL CONSTRAINT [DF_Table_1_Value011_1]  DEFAULT ((0)),
	[Type04] [int] NOT NULL CONSTRAINT [DF_Table_1_Type011_2]  DEFAULT ((0)),
	[Value04] [int] NOT NULL CONSTRAINT [DF_Table_1_Value011_2]  DEFAULT ((0)),
	[Type05] [int] NOT NULL CONSTRAINT [DF_Table_1_Type011_3]  DEFAULT ((0)),
	[Value05] [int] NOT NULL CONSTRAINT [DF_Table_1_Value011_3]  DEFAULT ((0)),
	[Type06] [int] NOT NULL CONSTRAINT [DF_Table_1_Type051]  DEFAULT ((0)),
	[Value06] [int] NOT NULL CONSTRAINT [DF_Table_1_Value051]  DEFAULT ((0)),
	[Type07] [int] NOT NULL CONSTRAINT [DF_Table_1_Type051_1]  DEFAULT ((0)),
	[Value07] [int] NOT NULL CONSTRAINT [DF_Table_1_Value051_1]  DEFAULT ((0)),
	[Type08] [int] NOT NULL CONSTRAINT [DF_Table_1_Type051_2]  DEFAULT ((0)),
	[Value08] [int] NOT NULL CONSTRAINT [DF_Table_1_Value051_2]  DEFAULT ((0)),
	[Type09] [int] NOT NULL CONSTRAINT [DF_Table_1_Type051_3]  DEFAULT ((0)),
	[Value09] [int] NOT NULL CONSTRAINT [DF_Table_1_Value051_3]  DEFAULT ((0)),
	[Type10] [int] NOT NULL CONSTRAINT [DF_Table_1_Type051_4]  DEFAULT ((0)),
	[Value10] [int] NOT NULL CONSTRAINT [DF_Table_1_Value051_4]  DEFAULT ((0)),
 CONSTRAINT [PK_TB_DefMapAbil] PRIMARY KEY CLUSTERED 
(
	[AbilNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
