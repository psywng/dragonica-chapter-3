USE [DR2_Def]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_DefMapEntity]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[TB_DefMapEntity](
		[IDX] [int] IDENTITY(1,1) NOT NULL,
		[GroundNo] [int] NOT NULL,
		[Memo] [nvarchar](max) NULL,
		[iClass] [int] NOT NULL,
		[iLevel] [smallint] NOT NULL,
		[iEffect] [int] NOT NULL,
		[MinX] [real] NOT NULL,
		[MinY] [real] NOT NULL,
		[MinZ] [real] NOT NULL,
		[MaxX] [real] NOT NULL,
		[MaxY] [real] NOT NULL,
		[MaxZ] [real] NOT NULL,
		[OwnerPointGuid] [Uniqueidentifier] NULL
	) ON [PRIMARY]

	EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Map entity definition
	Detail :
	Some maps have special area which have special effect like poision' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TB_DefMapEntity'
END
ELSE
BEGIN
	IF NOT EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'dbo.TB_DefMapEntity') and [name] = N'OwnerPointGuid')
	BEGIN
		ALTER TABLE [dbo].[TB_DefMapEntity] ADD [OwnerPointGuid] [Uniqueidentifier] NULL
	END
END
GO

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'dbo.TB_DefMapEntity') and [name] = N'PK_TB_DefMapEntity')
BEGIN
	ALTER TABLE dbo.TB_DefMapEntity ADD CONSTRAINT
	PK_TB_DefMapEntity PRIMARY KEY CLUSTERED 
	(
	IDX
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END