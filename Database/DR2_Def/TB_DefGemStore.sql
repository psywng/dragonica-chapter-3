USE [DR2_DEF]
GO
/****** Object:  Table [dbo].[TB_DefGemStore]    Script Date: 04/14/2010 15:33:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_DefGemStore]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[TB_DefGemStore](
		[idx] [int] IDENTITY(1,1) NOT NULL,
		[Memo] [nvarchar](50) COLLATE Korean_Wansung_CI_AS NULL,
		[NpcGuid] [uniqueidentifier] NOT NULL,
		[Menu] [int] NOT NULL,
		[OrderIndex] [int] NOT NULL,
		[ItemNo] [int] NOT NULL,
		[ItemCount] [int] NOT NULL,
		[Gem1] [int] NULL,
		[GemCount1] [smallint] NULL,
		[Gem2] [int] NULL,
		[GemCount2] [smallint] NULL,
		[Gem3] [int] NULL,
		[GemCount3] [smallint] NULL,
		[Gem4] [int] NULL,
		[GemCount4] [smallint] NULL,
		[Gem5] [int] NULL,
		[GemCount5] [smallint] NULL,
	 CONSTRAINT [PK_TB_DefGemStore] PRIMARY KEY CLUSTERED 
	(
		[idx] ASC
	)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
	) ON [PRIMARY]
END

GO
