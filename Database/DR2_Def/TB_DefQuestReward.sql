USE DR2_Def
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
/*
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.TB_DefQuestReward
	DROP CONSTRAINT DF_TB_DefQuestReward_NationCode
GO
ALTER TABLE dbo.TB_DefQuestReward
	DROP CONSTRAINT DF_TB_DefQuestReward_GroupNo
GO
ALTER TABLE dbo.TB_DefQuestReward
	DROP CONSTRAINT DF_TB_DefQuestReward_IsType
GO
ALTER TABLE dbo.TB_DefQuestReward
	DROP CONSTRAINT DF_TB_DefQuestReward_LevelMin
GO
ALTER TABLE dbo.TB_DefQuestReward
	DROP CONSTRAINT DF_TB_DefQuestReward_LevelMax
GO
ALTER TABLE dbo.TB_DefQuestReward
	DROP CONSTRAINT DF_TB_DefQuestReward_TacticsLevel
GO
ALTER TABLE dbo.TB_DefQuestReward
	DROP CONSTRAINT DF_TB_DefQuestReward_TacticsExp
GO
ALTER TABLE dbo.TB_DefQuestReward
	DROP CONSTRAINT DF_TB_DefQuestReward_GuildExp
GO
ALTER TABLE dbo.TB_DefQuestReward
	DROP CONSTRAINT DF_TB_DefQuestReward_QuestDifficult
GO
ALTER TABLE dbo.TB_DefQuestReward
	DROP CONSTRAINT DF_TB_DefQuestReward_OrderIndex
GO
CREATE TABLE dbo.Tmp_TB_DefQuestReward
	(
	QuestID int NOT NULL,
	Memo nvarchar(50) NULL,
	NationCode int NOT NULL,
	XmlPath nvarchar(100) NULL,
	GroupNo int NOT NULL,
	QuestType int NOT NULL,
	RewardExp int NOT NULL,
	RewardGold int NOT NULL,
	LevelMin int NOT NULL,
	LevelMax int NOT NULL,
	LimitLevelMax int NOT NULL,
	TacticsLevel int NOT NULL,
	TacticsExp int NOT NULL,
	GuildExp int NOT NULL,
	QuestDifficult int NOT NULL,
	f_OrderIndex int NOT NULL
	)  ON [PRIMARY]
GO
DECLARE @v sql_variant 
SET @v = N'Quest reward definition'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_TB_DefQuestReward', NULL, NULL
GO
ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
	DF_TB_DefQuestReward_NationCode DEFAULT ((-1)) FOR NationCode
GO
ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
	DF_TB_DefQuestReward_GroupNo DEFAULT ((0)) FOR GroupNo
GO
ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
	DF_TB_DefQuestReward_IsType DEFAULT ((0)) FOR QuestType
GO
ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
	DF_TB_DefQuestReward_LevelMin DEFAULT ((1)) FOR LevelMin
GO
ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
	DF_TB_DefQuestReward_LevelMax DEFAULT ((9999)) FOR LevelMax
GO
ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
	DF_TB_DefQuestReward_LimitLevelMax DEFAULT 0 FOR LimitLevelMax
GO
ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
	DF_TB_DefQuestReward_TacticsLevel DEFAULT ((0)) FOR TacticsLevel
GO
ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
	DF_TB_DefQuestReward_TacticsExp DEFAULT ((0)) FOR TacticsExp
GO
ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
	DF_TB_DefQuestReward_GuildExp DEFAULT ((0)) FOR GuildExp
GO
ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
	DF_TB_DefQuestReward_QuestDifficult DEFAULT ((0)) FOR QuestDifficult
GO
ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
	DF_TB_DefQuestReward_OrderIndex DEFAULT ((0)) FOR f_OrderIndex
GO
IF EXISTS(SELECT * FROM dbo.TB_DefQuestReward)
	 EXEC('INSERT INTO dbo.Tmp_TB_DefQuestReward (QuestID, Memo, NationCode, XmlPath, GroupNo, QuestType, RewardExp, RewardGold, LevelMin, LevelMax, TacticsLevel, TacticsExp, GuildExp, QuestDifficult, f_OrderIndex)
		SELECT QuestID, Memo, NationCode, XmlPath, GroupNo, QuestType, RewardExp, RewardGold, LevelMin, LevelMax, TacticsLevel, TacticsExp, GuildExp, QuestDifficult, f_OrderIndex FROM dbo.TB_DefQuestReward WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.TB_DefQuestReward
GO
EXECUTE sp_rename N'dbo.Tmp_TB_DefQuestReward', N'TB_DefQuestReward', 'OBJECT' 
GO
ALTER TABLE dbo.TB_DefQuestReward ADD CONSTRAINT
	PK_TB_DefQuestReward_QuestID_NationCode PRIMARY KEY CLUSTERED 
	(
	QuestID,
	NationCode
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
*/

-- Add Remote Complete
--BEGIN TRANSACTION
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'dbo.TB_DefQuestReward') AND [name] = 'f_IsCanRemoteComplete')
BEGIN
	ALTER TABLE dbo.TB_DefQuestReward ADD
		f_IsCanRemoteComplete tinyint NOT NULL CONSTRAINT DF_TB_DefQuestReward_f_IsCanRemoteComplete DEFAULT 0
END
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'dbo.TB_DefQuestReward') AND [name] = 'f_IsCanShare')
BEGIN
	ALTER TABLE dbo.TB_DefQuestReward ADD
		f_IsCanShare tinyint NOT NULL CONSTRAINT DF_TB_DefQuestReward_f_IsCanShare DEFAULT 1
END
GO
--COMMIT

/*
	2010.12.28 퀘스트 테이블 통합 작업
*/
-----------------------------------------------------------------------------------------------------------------------------------BEGIN
IF NOT EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[TB_DefQuestReward]') AND [Name] = 'NationCodeStr')
BEGIN
	ALTER TABLE dbo.TB_DefQuestReward
		DROP CONSTRAINT DF_TB_DefQuestReward_NationCode
	
	ALTER TABLE dbo.TB_DefQuestReward
		DROP CONSTRAINT DF_TB_DefQuestReward_GroupNo
	
	ALTER TABLE dbo.TB_DefQuestReward
		DROP CONSTRAINT DF_TB_DefQuestReward_IsType
	
	ALTER TABLE dbo.TB_DefQuestReward
		DROP CONSTRAINT DF_TB_DefQuestReward_LevelMin
	
	ALTER TABLE dbo.TB_DefQuestReward
		DROP CONSTRAINT DF_TB_DefQuestReward_LevelMax
	
	ALTER TABLE dbo.TB_DefQuestReward
		DROP CONSTRAINT DF_TB_DefQuestReward_LimitLevelMax
	
	ALTER TABLE dbo.TB_DefQuestReward
		DROP CONSTRAINT DF_TB_DefQuestReward_TacticsLevel
	
	ALTER TABLE dbo.TB_DefQuestReward
		DROP CONSTRAINT DF_TB_DefQuestReward_TacticsExp
	
	ALTER TABLE dbo.TB_DefQuestReward
		DROP CONSTRAINT DF_TB_DefQuestReward_GuildExp
	
	ALTER TABLE dbo.TB_DefQuestReward
		DROP CONSTRAINT DF_TB_DefQuestReward_QuestDifficult
	
	ALTER TABLE dbo.TB_DefQuestReward
		DROP CONSTRAINT DF_TB_DefQuestReward_OrderIndex
	
	ALTER TABLE dbo.TB_DefQuestReward
		DROP CONSTRAINT DF_TB_DefQuestReward_f_IsCanRemoteComplete
	
	ALTER TABLE dbo.TB_DefQuestReward
		DROP CONSTRAINT DF_TB_DefQuestReward_f_IsCanShare
	
	CREATE TABLE dbo.Tmp_TB_DefQuestReward
		(
		NationCodeStr nvarchar(50) NOT NULL,
		QuestID int NOT NULL,
		Memo nvarchar(50) NULL,
		XmlPath nvarchar(100) NULL,
		GroupNo int NOT NULL,
		QuestType int NOT NULL,
		RewardExp int NOT NULL,
		RewardGold int NOT NULL,
		LevelMin int NOT NULL,
		LevelMax int NOT NULL,
		LimitLevelMax int NOT NULL,
		TacticsLevel int NOT NULL,
		TacticsExp int NOT NULL,
		GuildExp int NOT NULL,
		QuestDifficult int NOT NULL,
		f_OrderIndex int NOT NULL,
		f_IsCanRemoteComplete tinyint NOT NULL,
		f_IsCanShare tinyint NOT NULL,
		TitleText int NOT NULL,
		GroupName int NOT NULL,
		Class bigint NOT NULL,
		PreQuestAnd nvarchar(50) NOT NULL,
		PreQuestOr nvarchar(50) NOT NULL,
		NotQuest nvarchar(50) NOT NULL,
		MinParty int NOT NULL,
		MaxParty int NOT NULL
		)  ON [PRIMARY]
	
	DECLARE @v sql_variant 
	SET @v = N'Quest reward definition'
	EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_TB_DefQuestReward', NULL, NULL
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_NationCodeStr DEFAULT ((0)) FOR NationCodeStr
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_GroupNo DEFAULT ((0)) FOR GroupNo
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_IsType DEFAULT ((0)) FOR QuestType
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_LevelMin DEFAULT ((1)) FOR LevelMin
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_LevelMax DEFAULT ((9999)) FOR LevelMax
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_LimitLevelMax DEFAULT ((0)) FOR LimitLevelMax
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_TacticsLevel DEFAULT ((0)) FOR TacticsLevel
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_TacticsExp DEFAULT ((0)) FOR TacticsExp
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_GuildExp DEFAULT ((0)) FOR GuildExp
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_QuestDifficult DEFAULT ((0)) FOR QuestDifficult
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_OrderIndex DEFAULT ((0)) FOR f_OrderIndex
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_f_IsCanRemoteComplete DEFAULT ((0)) FOR f_IsCanRemoteComplete
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_f_IsCanShare DEFAULT ((1)) FOR f_IsCanShare
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_TitleText DEFAULT ((0)) FOR TitleText
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_GroupName DEFAULT ((0)) FOR GroupName
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_Class DEFAULT ((0)) FOR Class
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_PreQuestAnd DEFAULT ((0)) FOR PreQuestAnd
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_PreQuestOr DEFAULT ((0)) FOR PreQuestOr
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_NotQuest DEFAULT ((0)) FOR NotQuest
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_MinParty DEFAULT ((0)) FOR MinParty
	
	ALTER TABLE dbo.Tmp_TB_DefQuestReward ADD CONSTRAINT
		DF_TB_DefQuestReward_MaxParty DEFAULT ((0)) FOR MaxParty
	
	IF EXISTS(SELECT * FROM dbo.TB_DefQuestReward)
		 EXEC('INSERT INTO dbo.Tmp_TB_DefQuestReward (QuestID, Memo, XmlPath, GroupNo, QuestType, RewardExp, RewardGold, LevelMin, LevelMax, LimitLevelMax, TacticsLevel, TacticsExp, GuildExp, QuestDifficult, f_OrderIndex, f_IsCanRemoteComplete, f_IsCanShare)
			SELECT QuestID, Memo, XmlPath, GroupNo, QuestType, RewardExp, RewardGold, LevelMin, LevelMax, LimitLevelMax, TacticsLevel, TacticsExp, GuildExp, QuestDifficult, f_OrderIndex, f_IsCanRemoteComplete, f_IsCanShare FROM dbo.TB_DefQuestReward WITH (HOLDLOCK TABLOCKX)')
	
	DROP TABLE dbo.TB_DefQuestReward
	
	EXECUTE sp_rename N'dbo.Tmp_TB_DefQuestReward', N'TB_DefQuestReward', 'OBJECT' 
	
	ALTER TABLE dbo.TB_DefQuestReward ADD CONSTRAINT
		PK_TB_DefQuestReward PRIMARY KEY CLUSTERED 
		(
		NationCodeStr,
		QuestID
		) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
-----------------------------------------------------------------------------------------------------------------------------------END
