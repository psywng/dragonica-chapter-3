USE [DR2_Def]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_DefJobSkill_Shop]') and [type] in (N'U'))
BEGIN
	CREATE TABLE [dbo].[TB_DefJobSkill_Shop](
		[f_IDX] [int] NOT NULL,
		[f_Job_Category] [int] NOT NULL,
		[f_Kind_Category] [int] NOT NULL,
		[f_Memo] [nvarchar](50) NULL,
		[f_ItemNo] [int] NOT NULL,
		[f_Price] [int] NOT NULL,
		[f_NPC_Guid] [uniqueidentifier] NOT NULL,
		[f_TimeType] [tinyint] NOT NULL,
		[f_UseTime] [int] NOT NULL,
	 CONSTRAINT [PK_TB_DefJobSkill_Shop] PRIMARY KEY CLUSTERED 
	(
		[f_NPC_Guid] ASC,
		[f_ItemNo] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[TB_DefJobSkill_Shop]') AND [name] = N'f_ShowUser')
BEGIN
	ALTER TABLE dbo.[TB_DefJobSkill_Shop] ADD f_ShowUser int NOT NULL CONSTRAINT DF_TB_DefJobSkill_Shop_f_ShowUser DEFAULT 1
END
GO