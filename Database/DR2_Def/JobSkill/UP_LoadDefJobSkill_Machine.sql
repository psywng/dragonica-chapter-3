USE [DR2_Def]
GO

IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.UP_LoadDefJobSkill_Machine') AND [type] IN (N'P'))
BEGIN
	DROP PROCEDURE dbo.UP_LoadDefJobSkill_Machine
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.UP_LoadDefJobSkill_Machine
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [f_ItemNo]
		,[f_GatherType]
		,[f_01Need_JobSkill_No]
		,[f_01Need_JobSkillExpertness]
		,[f_02Need_JobSkill_No]
		,[f_02Need_JobSkillExpertness]
		,[f_03Need_JobSkill_No]
		,[f_03Need_JobSkillExpertness]
		,[f_Option_TurnTime]
		,[f_SlotCount]
	FROM [dbo].[TB_DefJobSkill_Machine]	
END
GO
