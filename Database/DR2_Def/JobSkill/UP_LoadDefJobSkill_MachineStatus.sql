USE [DR2_Def]
GO

IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.UP_LoadDefJobSkill_MachineStatus') AND [type] IN (N'P'))
BEGIN
	DROP PROCEDURE dbo.UP_LoadDefJobSkill_MachineStatus
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.UP_LoadDefJobSkill_MachineStatus
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [f_GatherType]
		,[f_Item_Grade]
		,[f_Machine_Status]
		,[f_Machine_FromStatus]
		,[f_Status_Probability]
		,[f_Status_TotalProbability]
		,[f_Status_TickTime]
		,[f_Status_ModelXmlPath]
		,[f_UnCommon_Txt]
		,[f_Repair_Txt]
	FROM [dbo].[TB_DefJobSkill_MachineStatus]
END
GO
