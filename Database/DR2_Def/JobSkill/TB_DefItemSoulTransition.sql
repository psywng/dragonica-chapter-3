USE [DR2_Def]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_DefItemSoulTransition]') and [type] in (N'U'))
BEGIN
	CREATE TABLE [dbo].[TB_DefItemSoulTransition](
		[f_Index] [int] NOT NULL,
		[f_EquipPos] [int] NOT NULL,
		[f_Memo] [nvarchar](50) NULL,
		[f_LevelLimit] [int] NOT NULL,
		[f_Rairity_Grade] [int] NOT NULL,
		[f_SuccessRate] [int] NOT NULL,
		[f_SoulItemCount] [int] NOT NULL,
		[f_NeedMoney] [int] NOT NULL,
		[f_InsuranceItemNo] [int] NOT NULL,
		[f_ProbabilityUpItemNo] [int] NOT NULL,
		[f_ProbabilityUpRate] [int] NOT NULL,
		[f_ProbabilityUpItemCount] [int] NOT NULL,
	CONSTRAINT [PK_TB_DefItemSoulTransition] PRIMARY KEY CLUSTERED 
		(
			[f_EquipPos]  ASC,
			[f_LevelLimit]  ASC,
			[f_Rairity_Grade]  ASC
		)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

IF EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'dbo.TB_DefItemSoulTransition') and [name] = N'f_EquipPos')
BEGIN
	EXECUTE sp_rename N'dbo.TB_DefItemSoulTransition.f_EquipPos', N'Tmp_f_f_EquipType', 'COLUMN' 
	EXECUTE sp_rename N'dbo.TB_DefItemSoulTransition.Tmp_f_f_EquipType', N'f_EquipType', 'COLUMN' 
END
GO