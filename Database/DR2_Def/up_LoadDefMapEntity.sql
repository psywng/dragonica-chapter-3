USE [DR2_Def]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_LoadDefMapEntity]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[up_LoadDefMapEntity]
GO


CREATE PROCEDURE [dbo].[up_LoadDefMapEntity]
AS
SET Nocount On;
BEGIN
	SELECT [GroundNo]	,[iClass]	,[iLevel]	,[iEffect]
		,[MinX]	,[MinY]	,[MinZ]	,[MaxX]	,[MaxY]	,[MaxZ], [OwnerPointGuid]
		FROM [dbo].[TB_DefMapEntity] WITH (NOLOCK) 		
END
GO
