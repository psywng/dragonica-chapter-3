USE [DR2_Def]
GO

--ALTER TABLE [dbo].[TB_DefMapEffect] ADD
--	[Realm] [smallint] NOT NULL CONSTRAINT [DF_TB_DefMapEffect_Realm] DEFAULT ((0)),
--	[Channel] [smallint] NOT NULL CONSTRAINT [DF_TB_DefMapEffect_Channel] DEFAULT ((0))
--GO

IF EXISTS ( SELECT 1 FROM sys.all_objects WHERE NAME = N'DF_TB_DefMapEffect_Realm' )
BEGIN
	ALTER TABLE [dbo].[TB_DefMapEffect] DROP CONSTRAINT [DF_TB_DefMapEffect_Realm]
	ALTER TABLE [dbo].[TB_DefMapEffect] DROP COLUMN [Realm]
END

IF EXISTS ( SELECT 1 FROM sys.all_objects WHERE NAME = N'DF_TB_DefMapEffect_Channel' )
BEGIN
	ALTER TABLE [dbo].[TB_DefMapEffect] DROP CONSTRAINT [DF_TB_DefMapEffect_Channel]
	ALTER TABLE [dbo].[TB_DefMapEffect] DROP COLUMN [Channel]
END