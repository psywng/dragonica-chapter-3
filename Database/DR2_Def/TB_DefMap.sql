USE [DR2_Def]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_DefMap]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[TB_DefMap](
		[IDX] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
		[MapNo] [int] NOT NULL,
		[MapKey] [int] NOT NULL,
		[Attribute] [int] NOT NULL,
		[Memo] [nvarchar](200) COLLATE Korean_Wansung_CI_AS NULL,
		[NameNo] [int] NOT NULL,
		[XmlPath] [nvarchar](100) COLLATE Korean_Wansung_CI_AS NOT NULL,
		[ZoneCX] [smallint] NOT NULL,
		[ZoneCY] [smallint] NOT NULL,
		[ZoneCZ] [smallint] NOT NULL,
		[Continent] [smallint] NOT NULL,
		[HometownNo] [smallint] NULL
	) ON [PRIMARY]
	
	RETURN
END
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[TB_DefMap]') and [name] = N'HometownNo')
BEGIN
	ALTER TABLE [dbo].[TB_DefMap] ADD[HometownNo] [smallint] NULL
END
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TB_DefMap_MaxUserNo]') AND type = 'D')
BEGIN
	ALTER TABLE [dbo].[TB_DefMap] DROP CONSTRAINT [DF_TB_DefMap_MaxUserNo]
END
GO

IF EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[TB_DefMap]') and [name] = N'MaxUserCount')
BEGIN
	EXECUTE sp_rename N'[dbo].[TB_DefMap].[MaxUserCount]', N'Continent', 'column'
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TB_DefMap_Hidden_Index]') AND type = 'D')
BEGIN
		ALTER TABLE [dbo].[TB_DefMap] ADD 
			[Hidden_Index] [smallint] NOT NULL CONSTRAINT [DF_TB_DefMap_Hidden_Index] DEFAULT ((0))	
END
GO

/*
	Eric Kim, 2009-11-12
	Abil column �߰�
*/
IF NOT EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[TB_DefMap]') and [name] in(N'Abil01', N'Abil02', N'Abil03', N'Abil04'))
BEGIN
	ALTER TABLE [dbo].[TB_DefMap] 
		ADD [Abil01] [int] NOT NULL CONSTRAINT [DF_TB_DefMap_Abil01]  DEFAULT ((0)),
		[Abil02] [int] NOT NULL CONSTRAINT [DF_TB_DefMap_Abil02]  DEFAULT ((0)),
		[Abil03] [int] NOT NULL CONSTRAINT [DF_TB_DefMap_Abil03]  DEFAULT ((0)),
		[Abil04] [int] NOT NULL CONSTRAINT [DF_TB_DefMap_Abil04]  DEFAULT ((0)),
		[Abil05] [int] NOT NULL CONSTRAINT [DF_TB_DefMap_Abil05]  DEFAULT ((0))
END
GO


IF NOT EXISTS(SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'dbo.TB_DefMap') and [name] = N'PK_TB_DefMap')
BEGIN
	ALTER TABLE dbo.TB_DefMap ADD CONSTRAINT
		PK_TB_DefMap PRIMARY KEY CLUSTERED 
		(
		MapNo
		) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO