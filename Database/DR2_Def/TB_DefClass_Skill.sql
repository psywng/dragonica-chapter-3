USE [DR2_Def]
GO
/****** Object:  Table [dbo].[TB_DefClass_Skill]    Script Date: 06/30/2010 10:49:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_DefClass_Skill](
	[iClass] [int] NOT NULL,
	[iLevel] [smallint] NOT NULL,
	[ClassName] [int] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_ClassName]  DEFAULT ((0)),
	[Memo] [nvarchar](100) COLLATE Korean_Wansung_CI_AS NULL,
	[Experience] [bigint] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_Experience_1]  DEFAULT ((0)),
	[MaxHP] [smallint] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_MaxHP_1]  DEFAULT ((0)),
	[MaxMP] [smallint] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_MaxMP_1]  DEFAULT ((0)),
	[Str] [smallint] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_Str_1]  DEFAULT ((0)),
	[Int] [smallint] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_Int_1]  DEFAULT ((0)),
	[Dex] [smallint] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_Dex_1]  DEFAULT ((0)),
	[Con] [smallint] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_Con_1]  DEFAULT ((0)),
	[HP_Recovery_Speed] [smallint] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_HP_Recovery_Speed_1]  DEFAULT ((0)),
	[HP_Recovery] [smallint] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_HP_Recovery_1]  DEFAULT ((0)),
	[MP_Recovery_Speed] [smallint] NOT NULL,
	[MP_Recovery] [smallint] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_MP_Recovery_1]  DEFAULT ((0)),
	[MoveSpeed] [smallint] NOT NULL,
	[Abil01] [int] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_Abil01_1]  DEFAULT ((0)),
	[Abil02] [int] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_Abil02_1]  DEFAULT ((0)),
	[Abil03] [int] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_Abil03_1]  DEFAULT ((0)),
	[Abil04] [int] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_Abil04_1]  DEFAULT ((0)),
	[Abil05] [int] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_Abil05_1]  DEFAULT ((0)),
	[Abil06] [int] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_Abil06_1]  DEFAULT ((0)),
	[Abil07] [int] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_Abil07_1]  DEFAULT ((0)),
	[Abil08] [int] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_Abil08_1]  DEFAULT ((0)),
	[Abil09] [int] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_Abil09_1]  DEFAULT ((0)),
	[Abil10] [int] NOT NULL CONSTRAINT [DF_TB_DefClass_Skill_Abil10_1]  DEFAULT ((0)),
 CONSTRAINT [PK_TB_DefClass_Skill] PRIMARY KEY CLUSTERED 
(
	[iClass] ASC,
	[iLevel] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Character class Skill and level definition' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TB_DefClass_Skill'
