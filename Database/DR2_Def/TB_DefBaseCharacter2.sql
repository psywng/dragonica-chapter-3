USE DR2_Def

/*
2009-03-10 ���� ������ Ȯ�� �߰�
*/

IF EXISTS (SELECT * FROM sys.all_objects WHERE NAME = 'DF_TB_DefBaseCharacter2_f_QuickSlot')
BEGIN
	ALTER TABLE [dbo].[TB_DefBaseCharacter2] DROP CONSTRAINT [DF_TB_DefBaseCharacter2_f_QuickSlot]
END

ALTER TABLE [dbo].[TB_DefBaseCharacter2] ALTER COLUMN [f_QuickSlot] [binary] (281) NOT NULL

ALTER TABLE [dbo].[TB_DefBaseCharacter2] ADD CONSTRAINT [DF_TB_DefBaseCharacter2_f_QuickSlot] DEFAULT (CONVERT([binary](281),(0),(0))) FOR [f_QuickSlot]


IF NOT EXISTS (SELECT * FROM sys.all_objects WHERE NAME = 'DF_TB_DefBaseCharacter2_f_InvExtend')
BEGIN
	ALTER TABLE [dbo].[TB_DefBaseCharacter2] ADD [f_InvExtend] [binary] (40) NOT NULL CONSTRAINT [DF_TB_DefBaseCharacter2_f_InvExtend] DEFAULT (CONVERT([binary](40),(0),(0)))
END

IF EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[TB_DefBaseCharacter2]') AND [Name] = 'f_GMLevel')
BEGIN
	ALTER TABLE [dbo].[TB_DefBaseCharacter2] DROP COLUMN [f_GMLevel]
END
GO


IF EXISTS(SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'DF_TB_DefBaseCharacter2_f_IngQuest'))
BEGIN
	ALTER TABLE [dbo].[TB_DefBaseCharacter2] DROP CONSTRAINT [DF_TB_DefBaseCharacter2_f_IngQuest]
END
GO
ALTER TABLE [dbo].[TB_DefBaseCharacter2] ALTER COLUMN [f_IngQuest] binary(300) NOT NULL
GO
ALTER TABLE [dbo].[TB_DefBaseCharacter2] ADD CONSTRAINT [DF_TB_DefBaseCharacter2_f_IngQuest] DEFAULT (CONVERT([binary](300),(0),(0))) For [f_IngQuest]
GO


IF NOT EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[TB_DefBaseCharacter2]') AND [Name] = 'f_EndQuestExt')
BEGIN
	ALTER TABLE [dbo].[TB_DefBaseCharacter2] ADD [f_EndQuestExt] [binary](260) NOT NULL CONSTRAINT [DF_TB_DefBaseCharacter2_f_EndQuestExt] DEFAULT (CONVERT([Binary](260),(0),(0)))
END

/*
	ĳ���� ���� ����Ʈ ���� �ʵ� Ȯ��( ������� ����Ʈ) 2010.02.10 �赿ȯ
*/
IF EXISTS(SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'DF_TB_DefBaseCharacter2_f_IngQuest'))
BEGIN
	ALTER TABLE [dbo].[TB_DefBaseCharacter2] DROP CONSTRAINT [DF_TB_DefBaseCharacter2_f_IngQuest]
END
GO
ALTER TABLE [dbo].[TB_DefBaseCharacter2] ALTER COLUMN [f_IngQuest] binary(350) NOT NULL
GO
ALTER TABLE [dbo].[TB_DefBaseCharacter2] ADD CONSTRAINT [DF_TB_DefBaseCharacter2_f_IngQuest] DEFAULT (CONVERT([binary](350),(0),(0))) For [f_IngQuest]
GO