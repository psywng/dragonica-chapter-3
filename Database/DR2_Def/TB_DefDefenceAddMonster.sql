USE [DR2_Def]
GO

/****** Object:  Table [dbo].[TB_DefDefenceAddMonster]    Script Date: 02/12/2010 16:23:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_DefDefenceAddMonster]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[TB_DefDefenceAddMonster](
		[AddMonster_GroupNo] [int] NOT NULL,
		[Success_Count] [int] NOT NULL,
		[Memo] [nvarchar](100) COLLATE Korean_Wansung_CI_AS NULL,
		[Add_StageTime] [int] NOT NULL CONSTRAINT [DF_DefDefenceAddMonster_Add_StageTime]  DEFAULT ((0)),
		[MonsterNo] [int] NOT NULL CONSTRAINT [DF_DefDefenceAddMonster_MonsterNo]  DEFAULT ((0)),
		[Enchant_Probability] [int] NOT NULL CONSTRAINT [DF_DefDefenceAddMonster_Enchant_Probability]  DEFAULT ((0)),
		[ExpRate] [int] NOT NULL CONSTRAINT [DF_DefDefenceAddMonster_ExpRate]  DEFAULT ((0)),
		[ItemNo] [int] NOT NULL CONSTRAINT [DF_DefDefenceAddMonster_ItemNo]  DEFAULT ((0)),
		[DropRate] [int] NOT NULL CONSTRAINT [DF_DefDefenceAddMonster_DropRate]  DEFAULT ((0)),
	 CONSTRAINT [PK_TB_DefDefenceAddMonster] PRIMARY KEY CLUSTERED 
	(
		[AddMonster_GroupNo] ASC,
		[Success_Count] ASC
	)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
	) ON [PRIMARY]
END
ELSE
BEGIN
	IF NOT EXISTS(SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[TB_DefDefenceAddMonster]') AND [name] = N'MonRes')
	BEGIN
		ALTER TABLE [dbo].[TB_DefDefenceAddMonster] ADD [MonRes] [nvarchar](100) NULL
	END
END
GO

ALTER TABLE dbo.TB_DefDefenceAddMonster
	DROP CONSTRAINT DF_DefDefenceAddMonster_Add_StageTime
GO
ALTER TABLE dbo.TB_DefDefenceAddMonster
	DROP CONSTRAINT DF_DefDefenceAddMonster_MonsterNo
GO
ALTER TABLE dbo.TB_DefDefenceAddMonster
	DROP CONSTRAINT DF_DefDefenceAddMonster_Enchant_Probability
GO
ALTER TABLE dbo.TB_DefDefenceAddMonster
	DROP CONSTRAINT DF_DefDefenceAddMonster_ExpRate
GO
ALTER TABLE dbo.TB_DefDefenceAddMonster
	DROP CONSTRAINT DF_DefDefenceAddMonster_ItemNo
GO
ALTER TABLE dbo.TB_DefDefenceAddMonster
	DROP CONSTRAINT DF_DefDefenceAddMonster_DropRate
GO
CREATE TABLE dbo.Tmp_TB_DefDefenceAddMonster
	(
	f_NationCodeStr nvarchar(50) NOT NULL CONSTRAINT [DF_DefDefenceAddMonster_f_NationCodeStr]  DEFAULT ((0)),
	AddMonster_GroupNo int NOT NULL,
	Success_Count int NOT NULL,
	Memo nvarchar(100) NULL,
	Add_StageTime int NOT NULL,
	MonsterNo int NOT NULL,
	Enchant_Probability int NOT NULL,
	ExpRate int NOT NULL,
	ItemNo int NOT NULL,
	DropRate int NOT NULL,
	MonRes nvarchar(100) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_TB_DefDefenceAddMonster SET (LOCK_ESCALATION = TABLE)
GO
DECLARE @v sql_variant 
SET @v = N'디팬스 모드에서 나오는 몬스터들을 설정하는 테이블'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_TB_DefDefenceAddMonster', NULL, NULL
GO
ALTER TABLE dbo.Tmp_TB_DefDefenceAddMonster ADD CONSTRAINT
	DF_DefDefenceAddMonster_Add_StageTime DEFAULT ((0)) FOR Add_StageTime
GO
ALTER TABLE dbo.Tmp_TB_DefDefenceAddMonster ADD CONSTRAINT
	DF_DefDefenceAddMonster_MonsterNo DEFAULT ((0)) FOR MonsterNo
GO
ALTER TABLE dbo.Tmp_TB_DefDefenceAddMonster ADD CONSTRAINT
	DF_DefDefenceAddMonster_Enchant_Probability DEFAULT ((0)) FOR Enchant_Probability
GO
ALTER TABLE dbo.Tmp_TB_DefDefenceAddMonster ADD CONSTRAINT
	DF_DefDefenceAddMonster_ExpRate DEFAULT ((0)) FOR ExpRate
GO
ALTER TABLE dbo.Tmp_TB_DefDefenceAddMonster ADD CONSTRAINT
	DF_DefDefenceAddMonster_ItemNo DEFAULT ((0)) FOR ItemNo
GO
ALTER TABLE dbo.Tmp_TB_DefDefenceAddMonster ADD CONSTRAINT
	DF_DefDefenceAddMonster_DropRate DEFAULT ((0)) FOR DropRate
GO
IF EXISTS(SELECT * FROM dbo.TB_DefDefenceAddMonster)
	 EXEC('INSERT INTO dbo.Tmp_TB_DefDefenceAddMonster (AddMonster_GroupNo, Success_Count, Memo, Add_StageTime, MonsterNo, Enchant_Probability, ExpRate, ItemNo, DropRate, MonRes)
		SELECT AddMonster_GroupNo, Success_Count, Memo, Add_StageTime, MonsterNo, Enchant_Probability, ExpRate, ItemNo, DropRate, MonRes FROM dbo.TB_DefDefenceAddMonster WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.TB_DefDefenceAddMonster
GO
EXECUTE sp_rename N'dbo.Tmp_TB_DefDefenceAddMonster', N'TB_DefDefenceAddMonster', 'OBJECT' 
GO
ALTER TABLE dbo.TB_DefDefenceAddMonster ADD CONSTRAINT
	PK_TB_DefDefenceAddMonster PRIMARY KEY CLUSTERED 
	(
	f_NationCodeStr,
	AddMonster_GroupNo,
	Success_Count
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
