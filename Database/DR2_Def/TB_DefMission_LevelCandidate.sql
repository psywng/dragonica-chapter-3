USE DR2_Def

ALTER TABLE [dbo].[TB_DefMission_LevelCandidate] ADD
[RankSSS_Point] [int] NOT NULL CONSTRAINT [DF_TB_DefMission_LevelCandidate_RankSSS_Point] DEFAULT ((0)),
[RankSS_Point] [int] NOT NULL CONSTRAINT [DF_TB_DefMission_LevelCandidate_RankSS_Point] DEFAULT ((0))
GO
