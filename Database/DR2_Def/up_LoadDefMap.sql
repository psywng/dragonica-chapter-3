USE [DR2_Def]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_LoadDefMap]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[up_LoadDefMap]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[up_LoadDefMap]
AS

BEGIN
	SELECT	[dbo].[TB_DefMap].[MapNo],
		[dbo].[TB_DefMap].[MapKey],
		[dbo].[TB_DefMap].[Attribute],
		[dbo].[TB_DefMap].[NameNo],
		[dbo].[TB_DefMapToMission].[MissionNo],
		[dbo].[TB_DefMap].[XmlPath],
		[dbo].[TB_DefMap].[ZoneCX],
		[dbo].[TB_DefMap].[ZoneCY],
		[dbo].[TB_DefMap].[ZoneCZ],
		[dbo].[TB_DefMap].[Continent],
		[dbo].[TB_DefMap].[HometownNo],
		[dbo].[TB_DefMap].[Hidden_Index],
		[dbo].[TB_DefMap].[Abil01],
		[dbo].[TB_DefMap].[Abil02],
		[dbo].[TB_DefMap].[Abil03],
		[dbo].[TB_DefMap].[Abil04],
		[dbo].[TB_DefMap].[Abil05]
		FROM	[dbo].[TB_DefMap] LEFT OUTER JOIN
		[dbo].[TB_DefMapToMission] ON [dbo].[TB_DefMap].[MapNo] = [dbo].[TB_DefMapToMission].[MapNo]

	SELECT [AbilNo]
		,[Type01] ,[Value01] ,[Type02] ,[Value02]
		,[Type03] ,[Value03] ,[Type04] ,[Value04]
		,[Type05] ,[Value05] ,[Type06] ,[Value06]
		,[Type07] ,[Value07] ,[Type08] ,[Value08]
		,[Type09] ,[Value09] ,[Type10] ,[Value10]
		FROM [DR2_Def].[dbo].[TB_DefMapAbil]
END
GO
