USE [DR2_Def]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_DefSpecificReward]') AND type in (N'U'))
BEGIN
	DROP TABLE [dbo].[TB_DefSpecificReward]
END

USE [DR2_Local]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_DefSpecificReward]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[TB_DefSpecificReward](
		[Type] [int] NOT NULL,
		[SubType][int] NOT NULL,
		[RewardItemNo] [int] NOT NULL,
		[RewardCount] [bigint] NOT NULL,
		[Memo] [nvarchar](100) COLLATE Korean_Wansung_CI_AS NULL,
	 CONSTRAINT [PK_TB_DefSpecificReward] PRIMARY KEY CLUSTERED 
	(
		[Type] ASC,
		[SubType] ASC
	)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
	) ON [PRIMARY]
END
GO