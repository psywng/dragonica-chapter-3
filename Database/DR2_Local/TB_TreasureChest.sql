USE [DR2_Local]
GO

/****** Object:  Table [dbo].[TB_TreasureChest]    Script Date: 12/06/2010 14:23:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.TB_TreasureChest') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TB_TreasureChest](
	[BoxItemNo] [int] NOT NULL,
	[KeyItemNo] [int] NOT NULL,
	[memo] [nvarchar](50) NULL,
	[ResultItemNo] [int] NULL,
	[ResultItemCount] [smallint] NULL,
	[TimeType] [tinyint] NULL,
	[UseTime] [smallint] NULL,
	[Rarity] [tinyint] NULL,
	[EnchantType] [tinyint] NULL,
	[EnchantLevel] [tinyint] NULL,
	[Rate] [int] NULL,
	[Idx] [int] IDENTITY(1,1) NOT NULL,
	[Broadcast] [tinyint] NULL,
 CONSTRAINT [PK_TB_TreasureChest] PRIMARY KEY CLUSTERED 
(
	[Idx] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO


