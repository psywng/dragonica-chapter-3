USE [DR2_Local]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_DefPvPTime]') AND type in (N'U'))
BEGIN

	CREATE TABLE [dbo].[TB_DefPvPTime](
		[TypeID] [INT]
	,	[Memo] [NVARCHAR](100)
	,	[BeginTime_Hour] [TINYINT]
	,	[BeginTime_Minute] [TINYINT]
	,	[EndTime_Hour] [TINYINT]
	,	[EndTime_Minute] [TINYINT]
	,	[LevelLimit] [SMALLINT] NULL
	,	[iValue01] [INT] NULL
	,	[iValue02] [INT] NULL
	,	[iBalanceLevelMin] [INT] NOT NULL CONSTRAINT [TB_DefPvPTime_BalanceLevelMin] DEFAULT ((1))
	,	[iBalanceLevelMax] [INT] NOT NULL CONSTRAINT [TB_DefPvPTime_BalanceLevelMax] DEFAULT ((1))
	) ON [PRIMARY]
	
	CREATE INDEX [IX_TB_DefPvPTime] ON [dbo].[TB_DefPvPTime]([TypeID] ASC)
END
GO

ALTER TABLE [dbo].[TB_DefPvPTime] ADD [LevelLimit] [SMALLINT] NULL
GO
	
ALTER TABLE [dbo].[TB_DefPvPTime] ADD [iValue01] [INT] NULL
GO

ALTER TABLE [dbo].[TB_DefPvPTime] ADD [iValue02] [INT] NULL
GO

ALTER TABLE [dbo].[TB_DefPvPTime] ADD [iBalanceLevelMin] [INT] NOT NULL CONSTRAINT [TB_DefPvPTime_BalanceLevelMin] DEFAULT ((1))
GO

ALTER TABLE [dbo].[TB_DefPvPTime] ADD [iBalanceLevelMax] [INT] NOT NULL CONSTRAINT [TB_DefPvPTime_BalanceLevelMax] DEFAULT ((1))
GO

IF  NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[PK_TB_DefPvPTime]') AND type = 'D')
BEGIN
	
	ALTER TABLE [dbo].[TB_DefPvPTime]
	DROP CONSTRAINT [PK_TB_DefPvPTime]

	CREATE INDEX [IX_TB_DefPvPTime] ON [dbo].[TB_DefPvPTime]([TypeID] ASC)
END
