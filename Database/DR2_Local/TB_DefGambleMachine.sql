USE [DR2_Local]
GO
/****** Object:  Table [dbo].[TB_DefGambleMachine]    Script Date: 09/02/2009 13:34:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_DefGambleMachine](
	[Idx] [int] IDENTITY(1,1) NOT NULL,
	[Memo] [nvarchar](50) COLLATE Korean_Wansung_CI_AS NULL,
	[Coin] [tinyint] NOT NULL,
	[ItemNo] [int] NULL,
	[Rate] [int] NULL,
	[Broadcast] [tinyint] NULL,
	[TimeType] [tinyint] NULL,
	[UseTime] [smallint] NULL,
 CONSTRAINT [PK_TB_DefGambleMachine] PRIMARY KEY CLUSTERED 
(
	[Idx] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TB_DefGambleMachine] ADD
[Count] [smallint] NULL CONSTRAINT [DF_TB_DefGambleMachine_Count] DEFAULT ((1))
GO

ALTER TABLE [dbo].[TB_DefGambleMachine] ADD
[GroupNo] [int] NULL
GO
