USE DR2_User
GO

IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.UP_Delete_JobSkill_WorkBench_Log') and [type] = N'P')
BEGIN
	DROP PROCEDURE dbo.UP_Delete_JobSkill_WorkBench_Log
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.UP_Delete_JobSkill_WorkBench_Log
	@DateTime [datetime],
	@ItemGuid [uniqueidentifier]
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM [dbo].[TB_JobSkill_WorkBench_Log]
		WHERE [f_ItemGuid] = @ItemGuid
		AND [f_DateTime] <= @DateTime
END
GO
