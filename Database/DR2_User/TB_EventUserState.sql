USE [DR2_User]
GO

/****** 개체:  Table [dbo].[TB_EventUserState]    스크립트 날짜: 09/22/2009 02:41:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_EventUserState]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TB_EventUserState](
	[EventType] [tinyint] NOT NULL,
	[EventId] [int] NOT NULL,
	[MemberID] [uniqueidentifier] NOT NULL,
	[CharacterID] [uniqueidentifier] NOT NULL,
	[intData01] [int] NULL,
	[intData02] [int] NULL,
	[intData03] [int] NULL,
	[bigintData01] [bigint] NULL,
	[guidData01] [uniqueidentifier] NULL,
	[DataDate01] [datetime] NULL,
	[stringData01] [nvarchar](100) NULL
) ON [PRIMARY]
END
GO

ALTER TABLE [dbo].[TB_EventUserState] ALTER COLUMN [EventType] [int] NOT NULL
GO