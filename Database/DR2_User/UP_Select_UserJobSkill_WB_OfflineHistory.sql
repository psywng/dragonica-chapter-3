USE DR2_User
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.UP_Select_UserJobSkill_WB_OfflineHistory') and [type] in (N'P'))
BEGIN
	DROP PROCEDURE dbo.UP_Select_UserJobSkill_WB_OfflineHistory
END
Go

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.UP_Select_UserJobSkill_WB_OfflineHistory
	@CharGuid UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		[f_Type],
		[f_WorkBenchItemNo],
		[f_ItemNo]
	FROM [dbo].[TB_UserJobSkill_WB_OfflineHistory]
	WHERE [f_CharGuid] = @CharGuid
END
GO
