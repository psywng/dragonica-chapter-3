USE [DR2_User]
GO
/****** Object:  Table [dbo].[TB_GroupMail]    Script Date: 12/22/2009 15:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_GroupMail](
	[CmdGuid] [uniqueidentifier] NOT NULL,
	[LevelMin] [int] NULL,
	[LevelMax] [int] NULL,
	[Class] [int] NULL,
	[title] [nvarchar](50) COLLATE Korean_Wansung_CI_AS NULL,
	[text] [nvarchar](200) COLLATE Korean_Wansung_CI_AS NULL,
	[ItemNo] [int] NULL,
	[Count] [smallint] NULL,
	[Money] [bigint] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[FromName] [nvarchar](50) COLLATE Korean_Wansung_CI_AS NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TB_GroupMail] ADD FromName NVARCHAR(50) NULL
GO

CREATE NONCLUSTERED INDEX [IX_TB_GroupMail_Class] ON [dbo].[TB_GroupMail] ([Class])
GO
CREATE NONCLUSTERED INDEX [IX_TB_GroupMail_Guid] ON [dbo].[TB_GroupMail] ([CmdGuid])
GO
CREATE NONCLUSTERED INDEX [IX_TB_GroupMail_Level] ON [dbo].[TB_GroupMail] ([LevelMin], [LevelMax])
GO
CREATE NONCLUSTERED INDEX [IX_TB_GroupMail_Date] ON [dbo].[TB_GroupMail] ([StartDate], [EndDate])
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TB_GroupMail]') AND name = N'PK_TB_GroupMail')
	ALTER TABLE [dbo].[TB_GroupMail] DROP CONSTRAINT [PK_TB_GroupMail]
GO

