USE DR2_User
GO

IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.UP_Update_JobSkill_WorkBench_Log') and [type] = N'P')
BEGIN
	DROP PROCEDURE dbo.UP_Update_JobSkill_WorkBench_Log
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.UP_Update_JobSkill_WorkBench_Log
	@ItemGuid [uniqueidentifier],
	@DateTime [datetime],
	@EventType [tinyint],
	@Data [binary](50),
	@WhoName [nvarchar](20)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO [dbo].[TB_JobSkill_WorkBench_Log]
		([f_DateTime],
		[f_ItemGuid],
		[f_EventType],
		[f_Data],
		[f_WhoName])
	VALUES
		(@DateTime, @ItemGuid,
		@EventType, @Data, @WhoName)
END
GO
