USE [DR2_User]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_Load_WorkBench_status]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[up_Load_WorkBench_status]

USE [DR2_User]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		김동환
-- Create date: 10.09.03
-- Description:	가공중인 아이템/작업대 상태 로드
-- =============================================
CREATE PROCEDURE [dbo].[up_Load_WorkBench_status]
	@WorkBenchGuid UNIQUEIDENTIFIER
AS
BEGIN	
	SELECT [f_WorkBenchGuid]
	,[f_ItemNo]
	,[f_Duration]
	,[f_01SlotItemNo]
	,[f_01SlotEleapsedTime]
	,[f_02SlotItemNo]
	,[f_02SlotEleapsedTime]
	,[f_03SlotItemNo]
	,[f_03SlotEleapsedTime]
	,[f_04SlotItemNo]
	,[f_04SlotEleapsedTime]
	,[f_05SlotItemNo]
	,[f_05SlotEleapsedTime]
	,[f_06SlotItemNo]
	,[f_06SlotEleapsedTime]
	,[f_07SlotItemNo]
	,[f_07SlotEleapsedTime]
	,[f_08SlotItemNo]
	,[f_08SlotEleapsedTime]
	,[f_09SlotItemNo]
	,[f_09SlotEleapsedTime]
	,[f_10SlotItemNo]
	,[f_10SlotEleapsedTime]
	,[f_CurUpgradeSlot]
	,[f_IsHasTrouble]
	,[f_TroubleTime]
	,[f_BlessDurationSec]
	,[f_BlessCharGuid]
	,[f_BlessSkillNo]
	FROM [dbo].[TB_JobSkill_WorkBench_Status]
	WHERE [f_WorkBenchGuid] = @WorkBenchGuid
END