USE DR2_User
GO

IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.UP_Selete_JobSkill_WorkBench_Log') and [type] = N'P')
BEGIN
	DROP PROCEDURE dbo.UP_Selete_JobSkill_WorkBench_Log
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.UP_Selete_JobSkill_WorkBench_Log
	@ItemGuid [uniqueidentifier]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		[f_ItemGuid],
		[f_DateTime],
		[f_EventType],
		[f_Data],
		[f_WhoName]
	FROM [dbo].[TB_JobSkill_WorkBench_Log]
	WHERE
		[f_ItemGuid] = @ItemGuid
END
GO
