USE DR2_User
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.UP_Update_UserJobSkill_WB_OfflineHistory') and [type] in (N'P'))
BEGIN
	DROP PROCEDURE dbo.UP_Update_UserJobSkill_WB_OfflineHistory
END
Go

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.UP_Update_UserJobSkill_WB_OfflineHistory
	@CharGuid UNIQUEIDENTIFIER,
	@Type INT,
	@WorkbenchItemNo INT,
	@ItemNo INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[TB_UserJobSkill_WB_OfflineHistory](
		[f_CharGuid],
		[f_Type],
		[f_WorkBenchItemNo],
		[f_ItemNo])
	VALUES
		(@CharGuid, @Type, @WorkbenchItemNo, @ItemNo)
END
GO
