USE [DR2_User]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_Delete_WorkBench_status]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[up_Delete_WorkBench_status]

USE [DR2_User]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		김동환
-- Create date: 10.09.03
-- Description:	가공중인 아이템/작업대 상태 제거
-- =============================================
CREATE PROCEDURE [dbo].[up_Delete_WorkBench_status]
	@WorkBenchGuid UNIQUEIDENTIFIER
AS
BEGIN	
	DELETE 
	FROM [dbo].[TB_JobSkill_WorkBench_Status]
	WHERE [f_WorkBenchGuid] = @WorkBenchGuid
END