USE [DR2_User]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TB_UserPortal]'))
BEGIN

CREATE TABLE [dbo].[TB_UserPortal](
	[CharacterGuid] [uniqueidentifier] NOT NULL,
	[PortalGuid] [uniqueidentifier] NOT NULL,
	[Comment] [nvarchar](50) COLLATE Korean_Wansung_CI_AS NULL,
	[GroundNo] [int] NOT NULL,
	[x] [float] NOT NULL,
	[y] [float] NOT NULL,
	[z] [float] NOT NULL,
 CONSTRAINT [PK_TB_UserPortal] PRIMARY KEY CLUSTERED 
(
	[PortalGuid] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
	
END

