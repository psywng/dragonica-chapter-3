USE [DR2_User]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_Update_WorkBench_Status]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_Update_WorkBench_Status]

USE [DR2_User]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		김동환
-- Create date: 10.09.03
-- Description:	가공중인 아이템/작업대 상태 저장
-- =============================================
CREATE PROCEDURE [dbo].[UP_Update_WorkBench_Status]
	@WorkBenchGuid uniqueidentifier,
	@ItemNo int,
	@Duration int,
	@01SlotItemNo int,
	@01SlotEleapsedTime bigint,
	@02SlotItemNo int,
	@02SlotEleapsedTime bigint,
	@03SlotItemNo int,
	@03SlotEleapsedTime bigint,
	@04SlotItemNo int,
	@04SlotEleapsedTime bigint,
	@05SlotItemNo int,
	@05SlotEleapsedTime bigint,
	@06SlotItemNo int,
	@06SlotEleapsedTime bigint,
	@07SlotItemNo int,
	@07SlotEleapsedTime bigint,
	@08SlotItemNo int,
	@08SlotEleapsedTime bigint,
	@09SlotItemNo int,
	@09SlotEleapsedTime bigint,
	@10SlotItemNo int,
	@10SlotEleapsedTime bigint,
	@CurUpgradeSlot int,
	@IsHasTrouble tinyint,
	@TroubleTime bigint,
	@BlessDurationSec bigint,
	@BlessCharGuid UNIQUEIDENTIFIER,
	@BlessSkillNo int
AS
BEGIN	
	IF NOT EXISTS( SELECT 1 FROM dbo.TB_JobSkill_WorkBench_Status WHERE f_WorkBenchGuid = @WorkBenchGuid )
	BEGIN
		INSERT INTO dbo.TB_JobSkill_WorkBench_Status
		(
			f_WorkBenchGuid,
			f_ItemNo,
			f_Duration,
			f_01SlotItemNo,f_01SlotEleapsedTime,
			f_02SlotItemNo,f_02SlotEleapsedTime,
			f_03SlotItemNo,f_03SlotEleapsedTime,
			f_04SlotItemNo,f_04SlotEleapsedTime,
			f_05SlotItemNo,f_05SlotEleapsedTime,
			f_06SlotItemNo,f_06SlotEleapsedTime,
			f_07SlotItemNo,f_07SlotEleapsedTime,
			f_08SlotItemNo,f_08SlotEleapsedTime,
			f_09SlotItemNo,f_09SlotEleapsedTime,
			f_10SlotItemNo,f_10SlotEleapsedTime,
			f_CurUpgradeSlot,
			f_IsHasTrouble,
			f_TroubleTime,
			f_BlessDurationSec,
			f_BlessCharGuid,
			f_BlessSkillNo
		)
		VALUES
		(
			@WorkBenchGuid,
			@ItemNo,
			@Duration,
			@01SlotItemNo,@01SlotEleapsedTime,
			@02SlotItemNo,@02SlotEleapsedTime,
			@03SlotItemNo,@03SlotEleapsedTime,
			@04SlotItemNo,@04SlotEleapsedTime,
			@05SlotItemNo,@05SlotEleapsedTime,
			@06SlotItemNo,@06SlotEleapsedTime,
			@07SlotItemNo,@07SlotEleapsedTime,
			@08SlotItemNo,@08SlotEleapsedTime,
			@09SlotItemNo,@09SlotEleapsedTime,
			@10SlotItemNo,@10SlotEleapsedTime,
			@CurUpgradeSlot,
			@IsHasTrouble,
			@TroubleTime,
			@BlessDurationSec,
			@BlessCharGuid,
			@BlessSkillNo
		)
	END
	ELSE
	BEGIN
		UPDATE dbo.TB_JobSkill_WorkBench_Status SET
			f_ItemNo	=	@ItemNo,
			f_Duration	=	@Duration,
			f_01SlotItemNo	=	@01SlotItemNo,f_01SlotEleapsedTime	=	@01SlotEleapsedTime,
			f_02SlotItemNo	=	@02SlotItemNo,f_02SlotEleapsedTime	=	@02SlotEleapsedTime,
			f_03SlotItemNo	=	@03SlotItemNo,f_03SlotEleapsedTime	=	@03SlotEleapsedTime,
			f_04SlotItemNo	=	@04SlotItemNo,f_04SlotEleapsedTime	=	@04SlotEleapsedTime,
			f_05SlotItemNo	=	@05SlotItemNo,f_05SlotEleapsedTime	=	@05SlotEleapsedTime,
			f_06SlotItemNo	=	@06SlotItemNo,f_06SlotEleapsedTime	=	@06SlotEleapsedTime,
			f_07SlotItemNo	=	@07SlotItemNo,f_07SlotEleapsedTime	=	@07SlotEleapsedTime,
			f_08SlotItemNo	=	@08SlotItemNo,f_08SlotEleapsedTime	=	@08SlotEleapsedTime,
			f_09SlotItemNo	=	@09SlotItemNo,f_09SlotEleapsedTime	=	@09SlotEleapsedTime,
			f_10SlotItemNo	=	@10SlotItemNo,f_10SlotEleapsedTime	=	@10SlotEleapsedTime,
			f_CurUpgradeSlot	=	@CurUpgradeSlot,
			f_IsHasTrouble	=	@IsHasTrouble,
			f_TroubleTime	=	@TroubleTime,
			f_BlessDurationSec	=	@BlessDurationSec,
			f_BlessCharGuid = @BlessCharGuid,
			f_BlessSkillNo = @BlessSkillNo
		WHERE f_WorkBenchGuid = @WorkBenchGuid
	END
END
